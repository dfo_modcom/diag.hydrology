* This script is designed to plot a watershed map.
* It can be executed using a command like
*
*     grads -blc "diag.hydroshed.watershed.ppoints Fulldom_hires_outlets"
*
* - RD April 2021.

function plot(args)
fila = subwrd(args,1)".ctl"
filb = subwrd(args,1)".txt" ; fizb = "Fulldom_hires_outletz.txt"
filc = subwrd(args,1)"_ppoints.png"

"open "fila
"clear"
"set grid   off"
"set xlab   off" ; "set xlopts 1 3 0.18"
"set ylab   off" ; "set ylopts 1 3 0.18"
"set mpdraw off" ; "set yflip  on"

"set rgb 16 80 80 80"
"set rgb 99  0  0  0  0"
"set gxout fgrid" ; "set fgvals 1 16" ; "set grads off" ; "d maskout(1,topo)"
"set gxout grfill" ; "set ccols  4 99" ; "set clevs 0.99" ; "d smth9(const(maskout(riv,riv),1,-u))"
"set gxout fgrid" ; "set fgvals 0 4" ; "set grads off" ; "d maskout(lake-lake,lake)"

numb = 0
wcol = 2
filestat = read(filb)
while (sublin(filestat,1) = 0) & wcol < 9e9
  line = sublin(filestat,2)
  wlev = subwrd(line,1)
  wlat = subwrd(line,4) ; llat = subwrd(line,5)
  wlon = subwrd(line,2) ; llon = subwrd(line,6)
  numb = numb + 1

  if (llon > -87.7 & llat < 62.7 & llat > 40.6)
* wcol = 15
  if (wcol = 4) ; wcol = 5 ; endif
# "set gxout fgrid" ; "set fgvals "wlev" "wcol ; "d floacc"
# "set gxout contour" ; "set cthick 4" ; "set clab off"
# "set clevs 0.5" ; "set ccolor 1" ; "set cstyle 1"
# "set grads off" ; "d const(maskout(maskout(1,floacc-"wlev"),"wlev"-floacc),0,-u)"
  "q gr2xy "wlon" "wlat ; rec = sublin(result,1) ; obsx = subwrd(rec,3) ; obsy = subwrd(rec,6)
  "set line 1 1 5" ; "draw mark 3 "obsx" "obsy" 0.07" ; "set line 1 1 5"
  endif

  wcol = wcol + 1 ; if (wcol = 16) ; wcol = 2 ; endif
  filestat = read(filb)
endwhile
filestat = close(filb)
say numb" outflows read"

*"set gxout fgrid" ; "set fgvals  0 4" ; "set grads off" ; "d maskout(lake-lake,lake)"
*"set gxout fgrid" ; "set fgvals 0 4" ; "set grads off" ; "d riv"  ;* "d maskout(riv,riv)"
*"set gxout grfill" ; "set ccols  4 99" ; "set clevs 0.99" ; "d smth9(const(maskout(riv,riv),1,-u))"
*"set gxout fgrid" ; "set fgvals 0 4" ; "set grads off" ; "d maskout(lake-lake,lake)"

"q gxinfo"
line3 = sublin(result,3)
line4 = sublin(result,4)
x1 = subwrd(line3,4)
x2 = subwrd(line3,6)
y1 = subwrd(line4,4)
y2 = subwrd(line4,6)
xmid = (x1 + x2) / 2.0
ymid = (y1 + y2) / 2.0
say x1" "x2" "y1" "y2" "xmid ;* 2 10.5 0.75 7.75 6.25
"set string 1 bc 5" ; "set strsiz 0.23"
"draw string "xmid" "y2+0.2" Merit Watersheds and Rivers"

say "gxprint "filc" png white x1100 y850"
    "gxprint "filc" png white x1100 y850"
"quit"
