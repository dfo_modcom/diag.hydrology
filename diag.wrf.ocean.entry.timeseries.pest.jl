#=
 = Combine PEST's ensemble of three-hourly WRF-Hydro streamflow estimates with daily
 = HYDAT observations into a NetCDF file that includes performance metrics.  Ensemble
 = order follows the time of PEST output.  Shortened station names (for plotting) are
 = recorded in a separate text file.  In addition to streamflow, HYDAT obs indicators
 = are taken directly from source NetCDF in the hydat.calibration dir - RD Sep 2023.
 =#

using My, Printf, NetCDF

const DELT             = 3                              # timestep (hours)
const LAST             = @sprintf("-%d", 24 - DELT)     # last hour of the day with data
const MISS             = -9999.0                        # generic missing value

if (argc = length(ARGS)) != 2
  print("\nUsage: jjj $(basename(@__FILE__)) pest_nwm_DOMAIN_full_full_FORCING.era_2019-03-31_000214.res ../hydat.calibration/hydat_station_location\n\n")
  exit(1)
end
psta = ARGS[1] ; dirs = readdir(psta)
pstb = ARGS[1] * ".nc"
pstc = ARGS[1] * ".txt"
pstd = ARGS[1] * "/wrftest.par" ; fpa = My.ouvre(pstd, "r") ; linp = readlines(fpa) ; close(fpa)
hyda = ARGS[2] * ".chk"         ; fpa = My.ouvre(hyda, "r") ; lina = readlines(fpa) ; close(fpa)
hydb = ARGS[2] * ".csv"         ; fpa = My.ouvre(hydb, "r") ; linb = readlines(fpa) ; close(fpa)
hydc = ARGS[2] * ".out"         ; fpa = My.ouvre(hydc, "r") ; linc = readlines(fpa) ; close(fpa)
wrfl = Array{AbstractString}(undef,0)
wrfo = Array{AbstractString}(undef,0)

tmpa, tmpb, tmpc = split(linc[1])                                             # get data times at DELT intervals
hors = Array{Float64}(undef,0)
hora = tmpa[1:10] * "-00" ; tima = My.datesous("1900-01-01-00", hora, "hr")
horc = tmpc[1:10] * LAST  ; timc = My.datesous("1900-01-01-00", horc, "hr")
while tima <= timc
  global tima, hora
  push!(hors, tima)
  hora = dateadd(hora, DELT, "hr")
  tima = My.datesous("1900-01-01-00", hora, "hr")
end
nhor = length(hors) ; hora = tmpa[1:10] * "-00"
@printf("\nreading %5d values between %s and %s at %d-h intervals\n", nhor, hora, horc, DELT)

popt = Array{       Float64}(undef,0)                                         # read the optimal PEST parameters
pnam = Array{AbstractString}(undef,0)
for a = 2:length(linp)
  vals = split(linp[a])
  valp = parse(Float64, vals[2])
  push!(pnam,           vals[1])
  push!(popt,           valp)
end
npar = length(popt)
@printf("\nreading %5d optimal PEST parameters\n", npar)

snam = Array{AbstractString}(undef,0)                                         # read the valid station names
scod = Array{AbstractString}(undef,0)                                         # (with more than one data value)
cton = Dict{AbstractString, AbstractString}()                                 # and include a shortened version
for line in linc
  local tmpa, tmpb, tmpc, tmpd, tmpe
  if startswith(line, "found") && contains(line, "possible") && !endswith(line, "FILLER")
    nams = split(line, ":")
    tmpa = nams[1][end-6:end]
    tmpb = nams[2]
    tmpc = split(split(tmpb, " (")[1], " RIVER")[1]
    tmpd = split(tmpb, "]")[1] * " " * tmpc * "]"
    push!(scod,  tmpa)
    push!(snam,  tmpd)
    cton[tmpa] = tmpd
  end
end
nsta = length(snam)
@printf("reading %5d station%s\n", nsta, nsta != 1 ? "s" : "")

nsub = ndif = osiz = 0                                                        # read the PEST ensemble of WRF-Hydro
for dirt in dirs                                                              # streamflow output files (assuming
  global nsub, ndif, osiz                                                     # all are as large as the first read)
  pert = psta * "/" * dirt                                                    # and order them by PEST output time
  if isdir(pert) && startswith(dirt, "test")
    nsub += 1
    fils = readdir(pert)
    for file in fils
      full = pert * "/" * file
      if isfile(full) && startswith(file, "wrfo")
        osiz == 0 && (osiz = filesize(full))
        if osiz == filesize(full)
          push!(wrfl, full)
          push!(wrfo, file)
        else
          ndif += 1
        end
      end
    end
  end
end
indx = sortperm(wrfo) ; ordl = wrfl[indx]
npes =   length(wrfo) ; ordo = wrfo[indx]
@printf("reading %5d ensemble members (each %d bytes) in %d subdirs (and ignoring %d)\n\n", npes, osiz, nsub, ndif)

function nccreer(fn::AbstractString, tims::Array{Float64,1}, lats::Array{Float64,1}, lons::Array{Float64,1})
  nctim = NcDim("time", length(tims), atts = Dict{Any,Any}("units"=>"hours since 1900-01-01 00:00:0.0"), values = tims)
  nclat = NcDim( "lat", length(lats), atts = Dict{Any,Any}("units"=>"degrees_north"),                    values = lats)
  nclon = NcDim( "lon", length(lons), atts = Dict{Any,Any}("units"=> "degrees_east"),                    values = lons)
  ncvrs = Array{NetCDF.NcVar}(undef, 16)
  ncvrs[ 1] = NcVar("hydt", [       nclat, nctim], atts = Dict{Any,Any}("units"=>"m3/s", "missing_value"=>MISS), t=Float64, compress=-1)
  ncvrs[ 2] = NcVar("hydi", [       nclat, nctim], atts = Dict{Any,Any}("units"=>"none", "missing_value"=>MISS), t=Float64, compress=-1)
  ncvrs[ 3] = NcVar("flow", [nclon, nclat, nctim], atts = Dict{Any,Any}("units"=>"m3/s", "missing_value"=>MISS), t=Float64, compress=-1)
  ncvrs[ 4] = NcVar("para", [nclon,        nctim], atts = Dict{Any,Any}("units"=>"none", "missing_value"=>MISS), t=Float64, compress=-1)
  ncvrs[ 5] = NcVar("parb", [nclon,        nctim], atts = Dict{Any,Any}("units"=>"none", "missing_value"=>MISS), t=Float64, compress=-1)
  ncvrs[ 6] = NcVar("parc", [nclon,        nctim], atts = Dict{Any,Any}("units"=>"none", "missing_value"=>MISS), t=Float64, compress=-1)
  ncvrs[ 7] = NcVar("tmpa", [nclon, nclat       ], atts = Dict{Any,Any}("units"=>"none", "missing_value"=>MISS), t=Float64, compress=-1)
  ncvrs[ 8] = NcVar("tmpb", [nclon, nclat       ], atts = Dict{Any,Any}("units"=>"none", "missing_value"=>MISS), t=Float64, compress=-1)
  ncvrs[ 9] = NcVar("tmpc", [nclon, nclat       ], atts = Dict{Any,Any}("units"=>"none", "missing_value"=>MISS), t=Float64, compress=-1)
  ncvrs[10] = NcVar("tmpd", [nclon, nclat       ], atts = Dict{Any,Any}("units"=>"none", "missing_value"=>MISS), t=Float64, compress=-1)
  ncvrs[11] = NcVar("tmpe", [nclon, nclat       ], atts = Dict{Any,Any}("units"=>"none", "missing_value"=>MISS), t=Float64, compress=-1)
  ncvrs[12] = NcVar("tmpf", [nclon, nclat       ], atts = Dict{Any,Any}("units"=>"none", "missing_value"=>MISS), t=Float64, compress=-1)
  ncvrs[13] = NcVar("tmpg", [nclon, nclat       ], atts = Dict{Any,Any}("units"=>"none", "missing_value"=>MISS), t=Float64, compress=-1)
  ncvrs[14] = NcVar("tmph", [nclon, nclat       ], atts = Dict{Any,Any}("units"=>"none", "missing_value"=>MISS), t=Float64, compress=-1)
  ncvrs[15] = NcVar("tmpi", [nclon, nclat       ], atts = Dict{Any,Any}("units"=>"none", "missing_value"=>MISS), t=Float64, compress=-1)
  ncvrs[16] = NcVar("tmpj", [nclon, nclat       ], atts = Dict{Any,Any}("units"=>"none", "missing_value"=>MISS), t=Float64, compress=-1)
  ncfil = NetCDF.create(fn, ncvrs, gatts = Dict{Any,Any}("units"=>"none"), mode = NC_NETCDF4)
end
lats = collect(1.0:nsta)                                                      # create the NetCDF template
lons = collect(1.0:npes)
nccreer(pstb, hors, lats, lons)
#ncinfo(pstb)

upto = 0                                                                      # get the HYDAT source times
for a = 1:length(ARGS[2])  global upto ; ARGS[2][a] == '/' && (upto = a)  end
stem = ARGS[2][1:upto] ; upto == 0 && (stem = "./")
file = stem * "../hydat/hydat_zzzzzzz.nc"
days = ncread(file, "time", start=[1], count=[-1])

hydt = ones(nsta, nhor) * MISS                                                # read and store the HYDAT data
hydi = ones(nsta, nhor) * MISS                                                # (including obs indicators)
for a = 1:length(lina)
  local file
  vals = split(lina[a])
  datw = vals[6][end-9:end] * "-" * vals[7][1:2]
  delw = datesous("1900-01-01-00", datw, "hr")
  tind = findfirst(isequal(delw), hors)
  sind =            parse(  Int64, vals[8][1:end-1])
  hydt[sind,tind] = parse(Float64, vals[2])
  file = stem * vals[5]
  dind = findfirst(isequal(delw), days)
  hydi[sind,tind] = ncread(file, "floi", start=[1,1,dind], count=[-1,-1,-1])[1]
end
ncwrite(hydt, pstb, "hydt", start=[1,1], count=[-1,-1])
ncwrite(hydi, pstb, "hydi", start=[1,1], count=[-1,-1])

flow = ones(npes, nsta, nhor) * MISS                                          # read and store the PEST ensemble
para = ones(npes,       nhor) * MISS                                          # and their corresponding parameters
for a = 1:npes
  local fpa
  fpa = My.ouvre(ordl[a], "r") ; lino = readlines(fpa) ; close(fpa)
  para[a,1] = npar
  para[a,2] = 0.0
  for b = 1:npar
    vals = split(lino[b])
    para[a,b+2] = parse(Float64, vals[2])
  end
  for b = 10:length(lino)
    vals = split(lino[b], ",")
    datw = vals[2][1:10] * "-" * vals[2][12:13]
    delw = datesous("1900-01-01-00", datw, "hr")
    tind = findfirst(isequal(delw), hors)
    if !isnothing(tind)
      sind =              parse(  Int64, vals[3])
      flow[a,sind,tind] = parse(Float64, vals[6])
    end
  end
end

pdif = 9e9 ; pind = 0                                                         # and identify the ensemble member
for a = 1:npes                                                                # by para[:,2] that is PEST optimal
  global pdif, pind
  tmpd = sum(abs.(para[a,3:npar+2] .- popt))
  tmpd < pdif && (pdif = tmpd ; pind = a)
end
para[:,2] .= -pind ; para[pind,2] = pind

ncwrite(flow, pstb, "flow", start=[1,1,1], count=[-1,-1,-1])
ncwrite(para, pstb, "para", start=[1,1],   count=[-1,-1])

fpa = My.ouvre(pstc, "w")                                                     # finally, write supplementary text
for a = 2:length(linb)
  global fpa
  code = split(linb[a], ",")[4]
  in(code, scod) && write(fpa, linb[a] * "," * cton[code] * "\n")
end

foru = "" ; @printf("\nviewing ensemble member %5d out of %5d as PEST optimal\n", pind, npes)
foru *=    @sprintf("\nviewing ensemble member %5d out of %5d as PEST optimal\n", pind, npes)
for a = 1:npar
  global foru
  str = "~~~" ; popt[a] == para[pind,a+2] && (str = " = ")
           @printf("from wrftest.par %15s %15.8f %s %15.8f from %s\n", pnam[a], popt[a], str, para[pind,a+2], ordl[pind])
  foru *= @sprintf("from wrftest.par %15s %15.8f %s %15.8f from %s\n", pnam[a], popt[a], str, para[pind,a+2], ordl[pind])
end
write(fpa, foru)
close(fpa)
exit(0)

#=
  cval = GOOD                                                                 # is a good value by default
  code == "A" && (cval = -1.0)                                                # or partial day otherwise,
  code == "B" && (cval = -2.0)                                                # or ice conditions,
  code == "D" && (cval = -3.0)                                                # or dry,
  code == "E" && (cval = -4.0)                                                # or estimated,
  code == "S" && (cval = -5.0)                                                # or sample(s) taken
=#
