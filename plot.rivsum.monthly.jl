#=
 = Convert the RIVSUM csv dataset from https://catalogue.ogsl.ca/en/dataset
 = to a netcdf file - RD May 2020. 
 =#

using My, Printf, NetCDF

const DELT             = 24.0                           # timestep (hours)
const MISS             = -9999.0                        # generic missing value
const D2R              = 3.141592654 / 180.0            # degrees to radians conversion

if (argc = length(ARGS)) != 1
  print("\nUsage: jjj $(basename(@__FILE__)) rivsum.1955_2020\n\n")
  exit(1)
end
fila = ARGS[1]
filb = ARGS[1] * ".nc"

fpa = My.ouvre(fila, "r")
dat = Array{Float64}(undef,0)
riv = Array{Float64}(undef,0)
for line in eachline(fpa)
  if line[1:3] != "Ann"
    tmp = split(line, ",")
    for a = 1:12
      ref = tmp[1] * (a < 10 ? "0" : "") * @sprintf("%d", a) * "01"
      val = My.datesous("19000101", ref, "hr")
      push!(dat, val)
      val = tmp[a+1] == "" ? MISS : parse(Float64, tmp[a+1])
      push!(riv, val)
    end
  end
end
close(fpa)

nccreer(              filb, length(riv[210:365]), 1, 1, MISS)
ncwrite(riv[210:365], filb,  "tmp", start=[1,1,1], count=[-1,-1,-1])
ncwrite(dat[210:365], filb, "time", start=[1],     count=[-1])
ncputatt(             filb, "time", Dict("units" => "hours since 1900-01-01 00:00:0.0"))

filb = ARGS[1] * ".all.nc"
nccreer(     filb, length(riv), 1, 1, MISS)
ncwrite(riv, filb,  "tmp", start=[1,1,1], count=[-1,-1,-1])
ncwrite(dat, filb, "time", start=[1],     count=[-1])
ncputatt(    filb, "time", Dict("units" => "hours since 1900-01-01 00:00:0.0"))
exit(0)
