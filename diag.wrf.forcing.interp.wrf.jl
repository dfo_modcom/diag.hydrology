#=
 = Interpolate WRF surface data to the WRFHydro grid using ncl.  Daily WRF
 = files are converted to the expected unit and all variables are taken as
 = snapshots at 6-h intervals - RD March 2021.
 =#

using My, Printf, NetCDF

const HADGEM           = false                          # true for atypical post-processing

const DELT             = 6                              # number of hours between WRFHydro grids
const AGAIN            = false                          # recreate previously interpolated grids
if     in("WRFSDOM", keys(ENV)) && ENV["WRFSDOM"] == "_AuxF"
  const LATS           =      9                         # number of  latitudes in WRFHydro grid
  const LONS           =      9                         # number of longitudes in WRFHydro grid
  const OSIZ           =  18288                         # number of bytes of a typical output file
elseif in("WRFSDOM", keys(ENV)) && ENV["WRFSDOM"] == "_East"
  const LATS           =     54                         # number of  latitudes in WRFHydro grid
  const LONS           =     62                         # number of longitudes in WRFHydro grid
  const OSIZ           = 149752                         # number of bytes of a typical output file
else
  const LATS           =     86                         # number of  latitudes in WRFHydro grid
  const LONS           =    108                         # number of longitudes in WRFHydro grid
  const OSIZ           = 394872                         # number of bytes of a typical output file
end

if (argc = length(ARGS)) != 2
  print("\nUsage: jjj $(basename(@__FILE__)) 1990-01-01 2004-12-31\n\n")
  exit(1)
end
yearon = ARGS[1][1:4]
srcsrc = "wrfpost.surface.y" * yearon * "m01d01.nc"                           # define interpolation file names
dstsrc = "../DOMAIN_full_full/geogrid.nc"
nclsrc = "z_$yearon.nclcn.ncl" ; isfile(nclsrc) && rm(nclsrc)
srcwgt = "z_$yearon.srcwgt.nc" ; isfile(srcwgt) && rm(srcwgt)
dstwgt = "z_$yearon.dstwgt.nc" ; isfile(dstwgt) && rm(dstwgt)
nclwgt = "z_$yearon.nclwgt.nc" #; isfile(nclwgt) && rm(nclwgt)

function create_nclwgt()                                                      # generate weights for interpolating
  form  = "load \"\$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl\"\n"     # source variables to the dest grid
  form *= "load \"\$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl\"\n"      # (can't seem to turn off PETLog)
  form *= "load \"\$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl\"\n"
  form *= "load \"\$NCARG_ROOT/lib/ncarg/nclscripts/contrib/ut_string.ncl\"\n"
  form *= "load \"\$NCARG_ROOT/lib/ncarg/nclscripts/esmf/ESMF_regridding.ncl\"\n"
  form *= "begin\n"
  form *= "srcfil = addfile(\"$srcsrc\", \"r\")\n"
  form *= "srclat = srcfil->lat(:,:)\n"
  form *= "srclon = srcfil->lon(:,:)\n"
  form *= "Opt              = True\n"
  form *= "Opt@SrcRegional  = True\n"
  form *= "Opt@NoPETLog     = True\nOpt@ESMF_LOGKIND_Multi_On_Error = True\n"
  form *= "curvilinear_to_SCRIP(\"$srcwgt\", srclat, srclon, Opt)\ndelete(Opt)\n"
  form *= "dstfil = addfile(\"$dstsrc\", \"r\")\n"
  form *= "dstlat = dstfil->CLAT(0,:,:)\n"
  form *= "dstlon = dstfil->CLONG(0,:,:)\n"
  form *= "Opt              = True\n"
  form *= "Opt@DstRegional  = True\n"
  form *= "Opt@NoPETLog     = True\nOpt@ESMF_LOGKIND_Multi_On_Error = True\n"
  form *= "curvilinear_to_SCRIP(\"$dstwgt\", dstlat, dstlon, Opt)\ndelete(Opt)\n"
  form *= "Opt              = True\n"
  form *= "Opt@InterpMethod = \"bilinear\"\n"
  form *= "Opt@SrcRegional  = True\n"
  form *= "Opt@DstRegional  = True\n"
  form *= "Opt@NoPETLog     = True\nOpt@ESMF_LOGKIND_Multi_On_Error = True\n"
  form *= "ESMF_regrid_gen_weights(\"$srcwgt\", \"$dstwgt\", \"$nclwgt\", Opt)\ndelete(Opt)\n"
  form *= "end\n"
  fpa = My.ouvre(nclsrc, "w", false) ; write(fpa, form) ; close(fpa)
  print(" ncl $nclsrc\n")
     run(`ncl $nclsrc`)
            rm(nclsrc) ; rm(srcwgt) ; rm(dstwgt) ; rm("PET0.RegridWeightGen.Log") ; print("\n")
end

print("\n")
if !isfile(nclwgt)
  print("creating interpolation weights $nclwgt\n")
  create_nclwgt()
end 

function template(fn::AbstractString, nlat::Int64, nlon::Int64)               # define a NetCDF template using
  nctim = NcDim(       "Time",    1, values = [1])                            # HRLDAS-hr format (FORC_TYP = 1)
  nclat = NcDim("south_north", nlat, values = collect(1:nlat))
  nclon = NcDim(  "west_east", nlon, values = collect(1:nlon))
  nctat = NcVar(     "lat", [nclon, nclat       ], t=Float32, compress=-1)
  ncton = NcVar(     "lon", [nclon, nclat       ], t=Float32, compress=-1)
  ncair = NcVar(     "T2D", [nclon, nclat, nctim], t=Float32, compress=-1)
  ncshu = NcVar(     "Q2D", [nclon, nclat, nctim], t=Float32, compress=-1)
  ncuwd = NcVar(     "U2D", [nclon, nclat, nctim], t=Float32, compress=-1)
  ncvwd = NcVar(     "V2D", [nclon, nclat, nctim], t=Float32, compress=-1)
  ncprs = NcVar(    "PSFC", [nclon, nclat, nctim], t=Float32, compress=-1)
  ncrai = NcVar("RAINRATE", [nclon, nclat, nctim], t=Float32, compress=-1)
  ncswd = NcVar(  "SWDOWN", [nclon, nclat, nctim], t=Float32, compress=-1)
  nclwd = NcVar(  "LWDOWN", [nclon, nclat, nctim], t=Float32, compress=-1)
  ncfil = NetCDF.create(fn, [nctat, ncton, ncair, ncshu, ncuwd, ncvwd, ncprs, ncrai, ncswd, nclwd], mode = NC_NETCDF4)
  print("created $fn with $nlat lats and $nlon lons\n")
end

template(dstwgt, LATS, LONS)                                                  # and initialize invariants (lat/lon)
lats = ncread(dstsrc,  "CLAT", start=[1,1,1], count=[-1,-1,-1])[:,:,1]
lons = ncread(dstsrc, "CLONG", start=[1,1,1], count=[-1,-1,-1])[:,:,1]
ncwrite(lats, dstwgt,   "lat", start=[1,1],   count=[-1,-1])
ncwrite(lons, dstwgt,   "lon", start=[1,1],   count=[-1,-1])

function interpolate(srcfil::AbstractString, dstfil::AbstractString)          # then interpolate each daily file
  form  = "load \"\$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl\"\n"
  form *= "load \"\$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl\"\n"
  form *= "load \"\$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl\"\n"
  form *= "load \"\$NCARG_ROOT/lib/ncarg/nclscripts/contrib/ut_string.ncl\"\n"
  form *= "load \"\$NCARG_ROOT/lib/ncarg/nclscripts/esmf/ESMF_regridding.ncl\"\n"
  form *= "begin\n"
  form *= "Opt              = True\n"
  form *= "Opt@NoPETLog     = True\nOpt@ESMF_LOGKIND_Multi_On_Error = True\n"
  form *= "fila = addfile(\"$srcfil\", \"r\")\n"
  form *= "filb = addfile(\"$dstfil\", \"c\")\n"
  for a = 1:length(srcvar)
    form *= "vara = fila->$(srcvar[a])\n"
    form *= "varb = ESMF_regrid_with_weights(vara, \"$nclwgt\", Opt)\n"
    form *= "filb->$(srcvar[a]) = varb\n"
  end
  form *= "end\n"
  fpa = My.ouvre(nclsrc, "w", false) ; write(fpa, form) ; close(fpa)
  print(" ncl $nclsrc\n")
     run(`ncl $nclsrc`)
            rm(nclsrc) ; print("\n")
end

srcvar = ["T_2m", "rainncv",   "raincv",   "LW_d",     "SW", "p_sfc", "q_2m", "u_10m_tr", "v_10m_tr"]
dstvar = [ "T2D",        "", "RAINRATE", "LWDOWN", "SWDOWN",  "PSFC",  "Q2D",      "U2D",      "V2D"]
dstwgt = "z_1990.dstwgt.nc"

dnow = ARGS[1]                                                                # and save daily interpolations
flag = true                                                                   # to separate 6-h output files
while flag                                                                    # (using the template for each)
  global dnow, flag
  dstday =                       dnow[1:4] *       dnow[6:7] *       dnow[9:10]
  srcfil = "wrfpost.surface.y" * dnow[1:4] * "m" * dnow[6:7] * "d" * dnow[9:10] * ".nc"
  tmpfil = "wrfpost.surface.y" * dnow[1:4] * "m" * dnow[6:7] * "d" * dnow[9:10] * ".wrfh.nc"
  isfile(tmpfil) && rm(tmpfil) ; interpolate(srcfil, tmpfil)
  for (a, hour) in enumerate(["00", "06", "12", "18"])
    dstfil = dstday * hour * ".LDASIN_DOMAIN1"
#   if AGAIN || !isfile(dstfil)
    cp(dstwgt, dstfil; force = true)
    print("writing")
    for b = 1:length(dstvar)
      if dstvar[b] != ""
        print(" $(dstvar[b])")
                                    vara   = ncread(tmpfil, srcvar[b  ], start=[1,1,a], count=[-1,-1,1])
        dstvar[b] == "RAINRATE" && (vara  += ncread(tmpfil, srcvar[b-1], start=[1,1,a], count=[-1,-1,1]))
        dstvar[b] ==     "PSFC" && (vara  *= 100)
if HADGEM                                                                     # (allow that one corner of the
        if dstvar[b] ==   "T2D"                                               #  HadGEM grid is missing and
          (nlon, nlat, ntim) = size(vara)                                     #  temperature might not be in
          for c = 1:nlat, d = 1:nlon                                          #  Kelvin)
            vara[d,c,1] < -99 && (vara[d,c,1] += 273.15)
            vara[d,c,1] > 400 && (vara[d,c,1] -= 273.15)
          end
        end
        vara[1,end  ,1] = vara[2,end  ,1] = vara[3,end  ,1]
        vara[1,end-1,1] = vara[2,end-1,1] = vara[3,end-1,1]
        vara[1,end-2,1] = vara[2,end-2,1]
end
                            ncwrite(vara,           dstfil, dstvar[b  ], start=[1,1,1], count=[-1,-1,1])
      end
    end
    print(" to $dstfil\n")
  end
  rm(tmpfil)
  dnow == ARGS[2] && (flag = false)
  dnow = dateadd(dnow, 1, "dy")
end
exit(0)
