* This script is designed to plot a simple timeseries.
* It can be executed using a command like
*
*     grads -blc "diag.wrf.ocean.entry.timeseries streamflow.190101020000.CHRTOUT_GRID1.1094"
*
* - RD November 2012

function plot(args)
stem = subwrd(args,1)
fila = stem".narraw.nc" ; col.1 = 2
filb = stem".wrfraw.nc" ; col.2 = 1
filc = stem".wrfcal.nc" ; col.3 = 4
fild = stem".wrfcal.png"

"sdfopen "fila
"sdfopen "filb
"sdfopen "filc
"q file" ; ret = sublin(result,5) ; tims = subwrd(ret,12) ; "set t 0 "tims+1

"set vpage 0 11 4.5 8.5"
"set gxout line"
"set grid off"
"set vrange 0 0.13"
"set xlopts 1 3 0.18"
"set ylopts 1 3 0.18" ; "set ylint 0.04"
"set cthick 8"
"set grads off" ; "set ccolor "col.1 ; "set cstyle 1" ; "set cmark 0" ; "d tmp.1/1000000"
"set grads off" ; "set ccolor "col.2 ; "set cstyle 1" ; "set cmark 0" ; "d tmp.2/1000000"
"set grads off" ; "set ccolor "col.3 ; "set cstyle 1" ; "set cmark 0" ; "d tmp.3/1000000"

"set vpage off"
lab.1 = "NARR"
lab.2 = "WRF"
lab.3 = "WRF Calibrated"
"set strsiz 0.16"
"set string 1 l 5 0"
dx = 0.25 ; dy = 0.03 ; xm = 2.70 ; ym = 7.40
c = 1
while (c <= 3)
  xl = xm - dx ; xr = xm + dx ; yb = ym - dy ; yt = ym + dy
  "set line "col.c
  "draw recf "xl" "yb" "xr" "yt
  "draw string "xr+0.2" "ym" "lab.c
* xm = xm + 0.5
  ym = ym - 0.3
  c = c + 1
endwhile

"q gxinfo"
line3 = sublin(result,3)
line4 = sublin(result,4)
x1 = subwrd(line3,4)
x2 = subwrd(line3,6)
y1 = subwrd(line4,4)
y2 = subwrd(line4,6)
xmid = (x1 + x2) / 2.0
ymid = (y1 + y2) / 2.0
say x1" "x2" "y1" "y2" "xmid ;* 2 10.5 0.75 7.75 6.25

"set string 1 bc 5" ; "set strsiz 0.23"
"draw string "xmid" 7.9 Spinup River Flow to North Atlantic (Sv)"

say "gxprint "fild" png white x1100 y850"
    "gxprint "fild" png white x1100 y850"
"quit"
