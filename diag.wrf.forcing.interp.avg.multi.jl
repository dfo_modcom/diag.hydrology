#=
 = Read each WRF-Hydro forcing variable and construct various timeseries
 = (e.g., of specific humidity and its variance) for hourly, daily, monthly,
 = seasonal (mid-May through mid-August), and annual-average-daily timeseries
 = - RD Dec 2023.
 =#

using My, Printf, NetCDF

const RAWW             = 1                              # raw variable
const VARR             = 2                              # and its variance
const DNUM             = 2                              # number of variable types
const R2DD             = 1                              # relative humidity
const S2DD             = 2                              # wind speed
const T2DD             = 3                              # temperature
const Q2DD             = 4                              # specific humidity
const U2DD             = 5                              # wind component zonal
const V2DD             = 6                              # wind component meridional
const PSFC             = 7                              # surface pressure
const RAIN             = 8                              # precipitation (rain, snow, ice, convective, and stratiform)
const SWDN             = 9                              # radiation downward shortwave
const LWDN             = 10                             # radiation downward  longwave
const TOTL             = 11                             # count of valid data in timeseries
const VNUM             = 11                             # number of variables
const DELT             = 6                              # source data timestep (hours)
const DELD             = 24                             # daily       timestep (hours)
const SBEG             = 515                            # first month/day of the seasonal domain
const SEND             = 815                            #  last month/day of the seasonal domain

const DELMISS          = -8e9                           # generic missing value comparison
const GRDMISS          = -9e9                           # generic missing value on a grid
const MISS             = -9999.0                        # generic missing value

if (argc = length(ARGS)) != 4
  print("\nUsage: jjj $(basename(@__FILE__)) wrf 1990 2004 2004\n\n")
  exit(1)
end
fila = "forcing_" * ARGS[1] * "_" * ARGS[2] * "_" * ARGS[4] * ".byday.nc"     # construct file names,
filb = "forcing_" * ARGS[1] * "_" * ARGS[2] * "_" * ARGS[3] * ".daily.txt"    # full timeseries dates,
filc = "forcing_" * ARGS[1] * "_" * ARGS[2] * "_" * ARGS[3] * ".monthly.txt"  # and allocate data arrays
fild = "forcing_" * ARGS[1] * "_" * ARGS[2] * "_" * ARGS[3] * ".seasonal.txt"
file = "forcing_" * ARGS[1] * "_" * ARGS[2] * "_" * ARGS[3] * ".annual.txt"

hora = ARGS[2] * "-01-01-00" ; tima = My.datesous("1900-01-01-00", hora, "hr")
horb = ARGS[3] * "-12-31-18" ; timb = My.datesous("1900-01-01-00", horb, "hr")
hors = collect(tima:DELT:timb) ; nhor = length(hors)
dath = zeros(DNUM, VNUM, nhor)

daya = hora[1:10] * "-00" ; tima = My.datesous("1900-01-01-00", daya, "hr")   # daily dates and data
dayb = horb[1:10] * "-00" ; timb = My.datesous("1900-01-01-00", dayb, "hr")
days = collect(tima:DELD:timb) ; nday = length(days)
datd = zeros(DNUM, VNUM, nday)

mons = Array{Float64}(undef,0)                                                # monthly dates and data
mona = hora[1:7] * "-15-00" ; tima = My.datesous("1900-01-01-00", mona, "hr")
monb = horb[1:7] * "-15-00" ; timb = My.datesous("1900-01-01-00", monb, "hr")
while tima <= timb
  global tima, mona
  push!(mons, tima)
  mona = dateadd(mona, 30, "dy")
  mona = mona[1:7] * "-15-00"
  tima = My.datesous("1900-01-01-00", mona, "hr")
end
nmon = length(mons)
datm = zeros(DNUM, VNUM, nmon)

seas = Array{Float64}(undef,0)                                                # seasonal dates and data
seaa = hora[1:4] * "-06-30-00" ; tima = My.datesous("1900-01-01-00", seaa, "hr")
seab = horb[1:4] * "-06-30-00" ; timb = My.datesous("1900-01-01-00", seab, "hr")
while tima <= timb
  global tima, seaa
  push!(seas, tima)
  seaa = dateadd(seaa, 364, "dy")
  seaa = seaa[1:4] * "-06-30-00"
  tima = My.datesous("1900-01-01-00", seaa, "hr")
end
nsea = length(seas)
dats = zeros(DNUM, VNUM, nsea)

anna = hora[1:4] * "-01-01-00" ; tima = My.datesous("1900-01-01-00", anna, "hr")  # and annual-average-daily
annb = hora[1:4] * "-12-31-00" ; timb = My.datesous("1900-01-01-00", annb, "hr")
anns = collect(tima:DELD:timb) ; nann = length(anns)
data = zeros(DNUM, VNUM, nann)

vars = ["R2D", "S2D", "T2D", "Q2D", "U2D", "V2D", "PSFC", "RAINRATE", "SWDOWN", "LWDOWN", "TOTL"]
varz = [i[1:end-1] * "Z" for i in vars]
for a = 1:VNUM-1
  dath[RAWW,a,:] = ncread(fila, vars[a], start=[1,1,1], count=[-1,-1,nhor])[1,1,:]
end
any(x -> (x < DELMISS || x > -DELMISS), dath) && error("\nERROR : $fila contains missing data\n\n")

hnow = hora                                                                   # then construct each timeseries
for a = 1:nhor                                                                # (but skip Feb 29)
  global hnow
  if hnow[5:10] != "-02-29"
    dnow = My.datesous("1900-01-01-00",             hnow[1:10] *       "-00", "hr") ; dind = findfirst(isequal(dnow), days)
    mnow = My.datesous("1900-01-01-00",             hnow[1: 7] *    "-15-00", "hr") ; mind = findfirst(isequal(mnow), mons)
    snow = My.datesous("1900-01-01-00",             hnow[1: 4] * "-06-30-00", "hr") ; sind = findfirst(isequal(snow), seas)
    anow = My.datesous("1900-01-01-00", hora[1:4] * hnow[5:10] *       "-00", "hr") ; aind = findfirst(isequal(anow), anns)
    seain = SBEG <= 100 * parse(Int, hnow[6:7]) + parse(Int, hnow[9:10]) <= SEND ? true : false
    for b = 1:VNUM-1
                datd[RAWW,b,dind] += dath[RAWW,b,a] ; datd[VARR,b,dind] += dath[RAWW,b,a]^2
                datm[RAWW,b,mind] += dath[RAWW,b,a] ; datm[VARR,b,mind] += dath[RAWW,b,a]^2
      seain && (dats[RAWW,b,sind] += dath[RAWW,b,a] ; dats[VARR,b,sind] += dath[RAWW,b,a]^2)
                data[RAWW,b,aind] += dath[RAWW,b,a] ; data[VARR,b,aind] += dath[RAWW,b,a]^2
    end
              datd[RAWW,TOTL,dind] += 1 ; datd[VARR,TOTL,dind] += 1
              datm[RAWW,TOTL,mind] += 1 ; datm[VARR,TOTL,mind] += 1
    seain && (dats[RAWW,TOTL,sind] += 1 ; dats[VARR,TOTL,sind] += 1)
              data[RAWW,TOTL,aind] += 1 ; data[VARR,TOTL,aind] += 1
  end
  hnow = My.dateadd(hnow, DELT, "hr")
end

for z = 1:4                                                                   # and store the timeseries
  z == 1 && (znam = filb ; zzss = days ; zznn = nday ; datz = datd)
  z == 2 && (znam = filc ; zzss = mons ; zznn = nmon ; datz = datm)
  z == 3 && (znam = fild ; zzss = seas ; zznn = nsea ; datz = dats)
  z == 4 && (znam = file ; zzss = anns ; zznn = nann ; datz = data)

  fpa = My.ouvre(znam, "w", false)
  for a = 1:zznn
    line = @sprintf("%s", My.dateadd("1900-01-01-00", zzss[a], "hr"))
    if datz[RAWW,  TOTL  ,a]  < 2
       datz[RAWW,1:VNUM-1,a] .= MISS
       datz[VARR,1:VNUM-1,a] .= MISS
    else
      for b = 1:VNUM-1
        datz[VARR,b,a]  = (datz[VARR,b,a] - datz[RAWW,b,a]^2 / datz[RAWW,TOTL,a]) / (datz[RAWW,TOTL,a] - 1)
        datz[RAWW,b,a] /=                                                            datz[RAWW,TOTL,a]
      end
    end
    for b = 1:VNUM  line *= @sprintf(" %15.8f %15.8f", datz[RAWW,b,a], datz[VARR,b,a])  end
    line *= "\n"
    print(fpa, line)
  end
  close(fpa)

  znam = znam[1:end-4] * ".nc"
  nccreer(znam, zznn, 1, 1, MISS; vnames = [vars; varz])
  for a = 1:VNUM
    ncwrite(datz[RAWW,a,:], znam, vars[a], start=[1,1,1], count=[-1,-1,-1])
    ncwrite(datz[VARR,a,:], znam, varz[a], start=[1,1,1], count=[-1,-1,-1])
  end
  ncwrite(zzss, znam, "time", start=[1], count=[-1])
  ncputatt(     znam, "time", Dict("units" => "hours since 1900-01-01 00:00:0.0"))
end
exit(0)
