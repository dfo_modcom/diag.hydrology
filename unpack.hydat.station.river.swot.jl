#=
 = Loop through available text station files (.sta) and replace initially missing
 = locations with the nearest valid model and SWOT river locations, as given by
 = the corresponding model and SWOT grids - RD Jan 2023.
 =#

using My, Printf, NetCDF

const FATS             = 2150                           # number of  latitudes  in Fulldom_hires.nc
const FONS             = 2750                           # number of longitudes  in Fulldom_hires.nc
const DELMISS          = -8e9                           # generic missing value comparison
const GRDMISS          = -9e9                           # generic missing value on a grid
const MISS             = -9999.0                        # generic missing value
const MIST             = @sprintf("%d", Int64(MISS))    # generic missing value string

if (argc = length(ARGS)) != 2
  print("\nUsage: jjj $(basename(@__FILE__)) ../DOMAIN_full_full/ ../sword/\n\n")
  exit(1)
end
grdfil  = ARGS[1] * "Fulldom_hires.nc"
swofil  = ARGS[2] * "SWORD_v14_NA.nc"

fnet =  Set{Tuple{  Int32,   Int32}}()                                        # read the river network points
floc =  Set{Tuple{Float64, Float64}}()                                        # and their lat/lon locations
itol = Dict{Tuple{  Int32,   Int32}, Tuple{Float64, Float64}}()               # (with the lat index flipped)
ltoi = Dict{Tuple{Float64, Float64}, Tuple{  Int32,   Int32}}()
friv = ncread(grdfil, "CHANNELGRID", start=[1,1], count=[-1,-1])
flat = ncread(grdfil,    "LATITUDE", start=[1,1], count=[-1,-1])
flon = ncread(grdfil,   "LONGITUDE", start=[1,1], count=[-1,-1])
for a = 1:FATS, b = 1:FONS
  if friv[b,a] >= 0
    c = Int32(b) ; d = Int32(FATS+1-a) ; e = flon[b,a] ; f = flat[b,a]
    push!(fnet,   (c,d))
    push!(floc,   (e,f))
    itol[(c,d)] = (e,f)
    ltoi[(e,f)] = (c,d)
  end
end
@printf("\n%9d gridboxes define the river network\n", length(fnet))

files = readdir(".")                                                          # then loop through each .sta file
for file in files                                                             # and find the river gridbox that
  if isfile(file) && endswith(file, ".sta")                                   # is closest to each station, and
    fpa   = My.ouvre(file, "r")                                               # replace the dummy values
    lins  = readlines(fpa, keep = true) ; close(fpa)
    hlat  = parse(Float64, split(lins[6])[2])
    hlon  = parse(Float64, split(lins[7])[2])
    grdlat = grdlon = swolat = swolon =       MISS
    grdlai = grdloi = swolai = swoloi = Int64(MISS)

    dmin  = 8e8
    for (rlon, rlat) in floc
      delt = abs(hlat - rlat) + abs(hlon - rlon)
      delt < dmin && (dmin = delt ; grdlat = rlat ; grdlon = rlon)
    end
    grdlat != MISS && ((grdloi, grdlai) = ltoi[(grdlon, grdlat)])

    form  = "" ; for a = 1:19             form *= lins[a]  end
    form *= @sprintf("%-24s %f %d\n",  "LATITUDE_GRID", grdlat, grdlai)
    form *= @sprintf("%-24s %f %d\n", "LONGITUDE_GRID", grdlon, grdloi)
    form *= @sprintf("%-24s %f %d\n",  "LATITUDE_SWOT", swolat, swolai)
    form *= @sprintf("%-24s %f %d\n", "LONGITUDE_SWOT", swolon, swoloi)
                 for a = 24:length(lins)  form *= lins[a]  end
    fpa   = My.ouvre(file, "w")
    write(fpa, form) ; close(fpa)
  end
end
exit(0)

#=
hdrfil = ARGS[1]
sarfil = ARGS[1][1:end-3] * "sar.nc"

latp = ncread(sarfil, "lat",  start=[1],     count=[-1])                      # read the index lat,lon proxies
lonp = ncread(sarfil, "lon",  start=[1],     count=[-1])                      # and latitude and longitude
lats = ncread(sarfil, "lats", start=[1,1,1], count=[-1,-1,-1])
lons = ncread(sarfil, "lons", start=[1,1,1], count=[-1,-1,-1])
(nlon, nlat) = size(lons)
linregx = ones(nlat*nlon, 4)

c = 1                                                                         # linear calibration of latitude,
callat = Array{Float64}(undef, nlat*nlon)                                     # longitude with respect to index
callon = Array{Float64}(undef, nlat*nlon)                                     # (i.e., get lat/lon from index)
calmat = Array{Float64}(undef, nlat*nlon, 4)
for a = 1:nlat, b = 1:nlon
  global c
  if DELMISS < lats[b,a,1] < -DELMISS && DELMISS < lons[b,a,1] < -DELMISS
    calmat[c,1] = 1.0
    calmat[c,2] = a
    calmat[c,3] = b
    calmat[c,4] = a * b
    callat[c]   = lats[b,a,1]
    callon[c]   = lons[b,a,1]
    c += 1
  end
end
c -= 1
iilat = calmat[1:c,:] \ callat[1:c]
iilon = calmat[1:c,:] \ callon[1:c]

c = 1                                                                         # linear calibration of lat,lon proxies
callat = Array{Float64}(undef, nlat*nlon)                                     # with respect to latitude and longitude
callon = Array{Float64}(undef, nlat*nlon)                                     # (i.e., get proxy lat/lon from lat/lon)
calmat = Array{Float64}(undef, nlat*nlon, 4)
for a = 1:nlat, b = 1:nlon
  global c
  if DELMISS < lats[b,a,1] < -DELMISS && DELMISS < lons[b,a,1] < -DELMISS
    calmat[c,1] = 1.0
    calmat[c,2] = lats[b,a,1]
    calmat[c,3] = lons[b,a,1]
    calmat[c,4] = lats[b,a,1] * lons[b,a,1]
    callat[c]   = latp[a]
    callon[c]   = lonp[b]
    c += 1
  end
end
c -= 1
latll = calmat[1:c,:] \ callat[1:c]
lonll = calmat[1:c,:] \ callon[1:c]

gbox  = split(replace(GBOX, "," => " ")) ; gbox_llgrd = ""
for a = 1:div(length(gbox),2)
  global gbox_llgrd
  lon = parse(Float64, gbox[2*a-1])
  lat = parse(Float64, gbox[2*a])
  gbox_llgrd *= @sprintf(" %9.4f %8.4f,", lonll[1] + lat * lonll[2] + lon * lonll[3] + lat * lon * lonll[4],
                                          latll[1] + lat * latll[2] + lon * latll[3] + lat * lon * latll[4])
end
gbox_llgrd = gbox_llgrd[2:end-1]

nbox  = split(replace(NBOX, "," => " ")) ; nbox_llgrd = ""
for a = 1:div(length(nbox),2)
  global nbox_llgrd
  lon = parse(Float64, nbox[2*a-1])
  lat = parse(Float64, nbox[2*a])
  nbox_llgrd *= @sprintf(" %9.4f %8.4f,", lonll[1] + lat * lonll[2] + lon * lonll[3] + lat * lon * lonll[4],
                                          latll[1] + lat * latll[2] + lon * latll[3] + lat * lon * latll[4])
end
nbox_llgrd = nbox_llgrd[2:end-1]

sbox  = split(replace(SBOX, "," => " ")) ; sbox_llgrd = ""
for a = 1:div(length(sbox),2)
  global sbox_llgrd
  lon = parse(Float64, sbox[2*a-1])
  lat = parse(Float64, sbox[2*a])
  sbox_llgrd *= @sprintf(" %9.4f %8.4f,", lonll[1] + lat * lonll[2] + lon * lonll[3] + lat * lon * lonll[4],
                                          latll[1] + lat * latll[2] + lon * latll[3] + lat * lon * latll[4])
end
sbox_llgrd = sbox_llgrd[2:end-1]

fpa   = My.ouvre(hdrfil, "r")                                                 # read hdr, and save the first
lins  = readlines(fpa, keep = true) ; close(fpa)                              # 27 lines with added mappings
form  = "" ; for a = 1:27  global form ; form *= lins[a]  end
form *= @sprintf("lat_plane_ii2ll   %18.10f %18.10f %18.10f %18.10f\n", iilat[1], iilat[2], iilat[3], iilat[4])
form *= @sprintf("lon_plane_ii2ll   %18.10f %18.10f %18.10f %18.10f\n", iilon[1], iilon[2], iilon[3], iilon[4])
form *= @sprintf("lat_plane_ll2ll   %18.10f %18.10f %18.10f %18.10f\n", latll[1], latll[2], latll[3], latll[4])
form *= @sprintf("lon_plane_ll2ll   %18.10f %18.10f %18.10f %18.10f\n", lonll[1], lonll[2], lonll[3], lonll[4])
form *= @sprintf("gbox_llraw           %s\n", replace(GBOX,       "," => " "))
form *= @sprintf("gbox_llgrd           %s\n", replace(gbox_llgrd, "," => " "))
form *= @sprintf("nbox_llraw           %s\n", replace(NBOX,       "," => " "))
form *= @sprintf("nbox_llgrd           %s\n", replace(nbox_llgrd, "," => " "))
form *= @sprintf("sbox_llraw           %s\n", replace(SBOX,       "," => " "))
form *= @sprintf("sbox_llgrd           %s\n", replace(sbox_llgrd, "," => " "))
fpa   = My.ouvre(hdrfil, "w")
write(fpa, form) ; close(fpa)
=#

#=
const NBOX             = " -67.0000  50.0000,  -67.0000  49.0000,  -65.0000  49.0000,  -65.0000  50.0000,  -67.0000  50.0000"
const SBOX             = " -63.8829  48.6289,  -64.5341  47.3984,  -63.4314  47.3365,  -63.3142  48.4006,  -63.8829  48.6289"

for a = 1:nlat, b = 1:nlon
  lats[b,a,1] = iilat[1] + a * iilat[2] + b * iilat[3] + a * b * iilat[4]
  lons[b,a,1] = iilon[1] + a * iilon[2] + b * iilon[3] + a * b * iilon[4]
end
ncwrite(lats, sarfil, "mask", start=[1,1,1], count=[-1,-1,-1])
ncwrite(lons, sarfil, "land", start=[1,1,1], count=[-1,-1,-1])

for a = 1:nlat, b = 1:nlon
  lat = lats[b,a,1]
  lon = lons[b,a,1]
  lats[b,a,1] = latll[1] + lat * latll[2] + lon * latll[3] + lat * lon * latll[4]
  lons[b,a,1] = lonll[1] + lat * lonll[2] + lon * lonll[3] + lat * lon * lonll[4]
end
ncwrite(lats, sarfil, "mask", start=[1,1,1], count=[-1,-1,-1])
ncwrite(lons, sarfil, "land", start=[1,1,1], count=[-1,-1,-1])
=#
