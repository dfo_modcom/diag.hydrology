#=
 = Create plots of Hydat station location and HydroSHEDS rivers - RD Feb 2023.
 =#

using My, Printf
const ADYR             = 0                              # number of years to add before linking

if (argc = length(ARGS)) != 1
  print("\nUsage: jjj $(basename(@__FILE__)) 2\n\n")
  exit(1)
end
dell = ARGS[1]

function walkdir(dir)
  files = readdir(dir)
  for file in files
    if isfile(file) && endswith(file, ".sta")
      print("grads --quiet -blc \"diag.hydroshed.watershed.hydat $file $dell\"\n")
        run(`grads --quiet -blc  "diag.hydroshed.watershed.hydat $file $dell"`)
    end
  end
end

walkdir(".")
