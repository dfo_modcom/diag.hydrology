#=
 = Construct subdir links to station data files in the current directory.
 = Links in the first subdir are to stations that are considered natural
 = upstream, and links to the second subdir are to stations with regulated
 = upstream conditions - RD Jan 2024.
 =#

using My, Printf, NetCDF

const NPAR             = 4                              # number of WRF-Hydro streamflow estimates (WRF/ERA forcing, raw/neural-network)
const MISS             = -9999.0                        # generic missing value

if (argc = length(ARGS)) != 4
  print("\nUsage: jjj $(basename(@__FILE__)) hydat_valding_aall_1000_incl _1990-01-01_2004-12-30.nc hydat_upstream_natural hydat_upstream_regulat\n\n")
  exit(1)
end
fila =                 ARGS[1] *       ".txt" ; fpa = My.ouvre(fila, "r", false) ; lins = readlines(fpa, keep = true) ; close(fpa)
filb = ARGS[3] * "/" * ARGS[1] * "_upnat.txt"
filc = ARGS[4] * "/" * ARGS[1] * "_upreg.txt"
fild =                 ARGS[1] *            ARGS[2]
file = ARGS[3] * "/" * ARGS[1] * "_upnat" * ARGS[2]
filf = ARGS[4] * "/" * ARGS[1] * "_upreg" * ARGS[2]
dira = ARGS[3] ; !isfile(dira) && !isdir(dira) && mkdir(dira)
dirb = ARGS[4] ; !isfile(dirb) && !isdir(dirb) && mkdir(dirb)

fora = forb = ""                                                              # create a mask for natural vs regulated
nlet = length(lins)                                                           # stations and recreate the subdir texts
mask = falses(nlet)
for a = 1:nlet
  global fora, forb
  filz = split(lins[a])[1] * ".sta"
  fpz  = My.ouvre(filz, "r", false) ; linz = readlines(fpz, keep = true) ; close(fpz)
  if split(linz[93])[5] == "Natural"
    mask[a] = true
    fora *= lins[a]
  else
    forb *= lins[a]
  end
end
lnat = lins[  mask] ; nnat = length(lnat)
lreg = lins[.!mask] ; nreg = length(lreg)

@printf("\nwriting %3d   natural stations to %s\n", nnat, filb)
@printf(  "    and %3d regulated stations to %s\n", nreg, filc)
fpa = My.ouvre(filb, "w", false) ; write(fpa, fora) ; close(fpa)
fpa = My.ouvre(filc, "w", false) ; write(fpa, forb) ; close(fpa)

tais = [".nc", ".shp", ".sta", ".sta.hwshed", ".sta.hyshed", ".sta.png"]      # link subdir files to the current dir
for line in lnat
  stem = split(line)[1]
  for tail in tais
    full = ".." * "/" * stem * tail
    filz = dira * "/" * stem * tail
    !isfile(filz) && !islink(filz) && symlink(full, filz)
  end
end

for line in lreg
  stem = split(line)[1]
  for tail in tais
    full = ".." * "/" * stem * tail
    filz = dirb * "/" * stem * tail
    !isfile(filz) && !islink(filz) && symlink(full, filz)
  end
end

function nccreer(fn::AbstractString, tims::Array{Float64,1}, lats::Array{Float64,1}, lons::Array{Float64,1})
  nctim = NcDim("time", length(tims), atts = Dict{Any,Any}("units"=>"hours since 1900-01-01 00:00:0.0"), values = tims)
  nclat = NcDim( "lat", length(lats), atts = Dict{Any,Any}("units"=>"degrees_north"),                    values = lats)
  nclon = NcDim( "lon", length(lons), atts = Dict{Any,Any}("units"=> "degrees_east"),                    values = lons)
  ncvrs = Array{NetCDF.NcVar}(undef, 16)
  ncvrs[ 1] = NcVar("hydt", [       nclat, nctim], atts = Dict{Any,Any}("units"=>"m3/s", "missing_value"=>MISS), t=Float64, compress=-1)
  ncvrs[ 2] = NcVar("hydi", [       nclat, nctim], atts = Dict{Any,Any}("units"=>"none", "missing_value"=>MISS), t=Float64, compress=-1)
  ncvrs[ 3] = NcVar("flow", [nclon, nclat, nctim], atts = Dict{Any,Any}("units"=>"m3/s", "missing_value"=>MISS), t=Float64, compress=-1)
  ncvrs[ 4] = NcVar("para", [nclon,        nctim], atts = Dict{Any,Any}("units"=>"none", "missing_value"=>MISS), t=Float64, compress=-1)
  ncvrs[ 5] = NcVar("parb", [nclon,        nctim], atts = Dict{Any,Any}("units"=>"none", "missing_value"=>MISS), t=Float64, compress=-1)
  ncvrs[ 6] = NcVar("parc", [nclon,        nctim], atts = Dict{Any,Any}("units"=>"none", "missing_value"=>MISS), t=Float64, compress=-1)
  ncvrs[ 7] = NcVar("tmpa", [nclon, nclat       ], atts = Dict{Any,Any}("units"=>"none", "missing_value"=>MISS), t=Float64, compress=-1)
  ncvrs[ 8] = NcVar("tmpb", [nclon, nclat       ], atts = Dict{Any,Any}("units"=>"none", "missing_value"=>MISS), t=Float64, compress=-1)
  ncvrs[ 9] = NcVar("tmpc", [nclon, nclat       ], atts = Dict{Any,Any}("units"=>"none", "missing_value"=>MISS), t=Float64, compress=-1)
  ncvrs[10] = NcVar("tmpd", [nclon, nclat       ], atts = Dict{Any,Any}("units"=>"none", "missing_value"=>MISS), t=Float64, compress=-1)
  ncvrs[11] = NcVar("tmpe", [nclon, nclat       ], atts = Dict{Any,Any}("units"=>"none", "missing_value"=>MISS), t=Float64, compress=-1)
  ncvrs[12] = NcVar("tmpf", [nclon, nclat       ], atts = Dict{Any,Any}("units"=>"none", "missing_value"=>MISS), t=Float64, compress=-1)
  ncvrs[13] = NcVar("tmpg", [nclon, nclat       ], atts = Dict{Any,Any}("units"=>"none", "missing_value"=>MISS), t=Float64, compress=-1)
  ncvrs[14] = NcVar("tmph", [nclon, nclat       ], atts = Dict{Any,Any}("units"=>"none", "missing_value"=>MISS), t=Float64, compress=-1)
  ncvrs[15] = NcVar("tmpi", [nclon, nclat       ], atts = Dict{Any,Any}("units"=>"none", "missing_value"=>MISS), t=Float64, compress=-1)
  ncvrs[16] = NcVar("tmpj", [nclon, nclat       ], atts = Dict{Any,Any}("units"=>"none", "missing_value"=>MISS), t=Float64, compress=-1)
  ncfil = NetCDF.create(fn, ncvrs, gatts = Dict{Any,Any}("units"=>"none"), mode = NC_NETCDF4)
end

tims = ncread(fild, "time", start=[1],     count=[-1])                        # create the natural NetCDF template
hydt = ncread(fild, "hydt", start=[1,1],   count=[-1,-1])[mask,:]
hydi = ncread(fild, "hydi", start=[1,1],   count=[-1,-1])[mask,:]
flow = ncread(fild, "flow", start=[1,1,1], count=[-1,-1,-1])[:,mask,:]
para = ncread(fild, "para", start=[1,1],   count=[-1,-1])
npar, ntim = size(para)
lons = collect(1.0:npar)
lats = collect(1.0:nnat)
nccreer(      file, tims, lats, lons)
ncwrite(hydt, file, "hydt", start=[1,1],   count=[-1,-1])
ncwrite(hydi, file, "hydi", start=[1,1],   count=[-1,-1])
ncwrite(flow, file, "flow", start=[1,1,1], count=[-1,-1,-1])
ncwrite(para, file, "para", start=[1,1],   count=[-1,-1])

hydt = ncread(fild, "hydt", start=[1,1],   count=[-1,-1])[.!mask,:]           # create the regulated NetCDF template
hydi = ncread(fild, "hydi", start=[1,1],   count=[-1,-1])[.!mask,:]
flow = ncread(fild, "flow", start=[1,1,1], count=[-1,-1,-1])[:,.!mask,:]
lats = collect(1.0:nreg)
nccreer(      filf, tims, lats, lons)
ncwrite(hydt, filf, "hydt", start=[1,1],   count=[-1,-1])
ncwrite(hydi, filf, "hydi", start=[1,1],   count=[-1,-1])
ncwrite(flow, filf, "flow", start=[1,1,1], count=[-1,-1,-1])
ncwrite(para, filf, "para", start=[1,1],   count=[-1,-1])
exit(0)

#=
fora = forb = ""                                                              # create a mask for natural vs regulated
nlet = length(lins)                                                           # stations and recreate the subdir texts
mask = trues(nlet)
for a = 1:nlet
  global fora, forb
  filz = split(lins[a])[1]
  tmp = "02RF001" ; contains(filz, tmp) && (mask[a] = false)
  tmp = "02BE002" ; contains(filz, tmp) && (mask[a] = false)
  tmp = "02HK010" ; contains(filz, tmp) && (mask[a] = false)
  tmp = "02CA001" ; contains(filz, tmp) && (mask[a] = false)
  tmp = "02HA003" ; contains(filz, tmp) && (mask[a] = false)
  tmp = "02OA024" ; contains(filz, tmp) && (mask[a] = false)
  if mask[a] == true
    fora *= lins[a]
  else
    forb *= lins[a]
  end
end
lnat = lins[  mask] ; nnat = length(lnat)
lreg = lins[.!mask] ; nreg = length(lreg)
=#
