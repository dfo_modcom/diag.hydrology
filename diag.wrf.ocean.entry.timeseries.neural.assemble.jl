#=
 = Apply a previously trained neural network to post-process streamflow
 = at all HYDAT stations.  Each neural network prediction employs a
 = normalized (2*LAGS+1) timeseries at each station - RD Jan 2024.
 =#

using My, Printf, NetCDF, Statistics, Flux, JLD2

const LAGS             = 48                             # number of 3-h streamflow values before and after
const FEAT             = 2                              # number of features (neural network inputs)
const SMAL             = 0.001                          # generic small streamflow value
const MISS             = -9999.0                        # generic missing value

if (argc = length(ARGS)) != 2
  print("\nUsage: jjj $(basename(@__FILE__)) hydat_valding_aall_1000_incl_1990-01-01_2004-12-30.nc pest_nwm_DOMAIN_East.FORCING.era_2019-03-31_000214.remsvd.prep.nnet.elu.jld\n\n")
  exit(1)
end
fila = ARGS[1]
filb = ARGS[2]
filc = ARGS[1][1:end-3] * ".neural.nc"
@printf("\ncopying %s %s\n", fila, filc) ; cp(fila, filc; force = true)

flow = ncread(filc, "flow", start=[1,1,1], count=[-1,-1,-1])                  # read the streamflow estimates
npar, nsta, ntim = size(flow)
@printf("with %d times %d hydat stations and %d flow types\n\n", ntim, nsta, npar)

mask = falses(npar, nsta, ntim)                                               # (define a missing mask just in case)
for a = 1:ntim, b = 1:nsta, c = 1:npar
  flow[c,b,a] > MISS && (mask[c,b,a] = true)
end

function flonorm(x::Array{Float64}, alp::Float64, bet::Float64)               # and normalize streamflow by its mean
  if alp == MISS || bet == MISS                                               # and std (or else by input mean, std)
    alp = mean(x) ; bet = std(x, mean = alp, corrected = false)
  end
  y = (x .- alp) ./ (bet + eps())
  return y, alp, bet
end

alpa = Array{Float64}(undef,npar,nsta)
beta = Array{Float64}(undef,npar,nsta)
for a = 1:nsta, b = 1:2:npar
  flow[b,a,mask[b,a,:]], alpa[b,a], beta[b,a] = flonorm(flow[b,a,mask[b,a,:]], MISS, MISS)
end

acti =     split(ARGS[2], ".")[end-1]                                         # then load the neural network
pars = JLD2.load(ARGS[2], "model_state");
acti == "identity" && (model = Chain(Dense(FEAT => FEAT+1),                               BatchNorm(3), Dense(FEAT+1 => FEAT)))
acti != "identity" && (model = Chain(Dense(FEAT => FEAT+1, getfield(Main, Symbol(acti))), BatchNorm(3), Dense(FEAT+1 => FEAT, getfield(Main, Symbol(acti)))))
Flux.loadmodel!(model, pars)

@printf("writing predicted flows to %s\n", filc)                              # write the rescaled neural network estimates
flon = ones(npar, nsta, ntim) * MISS                                          # for each station timeseries, where all
for a = 1+LAGS:ntim-LAGS, b = 1:nsta, c = 1:2:npar                            # input values are valid
  if all(mask[c,b,a-LAGS:a+LAGS])
    xref          = Float32(flow[c,b,a])
    xdev          = Float32(flow[c,b,a] - mean(flow[c,b,a-LAGS:a+LAGS]))
    flon[c  ,b,a] =         flow[c,b,a]                         * beta[c,b] .+ alpa[c,b]
    flon[c+1,b,a] = Float64(model(reshape([xref,xdev],2,1))[1]) * beta[c,b] .+ alpa[c,b]
    flon[c+1,b,a] < 0 && (flon[c+1,b,a] = SMAL)
  end
end

ncwrite(flon, filc, "flow", start=[1,1,1], count=[-1,-1,-1])
exit(0)
