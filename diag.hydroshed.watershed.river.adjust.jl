#=
 = Reset the native WRF-Hydro DEM to a minimum value along rivers and a maximum
 = value along the watershed boundaries, where values are taken from a subdomain
 = of the original MERIT lat-lon DEM.  Rivers and watersheds are taken from the
 = HydroSHEDS 15-s reference - RD April 2023.
 =#

using My, Printf, NetCDF, Shapefile #, LibGEOS

const FATS             = 2150                           # number of  latitudes  in Fulldom_hires.nc
const FONS             = 2750                           # number of longitudes  in Fulldom_hires.nc
const FDIS             = 2.0                            # nominal distance between Fulldom_hires.nc gridpoints

const MATS             =  6600                          # number of latitudes   in MERIT grid
const MONS             = 21600                          # number of longitudes  in MERIT grid
const DELL             =    0.0083333333333333333       # lat/lon grid spacing
const LATA             =   30.0041666666666666667       # first latitude on grid
const LONA             = -179.9958333333333333333       # first longitude on grid

const DELR             = 3                              # number of nearby MERIT gridboxes to look for a  lower              river elevation
const DELW             = 5                              # number of nearby MERIT gridboxes to look for a higher watershed boundary elevation

const D2R              = 3.141592654 / 180.0            # degrees to radians conversion
const DELMISS          = -8e9                           # generic missing value comparison
const GRDMISS          = -9e9                           # generic missing value on a grid
const MISS             = -9999.0                        # generic missing value
const MIST             = @sprintf("%d", Int64(MISS))    # generic missing value string

if (argc = length(ARGS)) != 4
# print("\nUsage: jjj $(basename(@__FILE__)) ../HYDROSHEDS/groups.shp ../HYDROSHEDS/ ../merit/merit_hydrodem_30_85N_180_00W_30sec_elevtn.nc Fulldom_hires_04.nc\n\n")
  print("\nUsage: jjj $(basename(@__FILE__)) ../hydat/hydat_03JB002.sta.hyshed ../HYDROSHEDS/HydroRIVERS_v10_70014971.shp ../merit/merit_hydrodem_30_85N_180_00W_30sec_elevtn.nc Fulldom_hires.nc\n\n")
  exit(1)
end
fila = ARGS[1] ;   fpa = My.ouvre(fila, "r")   ; lina = readlines(fpa) ; close(fpa)
filb = ARGS[2] ; rvtab = Shapefile.Table(filb) ; rvlen = length(rvtab) ; rvgeo = Shapefile.shapes(rvtab)
filc = ARGS[3]
fild = ARGS[4]
file = ARGS[4][1:end-3] * "_walls.nc" ; cp(fild, file; force=true)

function subroute()
  elev = ncread(file, "TOPOGRAPHY", start=[1,1], count=[-1,-1])               # read WRF-Hydro grids
  lats = ncread(file,   "LATITUDE", start=[1,1], count=[-1,-1])               # (with latitude flipped)
  lons = ncread(file,  "LONGITUDE", start=[1,1], count=[-1,-1])
  (nlon, nlat) = size(lons)

  function telescope(rlat::Float64, rlon::Float64)                            # telescope through the WRF-Hydro grid
    mindis = minlat = minlon = 9e9                                            # to get a location closest to a river
    minlin = div(FATS, 2)
    minpix = div(FONS, 2)
    for a = 25:-1:0
      for b = minlin - 2^a : 2^a : minlin + 2^a
        if b >= 0 && b < FATS
          for c = minpix - 2^a : 2^a : minpix + 2^a
            if c >= 0 && c < FONS
              gdlatitude  = lats[c+1,b+1]
              gdlongitude = lons[c+1,b+1]
              tmpdis = (rlat - gdlatitude)^2 + (rlon - gdlongitude)^2
              if tmpdis < mindis
                mindis = tmpdis
                minlat = gdlatitude
                minlon = gdlongitude
                minlin = b
                minpix = c
#               @printf("a b c are (%8d %8d %8d) and lat lon mindis are %lf %lf %e ***\n", a, b, c, gdlatitude, gdlongitude, mindis)
#             else
#               @printf("a b c are (%8d %8d %8d) and lat lon tmpdis are %lf %lf %e\n", a, b, c, gdlatitude, gdlongitude, tmpdis)
              end
            end
          end
        end
      end
    end
    if 111 * mindis^0.5 > 10 * FDIS
      print("\nrlat rlon are $rlat $rlon\n")
      print("minlat minlon are $minlat $minlon at line pixel $minlin $minpix\n")
      print("allowable distance between them is about $(10 * FDIS) km\n")
      print("actual    distance between them is about $(111 * mindis^0.5) km\n")
      exit(0)
    end
    (minpix, minlin)
  end

  friv = Set{Tuple{Int32, Int32}}()                                           # identify all WRF-Hydro gridboxes that are
  for b = 1:rvlen                                                             # collocated with HydroSHEDS river points
    rvpts = rvgeo[b].points
    for c = 1:length(rvpts)
      push!(friv, telescope(rvpts[c].y, rvpts[c].x))
    end
  end
  @printf("\n%9d gridboxes define the HydroSHEDS river network on the WRF-Hydro grid\n", length(friv))

  fshd = Set{Tuple{Int32, Int32}}()                                           # identify all WRF-Hydro gridboxes that are
  for b = 2:length(lina)                                                      # collocated with HydroSHEDS river points
    hlat = parse(Float64, split(lina[b])[1])
    hlon = parse(Float64, split(lina[b])[2])
    push!(fshd, telescope(hlat, hlon))
  end
  @printf("\n%9d gridboxes define the HydroSHEDS river network on the WRF-Hydro grid\n", length(fshd))

  glat = ncread(filc,   "lat", start=[1],   count=[-1])
  glon = ncread(filc,   "lon", start=[1],   count=[-1])
  glev = ncread(filc, "Band1", start=[1,1], count=[MONS,MATS])

  for (b, a) in friv                                                          # then get the corresponding MERIT grid
    latmin, latind = findmin(abs.(lats[b+1,a+1] .- glat))                     # central locations and search within a
    lonmin, lonind = findmin(abs.(lons[b+1,a+1] .- glon))                     # radius for a lower river elevation to
    for c = latind-DELR:latind+DELR                                           # replace the existing WRF-Hydro value
      if c > 0 && b <= MATS
        for d = lonind-DELR:lonind+DELR
          if d > 0 && d <= MONS
            glev[d,c] < elev[b+1,a+1] && (elev[b+1,a+1] = glev[d,c])
          end
        end
      end
    end
  end

  for (b, a) in fshd                                                          # then get the corresponding MERIT grid
    if elev[b+1,a+1] > 0
      latmin, latind = findmin(abs.(lats[b+1,a+1] .- glat))                   # central locations and search within a
      lonmin, lonind = findmin(abs.(lons[b+1,a+1] .- glon))                   # radius for a lower river elevation to
      for c = latind-DELW:latind+DELW                                         # replace the existing WRF-Hydro value
        if c > 0 && b <= MATS
          for d = lonind-DELW:lonind+DELW
            if d > 0 && d <= MONS
              glev[d,c] > elev[b+1,a+1] && (elev[b+1,a+1] = glev[d,c])
            end
          end
        end
      end
    end
  end
  ncwrite(elev, file, "TOPOGRAPHY", start=[1,1], count=[-1,-1])
end

subroute()
exit(0)

#=
==> ../hydat/hydat_03JB002.sta.hyshed <==
 56.81458  59.21458  -75.84375  -70.17708 4672
 59.21458  -71.58125
 59.21458  -71.58542
 59.21042  -71.58958
 59.20625  -71.59375
 59.20208  -71.59792
 59.19792  -71.59792
 59.19375  -71.59792
 59.18958  -71.60208
 59.18958  -71.60625

nafil = ARGS[1] * "HydroRIVERS_v10_na.shp" ; natab = Shapefile.Table(nafil) ; nalen = length(natab) ; nageo = Shapefile.shapes(natab)
arfil = ARGS[1] * "HydroRIVERS_v10_ar.shp" ; artab = Shapefile.Table(arfil) ; arlen = length(artab) ; argeo = Shapefile.shapes(artab)
grfil = ARGS[1] * "HydroRIVERS_v10_gr.shp" ; grtab = Shapefile.Table(grfil) ; grlen = length(grtab) ; grgeo = Shapefile.shapes(grtab)
fmis  = "hydat_zzzzzzz.mis"

function findreach()                                                          # locate a HydroSHEDS river reach
  fpa   = My.ouvre(fila, "r")                                                 # endpoint that is near a station and
  lins  = readlines(fpa, keep = true) ; close(fpa)                            # is similar in upstream area (but if
  hlat  = parse(Float64, split(lins[6])[2])                                   # there is no upstream area value,
  hlon  = parse(Float64, split(lins[7])[2])                                   # then ignore this limit)
  if split(lins[8])[2] != "missing"
    hare = parse(Float64, split(lins[8])[2]) ; hdel = hare * FRAC
  else
    hare = 0.0 ; hdel = 9e9
  end

  clos = Array{Tuple{Float64, Int8, Int64, Int16, Float64, Float64}}(undef, 0)
  for a = 1:3
    a == 1 && (ddtab = natab ; ddlen = nalen ; ddgeo = nageo)               # first compile the HydroSHEDS endpoints
    a == 2 && (ddtab = artab ; ddlen = arlen ; ddgeo = argeo)               # inside the station search distance
    a == 3 && (ddtab = grtab ; ddlen = grlen ; ddgeo = grgeo)
    for b = 1:ddlen
      ddpts = ddgeo[b].points
      for c = 1:length(ddpts)
        (ddlat, ddlon) = (ddpts[c].y, ddpts[c].x)
        tmpdis = abs(ddlat - hlat) + abs(ddlon - hlon) * cos(hlat * D2R)
        tmpdis < RDIS && push!(clos, (tmpdis, a, b, c, ddtab.MAIN_RIV[b], ddtab.UPLAND_SKM[b]))
      end
    end
  end

  if length(clos) == 0
    print("no match for $fila\n")
    form = "mv $(fila[1:end-4]) hydat_noriver\n"
    fpa = My.ouvre(fmis, "a") ; write(fpa, form) ; close(fpa)
  else                                                                      # and if FRAC is not too constraining,
    print("   match for $fila is $(length(clos))\n")                        # select from among HydroSHEDS endpoints
    mindisa = 9e9 ; mininda = 0                                             # within FRAC of upstream catchment area
    mindisb = 9e9 ; minindb = 0                                             # (so mininda != 0 but minindb can be 0)
    for a = 1:length(clos)
      clos[a][1] < mindisa                                           && (mindisa = clos[a][1] ; mininda = a)
      clos[a][1] < mindisb && hare - hdel < clos[a][6] < hare + hdel && (mindisb = clos[a][1] ; minindb = a)
    end
    minind = minindb == 0 ? mininda : minindb
    mina = clos[minind][2] ; minb = clos[minind][3] ; minc = clos[minind][4]
    mina == 1 && (ddtab = natab ; ddlen = nalen ; ddgeo = nageo)
    mina == 2 && (ddtab = artab ; ddlen = arlen ; ddgeo = argeo)
    mina == 3 && (ddtab = grtab ; ddlen = grlen ; ddgeo = grgeo)            # then save the match and station data

    form  = "" ; for a = 1:20  form *= lins[a]  end
    form *= @sprintf("%-24s %17.14f %d\n",            "LATITUDE", ddgeo[minb].points[minc].y, Int64(MISS))
    form *= @sprintf("%-24s %17.14f %d\n",           "LONGITUDE", ddgeo[minb].points[minc].x, Int64(MISS))
    form *= @sprintf("%-24s %d\n",                    "HYRIV_ID", ddtab.HYRIV_ID[minb])
    form *= @sprintf("%-24s %d\n",                   "NEXT_DOWN", ddtab.NEXT_DOWN[minb])
    form *= @sprintf("%-24s %d\n",                    "MAIN_RIV", ddtab.MAIN_RIV[minb])
    form *= @sprintf("%-24s %f\n",                   "LENGTH_KM", ddtab.LENGTH_KM[minb])
    form *= @sprintf("%-24s %f\n",                  "DIST_DN_KM", ddtab.DIST_DN_KM[minb])
    form *= @sprintf("%-24s %f\n",                  "DIST_UP_KM", ddtab.DIST_UP_KM[minb])
    form *= @sprintf("%-24s %f\n",                   "CATCH_SKM", ddtab.CATCH_SKM[minb])
    form *= @sprintf("%-24s %f\n",                  "UPLAND_SKM", ddtab.UPLAND_SKM[minb])
    form *= @sprintf("%-24s %d\n",                   "ENDORHEIC", ddtab.ENDORHEIC[minb])
    form *= @sprintf("%-24s %f\n",                  "DIS_AV_CMS", ddtab.DIS_AV_CMS[minb])
    form *= @sprintf("%-24s %d\n",                    "ORD_STRA", ddtab.ORD_STRA[minb])
    form *= @sprintf("%-24s %d\n",                    "ORD_CLAS", ddtab.ORD_CLAS[minb])
    form *= @sprintf("%-24s %d\n",                    "ORD_FLOW", ddtab.ORD_FLOW[minb])
    form *= @sprintf("%-24s %d\n",                   "HYBAS_L12", ddtab.HYBAS_L12[minb])
    form *= @sprintf("%-24s %s as of %s\n", "HydroSHEDS_VERSION", "1.0", "2019-10-01")
    for a = 38:length(lins)  form *= lins[a]  end
    fpa = My.ouvre(fila, "w") ; write(fpa, form) ; close(fpa)
  end
end
=#
