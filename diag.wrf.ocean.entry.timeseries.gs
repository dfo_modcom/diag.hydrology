* This script is designed to plot a simple timeseries.
* It can be executed using a command like
*
*     grads -blc "diag.wrf.ocean.entry.timeseries streamflow 1933060100 00.CHRTOUT_GRID1 1601 231 535"
*
* - RD November 2012

function plot(args)
fila = subwrd(args,1)"."subwrd(args,2)""subwrd(args,3)"."subwrd(args,4)"."subwrd(args,5)"."subwrd(args,6)".nc"
filb = subwrd(args,1)"."subwrd(args,2)""subwrd(args,3)"."subwrd(args,4)"."subwrd(args,5)"."subwrd(args,6)".png"

XX = subwrd(args,6)
if (XX="1240") ; lab = "ChurchillRiver"; vmax =  3000 ; endif
if (XX="1934") ; lab = "AuxFeuilles"   ; vmax =  9000 ; endif
if (XX="1940") ; lab = "AuxFeuilles"   ; vmax =  9000 ; endif
if (XX="1998") ; lab = "Caniapiscau"   ; vmax = 40000 ; endif
if (XX="2019") ; lab = "ALaBeleine"    ; vmax =  9000 ; endif
if (XX="2058") ; lab = "George"        ; vmax = 12000 ; endif
if (XX="2218") ; lab = "Moisie"        ; vmax =  4500 ; endif
if (XX="2292") ; lab = "Romaine"       ; vmax =  3000 ; endif
if (XX="2363") ; lab = "Natashquan"    ; vmax =  3500 ; endif
if (XX="2428") ; lab = "PetitMecatina" ; vmax =  3500 ; endif
if (XX="2107") ; lab = "StLawrence"    ; vmax = 60000 ; endif
if (XX="2263") ; lab = "Bonaventure"   ; vmax =   900 ; endif
if (XX="2238") ; lab = "Restigouche"   ; vmax =  3000 ; endif
if (XX="2305") ; lab = "Miramachi"     ; vmax =  2500 ; endif
if (XX="2315") ; lab = "StJohn"        ; vmax = 12000 ; endif
if (XX="2226") ; lab = "Penobscot"     ; vmax =  3500 ; endif
if (XX="2201") ; lab = "Kennebec"      ; vmax =  4500 ; endif
if (XX="2175") ; lab = "Merrimack"     ; vmax =  3000 ; endif
filb = subwrd(args,1)"."subwrd(args,2)""subwrd(args,3)"."subwrd(args,4)"."lab".png"

"sdfopen "fila
"sdfopen "
*"set t 1 14975"
"set grads off"
"set grid off"
"set vrange 0 "vmax
"set xlopts 1 3 0.18"
"set ylopts 1 3 0.18"
*"d smth9(smth9(smth9(tmp)))"
*"d tmp/1000"
if (subwrd(args,1) = "streamflow")
  "q file" ; ret = sublin(result,5) ; tims = subwrd(ret,12) ; "set t 1 "tims+1
  "d tmp"
*else
"close 1"
"sdfopen obsflow.200701010000.CHRTOUT_GRID1.2192.1031.1240.nc"
  "set t 9131 11324"
  "set ccolor 2"
  "d flow"
endif

"q gxinfo"
line3 = sublin(result,3)
line4 = sublin(result,4)
x1 = subwrd(line3,4)
x2 = subwrd(line3,6)
y1 = subwrd(line4,4)
y2 = subwrd(line4,6)
xmid = (x1 + x2) / 2.0
ymid = (y1 + y2) / 2.0
say x1" "x2" "y1" "y2" "xmid ;* 2 10.5 0.75 7.75 6.25

"set string 1 bc 5" ; "set strsiz 0.23"
*"draw string "xmid" "y2+0.2" WRF-Hydro at "lab" (mSv)"
if (subwrd(args,1) = "streamflow")
  "draw string "xmid" "y2+0.2" WRF-Hydro at "lab" (m`a3`ns)"
*else
  "set string 2"
  "draw string "xmid" "y2-0.5" Observation at 06FD001"
* "draw string "xmid" "y2+0.2" Observation at "lab" (m`a3`ns)"
endif

say "gxprint "filb" png white x1100 y850"
    "gxprint "filb" png white x1100 y850"
"quit"
