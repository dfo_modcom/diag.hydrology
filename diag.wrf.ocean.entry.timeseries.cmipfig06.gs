* This script is designed to plot daily/monthly and annual-mean (daily) streamflow
* It can be executed using a command like
*
*     grads -bpc "diag.wrf.ocean.entry.timeseries.cmipfig06 hydat_valding_aall_1000_ _1990-01-01_2022-12-30 neural neuall.identity"
*
* - RD Jan 2024.

function plot(args)
stema = subwrd(args,1)
stemb = subwrd(args,2)
stemc = subwrd(args,3)
stemd = subwrd(args,4)

"sdfopen "stema"iccs"stemb"."stemc".daily.nc"     ; "q file 1" ; ret = sublin(result,5) ; tima = subwrd(ret,12) / 6
"sdfopen "stema"iccs"stemb"."stemc".monthly.nc"   ; "q file 2" ; ret = sublin(result,5) ; timb = subwrd(ret,12) / 6
"sdfopen "stema"iccs"stemb"."stemc".annualday.nc" ; "q file 3" ; ret = sublin(result,5) ; timc = subwrd(ret,12)
"sdfopen "stema"iccs"stemb"."stemc"."stemd".daily.nc"          ; lats = subwrd(ret, 6)
"sdfopen "stema"iccs"stemb"."stemc"."stemd".monthly.nc"        ; lons = subwrd(ret, 3)
"sdfopen "stema"iccs"stemb"."stemc"."stemd".annualday.nc"
"sdfopen "stema"ih45"stemb"."stemc".daily.nc"
"sdfopen "stema"ih45"stemb"."stemc".monthly.nc"
"sdfopen "stema"ih45"stemb"."stemc".annualday.nc"
"sdfopen "stema"ih45"stemb"."stemc"."stemd".daily.nc"
"sdfopen "stema"ih45"stemb"."stemc"."stemd".monthly.nc"
"sdfopen "stema"ih45"stemb"."stemc"."stemd".annualday.nc"
"sdfopen "stema"ih85"stemb"."stemc".daily.nc"
"sdfopen "stema"ih85"stemb"."stemc".monthly.nc"
"sdfopen "stema"ih85"stemb"."stemc".annualday.nc"
"sdfopen "stema"ih85"stemb"."stemc"."stemd".daily.nc"
"sdfopen "stema"ih85"stemb"."stemc"."stemd".monthly.nc"
"sdfopen "stema"ih85"stemb"."stemc"."stemd".annualday.nc"
"sdfopen "stema"impi"stemb"."stemc".daily.nc"
"sdfopen "stema"impi"stemb"."stemc".monthly.nc"
"sdfopen "stema"impi"stemb"."stemc".annualday.nc"
"sdfopen "stema"impi"stemb"."stemc"."stemd".daily.nc"
"sdfopen "stema"impi"stemb"."stemc"."stemd".monthly.nc"
"sdfopen "stema"impi"stemb"."stemc"."stemd".annualday.nc"

fpz = "xyzzy.forgetit."subwrd(args,1) ; "!echo $HOME > "fpz ; line = read(fpz) ; _home = sublin(line,2) ; ret = close(fpz) ; "!rm "fpz
"sdfopen "_home"/work/workg/HYDROSHEDS/hgt.sfc.nc"

c = 1
lista = stema"iccs"stemb".txt"
filestat = read(lista)
while (sublin(filestat,1) = 0 & sublin(filestat,2) != "")
  if (c < 10) ; tail.c = "0"c ; else ; tail.c = c ; endif
  line = sublin(filestat,2) ; nam.c = subwrd(line,1)
  if (substr(nam.1,1,1) = "h") ; aa = 8 ; bb = 7 ; else ; aa = 9 ; bb = 8 ; endif
  a = 1 ; while (substr(line,a,1) != "[") ; a = a + 1 ; endwhile ; a = a + 1 + 8
  b = a ; while (substr(line,b,1) != "]") ; b = b + 1 ; endwhile ; b = b - a
  riv.c =        substr(line,a,b) ; stn.c = substr(line,a-aa,bb)   ; c = c + 1
  filestat = read(lista)
endwhile
filestat = close(lista) ; riv.c = "All Stations"
if (substr(nam.1,1,1) = "h") ; nam.c = "hydat_9999999" ; else ; nam.c = "wrfh_99999999" ; endif
if (c < 10) ; tail.c = "0"c ; else ; tail.c = c ; endif
*if (c =  2) ; c = 1 ; endif

lef = 1.0 ; rig = 7.5 ; cen = (lef + rig) / 2
ypic = 3 ; string = "0.5 10.5 3.0 "ypic ; inner_decomp(string)
a = 1 ; while (a <= ypic) ; b = ypic - a + 1 ; low.b = _retlef.a ; mid.b = _retmid.a ; hig.b = _retrig.a ; a = a + 1 ; endwhile
"set lwid 20 14"
"set lwid 22 14"
"set rgb 55   0 200 200 100"
"set rgb 66   0 255   0 100"
"set rgb 88 240 130  40 100"
"set rgb 99 160   0 200 100"

z =  79 ; while (z < c)
* 79 157
* z = 58 ; while (z < 59)
* z = 86 ; while (z < 87)
* if (z = 19) ; z = 20 ; endif
* if (z = 36) ; z = 37 ; endif
  "clear" ; "set datawarn off"
* if (z < c)
    "set dfile 1" ; tims = tima ; "set parea "lef" "rig" "low.1" "hig.1
    "set gxout stat" ; max = 0
    "set t 1 "tims ; "set y "z ; "set x 1" ; "d hydt" ; ret = sublin(result,8) ; val = subwrd(ret,5) ; if (val > max) ; max = val ; endif
    "set t 1 "tims ; "set y "z ; "set x 1" ; "d flow" ; ret = sublin(result,8) ; val = subwrd(ret,5) ; if (val > max) ; max = val ; endif
    "set t 1 "tims ; "set y "z ; "set x 2" ; "d flow" ; ret = sublin(result,8) ; val = subwrd(ret,5) ; if (val > max) ; max = val ; endif
    "set t 1 "tims ; "set y "z ; "set x 3" ; "d flow" ; ret = sublin(result,8) ; val = subwrd(ret,5) ; if (val > max) ; max = val ; endif
    "set t 1 "tims ; "set y "z ; "set x 4" ; "d flow" ; ret = sublin(result,8) ; val = subwrd(ret,5) ; if (val > max) ; max = val ; endif
    max = max / 1  ;  vran =20000 ; vint = 4000
    if (max <10000) ; vran =10000 ; vint = 2000 ; endif
    if (max < 5000) ; vran = 5000 ; vint = 1000 ; endif
    if (max < 4000) ; vran = 4000 ; vint =  800 ; endif
    if (max < 3000) ; vran = 3000 ; vint =  500 ; endif
    if (max < 2000) ; vran = 2000 ; vint =  400 ; endif
    if (max < 1000) ; vran = 1000 ; vint =  200 ; endif
    if (max <  500) ; vran =  500 ; vint =  100 ; endif
    if (max <  400) ; vran =  400 ; vint =   80 ; endif
    if (max <  300) ; vran =  300 ; vint =   50 ; endif
    if (max <  200) ; vran =  200 ; vint =   40 ; endif
    if (max <  100) ; vran =  100 ; vint =   20 ; endif
    if (max <   50) ; vran =   50 ; vint =   10 ; endif
    if (max <   30) ; vran =   30 ; vint =    5 ; endif
    if (max <   10) ; vran =   10 ; vint =    2 ; endif
    a = 1 ; while (a < 5) ; "set t 1" ; "set y "z ; "set x "a
      "d tmpa" ; ret = sublin(result,8) ; val = subwrd(ret,5) ; nse = math_format("%5.2f",val) ; "d tmpb" ; ret = sublin(result,8) ; val = subwrd(ret,5) ; cor = math_format("%5.2f",val)
      "d tmpd" ; ret = sublin(result,8) ; val = subwrd(ret,5) ; rms = math_format("%4.0f",val) ; "d tmpe" ; ret = sublin(result,8) ; val = subwrd(ret,5) ; bia = math_format("%4.0f",val)
      line.a = nse" "cor" "rms" "bia
    a = a + 1 ; endwhile
    "set t 1 "tims ; "set gxout line" ; "set mproj off" ; "set grid off" ; "set mpdraw off" ; "set xlab on" ; "set ylab on"
    "set vrange 0 "vran ; "set ylint "vint ; "set cthick 12" ; "set line 1 1 12" ; "set string 1 c 8" ; "set missconn off"
    "set ccolor  1" ; "set cstyle 1" ; "set cmark 0" ; "set grads off" ; "set y "z ; "set x 1" ; "d hydt"
    "set ccolor  7" ; "set cstyle 1" ; "set cmark 0" ; "set grads off" ; "set y "z ; "set x 1" ; "d flow"
    "set ccolor  8" ; "set cstyle 1" ; "set cmark 0" ; "set grads off" ; "set y "z ; "set x 2" ; "d flow"
    "set ccolor  9" ; "set cstyle 1" ; "set cmark 0" ; "set grads off" ; "set y "z ; "set x 4" ; "d flow.4"
    "set ccolor  7" ; "set cstyle 2" ; "set cmark 0" ; "set grads off" ; "set y "z ; "set x 1" ; "d flow.7"
    "set ccolor  8" ; "set cstyle 2" ; "set cmark 0" ; "set grads off" ; "set y "z ; "set x 2" ; "d flow.7"
    "set ccolor  9" ; "set cstyle 2" ; "set cmark 0" ; "set grads off" ; "set y "z ; "set x 4" ; "d flow.10"
    "set ccolor  7" ; "set cstyle 3" ; "set cmark 0" ; "set grads off" ; "set y "z ; "set x 1" ; "d flow.13"
    "set ccolor  8" ; "set cstyle 3" ; "set cmark 0" ; "set grads off" ; "set y "z ; "set x 2" ; "d flow.13"
    "set ccolor  9" ; "set cstyle 3" ; "set cmark 0" ; "set grads off" ; "set y "z ; "set x 4" ; "d flow.16"
    "set ccolor  7" ; "set cstyle 5" ; "set cmark 0" ; "set grads off" ; "set y "z ; "set x 1" ; "d flow.19"
    "set ccolor  8" ; "set cstyle 5" ; "set cmark 0" ; "set grads off" ; "set y "z ; "set x 2" ; "d flow.19"
    "set ccolor  9" ; "set cstyle 5" ; "set cmark 0" ; "set grads off" ; "set y "z ; "set x 4" ; "d flow.22"
*   "set ccolor  1" ; "set cstyle 1" ; "set cmark 0" ; "set grads off" ; "set y "z ; "set x 1" ; "d hydt+"vint
    "set cthick 22" ; "set line 1 1 22"
    "set ccolor 55" ; "set cstyle 1" ; "set cmark 0" ; "set grads off" ; "set y "z ; "set x    1" ; "d maskout(maskout(0*hydi,2.5+hydi),-1.5-hydi)"
    "set ccolor 66" ; "set cstyle 1" ; "set cmark 0" ; "set grads off" ; "set y "z ; "set x    1" ; "d maskout(maskout(0*hydi,4.5+hydi),-3.5-hydi)"
    "set cthick 12" ; "set line 1 1 12"
*   "set strsiz 0.19" ; "set string 1 c 6" ; "draw string "cen-0.0" 10.7 "riv.z" flow (m`a3`n/s)"
*   "set strsiz 0.12" ; "set string 1 l 6" ; "draw string 1.1 10.3 "stn.z

    "set dfile 2" ; tims = timb ; "set parea "lef" "rig" "low.2" "hig.2
    "set gxout stat" ; max = 0
    "set t 1 "tims ; "set y "z ; "set x 1" ; "d hydt" ; ret = sublin(result,8) ; val = subwrd(ret,5) ; if (val > max) ; max = val ; endif
    "set t 1 "tims ; "set y "z ; "set x 1" ; "d flow" ; ret = sublin(result,8) ; val = subwrd(ret,5) ; if (val > max) ; max = val ; endif
    "set t 1 "tims ; "set y "z ; "set x 2" ; "d flow" ; ret = sublin(result,8) ; val = subwrd(ret,5) ; if (val > max) ; max = val ; endif
    "set t 1 "tims ; "set y "z ; "set x 3" ; "d flow" ; ret = sublin(result,8) ; val = subwrd(ret,5) ; if (val > max) ; max = val ; endif
    "set t 1 "tims ; "set y "z ; "set x 4" ; "d flow" ; ret = sublin(result,8) ; val = subwrd(ret,5) ; if (val > max) ; max = val ; endif
                      vran =20000 ; vint = 4000
    if (max <10000) ; vran =10000 ; vint = 2000 ; endif
    if (max < 5000) ; vran = 5000 ; vint = 1000 ; endif
    if (max < 4000) ; vran = 4000 ; vint =  800 ; endif
    if (max < 3000) ; vran = 3000 ; vint =  500 ; endif
    if (max < 2000) ; vran = 2000 ; vint =  400 ; endif
    if (max < 1000) ; vran = 1000 ; vint =  200 ; endif
    if (max <  500) ; vran =  500 ; vint =  100 ; endif
    if (max <  400) ; vran =  400 ; vint =   80 ; endif
    if (max <  300) ; vran =  300 ; vint =   50 ; endif
    if (max <  200) ; vran =  200 ; vint =   40 ; endif
    if (max <  100) ; vran =  100 ; vint =   20 ; endif
    if (max <   50) ; vran =   50 ; vint =   10 ; endif
    if (max <   30) ; vran =   30 ; vint =    5 ; endif
    if (max <   10) ; vran =   10 ; vint =    2 ; endif
    a = 1 ; while (a < 5) ; "set t 1" ; "set y "z ; "set x "a
      "d tmpa" ; ret = sublin(result,8) ; val = subwrd(ret,5) ; nse = math_format("%5.2f",val) ; "d tmpb" ; ret = sublin(result,8) ; val = subwrd(ret,5) ; cor = math_format("%5.2f",val)
      "d tmpd" ; ret = sublin(result,8) ; val = subwrd(ret,5) ; rms = math_format("%4.0f",val) ; "d tmpe" ; ret = sublin(result,8) ; val = subwrd(ret,5) ; bia = math_format("%4.0f",val)
      line.a = nse" "cor" "rms" "bia
    a = a + 1 ; endwhile
    "set t 0.5 "tims+0.5 ; "set gxout line" ; "set mproj off" ; "set grid off" ; "set mpdraw off" ; "set xlab on" ; "set ylab on"
    "set vrange 0 "vran ; "set ylint "vint ; "set cthick 12" ; "set line 1 1 12" ; "set string 1 c 8" ; "set missconn off"
    "set ccolor  1" ; "set cstyle 1" ; "set cmark 0" ; "set grads off" ; "set y "z ; "set x 1" ; "d hydt"
    "set ccolor  7" ; "set cstyle 1" ; "set cmark 0" ; "set grads off" ; "set y "z ; "set x 1" ; "d flow"
    "set ccolor  8" ; "set cstyle 1" ; "set cmark 0" ; "set grads off" ; "set y "z ; "set x 2" ; "d flow"
    "set ccolor  9" ; "set cstyle 1" ; "set cmark 0" ; "set grads off" ; "set y "z ; "set x 4" ; "d flow.5"
    "set ccolor  7" ; "set cstyle 2" ; "set cmark 0" ; "set grads off" ; "set y "z ; "set x 1" ; "d flow.8"
    "set ccolor  8" ; "set cstyle 2" ; "set cmark 0" ; "set grads off" ; "set y "z ; "set x 2" ; "d flow.8"
    "set ccolor  9" ; "set cstyle 2" ; "set cmark 0" ; "set grads off" ; "set y "z ; "set x 4" ; "d flow.11"
    "set ccolor  7" ; "set cstyle 3" ; "set cmark 0" ; "set grads off" ; "set y "z ; "set x 1" ; "d flow.14"
    "set ccolor  8" ; "set cstyle 3" ; "set cmark 0" ; "set grads off" ; "set y "z ; "set x 2" ; "d flow.14"
    "set ccolor  9" ; "set cstyle 3" ; "set cmark 0" ; "set grads off" ; "set y "z ; "set x 4" ; "d flow.17"
    "set ccolor  7" ; "set cstyle 5" ; "set cmark 0" ; "set grads off" ; "set y "z ; "set x 1" ; "d flow.20"
    "set ccolor  8" ; "set cstyle 5" ; "set cmark 0" ; "set grads off" ; "set y "z ; "set x 2" ; "d flow.20"
    "set ccolor  9" ; "set cstyle 5" ; "set cmark 0" ; "set grads off" ; "set y "z ; "set x 4" ; "d flow.23"
*   "set ccolor  1" ; "set cstyle 1" ; "set cmark 0" ; "set grads off" ; "set y "z ; "set x 1" ; "d hydt"
    xa = 5.9 ; xb = 6.0 ; ya = hig.1-0.2 ; "set line  1 1 22" ; "draw line "xa" "ya" "xb" "ya
                          ya = hig.1-0.4 ; "set line  9 1 22" ; "draw line "xa" "ya" "xb" "ya
                          ya = hig.1-0.6 ; "set line  8 1 22" ; "draw line "xa" "ya" "xb" "ya
                          ya = hig.1-0.8 ; "set line  7 1 22" ; "draw line "xa" "ya" "xb" "ya
*   xa = 5.9 ; xb = 6.0 ; ya = hig.1-0.2 ; "set line 55 1 22" ; "draw line "xa" "ya" "xb" "ya
*   xa = 6.5 ; xb = 6.6 ; ya = hig.1-0.2 ; "set line 66 1 22" ; "draw line "xa" "ya" "xb" "ya
    xa = 6.2 ;            ya = hig.1-0.2 ; "set strsiz 0.12" ; "set string 1 l 6" ; "draw string "xa" "ya" HYDAT"
                          ya = hig.1-0.4 ; "set strsiz 0.12" ; "set string 1 l 6" ; "draw string "xa" "ya" NN343"
                          ya = hig.1-0.6 ; "set strsiz 0.12" ; "set string 1 l 6" ; "draw string "xa" "ya" NN232"
                          ya = hig.1-0.8 ; "set strsiz 0.12" ; "set string 1 l 6" ; "draw string "xa" "ya" PEST"
*   xa = 6.1 ;            ya = hig.1-0.2 ; "set strsiz 0.12" ; "set string 1 l 6" ; "draw string "xa" "ya" ice"
*   xa = 6.7 ;            ya = hig.1-0.2 ; "set strsiz 0.12" ; "set string 1 l 6" ; "draw string "xa" "ya" estim)"
*   xa = 1.4 ; xb = 2.4 ; ya = hig.3-0.3 ; "set line  1 1 12" ; "draw line "xa" "ya" "xb" "ya ; yb = ya - 0.2 ; "draw string "xa" "yb" CCSM-8.5"
*   xa = 2.9 ; xb = xa + 1.0             ; "set line  1 2 12" ; "draw line "xa" "ya" "xb" "ya ; yb = ya - 0.2 ; "draw string "xa" "yb" HadGEM-4.5"
*   xa = 4.4 ; xb = xa + 1.0             ; "set line  1 3 12" ; "draw line "xa" "ya" "xb" "ya ; yb = ya - 0.2 ; "draw string "xa" "yb" HadGEM-8.5"
*   xa = 5.9 ; xb = xa + 1.0             ; "set line  1 5 12" ; "draw line "xa" "ya" "xb" "ya ; yb = ya - 0.2 ; "draw string "xa" "yb" MPI-LR-8.5"
    xa = 6.0 ; xb = 5.3 ; xc = 5.8 ; ya = hig.2-0.2 ; "set line  1 1 12" ; "draw line "xb" "ya" "xc" "ya ; "draw string "xa" "ya" CCSM-8.5"
                                     ya = hig.2-0.4 ; "set line  1 2 12" ; "draw line "xb" "ya" "xc" "ya ; "draw string "xa" "ya" HadGEM-4.5"
                                     ya = hig.2-0.6 ; "set line  1 3 12" ; "draw line "xb" "ya" "xc" "ya ; "draw string "xa" "ya" HadGEM-8.5"
                                     ya = hig.2-0.8 ; "set line  1 5 12" ; "draw line "xb" "ya" "xc" "ya ; "draw string "xa" "ya" MPI-LR-8.5"
    xa = lef+0.2      ;   ya = hig.1-0.3 ; "set strsiz 0.14" ; "set string 1 l 6" ; "draw string "xa" "ya" a) Daily"
                          ya = hig.2-0.3 ; "set strsiz 0.14" ; "set string 1 l 6" ; "draw string "xa" "ya" b) Monthly"
                          ya = hig.3-0.3 ; "set strsiz 0.14" ; "set string 1 l 6" ; "draw string "xa" "ya" c) Annual-Mean Daily"
    "set strsiz 0.14" ; "set string 1 l 6" ; "draw string "cen-1.0" "hig.1-0.2" "riv.z
    "set strsiz 0.14" ; "set string 1 l 6" ; "draw string "cen-1.0" "hig.1-0.5" "stn.z" (m`a3`n/s)"

    "set dfile 3" ; tims = timc ; "set parea "lef" "rig" "low.3" "hig.3
    "set gxout stat" ; max = 0
    "set t 1 "tims ; "set y "z ; "set x 1" ; "d hydt" ; ret = sublin(result,8) ; val = subwrd(ret,5) ; if (val > max) ; max = val ; endif
    "set t 1 "tims ; "set y "z ; "set x 1" ; "d flow" ; ret = sublin(result,8) ; val = subwrd(ret,5) ; if (val > max) ; max = val ; endif
    "set t 1 "tims ; "set y "z ; "set x 2" ; "d flow" ; ret = sublin(result,8) ; val = subwrd(ret,5) ; if (val > max) ; max = val ; endif
    "set t 1 "tims ; "set y "z ; "set x 3" ; "d flow" ; ret = sublin(result,8) ; val = subwrd(ret,5) ; if (val > max) ; max = val ; endif
    "set t 1 "tims ; "set y "z ; "set x 4" ; "d flow" ; ret = sublin(result,8) ; val = subwrd(ret,5) ; if (val > max) ; max = val ; endif
                      vran =20000 ; vint = 4000
    if (max <10000) ; vran =10000 ; vint = 2000 ; endif
    if (max < 5000) ; vran = 5000 ; vint = 1000 ; endif
    if (max < 4000) ; vran = 4000 ; vint =  800 ; endif
    if (max < 3000) ; vran = 3000 ; vint =  500 ; endif
    if (max < 2000) ; vran = 2000 ; vint =  400 ; endif
    if (max < 1000) ; vran = 1000 ; vint =  200 ; endif
    if (max <  500) ; vran =  500 ; vint =  100 ; endif
    if (max <  400) ; vran =  400 ; vint =   80 ; endif
    if (max <  300) ; vran =  300 ; vint =   50 ; endif
    if (max <  200) ; vran =  200 ; vint =   40 ; endif
    if (max <  100) ; vran =  100 ; vint =   20 ; endif
    if (max <   50) ; vran =   50 ; vint =   10 ; endif
    if (max <   30) ; vran =   30 ; vint =    5 ; endif
    if (max <   10) ; vran =   10 ; vint =    2 ; endif
    a = 1 ; while (a < 5) ; "set t 1" ; "set y "z ; "set x "a
      "d tmpa" ; ret = sublin(result,8) ; val = subwrd(ret,5) ; nse = math_format("%5.2f",val) ; "d tmpb" ; ret = sublin(result,8) ; val = subwrd(ret,5) ; cor = math_format("%5.2f",val)
      "d tmpd" ; ret = sublin(result,8) ; val = subwrd(ret,5) ; rms = math_format("%4.0f",val) ; "d tmpe" ; ret = sublin(result,8) ; val = subwrd(ret,5) ; bia = math_format("%4.0f",val)
      line.a = nse" "cor" "rms" "bia
    a = a + 1 ; endwhile
    "set t 0.5 "tims+0.5 ; "set gxout line" ; "set mproj off" ; "set grid off" ; "set mpdraw off" ; "set xlab on" ; "set ylab on"
    "set vrange 0 "vran ; "set ylint "vint ; "set cthick 12" ; "set line 1 1 12" ; "set string 1 c 8" ; "set missconn off"
    "set ccolor  1" ; "set cstyle 1" ; "set cmark 0" ; "set grads off" ; "set y "z ; "set x 1" ; "d hydt"
    "set ccolor  7" ; "set cstyle 1" ; "set cmark 0" ; "set grads off" ; "set y "z ; "set x 1" ; "d flow"
    "set ccolor  8" ; "set cstyle 1" ; "set cmark 0" ; "set grads off" ; "set y "z ; "set x 2" ; "d flow"
    "set ccolor  9" ; "set cstyle 1" ; "set cmark 0" ; "set grads off" ; "set y "z ; "set x 4" ; "d flow.6"
    "set ccolor  7" ; "set cstyle 2" ; "set cmark 0" ; "set grads off" ; "set y "z ; "set x 1" ; "d flow.9"
    "set ccolor  8" ; "set cstyle 2" ; "set cmark 0" ; "set grads off" ; "set y "z ; "set x 2" ; "d flow.9"
    "set ccolor  9" ; "set cstyle 2" ; "set cmark 0" ; "set grads off" ; "set y "z ; "set x 4" ; "d flow.12"
    "set ccolor  7" ; "set cstyle 3" ; "set cmark 0" ; "set grads off" ; "set y "z ; "set x 1" ; "d flow.15"
    "set ccolor  8" ; "set cstyle 3" ; "set cmark 0" ; "set grads off" ; "set y "z ; "set x 2" ; "d flow.15"
    "set ccolor  9" ; "set cstyle 3" ; "set cmark 0" ; "set grads off" ; "set y "z ; "set x 4" ; "d flow.18"
    "set ccolor  7" ; "set cstyle 5" ; "set cmark 0" ; "set grads off" ; "set y "z ; "set x 1" ; "d flow.21"
    "set ccolor  8" ; "set cstyle 5" ; "set cmark 0" ; "set grads off" ; "set y "z ; "set x 2" ; "d flow.21"
    "set ccolor  9" ; "set cstyle 5" ; "set cmark 0" ; "set grads off" ; "set y "z ; "set x 4" ; "d flow.24"
*   "set ccolor  1" ; "set cstyle 1" ; "set cmark 0" ; "set grads off" ; "set y "z ; "set x 1" ; "d hydt"
*   xa = 4.8 ; xb = 4.9 ; ya = hig.3-0.6 ; "set line  9 1 22" ; "draw line "xa" "ya" "xb" "ya
*                         ya = hig.3-0.4 ; "set line  8 1 22" ; "draw line "xa" "ya" "xb" "ya
*                         ya = hig.3-0.2 ; "set line  1 1 22" ; "draw line "xa" "ya" "xb" "ya
*   xa = 4.6 ;            ya = hig.3-0.6 ; "set strsiz 0.12" ; "set string 1 r 6" ; "draw string "xa" "ya" "line.4
*                         ya = hig.3-0.4 ; "set strsiz 0.12" ; "set string 1 r 6" ; "draw string "xa" "ya" "line.3
*                         ya = hig.3-0.2 ; "set strsiz 0.12" ; "set string 1 r 6" ; "draw string "xa" "ya" NSE`3 `0 COR`3 `0 RMS`3 `0 BIAS"
*   xa = 5.3 ;            ya = hig.3-0.6 ; "set strsiz 0.12" ; "set string 1 l 6" ; "draw string "xa" "ya" WRF forcing-neural"
*                         ya = hig.3-0.4 ; "set strsiz 0.12" ; "set string 1 l 6" ; "draw string "xa" "ya" WRF forcing-raw"
*   xa = 7.3 ; xb = 7.4 ; ya = hig.3-0.6 ; "set line  4 1 22" ; "draw line "xa" "ya" "xb" "ya
*                         ya = hig.3-0.4 ; "set line  7 1 22" ; "draw line "xa" "ya" "xb" "ya
* else
*   "set strsiz 0.22" ; "set string 1 c 6" ; "draw string "cen-0.0" 7.9 All Stations"
* endif

  "set line 0" ; "draw recf 0.70 "low.3-0.40" 1.20 "low.3-0.20
* xa = 7.65 ; ya = mid.1 ; "set strsiz 0.12" ; "set string 1 c 6 90" ; "draw string "xa" "ya" Daily"
*             ya = mid.2 ; "set strsiz 0.12" ; "set string 1 c 6 90" ; "draw string "xa" "ya" Monthly"
*             ya = mid.3 ; "set strsiz 0.12" ; "set string 1 c 6 90" ; "draw string "xa" "ya" Annual-Mean Daily"
*                                              "set string 1 c 6  0"

  "set t 1" ; "set mproj latlon"
  "set vpage 4.5 8 "hig.3-1.6" "hig.3-0.1
  newname = nam.z".sta"
  inner_map(newname)
  "set vpage off"
  "set t 1 "tims ; "set mproj off"

* plot = "plot."stema"."tail.z".png"
  plot = nam.z".hyd.png"
  say "printim "plot" png white x1700 y2200"
      "printim "plot" png white x1700 y2200"
  z = z + 1
  "quit"
endwhile
*"close 1"
"quit"


function inner_decomp(args)
  lef = subwrd(args,1)
  rig = subwrd(args,2)
  wid = subwrd(args,3)
  num = subwrd(args,4)
  _retmid.1   = lef + wid / 2
  _retmid.num = rig - wid / 2
  a = 2
  while (a < num)
    _retmid.a = (_retmid.num * (a-1) + _retmid.1 * (num-a)) / (num - 1)
    a = a + 1
  endwhile

  a = 1
  while (a <= num)
    _retlef.a = _retmid.a - wid / 2
    _retrig.a = _retmid.a + wid / 2
    a = a + 1
  endwhile
return

function inner_map(args)
say args
fila = subwrd(args,1)
filb = subwrd(args,1)".hyshed"
*filc = _home"/work/workg/DOMAIN_full_full/Fulldom_hires_river.txt"
filc = "Fulldom_hires_river.txt"
fild = subwrd(args,1)".hwshed"
*dell = subwrd(args,2)

filestat =  read(fila) ; snam = sublin(filestat,2)
a = 0 ; while a <  4   ; filestat = read(fila) ; a = a + 1 ; endwhile
filestat =  read(fila) ; line = sublin(filestat,2) ; hlat = subwrd(line,2)
filestat =  read(fila) ; line = sublin(filestat,2) ; hlon = subwrd(line,2)
a = 0 ; while a < 13   ; filestat = read(fila) ; a = a + 1 ; endwhile
filestat =  read(fila) ; line = sublin(filestat,2) ; glat = subwrd(line,2)
filestat =  read(fila) ; line = sublin(filestat,2) ; glon = subwrd(line,2)
a = 0 ; while a <  7   ; filestat = read(fila) ; a = a + 1 ; endwhile
filestat =  read(fila) ; line = sublin(filestat,2) ; hrea = subwrd(line,4) ; hrea = math_format("%.0f", hrea)
a = 0 ; while a < 59   ; filestat = read(fila) ; a = a + 1 ; endwhile
filestat =  read(fila) ; line = sublin(filestat,2) ; wlat = subwrd(line,2)
                                                     wrea = subwrd(line,5) ; wrea = math_format("%.0f", wrea)
filestat =  read(fila) ; line = sublin(filestat,2) ; wlon = subwrd(line,2) ; wper = math_format("%.0f", wrea / hrea * 100)
filestat = close(fila)

scod = substr(fila,7,7)
a =     26 ;                             b = substr(snam,a,1) ; c = substr(snam,a+1,1)
while (c != "[" & c != "") ; a = a + 1 ; b = substr(snam,a,1) ; c = substr(snam,a+1,1) ; endwhile
a = a - 26 ; name = substr(snam,26,a) ; len = a
temp = ""
a = 1 ; while (a <= len)
  b = substr(name,a,2)
  if (b = "À") ; b = "A" ; a = a + 1 ; endif
  if (b = "È") ; b = "E" ; a = a + 1 ; endif
  if (b = "É") ; b = "E" ; a = a + 1 ; endif
  if (b = "Ê") ; b = "E" ; a = a + 1 ; endif
  if (b = "Î") ; b = "I" ; a = a + 1 ; endif
  temp = temp""substr(b,1,1) ; a = a + 1
endwhile
snam = temp

filestat =  read(fild) ; line = sublin(filestat,2)
minlatb = subwrd(line,1) ; maxlatb = subwrd(line,2)
minlonb = subwrd(line,3) ; maxlonb = subwrd(line,4) ; lins = subwrd(line,5)
a = 0 ; wbord = ""
while (a < lins)
  filestat = read(fild) ; wbord = wbord" "sublin(filestat,2)
  a = a + 1
endwhile
filestat = close(fild)

minlat = minlatb ; if (minlatb < minlat) ; minlat = minlatb ; endif
maxlat = maxlatb ; if (maxlatb > maxlat) ; maxlat = maxlatb ; endif
minlon = minlonb ; if (minlonb < minlon) ; minlon = minlonb ; endif
maxlon = maxlonb ; if (maxlonb > maxlon) ; maxlon = maxlonb ; endif

"set dfile 25"
"set grads off"
"set grid off"
"set xlab off" ; "set xlopts 1 4 0.13"
"set ylab off" ; "set ylopts 1 4 0.13"
"set digsiz 0.09"
"set dignum 2"
"set mpdset hires"
*"set lat "hlat-1.0*dell" "hlat+1.0*dell
*"set lon "hlon-1.5*dell" "hlon+1.5*dell
*say "set lat "minlat-0.2" "maxlat+0.2
*say "set lon "minlon-0.2" "maxlon+0.2
"set lat "minlat-0.2" "maxlat+0.2
"set lon "minlon-0.2" "maxlon+0.2
"set xlint "inner_labint((maxlon - minlon) / 5) ; "set xlint 1"
"set ylint "inner_labint((maxlat - minlat) / 5) ; "set ylint 1"
"set clevs 9e9" ; "d hgt(t=1)"

inner_wbord = inner_disp_box(wbord)
"set rgb  54 150 150 150"
"set rgb  56  50  50  50 150"
"set line 56 1  1" ; "set grads off" ; "draw polyf "inner_wbord
"set line 1  1  2" ; "set grads off" ; "draw  line "inner_wbord

"set rgb  44 181 101  29"
"set rgb  45 222 170 136"

if (1 = 1)
  "set lwid 99 3" ; "set line 3 1 99"
  "draw shp "_home"/work/workg/sword/SWORD_v14_NA.shp/na_sword_reaches_hb71_v14"
  "draw shp "_home"/work/workg/sword/SWORD_v14_NA.shp/na_sword_reaches_hb72_v14"
  "draw shp "_home"/work/workg/sword/SWORD_v14_NA.shp/na_sword_reaches_hb73_v14"
  "draw shp "_home"/work/workg/sword/SWORD_v14_NA.shp/na_sword_reaches_hb74_v14"
  "draw shp "_home"/work/workg/sword/SWORD_v14_NA.shp/na_sword_reaches_hb78_v14"
  "draw shp "_home"/work/workg/sword/SWORD_v14_NA.shp/na_sword_reaches_hb81_v14"
  "draw shp "_home"/work/workg/sword/SWORD_v14_NA.shp/na_sword_reaches_hb82_v14"
  "draw shp "_home"/work/workg/sword/SWORD_v14_NA.shp/na_sword_reaches_hb83_v14"
  "draw shp "_home"/work/workg/sword/SWORD_v14_NA.shp/na_sword_reaches_hb84_v14"
  "draw shp "_home"/work/workg/sword/SWORD_v14_NA.shp/na_sword_reaches_hb85_v14"
  "draw shp "_home"/work/workg/sword/SWORD_v14_NA.shp/na_sword_reaches_hb86_v14"
  "draw shp "_home"/work/workg/sword/SWORD_v14_NA.shp/na_sword_reaches_hb91_v14"
* "set lwid 98 4"        ; "set line 44 1 98" ; "draw shp "_home"/work/workg/HYDROSHEDS/dams/GRanD_reservoirs_v1_1"
* "set shpopts -1 3 0.2" ; "set line 45 1 98" ; "draw shp "_home"/work/workg/HYDROSHEDS/dams/GRanD_dams_v1_1"
endif

"q gxinfo" ; _gxinfo = result
line3 = sublin(_gxinfo,3)
line4 = sublin(_gxinfo,4)
x1 = subwrd(line3,4)
x2 = subwrd(line3,6)
y1 = subwrd(line4,4)
y2 = subwrd(line4,6)
"set clip "x1" "x2" "y1" "y2

*if (1 = 0)
"set line 4 1 1"
filestat = read(filc)
while (sublin(filestat,1) = 0)
  line = sublin(filestat,2)
  linf = subwrd(line,1)" "subwrd(line,2)" "subwrd(line,3)" "subwrd(line,4)
  "draw line "inner_disp_box(linf)
  filestat = read(filc)
endwhile
filestat = close(filc)
*"set clip off"
*endif

"q w2xy "wlon" "wlat ; rec = sublin(result,1) ; xa  = subwrd(rec,3) ; ya  = subwrd(rec,6) ; "set line  1 1 8" ; "draw mark 3 "xa" "ya" 0.15"
*"q w2xy "glon" "glat ; rec = sublin(result,1) ; xa  = subwrd(rec,3) ; ya  = subwrd(rec,6) ; "set line 65 1 8" ; "draw mark 2 "xa" "ya" 0.25"
*"q w2xy "hlon" "hlat ; rec = sublin(result,1) ; xa  = subwrd(rec,3) ; ya  = subwrd(rec,6) ; "set line  1 1 8" ; "draw mark 2 "xa" "ya" 0.25"
"set dfile 1"
return

function inner_labint(args)
  diff = subwrd(args,1)
  if                (diff > 7.50) ; cint = 10   ; endif
  if (diff <= 7.50 & diff > 3.00) ; cint =  5   ; endif
  if (diff <= 3.00 & diff > 1.50) ; cint =  2   ; endif
  if (diff <= 1.50 & diff > 0.75) ; cint =  1   ; endif
  if (diff <= 0.75 & diff > 0.30) ; cint =  0.5 ; endif
  if (diff <= 0.30 & diff > 0.15) ; cint =  0.2 ; endif
  if (diff <= 0.15)               ; cint =  0.1 ; endif
return(cint)

function inner_disp_box(args)
  a = 1
  locs = ""
  lata = subwrd(args,a)
  lona = subwrd(args,a+1)
  while lona != ""
    "q w2xy "lona" "lata
    xa = subwrd(result,3)
    ya = subwrd(result,6)
    locs = locs" "xa" "ya
    a = a + 2
    lata = subwrd(args,a)
    lona = subwrd(args,a+1)
  endwhile
return(locs)
