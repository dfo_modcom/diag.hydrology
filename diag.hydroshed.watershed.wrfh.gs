* This script is designed to plot a watershed map.
* It can be executed using a command like
*
*     grads -blc "diag.hydroshed.watershed.wrfh wrfh_15370792.sta"
*
* - RD April 2021.

function plot(args)

fpz = "xyzzy.forgetit."subwrd(args,1) ; "!echo $HOME > "fpz ; line = read(fpz) ; home = sublin(line,2) ; ret = close(fpz) ; "!rm "fpz
fila = subwrd(args,1)
filb = subwrd(args,1)".hyshed"
*filc = home"/work/workg/DOMAIN_full_full/Fulldom_hires_river.txt"
filc = "Fulldom_hires_river.txt"
fild = subwrd(args,1)".hwshed"
*dell = subwrd(args,2)

filestat =  read(fila) ; snam = sublin(filestat,2)
a = 0 ; while a <  4   ; filestat = read(fila) ; a = a + 1 ; endwhile
filestat =  read(fila) ; line = sublin(filestat,2) ; hlat = subwrd(line,2)
filestat =  read(fila) ; line = sublin(filestat,2) ; hlon = subwrd(line,2)
a = 0 ; while a < 13   ; filestat = read(fila) ; a = a + 1 ; endwhile
filestat =  read(fila) ; line = sublin(filestat,2) ; glat = subwrd(line,2)
filestat =  read(fila) ; line = sublin(filestat,2) ; glon = subwrd(line,2)
a = 0 ; while a <  7   ; filestat = read(fila) ; a = a + 1 ; endwhile
filestat =  read(fila) ;*line = sublin(filestat,2) ; hrea = subwrd(line,4) ; hrea = math_format("%.0f", hrea)
a = 0 ; while a < 59   ; filestat = read(fila) ; a = a + 1 ; endwhile
filestat =  read(fila) ; line = sublin(filestat,2) ; wlat = subwrd(line,2)
                                                     wrea = subwrd(line,5) ; wrea = math_format("%.0f", wrea)
filestat =  read(fila) ; line = sublin(filestat,2) ; wlon = subwrd(line,2) ;*wper = math_format("%.0f", wrea / hrea * 100)
filestat = close(fila)

scod = substr(fila,6,8)
a =     26 ;                             b = substr(snam,a,1) ; c = substr(snam,a+1,1)
while (c != "[" & c != "") ; a = a + 1 ; b = substr(snam,a,1) ; c = substr(snam,a+1,1) ; endwhile
a = a - 26 ; name = substr(snam,26,a) ; len = a
temp = ""
a = 1 ; while (a <= len)
  b = substr(name,a,2)
  if (b = "À") ; b = "A" ; a = a + 1 ; endif
  if (b = "È") ; b = "E" ; a = a + 1 ; endif
  if (b = "É") ; b = "E" ; a = a + 1 ; endif
  if (b = "Ê") ; b = "E" ; a = a + 1 ; endif
  if (b = "Î") ; b = "I" ; a = a + 1 ; endif
  temp = temp""substr(b,1,1) ; a = a + 1
endwhile
snam = temp

if (1 = 0)
filestat =  read(filb) ; line = sublin(filestat,2)
minlata = subwrd(line,1) ; maxlata = subwrd(line,2)
minlona = subwrd(line,3) ; maxlona = subwrd(line,4) ; lins = subwrd(line,5)
a = 0 ; hbord = ""
while (a < lins)
  filestat = read(filb) ; hbord = hbord" "sublin(filestat,2)
  a = a + 1
endwhile
filestat = close(filb)
endif

filestat =  read(fild) ; line = sublin(filestat,2)
minlatb = subwrd(line,1) ; maxlatb = subwrd(line,2)
minlonb = subwrd(line,3) ; maxlonb = subwrd(line,4) ; lins = subwrd(line,5)
a = 0 ; wbord = ""
while (a < lins)
  filestat = read(fild) ; wbord = wbord" "sublin(filestat,2)
  a = a + 1
endwhile
filestat = close(fild)

minlat = minlatb ;*if (minlata < minlat) ; minlat = minlata ; endif
maxlat = maxlatb ;*if (maxlata > maxlat) ; maxlat = maxlata ; endif
minlon = minlonb ;*if (minlona < minlon) ; minlon = minlona ; endif
maxlon = maxlonb ;*if (maxlona > maxlon) ; maxlon = maxlona ; endif

"clear"
"set grads off"
"set grid off"
"set xlab on"
"set ylab on"
"set digsiz 0.09"
"set dignum 2"
"set mpdset hires"
*"set mpdraw off"
*"set mproj off"
*"set mproj nps"
*"set gxout grfill"

"sdfopen "home"/work/workg/HYDROSHEDS/hgt.sfc.nc"
*"set lat "hlat-1.0*dell" "hlat+1.0*dell
*"set lon "hlon-1.5*dell" "hlon+1.5*dell
"set lat "minlat-0.2" "maxlat+0.2
"set lon "minlon-0.2" "maxlon+0.2
"set xlint "inner_labint((maxlon - minlon) / 5)
"set ylint "inner_labint((maxlat - minlat) / 5)
"set clevs 9e9" ; "d hgt"

*inner_hbord = inner_disp_box(hbord)
inner_wbord = inner_disp_box(wbord)

"set rgb  65 230 220  50"
"set rgb  66 230 220  50 100"
*"set line 66 1  1" ; "set grads off" ; "draw polyf "inner_hbord
*"set line 65 1 12" ; "set grads off" ; "draw  line "inner_hbord

"set rgb  54 150 150 150"
"set rgb  55 150 150 150 100"
"set line 55 1  1" ; "set grads off" ; "draw polyf "inner_wbord
"set line 54 1 12" ; "set grads off" ; "draw  line "inner_wbord

if (1 = 1)
  "set lwid 99 15" ; "set line 3 1 99"
  "draw shp "home"/work/workg/sword/SWORD_v14_NA.shp/na_sword_reaches_hb71_v14"
  "draw shp "home"/work/workg/sword/SWORD_v14_NA.shp/na_sword_reaches_hb72_v14"
  "draw shp "home"/work/workg/sword/SWORD_v14_NA.shp/na_sword_reaches_hb73_v14"
  "draw shp "home"/work/workg/sword/SWORD_v14_NA.shp/na_sword_reaches_hb74_v14"
  "draw shp "home"/work/workg/sword/SWORD_v14_NA.shp/na_sword_reaches_hb78_v14"
  "draw shp "home"/work/workg/sword/SWORD_v14_NA.shp/na_sword_reaches_hb81_v14"
  "draw shp "home"/work/workg/sword/SWORD_v14_NA.shp/na_sword_reaches_hb82_v14"
  "draw shp "home"/work/workg/sword/SWORD_v14_NA.shp/na_sword_reaches_hb83_v14"
  "draw shp "home"/work/workg/sword/SWORD_v14_NA.shp/na_sword_reaches_hb84_v14"
  "draw shp "home"/work/workg/sword/SWORD_v14_NA.shp/na_sword_reaches_hb85_v14"
  "draw shp "home"/work/workg/sword/SWORD_v14_NA.shp/na_sword_reaches_hb86_v14"
  "draw shp "home"/work/workg/sword/SWORD_v14_NA.shp/na_sword_reaches_hb91_v14"
endif

"q gxinfo" ; _gxinfo = result
line3 = sublin(_gxinfo,3)
line4 = sublin(_gxinfo,4)
x1 = subwrd(line3,4)
x2 = subwrd(line3,6)
y1 = subwrd(line4,4)
y2 = subwrd(line4,6)
"set clip "x1" "x2" "y1" "y2

"set line 4 1 12"
filestat = read(filc)
while (sublin(filestat,1) = 0)
  line = sublin(filestat,2)
  linf = subwrd(line,1)" "subwrd(line,2)" "subwrd(line,3)" "subwrd(line,4)
  "draw line "inner_disp_box(linf)
  filestat = read(filc)
endwhile
filestat = close(filc)
*"set clip off"

"set line 1 1 3" ; "set shpopts 15"
"set grads off" ; "set mpdraw off" ; "draw shp "home"/work/workg/HYDROSHEDS/HydroRIVERS_v10_30_85N_180_00W.shp"
*endif

"q w2xy "wlon" "wlat ; rec = sublin(result,1) ; xa  = subwrd(rec,3) ; ya  = subwrd(rec,6) ; "set line  4 1 8" ; "draw mark 2 "xa" "ya" 0.25"
"q w2xy "glon" "glat ; rec = sublin(result,1) ; xa  = subwrd(rec,3) ; ya  = subwrd(rec,6) ; "set line 65 1 8" ; "draw mark 2 "xa" "ya" 0.25"
*"q w2xy "hlon" "hlat ; rec = sublin(result,1) ; xa  = subwrd(rec,3) ; ya  = subwrd(rec,6) ; "set line  1 1 8" ; "draw mark 2 "xa" "ya" 0.25"

*"q gxinfo" ; _gxinfo = result
*line3 = sublin(_gxinfo,3)
*line4 = sublin(_gxinfo,4)
*x1 = subwrd(line3,4)
*x2 = subwrd(line3,6)
*y1 = subwrd(line4,4)
*y2 = subwrd(line4,6)
*"set clip "x1" "x2" "y1" "y2

*"set rgb  65 230 220  50"
*"set rgb  66 230 220  50 100"
*"set rgb  54 150 150 150"
*"set rgb  55 150 150 150 100" ; "set lwid 99 15"
*"set vpage off"
"set string 1 l 5"
xx = 0.7 ; yy = y2+0.7 ; "set line  1 1 8" ; "draw mark 2 "xx"     "yy" 0.25"
                         "set strsiz 0.15" ; "draw string "xx+0.3" "yy" "scod
                         "set strsiz 0.10" ; "draw string "xx+1.7" "yy" "snam
xx = 0.7 ; yy = y2+0.3 ; "set line 55 1 8" ; "draw mark 5 "xx"     "yy" 0.25"
                         "set line 54 1 8" ; "draw mark 4 "xx"     "yy" 0.25"
                         "set line 4 1 12" ; "draw line   "xx-0.2" "yy" "xx+0.2" "yy
                         "set strsiz 0.15" ; "draw string "xx+0.3" "yy" WRF-Hydro ("wrea"km`a2`n)"
xx = 4.6 ; yy = y2+0.3 ; "set strsiz 0.15" ;*"draw string "xx+0.3" "yy" "wper"% of"
xx = 7.0 ; yy = y2+0.3 ; "set line 66 1 8" ;*"draw mark 5 "xx"     "yy" 0.25"
                         "set line 65 1 8" ;*"draw mark 4 "xx"     "yy" 0.25"
                         "set line 1 1  8" ;*"draw line   "xx-0.2" "yy" "xx+0.2" "yy
                         "set strsiz 0.15" ;*"draw string "xx+0.3" "yy" HydroSHEDS ("hrea"km`a2`n)"
xx = 9.2 ; yy = y2+0.7 ; "set line 3 1 99" ; "draw line   "xx-0.2" "yy" "xx+0.2" "yy
                         "set strsiz 0.15" ; "draw string "xx+0.4" "yy" SWOT"

say "gxprint "fila".png png white x1100 y850"
    "gxprint "fila".png png white x1100 y850"
"quit"

function inner_labint(args)
  diff = subwrd(args,1)
  if                (diff > 7.50) ; cint = 10   ; endif
  if (diff <= 7.50 & diff > 3.00) ; cint =  5   ; endif
  if (diff <= 3.00 & diff > 1.50) ; cint =  2   ; endif
  if (diff <= 1.50 & diff > 0.75) ; cint =  1   ; endif
  if (diff <= 0.75 & diff > 0.30) ; cint =  0.5 ; endif
  if (diff <= 0.30 & diff > 0.15) ; cint =  0.2 ; endif
  if (diff <= 0.15)               ; cint =  0.1 ; endif
return(cint)

function inner_disp_box(args)
  a = 1
  locs = ""
  lata = subwrd(args,a)
  lona = subwrd(args,a+1)
  while lona != ""
    "q w2xy "lona" "lata
    xa = subwrd(result,3)
    ya = subwrd(result,6)
    locs = locs" "xa" "ya
    a = a + 2
    lata = subwrd(args,a)
    lona = subwrd(args,a+1)
  endwhile
return(locs)
