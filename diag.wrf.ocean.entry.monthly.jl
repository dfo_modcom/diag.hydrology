#=
 = Convert daily data to monthly by simple averaging - RD May 2020.
 =#

using My, Printf, NetCDF

const DELT             = 1, "dy"                        # input file timestep
const MISS             = -9999.0                        # generic missing value
const D2R              = 3.141592654 / 180.0            # degrees to radians conversion

if (argc = length(ARGS)) != 6
  print("\nUsage: jjj $(basename(@__FILE__)) streamflow 1972060100 00.CHRTOUT_GRID1 4747 231 535\n")
  exit(1)
end
fila = ARGS[1] * "." * ARGS[2] * ARGS[3] * "." * ARGS[4] * "." * ARGS[5] * "." * ARGS[6] * ".nc"
filb = ARGS[1] * "." * ARGS[2] *           "." * ARGS[4] * "." * ARGS[5] * "." * ARGS[6] * ".monthly.nc"
ndat = parse(Int, ARGS[4])
vals = Array{Float64}(undef,0)
tims = Array{Float64}(undef,0)

dnow = ARGS[2]                                                                # read the timeseries then store it
dref = ARGS[2][1:6]
valb = numb = 0.0
for a = 1:ndat
  global dnow, dref, valb, numb
  valu = ncread(fila, "tmp", start=[1,1,a], count=[1,1,1])[1]
  valu != MISS && (valb += valu ; numb += 1.0)
  next = My.dateadd(dnow, DELT[1], DELT[2])
  if next[1:6] != dref || a == ndat
    time = My.datesous("1900010100", dref * "0100", "hr")
    valb = numb == 0 ? MISS : valb / numb
    push!(vals, valb) ; push!(tims, time)
    dref = next[1:6]
    valb = numb = 0.0
  end
  dnow = next
end

nccreer(      filb, length(vals), 1, 1, MISS)
ncwrite(vals, filb,  "tmp", start=[1,1,1], count=[-1,-1,-1])
ncwrite(tims, filb, "time", start=[1],     count=[-1])
ncputatt(     filb, "time", Dict("units" => "hours since 1900-01-01 00:00:0.0"))
exit(0)
