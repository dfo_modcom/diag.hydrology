#=
 = Train a neural network to better match the normalized HYDAT and WRF-Hydro
 = streamflow timeseries, treating each station like a separate catchment with
 = a separate neural networks on output.  Omit data prior to an intermediate
 = date and any missing values.  Day of the year and various mean differences
 = between HYDAT and WRF-Hydro streamflow are given as input - RD Feb 2024.
 =#

using My, Printf, NetCDF, Flux, Statistics, ProgressMeter, JLD2

const WRFR             = 1                              # streamflow forced by WRF-Hydro
const WRFN             = 2                              # streamflow forced by WRF-Hydro neural network post-processing
const ERAR             = 3                              # streamflow forced by ERA-5
const ERAN             = 4                              # streamflow forced by ERA-5     neural network post-processing
const DNUM             = 4                              # number of variable types
const DELT             = 3                              # source data timestep (hours)
const DELD             = 24                             # daily       timestep (hours)
const LAGS             = 48                             # number of 3-h streamflow values before and after
const FEAT             = 3                              # number of features (neural network inputs)
const MISS             = -9999.0                        # generic missing value

if (argc = length(ARGS)) != 2 && (argc = length(ARGS)) != 3
  print("\nUsage: jjj $(basename(@__FILE__)) 1990-01-01 hydat_valding_aall_1000_incl_upnat_1990-01-01_2004-12-30.neural.prep [identity]\n\n")
  exit(1)
end
acti = argc == 2 ? "identity" : ARGS[3]
cutt = ARGS[1] * "-00"
fila = ARGS[2] * ".nc"
dila = ARGS[2] * ".daily.nc"
mila = ARGS[2] * ".monthly.nc"
yila = ARGS[2] * ".annualday.nc"
filb = ARGS[2] * ".txt"
filc = ARGS[2] * ".val"
fild = ARGS[2] * ".nnet.$acti.nc"
dild = ARGS[2] * ".nnet.$acti.daily.nc"
mild = ARGS[2] * ".nnet.$acti.monthly.nc"
yild = ARGS[2] * ".nnet.$acti.annualday.nc"
file = ARGS[2] * ".nnet.$acti.txt"
filf = ARGS[2] * ".nnet.$acti.val"
stem = ARGS[2] * ".nnet.$acti"

@printf("\ncopying %s %s\n", fila, fild) ; cp(fila, fild; force = true)
@printf(  "copying %s %s\n", dila, dild) ; cp(dila, dild; force = true)
@printf(  "copying %s %s\n", mila, mild) ; cp(mila, mild; force = true)
@printf(  "copying %s %s\n", yila, yild) ; cp(yila, yild; force = true)
@printf(  "copying %s %s\n", filb, file) ; cp(filb, file; force = true)
@printf(  "copying %s %s\n", filc, filf) ; cp(filc, filf; force = true)

tims = ncread(fild, "time", start=[1], count=[-1]) ; ntim = length(tims)
dims = ncread(dild, "time", start=[1], count=[-1]) ; ndim = length(dims)
mims = ncread(mild, "time", start=[1], count=[-1]) ; nmim = length(mims)
yims = ncread(yild, "time", start=[1], count=[-1]) ; nyim = length(yims)
fpa  = My.ouvre(file, "r") ; line = readlines(fpa) ; close(fpa)
fpa  = My.ouvre(filf, "r") ; linf = readlines(fpa) ; close(fpa)
nsta = length(line)
npes = DNUM
pind = ERAN

@printf("reading %-99s with %7d times %d stations %d streamflow types, with training on %d\n", fild, ntim, nsta, npes, pind)
@printf("reading %-99s with %7d times %d stations %d streamflow types, with training on %d\n", dild, ndim, nsta, npes, pind)
@printf("reading %-99s with %7d times %d stations %d streamflow types, with training on %d\n", mild, nmim, nsta, npes, pind)
@printf("reading %-99s with %7d times %d stations %d streamflow types, with training on %d\n", yild, nyim, nsta, npes, pind)

ftod = Dict{Int64,   Int64}()                                                 # parse the DELT-h start/end times
ftom = Dict{Int64,   Int64}()                                                 # and map the time index from DELT
ftoy = Dict{Int64,   Int64}()                                                 # to daily, monthly, and annual
fiii = Dict{Int64, Float32}()
tatt = ncgetatt(fild, "time", "units")
tmpa = dateref(tims[  1], tatt) ; hora = tmpa[1:4] * "-" * tmpa[5:6] * "-" * tmpa[7:8] * "-" * tmpa[9:10]
tmpb = dateref(tims[end], tatt) ; horb = tmpb[1:4] * "-" * tmpb[5:6] * "-" * tmpb[7:8] * "-" * tmpb[9:10]
hnow = hora
for a = 1:ntim                                                                # (but use Feb 28 for Feb 29)
  global hnow
  if hnow[5:10] == "-02-29"
    dnow = My.datesous("1900-01-01-00",             hnow[1: 7] * "-28-00", "hr") ; dind = findfirst(isequal(dnow), dims)
    mnow = My.datesous("1900-01-01-00",             hnow[1: 7] * "-15-00", "hr") ; mind = findfirst(isequal(mnow), mims)
    ynow = My.datesous("1900-01-01-00", hora[1:4] * hnow[5: 7] * "-28-00", "hr") ; yind = findfirst(isequal(ynow), yims)
  else
    dnow = My.datesous("1900-01-01-00",             hnow[1:10] *    "-00", "hr") ; dind = findfirst(isequal(dnow), dims)
    mnow = My.datesous("1900-01-01-00",             hnow[1: 7] * "-15-00", "hr") ; mind = findfirst(isequal(mnow), mims)
    ynow = My.datesous("1900-01-01-00", hora[1:4] * hnow[5:10] *    "-00", "hr") ; yind = findfirst(isequal(ynow), yims)
  end
  ftod[a] = dind
  ftom[a] = mind
  ftoy[a] = yind
  fiii[a] = parse(Float32, hnow[6:7])
# @show hnow, dateref(dims[ftod[a]], tatt), dateref(mims[ftom[a]], tatt), dateref(yims[ftoy[a]], tatt)
  hnow = My.dateadd(hnow, DELT, "hr")
end

#for a = 86:86 # 1:nsta
#for a in [58,86]
for a = 1:nsta
  floh = ncread(fild, "hydt", start=[  a,1], count=[   1,-1])                 # read the various streamflow estimates
  dloh = ncread(dild, "hydt", start=[  a,1], count=[   1,-1])
  mloh = ncread(mild, "hydt", start=[  a,1], count=[   1,-1])
  yloh = ncread(yild, "hydt", start=[  a,1], count=[   1,-1])
  flow = ncread(fild, "flow", start=[1,a,1], count=[-1,1,-1])
  dlow = ncread(dild, "flow", start=[1,a,1], count=[-1,1,-1])
  mlow = ncread(mild, "flow", start=[1,a,1], count=[-1,1,-1])
  ylow = ncread(yild, "flow", start=[1,a,1], count=[-1,1,-1])

  mskh = falses(1, ntim) ; mskw = falses(npes, 1, ntim)                       # define masks where HYDAT observations
  msdh = falses(1, ntim) ; msdw = falses(npes, 1, ntim)                       # and the WRF-Hydro model are valid
  msmh = falses(1, ntim) ; msmw = falses(npes, 1, ntim)
  msyh = falses(1, ntim) ; msyw = falses(npes, 1, ntim)
  for b = 1:ntim  floh[1,b] > MISS && (mskh[1,b] = true) ; for c = 1:npes  flow[c,1,b] > MISS && (mskw[c,1,b] = true)  end  end
  for b = 1:ndim  dloh[1,b] > MISS && (msdh[1,b] = true) ; for c = 1:npes  dlow[c,1,b] > MISS && (msdw[c,1,b] = true)  end  end
  for b = 1:nmim  mloh[1,b] > MISS && (msmh[1,b] = true) ; for c = 1:npes  mlow[c,1,b] > MISS && (msmw[c,1,b] = true)  end  end
  for b = 1:nyim  yloh[1,b] > MISS && (msyh[1,b] = true) ; for c = 1:npes  ylow[c,1,b] > MISS && (msyw[c,1,b] = true)  end  end

  xxtt = Array{Float32}(undef,FEAT+0,0)                                       # populate the un/calibrated matrices
  yytt = Array{Float32}(undef,FEAT  ,0)                                       # with normalized HYDAT and WRF-Hydro
  mskk = Array{  Int64}(undef,       0)                                       # short timeseries (where all valid)
  local hnow ; hnow = hora
  for b = 1:ntim
    if mskh[1,b] && mskw[pind,1,b] && msmh[1,ftom[b]] && msmw[pind,1,ftom[b]] && msyh[1,ftoy[b]] && msyw[pind,1,ftoy[b]]
      xxaa = Float32(flow[pind,1,     b])
      yyaa = Float32(floh[     1,     b])
      xxbb = Float32(mlow[pind,1,ftom[b]])
      yybb = Float32(mloh[     1,ftom[b]])
      xxcc = Float32(ylow[pind,1,ftoy[b]])
      yycc = Float32(yloh[     1,ftoy[b]])
      xxdd =                     fiii[b]
      yydd =                     fiii[b]
#     xxtt = [xxtt [xxaa, xxbb, xxcc, xxdd]]
      xxtt = [xxtt [xxaa, xxbb, xxcc]]
      yytt = [yytt [yyaa, yybb, yycc]]
      push!(mskk, a)
    end
    hnow = My.dateadd(hnow, DELT, "hr")
  end
  @printf("station %3d has %5d training collocations\n", a, length(mskk[mskk .== a]))

  acti == "identity" && (model = Chain(Dense(FEAT+0 => FEAT+1),                               BatchNorm(FEAT+1), Dense(FEAT+1 => FEAT)))
  acti != "identity" && (model = Chain(Dense(FEAT+0 => FEAT+1, getfield(Main, Symbol(acti))), BatchNorm(FEAT+1), Dense(FEAT+1 => FEAT, getfield(Main, Symbol(acti)))))
  optim = Flux.setup(Flux.Adam(0.01), model)

  losses = []                                                                 # evaluate model and loss inside gradient context
  @showprogress for epoch in 1:1000                                           # and record losses outside gradient context
    loss, grads = Flux.withgradient(model) do m
      yhat = m(xxtt)
      Flux.mse(yhat, yytt)
    end
    Flux.update!(optim, model, grads[1])
    push!(losses, loss)
  end
  @printf("first   loss %15.8f\n last-1 loss %15.8f\n last   loss %15.8f\n", losses[1], losses[end-1], losses[end])

  @printf("writing full flows back to %s\n", fild)                            # replace all WRF-Hydro estimates
  for c = 1:npes                                                              # with neural network estimates
    temp = zeros(ntim)
    local hnow ; hnow = hora
    for b = 1:ntim
      if mskw[c,1,b] &&   msmw[c,1,ftom[b]] && msyw[c,1,ftoy[b]]
        xxaa    = Float32(flow[c,1,     b])
        xxbb    = Float32(mlow[c,1,ftom[b]])
        xxcc    = Float32(ylow[c,1,ftoy[b]])
        xxdd    =                  fiii[b]
#       temp[b] = Float64(model(reshape([xxaa,xxbb,xxcc,xxdd],FEAT+1,1))[1])
        temp[b] = Float64(model(reshape([xxaa,xxbb,xxcc     ],FEAT+0,1))[1])
      end
      hnow = My.dateadd(hnow, DELT, "hr")
    end
    for b = 1:ntim
      if mskw[c,1,b]
        temp[b] <= 0 && print("WARNING: $c flow $(temp[b]) <= 0\n")
        flow[c,1,b] = temp[b]
      end
    end
  end

  filg = @sprintf("%s.%3d.jld", stem, a)                                      # and save the neural network
  filg = replace(filg, ' ' => '0')
  @printf("writing  neural network to %s\n\n", filg)
  model_state = Flux.state(model) ; jldsave(filg; model_state)

  alp, bet = parse.(Float64, split(linf[a]))                                  # finally rescale HYDAT and WRF-Hydro
  for b = 1:ntim  floh[1,b] > MISS && (floh[1,b] = floh[1,b] * bet + alp) ; for c = 1:npes  flow[c,1,b] > MISS && (flow[c,1,b] = flow[c,1,b] * bet + alp)  end  end
  for b = 1:ndim  dloh[1,b] > MISS && (dloh[1,b] = dloh[1,b] * bet + alp) ; for c = 1:npes  dlow[c,1,b] > MISS && (dlow[c,1,b] = dlow[c,1,b] * bet + alp)  end  end
  for b = 1:nmim  mloh[1,b] > MISS && (mloh[1,b] = mloh[1,b] * bet + alp) ; for c = 1:npes  mlow[c,1,b] > MISS && (mlow[c,1,b] = mlow[c,1,b] * bet + alp)  end  end
  for b = 1:nyim  yloh[1,b] > MISS && (yloh[1,b] = yloh[1,b] * bet + alp) ; for c = 1:npes  ylow[c,1,b] > MISS && (ylow[c,1,b] = ylow[c,1,b] * bet + alp)  end  end
  ncwrite(floh, fild, "hydt", start=[  a,1], count=[   1,-1])
  ncwrite(dloh, dild, "hydt", start=[  a,1], count=[   1,-1])
  ncwrite(mloh, mild, "hydt", start=[  a,1], count=[   1,-1])
  ncwrite(yloh, yild, "hydt", start=[  a,1], count=[   1,-1])
  ncwrite(flow, fild, "flow", start=[1,a,1], count=[-1,1,-1])
  ncwrite(dlow, dild, "flow", start=[1,a,1], count=[-1,1,-1])
  ncwrite(mlow, mild, "flow", start=[1,a,1], count=[-1,1,-1])
  ncwrite(ylow, yild, "flow", start=[1,a,1], count=[-1,1,-1])
end

print("\n")
exit(0)

#=
  cuti = 0
# cutd = datesous("1900-01-01-00", cutt, "hr")
# cuti = findfirst(isequal(cutd), tims) ; isnothing(cuti) && (cuti = 0)
# @printf("omitting 1:%d of 1:%d timeseries\n", cuti, ntim)

                           mask = falses(npes, 1, ntim)
  for b = 1:ntim
    floh[1,b]   > MISS &&                                    (mskh[  1,b] = true)
    for c = 1:npes
                          flow[c,1,b] > MISS &&              (mskw[c,1,b] = true)
      floh[1,b] > MISS && flow[c,1,b] > MISS && b >= cuti && (mask[c,1,b] = true)
    end
    b == ntim && @printf("station %3d has %5d %5d %5d valid data (HYDAT / WRF-Hydro / both)\n",
      a, length(mskh[1,mskh[1,:]]), length(mskw[pind,1,mskw[pind,1,:]]), length(mask[pind,1,mask[pind,1,:]]))
  end

  alp, bet = parse.(Float64, split(linf[a]))                                  # finally rescale HYDAT and WRF-Hydro
                  floh[  a,mskh[  a,:]] = floh[  a,mskh[  a,:]] * bet .+ alp
  for b = 1:npes  flow[b,a,mskw[b,a,:]] = flow[b,a,mskw[b,a,:]] * bet .+ alp  end

  for b = 1:ntim
    floh[1,b]   > MISS &&                                    (mskh[  1,b] = true)
    for c = 1:npes
                          flow[c,1,b] > MISS &&              (mskw[c,1,b] = true)
      floh[1,b] > MISS && flow[c,1,b] > MISS && b >= cuti && (mask[c,1,b] = true)
    end
    b == ntim && @printf("station %3d has %5d %5d %5d valid data (HYDAT / WRF-Hydro / both)\n",
      a, length(mskh[1,mskh[1,:]]), length(mskw[pind,1,mskw[pind,1,:]]), length(mask[pind,1,mask[pind,1,:]]))
  end
=#
