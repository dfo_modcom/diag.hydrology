#=
 = This program extends the timeseries of the FORCING data and identifies any missing files
 = based on the interval and start and end dates of the expected file names - RD March 2020.
 =#

using My
const MISS             = -9999.0                        # generic missing value

if (argc = length(ARGS)) != 5
  print("\nUsage: jjj $(basename(@__FILE__)) 1969010118 2005010118 .LDASIN_DOMAIN1 6 hr\n\n")
  exit(1)
end
datea =        ARGS[1]
dateb =        ARGS[2]
tails =        ARGS[3]
delta = -float(ARGS[4])
deltb =        ARGS[5]

num = 0
while dateb != datea
  nama = @sprintf("%d", parse(Int, dateb[1:4]) - 36) * dateb[5:10] * tails
  namb =                           dateb                           * tails
  if !isfile(namb)
    println(namb)
    num += 1
  else
    symlink(namb, nama)
#   @printf("ln -s %s %s\n", namb, nama)
  end
  dateb = dateadd(dateb, delta, deltb)
end

@printf("missing %d files\n", num)
exit(0)
