#=
 = Collect individual river and basin networks into a grouped shapefile
 = for flows to the ocean based on the HydroSHEDs 15-sec river and basin
 = shapefiles.  Only larger basin flows entirely northward of a latitude
 = cutoff are collected (third argument is the upstream watershed area
 = cutoff in km2 and fourth is latitude cutoff on the eastern side of NA,
 = with predefined cutoffs on the western side) - RD Apr 2021, 2023.
 =#

using My, Printf

if (argc = length(ARGS)) != 4
  print("\nUsage: jjj $(basename(@__FILE__)) HydroRIVERS_v10.shp hybas_na_lev12_v1c.shp 1000 41\n\n")
  exit(1)
end
rivr = split(ARGS[1], ".shp")[1] ; rivs = rivr * "_30_85N_180_00W"
basn = split(ARGS[2], ".shp")[1] ; bass = basn * "_30_85N_180_00W"
ucut = parse(Float64, ARGS[3])   ; lcut = parse(Float64, ARGS[4])
rivo = @sprintf("%s_outlets_%4.0f_%2.0fN", rivs, ucut, lcut)
rivo = replace(rivo, ' ' => '0') ; print("\n")

!isfile("$rivs.csv") && error("\nERROR : $rivs.csv is missing\n\n")
fpa = My.ouvre("$rivs.csv", "r") ; lins = readlines(fpa) ; close(fpa)
fpb = My.ouvre("$rivo.txt", "a")

for a = 2:length(lins)                                                        # and for each outflow whose upstream area
  temp    = split(lins[a], ",")                                               # is larger than ucut, group all connected
  rivnum  =     parse(  Int64,       temp[1][2:end-1])                        # basins and save the upstream shape, with
  rivcol  = sum(parse.( Int64, split(temp[1][2:end-1], ""))) % 14 + 2         # a list of shapefile names and a colour
  mainum  =     parse(  Int64,       temp[2][2:end-1])                        # between 2 and 15 (fixed by rivnum)
  upland  =     parse(Float64,       temp[3])
  basnum  =     parse(  Int64,       temp[4][2:end-1])
  rivnum != mainum && error("\nERROR : $rivnum != $mainum\n\n")

  if upland >= ucut
    tail = @sprintf("%9.0f_%9d_%11d", upland * 10, rivnum, basnum) ; tail = replace(tail, ' ' => '0')
    if isfile("grp$tail.shp")
#     print("ogr2ogr -spat -139.0  47.0 -95.0 85.0 bas$tail.shp grp$tail.shp\n")
        run(`ogr2ogr -spat -139.0  47.0 -95.0 85.0 bas$tail.shp grp$tail.shp`)
      west = 0 ; isfile("bas$tail.shp") && (west = filesize("bas$tail.shp"))
      rm("bas$tail.dbf") ; rm("bas$tail.prj") ; rm("bas$tail.shp") ; rm("bas$tail.shx")
#     print("ogr2ogr -spat  -95.0 $lcut   0.0 85.0 bas$tail.shp grp$tail.shp\n")
        run(`ogr2ogr -spat  -95.0 $lcut   0.0 85.0 bas$tail.shp grp$tail.shp`)
      east = 0 ; isfile("bas$tail.shp") && (east = filesize("bas$tail.shp"))
      rm("bas$tail.dbf") ; rm("bas$tail.prj") ; rm("bas$tail.shp") ; rm("bas$tail.shx")
      if west > 200 || east > 200
        if !isfile("$rivo.shp")
          print("ogr2ogr                 $rivo.shp grp$tail.shp -nln $rivo\n")
            run(`ogr2ogr                 $rivo.shp grp$tail.shp -nln $rivo`)
        else
          print("ogr2ogr -update -append $rivo.shp grp$tail.shp -nln $rivo\n")
            run(`ogr2ogr -update -append $rivo.shp grp$tail.shp -nln $rivo`)
        end
        form = @sprintf("%s %s %3d\n", lins[a], "grp$tail.shp", rivcol)
        write(fpb, form)
#     else
#       print("omitting grp$tail.shp\n")
      end
#   else
#     print("skipping grp$tail.shp\n")
    end
  end
end
close(fpb)
exit(0)
