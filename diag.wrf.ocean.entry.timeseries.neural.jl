#=
 = Train a neural network to better match the normalized HYDAT and WRF-Hydro
 = streamflow timeseries, treating all available stations like one catchment,
 = except where lagged streamflow needs to be from the same station.  Short
 = (2*LAGS+1) timeseries are employed to create neural network inputs.  Omit
 = data prior to an intermediate date and any missing values - RD Oct,Nov 2023.
 =#

using My, Printf, NetCDF, Flux, Statistics, ProgressMeter, JLD2

const LAGS             = 48                             # number of 3-h streamflow values before and after
const FEAT             = 2                              # number of features (neural network inputs)
const MISS             = -9999.0                        # generic missing value

if (argc = length(ARGS)) != 2 && (argc = length(ARGS)) != 3
  print("\nUsage: jjj $(basename(@__FILE__)) 2019-05-01 pest_nwm_DOMAIN_AuxF.FORCING.era_2019-03-31_000214.remest.prep [identity]\n\n")
  exit(1)
end
acti = argc == 2 ? "identity" : ARGS[3]
cutt = ARGS[1] * "-00"
fila = ARGS[2] * ".nc"
filb = ARGS[2] * ".txt"
filc = ARGS[2] * ".val"
fild = ARGS[2] * ".nnet.$acti.nc"
file = ARGS[2] * ".nnet.$acti.txt"
filf = ARGS[2] * ".nnet.$acti.val"
filg = ARGS[2] * ".nnet.$acti.jld"
@printf("\ncopying %s %s\n", fila, fild) ; cp(fila, fild; force = true)
@printf(  "copying %s %s\n", filb, file) ; cp(filb, file; force = true)
@printf(  "copying %s %s\n", filc, filf) ; cp(filc, filf; force = true)
fpa  = My.ouvre(file, "r") ; line = readlines(fpa) ; close(fpa)
fpa  = My.ouvre(filf, "r") ; linf = readlines(fpa) ; close(fpa)

tims = ncread(fild, "time", start=[1],     count=[-1])                        # read the timeseries dates and
floh = ncread(fild, "hydt", start=[1,1],   count=[-1,-1])                     # two streamflow estimates
flow = ncread(fild, "flow", start=[1,1,1], count=[-1,-1,-1])
npes, nsta, ntim = size(flow)
pind = parse(Int64, split(line[nsta+2])[4])
@printf("with %d times %d stations %d PEST iterations of which %d is optimal\n\n", ntim, nsta, npes, pind)

cutd = datesous("1900-01-01-00", cutt, "hr")
cuti = findfirst(isequal(cutd), tims)
@printf("omitting 1:%d of 1:%d timeseries\n", cuti, ntim)

mskh = falses(      nsta, ntim)                                               # define masks where HYDAT observations
mskw = falses(npes, nsta, ntim)                                               # and WRF-Hydro model are (both) valid
mask = falses(npes, nsta, ntim)
for a = 1:ntim, b = 1:nsta
  floh[b,a]   > MISS &&                                    (mskh[  b,a] = true)
  for c = 1:npes
                        flow[c,b,a] > MISS &&              (mskw[c,b,a] = true)
    floh[b,a] > MISS && flow[c,b,a] > MISS && a >= cuti && (mask[c,b,a] = true)
  end
  a == ntim && @printf("station %3d has %5d %5d %5d valid data (HYDAT / PEST-optimal WRF-Hydro / both)\n",
    b, length(mskh[b,mskh[b,:]]), length(mskw[pind,b,mskw[pind,b,:]]), length(mask[pind,b,mask[pind,b,:]]))
end

xxtt = Array{Float32}(undef,FEAT,0)                                           # populate the un/calibrated matrices
yytt = Array{Float32}(undef,FEAT,0)                                           # with normalized HYDAT and WRF-Hydro
mskk = Array{  Int64}(undef,     0)                                           # short timeseries (where all valid)
for a = 1+LAGS:ntim-LAGS, b = 1:nsta
  global xxtt, yytt, mskk
  if all(mskw[pind,b,a-LAGS:a+LAGS]) && mskh[b,a]
    xref = Float32(flow[pind,b,a])
    yref = Float32(floh[     b,a])
    xdev = Float32(flow[pind,b,a] - mean(flow[pind,b,a-LAGS:a+LAGS]))
    ydev = Float32(floh[     b,a] - mean(floh[     b,a-LAGS:a+LAGS][mskh[b,a-LAGS:a+LAGS]]))
    xxtt = [xxtt [xref, xdev]]
    yytt = [yytt [yref, ydev]]
    push!(mskk, b)
  end
end
for a = 1:nsta
  @printf("station %3d has %5d training collocations\n", a, length(mskk[mskk .== a]))
end

acti == "identity" && (model = Chain(Dense(FEAT => FEAT+1),                               BatchNorm(3), Dense(FEAT+1 => FEAT)))
acti != "identity" && (model = Chain(Dense(FEAT => FEAT+1, getfield(Main, Symbol(acti))), BatchNorm(3), Dense(FEAT+1 => FEAT, getfield(Main, Symbol(acti)))))
optim = Flux.setup(Flux.Adam(0.01), model)

losses = []                                                                   # evaluate model and loss inside gradient context
@showprogress for epoch in 1:1000                                             # and record losses outside gradient context
  loss, grads = Flux.withgradient(model) do m
    yhat = m(xxtt)
    Flux.mse(yhat, yytt)
  end
  Flux.update!(optim, model, grads[1])
  push!(losses, loss)
end
@printf("first   loss %15.8f\n last-1 loss %15.8f\n last   loss %15.8f\n", losses[1], losses[end-1], losses[end])

@printf("writing full flows back to %s\n", fild)                              # replace PEST-optimal WRF-Hydro
temp = zeros(nsta, ntim)                                                      # with neural network estimates
for a = 1+LAGS:ntim-LAGS, b = 1:nsta
  if all(mskw[pind,b,a-LAGS:a+LAGS])
    xref      = Float32(flow[pind,b,a])
    xdev      = Float32(flow[pind,b,a] - mean(flow[pind,b,a-LAGS:a+LAGS]))
    temp[b,a] = Float64(model(reshape([xref,xdev],2,1))[1])
  end
end
for a = 1+LAGS:ntim-LAGS, b = 1:nsta
  if all(mskw[pind,b,a-LAGS:a+LAGS])
    flow[pind,b,a] = temp[b,a]
  end
end

@printf("writing  neural network to %s\n\n", filg)                            # and save the neural network
model_state = Flux.state(model) ; jldsave(filg; model_state)

for a = 1:nsta                                                                # finally rescale HYDAT and WRF-Hydro
  alp, bet = parse.(Float64, split(linf[a]))
                  floh[  a,mskh[  a,:]] = floh[  a,mskh[  a,:]] * bet .+ alp
  for b = 1:npes  flow[b,a,mskw[b,a,:]] = flow[b,a,mskw[b,a,:]] * bet .+ alp  end
end
ncwrite(floh, fild, "hydt", start=[1,1],   count=[-1,-1])
ncwrite(flow, fild, "flow", start=[1,1,1], count=[-1,-1,-1])
exit(0)

#=
#jldsave(filg, model_state = Flux.state(model), loss = losses[end])
model_state = JLD2.load(filg, "model_state")
acti == "identity" && (model = Chain(Dense(FEAT => FEAT+1),                               BatchNorm(3), Dense(FEAT+1 => FEAT)))
acti != "identity" && (model = Chain(Dense(FEAT => FEAT+1, getfield(Main, Symbol(acti))), BatchNorm(3), Dense(FEAT+1 => FEAT, getfield(Main, Symbol(acti)))))
Flux.loadmodel!(model, model_state) ; xref,xdev = 1,2 ; model(reshape([xref,xdev],2,1))

#   xxtt = [xxtt [xref]]
#   yytt = [yytt [yref]]
#   flow[pind,b,a] = Float64(model([xref])[1])

function loss(model, x, y)
  out = model(x)
  Flux.mse(out, y)
end
function train_model()
  dLdm, _, _      = gradient(loss, model, xxtt, yytt)
  @. model.weight = model.weight - STEPSIZE * dLdm.weight
  @. model.bias   = model.bias   - STEPSIZE * dLdm.bias
end
loss_init = Inf
while true
  global loss_init
  train_model()
  if loss_init == Inf
    loss_init = loss(model, xxtt, yytt)
    @printf("first loss %15.8f\n", loss_init)
    continue
  end
  if abs(loss_init - loss(model, xxtt, yytt)) < LOSSCRIT
    @printf(" last loss %15.8f %15.8f %15.8f\n", loss_init, loss(model, xxtt, yytt), abs(loss_init - loss(model, xxtt, yytt)))
    break
  else
    loss_init = loss(model, xxtt, yytt)
  end
end

zztt = model(xxtt)                                                            # identify a linear adjustment of the
avey =  mean(yytt[1,:]) ; vary = var(yytt[1,:])                               # neural network toward HYDAT reference
avez =  mean(zztt[1,:]) ; varz = var(zztt[1,:])
cvyz =   cov(yytt[1,:],              zztt[1,:])
beta = cvyz / vary ; alph = avez - beta * avey
@show alph, beta
alph = 0.0 ; beta = 1.0
=#
