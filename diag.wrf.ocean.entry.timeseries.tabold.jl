#=
 = Tabulate performance metrics of the optimal PEST experiment
 = for individual and total rivers - RD Oct 2023.
 =#

using My, Printf, NetCDF

const MISS             = -9999.0                        # generic missing value

if (argc = length(ARGS)) != 1
  print("\nUsage: jjj $(basename(@__FILE__)) pest_nwm_DOMAIN_East.FORCING.era_2019-03-31_000214.remest\n\n")
  exit(1)
end
fila = ARGS[1] * ".nc"
filb = ARGS[1] * ".txt"
filc = ARGS[1] * ".perform"

fpa  = My.ouvre(filb, "r") ; lina = readlines(fpa) ; close(fpa)               # read the station list, data dims,
tmpa = ncread(fila, "tmpa", start=[1,1], count=[-1,-1])                       # and optimal PEST performance index
npes, nsta = size(tmpa)
pest = parse(Int64, split(lina[nsta + 2])[4]) ; pest = 1
filc *= @sprintf(".%d", pest)

tarea  = 0.0
forma  = "  Station / River                &       Drainage         &  Obs &     Obs Flow    &    Model   &     NSE     & Pearson \\\\\n"
forma *= "                                 &       Area (km\$^2\$)    & Days & (m\$^3\$s\$^{-1}\$) &  Diff (\\%) &             &  Corr   \\\\\n  \\colrule\n"
goodn  = 0
for a = 1:nsta
  global tarea, forma, goodn
  vals = split(lina[a],",")                                                   # then get individual station metrics
  name = split(lina[a],"[")[2][1:end-1]                                       # (and sum individual drainage areas)
# fild = "../hydat.calibration/hydat_" * name[1:7] * ".sta"
  fild =                      "hydat_" * name[1:7] * ".sta"
  fpd  = My.ouvre(fild, "r") ; linc = readlines(fpd) ; close(fpd)
  lind = split(linc[30]) ; area = parse(Float64, lind[2]) ; areb = parse(Float64, lind[4]) ; tarea += area
  nsef = ncread(fila, "tmpa", start=[pest,a], count=[1,1])[1] ; (nsaa, nsbb) = nsef >= 0.50 ? ("{\\bf", "}") : ("    ", " ")
  nseg = ncread(fila, "tmpa", start=[   1,a], count=[1,1])[1] - nsef
  pcor = ncread(fila, "tmpb", start=[pest,a], count=[1,1])[1]
  dcor = ncread(fila, "tmpc", start=[pest,a], count=[1,1])[1]
  rmse = ncread(fila, "tmpd", start=[pest,a], count=[1,1])[1]
  bias = ncread(fila, "tmpe", start=[pest,a], count=[1,1])[1]
  numb = ncread(fila, "tmpf", start=[pest,a], count=[1,1])[1]
  tavg = ncread(fila, "tmpg", start=[pest,a], count=[1,1])[1] - bias
  rerr = ncread(fila, "tmph", start=[pest,a], count=[1,1])[1] ; (reaa, rebb) = rerr <= 15.0 ? ("{\\bf", "}") : ("    ", " ")
  nsef >= 0.50 && rerr <= 15.0 && (goodn += 1)
  forma *= @sprintf("  %7s / %-20s & %21.0f  & %4.0f & %15.0f & %s %4.0f%s & %s %5.2f%s & %5.2f   \\\\\n", name[1:7], name[9:end], area, numb, tavg, reaa, rerr, rebb, nsaa, nsef, nsbb, pcor)
end

nsef = ncread(fila, "parb", start=[pest,1], count=[1,1])[1] ; (nsaa, nsbb) = nsef >= 0.50 ? ("{\\bf", "}") : ("    ", " ")
nseg = ncread(fila, "parb", start=[   1,1], count=[1,1])[1] - nsef
pcor = ncread(fila, "parb", start=[pest,2], count=[1,1])[1]
dcor = ncread(fila, "parb", start=[pest,3], count=[1,1])[1]
rmse = ncread(fila, "parb", start=[pest,4], count=[1,1])[1]                   # and append total performance
bias = ncread(fila, "parb", start=[pest,5], count=[1,1])[1]
numb = ncread(fila, "parb", start=[pest,6], count=[1,1])[1]
tavg = ncread(fila, "parb", start=[pest,7], count=[1,1])[1] - bias
rerr = ncread(fila, "parb", start=[pest,8], count=[1,1])[1] ; (reaa, rebb) = rerr <= 15.0 ? ("{\\bf", "}") : ("    ", " ")
forma *= @sprintf("  %24s (%3d) & %21.0f  & %4.0f & %15.0f & %s %4.0f%s & %s %5.2f%s & %5.2f   \\\\\n", "All Rivers", goodn, tarea, numb, tavg, reaa, rerr, rebb, nsaa, nsef, nsbb, pcor)
fpa = My.ouvre(filc, "w") ; write(fpa, forma) ; close(fpa)
print(forma)
exit(0)
