* This script is designed to plot a watershed map.
* It can be executed using a command like
*
*     grads -blc "diag.hydroshed.watershed.entries Fulldom_hires_outlets"
*
* - RD April 2021.

function plot(args)
fila = subwrd(args,1)".ctl"
filb = subwrd(args,1)".txt"
filc = subwrd(args,1)".png"

"open "fila
"clear"
"set grid   off"
"set xlab   off" ; "set xlopts 1 3 0.18"
"set ylab   off" ; "set ylopts 1 3 0.18"
"set mpdraw off" ; "set yflip  on"

"set rgb 16 80 80 80"
"set gxout fgrid" ; "set fgvals 1 16" ; "set grads off" ; "d maskout(1,topo)"

*wcol = 2
filestat = read(filb)
while (sublin(filestat,1) = 0)
  line = sublin(filestat,2)
  wlev = subwrd(line,1)
  wlat = subwrd(line,4)
  wlon = subwrd(line,2)

  wcol = 15
  "set gxout fgrid" ; "set fgvals "wlev" "wcol ; "d floacc"
  "set gxout contour" ; "set cthick 4" ; "set clab off"
  "set clevs 0.5" ; "set ccolor 1" ; "set cstyle 1"
  "set grads off" ; "d const(maskout(maskout(1,floacc-"wlev"),"wlev"-floacc),0,-u)"
* "q gr2xy "wlon" "wlat ; rec = sublin(result,1) ; obsx = subwrd(rec,3) ; obsy = subwrd(rec,6)
* "set line 1 1 5" ; "draw mark 3 "obsx" "obsy" 0.05" ; "set line 1 1 5"

* wcol = wcol + 1 ; if (wcol = 16) ; wcol = 2 ; endif
  filestat = read(filb)
endwhile
filestat = close(filb)

"set gxout fgrid" ; "set fgvals 0 0" ; "set grads off" ; "d maskout(lake-lake,lake)"

"q gxinfo"
line3 = sublin(result,3)
line4 = sublin(result,4)
x1 = subwrd(line3,4)
x2 = subwrd(line3,6)
y1 = subwrd(line4,4)
y2 = subwrd(line4,6)
xmid = (x1 + x2) / 2.0
ymid = (y1 + y2) / 2.0
say x1" "x2" "y1" "y2" "xmid ;* 2 10.5 0.75 7.75 6.25
"set string 1 bc 5" ; "set strsiz 0.23"
"draw string "xmid" "y2+0.2" WRF-Hydro Watersheds and Rivers"

"set line 0 1 10"
"draw recf 0.65 4.12 5.65 7.63"
"set vpage 0.4 5.9 3.75 8.00"
"set grid   off"
"set xlab   off" ; "set xlopts 1 3 0.18"
"set ylab   off" ; "set ylopts 1 3 0.18"
"set mpdraw off" ; "set yflip  on"
"set x 1650 2000"
"set y 1100 1350"
"set rgb 16 80 80 80"
"set gxout fgrid" ; "set fgvals 1 16" ; "set grads off" ; "d maskout(1,topo)"

*wcol = 2
filestat = read(filb)
while (sublin(filestat,1) = 0)
  line = sublin(filestat,2)
  wlev = subwrd(line,1)
  wlat = subwrd(line,4)
  wlon = subwrd(line,2)

  wcol = 15
  "set gxout fgrid" ; "set fgvals "wlev" "wcol ; "d floacc"
  "set gxout contour" ; "set cthick 4" ; "set clab off"
  "set clevs 0.5" ; "set ccolor 1" ; "set cstyle 1"
  "set grads off" ; "d const(maskout(maskout(1,floacc-"wlev"),"wlev"-floacc),0,-u)"
* "q gr2xy "wlon" "wlat ; rec = sublin(result,1) ; obsx = subwrd(rec,3) ; obsy = subwrd(rec,6)
* "set line 1 1 5" ; "draw mark 3 "obsx" "obsy" 0.05" ; "set line 1 1 5"

* wcol = wcol + 1 ; if (wcol = 16) ; wcol = 2 ; endif
  filestat = read(filb)
endwhile
filestat = close(filb)

"set gxout fgrid" ; "set fgvals 0 0" ; "set grads off" ; "d maskout(lake-lake,lake)"
"set gxout fgrid" ; "set fgvals 0 4" ; "set grads off" ; "d maskout(riv,riv)"
"set vpage off"

say "gxprint "filc" png white x1100 y850"
    "gxprint "filc" png white x1100 y850"
"quit"


"set xlab off" ; "set ylab off"
"set mpdraw off" ; "set gxout grfill" ; "set yflip on"
"d maskout(topo-topo,topo)" ;* "d maskout(log10(floacc),floacc)"
"set ccols 4" ; "set clevs   1" ; "d maskout(riv,riv)"
*"set ccols 4" ; "set clevs 100" ; "d maskout(lake,lake)"
*"d riv"
*"d maskout(flodir,-1.5-flodir)"
"d maskout(floacc,floacc)"

#"d floacc"
"set gxout grid"
*"d maskout(floacc,floacc/100-2)"
*"d maskout(log10(floacc),floacc)"
"set gxout grfill"
#"d floacc"
#"d topo"
#"d retdeprtfac"
#"d ovroughrtfac"
#"d frxst"
#"d basnmsk"
#"d lake"
#"d landuse"
#"d lksatfac"
#"d clat"
#"d clon"
#"d riv"

#      "set xlab off" ; "set ylab off" ; "set mpdraw off" ; "set gxout grfill" ; "set yflip on"
#      "d maskout(topo-topo,topo)" ;* "d maskout(log10(floacc),floacc)"
#      "set ccols 1" ; "set clevs   1" ; "d maskout(riv,riv)"
#      "set ccols 4" ; "set clevs 100" ; "d maskout(lake,lake)"
