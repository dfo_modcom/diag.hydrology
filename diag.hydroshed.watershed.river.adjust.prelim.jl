#=
 = Burn the 15-s HydroSHEDS watershed boundaries into a 2-km WRF-Hydro DEM
 = ensuring that the WRF-Hydro values are slightly higher along HydroSHEDS
 = boundaries, but avoid raising the WRF-Hydro DEM at the lowest elevations
 = of each watershed, where this drains to the ocean.  We ensure that ocean
 = points are identified in the NetCDF output (fild) because we reuse this
 = grid as input to Build_Routing_Stack.py - RD Dec 2023.
 =#

using My, Printf, NetCDF, Shapefile

const BUFF             = 12                             # WRF-Hydro routing grid boundary buffer
const FDIS             = 2.0                            # WRF-Hydro routing grid     resolution (km)
const DEL              = 0.001                          # WRF-Hydro routing grid sub-resolution (degrees)
const D2R              = 3.141592654 / 180.0            # degrees to radians conversion
const DELMISS          = -8e9                           # generic missing value comparison
const GRDMISS          = -9e9                           # generic missing value on a grid
const MISS             = -9999.0                        # generic missing value
const MIST             = @sprintf("%d", Int64(MISS))    # generic missing value string

if (argc = length(ARGS)) != 3
  print("\nUsage: jjj $(basename(@__FILE__)) Fulldom_hires.nc ../../../HYDROSHEDS/HydroRIVERS_v10_30_85N_180_00W ../../../merit/merit_hydrodem_hires_prelim.nc\n\n")
  exit(1)
end
fila = ARGS[1]
filb = ARGS[2] * "_wsheds.shp" ; hytab = Shapefile.Table(filb) ; hylen = length(hytab) ; hygeo = Shapefile.shapes(hytab)
filc = ARGS[2] * "_rivers.shp" ; rvtab = Shapefile.Table(filc) ; rvlen = length(rvtab) ; rvgeo = Shapefile.shapes(rvtab)
fild = ARGS[3]
file = ARGS[3][1:end-3] * "_adjust_pour.txt"
filf = ARGS[3][1:end-3] * "_adjust_bndy.txt"
filg = ARGS[3][1:end-3] * "_adjust_rivr.txt"

@printf("\nusing %s as a template for %s\n", fila, fild)                      # create a template for the adjusted DEM
run(`gdal_translate NETCDF:"$fila":TOPOGRAPHY $fila.bil`)                     # and specify the value of ocean points
run(`gdal_translate                           $fila.bil $fila.bil.nc`)
run(`rm                                       $fila.bil $fila.bil.aux.xml $fila.hdr $fila.prj`)
run(`mv                                                 $fila.bil.nc      $fild`)
ncputatt(fild, "TOPOGRAPHY", Dict{Any,Any}("long_name"=>"GDAL Band Number 1", "_FillValue"=>MISS, "esri_pe_string"=>"PROJCS[unnamed,GEOGCS[GCS_Sphere,DATUM[D_Sphere,SPHEROID[unnamed,6370000.0,0.0]],PRIMEM[Greenwich,0.0],UNIT[Degree,0.0174532925199433]],PROJECTION[Lambert_Conformal_Conic],PARAMETER[False_Easting,0.0],PARAMETER[False_Northing,0.0],PARAMETER[Central_Meridian,-90.0],PARAMETER[Standard_Parallel_1,59.0],PARAMETER[Standard_Parallel_2,59.0],PARAMETER[Latitude_Of_Origin,59.0000038146973],UNIT[Meter,1.0]]", "grid_mapping"=>"lambert_conformal_conic"))

lats = ncread(fila,   "LATITUDE", start=[1,1], count=[-1,-1])[:,end:-1:1]     # read the 15-s HydroSHEDS watershed
lons = ncread(fila,  "LONGITUDE", start=[1,1], count=[-1,-1])[:,end:-1:1]     # boundaries and WRF-Hydro elevation
levs = ncread(fila, "TOPOGRAPHY", start=[1,1], count=[-1,-1])[:,end:-1:1]     # (and flip latitude)
lata = minimum(lats) ; latb = maximum(lats)
lona = minimum(lons) ; lonb = maximum(lons)
(nlon, nlat) = size(levs)

function hydroburn()                                                          # burn the watershed boundaries into
  pour = Set{Tuple{Int32, Int32}}()                                           # the WRF-Hydro DEM; first identify all
  bndy = Set{Tuple{Int32, Int32}}()                                           # ocean pour points, drainage boundaries,
  rivr = Set{Tuple{Int32, Int32}}()                                           # and interior rivers on the DEM grid

  function telescope(rlat::Float64, rlon::Float64)                            # telescope through the WRF-Hydro grid
    mindis = minlat = minlon = 9e9                                            # to get a location closest to a lake
    minlin = div(nlat, 2)                                                     # shapefile point
    minpix = div(nlon, 2)
    for a = 25:-1:0
      for b = minlin - 2^a : 2^a : minlin + 2^a
        if 1 <= b <= nlat
          for c = minpix - 2^a : 2^a : minpix + 2^a
            if 1 <= c <= nlon
              gdlatitude  = lats[c,b]
              gdlongitude = lons[c,b]
              tmpdis = (rlat - gdlatitude)^2 + (rlon - gdlongitude)^2
              if tmpdis < mindis
                mindis = tmpdis
                minlat = gdlatitude
                minlon = gdlongitude
                minlin = b
                minpix = c
#               @printf("a b c are (%8d %8d %8d) and lat lon mindis are %lf %lf %e ***\n", a, b, c, gdlatitude, gdlongitude, mindis)
#             else
#               @printf("a b c are (%8d %8d %8d) and lat lon tmpdis are %lf %lf %e\n", a, b, c, gdlatitude, gdlongitude, tmpdis)
              end
            end
          end
        end
      end
    end
#   if 111 * mindis^0.5 > 10 * FDIS
#     print("\nrlat rlon are $rlat $rlon\n")
#     print("minlat minlon are $minlat $minlon at line pixel $minlin $minpix\n")
#     print("allowable distance between them is about $(10 * FDIS) km\n")
#     print("actual    distance between them is about $(111 * mindis^0.5) km\n")
#     exit(0)
#   end
    if BUFF < minpix <= nlon-BUFF && BUFF < minlin <= nlat-BUFF
      return(minpix, minlin)
    else
      return(0)
    end
  end

  if isfile(file)                                                             # write WRF-Hydro coastal points
    @printf("reading %s\n", file)                                             # (then can read quickly)
    for line in eachline(file)
      lonind, latind = parse.(Int32, split(line))
      push!(pour, (lonind, latind))
    end
  else
    @printf("writing %s\n", file)
    for a = 2:nlat-1, b = 2:nlon-1
      if levs[b,a] >= 0 && (levs[b-1,a] < 0 || levs[b+1,a] < 0 || levs[b,a-1] < 0 || levs[b,a+1] < 0)
        push!(pour, (b, a))
      end
    end
    fpa = My.ouvre(file, "w")
    for (lonind, latind) in pour
      form = @sprintf("%6d %5d\n", lonind, latind) ; write(fpa, form)
    end
    close(fpa)
  end

  if isfile(filf)                                                             # write watershed boundaries
    @printf("reading %s\n", filf)                                             # (then can read quickly)
    for line in eachline(filf)
      lonind, latind = parse.(Int32, split(line))
      push!(bndy, (lonind, latind))
    end
  else
    @printf("writing %s\n", filf)
    for b = 1:hylen
      hypts = hygeo[b].points
      for c = 1:length(hypts)
        if lata < hypts[c].y < latb && lona < hypts[c].x < lonb
          vals = telescope(hypts[c].y, hypts[c].x)
          vals != 0 && push!(bndy, vals)
        end
      end
    end
    fpa = My.ouvre(filf, "w")
    for (lonind, latind) in bndy
      form = @sprintf("%6d %5d\n", lonind, latind) ; write(fpa, form)
    end
    close(fpa)
  end

  if isfile(filg)                                                             # write HydroSHEDS large rivers
    @printf("reading %s\n", filg)                                             # (then can read quickly)
    for line in eachline(filg)
      lonind, latind = parse.(Int32, split(line))
      push!(rivr, (lonind, latind))
    end
  else
    @printf("writing %s\n", filg)
    for b = 1:rvlen
      rvpts = rvgeo[b].points
      for c = 1:length(rvpts)
        if lata < rvpts[c].y < latb && lona < rvpts[c].x < lonb
          vals = telescope(rvpts[c].y, rvpts[c].x)
          vals != 0 && push!(rivr, vals)
        end
      end
    end
    fpa = My.ouvre(filg, "w")
    for (lonind, latind) in rivr
      form = @sprintf("%6d %5d\n", lonind, latind) ; write(fpa, form)
    end
    close(fpa)
  end
  @printf("%9d gridboxes define the coastline of %s\n",                    length(pour), fild)
  @printf("%9d gridboxes define the %d HydroSHEDS watershed boundaries\n", length(bndy), hylen)
  @printf("%9d gridboxes define the %d HydroSHEDS large river networks\n", length(rivr), rvlen)

  mask = zeros(Int16, nlon, nlat)                                             # make no adjustments near shore
  for (b, a) in pour  mask[b,a]  = -1  end                                    # or along HydroSHEDS rivers, but
  e = 2                                                                       # only where watershed boundaries
  for a = 1+e:nlat-e, b = 1+e:nlon-e                                          # are isolated from these (mask=2)
    if mask[b,a] == -1
      for c = -e:e, d = -e:e
        mask[b+d,a+c] == 0 && (mask[b+d,a+c] = -2)
      end
    end
  end
  for a = 1:nlat, b = 1:nlon  mask[b,a] == -2 && (mask[b,a] = -1)  end        # here, shore=-1, wshed=2, river=5
  for (b, a) in bndy  mask[b,a] +=  2  end                                    # and mask=1,4,6,7 are combinations
  for (b, a) in rivr  mask[b,a] +=  5  end

  clos = zeros(Int16, nlon, nlat)                                             # close the diagonal bndy openings
  for (b, a) in bndy  clos[b,a]  =  1  end
  for a = 2:nlat-1, b = 2:nlon-1
    clos[b,a] == 1 && clos[b-1,a-1] == 1 && clos[b,a-1] == 0 && clos[b-1,a] == 0 && (clos[b,a-1] = clos[b-1,a] = 2)
    clos[b,a] == 1 && clos[b-1,a+1] == 1 && clos[b,a+1] == 0 && clos[b-1,a] == 0 && (clos[b,a+1] = clos[b-1,a] = 2)
    clos[b,a] == 1 && clos[b+1,a-1] == 1 && clos[b,a-1] == 0 && clos[b+1,a] == 0 && (clos[b,a-1] = clos[b+1,a] = 2)
    clos[b,a] == 1 && clos[b+1,a+1] == 1 && clos[b,a+1] == 0 && clos[b+1,a] == 0 && (clos[b,a+1] = clos[b+1,a] = 2)
  end
  for a = 1:nlat, b = 1:nlon
    clos[b,a] == 2 && (mask[b,a] +=  2)
  end

  for a = 1:nlat, b = 1:nlon
#   levs[b,a] += mask[b,a]
    mask[b,a] == 2 && (levs[b,a] += 50)
#   clos[b,a] == 2 && (levs[b,a] += 50)
  end
  levs[198,208] += 50
  levs[199,207] += 50
  levs[200,207] += 50
  levs[205,207] += 50
  levs[206,207] += 50
  levs[265,136] += 50
  levs[264,135] += 50
  ncwrite(levs, fild, "TOPOGRAPHY", start=[1,1], count=[-1,-1])               # (no need to flip latitude here)
end

hydroburn()
exit(0)

#=
  alin = ""                                                                   # but only for points inside this grid
  for a = nlat-BUFF:-BUFF:BUFF  alin *= @sprintf("%14.9f %13.9f, ", lons[BUFF,a],      lats[BUFF,a])       end
  for a = BUFF:BUFF:nlon-BUFF   alin *= @sprintf("%14.9f %13.9f, ", lons[a,BUFF],      lats[a,BUFF])       end
  for a = BUFF:BUFF:nlat-BUFF   alin *= @sprintf("%14.9f %13.9f, ", lons[nlon-BUFF,a], lats[nlon-BUFF,a])  end
  for a = nlon-BUFF:-BUFF:BUFF  alin *= @sprintf("%14.9f %13.9f, ", lons[a,nlat-BUFF], lats[a,nlat-BUFF])  end
  abox = readgeom("POLYGON(($(alin[1:end-2])))")
                       llpt =       readgeom("POINT($(rvpts[c].x) $(rvpts[c].y))")
        contains(abox, llpt) && push!(rivr, telescope(rvpts[c].y,   rvpts[c].x))

  for (b, a) in bndy                                                          # then increase elevation by 1%
    elev[b,a] = 1                                                             # at the watershed boundaries and
  end                                                                         # 0.5% at two adjacent gridboxes
  for a = 3:nlat-2, b = 3:nlon-2
    if mask[b,a] > 0
      for c = -2:2, d = -2:2
        mask[b+d,a+c] == 0 && (mask[b+d,a+c] = -1)
      end
    end
  end

  for a = 2:nlat-1, b = 2:nlon-1                                              # promote from 0.5% to 1% increase
    if mask[b,a] < 0                                                          # where any increase is surrounded
      numb = 0                                                                # (to cross watershed boundary gaps)
      for c = -1:1, d = -1:1
        mask[b+d,a+c] != 0 && (numb += 1)
      end
      numb == 9 && (mask[b,a] = 1)
    end
  end

  for (b, a) in rivr
    mask[b,a]  = 1
  end
  for (b, a) in pour                                                          # but keep elevation unadjusted
    mask[b,a] += 2                                                            # near any calibration stations
  end

  e = 1
  for a = 1+e:nlat-e, b = 1+e:nlon-e
    if mask[b,a] > 1
      for c = -e:e, d = -e:e
        mask[b+d,a+c] < 2 && (mask[b+d,a+c] += 2)
      end
    end
  end

  for (b, a) in rivr                                                          # or along interior rivers
    if a != 1 && a != nlat && b != 1 && b != nlon
      for c = -1:1, d = -1:1
        mask[b+d,a+c] = 0
      end
    end
  end

# for a = 2:nlat-1, b = 2:nlon-1
#   if mask[b,a] == 10
#     for c = -1:1, d = -1:1
#       mask[b+d,a+c] = 0
#     end
#   end
# end

  for a = 1:nlat, b = 1:nlon
    levs[b,a] += mask[b,a]
#   if levs[b,a] > 50
#     mask[b,a] == 1 && (levs[b,a] +=  50)
#     mask[b,a] == 2 && (levs[b,a] += 100)
#     mask[b,a] == 3 && (levs[b,a] += 150)
#     mask[b,a] > 0 && (levs[b,a] += round(Int16, levs[b,a] / 10))
#     mask[b,a] < 0 && (levs[b,a] += round(Int16, levs[b,a] / 20))
#   end
  end

# a = 3249 ; for b = 12541:12546  levs[b,a] += 100  end                       # block at Aux Feuilles
# a = 3250 ; for b = 12542:12547  levs[b,a] += 100  end
# b, a = 14664, 2797                                                          # block at Eagle River
# for c = -5:5, d = -5:5
#          abs(c) < 2 &&        abs(d) < 2   && (levs[b+d,a+c] = 400)
#   mask[b+d,a+c] > 0 && levs[b+d,a+c] < 400 && (levs[b+d,a+c] = 400)
# end
  ncwrite(levs[:,end:-1:1], fild, "TOPOGRAPHY", start=[1,1], count=[-1,-1])
end
=#
