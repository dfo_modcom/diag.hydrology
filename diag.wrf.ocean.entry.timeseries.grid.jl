#=
 = Extract a streamflow timeseries for each river outlet from WRF Hydro output files
 = and save these as a netcdf file that varies in time and latitude, with each outlet
 = on a different proxy latitude (true outlet positions, as well as ocean pour points
 = that are slightly displaced into the ocean, are stored as time-invariant variables).
 = If there is difficulty writing data in one complete pass, stations are written at
 = each timestep and a date file is unpdated so the script can restart where it left
 = off - RD Apr 2020, May 2021, Feb 2023, Feb 2024.
 =#

using My, Printf, NetCDF

const DELT             = 3.0                            # timestep (hours)
const MISS             = -9999.0                        # generic missing value

if (argc = length(ARGS)) != 2
  print("\nUsage: jjj $(basename(@__FILE__)) file_name_firstdate_lastdate positions.txt\n")
  print(  "e.g.,  jjj $(basename(@__FILE__)) run_DOMAIN_full_full_FORCING.wrf_1990-01-01_2099-12-31 ../../DOMAIN_full_full/Fulldom_hires_outlets.txt\n\n")
  exit(1)
end
fila = @sprintf("%s_%dh.nc",       ARGS[1], DELT)
filb = @sprintf("%s_%dh.txt",      ARGS[1], DELT) ; cp(ARGS[2], filb; force = true)
filc = @sprintf("%s_%dh.lastdate", ARGS[1], DELT)

temp = split(ARGS[1], "_")
dbeg = temp[end-1][1:4] * temp[end-1][6:7] * temp[end-1][9:10] * @sprintf("%2d",      DELT) ; dbeg = replace(dbeg, ' ' => '0')
dend = temp[end  ][1:4] * temp[end  ][6:7] * temp[end  ][9:10] * @sprintf("%2d", 24 - DELT) ; dend = replace(dend, ' ' => '0')
dnum = datesous(dbeg, dend, "hr") / DELT + 1

xind = Array{  Int64}(undef,0) ; rlon = Array{Float64}(undef,0) ; olon = Array{Float64}(undef,0)
yind = Array{  Int64}(undef,0) ; rlat = Array{Float64}(undef,0) ; olat = Array{Float64}(undef,0)
tims = Array{Float64}(undef,0)

fpa = My.ouvre(filb, "r") ; lins = readlines(fpa) ; close(fpa)                # read river outlet locations (immediately
nlet = length(lins)                                                           # upriver of the corresponding ocean entries)
for a = 1:nlet
  tmpa = split(lins[a])
  push!(xind, parse(  Int64, tmpa[2]))
  push!(yind, parse(  Int64, tmpa[3]))
  push!(rlat, parse(Float64, tmpa[5]))
  push!(rlon, parse(Float64, tmpa[6]))
  push!(olat, parse(Float64, tmpa[10]))
  push!(olon, parse(Float64, tmpa[11]))
end

dnow = dbeg                                                                   # construct the times and netcdf template
for a = 1:dnum
  global dnow
  time = My.datesous("1900010100", dnow, "hr")
  push!(tims, time)
  dnow = My.dateadd(dnow, DELT, "hr")
end

function nccreate(fn::AbstractString, ntim::Int, nlat::Int, nlon::Int, missing::Float64)
  nctim = NcDim("time", ntim, atts = Dict{Any,Any}("units"=>"hours since 1900-01-01 00:00:0.0"), values = collect(range(    0, stop =                  ntim - 1 , length = ntim)))
  nclat = NcDim( "lat", nlat, atts = Dict{Any,Any}("units"=>                   "degrees_north"), values = collect(range( 50.0, stop =  50.0 + 0.001 * (nlat - 1), length = nlat)))
  nclon = NcDim( "lon", nlon, atts = Dict{Any,Any}("units"=>                    "degrees_east"), values = collect(range(280.0, stop = 280.0 + 0.001 * (nlon - 1), length = nlon)))
  ncvrs = Array{NetCDF.NcVar}(undef, 7)
  ncvrs[1] = NcVar("rivgridx", [nclon, nclat       ], atts = Dict{Any,Any}("units"=>"none",   "long_name"=>"river outlet x-index on WRF-Hydro grid", "missing_value"=>missing), t=  Int64, compress=-1)
  ncvrs[2] = NcVar("rivgridy", [nclon, nclat       ], atts = Dict{Any,Any}("units"=>"none",   "long_name"=>"river outlet y-index on WRF-Hydro grid", "missing_value"=>missing), t=  Int64, compress=-1)
  ncvrs[3] = NcVar("riverlat", [nclon, nclat       ], atts = Dict{Any,Any}("units"=>"degree", "long_name"=>"river outlet latitude",                  "missing_value"=>missing), t=Float64, compress=-1)
  ncvrs[4] = NcVar("riverlon", [nclon, nclat       ], atts = Dict{Any,Any}("units"=>"degree", "long_name"=>"river outlet longitude",                 "missing_value"=>missing), t=Float64, compress=-1)
  ncvrs[5] = NcVar("oceanlat", [nclon, nclat       ], atts = Dict{Any,Any}("units"=>"degree", "long_name"=>"ocean pour point latitude",              "missing_value"=>missing), t=Float64, compress=-1)
  ncvrs[6] = NcVar("oceanlon", [nclon, nclat       ], atts = Dict{Any,Any}("units"=>"degree", "long_name"=>"ocean pour point longitude",             "missing_value"=>missing), t=Float64, compress=-1)
  ncvrs[7] = NcVar("riverflo", [nclon, nclat, nctim], atts = Dict{Any,Any}("units"=>"m3 s-1", "long_name"=>"river outlet streamflow",                "missing_value"=>missing), t=Float64, compress=-1)
  descript = "WRF-Hydro streamflow timeseries for a single oceanic river outlet on each proxy latitude, where true outlet positions (riverlat/lon) " *
             "as well as ocean pour points that are slightly displaced into the ocean (oceanlat/lon), are given as corresponding time-invariants"
  ncfil = NetCDF.create(fn, ncvrs, gatts = Dict{Any,Any}("Description"=>descript), mode = NC_NETCDF4)
  print("\ncreated $fn with $ntim times $nlat lats and $nlon lons\n\n")
end

if !isfile(fila)                                                              # either create the NetCDF template
  nccreate(     fila, length(tims), nlet, 1, MISS)                            # or take note of what was written
  ncwrite(tims, fila,     "time", start=[1],   count=[-1])                    # to this file so far
  ncwrite(xind, fila, "rivgridx", start=[1,1], count=[-1,-1])
  ncwrite(yind, fila, "rivgridy", start=[1,1], count=[-1,-1])
  ncwrite(rlat, fila, "riverlat", start=[1,1], count=[-1,-1])
  ncwrite(rlon, fila, "riverlon", start=[1,1], count=[-1,-1])
  ncwrite(olat, fila, "oceanlat", start=[1,1], count=[-1,-1])
  ncwrite(olon, fila, "oceanlon", start=[1,1], count=[-1,-1])
  dnow = dbeg
  nbeg = 1
else
  line = split(readline(filc))
  dnow =                line[1]
  nbeg = parse(Float64, line[2])
end

function ncseries()                                                           # then construct the timeseries
  global dnow
  for a = nbeg:dnum
    vals = MISS * ones(nlet)
    dnam = "dat/" * dnow * "00.CHRTOUT_GRID1"
    if isfile(dnam)
      print("reading grid for $dnam\n")
      grid = ncread(dnam, "streamflow", start=[1,1,1], count=[-1,-1,-1])
      for b = 1:nlet
        vals[b] = grid[xind[b],yind[b],1]
      end
    else
      print("missing grid for $dnam\n")
      exit(0)
    end
    ncwrite(vals, fila, "riverflo", start=[1,1,a], count=[-1,-1,1])
    form = dnow * " $a"
    fpc  = My.ouvre(filc, "w", false) ; write(fpc, form) ; close(fpc)
    dnow = My.dateadd(dnow, DELT, "hr")
  end
end
ncseries()

print("\n")
exit(0)
