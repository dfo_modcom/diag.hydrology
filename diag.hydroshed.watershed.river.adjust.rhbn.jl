#=
 = Burn the "hyshed" (15-s HydroSHEDS) calibration-station drainage boundaries into
 = the 30-s MERIT DEM by ensuring that the MERIT values are slightly higher along the
 = hyshed boundaries, but avoid raising the MERIT DEM near the station itself (i.e.,
 = at the lowest elevations of each drainage basin) or along any HydroSHEDS rivers
 = interior to the drainage basin - RD Jun 2023.
 =#

using My, Printf, NetCDF, Shapefile

const MATS             =  6600                          # number of latitudes   in MERIT grid
const MONS             = 21600                          # number of longitudes  in MERIT grid
const DELL             =    0.0083333333333333333       # lat/lon grid spacing
const LATA             =   30.0041666666666666667       # first latitude on grid
const LONA             = -179.9958333333333333333       # first longitude on grid

const D2R              = 3.141592654 / 180.0            # degrees to radians conversion
const DELMISS          = -8e9                           # generic missing value comparison
const GRDMISS          = -9e9                           # generic missing value on a grid
const MISS             = -9999.0                        # generic missing value
const MIST             = @sprintf("%d", Int64(MISS))    # generic missing value string

if (argc = length(ARGS)) != 2
  print("\nUsage: jjj $(basename(@__FILE__)) merit_hydrodem_30_85N_180_00W_30sec_elevtn.nc ../hydat.calibration/\n\n")
  exit(1)
end
fila = ARGS[1]
filb = ARGS[1][1:end-3] * "_rhbn.nc" ; cp(fila, filb; force=true, follow_symlinks=true)

mlat = ncread(filb,   "lat", start=[1],   count=[-1])                         # read the 15-s HydroSHEDS watershed
mlon = ncread(filb,   "lon", start=[1],   count=[-1])                         # boundaries and the MERIT elevation
mlev = ncread(filb, "Band1", start=[1,1], count=[-1,-1])
mask = zeros(Int16, MONS, MATS)

function hydroburn()                                                          # burn the drainage boundaries into
  rhbn = Set{Tuple{Int32, Int32}}()                                           # the MERIT DEM; first identify all
  bndy = Set{Tuple{Int32, Int32}}()                                           # RHBN stations, drainage boundaries,
  rivr = Set{Tuple{Int32, Int32}}()                                           # and interior rivers on the MERIT grid
  files = readdir(ARGS[2])
  for file in files
    if isfile(ARGS[2] * file) && endswith(file, ".sta")
      fpa   = My.ouvre(ARGS[2] * file, "r")
      lins  = readlines(fpa, keep = true) ; close(fpa)
      hlat  = parse(Float64, split(lins[21])[2])
      hlon  = parse(Float64, split(lins[22])[2])
      latmin, latind = findmin(abs.(hlat .- mlat))
      lonmin, lonind = findmin(abs.(hlon .- mlon))
      push!(rhbn, (lonind, latind))
    end

    if isfile(ARGS[2] * file) && endswith(file, ".sta.hyshed")
      fpa  = My.ouvre(ARGS[2] * file, "r")
      lins = readlines(fpa, keep = true) ; close(fpa)
      for b = 2:length(lins)
        hlat = parse(Float64, split(lins[b])[1])
        hlon = parse(Float64, split(lins[b])[2])
        latmin, latind = findmin(abs.(hlat .- mlat))
        lonmin, lonind = findmin(abs.(hlon .- mlon))
        push!(bndy, (lonind, latind))
      end
    end

    if isdir(ARGS[2] * file) && endswith(file, ".shp")
      rshp = ARGS[2] * file * "/" * file[1:end-4] * "_hyrivr.shp"
      rtab = Shapefile.Table(rshp) ; rlen = length(rtab) ; rgeo = Shapefile.shapes(rtab)
      for b = 1:rlen
        rpts = rgeo[b].points
        for c = 1:length(rpts)
          (hlat, hlon) = (rpts[c].y, rpts[c].x)
          latmin, latind = findmin(abs.(hlat .- mlat))
          lonmin, lonind = findmin(abs.(hlon .- mlon))
          push!(rivr, (lonind, latind))
        end
      end
    end
  end
  @printf("\n%9d MERIT gridboxes define the RHBN station locations\n",         length(rhbn))
  @printf(  "%9d MERIT gridboxes define the HydroSHEDS drainage boundaries\n", length(bndy))
  @printf(  "%9d MERIT gridboxes define the interior river network\n",         length(rivr))

  for (b, a) in bndy                                                          # then increase elevation by 1%
    mask[b,a] = 1                                                             # at the watershed boundaries and
  end                                                                         # 0.5% at two adjacent gridboxes
  for a = 3:MATS-2, b = 3:MONS-2
    if mask[b,a] > 0
      for c = -2:2, d = -2:2
        mask[b+d,a+c] == 0 && (mask[b+d,a+c] = -1)
      end
    end
  end

  for a = 2:MATS-1, b = 2:MONS-1                                              # promote from 0.5% to 1% increase
    if mask[b,a] < 0                                                          # where any increase is surrounded
      numb = 0                                                                # (to cross watershed boundary gaps)
      for c = -1:1, d = -1:1
        mask[b+d,a+c] != 0 && (numb += 1)
      end
      numb == 9 && (mask[b,a] = 1)
    end
  end

  for (b, a) in rhbn                                                          # but keep elevation unadjusted
    mask[b,a] = 10                                                            # near any calibration stations
  end
  for a = 6:MATS-5, b = 6:MONS-5
    if mask[b,a] == 10
      for c = -5:5, d = -5:5
        mask[b+d,a+c] = 0
      end
    end
  end

  for (b, a) in rivr                                                          # or along interior rivers
    mask[b,a] = 0
  end
# for a = 2:MATS-1, b = 2:MONS-1
#   if mask[b,a] == 10
#     for c = -1:1, d = -1:1
#       mask[b+d,a+c] = 0
#     end
#   end
# end

  for a = 1:MATS, b = 1:MONS
#   if mlev[b,a] > 50
      mask[b,a] > 0 && (mlev[b,a] += 100)
#     mask[b,a] < 0 && (mlev[b,a] += 25)
#     mask[b,a] > 0 && (mlev[b,a] += round(Int16, mlev[b,a] / 10))
#     mask[b,a] < 0 && (mlev[b,a] += round(Int16, mlev[b,a] / 20))
#   end
  end

  b, a = 14664, 2797                                                          # block at Eagle River
  for c = -5:5, d = -5:5
           abs(c) < 2 &&        abs(d) < 2   && (mlev[b+d,a+c] = 400)
    mask[b+d,a+c] > 0 && mlev[b+d,a+c] < 400 && (mlev[b+d,a+c] = 400)
  end
  ncwrite(mlev, filb, "Band1", start=[1,1], count=[-1,-1])
end

hydroburn()
exit(0)
