#=
 = Create an additive and multiplicative normalization of streamflow, and its
 = three normalized features (i.e., a centered streamflow, and its 30-day and
 = decadal-mean-daily running averages), for station-specific (regulated) neural
 = networks.  The output file contains features that depend on WRF-Hydro streamflow
 = and only its missing values, whereas an optional training output file contains
 = features that depend on both WRF-Hydro and HyDAT streamflow, so those features
 = are built from more sparse, but matching, values - RD Mar 2024.
 =#

using My, Printf, NetCDF, Statistics, StatsBase

const WRFN             = 2                              # streamflow forced by CCSM/HadGEM/MPI with neural network post-processing
const ERAN             = 4                              # streamflow forced by      ERA-5      with neural network post-processing
const FEAT             = 3                              # number of features (neural network inputs)
const DELT             = 3                              # source data timestep  (hours)
const DELD             = 24                             # daily       timestep  (hours)
const DELY             = 550 * div(DELD, DELT)          # number of   timesteps (before and after) for normalizing
const DELM             =  15 * div(DELD, DELT)          # number of   timesteps (before and after) for 30-day feature
const DELA             =   5                            # number of       years (before and after) for decadal feature
const MISS             = -9999.0                        # generic missing value

if (argc = length(ARGS)) != 2 && (argc = length(ARGS)) != 3
  print("\nUsage: jjj $(basename(@__FILE__)) hydat_valding_aall_1000_incl_1990-01-01_2004-12-30.neural.nc flow [train]\n\n")
  exit(1)
end
fila = ARGS[1]
filb = ARGS[1][1:end-3] * ".features.nc"
filc = ARGS[1][1:end-3] * ".featrain.nc"
fnam = ARGS[2] ;              fnid = ERAN
contains(ARGS[1], "iccs") && (fnid = WRFN)
contains(ARGS[1], "ih45") && (fnid = WRFN)
contains(ARGS[1], "ih85") && (fnid = WRFN)
contains(ARGS[1], "impi") && (fnid = WRFN)
fnam           != "flow"  && (fnid =    1)
@printf("\n  using %d where [1,2]=streamflow forced by CCSM/HadGEM/MPI with neural network post-processing\n", fnid)
@printf(   "            and    4 =streamflow forced by      ERA-5      with neural network post-processing\n")

hors = ncread(  fila, "time", start=[1], count=[-1])                          # read the DELT-h times and get
tatt = ncgetatt(fila, "time", "units")                                        # annual-average-daily dates
nhor = length(hors)
tmpa = dateref(hors[  1], tatt)
tmpb = dateref(hors[end], tatt)
hora = tmpa[1:4] * "-" * tmpa[5:6] * "-" * tmpa[7:8] * "-" * tmpa[9:10] ; yera = parse(Int64, tmpa[1:4])
horb = tmpb[1:4] * "-" * tmpb[5:6] * "-" * tmpb[7:8] * "-" * tmpb[9:10] ; yerb = parse(Int64, tmpb[1:4]) ; nyer = yerb - yera + 1
anna = tmpa[1:4] * "-01-01-00"
annb = tmpa[1:4] * "-12-31-00"
tima = My.datesous("1900-01-01-00", anna, "hr")
timb = My.datesous("1900-01-01-00", annb, "hr")
anns = collect(tima:DELD:timb) ; nann = length(anns)

hind = -ones(Int64, nann, nyer * div(DELD, DELT))                             # define a decadal-feature-time-index over which
dind = -ones(Int64, nann, nyer)                                               # to average (3-h for WRF-Hydro, daily for HyDAT)
anow = anna                                                                   # for each of the 365 annual-average-daily dates
for a = 1:nann                                                                # (note that these are all possible years)
  global anow ; local tmpa, tmpb, tmpc
  for b = 1:nyer
    tmpa = @sprintf("%4d-%s-12", b-1+yera, anow[6:10])
    tmpb = My.datesous("1900-01-01-00", tmpa, "hr")
    tmpc = findfirst(isequal(tmpb), hors)
    tmpc != nothing && (dind[a,b] = tmpc)
  end
  anow = My.dateadd(anow, DELD, "hr")
end

dind[   1,   1] = dind[     2,   1]                                           # (adjust daily time-index endpoints inward
dind[nann,nyer] = dind[nann-1,nyer]                                           #  as 3-h time-index endpoints are missing)
for a = 1:nann                                                                # and get 3-h decadal-feature-index (again
  d = 1 ; for b = 1:nyer, c = 1:div(DELD, DELT)                               #  for all possible years)
    hind[a,d] = dind[a,b] + c - 1 - div(div(DELD, DELT), 2)
    d += 1
  end
end

atoh = Dict{Int64, Tuple{Int64, Int64, Int64, Int64, Int64}}()                # define a map from hors to annual-average-day
hnow = hora                                                                   # (including Feb 29) and add bounds to the
for a = 1:nhor                                                                # decadal-time-index as a function of year
  global hnow ; local anow
  hnow[6:10] == "02-29" && (anow = My.datesous("1900-01-01-00", hora[1:4] *        "-02-28-00", "hr"))
  hnow[6:10] != "02-29" && (anow = My.datesous("1900-01-01-00", hora[1:4] * hnow[5:10] * "-00", "hr"))
  aind = findfirst(isequal(anow), anns)

  yerc = parse(Int64, hnow[1:4])                                              # allow shift in the centered bounds
  yind = findfirst(isequal(yerc), yera:yerb)                                  # to maintain a fixed window length
  bnda = yind - DELA
  bndb = yind + DELA
  bnda <    1 && (shft =    1 - bnda ; bnda += shft ; bndb += shft)
  bndb > nyer && (shft = bndb - nyer ; bnda -= shft ; bndb -= shft)
  bndc = (bnda - 1) * div(DELD, DELT) + 1
  bndd =  bndb      * div(DELD, DELT)
  atoh[a] = (aind, bnda, bndb, bndc, bndd)
  hnow = My.dateadd(hnow, DELT, "hr")
end

function flonorm(x::Array{Float64})                                           # employ uncorrected standard
  if length(x) == 0                                                           # deviation (div by N not N-1) to
    error("\nERROR : flonorm vector has zero length\n\n")                     # scale three years of streamflow
#   return MISS, MISS
  else
    alp = mean(x)
    bet = std(x, mean = alp, corrected = false)
    return alp, bet
  end
end

function normalize(data::Array{Float64,3}, mask::BitArray{3})                 # get a three-year normalization
  npes, nsta, ntim = size(data)
  alph = ones(nsta,ntim) * MISS
  beta = ones(nsta,ntim) * MISS
# any(x -> x < 0, data[mask]) && error("\nERROR : raw streamflow contains negative values\n\n")

  for a = 1:ntim                                                              # allow shift in centered timeseries
    c = a - DELY                                                              # to maintain a fixed window length
    d = a + DELY
    c <    1 && (e = 1 -    c ; c += e ; d += e)
    d > ntim && (e = d - ntim ; c -= e ; d -= e)
    for b = 1:nsta
      if mask[1,b,a]
        alph[b,a], beta[b,a] = flonorm(data[1,b,c:d][mask[1,b,c:d]])
      end
    end
  end
  return(alph, beta)
end

function featurize(flow::Array{Float64,3}, mskz::BitArray{3})
  npes, nsta, ntim = size(flow)
  feat = ones(FEAT,nsta,ntim) * MISS

  for a = 1:ntim                                                              # allow shift in centered timeseries
    c = a - DELM                                                              # to maintain a fixed window length
    d = a + DELM
    c <    1 && (e = 1 -    c ; c += e ; d += e)
    d > ntim && (e = d - ntim ; c -= e ; d -= e)
    (aind, bnda, bndb, bndc, bndd) = atoh[a]
    hhh = hind[aind,bndc:bndd]
    for b = 1:nsta
      if mskz[1,b,a]
        feat[ 1,b,a] =      flow[1,b,a]
        feat[ 2,b,a] = mean(flow[1,b,c:d][mskz[1,b,c:d]])
        feat[ 3,b,a] = mean(flow[1,b,hhh][mskz[1,b,hhh]])
      end
    end
  end
  return(feat)
end

function featurize(floh::Array{Float64,3}, flow::Array{Float64,3}, mskz::BitArray{3})
  npes, nsta, ntim = size(flow)
  feah = ones(FEAT,nsta,ntim) * MISS
  feat = ones(FEAT,nsta,ntim) * MISS

  for a = 1:ntim                                                              # allow shift in centered timeseries
    c = a - DELM                                                              # to maintain a fixed window length
    d = a + DELM
    c <    1 && (e = 1 -    c ; c += e ; d += e)
    d > ntim && (e = d - ntim ; c -= e ; d -= e)
    (aind, bnda, bndb, bndc, bndd) = atoh[a]
    ddd = dind[aind,bnda:bndb]
    hhh = hind[aind,bndc:bndd]
    for b = 1:nsta
      if mskz[1,b,a]
        feah[ 1,b,a] =      floh[1,b,a]
        feah[ 2,b,a] = mean(floh[1,b,c:d][mskz[1,b,c:d]])
        feah[ 3,b,a] = mean(floh[1,b,ddd][mskz[1,b,ddd]])
        feat[ 1,b,a] =      flow[1,b,a]
        feat[ 2,b,a] = mean(flow[1,b,c:d][mskz[1,b,c:d]])
        feat[ 3,b,a] = mean(flow[1,b,hhh][mskz[1,b,hhh]])
      end
    end
  end
  return(feah, feat)
end

function nccreera(fn::AbstractString, tims::Array{Float64,1}, lats::Array{Float64,1}, lons::Array{Float64,1})
  nctim = NcDim("time", length(tims), atts = Dict{Any,Any}("units"=>"hours since 1900-01-01 00:00:0.0"), values = tims)
  nclat = NcDim( "lat", length(lats), atts = Dict{Any,Any}("units"=>"degrees_north"),                    values = lats)
  nclon = NcDim( "lon", length(lons), atts = Dict{Any,Any}("units"=> "degrees_east"),                    values = lons)
  ncvrs = Array{NetCDF.NcVar}(undef, 3)
  ncvrs[1] = NcVar("alph", [       nclat, nctim], atts = Dict{Any,Any}("missing_value"=>MISS), t=Float64, compress=-1)
  ncvrs[2] = NcVar("beta", [       nclat, nctim], atts = Dict{Any,Any}("missing_value"=>MISS), t=Float64, compress=-1)
  ncvrs[3] = NcVar("feat", [nclon, nclat, nctim], atts = Dict{Any,Any}("missing_value"=>MISS), t=Float64, compress=-1)
  ncfil = NetCDF.create(fn, ncvrs, gatts = Dict{Any,Any}("units"=>"none"), mode = NC_NETCDF4)
end

function nccreerb(fn::AbstractString, tims::Array{Float64,1}, lats::Array{Float64,1}, lons::Array{Float64,1})
  nctim = NcDim("time", length(tims), atts = Dict{Any,Any}("units"=>"hours since 1900-01-01 00:00:0.0"), values = tims)
  nclat = NcDim( "lat", length(lats), atts = Dict{Any,Any}("units"=>"degrees_north"),                    values = lats)
  nclon = NcDim( "lon", length(lons), atts = Dict{Any,Any}("units"=> "degrees_east"),                    values = lons)
  ncvrs = Array{NetCDF.NcVar}(undef, 4)
  ncvrs[1] = NcVar("alph", [       nclat, nctim], atts = Dict{Any,Any}("missing_value"=>MISS), t=Float64, compress=-1)
  ncvrs[2] = NcVar("beta", [       nclat, nctim], atts = Dict{Any,Any}("missing_value"=>MISS), t=Float64, compress=-1)
  ncvrs[3] = NcVar("feah", [nclon, nclat, nctim], atts = Dict{Any,Any}("missing_value"=>MISS), t=Float64, compress=-1)
  ncvrs[4] = NcVar("feat", [nclon, nclat, nctim], atts = Dict{Any,Any}("missing_value"=>MISS), t=Float64, compress=-1)
  ncfil = NetCDF.create(fn, ncvrs, gatts = Dict{Any,Any}("units"=>"none"), mode = NC_NETCDF4)
end

tims = ncread(fila, "time", start=       [1], count=     [-1])                # for neural net predictions,
flow = ncread(fila,  fnam , start=[fnid,1,1], count=[1,-1,-1])                # read the timeseries dates and
npes, nsta, ntim = size(flow)                                                 # WRF-Hydro streamflow estimates
@printf("reading %s          %s[%d,1:%d,1:%d]\n", fila, fnam, fnid, nsta, ntim)

mskw = falses(npes, nsta, ntim)                                               # and define a flow validity mask
for a = 1:ntim, b = 1:nsta, c = 1:npes
  flow[c,b,a] > MISS && (mskw[c,b,a] = true)
end

@printf("writing %s with %d times %d stations %d flow features\n", filb, ntim, nsta, FEAT)
lats = collect(1.0:nsta)
lons = collect(1.0:FEAT)
nccreera(filb, tims, lats, lons)

(alph, beta) = normalize(flow, mskw)                                          # get a WRF-Hydro normalization
for a = 1:ntim, b = 1:nsta, c = 1:npes
  mskw[c,b,a] && (flow[c,b,a] = (flow[c,b,a] - alph[b,a]) / beta[b,a])
end
feat         = featurize(flow, mskw)                                          # and apply it to get the features
ncwrite(alph, filb, "alph", start=  [1,1], count=   [-1,-1])                  # (only where WRF-Hydro features
ncwrite(beta, filb, "beta", start=  [1,1], count=   [-1,-1])                  # are available)
ncwrite(feat, filb, "feat", start=[1,1,1], count=[-1,-1,-1])

if argc == 3                                                                  # for training neural nets,
  tims = ncread(fila, "time", start=       [1], count=     [-1])              # read the timeseries dates and
  floh = ncread(fila, "hydt", start=     [1,1], count=  [-1,-1])              # WRF-Hydro and HyDAT streamflow
  flow = ncread(fila,  fnam , start=[fnid,1,1], count=[1,-1,-1])
  npes, nsta, ntim = size(flow)
  @printf("\nreading %s          hydt and %s[%d,1:%d,1:%d]\n", fila, fnam, fnid, nsta, ntim)

  cpyh = fill(MISS, size(floh))                                               # duplicate daily HyDAT at 3-h
  for a = 1:ntim                                                              # intervals and use the same
    for b = 1:nsta                                                            # dimensions for floh as flow
      if floh[b,a] > MISS
        for c = -4:3
          if 1 <= a+c <= ntim
            cpyh[b,a+c] = floh[b,a]
          end
        end
      end
    end
  end
  floh = reshape(cpyh, 1, nsta, ntim)

  mskh = falses(       1, nsta, ntim)                                         # and define joint validity masks
  mskw = falses(       1, nsta, ntim)
  mskz = falses(       1, nsta, ntim)
  for a = 1:ntim, b = 1:nsta
    floh[1,b,a] > MISS                       && (mskh[1,b,a] = true)
                          flow[1,b,a] > MISS && (mskw[1,b,a] = true)
    floh[1,b,a] > MISS && flow[1,b,a] > MISS && (mskz[1,b,a] = true)
  end

  @printf("writing %s with %d times %d stations %d flow features\n", filc, ntim, nsta, FEAT)
  lats = collect(1.0:nsta)
  lons = collect(1.0:FEAT)
  nccreerb(filc, tims, lats, lons)

# (alph, beta) = normalize(flow, mskw)                                        # using the WRF-Hydro normalization
  for a = 1:ntim, b = 1:nsta                                                  # (ignoring all but collocations)
    if mskz[1,b,a]
      floh[ 1,b,a] = (floh[1,b,a] - alph[b,a]) / beta[b,a]
      flow[ 1,b,a] = (flow[1,b,a] - alph[b,a]) / beta[b,a]
    end
  end
  (feah, feat) = featurize(floh, flow, mskz)                                  # apply it to get features (but
  ncwrite(alph, filc, "alph", start=  [1,1], count=   [-1,-1])                # only where WRF-Hydro and HyDAT
  ncwrite(beta, filc, "beta", start=  [1,1], count=   [-1,-1])                # features are both available)
  ncwrite(feah, filc, "feah", start=[1,1,1], count=[-1,-1,-1])
  ncwrite(feat, filc, "feat", start=[1,1,1], count=[-1,-1,-1])
end

print("\n")
exit(0)

#=
function normalize(data::Array{Float64,3}, mask::BitArray{3})                 # get a three-year normalization
  npes, nsta, ntim = size(data)
  alph = ones(nsta,ntim) * MISS
  beta = ones(nsta,ntim) * MISS
# for a = 1:ntim, b = 1:nsta
#   mask[1,b,a] && data[1,b,a] < 0 && @printf("a = %11d b = %11d data = %15.8f\n", a, b, data[1,b,a])
# end
# any(x -> x < 0, data[mask]) && error("\nERROR : raw streamflow contains negative values\n\n")
  for b = 1:nsta
    aaaa, bbbb = flonorm(data[1,b,1:ntim][mask[1,b,1:ntim]])
    alph[b,:] .= aaaa
    beta[b,:] .= bbbb
  end

  @printf("copying %s  from %s\n", fild, fila) ; cp(fila, fild; force = true, follow_symlinks = true)
  @printf("copying %s from %s\n",  file, filb) ; cp(filb, file; force = true, follow_symlinks = true)
  floh = ncread(fild, "hydt", start=  [1,1], count=   [-1,-1])
  flow = ncread(fild, "flow", start=[1,1,1], count=[-1,-1,-1])
  npes, nsta, ntim = size(flow)
  @printf("reading %s  hydt and flow and normalizing\n", fild)
  for a = 1:ntim, b = 1:nsta
    if mskz[1,b,a]                                         floh[  b,a] = (floh[  b,a] - alph[b,a]) / beta[b,a]
    else                                                   floh[  b,a] = MISS                                         end
    if mskw[1,b,a]  for c = 1:npes  flow[c,b,a] > MISS && (flow[c,b,a] = (flow[c,b,a] - alph[b,a]) / beta[b,a])  end
    else            for c = 1:npes                         flow[c,b,a] = MISS                                    end  end
  end
  @printf("writing %s  hydt and flow, with %d times %d stations %d flow estimates\n", fild, ntim, nsta, npes)
  ncwrite(floh, fild, "hydt", start=  [1,1], count=   [-1,-1])
  ncwrite(flow, fild, "flow", start=[1,1,1], count=[-1,-1,-1])

elseif fnam == "riverflo"                                                     # for applying neural nets,
  tims = ncread(fila,     "time", start=    [1], count=      [-1])            # read the timeseries dates and
  flow = ncread(fila, "riverflo", start=[1,1,1], count=[-1,-1,-1])            # WRF-Hydro streamflow estimates
  npes, nsta, ntim = size(flow)
  @printf("\nreading %s      riverflo\n", fila)

  mskw = falses(npes, nsta, ntim)                                             # and define a flow validity mask
  for a = 1:ntim, b = 1:nsta, c = 1:npes
    flow[c,b,a] > MISS && (mskw[c,b,a] = true)
  end

  @printf("writing %s riverflo features, with %d times %d stations %d flow features\n", filc, ntim, nsta, FEAT)
  lats = collect(1.0:nsta)
  lons = collect(1.0:FEAT)
  nccreer(filc, tims, lats, lons)

  (alph, beta) = normalize(flow, mskw)                                        # get the normalization and
  for a = 1:ntim, b = 1:nsta, c = 1:npes
    mskw[c,b,a] && (flow[c,b,a] = (flow[c,b,a] - alph[b,a]) / beta[b,a])
  end
  feat         = featurize(flow, mskw)                                        # apply it to get the features
  ncwrite(alph, filc, "alph", start=  [1,1], count=   [-1,-1])
  ncwrite(beta, filc, "beta", start=  [1,1], count=   [-1,-1])
  ncwrite(feat, filc, "flow", start=[1,1,1], count=[-1,-1,-1])


(hora, horb, nhor, yera, yerb) = ("1990-01-01-03", "2004-12-30-21", 43823, 1990, 2004)

#   mskh[1,b,a] && (floh[1,b,a] = (floh[1,b,a] - alph[b,a]) / beta[b,a])
#   mskw[1,b,a] && (flow[1,b,a] = (flow[1,b,a] - alph[b,a]) / beta[b,a])
#   (feah, feat) = featurize(floh, flow, mskh, mskw, mskz)
# function featurize(floh::Array{Float64,3}, flow::Array{Float64,3}, mskh::BitArray{3}, mskw::BitArray{3}, mskz::BitArray{3})

#temp = split(ARGS[1], "_")
#dbeg = temp[end-1][1:4] * temp[end-1][6:7] * temp[end-1][9:10] * @sprintf("%2d",      DELT) ; dbeg = replace(dbeg, ' ' => '0')
#dend = temp[end  ][1:4] * temp[end  ][6:7] * temp[end  ][9:10] * @sprintf("%2d", 24 - DELT) ; dend = replace(dend, ' ' => '0')
#dnum = datesous(dbeg, dend, "hr") / DELT + 1

@show hora, horb, nhor, yera, yerb
# (hora, horb, nhor, yera, yerb) = ("1990-01-01-03", "2004-12-30-21", 43823, 1990, 2004)

#dyta = zeros(      nlet, nann) ; nyta = zeros(      nlet, nann)
#data = zeros(DNUM, nlet, nann) ; nata = zeros(DNUM, nlet, nann)
#tmpa = dateref(anns[  1], tatt)
#tmpb = dateref(anns[end], tatt)
#hora = tmpa[1:4] * "-" * tmpa[5:6] * "-" * tmpa[7:8] * "-" * tmpa[9:10]
#horb = tmpb[1:4] * "-" * tmpb[5:6] * "-" * tmpb[7:8] * "-" * tmpb[9:10]
#@show hora, horb, nann

hnow = hora
for a = 1:nhor
  global hnow
  (aind, bnda, bndb, bndc, bndd) = atoh[a] ;                          (a < 3 || a > nhor-2) && print("$hnow ")
  for b = bnda:bndb  hnnn = dateref(hors[dind[aind,b]], tatt)[1:10] ; (a < 3 || a > nhor-2) && print("$hnnn ")  end
(a < 3 || a > nhor-2) && print("\n")
(a < 3 || a > nhor-2) && print("$hnow ")
  for b = bndc:bndd  hnnn = dateref(hors[hind[aind,b]], tatt)[1:10] ; (a < 3 || a > nhor-2) && print("$hnnn ")  end
  hnow = My.dateadd(hnow, DELT, "hr") ;                               (a < 3 || a > nhor-2) && print("\n")
end

anow = anna
for a = 1:nann
  global anow ;                                                                 (a < 3 || a > 363) && print("$anow ")
# for b = 1:nyer                  hnow = dateref(hors[dind[a,b]], tatt)[1:10] ; (a < 3 || a > 363) && print("$hnow ")  end
  for b = 1:nyer*div(DELD, DELT)  hnow = dateref(hors[hind[a,b]], tatt)[1:10] ; (a < 3 || a > 363) && print("$hnow ")  end
  anow = My.dateadd(anow, DELD, "hr") ;                                         (a < 3 || a > 363) && print("\n")
end
=#
