#=
 = Copy a (Severn-Hudson) subset of river outlets from a file containing the WRF
 = Hydro timeseries, varying in time and latitude with each outlet on a different
 = proxy latitude (true outlet positions, as well as ocean pour points that are
 = slightly displaced into the ocean, are stored as time-invariant variables).
 = Also copy the corresponding text file of outlet positions - RD Jan 2024.
 =#

using My, Printf, NetCDF

const MISS             = -9999.0                        # generic missing value

if (argc = length(ARGS)) != 1
  print("\nUsage: jjj $(basename(@__FILE__)) run_DOMAIN_full_full_FORCING.era_1990-01-01_2004-12-30_3h\n\n")
  exit(1)
end
fila = ARGS[1] * "_Severn_Hudson.nc"
filb = ARGS[1] * "_Severn_Hudson.txt" ; fpa = My.ouvre(filb, "r", false) ; lins = readlines(fpa, keep = true) ; close(fpa)
filc = ARGS[1] * "_Severn_SCroix.nc"
fild = ARGS[1] * "_Severn_SCroix.txt"

form = ""
nlet = length(lins)                                                           # read river outlet locations (immediately
mask = falses(nlet)                                                           # upriver of the corresponding ocean entries)
for a = 1:nlet                                                                # and mask those beyond the Severn-Hudson rivers
  global form
  tmpa = split(lins[a])
  rlat = parse(Float64, tmpa[5])
  rlon = parse(Float64, tmpa[6])
#45.16965866    -67.17859650
  if rlon > -87.7 && 40.6 < rlat < 62.7 && !(rlat < 45.17 && rlon < -67.17)
    mask[a] = true
    form *= lins[a]
  end
end
nset = length(mask[mask])

@printf("\nwriting %3d of %3d outlets to %s\n", nset, nlet, filc)
@printf(  "                          and %s\n",             fild)
fpa = My.ouvre(fild, "w", false) ; write(fpa, form) ; close(fpa)

function nccreate(fn::AbstractString, ntim::Int, nlat::Int, nlon::Int, missing::Float64)
  nctim = NcDim("time", ntim, atts = Dict{Any,Any}("units"=>   "hours since 1900-01-01 00:00:0.0",
                                                   "unites"=>"heures depuis 1900-01-01 00:00:0.0"), values = collect(range(    0, stop =                  ntim - 1 , length = ntim)))
  nclat = NcDim( "lat", nlat, atts = Dict{Any,Any}("units"=>                      "degrees_north",
                                                   "unites"=>                       "degrés nord"), values = collect(range( 50.0, stop =  50.0 + 0.001 * (nlat - 1), length = nlat)))
  nclon = NcDim( "lon", nlon, atts = Dict{Any,Any}("units"=>                       "degrees_east",
                                                   "unites"=>                        "degrés est"), values = collect(range(280.0, stop = 280.0 + 0.001 * (nlon - 1), length = nlon)))
  ncvrs = Array{NetCDF.NcVar}(undef, 5)
# ncvrs[1] = NcVar("rivgridx", [nclon, nclat       ], atts = Dict{Any,Any}("units"=>"none",   "long_name"=>"river outlet x-index on WRF-Hydro grid", "missing_value"=>missing), t=  Int64, compress=-1)
# ncvrs[2] = NcVar("rivgridy", [nclon, nclat       ], atts = Dict{Any,Any}("units"=>"none",   "long_name"=>"river outlet y-index on WRF-Hydro grid", "missing_value"=>missing), t=  Int64, compress=-1)
  ncvrs[1] = NcVar("riverlat", [nclon, nclat       ], atts = Dict{Any,Any}("units"=>"degree", "unites"=>"degrés",
                                                                           "long_name"=>"latitude of river outlet",
                                                                           "nom_long"=>"latitude de l'embouchure de la rivière",
                                                                           "missing_value"=>missing), t=Float64, compress=-1)
  ncvrs[2] = NcVar("riverlon", [nclon, nclat       ], atts = Dict{Any,Any}("units"=>"degree", "unites"=>"degrés",
                                                                           "long_name"=>"longitude of river outlet",
                                                                           "nom_long"=>"longitude de l'embouchure de la rivière",
                                                                           "missing_value"=>missing), t=Float64, compress=-1)
  ncvrs[3] = NcVar("oceanlat", [nclon, nclat       ], atts = Dict{Any,Any}("units"=>"degree", "unites"=>"degrés",
                                                                           "long_name"=>"latitude of ocean pour point",
                                                                           "nom_long"=>"latitude de l'océan point d'écoulement",
                                                                           "missing_value"=>missing), t=Float64, compress=-1)
  ncvrs[4] = NcVar("oceanlon", [nclon, nclat       ], atts = Dict{Any,Any}("units"=>"degree", "unites"=>"degrés",
                                                                           "long_name"=>"longitude of ocean pour point",
                                                                           "nom_long"=>"longitude de l'océan point d'écoulement",
                                                                           "missing_value"=>missing), t=Float64, compress=-1)
  ncvrs[5] = NcVar("riverflo", [nclon, nclat, nctim], atts = Dict{Any,Any}("units"=>"m3 s-1", "unites"=>"m3 s-1",
                                                                           "long_name"=>"streamflow at river outlet",
                                                                           "nom_long"=>"débit à la sortie de la rivière",
                                                                           "missing_value"=>missing), t=Float64, compress=-1)
  descript = "Série temporelle d\'écoulement de cours d\'eau WRF-Hydro post-traitée par réseau neuronal à 477 exutoires de rivières océaniques, où lat/lon sont des positions proxy, " *
             "riverlat/lon sont des positions d\'exutoire, et océanlat/lon sont des points d\'écoulement océaniques qui sont légèrement déplacés dans l\'océan (à côté de la position d\'exutoire sur le réseau WRF-Hydro)"
  summary  = "Neural network post-processed WRF-Hydro streamflow timeseries at 477 oceanic river outlets, where lat/lon are proxy position, riverlat/lon are outlet position, " *
             "and oceanlat/lon are ocean pour points that are displaced slightly into the ocean (next to outlet position on the WRF-Hydro grid)"
  ncfil = NetCDF.create(fn, ncvrs, gatts = Dict{Any,Any}("Description"=>descript, "Summary"=>summary), mode = NC_NETCDF4)
  print("\ncreated $fn with $ntim times $nlat lats and $nlon lons\n\n")
end

tims = ncread(fila,     "time", start=[1],     count=[-1])
#xind = ncread(fila, "rivgridx", start=[1,1],   count=[-1,-1])[:,mask]
#yind = ncread(fila, "rivgridy", start=[1,1],   count=[-1,-1])[:,mask]
rlat = ncread(fila, "riverlat", start=[1,1],   count=[-1,-1])[:,mask]
rlon = ncread(fila, "riverlon", start=[1,1],   count=[-1,-1])[:,mask]
olat = ncread(fila, "oceanlat", start=[1,1],   count=[-1,-1])[:,mask]
olon = ncread(fila, "oceanlon", start=[1,1],   count=[-1,-1])[:,mask]
flow = ncread(fila, "riverflo", start=[1,1,1], count=[-1,-1,-1])[:,mask,:]

nccreate(     filc, length(tims), nset, 1, MISS)
ncwrite(tims, filc,     "time", start=[1],     count=[-1])
#ncwrite(xind, filc, "rivgridx", start=[1,1],   count=[-1,-1])
#ncwrite(yind, filc, "rivgridy", start=[1,1],   count=[-1,-1])
ncwrite(rlat, filc, "riverlat", start=[1,1],   count=[-1,-1])
ncwrite(rlon, filc, "riverlon", start=[1,1],   count=[-1,-1])
ncwrite(olat, filc, "oceanlat", start=[1,1],   count=[-1,-1])
ncwrite(olon, filc, "oceanlon", start=[1,1],   count=[-1,-1])
ncwrite(flow, filc, "riverflo", start=[1,1,1], count=[-1,-1,-1])
exit(0)
