#=
 = Create local links to all files in a directory and any subdirectories
 = (note that a link is created only to the last file found, where two
 =  files from different subdirs have the same name) - RD April 2020.
 =#

using My, Printf

if (argc = length(ARGS)) != 1
  print("\nUsage: jjj $(basename(@__FILE__)) /gpfs/fs2/dfo/hpcmc-comda/dfo_comda/miz001/NA2/Hist/surface/\n\n")
  exit(1)
end

# wrfpost.surface.y2004m12d31.nc

function walkdir(dir)
  absdir = abspath(dir)
  files  = readdir(absdir)
  for file in files
    full = joinpath(absdir, file)
    if isfile(full) && startswith(file, "wrfpost.surface.y") && endswith(full, ".nc")
      year = parse(Int64, file[18:21])
#     if 1990 <= year <= 2004
        if islink(file)
          print("$file link already exists, but replacing with a link to $full\n")
          rm(file)
        end
        symlink(full, file)
#     end
    elseif isdir(full)
      walkdir(full)
    end
  end
end

walkdir(ARGS[1])

for a = 1904:4:2200
  fila = @sprintf("wrfpost.surface.y%dm02d28.nc", a)
  filb = @sprintf("wrfpost.surface.y%dm02d29.nc", a)
  if isfile(fila) && !isfile(filb)
    symlink(fila, filb)
  end
end
