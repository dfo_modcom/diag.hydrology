#=
 = This program applies a calibration that was previously computed to adjust one
 = timeseries to another.  Separate calibrations are performed for each UTC hour
 = (0,6,12,18) as well as for the continuous 6-h timeseries.  Output is a set of
 = calibrated NetCDF files - RD May 2021.
 =#

using My, Printf, NetCDF
const MISS             = -9999.0                        # generic missing value
const GRDMIS           = 99999.0                        # generic missing value on file

if (argc = length(ARGS)) != 2
  print("\nUsage: jjj $(basename(@__FILE__)) ../FORCING.wrf.mcal ../FORCING/forcing_wrf_1990_2004.cato.nar_1990_2004\n\n")
  exit(1)
end

vars = ["R2D", "S2D", "T2D", "Q2D", "U2D", "V2D", "PSFC", "RAINRATE", "SWDOWN", "LWDOWN"]
utcs = ["00", "06", "12", "18", "by"]
nvar = length(vars)
nutc = length(utcs)
cals = Array{Float64}(undef, 2, nutc, nvar)

fpa = My.ouvre(ARGS[2], "r") ; lins = readlines(fpa) ; close(fpa)             # read all calibration parameters
for a = 1:nvar
  tmpa = split(lins[a])
  for b = 1:nutc
    cals[1,b,a] = parse(Float64, tmpa[2*b  ])
    cals[2,b,a] = parse(Float64, tmpa[2*b+1])
  end
end

files = readdir(".")                                                          # and apply these calibrations to
for filu in files                                                             # a copy of each uncalibrated file
  if isfile(filu) && !islink(filu)                                            # in the current directory (ignore
    filc = ARGS[1] * "/" * filu                                               # missing grid values)
    filu[9:10] == utcs[1] && (b = 1)
    filu[9:10] == utcs[2] && (b = 2)
    filu[9:10] == utcs[3] && (b = 3)
    filu[9:10] == utcs[4] && (b = 4)
    print("writing $filc\n") ; cp(filu, filc, ; force = true)

    for a = 3:nvar
      cccc = ncread(filc, vars[a], start=[1,1,1], count=[-1,-1,-1])
      (nlon, nlat, ntim) = size(cccc)
      if a == 5 || a == 6
        for c = 1:nlat, d = 1:nlon
          cccc[d,c,1] = (cccc[d,c,1] - cals[1,b,a]) / cals[2,b,a]
        end
      else
        for c = 1:nlat, d = 1:nlon
          cccc[d,c,1] = (cccc[d,c,1] - cals[1,b,a]) / cals[2,b,a]
          cccc[d,c,1] < 0 && (cccc[d,c,1] = 0)
        end
      end
      ncwrite(cccc, filc, vars[a], start=[1,1,1], count=[-1,-1,-1])
    end
  end
end
exit(0)
