#=
 = Combine the separate HydroSHED tiles to create a single hydrologically
 = conditioned DEM that covers all watersheds of interest.  This assumes
 = prior conversion of each tile from, say, .bil to .bil.nc (for example,
 = by gdal_translate tile.bil tile.nc), possibly followed by conversation
 = of the resulting .nc file back to .bil (and thus, an attempt is made
 = to duplicate the NetCDF metadata in "crs" that GDAL expects, but this
 = might need to be modified to suit the final output grid).  As HydroSHED
 = tiles are 6000 gridboxes across, a multiple of this number defines the
 = LATS-LONS grid to encompass all NetCDF tiles in the current directory
 = (i.e., all tiles fit within the final grid and no extra tiles exist in
 = the current directory) - RD Jan 2021.
 =#

using My, Printf, NetCDF

const LATS             = 24000                          # number of latitudes
const LONS             = 54000                          # number of longitudes
const DELL             =   0.0008333333333333333        # lat/lon grid spacing
const LATA             =  40.0004166666666666667        # first latitude on grid
const LONA             = -94.9995833333333333333        # first longitude on grid
const MISS             = Int16(-9999)                   # generic missing value

if (argc = length(ARGS)) != 1
  print("\nUsage: jjj $(basename(@__FILE__)) na_con_3s_all.nc\n\n")
  exit(1)
end
filo = ARGS[1]

function nccreate(fn::AbstractString, lats::Array{Float64,1}, lons::Array{Float64,1}, missing::Int16)
  nclat = NcDim("lat", length(lats), atts = Dict{Any,Any}(
                "standard_name"               => "latitude",
                "long_name"                   => "latitude",
                "units"                       => "degrees_north"), values = lats)
  nclon = NcDim("lon", length(lons), atts = Dict{Any,Any}(
                "standard_name"               => "longitude",
                "long_name"                   => "longitude",
                "units"                       => "degrees_east"), values = lons)
  nclen = NcDim("len", 0)
  ncchr = NcVar("crs", [nclen], atts = Dict{Any,Any}(                         # define a NetCDF grid that GDAL
                "grid_mapping_name"           => "latitude_longitude",        # would recognize when converting
                "long_name"                   => "CRS definition",            # from .nc to .bil (attributes are
                "longitude_of_prime_meridian" => 0.,                          # taken from the .bil.nc files that
                "semi_major_axis"             => 6378137.,                    # GDAL creates from the .bil tiles)
                "inverse_flattening"          => 298.257223563,
                "spatial_ref"                 => "GEOGCS[\"WGS 84\",DATUM[\"WGS_1984\",SPHEROID[\"WGS 84\",6378137,298.257223563,AUTHORITY[\"EPSG\",\"7030\"]],AUTHORITY[\"EPSG\",\"6326\"]],PRIMEM[\"Greenwich\",0,AUTHORITY[\"EPSG\",\"8901\"]],UNIT[\"degree\",0.0174532925199433,AUTHORITY[\"EPSG\",\"9122\"]],AXIS[\"Latitude\",NORTH],AXIS[\"Longitude\",EAST],AUTHORITY[\"EPSG\",\"4326\"]]",
                "GeoTransform"                => "-95 0.0008333333333333 0 60 0 -0.0008333333333333 "), t=NC_CHAR, compress=-1)
  ncbnd = NcVar("Band1", [nclon, nclat], atts = Dict{Any,Any}(
                "long_name"                   => "GDAL Band Number 1",
                "_FillValue"                  => missing,
                "grid_mapping"                => "crs"), t=Int16, compress=-1)
  ncfil = NetCDF.create(fn, [ncchr, ncbnd], gatts = Dict{Any,Any}(
                "Conventions"                 => "CF-1.5",
                "GDAL"                        => "GDAL 3.0.4, released 2020/01/28"), mode = NC_NETCDF4)
  print("created $fn with $(length(lats)) lats and $(length(lons)) lons\n")
  return
end

lats = Array{Float64}(undef, LATS)                                            # define the output dimensions
lons = Array{Float64}(undef, LONS)
for a = 1:LATS  lats[a] = LATA + DELL * (a - 1)  end
for a = 1:LONS  lons[a] = LONA + DELL * (a - 1)  end
nccreate(filo, lats, lons, MISS)

grids = zeros(Int16, LONS, LATS) .+ MISS                                      # initialize the output grid as all
files = readdir(".")                                                          # ocean (MISS), substitute elevation
for file in files                                                             # on sections of the grid covered by
  if isfile(file) && endswith(file, "_con.bil.nc")                            # each tile in the current directory,
    print("reading $file ")                                                   # then save the grid
    glat = ncread(file, "lat", start=[1], count=[-1])
    glon = ncread(file, "lon", start=[1], count=[-1])
    lata = round(Int, (glat[  1] - LATA) / DELL) + 1
    lona = round(Int, (glon[  1] - LONA) / DELL) + 1
    latb = round(Int, (glat[end] - LATA) / DELL) + 1
    lonb = round(Int, (glon[end] - LONA) / DELL) + 1
    print("and updating on lat/lon indices [$lata,$latb] and [$lona,$lonb]\n")

    grid = ncread(file, "Band1", start=[1,1], count=[-1,-1])
    (nlon, nlat) = size(grid) ; lata -= 1 ; lona -= 1
    for a = 1:nlat, b = 1:nlon
      grids[lona+b,lata+a] = grid[b,a]
    end
  end
end
ncwrite(grids, filo, "Band1", start=[1,1], count=[-1,-1])
exit(0)
