#=
 = Loop through each HyDAT station and convert SQL data to text and NetCDF files
 = where the .sta text file contains a fixed number of entries (including initially
 = missing values for the nearest positions along each of the HydroSHEDS, SWOT/SWORD
 = and WRF-Hydro river networks), followed by any number of extra text lines for SQL
 = remarks from the HyDAT database - RD Dec 2022, Feb 2023.
 =#

using My, Printf, NetCDF, SQLite, DataFrames

const YBEG             = 1982                           # first year of daily timeseries
const YEND             = 2023                           #  last year of daily timeseries
const FLOW             = 1                              # daily flow rate (m3/s)
const LEVL             = 3                              # daily water level (m)
const SEDC             = 5                              # daily suspended sediment concentration (mg/l)
const LOAD             = 7                              # daily suspended sediment load (tonnes) calculated by SEDC * FLOW * const
const VNUM             = 8                              # number of variables
const DELT             = 24                             # timestep (hours)
const GOOD             = 1.0                            # code to represent a good measurement

const DELMISS          = -8e9                           # generic missing value comparison
const GRDMISS          = -9e9                           # generic missing value on a grid
const MISS             = -9999.0                        # generic missing value
const MIST             = @sprintf("%d", Int64(MISS))    # generic missing value string

if (argc = length(ARGS)) == 0
  print("\nUsage: jjj $(basename(@__FILE__)) Hydat.sqlite3 [01AP005 01AD002 01AD004 ...]\n\n")
  exit(1)
end
hydat = ARGS[1]

stall = SQLite.DB(hydat)                                                      # read the station data
stdat = DBInterface.execute(stall, "SELECT * FROM               STATIONS") |> DataFrame ; rtdat, ctdat = size(stdat)
strem = DBInterface.execute(stall, "SELECT * FROM            STN_REMARKS") |> DataFrame ; rtrem, ctrem = size(strem)
strng = DBInterface.execute(stall, "SELECT * FROM         STN_DATA_RANGE") |> DataFrame ; rtrng, ctrng = size(strng)
streg = DBInterface.execute(stall, "SELECT * FROM         STN_REGULATION") |> DataFrame ; rtreg, ctreg = size(streg)
stdcl = DBInterface.execute(stall, "SELECT * FROM    STN_DATA_COLLECTION") |> DataFrame ; rtdcl, ctdcl = size(stdcl)
stsdc = DBInterface.execute(stall, "SELECT * FROM   STN_DATUM_CONVERSION") |> DataFrame ; rtsdc, ctsdc = size(stsdc)
stsdu = DBInterface.execute(stall, "SELECT * FROM    STN_DATUM_UNRELATED") |> DataFrame ; rtsdu, ctsdu = size(stsdu)
stsos = DBInterface.execute(stall, "SELECT * FROM STN_OPERATION_SCHEDULE") |> DataFrame ; rtsos, ctsos = size(stsos)
stdal = DBInterface.execute(stall, "SELECT * FROM             DATUM_LIST") |> DataFrame ; rtdal, ctdal = size(stdal)
stagl = DBInterface.execute(stall, "SELECT * FROM            AGENCY_LIST") |> DataFrame ; rtagl, ctagl = size(stagl)
strol = DBInterface.execute(stall, "SELECT * FROM   REGIONAL_OFFICE_LIST") |> DataFrame ; rtrol, ctrol = size(strol)
strec = DBInterface.execute(stall, "SELECT * FROM       STN_REMARK_CODES") |> DataFrame ; rtrec, ctrec = size(strec)
stsac = DBInterface.execute(stall, "SELECT * FROM    SAMPLE_REMARK_CODES") |> DataFrame ; rtsac, ctsac = size(stsac)
stdty = DBInterface.execute(stall, "SELECT * FROM             DATA_TYPES") |> DataFrame ; rtdty, ctdty = size(stdty)
ststy = DBInterface.execute(stall, "SELECT * FROM         SED_DATA_TYPES") |> DataFrame ; rtsty, ctsty = size(ststy)
stopc = DBInterface.execute(stall, "SELECT * FROM        OPERATION_CODES") |> DataFrame ; rtopc, ctopc = size(stopc)
stmec = DBInterface.execute(stall, "SELECT * FROM      MEASUREMENT_CODES") |> DataFrame ; rtmec, ctmec = size(stmec)
stprc = DBInterface.execute(stall, "SELECT * FROM        PRECISION_CODES") |> DataFrame ; rtprc, ctprc = size(stprc)
stssc = DBInterface.execute(stall, "SELECT * FROM       STN_STATUS_CODES") |> DataFrame ; rtssc, ctssc = size(stssc)
stpec = DBInterface.execute(stall, "SELECT * FROM             PEAK_CODES") |> DataFrame ; rtpec, ctpec = size(stpec)
stdsy = DBInterface.execute(stall, "SELECT * FROM           DATA_SYMBOLS") |> DataFrame ; rtdsy, ctdsy = size(stdsy)
stsvs = DBInterface.execute(stall, "SELECT * FROM   SED_VERTICAL_SYMBOLS") |> DataFrame ; rtsvs, ctsvs = size(stsvs)
stsvl = DBInterface.execute(stall, "SELECT * FROM  SED_VERTICAL_LOCATION") |> DataFrame ; rtsvl, ctsvl = size(stsvl)
stcos = DBInterface.execute(stall, "SELECT * FROM  CONCENTRATION_SYMBOLS") |> DataFrame ; rtcos, ctcos = size(stcos)
stver = DBInterface.execute(stall, "SELECT * FROM                VERSION") |> DataFrame ; rtver, ctver = size(stver)
@printf("\nfound %d stations in %s\n", rtdat, hydat)

function stmatch(stnam::AbstractString, stdat::AbstractString)
  tail = ""
  if stnam == "REGIONAL_OFFICE_LIST"  for a = 1:rtrol  stdat == "$(strol[a,1])" && (tail = strol[a,2])  end  end
  if stnam ==     "STN_STATUS_CODES"  for a = 1:rtssc  stdat ==    stssc[a,1]   && (tail = stssc[a,2])  end  end
  if stnam ==           "DATA_TYPES"  for a = 1:rtdty  stdat ==    stdty[a,1]   && (tail = stdty[a,2])  end  end
  if stnam ==       "SED_DATA_TYPES"  for a = 1:rtsty  stdat ==    ststy[a,1]   && (tail = ststy[a,2])  end  end
  if stnam ==    "MEASUREMENT_CODES"  for a = 1:rtmec  stdat ==    stmec[a,1]   && (tail = stmec[a,2])  end  end
  if stnam ==      "OPERATION_CODES"  for a = 1:rtopc  stdat ==    stopc[a,1]   && (tail = stopc[a,2])  end  end
  tail === missing && (tail = MIST)
  tail
end
function stmatch(stnam::AbstractString, stdat::Int64)
  tail = ""
  if stnam ==          "AGENCY_LIST"  for a = 1:rtagl  stdat ==    stagl[a,1]   && (tail = stagl[a,2])  end  end
  if stnam ==           "DATUM_LIST"  for a = 1:rtdal  stdat ==    stdal[a,1]   && (tail = stdal[a,2])  end  end
  if stnam ==     "STN_REMARK_CODES"  for a = 1:rtrec  stdat ==    strec[a,1]   && (tail = strec[a,2])  end  end
  tail === missing && (tail = MIST)
  tail
end
function stmatch(stnam::AbstractString, stdat::Missing)  MIST  end
function nonewln(strng::AbstractString)  newa = replace(strng, "\r" => " ") ; newb = replace(newa, "\n" => " ")  end

stnms = names(stdat)                                                          # save station data in text files
function stwrite(ind::Int64)                                                  # with temporary missing values for
  stfil = "hydat_" * stdat[ind,1] * ".sta"                                    # the nearest model and SWOT rivers
  fpa = My.ouvre(stfil, "w")                                                  # (both location and grid indices)
  form = @sprintf("%-24s %s [%s]\n", stnms[2], stdat[ind,2], stdat[ind,1])
  write(fpa, form)
  for a = 3:ctdat                        form = @sprintf("%-24s %s\n",      stnms[a],                                                stdat[ind,a])
    stnms[a] == "REGIONAL_OFFICE_ID" && (form = @sprintf("%-24s %s [%s]\n", stnms[a], stmatch("REGIONAL_OFFICE_LIST", stdat[ind,a]), stdat[ind,a]))
    stnms[a] ==         "HYD_STATUS" && (form = @sprintf("%-24s %s [%s]\n", stnms[a], stmatch(    "STN_STATUS_CODES", stdat[ind,a]), stdat[ind,a]))
    stnms[a] ==         "SED_STATUS" && (form = @sprintf("%-24s %s [%s]\n", stnms[a], stmatch(    "STN_STATUS_CODES", stdat[ind,a]), stdat[ind,a]))
    stnms[a] ==     "CONTRIBUTOR_ID" && (form = @sprintf("%-24s %s [%s]\n", stnms[a], stmatch(         "AGENCY_LIST", stdat[ind,a]), stdat[ind,a]))
    stnms[a] ==        "OPERATOR_ID" && (form = @sprintf("%-24s %s [%s]\n", stnms[a], stmatch(         "AGENCY_LIST", stdat[ind,a]), stdat[ind,a]))
    stnms[a] ==           "DATUM_ID" && (form = @sprintf("%-24s %s [%s]\n", stnms[a], stmatch(          "DATUM_LIST", stdat[ind,a]), stdat[ind,a]))
    write(fpa, form)
  end
  form = @sprintf("%-24s %6d %6d %7.3f\n", "$YBEG-$YEND.DLY_FLOWS",      Int64(MISS), Int64(MISS), MISS) ; write(fpa, form)
  form = @sprintf("%-24s %6d %6d %7.3f\n", "$YBEG-$YEND.DLY_LEVELS",     Int64(MISS), Int64(MISS), MISS) ; write(fpa, form)
  form = @sprintf("%-24s %6d %6d %7.3f\n", "$YBEG-$YEND.SED_DLY_SUSCON", Int64(MISS), Int64(MISS), MISS) ; write(fpa, form)
  form = @sprintf("%-24s %6d %6d %7.3f\n", "$YBEG-$YEND.SED_DLY_LOADS",  Int64(MISS), Int64(MISS), MISS) ; write(fpa, form)
  form = @sprintf("%-24s %s as of %s\n",      "HYDAT_VERSION",           stver[1,1],  stver[1,2])        ; write(fpa, form)
  form = @sprintf(" ------------- HydroSHEDS ------------ \n")                                           ; write(fpa, form)
  form = @sprintf("%-24s %f %d\n",                 "LATITUDE",                 MISS,  Int64(MISS))       ; write(fpa, form)
  form = @sprintf("%-24s %f %d\n",                "LONGITUDE",                 MISS,  Int64(MISS))       ; write(fpa, form)
  form = @sprintf("%-24s %d\n",                    "HYRIV_ID",                        Int64(MISS))       ; write(fpa, form)
  form = @sprintf("%-24s %d\n",                   "NEXT_DOWN",                        Int64(MISS))       ; write(fpa, form)
  form = @sprintf("%-24s %d\n",                    "MAIN_RIV",                        Int64(MISS))       ; write(fpa, form)
  form = @sprintf("%-24s %d\n",                   "LENGTH_KM",                        Int64(MISS))       ; write(fpa, form)
  form = @sprintf("%-24s %d\n",                  "DIST_DN_KM",                        Int64(MISS))       ; write(fpa, form)
  form = @sprintf("%-24s %d\n",                  "DIST_UP_KM",                        Int64(MISS))       ; write(fpa, form)
  form = @sprintf("%-24s %d\n",                   "CATCH_SKM",                        Int64(MISS))       ; write(fpa, form)
  form = @sprintf("%-24s %d\n",                  "UPLAND_SKM",                        Int64(MISS))       ; write(fpa, form)
  form = @sprintf("%-24s %d\n",                   "ENDORHEIC",                        Int64(MISS))       ; write(fpa, form)
  form = @sprintf("%-24s %d\n",                  "DIS_AV_CMS",                        Int64(MISS))       ; write(fpa, form)
  form = @sprintf("%-24s %d\n",                    "ORD_STRA",                        Int64(MISS))       ; write(fpa, form)
  form = @sprintf("%-24s %d\n",                    "ORD_CLAS",                        Int64(MISS))       ; write(fpa, form)
  form = @sprintf("%-24s %d\n",                    "ORD_FLOW",                        Int64(MISS))       ; write(fpa, form)
  form = @sprintf("%-24s %d\n",                   "HYBAS_L12",                        Int64(MISS))       ; write(fpa, form)
  form = @sprintf("%-24s %s as of %s\n", "HydroSHEDS_VERSION",           stver[1,1],  stver[1,2])        ; write(fpa, form)
  form = @sprintf(" --------------- SWOT ---------------- \n")                                           ; write(fpa, form)
  form = @sprintf("%-24s %f %d\n",                 "node_lat",                 MISS,  Int64(MISS))       ; write(fpa, form)
  form = @sprintf("%-24s %f %d\n",                 "node_lon",                 MISS,  Int64(MISS))       ; write(fpa, form)
  form = @sprintf("%-24s %d\n",                     "node_id",                        Int64(MISS))       ; write(fpa, form)
  form = @sprintf("%-24s %d\n",                 "node_length",                        Int64(MISS))       ; write(fpa, form)
  form = @sprintf("%-24s %d\n",                    "reach_id",                        Int64(MISS))       ; write(fpa, form)
  form = @sprintf("%-24s %d\n",                         "wse",                        Int64(MISS))       ; write(fpa, form)
  form = @sprintf("%-24s %d\n",                     "wse_var",                        Int64(MISS))       ; write(fpa, form)
  form = @sprintf("%-24s %d\n",                       "width",                        Int64(MISS))       ; write(fpa, form)
  form = @sprintf("%-24s %d\n",                   "width_var",                        Int64(MISS))       ; write(fpa, form)
  form = @sprintf("%-24s %d\n",                  "n_chan_max",                        Int64(MISS))       ; write(fpa, form)
  form = @sprintf("%-24s %d\n",                  "n_chan_mod",                        Int64(MISS))       ; write(fpa, form)
  form = @sprintf("%-24s %d\n",                  "obstr_type",                        Int64(MISS))       ; write(fpa, form)
  form = @sprintf("%-24s %d\n",                     "grod_id",                        Int64(MISS))       ; write(fpa, form)
  form = @sprintf("%-24s %d\n",                   "hfalls_id",                        Int64(MISS))       ; write(fpa, form)
  form = @sprintf("%-24s %d\n",                    "dist_out",                        Int64(MISS))       ; write(fpa, form)
  form = @sprintf("%-24s %d\n",                        "type",                        Int64(MISS))       ; write(fpa, form)
  form = @sprintf("%-24s %d\n",                        "facc",                        Int64(MISS))       ; write(fpa, form)
  form = @sprintf("%-24s %d\n",                    "lakeflag",                        Int64(MISS))       ; write(fpa, form)
  form = @sprintf("%-24s %d\n",                   "max_width",                        Int64(MISS))       ; write(fpa, form)
  form = @sprintf("%-24s %d\n",                  "river_name",                        Int64(MISS))       ; write(fpa, form)
  form = @sprintf("%-24s %d\n",                   "sinuosity",                        Int64(MISS))       ; write(fpa, form)
  form = @sprintf("%-24s %d\n",                   "meand_len",                        Int64(MISS))       ; write(fpa, form)
  form = @sprintf("%-24s %d\n",                  "manual_add",                        Int64(MISS))       ; write(fpa, form)
  form = @sprintf("%-24s %d\n",                   "reach_lat",                        Int64(MISS))       ; write(fpa, form)
  form = @sprintf("%-24s %d\n",                   "reach_lon",                        Int64(MISS))       ; write(fpa, form)
  form = @sprintf("%-24s %d\n",                    "reach_id",                        Int64(MISS))       ; write(fpa, form)
  form = @sprintf("%-24s %d\n",                         "wse",                        Int64(MISS))       ; write(fpa, form)
  form = @sprintf("%-24s %d\n",                     "wse_var",                        Int64(MISS))       ; write(fpa, form)
  form = @sprintf("%-24s %d\n",                       "width",                        Int64(MISS))       ; write(fpa, form)
  form = @sprintf("%-24s %d\n",                   "width_var",                        Int64(MISS))       ; write(fpa, form)
  form = @sprintf("%-24s %d\n",                     "n_nodes",                        Int64(MISS))       ; write(fpa, form)
  form = @sprintf("%-24s %d\n",                  "n_chan_max",                        Int64(MISS))       ; write(fpa, form)
  form = @sprintf("%-24s %d\n",                  "n_chan_mod",                        Int64(MISS))       ; write(fpa, form)
  form = @sprintf("%-24s %d\n",                  "obstr_type",                        Int64(MISS))       ; write(fpa, form)
  form = @sprintf("%-24s %d\n",                     "grod_id",                        Int64(MISS))       ; write(fpa, form)
  form = @sprintf("%-24s %d\n",                   "hfalls_id",                        Int64(MISS))       ; write(fpa, form)
  form = @sprintf("%-24s %d\n",                       "slope",                        Int64(MISS))       ; write(fpa, form)
  form = @sprintf("%-24s %d\n",                    "dist_out",                        Int64(MISS))       ; write(fpa, form)
  form = @sprintf("%-24s %d\n",                    "n_rch_up",                        Int64(MISS))       ; write(fpa, form)
  form = @sprintf("%-24s %d\n",                  "n_rch_down",                        Int64(MISS))       ; write(fpa, form)
  form = @sprintf("%-24s %d\n",                   "rch_id_up",                        Int64(MISS))       ; write(fpa, form)
  form = @sprintf("%-24s %d\n",                   "rch_id_dn",                        Int64(MISS))       ; write(fpa, form)
  form = @sprintf("%-24s %d\n",                    "lakeflag",                        Int64(MISS))       ; write(fpa, form)
  form = @sprintf("%-24s %d\n",                   "max_width",                        Int64(MISS))       ; write(fpa, form)
  form = @sprintf("%-24s %d\n",                        "type",                        Int64(MISS))       ; write(fpa, form)
  form = @sprintf("%-24s %d\n",                        "facc",                        Int64(MISS))       ; write(fpa, form)
  form = @sprintf("%-24s %d\n",                    "swot_obs",                        Int64(MISS))       ; write(fpa, form)
  form = @sprintf("%-24s %d\n",                 "swot_orbits",                        Int64(MISS))       ; write(fpa, form)
  form = @sprintf("%-24s %d\n",                  "river_name",                        Int64(MISS))       ; write(fpa, form)
  form = @sprintf("%-24s %s as of %s\n",      "SWORD_VERSION",           stver[1,1],  stver[1,2])        ; write(fpa, form)
  form = @sprintf(" ------------- WRF-Hydro ------------- \n")                                           ; write(fpa, form)
  form = @sprintf("%-24s %f %d\n",                 "LATITUDE",                 MISS,  Int64(MISS))       ; write(fpa, form)
  form = @sprintf("%-24s %f %d\n",                "LONGITUDE",                 MISS,  Int64(MISS))       ; write(fpa, form)
  form = @sprintf(" -------------- remarks -------------- \n")                                           ; write(fpa, form)
  for a = 1:rtreg                                                             # then add station data that are
    if streg[a,1] == stdat[ind,1]                                             # not of fixed length (sort before
      streg[a,2] === missing && (streg[a,2] = Int64(MISS))                    # saving some categories to file)
      streg[a,3] === missing && (streg[a,3] = Int64(MISS))
      form = @sprintf("%-24s %5d to %5d", "STN_REGULATION", streg[a,2], streg[a,3])
      streg[a,4] ==  0       && (form *= " Natural [0]")
      streg[a,4] ==  1       && (form *= " Regulated [1]")
      streg[a,4] === missing && (form *= " $MIST [missing]")
      form *= "\n" ; write(fpa, form)
    end
  end

  form = ""
  for a = 1:rtdcl
    if stdcl[a,1] == stdat[ind,1]
      stdcl[a,3] === missing && (stdcl[a,3] = Int64(MISS))
      stdcl[a,4] === missing && (stdcl[a,4] = Int64(MISS))
      form *= @sprintf("%-24s %5d to %5d", "STN_DATA_COLLECTION", stdcl[a,3], stdcl[a,4])
      stdcl[a,2] !== missing && stdcl[a,2] != "NA" && (form *= @sprintf(" %33s [%s]", stmatch(       "DATA_TYPES", stdcl[a,2]), stdcl[a,2]))
      stdcl[a,5] !== missing && stdcl[a,5] != "NA" && (form *= @sprintf(" %33s [%s]", stmatch("MEASUREMENT_CODES", stdcl[a,5]), stdcl[a,5]))
      stdcl[a,6] !== missing && stdcl[a,6] != "NA" && (form *= @sprintf(" %33s [%s]", stmatch(  "OPERATION_CODES", stdcl[a,6]), stdcl[a,6]))
      form *= "\n"
    end
  end
  form != "" && write(fpa, join(sort(split(form, "\n")[1:end-1]), "\n") * "\n")

  form = ""
  for a = 1:rtsos
    if stsos[a,1] == stdat[ind,1]
      stsos[a,4] === missing && (stsos[a,4] = MIST)
      stsos[a,5] === missing && (stsos[a,5] = MIST)
      form *= @sprintf("%-24s %22s [%s] during %d from month %s to %s\n", "STN_OPERATION_SCHEDULE", stmatch("DATA_TYPES", stsos[a,2]), stsos[a,2], stsos[a,3], stsos[a,4], stsos[a,5])
    end
  end
  form != "" && write(fpa, join(sort(split(form, "\n")[1:end-1]), "\n") * "\n")

  for a = 1:rtsdc
    if stsdc[a,1] == stdat[ind,1]
      form = @sprintf("%-24s %11.5f from %s [%d] to %s [%d]\n", "STN_DATUM_CONVERSION", stsdc[a,4], stmatch("DATUM_LIST", stsdc[a,2]), stsdc[a,2], stmatch("DATUM_LIST", stsdc[a,3]), stsdc[a,3])
      write(fpa, form)
    end
  end

  form = ""
  for a = 1:rtrem
    if strem[a,1] == stdat[ind,1]
      strem[a,3] === missing && (strem[a,3] = Int64(MISS))
      strem[a,4] === missing && (strem[a,4] = strem[a,5]) ; strem[a,4] === missing && (strem[a,4] = "missing_remark")
      form *= @sprintf("%-24s %5d %s [%s]\n", "STN_REMARKS", strem[a,3], nonewln(strem[a,4]), stmatch("STN_REMARK_CODES", strem[a,2]))
    end
  end
  form != "" && write(fpa, join(sort(split(form, "\n")[1:end-1]), "\n") * "\n")

  for a = 1:rtsdu
    if stsdu[a,1] == stdat[ind,1]
      stsdu[a,3] === missing && (stsdu[a,3] = MIST)
      stsdu[a,4] === missing && (stsdu[a,4] = MIST)
      form = @sprintf("%-24s %s [%d] from %s to %s\n", "STN_DATUM_UNRELATED", stmatch("DATUM_LIST", stsdu[a,2]), stsdu[a,2], stsdu[a,3], stsdu[a,4])
      write(fpa, form)
    end
  end

  form = ""
  for a = 1:rtrng
    if strng[a,1] == stdat[ind,1]
      strng[a,4] === missing && (strng[a,4] = Int64(MISS))
      strng[a,5] === missing && (strng[a,5] = Int64(MISS))
      form *= @sprintf("%-24s %5d to %5d", "STN_DATA_RANGE", strng[a,4], strng[a,5])
      strng[a,2] != "NA" && (form *= @sprintf(" %33s [%s]", stmatch(    "DATA_TYPES", strng[a,2]), strng[a,2]))
      strng[a,3] != "NA" && (form *= @sprintf(" %33s [%s]", stmatch("SED_DATA_TYPES", strng[a,3]), strng[a,3]))
      form *= "\n"
    end
  end
  form != "" && write(fpa, join(sort(split(form, "\n")[1:end-1]), "\n") * "\n")
  close(fpa)
end

tnam = "hydat_zzzzzzz.nc"                                                     # construct a netcdf template
vars = ["flow", "floi", "levl", "levi", "sedc", "sedi", "load", "loai"]       # for a fixed time interval
daya = @sprintf("%s-01-01-12", YBEG) ; tima = My.datesous("1900-01-01-00", daya, "hr")
dayb = @sprintf("%s-12-31-12", YEND) ; timb = My.datesous("1900-01-01-00", dayb, "hr")
days = collect(tima:DELT:timb) ; nday = length(days)
My.nccreer(   tnam, nday, 1, 1, MISS; vnames = vars)
ncputatt(     tnam, "time", Dict("units" => "hours since 1900-01-01 00:00:0.0"))
ncwrite(days, tnam, "time", start=[1], count=[-1])

function obstype(code::AbstractString)                                        # assume that each measurement
  cval = GOOD                                                                 # is a good value by default
  code == "A" && (cval = -1.0)                                                # or partial day otherwise,
  code == "B" && (cval = -2.0)                                                # or ice conditions,
  code == "D" && (cval = -3.0)                                                # or dry,
  code == "E" && (cval = -4.0)                                                # or estimated,
  code == "S" && (cval = -5.0)                                                # or sample(s) taken
  cval
end
function obstype(code::Missing)  GOOD  end

function dywrite(ind::Int64)                                                  # and save each daily timeseries
  stnam = "hydat_" * stdat[ind,1] * ".nc"
  print("cp $tnam $stnam\n") ; cp(tnam, stnam; force = true)
  numb = zeros(VNUM)

  stvar = "DLY_FLOWS"  ; indv = FLOW ; indd = 12 ; indn = 2
  vara  = vars[indv  ] ; data = fill(MISS, nday)
  vari  = vars[indv+1] ; dati = fill(MISS, nday)
  daily = DBInterface.execute(stall, "SELECT * FROM $stvar WHERE STATION_NUMBER='$(stdat[ind,1])'") |> DataFrame ; raily, caily = size(daily)
  for a = 1:raily
    if YBEG <=  daily[a,2] <= YEND
      ynow = "$(daily[a,2])"
      mnow = "$(daily[a,3])" ; mnow = (length(mnow) == 1 ? "0" : "") * mnow
      dnow = "$ynow-$mnow-01-12" ; inow = indd ; mref = mnow
      while mref == mnow
        dref = My.datesous("1900-01-01-00", dnow, "hr")
        dind = findfirst(isequal(dref), days)
        if daily[a,inow] !== missing
          data[dind]  =         daily[a,inow]
          dati[dind]  = obstype(daily[a,inow+1])
          numb[indv] += 1
        end
#       line = @sprintf("%s flow is %f [%f]\n", dnow, data[dind], dati[dind]) ; print(line)
        dnow = My.dateadd(dnow, 1, "dy")
        mnow = dnow[6:7] ; inow += indn
      end
    end
  end
  ncwrite(data, stnam, vara, start=[1,1,1], count=[-1,-1,-1])
  ncwrite(dati, stnam, vari, start=[1,1,1], count=[-1,-1,-1])

  stvar = "DLY_LEVELS"  ; indv = LEVL ; indd = 13 ; indn = 2
  vara  = vars[indv  ] ; data = fill(MISS, nday)
  vari  = vars[indv+1] ; dati = fill(MISS, nday)
  daily = DBInterface.execute(stall, "SELECT * FROM $stvar WHERE STATION_NUMBER='$(stdat[ind,1])'") |> DataFrame ; raily, caily = size(daily)
  for a = 1:raily
    if YBEG <=  daily[a,2] <= YEND
      ynow = "$(daily[a,2])"
      mnow = "$(daily[a,3])" ; mnow = (length(mnow) == 1 ? "0" : "") * mnow
      dnow = "$ynow-$mnow-01-12" ; inow = indd ; mref = mnow
      while mref == mnow
        dref = My.datesous("1900-01-01-00", dnow, "hr") 
        dind = findfirst(isequal(dref), days)
        if daily[a,inow] !== missing
          data[dind]  =         daily[a,inow]
          dati[dind]  = obstype(daily[a,inow+1])
          numb[indv] += 1
        end
        dnow = My.dateadd(dnow, 1, "dy")
        mnow = dnow[6:7] ; inow += indn
      end
    end
  end
  ncwrite(data, stnam, vara, start=[1,1,1], count=[-1,-1,-1])
  ncwrite(dati, stnam, vari, start=[1,1,1], count=[-1,-1,-1])

  stvar = "SED_DLY_SUSCON"  ; indv = SEDC ; indd = 11 ; indn = 2
  vara  = vars[indv  ] ; data = fill(MISS, nday)
  vari  = vars[indv+1] ; dati = fill(MISS, nday)
  daily = DBInterface.execute(stall, "SELECT * FROM $stvar WHERE STATION_NUMBER='$(stdat[ind,1])'") |> DataFrame ; raily, caily = size(daily)
  for a = 1:raily
    if YBEG <=  daily[a,2] <= YEND
      ynow = "$(daily[a,2])"
      mnow = "$(daily[a,3])" ; mnow = (length(mnow) == 1 ? "0" : "") * mnow
      dnow = "$ynow-$mnow-01-12" ; inow = indd ; mref = mnow
      while mref == mnow
        dref = My.datesous("1900-01-01-00", dnow, "hr") 
        dind = findfirst(isequal(dref), days)
        if daily[a,inow] !== missing
          data[dind]  =         daily[a,inow]
          dati[dind]  = obstype(daily[a,inow+1])
          numb[indv] += 1
        end
        dnow = My.dateadd(dnow, 1, "dy")
        mnow = dnow[6:7] ; inow += indn
      end
    end
  end
  ncwrite(data, stnam, vara, start=[1,1,1], count=[-1,-1,-1])
  ncwrite(dati, stnam, vari, start=[1,1,1], count=[-1,-1,-1])

  stvar = "SED_DLY_LOADS"  ; indv = LOAD ; indd = 12 ; indn = 1
  vara  = vars[indv  ] ; data = fill(MISS, nday)
  vari  = vars[indv+1] ; dati = fill(MISS, nday)
  daily = DBInterface.execute(stall, "SELECT * FROM $stvar WHERE STATION_NUMBER='$(stdat[ind,1])'") |> DataFrame ; raily, caily = size(daily)
  for a = 1:raily
    if YBEG <=  daily[a,2] <= YEND
      ynow = "$(daily[a,2])"
      mnow = "$(daily[a,3])" ; mnow = (length(mnow) == 1 ? "0" : "") * mnow
      dnow = "$ynow-$mnow-01-12" ; inow = indd ; mref = mnow
      while mref == mnow
        dref = My.datesous("1900-01-01-00", dnow, "hr")
        dind = findfirst(isequal(dref), days)
        if daily[a,inow] !== missing
          data[dind]  = daily[a,inow]
          dati[dind]  = 1.0
          numb[indv] += 1
        end
        dnow = My.dateadd(dnow, 1, "dy")
        mnow = dnow[6:7] ; inow += indn
      end
    end
  end
  ncwrite(data, stnam, vara, start=[1,1,1], count=[-1,-1,-1])
  ncwrite(dati, stnam, vari, start=[1,1,1], count=[-1,-1,-1])

  stfil = "hydat_" * stdat[ind,1] * ".sta" ; fpa = My.ouvre(stfil, "r")       # finally insert time coverage
  sttmp = "hydat_" * stdat[ind,1] * ".tmp" ; fpb = My.ouvre(sttmp, "w")       # for each netcdf variable in
  lines = readlines(fpa; keep = true)      ;   c = length(lines)              # the corresponding text file
  for a = 1:14  form = lines[a] ; write(fpb, form)  end
  form = @sprintf("%-24s %6d %6d %7.3f\n", "$YBEG-$YEND.DLY_FLOWS",      numb[FLOW], nday, 100 * numb[FLOW] / nday) ; write(fpb, form)
  form = @sprintf("%-24s %6d %6d %7.3f\n", "$YBEG-$YEND.DLY_LEVELS",     numb[LEVL], nday, 100 * numb[LEVL] / nday) ; write(fpb, form)
  form = @sprintf("%-24s %6d %6d %7.3f\n", "$YBEG-$YEND.SED_DLY_SUSCON", numb[SEDC], nday, 100 * numb[SEDC] / nday) ; write(fpb, form)
  form = @sprintf("%-24s %6d %6d %7.3f\n", "$YBEG-$YEND.SED_DLY_LOADS",  numb[LOAD], nday, 100 * numb[LOAD] / nday) ; write(fpb, form)
  for a = 19:c  form = lines[a] ; write(fpb, form)  end
  close(fpa) ; close(fpb) ; mv(sttmp, stfil; force=true)
end

if argc == 1                                                                  # either unpack all stations
  for a = 1:rtdat
    stwrite(a)
#   dywrite(a)
  end
else                                                                          # or just the station of interest
  for a = 1:rtdat
    if in(stdat[a,1], ARGS)
      stwrite(a)
      dywrite(a)
    end
  end
end
exit(0)



#=
mons = Array{Float64}(undef,0)
mona = @sprintf("%s-01-15-12", YBEG) ; tima = My.datesous("1900-01-01-00", mona, "hr")
monb = @sprintf("%s-12-15-12", YBEG) ; timb = My.datesous("1900-01-01-00", monb, "hr")
while tima <= timb
  global tima, mona
  push!(mons, tima)
  mona = My.dateadd(mona, 30, "dy")
  mona = mona[1:7] * "-15-12"
  tima = My.datesous("1900-01-01-00", mona, "hr")
end
nmon = length(mons)

# "ANNUAL_INSTANT_PEAKS", "ANNUAL_STATISTICS", "DLY_FLOWS", "DLY_LEVELS", "SED_DLY_SUSCON", "SED_DLY_LOADS", "SED_SAMPLES", "SED_SAMPLES_PSD"
# if stdat[ind,1] == "01DB002" # "08EC004"
# strem = DBInterface.execute(stall, "SELECT * FROM            STN_REMARKS WHERE STATION_NUMBER='$(stdat[ind,1])'") |> DataFrame ; rtrem, ctrem = size(strem)
# strng = DBInterface.execute(stall, "SELECT * FROM         STN_DATA_RANGE WHERE STATION_NUMBER='$(stdat[ind,1])'") |> DataFrame ; rtrng, ctrng = size(strng)
# streg = DBInterface.execute(stall, "SELECT * FROM         STN_REGULATION WHERE STATION_NUMBER='$(stdat[ind,1])'") |> DataFrame ; rtreg, ctreg = size(streg)
# stdcl = DBInterface.execute(stall, "SELECT * FROM    STN_DATA_COLLECTION WHERE STATION_NUMBER='$(stdat[ind,1])'") |> DataFrame ; rtdcl, ctdcl = size(stdcl)

# stdat = DBInterface.execute(stall, "SELECT * FROM               STATIONS WHERE STATION_NUMBER='08EC004'") |> DataFrame ; rtdat, ctdat = size(stdat)
# strem = DBInterface.execute(stall, "SELECT * FROM            STN_REMARKS WHERE STATION_NUMBER='08EC004'") |> DataFrame ; rtrem, ctrem = size(strem)
# strng = DBInterface.execute(stall, "SELECT * FROM         STN_DATA_RANGE WHERE STATION_NUMBER='08EC004'") |> DataFrame ; rtrng, ctrng = size(strng)
# streg = DBInterface.execute(stall, "SELECT * FROM         STN_REGULATION WHERE STATION_NUMBER='08EC004'") |> DataFrame ; rtreg, ctreg = size(streg)
# stdcl = DBInterface.execute(stall, "SELECT * FROM    STN_DATA_COLLECTION WHERE STATION_NUMBER='08EC004'") |> DataFrame ; rtdcl, ctdcl = size(stdcl)
# stdcl = DBInterface.execute(stall, "SELECT * FROM    STN_DATA_COLLECTION WHERE STATION_NUMBER='01DB002'") |> DataFrame ; rtdcl, ctdcl = size(stdcl)

#stcod = ARGS[2]
#stnam = ARGS[3]

tables = ["STATIONS", "CONCENTRATION_SYMBOLS", "SED_SAMPLES_PSD", "ANNUAL_INSTANT_PEAKS", "STN_DATUM_UNRELATED", "DATA_SYMBOLS", "SED_VERTICAL_LOCATION", "STN_DATA_COLLECTION", "PEAK_CODES", "SED_DATA_TYPES", "MEASUREMENT_CODES", "SED_VERTICAL_SYMBOLS", "DATA_TYPES", "DLY_FLOWS", "STN_REMARKS", "STN_DATUM_CONVERSION", "AGENCY_LIST", "SED_DLY_SUSCON", "STN_OPERATION_SCHEDULE", "STN_DATA_RANGE", "PRECISION_CODES", "SED_DLY_LOADS", "DLY_LEVELS", "OPERATION_CODES", "STN_REGULATION", "DATUM_LIST", "ANNUAL_STATISTICS", "VERSION", "REGIONAL_OFFICE_LIST", "SAMPLE_REMARK_CODES", "STN_REMARK_CODES", "SED_SAMPLES", "STN_STATUS_CODES"]

SQLite.Query(SQLite.Stmt(SQLite.DB("Hydat.sqlite3"), 151), Base.RefValue{Int32}(100), [:STATION_NUMBER, :STATION_NAME, :PROV_TERR_STATE_LOC, :REGIONAL_OFFICE_ID, :HYD_STATUS, :SED_STATUS, :LATITUDE, :LONGITUDE, :DRAINAGE_AREA_GROSS, :DRAINAGE_AREA_EFFECT, :RHBN, :REAL_TIME, :CONTRIBUTOR_ID, :OPERATOR_ID, :DATUM_ID], Type[Union{Missing, String}, Union{Missing, String}, Union{Missing, String}, Union{Missing, String}, Union{Missing, String}, Union{Missing, String}, Union{Missing, Float64}, Union{Missing, Float64}, Union{Missing, Float64}, Union{Missing, Float64}, Union{Missing, Int64}, Union{Missing, Int64}, Union{Missing, Int64}, Union{Missing, Int64}, Union{Missing, Int64}], Dict(:REAL_TIME => 12,:RHBN => 11,:LATITUDE => 7,:STATION_NUMBER => 1,:LONGITUDE => 8,:OPERATOR_ID => 14,:CONTRIBUTOR_ID => 13,:SED_STATUS => 6,:HYD_STATUS => 5,:REGIONAL_OFFICE_ID => 4…), Base.RefValue{Int64}(0))
CSV.write(stcod * ".csv", y)

z = SQLite.tables(db) ; z[33].name ; length(z[33].schema.names) ; z[33].schema.names[1]

y = DBInterface.execute(db, "SELECT * FROM 'CONCENTRATION_SYMBOLS'")
CSV.write("CONCENTRATION_SYMBOLS.csv", y)
y = DBInterface.execute(db, "SELECT * FROM 'SED_SAMPLES_PSD'")
CSV.write("SED_SAMPLES_PSD.csv", y)

# SQLite.execute(db, "SELECT * from Student WHERE Name='Simon'")
#SQLite.tables(db)
#x = DBInterface.execute(db, "SELECT * FROM STATIONS ORDER BY LATITUDE LIMIT 3")
#x = DBInterface.execute(db, "SELECT * FROM STATIONS ORDER BY LATITUDE")
x = DBInterface.execute(db, "SELECT * FROM STATIONS WHERE PROV_TERR_STATE_LOC='QC' ORDER BY DRAINAGE_AREA_GROSS")
CSV.write("Hydat_station_list_drainage_QC.csv", x)
exit(0)

#y = DBInterface.execute(db, "SELECT '_rowid_',* FROM 'main'.'DLY_FLOWS' ORDER BY '_rowid_' ASC LIMIT 0, 49999")
#y = DBInterface.execute(db, "SELECT * FROM 'main'.'DLY_FLOWS' WHERE STATION_NUMBER='02SB005'")
y = DBInterface.execute(db, "SELECT * FROM 'main'.'DLY_FLOWS' WHERE STATION_NUMBER='$stcod'")
CSV.write(stcod * ".csv", y)
exit(0)




STATION_NUMBER,STATION_NAME,                       PROV_TERR_STATE_LOC,REGIONAL_OFFICE_ID,HYD_STATUS,SED_STATUS,        LATITUDE,         LONGITUDE,DRAINAGE_AREA_GROSS,DRAINAGE_AREA_EFFECT,RHBN,REAL_TIME,CONTRIBUTOR_ID,OPERATOR_ID,DATUM_ID
       03JB004,FEUILLES (RIVIERE AUX) EN AMONT DU RUISSEAU DUFREBOY,QC,                 6,         A,          ,58.2841682434082,-71.28250122070312,            33900.0,                    ,   0,        1,           740,        740,

https://www.geeksforgeeks.org/working-with-databases-in-julia

numb = 0                                                                      # for each available six.nc file of a
absdir = abspath(srcdir)                                                      # given size, read the hdr bounding box
srcfil = readdir(srcdir)
for file in srcfil
  if endswith(file, nctail)
    full = joinpath(absdir, file)
    if filesize(full) > 10^5
      hdr  = joinpath(absdir, file[1:end-6] * "hdr")
      fpa  = My.ouvre(hdr, "r") ; lins = readlines(fpa) ; close(fpa)
      lata = parse(Float64, split(lins[11])[2])
      lona = parse(Float64, split(lins[12])[2])
      latb = parse(Float64, split(lins[13])[2])
      lonb = parse(Float64, split(lins[14])[2])
      latc = parse(Float64, split(lins[15])[2])
      lonc = parse(Float64, split(lins[16])[2])                               # and append this box and full file name
      latd = parse(Float64, split(lins[17])[2])                               # to a corresponding daily text file
      lond = parse(Float64, split(lins[18])[2])
      full = srcdir * file
      (file[18:19] == "HH" || file[18:19] == "DH" || file[18:19] == "QP") && (vari = "sohh")
      (file[18:19] == "VV" || file[18:19] == "DV")                        && (vari = "sovv")
       file[18:19] == "HV"                                                && (vari = "sohv")
       file[18:19] == "VH"                                                && (vari = "sovh")
       file[18:19] == "CD"                                                && (vari = "soch")
      line = @sprintf("%15.9f %15.9f %15.9f %15.9f %15.9f %15.9f %15.9f %15.9f %s %s\n",
                      lata, lona, latb, lonb, latc, lonc, latd, lond, full, vari)
      fpb  = My.ouvre(file[1:10] * ".sarbox", "a") ; print(fpb, line) ; close(fpb)
      global numb += 1
    end
  end
end
@printf("\nwrote %d SAR bounding boxes\n", numb)
exit(0)
=#
