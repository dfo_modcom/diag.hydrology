#=
 = Read WRF-Hydro streamflow and construct daily, monthly, annual,
 = decadal, and annual-average-daily timeseries - RD Mar 2024.
 =#

using My, Printf, NetCDF

const RAWW             = 1                              # raw variable
const VARR             = 2                              # and its variance
const TOTL             = 3                              # and valid data count
const DNUM             = 3                              # number of variable types
const DELT             = 3                              # source data timestep (hours)
const DELD             = 24                             # daily       timestep (hours)

const DELMISS          = -8e9                           # generic missing value comparison
const GRDMISS          = -9e9                           # generic missing value on a grid
const MISS             = -9999.0                        # generic missing value

if (argc = length(ARGS)) != 1
  print("\nUsage: jjj $(basename(@__FILE__)) run_DOMAIN_full_full_FORCING.w85_1990-01-01_2099-12-30_3h_Severn_Hudson.nc\n\n")
  exit(1)
end
fila = ARGS[1]                                                                # construct file names,
filb = ARGS[1][1:end-2] *   "daily.txt"                                       # full timeseries dates,
filc = ARGS[1][1:end-2] * "monthly.txt"                                       # and allocate data arrays
fild = ARGS[1][1:end-2] *  "annual.txt"
file = ARGS[1][1:end-2] * "decadal.txt"
filf = ARGS[1][1:end-2] *  "annday.txt"
filg = ARGS[1][1:end-2] *  "decday.txt"

tims = ncread(fila,     "time", start=[1],     count=[-1])                    # read the time-invariant data
xind = ncread(fila, "rivgridx", start=[1,1],   count=[-1,-1])                 # and DELT-time-resolution flow
yind = ncread(fila, "rivgridy", start=[1,1],   count=[-1,-1])
rlat = ncread(fila, "riverlat", start=[1,1],   count=[-1,-1])
rlon = ncread(fila, "riverlon", start=[1,1],   count=[-1,-1])
olat = ncread(fila, "oceanlat", start=[1,1],   count=[-1,-1])
olon = ncread(fila, "oceanlon", start=[1,1],   count=[-1,-1])
#flow = ncread(fila, "riverflo", start=[1,1,1], count=[-1,-1,-1])
tatt = ncgetatt(fila, "time", "units")
ntim = length(tims)
nlet = length(xind)

tima = tims[  1] ; tmpa = dateref(tima, tatt)
timb = tims[end] ; tmpb = dateref(timb, tatt)
hora = tmpa[1:4] * "-" * tmpa[5:6] * "-" * tmpa[7:8] * "-" * tmpa[9:10]
horb = tmpb[1:4] * "-" * tmpb[5:6] * "-" * tmpb[7:8] * "-" * tmpb[9:10]
hors = collect(tima:DELT:timb) ; nhor = length(hors)
dath = zeros(DNUM, nlet, nhor)
dath[RAWW,1:nlet,1:nhor] = ncread(fila, "riverflo", start=[1,1,1], count=[-1,-1,-1])
#any(x -> (x < DELMISS || x > -DELMISS), dath) && error("\nERROR : $fila contains missing data\n\n")

daya = hora[1:10] * "-00" ; tima = My.datesous("1900-01-01-00", daya, "hr")   # daily dates and data
dayb = horb[1:10] * "-00" ; timb = My.datesous("1900-01-01-00", dayb, "hr")
days = collect(tima:DELD:timb) ; nday = length(days)
datd = zeros(DNUM, nlet, nday)

mons = Array{Float64}(undef,0)                                                # monthly dates and data
mona = hora[1:7] * "-15-00" ; tima = My.datesous("1900-01-01-00", mona, "hr")
monb = horb[1:7] * "-15-00" ; timb = My.datesous("1900-01-01-00", monb, "hr")
while tima <= timb
  global tima, mona
  push!(mons, tima)
  mona = dateadd(mona, 30, "dy")
  mona = mona[1:7] * "-15-00"
  tima = My.datesous("1900-01-01-00", mona, "hr")
end
nmon = length(mons)
datm = zeros(DNUM, nlet, nmon)

anns = Array{Float64}(undef,0)                                                # annual dates and data
anna = hora[1:4] * "-07-01-00" ; tima = My.datesous("1900-01-01-00", anna, "hr")
annb = horb[1:4] * "-07-01-00" ; timb = My.datesous("1900-01-01-00", annb, "hr")
while tima <= timb
  global tima, anna
  push!(anns, tima)
  anna = dateadd(anna, 350, "dy")
  anna = anna[1:4] * "-07-01-00"
  tima = My.datesous("1900-01-01-00", anna, "hr")
end
nann = length(anns)
data = zeros(DNUM, nlet, nann)

decs = Array{Float64}(undef,0)  ; dddd = Array{AbstractString}(undef,0)       # decadal dates and data
deca = hora[1:3] * "5-01-01-00" ; tima = My.datesous("1900-01-01-00", deca, "hr")
decb = horb[1:3] * "5-01-01-00" ; timb = My.datesous("1900-01-01-00", decb, "hr")
while tima <= timb
  global tima, deca
  push!(decs, tima)
  push!(dddd,         deca[1:3])
  tmpc = parse(Int64, deca[1:3]) + 1
  deca = @sprintf("%d", tmpc) * "5-01-01-00"
  tima = My.datesous("1900-01-01-00", deca, "hr")
end
ndec = length(decs)
datc = zeros(DNUM, nlet, ndec)

adya = hora[1:4] * "-01-01-00" ; tima = My.datesous("1900-01-01-00", adya, "hr")  # and  annual-average
adyb = hora[1:4] * "-12-31-00" ; timb = My.datesous("1900-01-01-00", adyb, "hr")  # and decadal-average
adys = collect(tima:DELD:timb) ; nady = length(adys)                              # daily
daty = zeros(DNUM, nlet,       nady)
dafy = zeros(DNUM, nlet, ndec, nady)

hnow = hora                                                                   # then construct each timeseries
for a = 1:nhor                                                                # (but skip Feb 29)
  global hnow ; local yind
  if hnow[5:10] != "-02-29"
    dnow = My.datesous("1900-01-01-00",             hnow[1:10] *       "-00", "hr") ; dind = findfirst(isequal(dnow), days)
    mnow = My.datesous("1900-01-01-00",             hnow[1: 7] *    "-15-00", "hr") ; mind = findfirst(isequal(mnow), mons)
    anow = My.datesous("1900-01-01-00",             hnow[1: 4] * "-07-01-00", "hr") ; aind = findfirst(isequal(anow), anns)
    cnow =                                          hnow[1: 3]                      ; cind = findfirst(isequal(cnow), dddd)
    ynow = My.datesous("1900-01-01-00", hora[1:4] * hnow[5:10] *       "-00", "hr") ; yind = findfirst(isequal(ynow), adys)
    for b = 1:nlet
      datd[RAWW,b,dind]      += dath[RAWW,b,a] ; datd[VARR,b,dind]      += dath[RAWW,b,a]^2 ; datd[TOTL,b,dind]      += 1
      datm[RAWW,b,mind]      += dath[RAWW,b,a] ; datm[VARR,b,mind]      += dath[RAWW,b,a]^2 ; datm[TOTL,b,mind]      += 1
      data[RAWW,b,aind]      += dath[RAWW,b,a] ; data[VARR,b,aind]      += dath[RAWW,b,a]^2 ; data[TOTL,b,aind]      += 1
      datc[RAWW,b,cind]      += dath[RAWW,b,a] ; datc[VARR,b,cind]      += dath[RAWW,b,a]^2 ; datc[TOTL,b,cind]      += 1
      daty[RAWW,b,     yind] += dath[RAWW,b,a] ; daty[VARR,b,     yind] += dath[RAWW,b,a]^2 ; daty[TOTL,b,     yind] += 1
      dafy[RAWW,b,cind,yind] += dath[RAWW,b,a] ; dafy[VARR,b,cind,yind] += dath[RAWW,b,a]^2 ; dafy[TOTL,b,cind,yind] += 1
    end
  end
  hnow = My.dateadd(hnow, DELT, "hr")
end

vars = ["riverflo"]
varz = ["riverstd"]

function nccreate(fn::AbstractString, ntim::Int, nlev::Int, nlat::Int, nlon::Int, missing::Float64)
               nctim = NcDim( "time", ntim, atts = Dict{Any,Any}("units"=>"hours since 1900-01-01 00:00:0.0"), values = collect(range(    0, stop =                  ntim - 1 , length = ntim)))
  nlev > 0 && (nclev = NcDim("level", nlev, atts = Dict{Any,Any}("units"=>                           "level"), values = collect(range(    1, stop =                  nlev     , length = nlev))))
               nclat = NcDim(  "lat", nlat, atts = Dict{Any,Any}("units"=>                   "degrees_north"), values = collect(range( 50.0, stop =  50.0 + 0.001 * (nlat - 1), length = nlat)))
               nclon = NcDim(  "lon", nlon, atts = Dict{Any,Any}("units"=>                    "degrees_east"), values = collect(range(280.0, stop = 280.0 + 0.001 * (nlon - 1), length = nlon)))
               ncvrs = Array{NetCDF.NcVar}(undef, 7)
               ncvrs[1] = NcVar("rivgridx", [nclon, nclat              ], atts = Dict{Any,Any}("units"=>"none",   "long_name"=>"river outlet x-index on WRF-Hydro grid", "missing_value"=>missing), t=  Int64, compress=-1)
               ncvrs[2] = NcVar("rivgridy", [nclon, nclat              ], atts = Dict{Any,Any}("units"=>"none",   "long_name"=>"river outlet y-index on WRF-Hydro grid", "missing_value"=>missing), t=  Int64, compress=-1)
               ncvrs[3] = NcVar("riverlat", [nclon, nclat              ], atts = Dict{Any,Any}("units"=>"degree", "long_name"=>"river outlet latitude",                  "missing_value"=>missing), t=Float64, compress=-1)
               ncvrs[4] = NcVar("riverlon", [nclon, nclat              ], atts = Dict{Any,Any}("units"=>"degree", "long_name"=>"river outlet longitude",                 "missing_value"=>missing), t=Float64, compress=-1)
               ncvrs[5] = NcVar("oceanlat", [nclon, nclat              ], atts = Dict{Any,Any}("units"=>"degree", "long_name"=>"ocean pour point latitude",              "missing_value"=>missing), t=Float64, compress=-1)
               ncvrs[6] = NcVar("oceanlon", [nclon, nclat              ], atts = Dict{Any,Any}("units"=>"degree", "long_name"=>"ocean pour point longitude",             "missing_value"=>missing), t=Float64, compress=-1)
  nlev < 1 && (ncvrs[7] = NcVar("riverflo", [nclon, nclat,        nctim], atts = Dict{Any,Any}("units"=>"m3 s-1", "long_name"=>"river outlet streamflow",                "missing_value"=>missing), t=Float64, compress=-1))
  nlev > 0 && (ncvrs[7] = NcVar("riverflo", [nclon, nclat, nclev, nctim], atts = Dict{Any,Any}("units"=>"m3 s-1", "long_name"=>"river outlet streamflow",                "missing_value"=>missing), t=Float64, compress=-1))
#              ncvrs[8] = NcVar("riverstd", [nclon, nclat,        nctim], atts = Dict{Any,Any}("units"=>"m3 s-1", "long_name"=>"river outlet streamflow standard dev",   "missing_value"=>missing), t=Float64, compress=-1)
  descript = "WRF-Hydro streamflow timeseries for a single oceanic river outlet on each proxy latitude, where true outlet positions (riverlat/lon) " *
             "as well as ocean pour points that are slightly displaced into the ocean (oceanlat/lon), are given as corresponding time-invariants"
  ncfil = NetCDF.create(fn, ncvrs, gatts = Dict{Any,Any}("Description"=>descript), mode = NC_NETCDF4)
  nlev < 1 && print("\ncreated $fn with $ntim times            $nlat lats and $nlon lons\n\n")
  nlev > 0 && print("\ncreated $fn with $ntim times $nlev levs $nlat lats and $nlon lons\n\n")
end

for z = 1:6                                                                   # and store the timeseries
  z == 1 && (znam = filb ; zzss = days ; zznn = nday ; datz = datd ; levz =    0 ; ptxt = false)
  z == 2 && (znam = filc ; zzss = mons ; zznn = nmon ; datz = datm ; levz =    0 ; ptxt =  true)
  z == 3 && (znam = fild ; zzss = anns ; zznn = nann ; datz = data ; levz =    0 ; ptxt =  true)
  z == 4 && (znam = file ; zzss = decs ; zznn = ndec ; datz = datc ; levz =    0 ; ptxt =  true)
  z == 5 && (znam = filf ; zzss = adys ; zznn = nady ; datz = daty ; levz =    0 ; ptxt =  true)
  z == 6 && (znam = filg ; zzss = adys ; zznn = nady ; datz = dafy ; levz = ndec ; ptxt =  true)

  if levz == 0
    sumz = zeros(1, 1, zznn)
    ptxt && (fpa = My.ouvre(znam, "w"))
    for a = 1:zznn
      ptxt && (line = @sprintf("%s", My.dateadd("1900-01-01-00", zzss[a], "hr")))
      for b = 1:nlet
        if datz[TOTL,b,a]  < 2
           datz[RAWW,b,a]  = MISS
           datz[VARR,b,a]  = MISS
          print("WARNING: sumz/latsum missing river $b at time $a\n")
        else
           datz[VARR,b,a]  = (datz[VARR,b,a] - datz[RAWW,b,a]^2 / datz[TOTL,b,a]) / (datz[TOTL,b,a] - 1)
           datz[RAWW,b,a] /=                                                         datz[TOTL,b,a]
           sumz[   1,1,a] +=  datz[RAWW,b,a]
        end
#       ptxt && (line *= @sprintf(" %15.8f", datz[RAWW,b,a]))
      end
#     ptxt && (line *=                 "\n"               ; print(fpa, line))
      ptxt && (line *= @sprintf(" %15.8f\n", sumz[1,1,a]) ; print(fpa, line))
    end
    ptxt && close(fpa)

    znam = znam[1:end-4] * ".nc"
    nccreate(znam, zznn, levz, nlet, 1, MISS)
    ncwrite(datz[RAWW,:,:], znam, vars[1], start=[1,1,1], count=[-1,-1,-1])
#   ncwrite(datz[VARR,:,:], znam, varz[1], start=[1,1,1], count=[-1,-1,-1])
    ncwrite(zzss, znam, "time", start=[1], count=[-1])
    ncputatt(     znam, "time", Dict("units" => "hours since 1900-01-01 00:00:0.0"))

    znam = znam[1:end-3] * ".latsum.nc"
    nccreate(znam, zznn, levz,    1, 1, MISS)
    ncwrite(sumz          , znam, vars[1], start=[1,1,1], count=[-1,-1,-1])
    ncwrite(zzss, znam, "time", start=[1], count=[-1])
    ncputatt(     znam, "time", Dict("units" => "hours since 1900-01-01 00:00:0.0"))
  else
    sumz = zeros(1, 1, levz, zznn)
    ptxt && (fpa = My.ouvre(znam, "w"))
    for a = 1:zznn
      ptxt && (line = @sprintf("%s", My.dateadd("1900-01-01-00", zzss[a], "hr")))
      for b = 1:levz, c = 1:nlet
        if datz[TOTL,c,b,a]  < 2
           datz[RAWW,c,b,a]  = MISS
           datz[VARR,c,b,a]  = MISS
          print("WARNING: sumz/latsum missing river $c at level $b and time $a\n")
        else
           datz[VARR,c,b,a]  = (datz[VARR,c,b,a] - datz[RAWW,c,b,a]^2 / datz[TOTL,c,b,a]) / (datz[TOTL,c,b,a] - 1)
           datz[RAWW,c,b,a] /=                                                               datz[TOTL,c,b,a]
           sumz[   1,1,b,a] +=  datz[RAWW,c,b,a]
        end
#       ptxt && (line *= @sprintf(" %15.8f", datz[RAWW,c,b,a]))
      end
#     ptxt && (line *=                        "\n"                                   ; print(fpa, line))
      ptxt && (line *= @sprintf(" %15.8f %15.8f\n", sumz[1,1,1,a], sumz[1,1,levz,a]) ; print(fpa, line))
    end
    ptxt && close(fpa)

    znam = znam[1:end-4] * ".nc"
    nccreate(znam, zznn, levz, nlet, 1, MISS)
    ncwrite(datz[RAWW,:,:,:], znam, vars[1], start=[1,1,1,1], count=[-1,-1,-1,-1])
#   ncwrite(datz[VARR,:,:,:], znam, varz[1], start=[1,1,1,1], count=[-1,-1,-1,-1])
    ncwrite(zzss, znam, "time", start=[1], count=[-1])
    ncputatt(     znam, "time", Dict("units" => "hours since 1900-01-01 00:00:0.0"))

    znam = znam[1:end-3] * ".latsum.nc"
    nccreate(znam, zznn, levz,    1, 1, MISS)
    ncwrite(sumz            , znam, vars[1], start=[1,1,1,1], count=[-1,-1,-1,-1])
    ncwrite(zzss, znam, "time", start=[1], count=[-1])
    ncputatt(     znam, "time", Dict("units" => "hours since 1900-01-01 00:00:0.0"))
  end
end

print("\n")
exit(0)
