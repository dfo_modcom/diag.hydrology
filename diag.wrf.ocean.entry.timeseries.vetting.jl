#=
 = Construct lists of stations that conform to requirements for calibration.
 = One list includes all stations with more than a few streamflow observations
 = since the start of the 2018 water year (CUTN); another is the subset of RHBN
 = stations that are larger than a few gridboxes at 50-km/2-km resolution (CUTR).
 = Output files are stored in both source and destination dirs - RD May,Jun 2023.
 =#

using My, Printf, NetCDF

const CUTN             = 100                            # minimum number of obs since 2018 water year
const CUTR             = 2500                           # minimum HydroSHEDS UPLAND_SKM value
const MISS             = -9999.0                        # generic missing value
const DELT             = datesous("1900-01-01-00", "2017-10-01-00", "hr") # start of the 2018 water year

if (argc = length(ARGS)) != 2
  print("\nUsage: jjj $(basename(@__FILE__)) hydat hydat.calibration\n\n")
  exit(1)
end
fila = @sprintf("%s/hydat_vetting.txt",         ARGS[1])
filb = @sprintf("%s/hydat_vetting_rhbn.txt",    ARGS[1])
filc = @sprintf("%s/hydat_vetting_rhbn_%s.txt", ARGS[2], CUTR)
fild = @sprintf("%s/hydat_vetting_rhbn_%s.com", ARGS[2], CUTR)

function vet(file::AbstractString)
  tima = ncread(file, "time", start=[1],     count=[-1])
  floa = ncread(file, "flow", start=[1,1,1], count=[-1,-1,-1])[1,1,:]

  numb = 0
  for (a, tval) in enumerate(tima)
    tval > DELT && floa[a] > 0 && (numb += 1)
  end
  return(numb)
end

forma = formb = formc = formd = ""                                            # save the station list to text
files = readdir(ARGS[1])
for file in files
  global forma, formb, formc, formd
  if isfile(ARGS[1] * "/" * file) && endswith(file, ".sta")
    fpa  = My.ouvre(ARGS[1] * "/" * file, "r")
    lins = readlines(fpa, keep = true) ; close(fpa)
    hlat = parse(Float64, split(lins[ 6])[2])
    hlon = parse(Float64, split(lins[ 7])[2])
    area = parse(Float64, split(lins[30])[2])
    rhbn = parse(  Int64, split(lins[10])[2])
    tmpa = lins[1][26:end]
    tmpb = tmpa[end-8:end-2]
    tmpc = split(split(tmpa, " (")[1], " RIVER")[1]
    labl = " [$tmpb $tmpc]\n"

    stem = file[1:end-4]
    numb = vet(ARGS[1] * "/" * stem * ".nc")
    line = @sprintf("%s %8d %15.8f %15.8f %9.1f %d\n", stem, numb, hlat, hlon, area, rhbn)
                                                               forma *= line
    numb >= CUTN && rhbn == 1 &&                              (formb *= line)
    numb >= CUTN && rhbn == 1 && area > CUTR && hlon > -90 && (formc *= line[1:end-1] * labl ; formd *= @sprintf("cp ../%s/%s* .\n", ARGS[1], stem))
  end
end

fpa = My.ouvre(fila, "w") ; write(fpa, forma) ; close(fpa)                     # then save the results to text
fpb = My.ouvre(filb, "w") ; write(fpb, formb) ; close(fpb)
fpc = My.ouvre(filc, "w") ; write(fpc, formc) ; close(fpc)
fpd = My.ouvre(fild, "w") ; write(fpd, formd) ; close(fpd)
exit(0)
