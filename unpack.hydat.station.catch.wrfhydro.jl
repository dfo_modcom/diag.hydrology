#=
 = Identify the watershed boundary that lies upstream of a HYDAT station.  Include
 = a bounding box for plotting and append the watershed area to the station file.
 = The input grid is flow direction from WRF-Hydro - RD Apr 2023.
 =#

using My, Printf, NetCDF, LibGEOS

if     in("WRFSDOM", keys(ENV)) && ENV["WRFSDOM"] == "_AuxF"
  const FATS             =  225                         # number of  latitudes in Fulldom_hires.nc
  const FONS             =  225                         # number of longitudes in Fulldom_hires.nc
elseif in("WRFSDOM", keys(ENV)) && ENV["WRFSDOM"] == "_Cali"
  const FATS             =  775                         # number of  latitudes in Fulldom_hires.nc
  const FONS             = 1425                         # number of longitudes in Fulldom_hires.nc
elseif in("WRFSDOM", keys(ENV)) && ENV["WRFSDOM"] == "_East"
  const FATS             = 1350                         # number of  latitudes in Fulldom_hires.nc
  const FONS             = 1550                         # number of longitudes in Fulldom_hires.nc
else
  const FATS             = 2150                         # number of  latitudes in Fulldom_hires.nc
  const FONS             = 2750                         # number of longitudes in Fulldom_hires.nc
end
const RESO             = 2.0                            # WRF gridbox resolution (without map scale)
const EARTH            = 6.371e6                        # mean Earth radius (m)
const D2R              = pi / 180.0                     # degrees to radians conversion
const MISS             = -1                             # generic missing value

if (argc = length(ARGS)) != 2
  print("\nUsage: jjj $(basename(@__FILE__)) ../DOMAIN_full_full/ hydat_03JB002.sta\n\n")
  exit(1)
end
fila = ARGS[2]
filb = ARGS[2] * ".hwshed"
filc = ARGS[1] * "Fulldom_hires.nc"
fild = ARGS[1] * "hires_geogrid_mapfac.nc"

function subroute()                                                           # read (flipped) grids of flow dir
  flat = Array{Float32}(undef, FONS, FATS)                                    # and indices of rivers and lakes
  flon = Array{Float32}(undef, FONS, FATS)                                    # (evidently a few gridboxes are
  fdir =   Array{Int16}(undef, FONS, FATS)                                    # ocean/lake or river/lake, and
  fmap = Array{Float64}(undef, FONS, FATS)
  temp = ncread(filc,      "LATITUDE", start=[1,1],   count=[-1,-1])
  for a = 1:FATS, b = 1:FONS  flat[b,a] = temp[b,FATS+1-a]  end
  temp = ncread(filc,     "LONGITUDE", start=[1,1],   count=[-1,-1])
  for a = 1:FATS, b = 1:FONS  flon[b,a] = temp[b,FATS+1-a]  end
  temp = ncread(filc, "FLOWDIRECTION", start=[1,1],   count=[-1,-1])
  for a = 1:FATS, b = 1:FONS  fdir[b,a] = temp[b,FATS+1-a]  end
  fmap = ncread(fild,      "MAPFAC_M", start=[1,1,1], count=[-1,-1,-1])

  fpa  = My.ouvre(fila, "r")                                                  # use the HydroSHEDS river reach location
  lins = readlines(fpa, keep = true) ; close(fpa)                             # (next to the HYDAT station)
  hlat = parse(Float64, split(lins[90])[2])
  hlon = parse(Float64, split(lins[91])[2])
  fups = Set{Tuple{Int32, Int32}}()

  mindis = 9e9 ; latind = lonind = Int32(0)                                   # to find a corresponding grid location
  for a = 1:FATS, b = 1:FONS                                                  # but take the adjacent downstream site
    tdis = abs(flat[b,a] - hlat) + abs(flon[b,a] - hlon) * cos(hlat * D2R)    # to capture a slightly larger upstream
    tdis < mindis && (mindis = tdis ; latind = Int32(a) ; lonind = Int32(b))  # drainage basin
  end
#=fdir[lonind,latind] == 128 && (latind += 1 ; lonind += 1)
  fdir[lonind,latind] ==  64 && (latind += 1              )
  fdir[lonind,latind] ==  32 && (latind += 1 ; lonind -= 1)
  fdir[lonind,latind] ==   1 && (              lonind += 1)
  fdir[lonind,latind] ==  16 && (              lonind -= 1)
  fdir[lonind,latind] ==   2 && (latind -= 1 ; lonind += 1)
  fdir[lonind,latind] ==   4 && (latind -= 1              )
  fdir[lonind,latind] ==   8 && (latind -= 1 ; lonind -= 1)  =#
  @printf("distance between grid %9.5f %9.5f\n", hlat, hlon)
  @printf(" and station location %9.5f %9.5f",                       flat[lonind,latind],          flon[lonind,latind])
  @printf(" is %9.5f km\n",           latlondist(hlat, hlon, Float64(flat[lonind,latind]), Float64(flon[lonind,latind])) / 1000)

  function watershed(b::Int32, a::Int32)
    numb = 0
    if 2 < a < FATS-1 && 2 < b < FONS-1
#    (a == 1 || a == FATS || b == 1 || b == FONS) && return(numb)
      fdir[b-1,a-1] == 128 && !in((b-1,a-1), fups) && (push!(fups, (b-1,a-1)) ; numb += 1)
      fdir[b  ,a-1] ==  64 && !in((b  ,a-1), fups) && (push!(fups, (b  ,a-1)) ; numb += 1)
      fdir[b+1,a-1] ==  32 && !in((b+1,a-1), fups) && (push!(fups, (b+1,a-1)) ; numb += 1)
      fdir[b-1,a  ] ==   1 && !in((b-1,a  ), fups) && (push!(fups, (b-1,a  )) ; numb += 1)
      fdir[b+1,a  ] ==  16 && !in((b+1,a  ), fups) && (push!(fups, (b+1,a  )) ; numb += 1)
      fdir[b-1,a+1] ==   2 && !in((b-1,a+1), fups) && (push!(fups, (b-1,a+1)) ; numb += 1)
      fdir[b  ,a+1] ==   4 && !in((b  ,a+1), fups) && (push!(fups, (b  ,a+1)) ; numb += 1)
      fdir[b+1,a+1] ==   8 && !in((b+1,a+1), fups) && (push!(fups, (b+1,a+1)) ; numb += 1)
    end
    numb
  end

  push!(fups, (lonind,latind))                                                # then identify the remainder of
  numb = totl = 1                                                             # the upstream watershed using fdir
  while (numb > 0)
    numb = 0
    for (b,a) in fups
      numb += watershed(b, a)
    end
    totl += numb
  end

  area = 0.0                                                                  # calculate watershed area
  minlat = minlon =  9999.0                                                   # and a bounding lat/lon box
  maxlat = maxlon = -9999.0
  for (b,a) in fups
    minlat > flat[b,a] && (minlat = flat[b,a])
    maxlat < flat[b,a] && (maxlat = flat[b,a])
    minlon > flon[b,a] && (minlon = flon[b,a])
    maxlon < flon[b,a] && (maxlon = flon[b,a])
    area += (RESO / fmap[b,a])^2
  end
  @printf("an area of %9.2f km2 (%9d gridboxes) define the station and its upstream watershed\n", area, totl)

  fpa   = My.ouvre(fila, "r")                                                 # update the station text file
  lins  = readlines(fpa, keep = true) ; close(fpa)
  form  = "" ; for a =  1:89           form *= lins[a]  end
  tmpa  = parse(Float64, split(lins[90])[2])
  tmpb  = parse(Float64, split(lins[91])[2])
  form *= @sprintf("%-24s %f %d      WRF_km2_with_no_holes %9.1f      WRF_count_gridbox %9d\n", "LATITUDE", tmpa, latind, area, totl)
  form *= @sprintf("%-24s %f %d\n",                                                            "LONGITUDE", tmpb, lonind)
               for a = 92:length(lins) form *= lins[a]  end
  fpa   = My.ouvre(fila, "w")
  write(fpa, form) ; close(fpa)

  bord = Array{Tuple{Int32, Int32, Int8}}(undef, 0)                           # identify the watershed border
  latbeg = lonbeg = -9999                                                     # starting with a northern point
  for (b,a) in fups                                                           # and search counterclockwise
    a > latbeg && (latbeg = a ; lonbeg = b)                                   # along the outer gridbox wall
  end                                                                         # (where w = 0123 = NWSE)
  push!(bord, (lonbeg,latbeg,0))

  function shedborder((b,a,w)::Tuple{Int32,Int32,Int8})
    if     w == 0
      !in((b  ,a+1), fups) && in((b-1,a+1), fups) && !in((b-1,a+1,3), bord) && (push!(bord, (b-1,a+1,3)) ; return(true))
      !in((b-1,a+1), fups) && in((b-1,a  ), fups) && !in((b-1,a  ,0), bord) && (push!(bord, (b-1,a  ,0)) ; return(true))
      !in((b-1,a  ), fups) &&                        !in((b  ,a  ,1), bord) && (push!(bord, (b  ,a  ,1)) ; return(true))
    elseif w == 1
      !in((b-1,a  ), fups) && in((b-1,a-1), fups) && !in((b-1,a-1,0), bord) && (push!(bord, (b-1,a-1,0)) ; return(true))
      !in((b-1,a-1), fups) && in((b  ,a-1), fups) && !in((b  ,a-1,1), bord) && (push!(bord, (b  ,a-1,1)) ; return(true))
      !in((b  ,a-1), fups) &&                        !in((b  ,a  ,2), bord) && (push!(bord, (b  ,a  ,2)) ; return(true))
    elseif w == 2
      !in((b  ,a-1), fups) && in((b+1,a-1), fups) && !in((b+1,a-1,1), bord) && (push!(bord, (b+1,a-1,1)) ; return(true))
      !in((b+1,a-1), fups) && in((b+1,a  ), fups) && !in((b+1,a  ,2), bord) && (push!(bord, (b+1,a  ,2)) ; return(true))
      !in((b+1,a  ), fups) &&                        !in((b  ,a  ,3), bord) && (push!(bord, (b  ,a  ,3)) ; return(true))
    else
      !in((b+1,a  ), fups) && in((b+1,a+1), fups) && !in((b+1,a+1,2), bord) && (push!(bord, (b+1,a+1,2)) ; return(true))
      !in((b+1,a+1), fups) && in((b  ,a+1), fups) && !in((b  ,a+1,3), bord) && (push!(bord, (b  ,a+1,3)) ; return(true))
      !in((b  ,a+1), fups) &&                        !in((b  ,a  ,0), bord) && (push!(bord, (b  ,a  ,0)) ; return(true))
    end
    false
  end
  while  shedborder(bord[end]) != false  end

  numb = 1                                                                    # finally save the border to text
  (latnow,lonnow) = (latbeg,lonbeg)                                           # (with northen point duplicated)
  form = @sprintf("%9.5f %10.5f\n", flat[lonnow,latnow], flon[lonnow,latnow]) # but using only gridbox centers
  for (b,a,w) in bord
    if (latnow,lonnow) != (a,b)
      form *= @sprintf("%9.5f %10.5f\n", flat[b,a], flon[b,a])
      (latnow,lonnow) = (a,b)
      numb += 1
    end
  end
  (latnow,lonnow) != (latbeg,lonbeg) && (form *= @sprintf("%9.5f %10.5f\n", flat[lonbeg,latbeg], flon[lonbeg,latbeg]) ; numb += 1)
  forn  = @sprintf("%9.5f %9.5f %10.5f %10.5f %d\n", minlat, maxlat, minlon, maxlon, numb)
  fpa   = My.ouvre(filb, "w") ; write(fpa, forn) ; write(fpa, form) ; close(fpa)
end

subroute()
exit(0)
