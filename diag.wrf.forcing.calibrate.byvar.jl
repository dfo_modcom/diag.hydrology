#=
 = This program calibrates timeseries against one another, where both might be
 = domain- and multiyear-averages at daily intervals.  Outputs are text and NetCDF
 = files of calibration parameters for the variables of interest.  Calibration is
 = multiplicative for wind speed and precipitation, and additive for other variables.
 = Pour chaque paire de série temporelles, Jacquelin (p. 21-36) donne deux régressions
 = sinusoïdales que nous calibrons - RD May 2021, Dec 20, Feb 2024.
 =#

using My, Printf, NetCDF, Statistics

const MISS             = -9999.0                        # generic missing value
const GRDMIS           = 99999.0                        # generic missing value on file

if (argc = length(ARGS)) < 4
  print("\nUsage: jjj $(basename(@__FILE__)) era wrf _1990_2004.annual.nc T2D [S2D Q2D etc.]\n\n")
  exit(1)
end

fila = "forcing_" * ARGS[1]                 * ARGS[3]
filb = "forcing_" * ARGS[2]                 * ARGS[3]
filc = "forcing_" * ARGS[2]                 * ARGS[3][1:end-3] * "_cali.nc"  ; cp(filb, filc, ; force = true)
fild = "forcing_" * ARGS[1] * "_" * ARGS[2] * ARGS[3][1:end-3] * "_cali.nc"  ; cp(filb, fild, ; force = true)
file = "forcing_" * ARGS[1] * "_" * ARGS[2] * ARGS[3][1:end-3] * "_cali.txt" ; isfile(file) && rm(file)
form = "" ; for a = 4:argc  global form ; form *= @sprintf(" %s", ARGS[a])  end ; form *= "\n"

mcal = [       "S2D",                                     "RAINRATE"]
vars = ["R2D", "S2D", "T2D", "Q2D", "U2D", "V2D", "PSFC", "RAINRATE", "SWDOWN", "LWDOWN"]
tims = ncread(filb, "time", start=[1], count=[-1])
ntim = length(tims) ; tttt = collect(Float64, 1:ntim)                         # define the calibration type (mcal)
cali = ones(1,1,ntim) * MISS                                                  # for each variable and initially
for a = 1:length(vars)                                                        # set all calibrations to missing
  ncwrite(cali, fild, vars[a], start=[1,1,1], count=[-1,-1,-1])
end
print("\ncreated $fild with $ntim times\n  using $fila as a reference for $filb\n")

function funfitjul(a, b, c)
  x -> (y = a + b * sin(2pi / 365.0 * x) + c * cos(2pi / 365.0 * x))
end

for a = 4:argc
  global form, cali
  cccc = ncread(fila, ARGS[a], start=[1,1,1], count=[-1,-1,-1])[1,1,:]
  uuuu = ncread(filb, ARGS[a], start=[1,1,1], count=[-1,-1,-1])[1,1,:]
  size(cccc) != size(uuuu) && error("\nERROR : $fila and $filb have different dimensions\n\n")

  cali =  ones(1,1,ntim) ;             unew = zeros(1,1,ntim)                 # define each calibration based on
  ccss = zeros(1,1,ntim) ; tmpa, tmpb, tmpc = My.integralsin(tttt, cccc)      # a smooth (annual-mean) sinusoidal
  uuss = zeros(1,1,ntim) ; tmpd, tmpe, tmpf = My.integralsin(tttt, uuuu)      # representation and update (unew)

  for b = 1:ntim
    ccss[1,1,b] = funfitjul(tmpa, tmpb, tmpc)(tttt[b])
    uuss[1,1,b] = funfitjul(tmpd, tmpe, tmpf)(tttt[b])
    if in(ARGS[a], mcal)
      cali[1,1,b] = ccss[1,1,b] / uuss[1,1,b]
      unew[1,1,b] = uuuu[    b] * cali[1,1,b]
    else
      cali[1,1,b] = ccss[1,1,b] - uuss[1,1,b]
      unew[1,1,b] = uuuu[    b] + cali[1,1,b]
    end
    form *= @sprintf("%s %9s %16.9f\n", My.dateadd("1900-01-01-00", tims[b], "hr"), ARGS[a], cali[1,1,b])
  end

  tcal = in(ARGS[a], mcal) ? "a multiplicative" : "     an additive"          # write calibration and unew
  @printf("writing %16s calibration for %8s to %s\n", tcal, ARGS[a], fild)
  ncwrite(cali, fild, ARGS[a], start=[1,1,1], count=[-1,-1,-1])
  ncwrite(unew, filc, ARGS[a], start=[1,1,1], count=[-1,-1,-1])
end

fpa = My.ouvre(file, "w") ; write(fpa, form) ; close(fpa)                     # echo calibration to text
print("\n")
exit(0)
