* This script is designed to plot daily/monthly and annual-mean (daily) streamflow
* It can be executed using a command like
*
*     grads -bpc "diag.wrf.ocean.entry.timeseries.grid.multi.mean run_DOMAIN_full_full_FORCING.w85_1990-01-01_2099-12-30_3h_Severn_Hudson"
*
* - RD Feb 2024.

function plot(args)
lista = "run_"subwrd(args,1)".txt"
stema = "run_"subwrd(args,1)
if (substr(stema,30,3) = "ccs") ; modlab = "CCSM-4 RCP-8.5" ; endif
if (substr(stema,30,3) = "h45") ; modlab = "HadGEM RCP-4.5" ; endif
if (substr(stema,30,3) = "h85") ; modlab = "HadGEM RCP-8.5" ; endif
if (substr(stema,30,3) = "w85") ; modlab = "MPI-LR SSP-8.5" ; endif

cc.1  =  9
cc.2  = 14
cc.3  =  4
cc.4  = 11
cc.5  =  5
cc.6  = 13
cc.7  =  3
cc.8  = 10
cc.9  =  7
cc.10 = 12
cc.11 =  8
cc.12 =  2

fila = stema".decadal.txt"
filestat = read(fila) ; a = 1
while (sublin(filestat,1) = 0)
  line     = sublin(filestat,2)
  labl.a   = substr(line,1,3)"0s"
  filestat = read(fila)
  a = a + 1
endwhile
filestat = close(fila)

lef = 1.0 ; rig = 8.0 ; cen = (lef + rig) / 2
ypic = 3 ; string = "0.5 10.5 3.0 "ypic ; inner_decomp(string)
a = 1 ; while (a <= ypic) ; b = ypic - a + 1 ; low.b = _retlef.a ; mid.b = _retmid.a ; hig.b = _retrig.a ; a = a + 1 ; endwhile

"sdfopen "stema".annual.latsum.nc"  ; "q file" ; ret = sublin(result,5)
tima = subwrd(ret,12) ; levs = subwrd(ret, 9) ; lats = subwrd(ret, 6) ; lons = subwrd(ret, 3)
"set datawarn off" ; tims = tima ; "set parea "lef" "rig" "low.1" "hig.1 ; "set xlopts 1 5 0.16" ; "set ylopts 1 5 0.16"
"set t 3 "tims-2 ; "set gxout line" ; "set mproj off" ; "set grid off" ; "set mpdraw off" ; "set xlab on" ; "set ylab on"
"set vrange 20 100" ; "set ylint 20" ; "set cthick 12" ; "set line 1 1 12" ; "set string 1 c 8" ; "set missconn off"
"set ccolor  1" ; "set cstyle 1" ; "set cmark 0" ; "set grads off" ; "set y 1" ; "set x 1" ; "d maskout(riverflo/1000,riverflo-1)"
"close 1"

"sdfopen "stema".decday.latsum.nc"  ; "q file" ; ret = sublin(result,5)
timb = subwrd(ret,12) ; levs = subwrd(ret, 9) ; lats = subwrd(ret, 6) ; lons = subwrd(ret, 3)
"set datawarn off" ; tims = timb ; "set parea "lef" "rig" "low.3" "hig.2 ; "set xlopts 1 5 0.16" ; "set ylopts 1 5 0.16"
"set t 1 "tims-2 ; "set gxout line" ; "set mproj off" ; "set grid off" ; "set mpdraw off" ; "set xlab on" ; "set ylab on"
"set vrange 0 180" ; "set ylint 20" ; "set cthick 12" ; "set line 1 1 12" ; "set string 1 c 8" ; "set missconn off"
xa = 5.65 ; ya = hig.2 - 3.1
a = 1 ; while (a <= levs)
  "set ccolor "cc.a ; "set cstyle 1" ; "set cmark 0" ; "set grads off" ; "set z "a ; "set y 1" ; "set x 1" ; "d maskout(riverflo/1000,riverflo-1)"
  "set strsiz 0.12" ; "set string "cc.a" r 12" ; "draw string "xa" "ya+0.25*a" "labl.a
a = a + 1 ; endwhile

                             "set strsiz 0.19" ; "set string 1 c 6  0" ; "draw string "cen-0.0" 10.7 Severn to Hudson Discharge (10`a3`nm`a3`n/s)"
#xa = 7.65 ; ya = mid.1     ; "set strsiz 0.16" ; "set string 1 c 6 90" ; "draw string "xa" "ya" Annual"
#ya = (mid.2 + mid.3)/2     ; "set strsiz 0.16" ; "set string 1 c 6 90" ; "draw string "xa" "ya" Decadal-Mean Daily"
xa = 7.75 ; ya = low.1+0.5 ; "set strsiz 0.16" ; "set string 1 r 6  0" ; "draw string "xa" "ya" a) Annual"
            ya = low.3+0.5 ; "set strsiz 0.16" ; "set string 1 r 6  0" ; "draw string "xa" "ya" b) Decadal-Mean Daily"
xa = 2.4  ; ya = hig.1-0.3 ; "set strsiz 0.16" ; "set string 1 c 6  0" ; "draw string "xa" "ya" "modlab

plot = stema".allflow.png"
say "printim "plot" png white x1700 y2200"
    "printim "plot" png white x1700 y2200"
"quit"


function inner_decomp(args)
  lef = subwrd(args,1)
  rig = subwrd(args,2)
  wid = subwrd(args,3)
  num = subwrd(args,4)
  _retmid.1   = lef + wid / 2
  _retmid.num = rig - wid / 2
  a = 2
  while (a < num)
    _retmid.a = (_retmid.num * (a-1) + _retmid.1 * (num-a)) / (num - 1)
    a = a + 1
  endwhile

  a = 1
  while (a <= num)
    _retlef.a = _retmid.a - wid / 2
    _retrig.a = _retmid.a + wid / 2
    a = a + 1
  endwhile
return
