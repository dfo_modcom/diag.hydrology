#=
 = Extract a timeseries of some variable (e.g., summed over all river outlet
 = locations) from WRF Hydro output files and save this as a separate netcdf
 = file for plotting - RD Apr 2020, May 2021. 
 =#

using My, Printf, NetCDF

const DELT             = 24.0                           # timestep (hours)
const MISS             = -9999.0                        # generic missing value

if (argc = length(ARGS)) != 5
  print("\nUsage: jjj $(basename(@__FILE__)) variable   first_date filename_ending  num_dates positions.txt\n")
  print(  "e.g.,  jjj $(basename(@__FILE__)) streamflow 1903010100 00.CHRTOUT_GRID1 365 ../Fulldom_hires_outlets.txt\n\n")
  exit(1)
end
vnam =            ARGS[1]
fbeg =            ARGS[2]
fend =            ARGS[3]
ndat = parse(Int, ARGS[4])
flet =            ARGS[5]
vals = Array{Float64}(undef,0)
tims = Array{Float64}(undef,0)
xloc = Array{  Int64}(undef,0)
yloc = Array{  Int64}(undef,0)

fpa = My.ouvre(flet, "r") ; lins = readlines(fpa) ; close(fpa)                # read river outlet locations (immediately
nlin = length(lins)                                                           # upriver of the corresponding ocean entries)
for a = 1:nlin
  tmpa = split(lins[a])
  push!(xloc, parse(Int64, tmpa[2]))
  push!(yloc, parse(Int64, tmpa[3]))
end

dnow = fbeg                                                                   # then construct the timeseries and store it
for a = 1:ndat
  global dnow
  fnam = "dat/" * dnow * fend
  if isfile(fnam)
    grid = ncread(fnam, vnam, start=[1,1,1], count=[-1,-1,-1])
    valu = 0
    for b = 1:nlin
      valu += grid[xloc[b],yloc[b],1]
    end
  else
    print("missing grid ($MISS value) for $fnam\n")
    valu = MISS
  end
  time = My.datesous("1900010100", dnow, "hr")
  push!(vals, valu) ; push!(tims, time)
  dnow = My.dateadd(dnow, DELT, "hr")
end

fnam = ARGS[1] * "." * ARGS[2] * ARGS[3] * "." * ARGS[4] * ".nc"
nccreer(      fnam, length(vals), 1, 1, MISS)
ncwrite(vals, fnam,  "tmp", start=[1,1,1], count=[-1,-1,-1])
ncwrite(tims, fnam, "time", start=[1],     count=[-1])
ncputatt(     fnam, "time", Dict("units" => "hours since 1900-01-01 00:00:0.0"))
exit(0)
