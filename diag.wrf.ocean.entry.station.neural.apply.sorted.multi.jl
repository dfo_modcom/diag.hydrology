#=
 = Apply a neural network to features of a normalized WRF-Hydro streamflow
 = timeseries, treating each station like a separate catchment with a separate
 = neural network.  Features that are missing are simply omitted.  Training by
 = eve/odd/all years is applied to odd/eve/all years, respectively.  HYDAT and
 = WRF-Hydro streamflow are not assumed to be collocated - RD Mar 2024.
 =#

using My, Printf, NetCDF, Flux, Statistics, JLD2

const WRFN             = 2                              # streamflow forced by CCSM/HadGEM/MPI with neural network post-processing
const ERAN             = 4                              # streamflow forced by      ERA-5      with neural network post-processing
const FEAT             = 3                              # number of features (neural network inputs)
const DELT             = 3                              # source data timestep  (hours)
const DELD             = 24                             # daily       timestep  (hours)
const SMAL             = 0.001                          # generic small streamflow value
const MISS             = -9999.0                        # generic missing value

if (argc = length(ARGS)) != 4
  print("\nUsage: jjj $(basename(@__FILE__)) hydat_valding_anat_1000_incl_1990-01-01_2022-12-30.neural.nc flow eve hydat_valding_anat_1000_incl_1990-01-01_2022-12-30.neural.soreve.identity.all.jld\n\n")
  exit(1)
end
acti = split(ARGS[4], ".")[end-2]
fila = ARGS[1]
filb = ARGS[1][1:end-3] * ".txt"
filc = ARGS[1][1:end-3] * ".features.nc"
fild = ARGS[1][1:end-3] * ".sor" * ARGS[3] * ".$acti.nc"
file = ARGS[1][1:end-3] * ".sor" * ARGS[3] * ".$acti.txt"
filf = ARGS[4]
fnam = ARGS[2] ;              fnid = ERAN
contains(ARGS[1], "iccs") && (fnid = WRFN)
contains(ARGS[1], "ih45") && (fnid = WRFN)
contains(ARGS[1], "ih85") && (fnid = WRFN)
contains(ARGS[1], "impi") && (fnid = WRFN)
fnam           != "flow"  && (fnid =    1)

@printf("\nwriting %d where [1,2]=streamflow forced by CCSM/HadGEM/MPI with neural network post-processing\n", fnid)
@printf(   "            and    4 =streamflow forced by      ERA-5      with neural network post-processing\n")
@printf("copying %s  %s\n", fila, fild) ; cp(fila, fild; force = true, follow_symlinks = true)
@printf("copying %s %s\n",  filb, file) ; cp(filb, file; force = true, follow_symlinks = true)

tims = ncread(  fila, "time", start=[1], count=[-1]) ; ntim = length(tims)    # define a mask for eve/odd/all years
lats = ncread(  fila,  "lat", start=[1], count=[-1]) ; nsta = length(lats)    #   (i.e., true for odd/eve/all years)
tatt = ncgetatt(fila, "time", "units")               ; msky = falses(ntim)
for a = 1:ntim
  year = parse(Int64, dateref(tims[a], tatt)[1:4])
  ARGS[3] == "eve" &&  isodd(year) && (msky[a] = true)
  ARGS[3] == "odd" && iseven(year) && (msky[a] = true)
  ARGS[3] == "all" &&                 (msky[a] = true)
end
@printf("\n%s has %7d times, with %7d non-%s years, and %d stations\n", fila, ntim, length(msky[msky]), ARGS[3], nsta)

pars = JLD2.load(filf, "model_state");                                        # then load the neural network
acti == "identity" && (model = Chain(Dense(FEAT => FEAT+1),                               BatchNorm(FEAT+1), Dense(FEAT+1 => FEAT)))
acti != "identity" && (model = Chain(Dense(FEAT => FEAT+1, getfield(Main, Symbol(acti))), BatchNorm(FEAT+1), Dense(FEAT+1 => FEAT, getfield(Main, Symbol(acti)))))
Flux.loadmodel!(model, pars)

for a = 1:nsta
  global model, pars
  alph = ncread(filc, "alph", start=  [a,1], count=   [1,-1])                 # read the normalized flow features
  beta = ncread(filc, "beta", start=  [a,1], count=   [1,-1])
  feat = ncread(filc, "feat", start=[1,a,1], count=[-1,1,-1])
  @printf(" %d", a)

  mskz = falses(ntim)                                                         # and define their validity mask
  for b = 1:ntim
    msky[b] && all(feat[:,1,b] .> MISS) && (mskz[b] = true)
  end

  flow = ones(1,1,ntim) * MISS                                                # and replace all WRF-Hydro estimates
  for b = 1:ntim                                                              # with rescaled neural network estimates
    if mskz[b]
      xxaa        = Float32(feat[1,1,b])
      xxbb        = Float32(feat[2,1,b])
      xxcc        = Float32(feat[3,1,b])
      flow[1,1,b] = Float64(model(reshape([xxaa,xxbb,xxcc],FEAT,1))[1]) * beta[1,b] + alph[1,b]
      flow[1,1,b] < 0 && (flow[1,1,b] = SMAL)
    end
  end
  ncwrite(flow, fild, "flow", start=[fnid,a,1], count=[1,1,-1])
end

print("\n\n")
exit(0)
