#=
 = Burn the 15-s HydroSHEDS watershed boundaries into the 30-s MERIT DEM by
 = ensuring that the MERIT values are slightly higher along the HydroSHEDS
 = boundaries, but avoid raising the MERIT DEM at the lowest elevations of
 = each watershed, where this drains to the ocean - RD April 2023.
 =#

using My, Printf, NetCDF, Shapefile

const MATS             =  6600                          # number of latitudes   in MERIT grid
const MONS             = 21600                          # number of longitudes  in MERIT grid
const DELL             =    0.0083333333333333333       # lat/lon grid spacing
const LATA             =   30.0041666666666666667       # first latitude on grid
const LONA             = -179.9958333333333333333       # first longitude on grid

const D2R              = 3.141592654 / 180.0            # degrees to radians conversion
const DELMISS          = -8e9                           # generic missing value comparison
const GRDMISS          = -9e9                           # generic missing value on a grid
const MISS             = -9999.0                        # generic missing value
const MIST             = @sprintf("%d", Int64(MISS))    # generic missing value string

if (argc = length(ARGS)) != 2
  print("\nUsage: jjj $(basename(@__FILE__)) merit_hydrodem_30_85N_180_00W_30sec_elevtn.nc ../HYDROSHEDS/HydroRIVERS_v10_30_85N_180_00W\n\n")
  exit(1)
end
fila = ARGS[1]
filb = ARGS[2]          * "_wsheds.shp" ; hytab = Shapefile.Table(filb) ; hylen = length(hytab) ; hygeo = Shapefile.shapes(hytab)
filc = ARGS[2]          * "_rivers.shp" ; rvtab = Shapefile.Table(filc) ; rvlen = length(rvtab) ; rvgeo = Shapefile.shapes(rvtab)
fild = ARGS[1][1:end-3] * "_adjust.nc"  ; cp(fila, fild; force=true, follow_symlinks=true)
file = ARGS[1][1:end-3] * "_adjust_pour.txt"
filf = ARGS[1][1:end-3] * "_adjust_bndy.txt"
filg = ARGS[1][1:end-3] * "_adjust_rivr.txt"

mlat = ncread(fila,   "lat", start=[1],   count=[-1])                         # read the 15-s HydroSHEDS watershed
mlon = ncread(fila,   "lon", start=[1],   count=[-1])                         # boundaries and the MERIT elevation
mlev = ncread(fila, "Band1", start=[1,1], count=[-1,-1])

function hydroburn()                                                          # burn the watershed boundaries into
  pour = Set{Tuple{Int32, Int32}}()                                           # the MERIT DEM; first identify all
  bndy = Set{Tuple{Int32, Int32}}()                                           # ocean pour points, drainage boundaries,
  rivr = Set{Tuple{Int32, Int32}}()                                           # and interior rivers on the MERIT grid

  if isfile(file)                                                             # write MERIT coastal points
    @printf("reading %s\n", file)                                             # (then can read quickly)
    for line in eachline(file)
      lonind, latind = parse.(Int32, split(line))
      push!(pour, (lonind, latind))
    end
  else
    @printf("writing %s\n", file)
    for a = 2:MATS-1, b = 2:MONS-1
      if mlev[b,a] >= 0 && (mlev[b-1,a] < 0 || mlev[b+1,a] < 0 || mlev[b,a-1] < 0 || mlev[b,a+1] < 0)
        push!(pour, (b, a))
      end
    end
    fpa = My.ouvre(file, "w")
    for (lonind, latind) in pour
      form = @sprintf("%6d %5d\n", lonind, latind) ; write(fpa, form)
    end
    close(fpa)
  end

  if isfile(filf)                                                             # write watershed boundaries
    @printf("reading %s\n", filf)                                             # (then can read quickly)
    for line in eachline(filf)
      lonind, latind = parse.(Int32, split(line))
      push!(bndy, (lonind, latind))
    end
  else
    @printf("writing %s\n", filf)
    for b = 1:hylen
      hypts = hygeo[b].points
      for c = 1:length(hypts)
        latmin, latind = findmin(abs.(hypts[c].y .- mlat))
        lonmin, lonind = findmin(abs.(hypts[c].x .- mlon))
        push!(bndy, (lonind, latind))
      end
    end
    fpa = My.ouvre(filf, "w")
    for (lonind, latind) in bndy
      form = @sprintf("%6d %5d\n", lonind, latind) ; write(fpa, form)
    end
    close(fpa)
  end

  if isfile(filg)                                                             # write HydroSHEDS large rivers
    @printf("reading %s\n", filg)                                             # (then can read quickly)
    for line in eachline(filg)
      lonind, latind = parse.(Int32, split(line))
      push!(rivr, (lonind, latind))
    end
  else
    @printf("writing %s\n", filg)
    for b = 1:rvlen
      rvpts = rvgeo[b].points
      for c = 1:length(rvpts)
        latmin, latind = findmin(abs.(rvpts[c].y .- mlat))
        lonmin, lonind = findmin(abs.(rvpts[c].x .- mlon))
        push!(rivr, (lonind, latind))
      end
    end
    fpa = My.ouvre(filg, "w")
    for (lonind, latind) in rivr
      form = @sprintf("%6d %5d\n", lonind, latind) ; write(fpa, form)
    end
    close(fpa)
  end
  @printf("%9d gridboxes define the coastline of %s\n",                    length(pour), fild)
  @printf("%9d gridboxes define the %d HydroSHEDS watershed boundaries\n", length(bndy), hylen)
  @printf("%9d gridboxes define the %d HydroSHEDS large river networks\n", length(rivr), rvlen)

  mask = zeros(Int16, MONS, MATS)
  for (b, a) in bndy                                                          # then increase elevation by 1%
    mask[b,a] = 1                                                             # at the watershed boundaries and
  end                                                                         # 0.5% at two adjacent gridboxes
  for a = 3:MATS-2, b = 3:MONS-2
    if mask[b,a] > 0
      for c = -2:2, d = -2:2
        mask[b+d,a+c] == 0 && (mask[b+d,a+c] = -1)
      end
    end
  end

  for a = 2:MATS-1, b = 2:MONS-1                                              # promote from 0.5% to 1% increase
    if mask[b,a] < 0                                                          # where any increase is surrounded
      numb = 0                                                                # (to cross watershed boundary gaps)
      for c = -1:1, d = -1:1
        mask[b+d,a+c] != 0 && (numb += 1)
      end
      numb == 9 && (mask[b,a] = 1)
    end
  end

  for (b, a) in pour                                                          # but keep elevation unadjusted
    mask[b,a] = -5                                                            # near any calibration stations
  end
  for a = 6:MATS-5, b = 6:MONS-5
    if mask[b,a] == -5
      for c = -5:5, d = -5:5
        mask[b+d,a+c] > 0 && (mask[b+d,a+c] = 0)
      end
    end
  end

  for (b, a) in rivr                                                          # or along interior rivers
    if a != 1 && a != MATS && b != 1 && b != MONS
      for c = -1:1, d = -1:1
        mask[b+d,a+c] = 0
      end
    end
  end
# for a = 2:MATS-1, b = 2:MONS-1
#   if mask[b,a] == 10
#     for c = -1:1, d = -1:1
#       mask[b+d,a+c] = 0
#     end
#   end
# end

  for a = 1:MATS, b = 1:MONS
#   if mlev[b,a] > 50
      mask[b,a] > 0 && (mlev[b,a] += 100)
#     mask[b,a] < 0 && (mlev[b,a] += 25)
#     mask[b,a] > 0 && (mlev[b,a] += round(Int16, mlev[b,a] / 10))
#     mask[b,a] < 0 && (mlev[b,a] += round(Int16, mlev[b,a] / 20))
#   end
  end

  a = 3249 ; for b = 12541:12546  mlev[b,a] += 100  end                       # block at Aux Feuilles
  a = 3250 ; for b = 12542:12547  mlev[b,a] += 100  end
  b, a = 14664, 2797                                                          # block at Eagle River
  for c = -5:5, d = -5:5
           abs(c) < 2 &&        abs(d) < 2   && (mlev[b+d,a+c] = 400)
    mask[b+d,a+c] > 0 && mlev[b+d,a+c] < 400 && (mlev[b+d,a+c] = 400)
  end
  ncwrite(mlev, fild, "Band1", start=[1,1], count=[-1,-1])
end

hydroburn()
exit(0)

#=
  function telescope(rlat::Float64, rlon::Float64)                            # telescope through the WRF-Hydro grid
    mindis = minlat = minlon = 9e9                                            # to get a location closest to a river
    minlin = div(FATS, 2)
    minpix = div(FONS, 2)
    for a = 25:-1:0
      for b = minlin - 2^a : 2^a : minlin + 2^a
        if b >= 0 && b < FATS
          for c = minpix - 2^a : 2^a : minpix + 2^a
            if c >= 0 && c < FONS
              gdlatitude  = lats[c+1,b+1]
              gdlongitude = lons[c+1,b+1]
              tmpdis = (rlat - gdlatitude)^2 + (rlon - gdlongitude)^2
              if tmpdis < mindis
                mindis = tmpdis
                minlat = gdlatitude
                minlon = gdlongitude
                minlin = b
                minpix = c
#               @printf("a b c are (%8d %8d %8d) and lat lon mindis are %lf %lf %e ***\n", a, b, c, gdlatitude, gdlongitude, mindis)
#             else
#               @printf("a b c are (%8d %8d %8d) and lat lon tmpdis are %lf %lf %e\n", a, b, c, gdlatitude, gdlongitude, tmpdis)
              end
            end
          end
        end
      end
    end
    if 111 * mindis^0.5 > 10 * FDIS
      print("\nrlat rlon are $rlat $rlon\n")
      print("minlat minlon are $minlat $minlon at line pixel $minlin $minpix\n")
      print("allowable distance between them is about $(10 * FDIS) km\n")
      print("actual    distance between them is about $(111 * mindis^0.5) km\n")
      exit(0)
    end
    (minpix, minlin)
  end

  elev = ncread(filc, "Band1", start=[1,1], count=[-1,-1])                    # read WRF-Hydro grids
  lats = ncread(filc,   "lat", start=[1],   count=[-1])                       # (with latitude flipped)
  lons = ncread(filc,   "lon", start=[1],   count=[-1])
  (nlon, nlat) = size(elev)


  friv = Set{Tuple{Int32, Int32}}()                                           # identify all WRF-Hydro gridboxes that are
  for b = 1:rvlen                                                             # collocated with HydroSHEDS river points
    rvpts = rvgeo[b].points
    for c = 1:length(rvpts)
      push!(friv, telescope(rvpts[c].y, rvpts[c].x))
    end
  end
  @printf("\n%9d gridboxes define the HydroSHEDS river network on the WRF-Hydro grid\n", length(friv))

  fshd = Set{Tuple{Int32, Int32}}()                                           # identify all WRF-Hydro gridboxes that are
  for b = 2:length(lina)                                                      # collocated with HydroSHEDS river points
    hlat = parse(Float64, split(lina[b])[1])
    hlon = parse(Float64, split(lina[b])[2])
    push!(fshd, telescope(hlat, hlon))
  end
  @printf("\n%9d gridboxes define the HydroSHEDS river network on the WRF-Hydro grid\n", length(fshd))


==> ../hydat/hydat_03JB002.sta.hyshed <==
 56.81458  59.21458  -75.84375  -70.17708 4672
 59.21458  -71.58125
 59.21458  -71.58542
 59.21042  -71.58958
 59.20625  -71.59375
 59.20208  -71.59792
 59.19792  -71.59792
 59.19375  -71.59792
 59.18958  -71.60208
 59.18958  -71.60625

nafil = ARGS[1] * "HydroRIVERS_v10_na.shp" ; natab = Shapefile.Table(nafil) ; nalen = length(natab) ; nageo = Shapefile.shapes(natab)
arfil = ARGS[1] * "HydroRIVERS_v10_ar.shp" ; artab = Shapefile.Table(arfil) ; arlen = length(artab) ; argeo = Shapefile.shapes(artab)
grfil = ARGS[1] * "HydroRIVERS_v10_gr.shp" ; grtab = Shapefile.Table(grfil) ; grlen = length(grtab) ; grgeo = Shapefile.shapes(grtab)
fmis  = "hydat_zzzzzzz.mis"

function findreach()                                                          # locate a HydroSHEDS river reach
  fpa   = My.ouvre(fila, "r")                                                 # endpoint that is near a station and
  lins  = readlines(fpa, keep = true) ; close(fpa)                            # is similar in upstream area (but if
  hlat  = parse(Float64, split(lins[6])[2])                                   # there is no upstream area value,
  hlon  = parse(Float64, split(lins[7])[2])                                   # then ignore this limit)
  if split(lins[8])[2] != "missing"
    hare = parse(Float64, split(lins[8])[2]) ; hdel = hare * FRAC
  else
    hare = 0.0 ; hdel = 9e9
  end

  clos = Array{Tuple{Float64, Int8, Int64, Int16, Float64, Float64}}(undef, 0)
  for a = 1:3
    a == 1 && (ddtab = natab ; ddlen = nalen ; ddgeo = nageo)               # first compile the HydroSHEDS endpoints
    a == 2 && (ddtab = artab ; ddlen = arlen ; ddgeo = argeo)               # inside the station search distance
    a == 3 && (ddtab = grtab ; ddlen = grlen ; ddgeo = grgeo)
    for b = 1:ddlen
      ddpts = ddgeo[b].points
      for c = 1:length(ddpts)
        (ddlat, ddlon) = (ddpts[c].y, ddpts[c].x)
        tmpdis = abs(ddlat - hlat) + abs(ddlon - hlon) * cos(hlat * D2R)
        tmpdis < RDIS && push!(clos, (tmpdis, a, b, c, ddtab.MAIN_RIV[b], ddtab.UPLAND_SKM[b]))
      end
    end
  end

  if length(clos) == 0
    print("no match for $fila\n")
    form = "mv $(fila[1:end-4]) hydat_noriver\n"
    fpa = My.ouvre(fmis, "a") ; write(fpa, form) ; close(fpa)
  else                                                                      # and if FRAC is not too constraining,
    print("   match for $fila is $(length(clos))\n")                        # select from among HydroSHEDS endpoints
    mindisa = 9e9 ; mininda = 0                                             # within FRAC of upstream catchment area
    mindisb = 9e9 ; minindb = 0                                             # (so mininda != 0 but minindb can be 0)
    for a = 1:length(clos)
      clos[a][1] < mindisa                                           && (mindisa = clos[a][1] ; mininda = a)
      clos[a][1] < mindisb && hare - hdel < clos[a][6] < hare + hdel && (mindisb = clos[a][1] ; minindb = a)
    end
    minind = minindb == 0 ? mininda : minindb
    mina = clos[minind][2] ; minb = clos[minind][3] ; minc = clos[minind][4]
    mina == 1 && (ddtab = natab ; ddlen = nalen ; ddgeo = nageo)
    mina == 2 && (ddtab = artab ; ddlen = arlen ; ddgeo = argeo)
    mina == 3 && (ddtab = grtab ; ddlen = grlen ; ddgeo = grgeo)            # then save the match and station data

    form  = "" ; for a = 1:20  form *= lins[a]  end
    form *= @sprintf("%-24s %17.14f %d\n",            "LATITUDE", ddgeo[minb].points[minc].y, Int64(MISS))
    form *= @sprintf("%-24s %17.14f %d\n",           "LONGITUDE", ddgeo[minb].points[minc].x, Int64(MISS))
    form *= @sprintf("%-24s %d\n",                    "HYRIV_ID", ddtab.HYRIV_ID[minb])
    form *= @sprintf("%-24s %d\n",                   "NEXT_DOWN", ddtab.NEXT_DOWN[minb])
    form *= @sprintf("%-24s %d\n",                    "MAIN_RIV", ddtab.MAIN_RIV[minb])
    form *= @sprintf("%-24s %f\n",                   "LENGTH_KM", ddtab.LENGTH_KM[minb])
    form *= @sprintf("%-24s %f\n",                  "DIST_DN_KM", ddtab.DIST_DN_KM[minb])
    form *= @sprintf("%-24s %f\n",                  "DIST_UP_KM", ddtab.DIST_UP_KM[minb])
    form *= @sprintf("%-24s %f\n",                   "CATCH_SKM", ddtab.CATCH_SKM[minb])
    form *= @sprintf("%-24s %f\n",                  "UPLAND_SKM", ddtab.UPLAND_SKM[minb])
    form *= @sprintf("%-24s %d\n",                   "ENDORHEIC", ddtab.ENDORHEIC[minb])
    form *= @sprintf("%-24s %f\n",                  "DIS_AV_CMS", ddtab.DIS_AV_CMS[minb])
    form *= @sprintf("%-24s %d\n",                    "ORD_STRA", ddtab.ORD_STRA[minb])
    form *= @sprintf("%-24s %d\n",                    "ORD_CLAS", ddtab.ORD_CLAS[minb])
    form *= @sprintf("%-24s %d\n",                    "ORD_FLOW", ddtab.ORD_FLOW[minb])
    form *= @sprintf("%-24s %d\n",                   "HYBAS_L12", ddtab.HYBAS_L12[minb])
    form *= @sprintf("%-24s %s as of %s\n", "HydroSHEDS_VERSION", "1.0", "2019-10-01")
    for a = 38:length(lins)  form *= lins[a]  end
    fpa = My.ouvre(fila, "w") ; write(fpa, form) ; close(fpa)
  end
end
=#
