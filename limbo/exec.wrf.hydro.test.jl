#=
 = Launch WRFHydro using namelists that are unmodified or modifed,
 = with the latter being for spinup or restart - RD March 2021.
 =#

using My, Printf

const NOMODS           = 0                              # namelist unmodified
const SPINUP           = 1                              # namelist for spinup
const RSTART           = 2                              # namelist for restart

argc = length(ARGS)
function progexec()
  print("\nUsage: jjj $(basename(@__FILE__)) cca/cmc nomod\n")
  print(  "   or: jjj $(basename(@__FILE__)) cca/cmc spinup\n")
  print(  "   or: jjj $(basename(@__FILE__)) cca/cmc restart\n\n")
  exit(1)
end
ccabat = "wrf_hydro.sbatch"
cmcbat = "wrf_hydro.jgen"

argc < 2 && progexec() ; nlis = -1
argc > 1 && ARGS[2] == "nomod"   && (nlis = NOMODS)
argc > 1 && ARGS[2] == "spinup"  && (nlis = SPINUP)
argc > 1 && ARGS[2] == "restart" && (nlis = RSTART)
nlis < 0 && progexec()
print("\n")

stem = split(abspath("."), "work/work")[1] * "work/workg/WRF/wrf_hydro_nwm_public/trunk/NDHMS/Run/"
fils = ["CHANPARM.TBL", "GENPARM.TBL", "HYDRO.TBL", "MPTABLE.TBL", "SOILPARM.TBL", "wrf_hydro.exe"]
for file in fils
  full = stem * file
  print("cp $full .\n")
  cp(full, file; force = true, follow_symlinks = true)
end

function namelists()
  form  = "&HYDRO_nlist\n"                                                    # write the hydro.namelist file
  form *= "sys_cpl                = 1\n"                                      # COUPLING: 1=HRLDAS (offline Noah-LSM), 2=WRF, 3=NASA/LIS, 4=CLM
  form *= "GEO_STATIC_FLNM        = \"./DOMAIN/geogrid.nc\"\n"                # INPUT DATA: land surface model gridded input data file (e.g.: "geo_em.d01.nc")
  form *= "GEO_FINEGRID_FLNM      = \"./DOMAIN/Fulldom_hires.nc\"\n"          # high-resolution routing terrain input data file (e.g.: "Fulldom_hires.nc")
  form *= "HYDROTBL_F             = \"./DOMAIN/hydro2dtbl.nc\"\n"             # spatial hydro parameters file (e.g.: "hydro2dtbl.nc") created if nonexist
  form *= "LAND_SPATIAL_META_FLNM = \"./DOMAIN/GEOGRID_LDASOUT_Spatial_Metadata.nc\"\n"  # spatial metadata file for land surface grid. (e.g.: "GEOGRID_LDASOUT_Spatial_Metadata.nc")
  form *= "RESTART_FILE           = \'RESTART/HYDRO_RST.2011-08-26_00:00_DOMAIN1\'\n"    # restart file if starting from restart...comment out with '!' if not...
  form *= "IGRID      =    1\n"                                               # SETUP OPTIONS: domain or nest number identifier (integer)
  form *= "rst_dt     = 1440\n"                                               # restart file write frequency (minutes) -99999 will output restarts on the first day of the month only.
  form *= "rst_typ    =    1\n"                                               # reset LSM soil states using high-res routing restart file (1=overwrite, 0=no overwrite) ONLY if routing
  form *= "rst_bi_in  =    0\n"                                               # 0: use netcdf input restart file format 1: use parallel io for reading multiple restart files, 1 per core
  form *= "rst_bi_out =    0\n"                                               # 0: use netcdf output restart file format 1: use parallel io for outputting multiple restart files, 1 per core
  form *= "RSTRT_SWC  =    1\n"                                               # set restart accumulation variables to 0 (0=no reset, 1=yes reset to 0.0)
  form *= "GW_RESTART =    1\n"                                               # baseflow/bucket model initialization...(0=cold start from table, 1=restart file)
  form *= "out_dt                      = 60\n"                                # OUTPUT CONTROL: file write frequency...(minutes)
  form *= "SPLIT_OUTPUT_COUNT          =  1\n"                                # number of times in each output file (1 for CHANNEL ROUTING ONLY/CALIBRATION SIMS/COUPLED TO WRF)
  form *= "order_to_write              =  1\n"                                # minimum stream order to output to netcdf point file...(integer) (lower value produces more output)
  form *= "io_form_outputs             =  4\n"                                # I/O routines: 0=deprecated, 1=scl/off/compress, 2=scl/off/NOcompress, 3=compress only, 4=no scl/off/compress
  form *= "io_config_outputs           =  5\n"                                # Realtime run config 0=all (default), 1=analys, 2/3/4=short/med/long-rng, 5=retrospec, 6=diag (1-4 combined)
  form *= "t0OutputFlag                =  1\n"                                # Option to write output files at time 0 (restart cold start time): 0=no, 1=yes (default)
  form *= "output_channelBucket_influx =  2\n"                                # channel+bucket influx (if UDMP_OPT=1 out_dt=NOAH_TIMESTEP): 2=qSfcLatRunoff&qBucket+qBtmVertRunoff_toBucket
  form *= "CHRTOUT_DOMAIN              =  1\n"                                # Netcdf point timeseries output at all channel points (1d):                0 = no output, 1 = output
  form *= "CHANOBS_DOMAIN              =  1\n"                                # Netcdf point timeseries at forecast/gage points (defined in Routelink):   0 = no output, 1 = output
  form *= "CHRTOUT_GRID                =  0\n"                                # Netcdf grid of channel streamflow values (2d), but not for reach routing: 0 = no output, 1 = output
  form *= "LSMOUT_DOMAIN               =  1\n"                                # Netcdf grid of variables passed between LSM and routing components (2d):  0 = no output, 1 = output
  form *= "RTOUT_DOMAIN                =  1\n"                                # Netcdf grid of terrain routing variables on routing grid (2d):            0 = no output, 1 = output
  form *= "output_gw                   =  1\n"                                # Netcdf GW output:                                                         0 = no output, 1 = output
  form *= "outlake                     =  1\n"                                # Netcdf grid of lake values (1d):                                          0 = no output, 1 = output
  form *= "frxst_pts_out               =  0\n"                                # ASCII text file of forecast points or gage points (defined in Routelink): 0 = no output, 1 = output
  form *= "NSOIL            =   4\n"                                          # PHYSICS OPTIONS: number of soil layers (integer)
  form *= "ZSOIL8(1)        =  -0.10\n"                                       # depth of the bottom of  first layer... (meters)
  form *= "ZSOIL8(2)        =  -0.40\n"                                       # depth of the bottom of second layer... (meters)
  form *= "ZSOIL8(3)        =  -1.00\n"                                       # depth of the bottom of  third layer... (meters)
  form *= "ZSOIL8(4)        =  -2.00\n"                                       # depth of the bottom of fourth layer... (meters)
  form *= "DXRT             = 250.0\n"                                        # grid spacing of the terrain routing grid...(meters)
  form *= "AGGFACTRT        =   4\n"                                          # multiple between the land model grid and the terrain routing grid...(integer)
  form *= "DTRT_CH          = 300\n"                                          # channel routing model timestep...(seconds)
  form *= "DTRT_TER         =  10\n"                                          # terrain routing model timestep...(seconds)
  form *= "SUBRTSWCRT       =   1\n"                                          # activate subsurface routing...(0=no, 1=yes)
  form *= "OVRTSWCRT        =   1\n"                                          # activate surface overland flow routing...(0=no, 1=yes)
  form *= "rt_option        =   1\n"                                          # overland flow routing option: 1=Seepest Descent (D8) 2=CASC2D (not active)
  form *= "CHANRTSWCRT      =   1\n"                                          # activate channel routing...(0=no, 1=yes)
  form *= "channel_option   =   2\n"                                          # channel routing option: 1=Muskingam-reach, 2=Musk.-Cunge-reach, 3=Diff.Wave-gridded
  form *= "route_link_f     = \"./DOMAIN/Route_Link.nc\"\n"                   # reach file for reach-based routing options (e.g.: "Route_Link.nc")
  form *= "compound_channel = .TRUE.\n"                                       # activate compound channel (if channel_option=2, with reach-based routing and UDMP=1) Default=.FALSE.
  form *= "route_lake_f     = \"./DOMAIN/LAKEPARM.nc\"\n"                     # lake parameter file (e.g.: "LAKEPARM.nc") REQUIRED if lakes are on
  form *= "GWBASESWCRT      =   1\n"                                          # activate baseflow bucket model...(0=none, 1=exp. bucket, 2=pass-through)
# form *= "gwbasmskfil      = \"./DOMAIN/GWBASINS.nc\"\n"                     # Groundwater/baseflow mask on land surface grid, required for active baseflow model (1/2) and UDMP_OPT=0
  form *= "GWBUCKPARM_file  = \"./DOMAIN/GWBUCKPARM.nc\"\n"                   # Groundwater bucket parameter file (e.g.: "GWBUCKPARM.nc")
  form *= "UDMP_OPT         =   1\n"                                          # User defined mapping, such as NHDPlus: 0=no (default), 1=yes
  form *= "udmap_file       = \"./DOMAIN/spatialweights.nc\"\n"               # user-defined mapping file (e.g.: "spatialweights.nc")
  form *= "/\n"
  form *= "&NUDGING_nlist\n"                                                  # NUDGING PARAMS
  form *= "timeSlicePath          = \"./nudgingTimeSliceObs/\"\n"             # Path to the "timeslice" observation files
  form *= "nudgingParamFile       = \"./DOMAIN/nudgingParams.nc\"\n"          # Nudging parameter file
  form *= "nudgingLastObsFile     = \'/nudging/cold/start/file\'\n"           # Nudging restart file (default='' uses nudgingLastObs.YYYY-mm-dd_HH:MM:SS.nc AT RUN INITALIZATION)
  form *= "readTimesliceParallel  =  .TRUE.\n"                                # Parallel input of nudging timeslice observation files
  form *= "temporalPersistence    =  .TRUE.\n"                                # temporalPersistence defaults to true, only runs if necessary params are present
  form *= "nLastObs               =     480\n"                                # number of last (obs, modeled) pairs to save in nudgingLastObs for removal of bias (max length)
  form *= "persistBias            =  .TRUE.\n"                                # persist bias after the last observation (If using temporalPersistence the last obs persists by default)
  form *= "biasWindowBeforeT0     = .FALSE.\n"                                # An(F) vs Forecast(T) bias persistence (F=window ends at model time(moving),T=window ends at init=t0(fcst))
  form *= "maxAgePairsBiasPersist =       3\n"                                # If persistBias: Only use this many last (obs, modeled) pairs. (If Commented out, Default=-1*nLastObs)
  form *= "minNumPairsBiasPersist =       1\n"                                # If persistBias: The minimum number of last (obs, modeled) pairs, with age less than maxAgePairsBiasPersist
  form *= "invDistTimeWeightBias  =  .TRUE.\n"                                # If persistBias: give more weight to observations closer in time? (default=FALSE)
  form *= "noConstInterfBias      =  .TRUE.\n"                                # If persistBias: "No constructive interference in bias correction?", Reduce the bias adjustment...
  form *= "/\n"
  fpa = My.ouvre("hydro.namelist", "w") ; write(fpa, form) ; close(fpa)

  form  = "&NOAHLSM_OFFLINE\n"                                                # write the namelist.hrldas file
  form *= "HRLDAS_SETUP_FILE = \"./DOMAIN/wrfinput_d01.nc\"\n"
  form *= "INDIR             = \"./FORCING\"\n"
  form *= "SPATIAL_FILENAME  = \"./DOMAIN/soil_properties.nc\"\n"
  form *= "OUTDIR            = \"./\"\n"
  form *= "START_YEAR  = 2011\n"
  form *= "START_MONTH =   08\n"
  form *= "START_DAY   =   26\n"
  form *= "START_HOUR  =   00\n"
  form *= "START_MIN   =   00\n"
  form *= "RESTART_FILENAME_REQUESTED = \"RESTART/RESTART.2011082600_DOMAIN1\"\n"
  form *= "KDAY        =    7\n"                                              # simulation length in  days
# form *= "KHOUR       =    8\n"                                              # simulation length in hours
  form *= "DYNAMIC_VEG_OPTION                = 4\n"                           # Physics options (see the documentation for details)
  form *= "CANOPY_STOMATAL_RESISTANCE_OPTION = 1\n"
  form *= "BTR_OPTION                        = 1\n"
  form *= "RUNOFF_OPTION                     = 3\n"
  form *= "SURFACE_DRAG_OPTION               = 1\n"
  form *= "FROZEN_SOIL_OPTION                = 1\n"
  form *= "SUPERCOOLED_WATER_OPTION          = 1\n"
  form *= "RADIATIVE_TRANSFER_OPTION         = 3\n"
  form *= "SNOW_ALBEDO_OPTION                = 1\n"
  form *= "PCP_PARTITION_OPTION              = 1\n"
  form *= "TBOT_OPTION                       = 2\n"
  form *= "TEMP_TIME_SCHEME_OPTION           = 3\n"
  form *= "GLACIER_OPTION                    = 2\n"
  form *= "SURFACE_RESISTANCE_OPTION         = 4\n"
  form *= "FORCING_TIMESTEP        = 3600\n"
  form *= "NOAH_TIMESTEP           = 3600\n"
  form *= "OUTPUT_TIMESTEP         = 3600\n"
  form *= "RESTART_FREQUENCY_HOURS =   24\n"                                  # Land surface model restart file write frequency
  form *= "SPLIT_OUTPUT_COUNT      =    1\n"                                  # Split output after split_output_count output times
  form *= "NSOIL                   =    4\n"                                  # Soil layer specification
  form *= "soil_thick_input(1)     =    0.10\n"
  form *= "soil_thick_input(2)     =    0.30\n"
  form *= "soil_thick_input(3)     =    0.60\n"
  form *= "soil_thick_input(4)     =    1.00\n"
  form *= "ZLVL                    =   10.0\n"                                # Forcing data measurement height for winds, temp, humidity
  form *= "rst_bi_in               =    0\n"                                  # 0: use netcdf input restart file 1: use parallel io for reading multiple restart files (1 per core)
  form *= "rst_bi_out              =    0\n"                                  # 0: use netcdf output restart file 1: use parallel io for outputting multiple restart files (1 per core)
  form *= "/\n"
  form *= "&WRF_HYDRO_OFFLINE\n"                                              # Specification of forcing data:  1=HRLDAS-hr format, 2=HRLDAS-min format, 3=WRF, 4=Idealized,
  form *= "FORC_TYP                =    1\n"                                  # 5=Ideal w/ Spec.Precip., 6=HRLDAS-hrl y fomat w/ Spec. Precip., 7=WRF w/ Spec. Precip., 9=Channel-only,
  form *= "/\n"                                                               # 10=Channel+Bucket only forcing, see hydro.namelist output_channelBucket_influxes
  fpa = My.ouvre("namelist.hrldas", "w") ; write(fpa, form) ; close(fpa)
end

function ccarun()
  form  = "#!/bin/tcsh\n"
  form *= "#SBATCH --account=def-wperrie\n"
  form *= "#SBATCH --job-name=x.wrf_hydro\n"
  form *= "#SBATCH   --output=x.wrf_hydro.sbatco\n"
  form *= "#SBATCH --ntasks=2\n"
  form *= "#SBATCH --mem-per-cpu=2000M\n"
  form *= "#SBATCH --time=00:59:00\n"
  form *= "echo Job running at \`hostname\`\n"
  form *= "echo Job running on \`grep \"model name\" /proc/cpuinfo | head -1\`\n"
  form *= "echo Job running in \`pwd\`\n"
  form *= "echo Job beginning  \`date\`\n"
  form *= "echo mpirun -np 2 ./wrf_hydro.exe\n"
  form *= "srun              ./wrf_hydro.exe\n"
  form *= "echo Job ending at \`date\`\n"
  fpa = My.ouvre("wrf_hydro.sbatch", "w") ; write(fpa, form) ; close(fpa)
  print("sbatch $ccabat\n")
#   run(`sbatch $ccabat`)
end

function cmcrun()
  form  = "#!/bin/bash\n"
  form *= "#JGEN -r name=wrf_hydro\n"
  form *= "#JGEN -r outpath=wrf_hydro_out\n"
  form *= "#JGEN -r joinouterr=y\n"
  form *= "#JGEN -c nslots=1\n"
  form *= "#JGEN -c ncores=16\n"
  form *= "#JGEN -c memory=48000M\n"
  form *= "#JGEN -c tmpfs=8000M\n"
  form *= "#JGEN -c image=dfo/dfo_all_default_ubuntu-18.04-amd64_latest\n"
  form *= "#JGEN -r wallclock=24:00:00\n"
  form *= "# load mpi environment\n"
  form *= ". ssmuse-sh -x /fs/ssm/main/opt/intelcomp/intelpsxe-cluster-19.0.3.199\n"
  form *= ". ssmuse-sh -x /fs/ssm/main/opt/openmpi/openmpi-3.1.2--hpcx-2.4.0-mofed-4.6--intel-19.0.3.199\n"
  form *= "# openmpi settings\n"
  form *= "# export OMPI_MCA_plm_rsh_no_tree_spawn=1\n"
  form *= "# export RUMPIRUN_ENV=\"LD_LIBRARY_PATH\"\n"
  form *= "# export OMPI_MCA_rmaps_base_mapping_policy=node\n"
  form *= "# export OMPI_MCA_hwloc_base_binding_policy=none\n"
  form *= "# export OMPI_MCA_coll_hcoll_enable=0\n"
  form *= "# export OMPI_MCA_pml=ucx\n"
  form *= "# export OMPI_MCA_btl=^shmem,openib\n"
  form *= "# export UCX_NET_DEVICES=\"\$(cd /sys/class/infiniband; /bin/ls -d mlx[45]_0):1\"\n"
  form *= "# run program\n"
  form *= "cd /gpfs/fs2/dfo/hpcmc-comda/dfo_odis/rid000/work/workh\n"
  form *= "rumpirun ./wrf_hydro.exe\n"
  fpa = My.ouvre("wrf_hydro.jgen", "w") ; write(fpa, form) ; close(fpa)
  print("jobsub -c gpsc2 $cmcbat\n")
#   run(`jobsub -c gpsc2 $cmcbat`)
end

print("\n")
nlis == NOMODS && namelists()
ARGS[1] == "cca" && ccarun()
ARGS[1] == "cmc" && cmcrun()
print("\n")
exit(0)
