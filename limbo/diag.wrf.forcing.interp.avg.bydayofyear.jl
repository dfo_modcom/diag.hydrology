#=
 = This program calculates a running 30-day, multi-year average over
 = a 6-h domain-averaged time series already stored on file.  Averages
 = for 0/6/12/18 UTC are also computed - RD December 2023.
 =#

using My, NetCDF
const MISS             = -9999.0                        # generic missing value
const GRDMIS           = 99999.0                        # generic missing gridbox value
const D2R              = 3.141592654 / 180.0            # degrees to radians conversion
const YEAR             = 120                            # number of 6-h intervals in a month
const HALF             = div(YEAR, 2)

if (argc = length(ARGS)) != 3
  print("\nUsage: jjj $(basename(@__FILE__)) wrf 1990 2004\n\n")
  exit(1)
end

fila = "forcing_" * ARGS[1] * "_" * ARGS[2] * "_" * ARGS[3] * ".bydayofyr.nc"
filb = "forcing_" * ARGS[1] * "_" * ARGS[2] * "_" * ARGS[3] * ".bydayofyr.avg.nc"
filc = "forcing_" * ARGS[1] * "_" * ARGS[2] * "_" * ARGS[3] * ".00dayofyr.avg.nc"
fild = "forcing_" * ARGS[1] * "_" * ARGS[2] * "_" * ARGS[3] * ".06dayofyr.avg.nc"
file = "forcing_" * ARGS[1] * "_" * ARGS[2] * "_" * ARGS[3] * ".12dayofyr.avg.nc"
filf = "forcing_" * ARGS[1] * "_" * ARGS[2] * "_" * ARGS[3] * ".18dayofyr.avg.nc"
mask = falses(length(-HALF:HALF)) ; for a = 3:4:length(-HALF:HALF)  mask[a] = true  end
tims = ncread(fila, "time", start=[1], count=[-1])
tatt = ncgetatt(fila, "time", "units")

vars = ["R2D", "S2D", "T2D", "Q2D", "U2D", "V2D", "PSFC", "RAINRATE", "SWDOWN", "LWDOWN"]
for vnam in vars
  data = ncread(fila, vnam, start=[1,1,1], count=[-1,-1,-1])
  (nlon, nlat, ntim) = size(data)

  datb = GRDMIS * ones(nlon, nlat, ntim)                                      # allow half-year missing values
  datc = GRDMIS * ones(nlon, nlat, ntim)                                      # at the beginning and end of the
  for a = 1+HALF:ntim-HALF                                                    # timeseries, and intermediate
    if data[1,1,a] != GRDMIS                                                  # average values are also missing
      vsum = vnum = wsum = wnum = 0.0                                         # if any 6-h values are missing
      for b = -HALF:HALF
        if data[1,1,a+b] != GRDMIS
          vsum += data[1,1,a+b]
          vnum += 1
          if mask[b+HALF+1]                                                   # also sum daily, by UTC hour
            wsum += data[1,1,a+b]                                             # (where mask[HALF+1] = true)
            wnum += 1
          end
        end
      end
      vnum == YEAR + 1 && (datb[a] = vsum / vnum ; datc[a] = wsum / wnum)
    end
  end

  if !isfile(filb)                                                            # then store the annual-average
    nccreer(filb, ntim, 1, 1, GRDMIS; vnames = vars)                          # series and update the dates
    ncwrite(tims, filb, "time", start=[1], count=[-1])
    ncputatt(filb, "time", Dict("units" => tatt))
  end
  print("writing $filb $vnam\n")
  ncwrite(datb, filb, vnam, start=[1,1,1], count=[-1,-1,-1])

  filz = filc ; datz = datc[1,1,1:4:end]
  if !isfile(filz)                                                            # and store the annual-average
    timz = tims[1:4:end] ; nzim = length(timz)                                # series for each UTC hour
    nccreer(filz, nzim, 1, 1, GRDMIS; vnames = vars)
    ncwrite(timz, filz, "time", start=[1], count=[-1])
    ncputatt(filz, "time", Dict("units" => tatt))
  end
  print("writing $filz $vnam\n")
  ncwrite(datz, filz, vnam, start=[1,1,1], count=[-1,-1,-1])

  filz = fild ; datz = datc[1,1,2:4:end]
  if !isfile(filz)
    timz = tims[2:4:end] ; nzim = length(timz)
    nccreer(filz, nzim, 1, 1, GRDMIS; vnames = vars)
    ncwrite(timz, filz, "time", start=[1], count=[-1])
    ncputatt(filz, "time", Dict("units" => tatt))
  end
  print("writing $filz $vnam\n")
  ncwrite(datz, filz, vnam, start=[1,1,1], count=[-1,-1,-1])

  filz = file ; datz = datc[1,1,3:4:end]
  if !isfile(filz)
    timz = tims[3:4:end] ; nzim = length(timz)
    nccreer(filz, nzim, 1, 1, GRDMIS; vnames = vars)
    ncwrite(timz, filz, "time", start=[1], count=[-1])
    ncputatt(filz, "time", Dict("units" => tatt))
  end
  print("writing $filz $vnam\n")
  ncwrite(datz, filz, vnam, start=[1,1,1], count=[-1,-1,-1])

  filz = filf ; datz = datc[1,1,4:4:end]
  if !isfile(filz)
    timz = tims[4:4:end] ; nzim = length(timz)
    nccreer(filz, nzim, 1, 1, GRDMIS; vnames = vars)
    ncwrite(timz, filz, "time", start=[1], count=[-1])
    ncputatt(filz, "time", Dict("units" => tatt))
  end
  print("writing $filz $vnam\n")
  ncwrite(datz, filz, vnam, start=[1,1,1], count=[-1,-1,-1])
end
exit(0)
