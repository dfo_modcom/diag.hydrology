#=
 = Convert three/six-hourly streamflow timeseries data to monthly means
 = for each river outlet that pours into the ocean - RD Feb 2023.
 =#

using My, Printf, NetCDF

const DELT             = 3.0                            # timestep (hours)
const MISS             = -9999.0                        # generic missing value

if (argc = length(ARGS)) != 1
  print("\nUsage: jjj $(basename(@__FILE__)) WRF-Hydro_1990010106_2020123118_3h.nc\n\n")
  exit(1)
end
fila = ARGS[1]
filb = ARGS[1][1:end-2] * "monthly.nc"

tims = ncread(fila,     "time", start=[1],     count=[-1])                    # read the time-invariant data
xind = ncread(fila, "rivgridx", start=[1,1],   count=[-1,-1])                 # and DELT-time-resolution flow
yind = ncread(fila, "rivgridy", start=[1,1],   count=[-1,-1])
rlat = ncread(fila, "riverlat", start=[1,1],   count=[-1,-1])
rlon = ncread(fila, "riverlon", start=[1,1],   count=[-1,-1])
olat = ncread(fila, "oceanlat", start=[1,1],   count=[-1,-1])
olon = ncread(fila, "oceanlon", start=[1,1],   count=[-1,-1])
flow = ncread(fila, "riverflo", start=[1,1,1], count=[-1,-1,-1])
tatt = ncgetatt(fila, "time", "units")
ntim = length(tims)
nlet = length(xind)

tmpa = dateref(tims[  1], tatt)
tmpb = dateref(tims[end], tatt)
mona = tmpa[1:4] * "-" * tmpa[5:6] * "-15-00"
monb = tmpb[1:4] * "-" * tmpb[5:6] * "-15-00"
daya = tmpa[1:4] * "-" * tmpa[5:6] * "-" * tmpa[7:8] * "-" * tmpa[9:10]
dayb = tmpb[1:4] * "-" * tmpb[5:6] * "-" * tmpb[7:8] * "-" * tmpb[9:10]

mons = Array{Float64}(undef,0)                                                # construct the monthly timeseries dates
tima = My.datesous("1900-01-01-00", mona, "hr")
timb = My.datesous("1900-01-01-00", monb, "hr")
while tima <= timb
  global tima, mona
  push!(mons, tima)
  mona = dateadd(mona, 30, "dy")
  mona = mona[1:7] * "-15-00"
  tima = My.datesous("1900-01-01-00", mona, "hr")
end
nmon = length(mons)

function nccreate(fn::AbstractString, ntim::Int, nlat::Int, nlon::Int, missing::Float64)
  nctim = NcDim("time", ntim, atts = Dict{Any,Any}("units"=>"hours since 1900-01-01 00:00:0.0"), values = collect(range(    0, stop =                  ntim - 1 , length = ntim)))
  nclat = NcDim( "lat", nlat, atts = Dict{Any,Any}("units"=>                   "degrees_north"), values = collect(range( 50.0, stop =  50.0 + 0.001 * (nlat - 1), length = nlat)))
  nclon = NcDim( "lon", nlon, atts = Dict{Any,Any}("units"=>                    "degrees_east"), values = collect(range(280.0, stop = 280.0 + 0.001 * (nlon - 1), length = nlon)))
  ncvrs = Array{NetCDF.NcVar}(undef, 7)
  ncvrs[1] = NcVar("rivgridx", [nclon, nclat       ], atts = Dict{Any,Any}("units"=>"none",   "long_name"=>"river outlet x-index on WRF-Hydro grid", "missing_value"=>missing), t=  Int64, compress=-1)
  ncvrs[2] = NcVar("rivgridy", [nclon, nclat       ], atts = Dict{Any,Any}("units"=>"none",   "long_name"=>"river outlet y-index on WRF-Hydro grid", "missing_value"=>missing), t=  Int64, compress=-1)
  ncvrs[3] = NcVar("riverlat", [nclon, nclat       ], atts = Dict{Any,Any}("units"=>"degree", "long_name"=>"river outlet latitude",                  "missing_value"=>missing), t=Float64, compress=-1)
  ncvrs[4] = NcVar("riverlon", [nclon, nclat       ], atts = Dict{Any,Any}("units"=>"degree", "long_name"=>"river outlet longitude",                 "missing_value"=>missing), t=Float64, compress=-1)
  ncvrs[5] = NcVar("oceanlat", [nclon, nclat       ], atts = Dict{Any,Any}("units"=>"degree", "long_name"=>"ocean pour point latitude",              "missing_value"=>missing), t=Float64, compress=-1)
  ncvrs[6] = NcVar("oceanlon", [nclon, nclat       ], atts = Dict{Any,Any}("units"=>"degree", "long_name"=>"ocean pour point longitude",             "missing_value"=>missing), t=Float64, compress=-1)
  ncvrs[7] = NcVar("riverflo", [nclon, nclat, nctim], atts = Dict{Any,Any}("units"=>"m3 s-1", "long_name"=>"river outlet streamflow",                "missing_value"=>missing), t=Float64, compress=-1)
  descript = "WRF-Hydro streamflow timeseries for a single oceanic river outlet on each proxy latitude, where true outlet positions (riverlat/lon) " *
             "as well as ocean pour points that are slightly displaced into the ocean (oceanlat/lon), are given as corresponding time-invariants"
  ncfil = NetCDF.create(fn, ncvrs, gatts = Dict{Any,Any}("Description"=>descript), mode = NC_NETCDF4)
  print("\ncreated $fn with $ntim times $nlat lats and $nlon lons\n\n")
end

nccreate(     filb, nmon, nlet, 1, MISS)
ncwrite(mons, filb,     "time", start=[1],   count=[-1])
ncwrite(xind, filb, "rivgridx", start=[1,1], count=[-1,-1])
ncwrite(yind, filb, "rivgridy", start=[1,1], count=[-1,-1])
ncwrite(rlat, filb, "riverlat", start=[1,1], count=[-1,-1])
ncwrite(rlon, filb, "riverlon", start=[1,1], count=[-1,-1])
ncwrite(olat, filb, "oceanlat", start=[1,1], count=[-1,-1])
ncwrite(olon, filb, "oceanlon", start=[1,1], count=[-1,-1])

flom = zeros(nlet, nmon)                                                      # then construct the monthly timeseries
floc = zeros(nlet, nmon)
dnow = daya
for a = 1:ntim
  global dnow
  mnow = My.datesous("1900-01-01-00", dnow[1:7] * "-15-00", "hr")
  mind = findfirst(isequal(mnow), mons)
  for b = 1:nlet
    if flow[1,b,a] > 0
      flom[b,mind] += flow[1,b,a]
      floc[b,mind] +=      1.0
    end
  end
  dnow = My.dateadd(dnow, DELT, "hr")
end

for a = 1:nmon, b = 1:nlet
  if floc[b,a] == 0  flom[b,a]  = MISS
  else               flom[b,a] /= floc[b,a]  end
end

ncwrite(flom, filb, "riverflo", start=[1,1,1], count=[-1,-1,-1])
exit(0)
