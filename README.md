```shell
`
# get a copy (GPSC shell is bash; Compute Canada shell is tcsh)
git clone https://gitlab.com/dfo_modcom/diag.hydrology.git

# define a script (ccp) to copy programs in this repo to ~/bin (as in "ccp x.jl"),
# where STEMZ and path can be defined in ~/.profile (bash) or ~/.cshrc (tcsh)
(bash) export STEMZ="~/bin" ; export PATH=${STEMZ}:$PATH
(tcsh) setenv STEMZ "~/bin" ; set    path=($STEMZ  $path)
       echo 'cp $1 $STEMZ ; chmod +x $STEMZ/$1' > $STEMZ/ccp
                            chmod +x              $STEMZ/ccp

# other useful packages and settings (ubuntu 20.04/22.04 bash/tcsh) include R,
# julia (http://julialang.org/), GrADs (http://cola.gmu.edu/grads/downloads.php),
# GNU parallel (http://www.gnu.org/software/parallel/), and GDAL/NetCDF/NCO/udunits
# on laptops (ubuntu 18.04/win), university (Digital Research Alliance of Canada),
# and Government of Canada (General Purpose Science Cluster) computers, for example via:
# (lap)  sudo apt install gdal-bin gdal-data libgdal-dev libnetcdf-dev netcdf-bin nco julia
# (DRAC) module load StdEnv/2023 julia/1.9.3 (or StdEnv/2020 julia/1.4.1 python/3.10.2 scipy-stack/2022a gcc/9.3.0 gdal/3.2.3 nco/5.0.6 proj/7.2.1 r/4.0.5)
       alias jjj 'julia   $STEMZ/\!*'
       alias rrr 'Rscript $STEMZ/\!*'
(or)   echo 'julia   $STEMZ/$@' > $STEMZ/jjj ; chmod +x $STEMZ/jjj
       echo 'Rscript $STEMZ/$@' > $STEMZ/rrr ; chmod +x $STEMZ/rrr
       alias work 'cd ~/work      ; ls -al'
       alias wrkg 'cd ~/work/workg; ls -al'
       alias wrkh 'cd ~/work/workh; ls -al'
       alias wrki 'cd ~/work/worki; ls -al'
       alias sof  'cd ~/soft;       ls -al'
(bash) export VS="cccnar" ; echo export VS="narwrf"    ; echo export VS="wrfccc"
       export RG="arc"    ;      export R4=$RG"_RCP45" ;      export R8=$RG"_RCP85"
       export RG="atl"    ;      export R4=$RG"_RCP45" ;      export R8=$RG"_RCP85"
(tcsh) set    VS="cccnar" ; echo set    VS="narwrf"    ; echo set    VS="wrfccc"
       set    RG="arc"    ;      set    R4=$RG"_RCP45" ;      set    R8=$RG"_RCP85"
       set    RG="atl"    ;      set    R4=$RG"_RCP45" ;      set    R8=$RG"_RCP85"
wrkg ; mkdir DOMAIN_full_full FORCING GRID HYDROSHEDS NRCAN WRF WRF/WPS hydat sword hydrolakes merit wrf_hydro_pour
       mkdir FORCING.ccs FORCING.ccs.mean SPINUP.ccs SPINUP.ccs/dat
       mkdir FORCING.era FORCING.era.mean SPINUP.era SPINUP.era/dat
       mkdir FORCING.h45 FORCING.had.mean SPINUP.had SPINUP.had/dat FORCING.h85
       mkdir FORCING.wrf FORCING.wrf.mean SPINUP.wrf SPINUP.wrf/dat
       mkdir FORCING.w85                  SPINUP.w85 SPINUP.w85/dat

# (optional) set up working dirs for subdomains at the same resolution as the full domain
(bash) export WRFSDOM="_AuxF" ; alias wrkg='cd ~/work/workg_AuxF; ls -al'
       export WRFSDOM="_Cali" ; alias wrkg='cd ~/work/workg_Cali; ls -al'
       export WRFSDOM="_East" ; alias wrkg='cd ~/work/workg_East; ls -al'
(tcsh) setenv WRFSDOM "_AuxF" ; alias wrkg 'cd ~/work/workg_AuxF; ls -al'
       setenv WRFSDOM "_Cali" ; alias wrkg 'cd ~/work/workg_Cali; ls -al'
       setenv WRFSDOM "_East" ; alias wrkg 'cd ~/work/workg_East; ls -al'
wrkg ; ln -s ~/work/workg/hydat      hydat
       ln -s ~/work/workg/sword      sword
       ln -s ~/work/workg/HYDROSHEDS HYDROSHEDS
       ln -s ~/work/workg/EEZ        EEZ
       ln -s ~/work/workg/hydrolakes hydrolakes
       ln -s ~/work/workg/merit/merit_hydrodem_30_85N_180_00W_30sec_elevtn.nc        merit/merit_hydrodem_30_85N_180_00W_30sec_elevtn.nc
       ln -s ~/work/workg/merit/merit_hydrodem_30_85N_180_00W_30sec_elevtn_adjust.nc merit/merit_hydrodem_30_85N_180_00W_30sec_elevtn_adjust.nc

----------------------------------------------------------------------------------//

# download the Water Survey of Canada HyDAT stations (https://wateroffice.ec.gc.ca/contactus/faq_e.html),
# the SWOT/SWORD and HydroSHEDS river networks (https://www.hydrosheds.org/products/hydrorivers and
# https://zenodo.org/record/3898570), the HydroSHEDS flow direction grid (based on the HydroSHEDS-v1
# void-filled DEM at 15-s resolution), unpack each dataset and identify a HyDAT calibration subset
wrkg ; cd hydat
       wget --mirror -e robots=off https://collaboration.cmc.ec.gc.ca/cmc/hydrometrics/www
       ln -s collaboration.cmc.ec.gc.ca/cmc/hydrometrics/www/Hydat_sqlite3_20221024.zip Hydat_sqlite3.zip
       unzip                                                                            Hydat_sqlite3.zip
       jjj unpack.hydat.station.jl                                                      Hydat.sqlite3
wrkg ; mkdir                                                hydat.calibration hydat.validation
       jjj diag.wrf.ocean.entry.timeseries.vetting.jl hydat hydat.calibration
       cd hydat.calibration
       csh hydat_vetting_rhbn_2500.com
       ls -1 hydat* | grep -E '.nc$' | parallel julia /home/riced/bin/unpack.hydat.station.text.jl 2017-10-01-00
wrkg ; cd sword
       wget "https://zenodo.org/record/7410433/files/SWORD_v14_shp.zip?download=1"
       mv "SWORD_v14_shp.zip?download=1" SWORD_v14_shp.zip ; unzip SWORD_v14_shp.zip
wrkg ; cd HYDROSHEDS
       wget https://data.hydrosheds.org/file/HydroRIVERS/HydroRIVERS_v10_na_shp.zip
       wget https://data.hydrosheds.org/file/HydroRIVERS/HydroRIVERS_v10_ar_shp.zip
       wget https://data.hydrosheds.org/file/HydroRIVERS/HydroRIVERS_v10_gr_shp.zip
       unzip HydroRIVERS_v10_na_shp.zip ; mv HydroRIVERS_v10_na_shp/* . ; rmdir HydroRIVERS_v10_na_shp
       unzip HydroRIVERS_v10_ar_shp.zip ; mv HydroRIVERS_v10_ar_shp/* . ; rmdir HydroRIVERS_v10_ar_shp
       unzip HydroRIVERS_v10_gr_shp.zip ; mv HydroRIVERS_v10_gr_shp/* . ; rmdir HydroRIVERS_v10_gr_shp
       rm    HydroRIVERS_TechDoc_v10.pdf     HydroRIVERS_v10_??_shp.zip
#      wget https://data.hydrosheds.org/file/hydrosheds-v1-dem/hyd_glo_dem_15s.zip ; unzip hyd_glo_dem_15s.zip ; gdal_translate hyd_glo_dem_15s.tif hyd_glo_dem_15s.tif.nc
#      jjj diag.hydroshed.watershed.dem.jl hyd_glo_dem_15s.tif.nc hydrosheds_30_85N_180_00W_15sec_elevtn.nc
#      rm hyd_glo_dem_15s.zip hyd_glo_dem_15s.tif hyd_glo_dem_15s.tif.nc HydroSHEDS_TechDoc_v1_4.pdf
#      "writing hydrosheds_30_85N_180_00W_15sec_elevtn.nc with 348244414 missing values (62.2% of 559828800)"
#      ncks -d lat,,,10      -d lon,,,10        hydrosheds_30_85N_180_00W_15sec_elevtn.nc hydrosheds_30_85N_180_00W_15sec_elevtn10.nc
#      ncks -d lat,6600,7100 -d lon,25600,26800 hydrosheds_30_85N_180_00W_15sec_elevtn.nc hydrosheds_30_85N_180_00W_15sec_elevtn10.nc
#      wget https://data.hydrosheds.org/file/hydrosheds-v1-acc/hyd_glo_acc_15s.zip ; unzip hyd_glo_acc_15s.zip ; gdal_translate hyd_glo_acc_15s.tif hyd_glo_acc_15s.tif.nc
#      jjj diag.hydroshed.watershed.acc.jl hyd_glo_acc_15s.tif.nc hydrosheds_30_85N_180_00W_15sec_floacc.nc
#      rm hyd_glo_acc_15s.zip hyd_glo_acc_15s.tif hyd_glo_acc_15s.tif.nc HydroSHEDS_TechDoc_v1_4.pdf
#      "writing hydrosheds_30_85N_180_00W_15sec_floacc.nc with 348125826 missing values (62.2% of 559828800)"
#      ncks -d lat,,,10      -d lon,,,10        hydrosheds_30_85N_180_00W_15sec_floacc.nc hydrosheds_30_85N_180_00W_15sec_floacc10.nc
#      ncks -d lat,6600,7100 -d lon,25600,26800 hydrosheds_30_85N_180_00W_15sec_floacc.nc hydrosheds_30_85N_180_00W_15sec_floacc10.nc
       wget https://data.hydrosheds.org/file/hydrosheds-v1-dir/hyd_glo_dir_15s.zip ; unzip hyd_glo_dir_15s.zip ; gdal_translate hyd_glo_dir_15s.tif hyd_glo_dir_15s.tif.nc
       jjj diag.hydroshed.watershed.dir.jl hyd_glo_dir_15s.tif.nc hydrosheds_30_85N_180_00W_15sec_flodir.nc
       rm hyd_glo_dir_15s.zip hyd_glo_dir_15s.tif hyd_glo_dir_15s.tif.nc HydroSHEDS_TechDoc_v1_4.pdf
       "writing hydrosheds_30_85N_180_00W_15sec_flodir.nc with 348125826 missing values (62.2% of 559828800)"
#      ncks -d lat,,,10      -d lon,,,10        hydrosheds_30_85N_180_00W_15sec_flodir.nc hydrosheds_30_85N_180_00W_15sec_flodir10.nc
#      ncks -d lat,6600,7100 -d lon,25600,26800 hydrosheds_30_85N_180_00W_15sec_flodir.nc hydrosheds_30_85N_180_00W_15sec_flodir10.nc
#      jjj diag.hydroshed.watershed.hydat.jl 0.4 ; di *png

# create the boundaries of watersheds for each river that drains to the oceans
# (again based on the HydroSHEDS-v1 void-filled DEM at 15-s resolution, but perhaps omitting Greenland watersheds below)
# (using Shapefile ; rshp = "HydroLAKES_polys_v10_Atlantic_999km2.shp" ; rtab = Shapefile.Table(rshp) ; rtab.Hylak_id')
wrkg ; cd HYDROSHEDS
       wget https://data.hydrosheds.org/file/HydroRIVERS/HydroRIVERS_v10_shp.zip
       wget https://data.hydrosheds.org/file/hydrobasins/standard/hybas_na_lev01-12_v1c.zip
       wget https://data.hydrosheds.org/file/hydrobasins/standard/hybas_ar_lev01-12_v1c.zip
       wget https://data.hydrosheds.org/file/hydrobasins/standard/hybas_gr_lev01-12_v1c.zip
       unzip -u hybas_na_lev01-12_v1c.zip       ; ogrinfo -al -so hybas_na_lev12_v1c.shp    > hybas_na_lev12_v1c.txt
       unzip -u hybas_ar_lev01-12_v1c.zip       ; ogrinfo -al -so hybas_ar_lev12_v1c.shp    > hybas_ar_lev12_v1c.txt
       unzip -u hybas_gr_lev01-12_v1c.zip       ; ogrinfo -al -so hybas_gr_lev12_v1c.shp    > hybas_gr_lev12_v1c.txt
#      unzip -u hybas_na_lev00_v1c.zip          ; ogrinfo -al -so HydroRIVERS_v10.shp       > HydroRIVERS_v10.txt
#      unzip -u dams-rev01-global-shp.zip       ; ogrinfo -al -so GRanD_dams_v1_1.shp       > GRanD_dams_v1_1.txt
#      unzip -u reservoirs-rev01-global-shp.zip ; ogrinfo -al -so GRanD_reservoirs_v1_1.shp > GRanD_reservoirs_v1_1.txt
       unzip -u HydroRIVERS_v10_shp.zip ; mv HydroRIVERS_v10_shp/* . ; rmdir HydroRIVERS_v10_shp
       rm hybas_??_lev0* hybas_??_lev10* hybas_??_lev11* HydroBASINS_TechDoc_v1c.pdf HydroRIVERS_TechDoc_v10.pdf
       wget ftp://ftp.cdc.noaa.gov/Datasets/20thC_ReanV3/timeInvariantSI/hgt.sfc.nc
       mkdir zip shp shp.na shp.ar shp.gr ; mv *.zip hybas*.txt zip
       ogr2ogr -spat -180.0 30.0 0.0 85.0                                                                                                                 HydroRIVERS_v10_30_85N_180_00W.shp HydroRIVERS_v10.shp
       ogr2ogr -sql "SELECT HYRIV_ID, MAIN_RIV, UPLAND_SKM, HYBAS_L12 FROM HydroRIVERS_v10_30_85N_180_00W WHERE (NEXT_DOWN = 0) ORDER BY UPLAND_SKM DESC" HydroRIVERS_v10_30_85N_180_00W.csv HydroRIVERS_v10_30_85N_180_00W.shp
       ogrinfo -al -so HydroRIVERS_v10_30_85N_180_00W.shp > HydroRIVERS_v10_30_85N_180_00W.txt
       cd shp.na ; cp ../HydroRIVERS_v10_30_85N_180_00W* ../hybas_na_lev12_v1c* . ; jjj diag.hydroshed.watershed.refer.jl HydroRIVERS_v10.shp hybas_na_lev12_v1c.shp ; rm HydroRIVERS* hybas*
       cd shp.ar ; cp ../HydroRIVERS_v10_30_85N_180_00W* ../hybas_ar_lev12_v1c* . ; jjj diag.hydroshed.watershed.refer.jl HydroRIVERS_v10.shp hybas_ar_lev12_v1c.shp ; rm HydroRIVERS* hybas*
       cd shp.gr ; cp ../HydroRIVERS_v10_30_85N_180_00W* ../hybas_gr_lev12_v1c* . ; jjj diag.hydroshed.watershed.refer.jl HydroRIVERS_v10.shp hybas_gr_lev12_v1c.shp ; rm HydroRIVERS* hybas*
wrkg ; cd HYDROSHEDS/shp
       cp ../HydroRIVERS_v10_30_85N_180_00W.csv ./
       jjj diag.hydroshed.watershed.refer.links.jl ../shp.na/
       jjj diag.hydroshed.watershed.refer.links.jl ../shp.ar/
       jjj diag.hydroshed.watershed.refer.group.jl HydroRIVERS_v10.shp hybas_na_lev12_v1c.shp 1000 41
       mv *outlet* .. ; cd ..
       grads -blc "diag.hydroshed.watershed.refer HydroRIVERS_v10_30_85N_180_00W_outlets_1000_41N" ; di HydroRIVERS_v10_30_85N_180_00W_outlets_1000_41N.png

# download shapefiles encompassing the Canadian, Alaskan, and Greenland coasts and save one
# polygon for each coastline as a contiguous.csv file, in order to identify river outflows
wrkg ; cd EEZ
       wget https://www.marineregions.org/download_file.php?name=EEZ_land_union_v3_202003.zip
       unzip                                                     EEZ_land_union_v3_202003.zip
       ogrinfo -al -so                      EEZ_Land_v3_202030.shp        > EEZ_Land_v3_202030.txt
       ogr2ogr -spat -150.0 60.0 -50.0 75.0 EEZ_Land_v3_202030_Canada.shp   EEZ_Land_v3_202030.shp
       ogrinfo -al -so                      EEZ_Land_v3_202030_Canada.shp > EEZ_Land_v3_202030_Canada.txt
       ogr2ogr -sql "SELECT MRGID_EEZ, TERRITORY1, MRGID_TER1, ISO_TER1, UN_TER1, SOVEREIGN1, MRGID_SOV1, ISO_SOV1, UN_SOV1, AREA_KM2 FROM EEZ_Land_v3_202030_Canada" EEZ_Land_v3_202030_Canada.csv EEZ_Land_v3_202030_Canada.shp
       mv EEZ_Land_v3_202030_Canada.csv EEZ_Land_v3_202030_Canada.cat
       "8493",Canada,"2169",CAN,"124",Canada,"2169",CAN,"124","15700025"
       "8438",Greenland,"2260",GRL,"304",Denmark,"2157",DNK,"208","4437543"
       "8463",Alaska,"8684",,,United States,"2204",USA,"840","5193061"
       "48943",Canada,"2169",CAN,"124",Canada,"2169",CAN,"124","24772"
       ogr2ogr -f CSV -dialect sqlite -sql "select AsGeoJSON(geometry) AS geom, * from EEZ_Land_v3_202030_Canada where (MRGID_EEZ = 8493)" EEZ_Land_v3_202030_contiguous_Canada.csv    EEZ_Land_v3_202030_Canada.shp
       ogr2ogr -f CSV -dialect sqlite -sql "select AsGeoJSON(geometry) AS geom, * from EEZ_Land_v3_202030_Canada where (MRGID_EEZ = 8438)" EEZ_Land_v3_202030_contiguous_Greenland.csv EEZ_Land_v3_202030_Canada.shp
       ogr2ogr -f CSV -dialect sqlite -sql "select AsGeoJSON(geometry) AS geom, * from EEZ_Land_v3_202030_Canada where (MRGID_EEZ = 8463)" EEZ_Land_v3_202030_contiguous_Alaska.csv    EEZ_Land_v3_202030_Canada.shp
       vi *contiguous* (to remove heading and trailing parts of biggest single polygons, then apply ":1,1s/,/ /g", ":1,1s/\[//g", and ":1,1s/\]/,/g")

# download dams and reservoirs (dams-rev01-global-shp.zip and reservoirs-rev01-global-shp.zip
# from sedac.ciesin.columbia.edu/data/set/grand-v1-dams-rev01/data-download after login)
wrkg ; cd HYDROSHEDS/dams
       unzip dams-rev01-global-shp.zip       ; ogrinfo -al -so GRanD_dams_v1_1.shp       > GRanD_dams_v1_1.txt
       unzip reservoirs-rev01-global-shp.zip ; ogrinfo -al -so GRanD_reservoirs_v1_1.shp > GRanD_reservoirs_v1_1.txt

# download (and trim) the HydroSHEDS lake shapefiles from https://www.hydrosheds.org/pages/hydrolakes
# (on Compute Canada) module load gcc/7.3.0 r/3.6.0 gdal/3.0.1 udunits/2.2.26 netcdf/4.6.1
wrkg ; cd hydrolakes
       wget https://97dc600d3ccc765f840c-d5a4231de41cd7a15e06ac00b0bcc552.ssl.cf5.rackcdn.com/HydroLAKES_polys_v10_shp.zip
       unzip HydroLAKES_polys_v10_shp.zip ; mv HydroLAKES_polys_v10_shp/* . ; rmdir HydroLAKES_polys_v10_shp
       ogrinfo -so                                                                           HydroLAKES_polys_v10.shp                 HydroLAKES_polys_v10
       ogr2ogr -spat -150.0 40.0 -50.0 75.0                                                  HydroLAKES_polys_v10_Canada.shp          HydroLAKES_polys_v10.shp
#      ogr2ogr -sql "SELECT * FROM HydroLAKES_polys_v10_Canada WHERE (Lake_area > 500)"      HydroLAKES_polys_v10_Canada_500km2.shp   HydroLAKES_polys_v10_Canada.shp
#      ogrinfo -so                                                                           HydroLAKES_polys_v10_Canada_500km2.shp   HydroLAKES_polys_v10_Canada_500km2
       ogr2ogr -sql "SELECT * FROM HydroLAKES_polys_v10_Canada WHERE (Lake_area > 999)"      HydroLAKES_polys_v10_Canada_999km2.shp   HydroLAKES_polys_v10_Canada.shp
       ogrinfo -so                                                                           HydroLAKES_polys_v10_Canada_999km2.shp   HydroLAKES_polys_v10_Canada_999km2
       ogr2ogr -spat  -93.0 40.0 -50.0 63.0                                                  HydroLAKES_polys_v10_East_allkm2.shp     HydroLAKES_polys_v10.shp
       ogr2ogr -sql "SELECT * FROM HydroLAKES_polys_v10_East_allkm2 WHERE (Lake_area > 999)" HydroLAKES_polys_v10_East_999km2.shp     HydroLAKES_polys_v10_East_allkm2.shp

# (optional) download a HydroSHEDS supplement on connectivity/fragentation/regulation (for about 74 named rivers in East domain)
# from https://figshare.com/articles/dataset/Mapping_the_world_s_free-flowing_rivers_data_set_and_technical_documentation/7688801
wrkg ; cd HYDROSHEDS/connect
       wget https://figshare.com/ndownloader/articles/7688801/versions/1
       mv 1 1.zip ; unzip 1.zip
       unzip 'Mapping the worlds free-flowing rivers_Data_Geodatabase.zip'
       mv    'Mapping the worlds free-flowing rivers_Data_Geodatabase' 1
       mv  1/'Mapping the worlds free-flowing rivers - Technical documentation - v1_0.pdf'  Grill_etal_2019_mapping_the_worlds_free_flowing_rivers_techdoc.pdf
       ogr2ogr                                                                              Grill_etal_2019_mapping_the_worlds_free_flowing_rivers_Canada.gpkg 1/FFR_river_network.gdb -where "COUNTRY='Canada'"
       ogr2ogr     Grill_etal_2019_mapping_the_worlds_free_flowing_rivers_Canada.shp        Grill_etal_2019_mapping_the_worlds_free_flowing_rivers_Canada.gpkg
       ogr2ogr     Grill_etal_2019_mapping_the_worlds_free_flowing_rivers_Canada_regul.gpkg Grill_etal_2019_mapping_the_worlds_free_flowing_rivers_Canada.gpkg -where "DOR > 0"
       ogr2ogr     Grill_etal_2019_mapping_the_worlds_free_flowing_rivers_Canada_regul.shp  Grill_etal_2019_mapping_the_worlds_free_flowing_rivers_Canada_regul.gpkg
       ogr2ogr     Grill_etal_2019_mapping_the_worlds_free_flowing_rivers_Canada_named.gpkg Grill_etal_2019_mapping_the_worlds_free_flowing_rivers_Canada.gpkg -where "BB_NAME!=''"
       ogr2ogr     Grill_etal_2019_mapping_the_worlds_free_flowing_rivers_Canada_named.shp  Grill_etal_2019_mapping_the_worlds_free_flowing_rivers_Canada_named.gpkg
       ogrinfo -so Grill_etal_2019_mapping_the_worlds_free_flowing_rivers_Canada_named.gpkg FFR_river_network_v1 > Grill_etal_2019_mapping_the_worlds_free_flowing_rivers_Canada_named.txt
       rm -r 1 1.zip Mapping*

# (optional) download lake bathymetry files and unpack them, following
# https://springernature.figshare.com/collections/GLOBathy_the_Global_Lakes_Bathymetry_Dataset/5243309
wrkg ; cd GLOBathy
       wget https://springernature.figshare.com/ndownloader/files/28919850 ; mv 28919850 GLOBathy_rasters.zip          ; unzip                 GLOBathy_rasters.zip
       wget https://springernature.figshare.com/ndownloader/files/28917783 ; mv 28917783 GLOBathy_hAV_relationships.nc ; mv Bathymetry_Rasters GLOBathy_rasters
       wget https://springernature.figshare.com/ndownloader/files/28919991 ; mv 28919991 GLOBathy_basic_parameters.zip ; unzip GLOBathy_basic_parameters.zip
       wget https://springernature.figshare.com/ndownloader/files/28917078 ; mv 28917078 GLOBathy_python_scripts.zip   ; unzip GLOBathy_python_scripts.zip

# download an upscaled version at 30-arcsec resolution of the MERIT hydrologically conditioned DEM
# (from https://doi.org/10.5281/zenodo.5166932) as given by Eilander et al. (2021), and as an initial
# adjustment, burn in the 15-s HydroSHEDS watershed boundaries by raising the DEM values by O[100m]
wrkg ; cd merit
#      wget https://zenodo.org/record/5166932/files/30sec_basids.tif ; gdal_translate 30sec_basids.tif 30sec_basids.tif.nc
       wget https://zenodo.org/record/5166932/files/30sec_elevtn.tif ; gdal_translate 30sec_elevtn.tif 30sec_elevtn.tif.nc
#      wget https://zenodo.org/record/5166932/files/30sec_flwdir.tif ; gdal_translate 30sec_flwdir.tif 30sec_flwdir.tif.nc
#      wget https://zenodo.org/record/5166932/files/30sec_flwerr.tif ; gdal_translate 30sec_flwerr.tif 30sec_flwerr.tif.nc
#      wget https://zenodo.org/record/5166932/files/30sec_strord.tif ; gdal_translate 30sec_strord.tif 30sec_strord.tif.nc
#      wget https://zenodo.org/record/5166932/files/30sec_uparea.tif ; gdal_translate 30sec_uparea.tif 30sec_uparea.tif.nc
#      jjj diag.hydroshed.watershed.tiles.merit.jl 30sec_basids.tif.nc merit_hydrodem_30_85N_180_00W_30sec_basids.nc ; rm 30sec_basids.tif.nc
       jjj diag.hydroshed.watershed.tiles.merit.jl 30sec_elevtn.tif.nc merit_hydrodem_30_85N_180_00W_30sec_elevtn.nc ; rm 30sec_elevtn.tif.nc
#      jjj diag.hydroshed.watershed.tiles.merit.jl 30sec_flwdir.tif.nc merit_hydrodem_30_85N_180_00W_30sec_flwdir.nc ; rm 30sec_flwdir.tif.nc
#      jjj diag.hydroshed.watershed.tiles.merit.jl 30sec_flwerr.tif.nc merit_hydrodem_30_85N_180_00W_30sec_flwerr.nc ; rm 30sec_flwerr.tif.nc
#      jjj diag.hydroshed.watershed.tiles.merit.jl 30sec_strord.tif.nc merit_hydrodem_30_85N_180_00W_30sec_strord.nc ; rm 30sec_strord.tif.nc
#      jjj diag.hydroshed.watershed.tiles.merit.jl 30sec_uparea.tif.nc merit_hydrodem_30_85N_180_00W_30sec_uparea.nc ; rm 30sec_uparea.tif.nc

----------------------------------------------------------------------------------//

# visually design the land model grid (e.g., 110-86 land grid @50km, 2750-2150 river grid @2km, with about 2150 ocean pour points)
wrkg ; cd GRID
       wget https://esrl.noaa.gov/gsd/wrfportal/domainwizard/WRFDomainWizard.zip
       unzip WRFDomainWizard.zip ; java -Xmx675m -jar WRFDomainWizard.jar

# get a geogrid.nc file by installing WRF, WPS, and their corresponding high resolution fixed fields to run geogrid.exe and interpolate
# (cf. www2.mmm.ucar.edu/wrf/OnLineTutorial/compilation_tutorial.php and www2.mmm.ucar.edu/wrf/users/download/get_sources_wps_geog.html
#  and /cvmfs/soft.computecanada.ca/easybuild/software/2017/avx2/MPI/intel2018.3/openmpi3.1/wps/3.9.1/geog)
# (on Compute Canada) module load nixpkgs/16.09 gcc/7.3.0 mpich/3.2.1 jasper/1.900.1
# wrkg ; cd WRF/WPS ; ln -s /project/6000950/riced/workg/WRF/WPS_GEOG/modis_landuse_20class_30s_with_lakes/index land_use_categories
wrkg ; cd WRF/LIBRARIES
       wget https://www2.mmm.ucar.edu/wrf/OnLineTutorial/compile_tutorial/tar_files/netcdf-4.1.3.tar.gz   ; tar xvfz netcdf-4.1.3.tar.gz
       wget https://www2.mmm.ucar.edu/wrf/OnLineTutorial/compile_tutorial/tar_files/libpng-1.2.50.tar.gz  ; tar xvfz libpng-1.2.50.tar.gz
       wget https://www2.mmm.ucar.edu/wrf/OnLineTutorial/compile_tutorial/tar_files/zlib-1.2.7.tar.gz     ; tar xvfz zlib-1.2.7.tar.gz
#      wget https://www2.mmm.ucar.edu/wrf/OnLineTutorial/compile_tutorial/tar_files/jasper-1.900.1.tar.gz ; tar xvfz jasper-1.900.1.tar.gz
(bash) export DIR="/fs/isi-nas1/dfo/bioios/dfo_bioios/rid000/work/workg/WRF/LIBRARIES"
       export CC="gcc"       ; export CXX="g++"
       export FC="gfortran"  ; export FCFLAGS="-m64"
       export F77="gfortran" ; export FFLAGS="-m64"
       export JASPERLIB="$DIR/grib2/lib"
       export JASPERINC="$DIR/grib2/include"
       export LDFLAGS="-L$DIR/grib2/lib"
       export CPPFLAGS="-I$DIR/grib2/include"
(tcsh) setenv DIR /project/6000950/riced/workg/WRF/LIBRARIES
       setenv CC gcc       ; setenv CXX g++
       setenv FC gfortran  ; setenv FCFLAGS -m64
       setenv F77 gfortran ; setenv FFLAGS -m64
       setenv JASPERLIB /cvmfs/soft.computecanada.ca/easybuild/software/2017/Core/jasper/1.900.1/lib
       setenv JASPERINC /cvmfs/soft.computecanada.ca/easybuild/software/2017/Core/jasper/1.900.1/include
       setenv LDFLAGS  -L$DIR/grib2/lib
       setenv CPPFLAGS -I$DIR/grib2/include
       cd netcdf-4.1.3      ; ./configure --prefix=$DIR/netcdf --disable-dap --disable-netcdf-4 --disable-shared ; make ; make install
       cd ../zlib-1.2.7     ; ./configure --prefix=$DIR/grib2                                                    ; make ; make install
       cd ../libpng-1.2.50  ; ./configure --prefix=$DIR/grib2                                                    ; make ; make install
#      cd ../jasper-1.900.1 ; ./configure --prefix=$DIR/grib2                                                    ; make ; make install
       setenv PATH $DIR/netcdf/bin:$PATH
       setenv NETCDF $DIR/netcdf
wrkg ; cd WRF
       wget https://www2.mmm.ucar.edu/wrf/src/WRFV4.0.TAR.gz ; tar xvfz WRFV4.0.TAR.gz
       wget https://www2.mmm.ucar.edu/wrf/src/WPSV4.0.TAR.gz ; tar xvfz WPSV4.0.TAR.gz
wrkg ; cd WRF/WRF
       ./configure                                      (32 seems ok)
       ./compile em_real >& log.compile
wrkg ; cd WRF/WPS
       ./configure                                      (1  seems ok)
       ./compile         >& log.compile
wrkg ; cd WRF
       wget https://www2.mmm.ucar.edu/wrf/src/wps_files/geog_high_res_mandatory.tar.gz
       tar xvfz geog_high_res_mandatory.tar.gz
wrkg ; cd WRF/WPS                                       (replace geo_em.d01.nc with geo_em.d02.nc if a subdomain is of interest)
       vi namelist.wps                                  (set geog_data_path = '/project/6000950/riced/work/workg/WRF/WPS_GEOG')
       ./geogrid.exe
       cp geo_em.d01.nc ../../DOMAIN_full_full/geogrid.nc
       cp geogrid.log   ../../DOMAIN_full_full
       vi namelist.wps                                  (multiply i/j_parent_start, e_we, e_sn by (x-1)*25+1 and divide dx,dy by 25)
       ./geogrid.exe                                    (below, routing grid resolution is 25 times that of land surface model grid)
       cp geo_em.d01.nc ../../DOMAIN_full_full/hires_geogrid_mapfac.nc
       cp geogrid.log   ../../DOMAIN_full_full/hires_geogrid_mapfac.log

# replace lake parameters in geogrid.nc with adjacent land values
wrkg ; cd DOMAIN_full_full
       jjj diag.hydroshed.watershed.geogrid.jl geogrid.nc
       mv geogrid.nc geogrid_with_lakes.nc ; mv geogrid_laketoland.nc geogrid.nc

# (optional) use the DOMAIN_full_full directory as the source of unmodified grids, and on subsequent passes,
# create DOMAIN directories for each river mouth of interest (and by extension, the watersheds that feed them)
# "0000" refers to all river mouths/watersheds that flow into the North Atlantic; otherwise, DOMX and DOMY are
# the x,y-indices of a particular river mouth on the land model grid
(tcsh) setenv DOMX "full" ; setenv DOMY $DOMX  ; setenv DOMZ "_"$DOMX"_"$DOMY
#      setenv DOMX "0000" ; setenv DOMY $DOMX  ; setenv DOMZ "_"$DOMX"_"$DOMY
#      setenv DOMX "2900" ; setenv DOMY "0150" ; setenv DOMZ "_"$DOMX"_"$DOMY
(bash) export DOMX="full" ; export DOMY=$DOMX  ; export DOMZ="_"$DOMX"_"$DOMY
#      export DOMX="0000" ; export DOMY=$DOMX  ; export DOMZ="_"$DOMX"_"$DOMY
#      export DOMX="2900" ; export DOMY="0150" ; export DOMZ="_"$DOMX"_"$DOMY

# (optional, and only if a DOMAIN_full_full routing stack is already available) create modified routing
# input grids for the river mouth(s) of interest (to obtain a routing stack in the next step); all input
# files are taken from DOMAIN_full_full (copy lakes shapefile and mask geogrid.nc and the 30-s MERIT DEM)
#rkg ; cd DOMAIN$DOMZ
#      cp ../DOMAIN_full_full/HydroLAKES_polys_v10* .
#      jjj diag.hydroshed.watershed.islands.jl ../DOMAIN_full_full/ $DOMX $DOMY

----------------------------------------------------------------------------------//

# simplify HydroLAKES shapefiles so that these represent contiguous shapes on the WRF-Hydro routing grid
# then burn in the 15-s HydroSHEDS watershed boundaries by raising merit DEM values by O[100m], but first
# ensure that all watershed and river network shapefile segments are near the routing grid resolution
wrkg ; cd HYDROSHEDS
       ogr2ogr -sql "select * from HydroRIVERS_v10_30_85N_180_00W_outlets_1000_41N"         HydroRIVERS_v10_30_85N_180_00W_wsheds.shp HydroRIVERS_v10_30_85N_180_00W_outlets_1000_41N.shp -segmentize 0.0005
       ogr2ogr -sql "select * from HydroRIVERS_v10_30_85N_180_00W WHERE (UPLAND_SKM > 100)" HydroRIVERS_v10_30_85N_180_00W_rivers.shp HydroRIVERS_v10_30_85N_180_00W.shp                  -segmentize 0.0005
wrkg ; cd hydrolakes
       jjj diag.hydroshed.watershed.river.adjust.hydrolakes.jl HydroLAKES_polys_v10_East_999km2.shp ../../workg$WRFSDOM/DOMAIN$DOMZ/hires_geogrid_mapfac.nc HydroLAKES_polys_v10_East.shp
       ogrinfo -so                                             HydroLAKES_polys_v10_East.shp                                                                HydroLAKES_polys_v10_East
wrkg ; cd merit
       jjj diag.hydroshed.watershed.river.adjust.merit.jl                  merit_hydrodem_30_85N_180_00W_30sec_elevtn.nc ../HYDROSHEDS/HydroRIVERS_v10_30_85N_180_00W
#      gdal_translate merit_hydrodem_30_85N_180_00W_30sec_elevtn_adjust.nc merit_hydrodem_30_85N_180_00W_30sec_elevtn_adjust.bil
#      gzip                                                                merit_hydrodem_30_85N_180_00W_30sec_elevtn_adjust.bil
#      scp                                                                 merit_hydrodem_30_85N_180_00W_30sec_elevtn_adjust.[bhp]* riced@graham.computecanada.ca:/scratch/riced/bio$WRFSDOM
#      mv                                                                  merit_hydrodem_30_85N_180_00W_30sec_elevtn_adjust.[bhp]* ../DOMAIN$DOMZ/

----------------------------------------------------------------------------------//

# (faster to) iterate on a river network (routing stack) using the open source WRF-Hydro GIS Pre-processor (beta version)
# following https://github.com/NCAR/wrf_hydro_gis_preprocessor; first create a conda environment and clone the processor
# (only for ubuntu 18.04) link a version of WBT that was precompiled with an old version of GLIBC
# (only for GPSC-conda) . ssmuse-sh -x hpco/exp/mib002/anaconda2/anaconda2-5.0.1-hpcobeta2
# for calibration, interpolate the   adjusted merit DEM onto the routing grid to avoid iterating on the adjustments (and omit lakes)
# for full domain, interpolate the unadjusted merit DEM onto the routing grid and perform adjustments on that (and include lakes)
# note that DEM interpolation is governed by line 237 of Build_Routing_Stack.py (resampling=GRA_Bilinear or gdal.GRA_NearestNeighbour)
       conda config --add channels conda-forge
       conda create -n wrfh_gis_env -c conda-forge python=3.10 gdal netCDF4 numpy pyproj whitebox=2.2.0 packaging shapely
       echo ubuntu-18.04 ; cd    ~/soft/miniconda3/envs/wrfh_gis_env/lib/python3.10/site-packages/whitebox
       echo ubuntu-18.04 ; mv whitebox_tools whitebox_tools_GLIBC_2.35
       echo ubuntu-18.04 ; ln -s ~/soft/whitebox_tools_v2.2.0_ubuntu_18.04_executable whitebox_tools
wrkg ; git clone https://github.com/NCAR/wrf_hydro_gis_preprocessor.git
wrkg ; cd                                wrf_hydro_gis_preprocessor/wrfhydro_gis
       mkdir out
       ln -s ../../../DOMAIN$DOMZ/hires_geogrid_mapfac.nc     out/hires_geogrid_mapfac.nc
       cp       ../../DOMAIN$DOMZ/Fulldom_hires_full_full.ctl out
       cp             ../../merit/Fulldom_hires_outlets.ctl   out
(bash) source activate wrfh_gis_env
(tcsh) conda  activate wrfh_gis_env
       echo ### full domain with resampling=gdal.GRA_Bilinear to write prelim.zip and then gdal.GRA_NearestNeighbour to read prelim.nc ###
       vi     Build_Routing_Stack.py              (set resampling=gdal.GRA_Bilinear on line 237)
       python Build_Routing_Stack.py              -i ../../DOMAIN$DOMZ/geogrid.nc -R 25 -t 50 -d ../../merit/merit_hydrodem_30_85N_180_00W_30sec_elevtn.nc -l ../../hydrolakes/HydroLAKES_polys_v10_East.shp -o out/prelim.zip
       cd out ; unzip -o prelim.zip
       jjj diag.hydroshed.watershed.river.adjust.prelim.jl Fulldom_hires.nc ../../../HYDROSHEDS/HydroRIVERS_v10_30_85N_180_00W ../../../merit/merit_hydrodem_hires_prelim.nc
       cd ..  ; mv out out.prelim ; mkdir out
       ln -s ../../../DOMAIN$DOMZ/hires_geogrid_mapfac.nc     out/hires_geogrid_mapfac.nc
       cp       ../../DOMAIN$DOMZ/Fulldom_hires_full_full.ctl out
       cp             ../../merit/Fulldom_hires_outlets.ctl   out
       vi     Build_Routing_Stack.py              (set resampling=gdal.GRA_NearestNeighbour on line 237)
       python Build_Routing_Stack.py              -i ../../DOMAIN$DOMZ/geogrid.nc -R 25 -t 50 -d ../../merit/merit_hydrodem_hires_prelim.nc                -l ../../hydrolakes/HydroLAKES_polys_v10_East.shp -o out/final.zip
       cd out ; unzip -o  final.zip
       echo ### calibration with resampling=gdal.GRA_Bilinear ###
#      python Create_Domain_Boundary_Shapefile.py -i ../../DOMAIN$DOMZ/geogrid.nc -o out
#      python Build_GeoTiff_From_Geogrid_File.py  -i ../../DOMAIN$DOMZ/geogrid.nc -v HGT_M -o out/HGT_M.tif
       python Build_Routing_Stack.py              -i ../../DOMAIN$DOMZ/geogrid.nc -R 25 -t 50 -d ../../merit/merit_hydrodem_30_85N_180_00W_30sec_elevtn_adjust.nc       --CSV out/hydat_station_location.csv -o out/calib.zip
       cd out ; unzip -o  calib.zip
(bash) source deactivate
(tcsh) conda  deactivate

# save the CALIBRATION WRF-Hydro river network as a text file, create a CALIBRATION watershed boundary grid, and list
# terminal river flow outlets (based on Fulldom_hires.nc), where watershed grid values correspond to river flow outlet
# numbers in the lists (the subset of Canadian, Alaskan, and Greenland river outlets are in Fulldom_hires_outletz.txt)
wrkg ; cd wrf_hydro_gis_preprocessor/wrfhydro_gis/out
       jjj unpack.hydat.station.river.wrfgrid.jl Fulldom_hires.nc
       jjj diag.merit.watershed.entries.jl ./ ../../..
       grads -blc "diag.merit.watershed.entries Fulldom_hires_outlets" ; di Fulldom_hires_outlets.png
       grads -blc "diag.merit.watershed.ppoints Fulldom_hires_outlets" ; di Fulldom_hires_outlets_ppoints.png

# for CALIBRATION, locate each HyDAT station on the 15-s HydroSHEDS river network and identify its upstream catchment
wrkg ; cd hydat.calibration
                                                                      jjj unpack.hydat.station.river.hydrosheds.jl ../HYDROSHEDS/ hydat_03JB004.sta
                                                                      jjj unpack.hydat.station.catch.hydrosheds.jl ../HYDROSHEDS/ hydat_03JB004.sta
       ls -1 | grep -E '.sta$' | parallel --dry-run julia /home/riced/bin/unpack.hydat.station.river.hydrosheds.jl ../HYDROSHEDS/       > xrhy ; nohup parallel -j 6 < xrhy > xrhy.xcom &
       ls -1 | grep -E '.sta$' | parallel --dry-run julia /home/riced/bin/unpack.hydat.station.catch.hydrosheds.jl ../HYDROSHEDS/       > xshy ; nohup parallel -j 6 < xshy > xshy.xcom &
       grads -blc "diag.wrf.ocean.entry.timeseries.vetting  hydat_vetting_rhbn_2500" ; di hydat_vetting_rhbn_2500.png

# for CALIBRATION, locate each HyDAT station on the WRF-Hydro river network and identify its upstream catchment
# (iterate on unpack.hydat.station.river.wrfhydro.jl to get the desired forecast points and upstream catchments)
wrkg ; cd hydat.calibration
       ln -f -s ../wrf_hydro_gis_preprocessor/wrfhydro_gis/out/Fulldom_hires_river.txt Fulldom_hires_river.txt
                                                                      jjj unpack.hydat.station.river.wrfhydro.jl   ../wrf_hydro_gis_preprocessor/wrfhydro_gis/out/ hydat_03JB004.sta
                                                                      jjj unpack.hydat.station.catch.wrfhydro.jl   ../wrf_hydro_gis_preprocessor/wrfhydro_gis/out/ hydat_03JB004.sta
                                                    grads --quiet -blc   "diag.hydroshed.watershed.hydat hydat_03JB004.sta" ; di hydat_03JB004.sta.png
       ls -1 | grep -E '.sta$' | parallel --dry-run julia /home/riced/bin/unpack.hydat.station.river.wrfhydro.jl   ../wrf_hydro_gis_preprocessor/wrfhydro_gis/out/ > xrwr ; nohup parallel -j 6 < xrwr > xrwr.xcom &
       ls -1 | grep -E '.sta$' | parallel --dry-run julia /home/riced/bin/unpack.hydat.station.catch.wrfhydro.jl   ../wrf_hydro_gis_preprocessor/wrfhydro_gis/out/ > xswr ; nohup parallel -j 6 < xswr > xswr.xcom &
       ls -1 | grep -E '.sta$' | parallel --dry-run grads --quiet -blc '"'diag.hydroshed.watershed.hydat > xgra ; vi xgra            (:1,3s/.sta/.sta"/) (")              ; nohup parallel -j 6 < xgra > xgra.xcom &

# as another CALIBRATION adjustment, burn the "hyshed" (15-s HydroSHEDS) station drainage boundaries by raising the DEM values by O[100m]
wrkg ; cd merit
       jjj diag.hydroshed.watershed.river.adjust.rhbn.jl merit_hydrodem_30_85N_180_00W_30sec_elevtn.nc ../hydat.calibration/
#      gdal_translate merit_hydrodem_30_85N_180_00W_30sec_elevtn_rhbn.nc merit_hydrodem_30_85N_180_00W_30sec_elevtn_rhbn.bil
#      gzip                                                              merit_hydrodem_30_85N_180_00W_30sec_elevtn_rhbn.bil
#      scp                                                               merit_hydrodem_30_85N_180_00W_30sec_elevtn_rhbn.[bhp]* riced@graham.computecanada.ca:/scratch/riced/bio$WRFSDOM
#      mv                                                                merit_hydrodem_30_85N_180_00W_30sec_elevtn_rhbn.[bhp]* ../DOMAIN$DOMZ/

# then rebuild the CALIBRATION routing stack (as above) after tabulating the CSV forecast points
# and copy domain files to DOMAIN$DOMZ, and from there, to other machines
wrkg ; cd hydat.calibration
       jjj unpack.hydat.station.location.jl .
       cp hydat_station_location.csv ../DOMAIN$DOMZ/
       cp hydat_station_location.csv ../wrf_hydro_gis_preprocessor/wrfhydro_gis/out/
wrkg ; cd wrf_hydro_gis_preprocessor/wrfhydro_gis/out
       cp F* G* hydat_station_location.csv r* ../../../DOMAIN_full_full/
wrkg ; cd DOMAIN$DOMZ
       scp * riced@graham.computecanada.ca:/scratch/riced/wrf$WRFSDOM

----------------------------------------------------------------------------------//

# (slower to) iterate on a river network (routing stack) using ArcGIS on a BIO laptop; first transfer files from DOMAIN$DOMZ
# to the laptop, then transfer the resulting routing stack (WRF_Hydro_routing_grids.zip) back to DOMAIN$DOMZ and unpack
wrkg ; cd DOMAIN$DOMZ
       scp geogrid.nc hydat_station_location.csv merit_hydrodem_30_85N_180_00W_30sec_elevtn_rhbn.[bhp]* riced@graham.computecanada.ca:/scratch/riced/bio$WRFSDOM
sftp ; to laptop the files from graham
       (may need to comment out arcpy.Delete_management(order) and (order2) on lines 2946-7 of wrf_hydro_functions.py)
       Parameter: Input GEOGRID File:                            C:\Users\DANIELSONR\Desktop\bio\geogrid.nc
       Parameter: Forecast Points (CSV):                         C:\Users\DANIELSONR\Desktop\bio\hydat_station_location.csv
       Parameter: Mask CHANNELGRID variable to forecast basins?: true
       Parameter: Create reach-based routing (RouteLink) files?: false
       Parameter: Create lake parameter (LAKEPARM) file?:        true
       Parameter: Reservoirs Shapefile or Feature Class:         C:\Users\DANIELSONR\Desktop\bio\HydroLAKES_polys_v10_Atlantic_999km2.shp
       Parameter: Input Elevation Raster:                        C:\Users\DANIELSONR\Desktop\bio\merit_hydrodem_30_85N_180_00W_30sec_elevtn.rhbn.bil
       Parameter: Regridding (nest) Factor:                      25
       Parameter: Number of routing grid cells to define stream: 50
       Parameter: OVROUGHRTFAC Value:                            1
       Parameter: RETDEPRTFAC Value:                             1
       Parameter: Output ZIP File:                               C:\Users\DANIELSONR\Desktop\redp\b\WRF_Hydro_routing_grids.zip
wrkg ; cd DOMAIN$DOMZ
       scp riced@graham.computecanada.ca:/scratch/riced/bio$WRFSDOM/bio$WRFSDOM.zip     .
       scp riced@graham.computecanada.ca:/scratch/riced/bio$WRFSDOM/bio$WRFSDOM.zip.log .
       unzip -o bio$WRFSDOM.zip

# save the ArcGIS WRF-Hydro river network as a text file, create an ArcGIS watershed boundary grid, and list
# terminal river flow outlets (based on Fulldom_hires.nc), where watershed grid values correspond to river flow outlet
# numbers in the lists (the subset of Canadian, Alaskan, and Greenland river outlets are in Fulldom_hires_outletz.txt)
wrkg ; cd DOMAIN$DOMZ
       jjj unpack.hydat.station.river.wrfgrid.jl Fulldom_hires.nc
wrkg ; cd merit
       jjj diag.merit.watershed.entries.jl ../DOMAIN$DOMZ/ ..
       cp                                  ../DOMAIN$DOMZ/Fulldom_hires_full_full.ctl Fulldom_hires_outlets.ctl ; vi Fulldom_hires_outlets.ctl
       grads -blc "diag.merit.watershed.entries Fulldom_hires_outlets" ; di Fulldom_hires_outlets.png

# locate each HyDAT calibration station on the ArcGIS WRF-Hydro river network and identify its upstream catchment
# (iterate on unpack.hydat.station.river.wrfhydro.jl to get the desired forecast points and upstream catchments)
wrkg ; cd hydat.calibration
       mkdir prelim ; mv *.sta.hwshed *.sta.png prelim
       ln -f -s ../DOMAIN_full_full/Fulldom_hires_river.txt Fulldom_hires_river.txt
                                                                      jjj unpack.hydat.station.river.wrfhydro.jl ../DOMAIN_full_full/ hydat_03JB004.sta
                                                                      jjj unpack.hydat.station.catch.wrfhydro.jl ../DOMAIN_full_full/ hydat_03JB004.sta
                                                         grads --quiet -blc   "diag.hydroshed.watershed.hydat hydat_03JB004.sta" ; di hydat_03JB004.sta.png
       ls -1 | grep -E '.sta$' | parallel --dry-run julia /home/riced/bin/unpack.hydat.station.river.wrfhydro.jl ../DOMAIN_full_full/ > xrwr ; nohup parallel -j 6 < xrwr > xrwr.xcom &
       ls -1 | grep -E '.sta$' | parallel --dry-run julia /home/riced/bin/unpack.hydat.station.catch.wrfhydro.jl ../DOMAIN_full_full/ > xswr ; nohup parallel -j 6 < xswr > xswr.xcom &
       ls -1 | grep -E '.sta$' | parallel --dry-run grads -bpc '"'diag.hydroshed.watershed.hydat              (:1,3s/.sta/.sta"/) (") > xgra ; nohup parallel -j 6 < xgra > xgra.xcom &

# as another adjustment, burn the "hyshed" (15-s HydroSHEDS) calibration-station drainage boundaries by raising the DEM values by O[100m]
wrkg ; cd merit
       jjj diag.hydroshed.watershed.river.adjust.rhbn.jl merit_hydrodem_30_85N_180_00W_30sec_elevtn.nc ../hydat.calibration/
       gdal_translate merit_hydrodem_30_85N_180_00W_30sec_elevtn_rhbn.nc merit_hydrodem_30_85N_180_00W_30sec_elevtn_rhbn.bil
       gzip                                                              merit_hydrodem_30_85N_180_00W_30sec_elevtn_rhbn.bil
       scp                                                               merit_hydrodem_30_85N_180_00W_30sec_elevtn_rhbn.[bhp]* riced@graham.computecanada.ca:/scratch/riced/bio$WRFSDOM
       mv                                                                merit_hydrodem_30_85N_180_00W_30sec_elevtn_rhbn.[bhp]* ../DOMAIN$DOMZ/

# then rebuild the ArcGIS routing stack (as above) after tabulating the CSV forecast points
wrkg ; cd hydat.calibration
       jjj unpack.hydat.station.location.jl .
       cp hydat_station_location.csv ../DOMAIN$DOMZ/
       cp hydat_station_location.csv ../wrf_hydro_gis_preprocessor/wrfhydro_gis/out/

----------------------------------------------------------------------------------//

# locate the remaining HyDAT stations on the 15-s HydroSHEDS river network and identify their upstream catchments
wrkg ; cd hydat
       jjj unpack.hydat.station.river.hydrosheds.jl ../HYDROSHEDS/ hydat_03JB004.sta
       jjj unpack.hydat.station.catch.hydrosheds.jl ../HYDROSHEDS/ hydat_03JB004.sta
       grads --quiet -blc "diag.hydroshed.watershed.hydat          hydat_03JB004.sta" ; di hydat_03JB004.sta.png
       ls -1 | grep -E '.sta$' | parallel --dry-run julia /home/riced/bin/unpack.hydat.station.river.hydrosheds.jl ../HYDROSHEDS/       > xrhy ; nohup parallel -j 6 < xrhy > xrhy.xcom &
       ls -1 | grep -E '.sta$' | parallel --dry-run julia /home/riced/bin/unpack.hydat.station.catch.hydrosheds.jl ../HYDROSHEDS/       > xshy ; nohup parallel -j 6 < xshy > xshy.xcom &

# locate the remaining HyDAT stations on the WRF-Hydro river network and identify their upstream catchments
wrkg ; cd hydat
       ln -f -s ../DOMAIN_full_full/Fulldom_hires_river.txt Fulldom_hires_river.txt
       jjj unpack.hydat.station.river.wrfhydro.jl   ../DOMAIN_full_full/ hydat_03JB004.sta
       jjj unpack.hydat.station.catch.wrfhydro.jl   ../DOMAIN_full_full/ hydat_03JB004.sta
       ls -1 | grep -E '.sta$' | parallel --dry-run julia /home/riced/bin/unpack.hydat.station.river.wrfhydro.jl   ../DOMAIN_full_full/ > xrwr ; nohup parallel -j 6 < xrwr > xrwr.xcom &
       ls -1 | grep -E '.sta$' | parallel --dry-run julia /home/riced/bin/unpack.hydat.station.catch.wrfhydro.jl   ../DOMAIN_full_full/ > xswr ; nohup parallel -j 6 < xswr > xswr.xcom &
       ls -1 | grep -E '.sta$' | parallel --dry-run grads -bpc '"'diag.hydroshed.watershed.hydat                (:1,3s/.sta/.sta"/) (") > xgra ; nohup parallel -j 6 < xgra > xgra.xcom &

----------------------------------------------------------------------------------//

# get a copy of WRF-Hydro and compile at v5.2.0 (branch 1571e9f6)
# (cc) module load intel/2019.3 openmpi/4.0.1 netcdf-fortran/4.4.5
work ; cd wrf_hydro_nwm_public/trunk/NDHMS
work ; git clone https://github.com/NCAR/wrf_hydro_nwm_public
       cd wrf_hydro_nwm_public ; git fetch && git fetch --tags ; git checkout v5.2.0
       cd trunk/NDHMS ; cp template/setEnvar.sh . ; vi setEnvar.sh
(bash) export NETCDF=`nc-config --prefix` ; echo $NETCDF
(tcsh) setenv NETCDF `nc-config --prefix` ; echo $NETCDF
       ./configure 2
       vi macros ; echo -lnetcdff to final NETCDFLIB
       vi macros ; echo NETCDFINC NETCDFLIB as last two lines
       vi Land_models/NoahMP/IO_code/module_hrldas_netcdf_io.F and on line 1172 replace with:
       write(6,*) "my_id, xstart, xend, ystart, yend, name, array(xstart,ystart)", my_id, xstart, xend, ystart, yend, name, array(xstart,ystart)
       ./compile_offline_NoahMP.sh setEnvar.sh

# create uniform, cold-start model initial conditions and parameter grids for hydro/soil properties (on GPSC)
       R ; .libPaths() ; install.packages("optparse") ; packageDescription("optparse")
wrkg ; cd GRID
       wget --no-check-certificate https://ral.ucar.edu/sites/default/files/public/create_wrfinput.zip
       unzip create_wrfinput.zip ; mv create_wrfinput/* . ; rm -r create_wrfinput create_wrfinput.zip ; chmod +x create_wrfinput.R
       ln -s ../DOMAIN$DOMZ/geogrid.nc geo_em.d01.nc
       mv README.txt create_wrfinput_README.txt
                   ./create_wrfinput.R --geogrid='geo_em.d01.nc' --filltyp=3 --laimo=1
       wget --no-check-certificate https://ral.ucar.edu/sites/default/files/public/projects/wrf-hydro/pre-processing-tools/create-soilproperties.zip
       unzip create-soilproperties.zip                    ; rm -r create-soilproperties.zip           ; chmod +x create_soilproperties.R
       ln -s ../../wrf_hydro_nwm_public/trunk/NDHMS/Run/GENPARM.TBL  GENPARM.TBL
       ln -s ../../wrf_hydro_nwm_public/trunk/NDHMS/Run/HYDRO.TBL    HYDRO.TBL
       ln -s ../../wrf_hydro_nwm_public/trunk/NDHMS/Run/MPTABLE.TBL  MPTABLE.TBL
       ln -s ../../wrf_hydro_nwm_public/trunk/NDHMS/Run/SOILPARM.TBL SOILPARM.TBL
       mv README.txt create_soilproperties_README.txt     ; vi create_soilproperties.R          (change to MODIS and add first line: #!/usr/bin/env Rscript)
                   ./create_soilproperties.R
       cp wrfinput_d01.nc hydro2dtbl.nc soil_properties.nc ../DOMAIN$DOMZ/

----------------------------------------------------------------------------------//

# prepare ERA5 forcing data, with variable names and data
# taken from /home/sdfo404/storage/Hindcast_Canada/Atmospheric_data/ERA5_for_Canada (uncalibrated raw forcing)
wrkg ; cd FORCING.era
       . ssmuse-sh -x hpco/exp/mib002/anaconda2/anaconda2-5.0.1-hpcobeta2 ; source   activate ncl_stable
       jjj diag.wrf.forcing.interp.era.jl 1990-01-01-00 2004-12-31-21
       jjj diag.wrf.forcing.interp.era.jl 2005-01-01-00 2021-12-31-21
       jjj diag.wrf.forcing.interp.era.jl 2022-01-01-00 2023-12-31-21
       mkdir FORCING.era.src.links ; mv z_* ????-* FORCING.era.src.links  ; source deactivate

# prepare CCSM forcing data, with variable names and data
# taken from existing wrfout* files (uncalibrated raw forcing)
wrkg ; cd FORCING.ccs
       jjj diag.wrf.forcing.links.wrf.jl /gpfs/fs7/dfo/hpcmc/comda/fs2_comda/sdfo404/DRCDS-E/ATM/CCSM4/hist/surface ; rm wrfpost.surface.y19[678]*
       jjj diag.wrf.forcing.links.wrf.jl /gpfs/fs7/dfo/hpcmc/comda/fs2_comda/sdfo404/DRCDS-E/ATM/CCSM4/rcp85/surface
       . ssmuse-sh -x hpco/exp/mib002/anaconda2/anaconda2-5.0.1-hpcobeta2 ; source   activate ncl_stable
       jjj diag.wrf.forcing.interp.wrf.jl 1990-01-01 2099-12-31
       mkdir FORCING.ccs.src.links ; mv [a-z]* FORCING.ccs.src.links      ; source deactivate

# prepare MPI forcing data, with variable names and data
# taken from existing wrfout* files (uncalibrated raw forcing)
wrkg ; cd FORCING.wrf
       jjj diag.wrf.forcing.links.wrf.jl /gpfs/fs7/dfo/hpcmc/comda/fs2_comda/sdfo404/DRCDS-E/ATM/MPI-ESM1-2-LR/hist_MKF/surface
       . ssmuse-sh -x hpco/exp/mib002/anaconda2/anaconda2-5.0.1-hpcobeta2 ; source   activate ncl_stable
       jjj diag.wrf.forcing.interp.wrf.jl 1990-01-01 2004-12-31
       mkdir FORCING.wrf.src.links ; mv [a-z]* FORCING.wrf.src.links      ; source deactivate

wrkg ; cd FORCING.w85
       jjj diag.wrf.forcing.links.wrf.jl /gpfs/fs7/dfo/hpcmc/comda/fs2_comda/sdfo404/DRCDS-E/ATM/MPI-ESM1-2-LR/hist_ssp585_MKF/surface
       ln -s /gpfs/fs7/dfo/hpcmc/comda/fs2_comda/sdfo404/DRCDS-E/ATM/MPI-ESM1-2-LR/hist_ssp585_MKF/surface/wrfpost.surface.y1974m12d25.nc wrfpost.surface.y1974m12d26.nc
       ln -s /gpfs/fs7/dfo/hpcmc/comda/fs2_comda/sdfo404/DRCDS-E/ATM/MPI-ESM1-2-LR/hist_ssp585_MKF/surface/wrfpost.surface.y1974m12d25.nc wrfpost.surface.y1974m12d27.nc
       ln -s /gpfs/fs7/dfo/hpcmc/comda/fs2_comda/sdfo404/DRCDS-E/ATM/MPI-ESM1-2-LR/hist_ssp585_MKF/surface/wrfpost.surface.y1974m12d25.nc wrfpost.surface.y1974m12d28.nc
       ln -s /gpfs/fs7/dfo/hpcmc/comda/fs2_comda/sdfo404/DRCDS-E/ATM/MPI-ESM1-2-LR/hist_ssp585_MKF/surface/wrfpost.surface.y1975m01d01.nc wrfpost.surface.y1974m12d29.nc
       ln -s /gpfs/fs7/dfo/hpcmc/comda/fs2_comda/sdfo404/DRCDS-E/ATM/MPI-ESM1-2-LR/hist_ssp585_MKF/surface/wrfpost.surface.y1975m01d01.nc wrfpost.surface.y1974m12d30.nc
       ln -s /gpfs/fs7/dfo/hpcmc/comda/fs2_comda/sdfo404/DRCDS-E/ATM/MPI-ESM1-2-LR/hist_ssp585_MKF/surface/wrfpost.surface.y1975m01d01.nc wrfpost.surface.y1974m12d31.nc
       . ssmuse-sh -x hpco/exp/mib002/anaconda2/anaconda2-5.0.1-hpcobeta2 ; source   activate ncl_stable
       jjj diag.wrf.forcing.interp.wrf.jl 1970-01-01 2099-12-31
       mkdir FORCING.w85.src.links ; mv [a-z]* FORCING.w85.src.links      ; source deactivate

# prepare HadGEM-WRF forcing data, with variable names and
# historical data from /gpfs/fs7/dfo/hpcmc/comda/fs2_comda/sdfo404/DRCDS-E/ATM/HadGEM2/HadGEM2_Hist/surface (1969-2004)
# and future data from /gpfs/fs7/dfo/hpcmc/comda/fs2_comda/sdfo404/DRCDS-E/ATM/HadGEM2/HadGEM2_RCP[48].5/surface (2005-2099)
wrkg ; cd FORCING.h45
       jjj diag.wrf.forcing.links.wrf.jl /gpfs/fs7/dfo/hpcmc/comda/fs2_comda/sdfo404/DRCDS-E/ATM/HadGEM2/HadGEM2_Hist/surface ; rm wrfpost.surface.y19[678]*
       jjj diag.wrf.forcing.links.wrf.jl /gpfs/fs7/dfo/hpcmc/comda/fs2_comda/sdfo404/DRCDS-E/ATM/HadGEM2/HadGEM2_RCP4.5/surface
       . ssmuse-sh -x hpco/exp/mib002/anaconda2/anaconda2-5.0.1-hpcobeta2 ; source   activate ncl_stable
       jjj diag.wrf.forcing.interp.wrf.jl        1990-01-01 2099-12-31
#      jjj diag.wrf.forcing.interp.wrf.kelvin.jl 1990-01-01 2099-12-31
       mkdir FORCING.h45.src.links ; mv [a-z]* FORCING.h45.src.links      ; source deactivate
       rm 2099123100.LDASIN_DOMAIN1
       ln -s 2099123000.LDASIN_DOMAIN1 2099123100.LDASIN_DOMAIN1
       ln -s 2099123006.LDASIN_DOMAIN1 2099123106.LDASIN_DOMAIN1
       ln -s 2099123012.LDASIN_DOMAIN1 2099123112.LDASIN_DOMAIN1
       ln -s 2099123018.LDASIN_DOMAIN1 2099123118.LDASIN_DOMAIN1

wrkg ; cd FORCING.h85
       jjj diag.wrf.forcing.links.wrf.jl /gpfs/fs7/dfo/hpcmc/comda/fs2_comda/sdfo404/DRCDS-E/ATM/HadGEM2/HadGEM2_Hist/surface ; rm wrfpost.surface.y19[678]*
       jjj diag.wrf.forcing.links.wrf.jl /gpfs/fs7/dfo/hpcmc/comda/fs2_comda/sdfo404/DRCDS-E/ATM/HadGEM2/HadGEM2_RCP8.5/surface
       . ssmuse-sh -x hpco/exp/mib002/anaconda2/anaconda2-5.0.1-hpcobeta2 ; source   activate ncl_stable
       jjj diag.wrf.forcing.interp.wrf.jl        1990-01-01 2099-12-31
#      jjj diag.wrf.forcing.interp.wrf.kelvin.jl 1990-01-01 2099-12-31
       mkdir FORCING.h85.src.links ; mv [a-z]* FORCING.h85.src.links      ; source deactivate
       rm 2099123100.LDASIN_DOMAIN1
       ln -s 2099123000.LDASIN_DOMAIN1 2099123100.LDASIN_DOMAIN1
       ln -s 2099123006.LDASIN_DOMAIN1 2099123106.LDASIN_DOMAIN1
       ln -s 2099123012.LDASIN_DOMAIN1 2099123112.LDASIN_DOMAIN1
       ln -s 2099123018.LDASIN_DOMAIN1 2099123118.LDASIN_DOMAIN1

----------------------------------------------------------------------------------//

# get areal and monthly-mean as well as annual averages of CCSM/ERA5/MPI/HadGEM forcing data using
# a historical 15-year segment, and apply a daily calibration of, say, wind speed to WRF forcing
# (update diag.wrf.forcing.interp.wrf.jl above or use diag.wrf.forcing.calibrate.byday.apply.jl, and if needed,
#  rsync -avz rid000@inter-dfo-ubuntu2004.science.gc.ca:/gpfs/fs7/dfo/bioios/rid000/work/workg_East/FORCING/ ./)
wrkg ; cd FORCING.ccs ; parallel --dry-run julia ~/bin/diag.wrf.forcing.interp.avg.byshd.jl ccs 1990 2099 ::: R2D S2D T2D Q2D U2D V2D PSFC RAINRATE SWDOWN LWDOWN  > xcom
wrkg ; cd FORCING.era ; parallel --dry-run julia ~/bin/diag.wrf.forcing.interp.avg.byshd.jl era 1990 2004 ::: R2D S2D T2D Q2D U2D V2D PSFC RAINRATE SWDOWN LWDOWN  > xcom
wrkg ; cd FORCING.h45 ; parallel --dry-run julia ~/bin/diag.wrf.forcing.interp.avg.byshd.jl h45 1990 2099 ::: R2D S2D T2D Q2D U2D V2D PSFC RAINRATE SWDOWN LWDOWN  > xcom
wrkg ; cd FORCING.h85 ; parallel --dry-run julia ~/bin/diag.wrf.forcing.interp.avg.byshd.jl h85 1990 2099 ::: R2D S2D T2D Q2D U2D V2D PSFC RAINRATE SWDOWN LWDOWN  > xcom
wrkg ; cd FORCING.w85 ; parallel --dry-run julia ~/bin/diag.wrf.forcing.interp.avg.byshd.jl w85 1990 2099 ::: R2D S2D T2D Q2D U2D V2D PSFC RAINRATE SWDOWN LWDOWN  > xcom
wrkg ; cd FORCING.wrf ; parallel --dry-run julia ~/bin/diag.wrf.forcing.interp.avg.byshd.jl wrf 1990 2004 ::: R2D S2D T2D Q2D U2D V2D PSFC RAINRATE SWDOWN LWDOWN  > xcom
       nohup parallel -j 10 < xcom > xcom.xcom &
wrkg ; mv FORCING.*/forc* FORCING/
wrkg ; cd                 FORCING
       mkdir limbo ; mv forctxt* limbo
       jjj diag.wrf.forcing.interp.avg.byday.jl ccs       1990 2099
       jjj diag.wrf.forcing.interp.avg.byday.jl era       1990 2004
       jjj diag.wrf.forcing.interp.avg.byday.jl h45       1990 2099
       jjj diag.wrf.forcing.interp.avg.byday.jl h85       1990 2099
       jjj diag.wrf.forcing.interp.avg.byday.jl w85       1990 2099
       jjj diag.wrf.forcing.interp.avg.byday.jl wrf       1990 2004
#      grads -bpc "diag.wrf.forcing.interp.avg.byday      nar_1990_2004 wrf_1990_2004 nar_2005_2020 wrf_RCP45_2005_2099 wrf_RCP85_2005_2099"
#      grads -bpc "diag.wrf.forcing.interp.avg.00day      nar_1990_2004 wrf_1990_2004 nar_2005_2020 wrf_RCP45_2005_2099 wrf_RCP85_2005_2099"
       grads -bpc "diag.wrf.forcing.interp.avg.byday.hist era_1990_2004 ccs_1990_2099 ccs_1990_2099"
       grads -bpc "diag.wrf.forcing.interp.avg.byday.hist era_1990_2004 h45_1990_2099 h85_1990_2099"
       grads -bpc "diag.wrf.forcing.interp.avg.byday.hist era_1990_2004 wrf_1990_2004 w85_1990_2099"
       jjj         diag.wrf.forcing.interp.avg.multi.jl   ccs 1990 2004 2099
       jjj         diag.wrf.forcing.interp.avg.multi.jl   era 1990 2004 2004
       jjj         diag.wrf.forcing.interp.avg.multi.jl   h45 1990 2004 2099
       jjj         diag.wrf.forcing.interp.avg.multi.jl   h85 1990 2004 2099
       jjj         diag.wrf.forcing.interp.avg.multi.jl   wrf 1990 2004 2004
       grads -bpc "diag.wrf.forcing.interp.avg.multi      era_1990_2004 ccs_1990_2004 annual"
       grads -bpc "diag.wrf.forcing.interp.avg.multi      era_1990_2004 ccs_1990_2004 monthly"
       grads -bpc "diag.wrf.forcing.interp.avg.multi      era_1990_2004 h45_1990_2004 annual"
       grads -bpc "diag.wrf.forcing.interp.avg.multi      era_1990_2004 h45_1990_2004 monthly"
       grads -bpc "diag.wrf.forcing.interp.avg.multi      era_1990_2004 h85_1990_2004 annual"
       grads -bpc "diag.wrf.forcing.interp.avg.multi      era_1990_2004 h85_1990_2004 monthly"
       grads -bpc "diag.wrf.forcing.interp.avg.multi      era_1990_2004 wrf_1990_2004 annual"
       grads -bpc "diag.wrf.forcing.interp.avg.multi      era_1990_2004 wrf_1990_2004 monthly"
       jjj         diag.wrf.forcing.calibrate.byvar.jl    era ccs _1990_2004.annual.nc S2D T2D Q2D PSFC RAINRATE
       jjj         diag.wrf.forcing.calibrate.byvar.jl    era h45 _1990_2004.annual.nc S2D T2D          RAINRATE
       jjj         diag.wrf.forcing.calibrate.byvar.jl    era h85 _1990_2004.annual.nc S2D T2D          RAINRATE
       jjj         diag.wrf.forcing.calibrate.byvar.jl    era wrf _1990_2004.annual.nc S2D
wrkg ; mkdir FORCING.ccs.uncalibrated ; rsync -avz FORCING.ccs/ FORCING.ccs.uncalibrated/
       mkdir FORCING.h45.uncalibrated ; rsync -avz FORCING.h45/ FORCING.h45.uncalibrated/
       mkdir FORCING.h85.uncalibrated ; rsync -avz FORCING.h85/ FORCING.h85.uncalibrated/
       mkdir FORCING.w85.uncalibrated ; rsync -avz FORCING.w85/ FORCING.w85.uncalibrated/
       mkdir FORCING.wrf.uncalibrated ; rsync -avz FORCING.wrf/ FORCING.wrf.uncalibrated/
wrkg ; cd FORCING.ccs ; jjj diag.wrf.forcing.calibrate.byday.apply.jl ../FORCING/forcing_era_ccs_1990_2004.annual_cali.txt
wrkg ; cd FORCING.h45 ; jjj diag.wrf.forcing.calibrate.byday.apply.jl ../FORCING/forcing_era_h45_1990_2004.annual_cali.txt
wrkg ; cd FORCING.h85 ; jjj diag.wrf.forcing.calibrate.byday.apply.jl ../FORCING/forcing_era_h85_1990_2004.annual_cali.txt
wrkg ; cd FORCING.w85 ; jjj diag.wrf.forcing.calibrate.byday.apply.jl ../FORCING/forcing_era_wrf_1990_2004.annual_cali.txt
wrkg ; cd FORCING.wrf ; jjj diag.wrf.forcing.calibrate.byday.apply.jl ../FORCING/forcing_era_wrf_1990_2004.annual_cali.txt

----------------------------------------------------------------------------------//

# prepare ERA5/CCSM/HadGEM/MPI 15-year mean forcing data (repeating over 1901-1903)
wrkg ; cd FORCING.era.mean
       jjj diag.wrf.forcing.mean.jl ../FORCING.era/ 1990 3
       jjj diag.wrf.forcing.mean.links.jl                3

wrkg ; cd FORCING.ccs.mean
       jjj diag.wrf.forcing.mean.jl ../FORCING.ccs/ 1990 6
       jjj diag.wrf.forcing.mean.links.jl                6

wrkg ; cd FORCING.had.mean
       jjj diag.wrf.forcing.mean.jl ../FORCING.h45/ 1990 6
       jjj diag.wrf.forcing.mean.links.jl                6

wrkg ; cd FORCING.wrf.mean
       jjj diag.wrf.forcing.mean.jl ../FORCING.wrf/ 1990 6
       jjj diag.wrf.forcing.mean.links.jl                6

----------------------------------------------------------------------------------//

# point the model to the calibration domain (without lakes) or the full domain (with lakes)
# and if a PEST calibration is available below, then (optionally) use PEST parameters, as in
# wrkg ; cd CALUN.era ; cat pest_nwm_DOMAIN_East.FORCING.era_2019-03-31_000214.remsvd.txt
# and transfer to ~/work/wrf_hydro_nwm_public/trunk/NDHMS/Run/OTHER.TBL
wrkg ; rm DOMAIN_full_full SPINUP.era ; ln -s DOMAIN_full_full.calibration DOMAIN_full_full ; ln -s SPINUP.era.calibration SPINUP.era
wrkg ; rm DOMAIN_full_full SPINUP.era ; ln -s DOMAIN_full_full.lake_incl   DOMAIN_full_full ; ln -s SPINUP.era.lake_incl   SPINUP.era

# perform a three-year spinup using the 15-year mean ERA5 forcing, link dat and res to all restart subdirs
# but retain only the last year of restart files and delete the output data files
wrkg ; cd SPINUP.era
       jjj exec.wrf.hydro.spinup.jl      DOMAIN_full_full FORCING.era.mean 1901-01-01 1904-01-01
       mkdir res src             src/run_DOMAIN_full_full_FORCING.era.mean_1901-01-01_1904-01-01
wrkg ; cd                 SPINUP.era/run_DOMAIN_full_full_FORCING.era.mean_1901-01-01_1904-01-01
       mv HYDRO_RST.1903*    RESTART.1903*    ../res/
       rm HYDRO_RST.190[12]* RESTART.190[12]*
       mv [A-z]*              ../src/run_DOMAIN_full_full_FORCING.era.mean_1901-01-01_1904-01-01
       cd .. ;                 rmdir run_DOMAIN_full_full_FORCING.era.mean_1901-01-01_1904-01-01
#      cd .. ;                    mv run_DOMAIN_full_full_FORCING.era.mean_1901-01-01_001095 dat
#      jjj exec.wrf.hydro.spinup.jl      DOMAIN_full_full FORCING.era.mean 1903-01-01    365 run_DOMAIN_full_full_FORCING.era.mean_1901-01-01_001095
#      jjj diag.wrf.datres.mean.links.jl 12 run_DOMAIN_full_full_FORCING.era.mean_1901-01-01_001095
#      rsync -avz SPINUP.era riced@graham.computecanada.ca:/scratch/riced/workg
#      rsync -avz            riced@graham.computecanada.ca:/scratch/riced/workg/SPINUP.era .

# perform a three-year spinup using the 15-year mean CCSM forcing, link dat and res to all restart subdirs
# but retain only the last year of restart files and delete the output data files
wrkg ; cd SPINUP.ccs
       jjj exec.wrf.hydro.spinup.jl      DOMAIN_full_full FORCING.ccs.mean 1901-01-01 1904-01-01
       mkdir res src             src/run_DOMAIN_full_full_FORCING.ccs.mean_1901-01-01_1904-01-01
wrkg ; cd                 SPINUP.ccs/run_DOMAIN_full_full_FORCING.ccs.mean_1901-01-01_1904-01-01
       mv HYDRO_RST.1903*    RESTART.1903*    ../res/
       rm HYDRO_RST.190[12]* RESTART.190[12]*
       mv [A-z]*              ../src/run_DOMAIN_full_full_FORCING.ccs.mean_1901-01-01_1904-01-01
       cd .. ;                 rmdir run_DOMAIN_full_full_FORCING.ccs.mean_1901-01-01_1904-01-01

# perform a three-year spinup using the 15-year mean WRF forcing, link dat and res to all restart subdirs
# but retain only the last year of restart files and delete the output data files; debug crashes using
# tail diag* | grep -v "DATE=1901" | grep -v "yw check" | grep -v diag_hydro.00 | uniq
wrkg ; cd SPINUP.wrf
       jjj exec.wrf.hydro.spinup.jl      DOMAIN_full_full FORCING.wrf.mean 1901-01-01 1904-01-01
       mkdir res src             src/run_DOMAIN_full_full_FORCING.wrf.mean_1901-01-01_1904-01-01
wrkg ; cd                 SPINUP.wrf/run_DOMAIN_full_full_FORCING.wrf.mean_1901-01-01_1904-01-01
       mv HYDRO_RST.1903*    RESTART.1903*    ../res/
       rm HYDRO_RST.190[12]* RESTART.190[12]*
       mv [A-z]*              ../src/run_DOMAIN_full_full_FORCING.wrf.mean_1901-01-01_1904-01-01
       cd .. ;                 rmdir run_DOMAIN_full_full_FORCING.wrf.mean_1901-01-01_1904-01-01
#      jjj exec.wrf.hydro.spinup.jl DOMAIN_0000_0000 FORCING.wrf.mean 1903-01-01  365 run_DOMAIN_0000_0000_FORCING.wrf.mean_1901-01-01_001095
#      jjj diag.wrf.datres.mean.links.jl 12 run_DOMAIN_0000_0000_FORCING.wrf.mean_1903-01-01_000334.res run_DOMAIN_0000_0000_FORCING.wrf.mean_1903-11-30_000032.res
#      jjj diag.wrf.ocean.entry.timeseries.total.jl streamflow 1901010200 00.CHRTOUT_GRID1 1094 ../Fulldom_hires_outlets.txt
#      mv streamflow.190101020000.CHRTOUT_GRID1.1094.nc ../streamflow.190101020000.CHRTOUT_GRID1.1094.wrfraw.nc
#      rsync -avz SPINUP.wrf riced@graham.computecanada.ca:/scratch/riced/workg
#      rsync -avz            riced@graham.computecanada.ca:/scratch/riced/workg/SPINUP.wrf .

# perform a three-year spinup using the 15-year mean HadGEM forcing, link dat and res to all restart subdirs
# but retain only the last year of restart files and delete the output data files
wrkg ; cd SPINUP.had
       jjj exec.wrf.hydro.spinup.jl      DOMAIN_full_full FORCING.had.mean 1901-01-01 1904-01-01
       mkdir res src             src/run_DOMAIN_full_full_FORCING.had.mean_1901-01-01_1904-01-01
wrkg ; cd                 SPINUP.had/run_DOMAIN_full_full_FORCING.had.mean_1901-01-01_1904-01-01
       mv HYDRO_RST.1903*    RESTART.1903*    ../res/
       rm HYDRO_RST.190[12]* RESTART.190[12]*
       mv [A-z]*              ../src/run_DOMAIN_full_full_FORCING.had.mean_1901-01-01_1904-01-01
       cd .. ;                 rmdir run_DOMAIN_full_full_FORCING.had.mean_1901-01-01_1904-01-01

----------------------------------------------------------------------------------//

# download and install PEST (ANL parallel SLURM version, following https://doi.org/10.5194/gmd-12-3523-2019)
# we also add PEST to PATH (export PATH=${PATH}:~/soft/pest/PEST.bin), incorporate the pslave.F edits in
# PESTCode/ReadMe, and update subdir file permissions and two make files (i.e., to ensure compilation of
# pest and calmaintain in pest.mak and specify INSTALLDIR=../PEST.bin in makefile)
sof  ; cd pest
       wget https://zenodo.org/record/3247116/files/PESTCode.zip
       wget https://zenodo.org/record/3247116/files/PESTFiles.zip
       wget https://zenodo.org/record/3247116/files/ScriptsForCori.zip
       wget https://zenodo.org/record/3247116/files/ScriptsForTheta.zip
       wget https://s3.amazonaws.com/docs.pesthomepage.org/documents/all_manuals.zip
       mv all_manuals.zip PESTmanuals.zip ; mkdir PEST.bin PESTmanuals
       unzip PESTCode.zip ; unzip PESTFiles.zip ; unzip PESTmanuals.zip
       unzip ScriptsForCori.zip ; unzip ScriptsForTheta.zip
       mv *pdf PESTmanuals
       cd PESTCode ; tar xvf pest13.tar
       chmod 755 *tex itptest vtp vtp/test
       chmod 644 *.F
       cp ../pest.mak.useinstead ./pest.mak
       cp ../makefile.useinstead ./makefile
       cp ../pslave.F.useinstead ./pslave.F
                    make    cppp
       make clean ; make -f pest.mak all
       make clean ; make -f ppest.mak all
       make clean ; make -f pestutl1.mak all
       make clean ; make -f pestutl2.mak all
       make clean ; make -f pestutl3.mak all
       make clean ; make -f pestutl4.mak all
       make clean ; make -f pestutl5.mak all
       make clean ; make -f pestutl6.mak all
       make clean ; make -f sensan.mak all
       make clean ; make -f beopest.mak all
       make clean ; make install

# create a unified PEST template file OTHER.TBL and copy this to the WRF-Hydro source (or create PEST template
# files from the WRF-Hydro source), then create wrftest.pst and wrftest.rmf following ~/soft/pest/PESTFiles
work ; cd wrf_hydro_nwm_public_pest/nwm
       vi OTHER.TBL ; cp OTHER.TBL ../../wrf_hydro_nwm_public/trunk/NDHMS/Run/
       cp ../wrf_hydro_nwm_public/trunk/NDHMS/Run/*TBL .
       cp CHANPARM.TBL CHANPARM.TPL
       cp GENPARM.TBL  GENPARM.TPL
       cp HYDRO.TBL    HYDRO.TPL
       cp OTHER.TBL    OTHER.TPL   ; vi *TPL wrftest.rmf wrftest.pst$WRFSDOM

----------------------------------------------------------------------------------//

# point the model to the calibration domain (without lakes)
wrkg ; rm DOMAIN_full_full SPINUP.era
       ln -s DOMAIN_full_full.calibration DOMAIN_full_full
       ln -s       SPINUP.era.calibration SPINUP.era

# perform a water-year spinup to obtain restart files (use default parameters and ERA5 forcing)
wrkg ; cd SPINUP.era
       jjj exec.wrf.hydro.spinup.jl DOMAIN_full_full FORCING.era 2018-10-01    365
       cd                       run_DOMAIN_full_full_FORCING.era_2018-10-01_000365 ; mkdir src ; mv *TBL DOMAIN FORCING [a-z]* src

# (optional) check sensitivity to start date and verify that parameter changes have an impact
wrkg ; cd CALUN.era
       jjj exec.wrf.hydro.calun.jl  DOMAIN_full_full FORCING.era 2019-03-31    214 ../SPINUP.era/run_DOMAIN_full_full_FORCING.era_2018-10-01_000365
       jjj exec.wrf.hydro.calun.jl  DOMAIN_full_full FORCING.era 2019-04-30    185 ../SPINUP.era/run_DOMAIN_full_full_FORCING.era_2018-10-01_000365
       tail                     run_DOMAIN_full_full_FORCING.era_2019-03-31_000214.res/frxst_pts_out.txt > aa
       tail                     run_DOMAIN_full_full_FORCING.era_2019-04-30_000185.res/frxst_pts_out.txt > bb ; diff aa bb
       jjj exec.wrf.hydro.calun.jl  DOMAIN_full_full FORCING.era 2019-03-31      3 ../SPINUP.era/run_DOMAIN_full_full_FORCING.era_2018-10-01_000365
       cd                       run_DOMAIN_full_full_FORCING.era_2019-03-31_000003.res
       ./wrf_hydro.exe ; mv   frxst_pts_out.txt frxst_pts_out.txt.ref
       vi OTHER.TBL ; jjj exec.wrf.hydro.calibrate.pest.tables.jl OTHER.TBL
       ./wrf_hydro.exe ; diff frxst_pts_out.txt frxst_pts_out.txt.ref

# create the PEST observation and instruction files of interest and update the wrftest.pst file
wrkg ; cd hydat.calibration
       jjj unpack.hydat.station.pest.jl 2019-03-31 2019-05-01 2019-10-30 hydat_station_location.csv ../CALUN.era/run_DOMAIN_full_full_FORCING.era_2019-03-31_000214.res/frxst_pts_out.txt
       cp                                                                hydat_station_location.ins ../../wrf_hydro_nwm_public_pest/nwm/frxst_pts_out.ins$WRFSDOM
       cp                                                                hydat_station_location.obs ../../wrf_hydro_nwm_public_pest/nwm/wrftest.pst.obs$WRFSDOM
work ; cd wrf_hydro_nwm_public_pest/nwm
       vi wrftest.pst$WRFSDOM                                                                                                  (and add wrftest.pst.obs$WRFSDOM)

# create a PEST directory and submit the calibration run starting from the water-year spinup, and (optional) make a single-experiment tarball
wrkg ; cd CALUN.era
       export DOTD="remest" ; export DOTD="remreg" ; export DOTD="remsvd"
       setenv DOTD "remest" ; setenv DOTD "remreg" ; setenv DOTD "remsvd"
       jjj exec.wrf.hydro.calibrate.pest.nwm.jl                        DOMAIN$WRFSDOM FORCING.era 2019-03-31    214 $DOTD ../SPINUP.era/run_DOMAIN_full_full_FORCING.era_2018-10-01_000365
       cd                                                     pest_nwm_DOMAIN$WRFSDOM.FORCING.era_2019-03-31_000214.$DOTD
       sbatch                                               x.pest_nwm_DOMAIN$WRFSDOM.FORCING.era_2019-03-31_000214.$DOTD.sbatch
       rm test?/Fulldom_hires.nc test?/HYDRO_RST.2019-03-31_00:00_DOMAIN1 test?/RESTART.2019033100_DOMAIN2 test?/soil_properties.nc test?/wrfinput_d01.nc
       cd .. ; tar cvfz x$WRFSDOM.taz                         pest_nwm_DOMAIN$WRFSDOM.FORCING.era_2019-03-31_000214.$DOTD

# or make a tarball of all experiments, after removing large working files and combining streamflow ensembles into NetCDF files
wrkg ; cd CALUN.era
       cat                                                    pest_nwm_DOMAIN$WRFSDOM.FORCING.era_2019-03-31_000214.$DOTD/wrftest.log
       cd                                                     pest_nwm_DOMAIN$WRFSDOM.FORCING.era_2019-03-31_000214.$DOTD
       rm test?/Fulldom_hires.nc test?/HYDRO_RST.2019-03-31_00:00_DOMAIN1 test?/RESTART.2019033100_DOMAIN2 test?/soil_properties.nc test?/wrfinput_d01.nc
       cp          wrftest.log                             ../pest_nwm_DOMAIN$WRFSDOM.FORCING.era_2019-03-31_000214.$DOTD.log
       cd                                                  ..
       jjj         diag.wrf.ocean.entry.timeseries.pest.jl    pest_nwm_DOMAIN$WRFSDOM.FORCING.era_2019-03-31_000214.$DOTD ../hydat.calibration/hydat_station_location
       tar cvfz x$WRFSDOM.taz *.log *.txt *.nc

# compute performance metrics (from intermediate date onward) and plot streamflow ensembles with metrics
wrkg ; cd CALUN.era/pest_nwm_DOMAIN_East.FORCING.era_2019-03-31_000214.remsvd
       jjj         diag.wrf.ocean.entry.timeseries.perfold.jl    2019-05-31 pest_nwm_DOMAIN$WRFSDOM.FORCING.era_2019-03-31_000214.$DOTD
       grads -bpc "diag.wrf.ocean.entry.timeseries.perform.fig05 2019-05-31 pest_nwm_DOMAIN$WRFSDOM.FORCING.era_2019-03-31_000214.$DOTD prep.nnet.elu"
       jjj         diag.wrf.ocean.entry.timeseries.tabold.jl                pest_nwm_DOMAIN$WRFSDOM.FORCING.era_2019-03-31_000214.$DOTD
       jjj         diag.wrf.ocean.entry.timeseries.tabold.diff.jl           pest_nwm_DOMAIN$WRFSDOM.FORCING.era_2019-03-31_000214.$DOTD 1 107

----------------------------------------------------------------------------------//

# update the best-PEST-WRF-Hydro streamflow (with HyDAT as reference) using a simple neural network (for natural rivers) and check performance
wrkg ; cd CALUN.era
       setenv DOTD "remest"
       setenv DOTD "remreg"
       setenv DOTD "remsvd"
       jjj                                      diag.wrf.ocean.entry.timeseries.prepare.jl 2019-05-01 pest_nwm_DOMAIN$WRFSDOM.FORCING.era_2019-03-31_000214.$DOTD
echo   grads -bpc                              "diag.wrf.ocean.entry.timeseries.perform    2019-05-01 pest_nwm_DOMAIN$WRFSDOM.FORCING.era_2019-03-31_000214.$DOTD.prep"
       jjj                                      diag.wrf.ocean.entry.timeseries.neural.jl  2019-05-01 pest_nwm_DOMAIN$WRFSDOM.FORCING.era_2019-03-31_000214.$DOTD.prep
       parallel --dry-run           julia ~/bin/diag.wrf.ocean.entry.timeseries.neural.jl  2019-05-01 pest_nwm_DOMAIN$WRFSDOM.FORCING.era_2019-03-31_000214.$DOTD.prep      ::: identity sigmoid hardsigmoid hardtanh relu leakyrelu relu6 rrelu elu gelu swish hardswish selu celu softplus softsign logsigmoid logcosh mish tanhshrink softshrink trelu lisht > xneu ;                                       nohup parallel -j 6 < xneu > xneu.xcom &
       jjj                                      diag.wrf.ocean.entry.timeseries.perform.jl 2019-05-01 pest_nwm_DOMAIN$WRFSDOM.FORCING.era_2019-03-31_000214.$DOTD.prep.nnet
       parallel --dry-run           julia ~/bin/diag.wrf.ocean.entry.timeseries.perform.jl 2019-05-01 pest_nwm_DOMAIN$WRFSDOM.FORCING.era_2019-03-31_000214.$DOTD.prep.nnet ::: identity sigmoid hardsigmoid hardtanh relu leakyrelu relu6 rrelu elu gelu swish hardswish selu celu softplus softsign logsigmoid logcosh mish tanhshrink softshrink trelu lisht > xper ;                                       nohup parallel -j 6 < xper > xper.xcom &
       grads -bpc                              "diag.wrf.ocean.entry.timeseries.perform    2019-05-01 pest_nwm_DOMAIN$WRFSDOM.FORCING.era_2019-03-31_000214.$DOTD.prep.nnet"
       parallel --dry-run grads --quiet -bpc '"'diag.wrf.ocean.entry.timeseries.perform    2019-05-01 pest_nwm_DOMAIN$WRFSDOM.FORCING.era_2019-03-31_000214.$DOTD.prep.nnet ::: identity sigmoid hardsigmoid hardtanh relu leakyrelu relu6 rrelu elu gelu swish hardswish selu celu softplus softsign logsigmoid logcosh mish tanhshrink softshrink trelu lisht > xgra ; vi xgra (:1,22s/[a-z]$/[a-z]"/) (") ; nohup parallel -j 6 < xgra > xgra.xcom &
       jjj                                      diag.wrf.ocean.entry.timeseries.table.jl              pest_nwm_DOMAIN$WRFSDOM.FORCING.era_2019-03-31_000214.$DOTD.prep.nnet
       parallel --dry-run           julia ~/bin/diag.wrf.ocean.entry.timeseries.table.jl              pest_nwm_DOMAIN$WRFSDOM.FORCING.era_2019-03-31_000214.$DOTD.prep.nnet ::: identity sigmoid hardsigmoid hardtanh relu leakyrelu relu6 rrelu elu gelu swish hardswish selu celu softplus softsign logsigmoid logcosh mish tanhshrink softshrink trelu lisht > xtab ;                                       nohup parallel -j 6 < xtab > xtab.xcom &

----------------------------------------------------------------------------------//

# point the model to the the full domain (with lakes; not calibration domain without lakes)
wrkg ; rm    DOMAIN_full_full           SPINUP.era
       ln -s DOMAIN_full_full.lake_incl DOMAIN_full_full
       ln -s       SPINUP.era.lake_incl SPINUP.era

# perform a 15-year optimal-PEST-parameter ERA-forced simulation (from SPINUP restart) and
# apply a natural-river neural network post-processing to streamflow at all ocean pour points
wrkg ; cd SPINUP.era
       jjj exec.wrf.hydro.calun.jl                                  DOMAIN_full_full FORCING.era 1990-01-01 2004-12-30
       jjj exec.wrf.hydro.calun.jl                                  DOMAIN_full_full FORCING.era 1993-03-10 2004-12-30 run_DOMAIN_full_full_FORCING.era_1990-01-01_2004-12-30
       jjj exec.wrf.hydro.calun.jl                                  DOMAIN_full_full FORCING.era 2004-12-30 2021-12-30 run_DOMAIN_full_full_FORCING.era_1993-03-10_2004-12-31.res
       jjj exec.wrf.hydro.calun.jl                                  DOMAIN_full_full FORCING.era 2014-09-17 2021-12-30 run_DOMAIN_full_full_FORCING.era_2004-12-30_2021-12-30.res
       jjj exec.wrf.hydro.calun.jl                                  DOMAIN_full_full FORCING.era 2021-12-29 2023-07-28 run_DOMAIN_full_full_FORCING.era_2014-09-17_2021-12-30.res
wrkg ; cd SPINUP.era/dat
       jjj diag.wrf.ocean.entry.links.jl                     ../run_DOMAIN_full_full_FORCING.era_1990-01-01_2004-12-31/
       jjj diag.wrf.ocean.entry.links.jl                     ../run_DOMAIN_full_full_FORCING.era_1993-03-10_2004-12-31.res/
       jjj diag.wrf.ocean.entry.links.jl                     ../run_DOMAIN_full_full_FORCING.era_2004-12-30_2021-12-30.res/
       jjj diag.wrf.ocean.entry.links.jl                     ../run_DOMAIN_full_full_FORCING.era_2014-09-17_2021-12-30.res/
       jjj diag.wrf.ocean.entry.links.jl                     ../run_DOMAIN_full_full_FORCING.era_2021-12-29_2023-07-28.res/
wrkg ; cd SPINUP.era
       jjj diag.wrf.ocean.entry.timeseries.grid.jl              run_DOMAIN_full_full_FORCING.era_1990-01-01_2004-12-30       ../DOMAIN_full_full/Fulldom_hires_outlets.txt
       jjj diag.wrf.ocean.entry.timeseries.grid.jl              run_DOMAIN_full_full_FORCING.era_1990-01-01_2023-07-27       ../DOMAIN_full_full/Fulldom_hires_outlets.txt
       jjj diag.wrf.ocean.entry.timeseries.grid.subset.jl       run_DOMAIN_full_full_FORCING.era_1990-01-01_2004-12-30_3h
       jjj diag.wrf.ocean.entry.timeseries.neural.post.jl       run_DOMAIN_full_full_FORCING.era_1990-01-01_2004-12-30_3h_Severn_Hudson.nc ../CALUN.era/pest_nwm_DOMAIN_East.FORCING.era_2019-03-31_000214.remsvd.prep.nnet.elu.jld

# perform a 110-year optimal-PEST-parameter CCSM-forced simulation (from SPINUP restart) and
# apply a natural-river neural network post-processing to streamflow at all ocean pour points
wrkg ; cd SPINUP.ccs
       jjj exec.wrf.hydro.calun.jl                                  DOMAIN_full_full FORCING.ccs 1990-01-01 2099-12-30
       jjj exec.wrf.hydro.calun.jl                                  DOMAIN_full_full FORCING.ccs 2004-02-21 2099-12-30 run_DOMAIN_full_full_FORCING.ccs_1990-01-01_2099-12-30
       jjj exec.wrf.hydro.calun.jl                                  DOMAIN_full_full FORCING.ccs 2018-07-31 2099-12-30 run_DOMAIN_full_full_FORCING.ccs_2004-02-21_2099-12-30.res
       jjj exec.wrf.hydro.calun.jl                                  DOMAIN_full_full FORCING.ccs 2025-08-12 2099-12-30 run_DOMAIN_full_full_FORCING.ccs_2018-07-31_2099-12-30.res
       jjj exec.wrf.hydro.calun.jl                                  DOMAIN_full_full FORCING.ccs 2031-01-24 2099-12-30 run_DOMAIN_full_full_FORCING.ccs_2025-08-12_2099-12-30.res
       jjj exec.wrf.hydro.calun.jl                                  DOMAIN_full_full FORCING.ccs 2044-06-26 2099-12-30 run_DOMAIN_full_full_FORCING.ccs_2031-01-24_2099-12-30.res
       jjj exec.wrf.hydro.calun.jl                                  DOMAIN_full_full FORCING.ccs 2055-11-24 2099-12-30 run_DOMAIN_full_full_FORCING.ccs_2044-06-26_2099-12-30.res
       jjj exec.wrf.hydro.calun.jl                                  DOMAIN_full_full FORCING.ccs 2060-10-23 2099-12-30 run_DOMAIN_full_full_FORCING.ccs_2055-11-24_2099-12-30.res
       jjj exec.wrf.hydro.calun.jl                                  DOMAIN_full_full FORCING.ccs 2074-03-29 2099-12-30 run_DOMAIN_full_full_FORCING.ccs_2060-10-23_2099-12-30.res
       jjj exec.wrf.hydro.calun.jl                                  DOMAIN_full_full FORCING.ccs 2082-12-28 2099-12-30 run_DOMAIN_full_full_FORCING.ccs_2074-03-29_2099-12-30.res
       jjj exec.wrf.hydro.calun.jl                                  DOMAIN_full_full FORCING.ccs 2083-08-04 2099-12-30 run_DOMAIN_full_full_FORCING.ccs_2082-12-28_2099-12-30.res
       jjj exec.wrf.hydro.calun.jl                                  DOMAIN_full_full FORCING.ccs 2091-01-16 2099-12-30 run_DOMAIN_full_full_FORCING.ccs_2083-08-04_2099-12-30.res
       jjj exec.wrf.hydro.calun.jl                                  DOMAIN_full_full FORCING.ccs 2091-07-14 2099-12-30 run_DOMAIN_full_full_FORCING.ccs_2091-01-16_2099-12-30.res
wrkg ; cd SPINUP.ccs/dat
       jjj diag.wrf.ocean.entry.links.jl                     ../run_DOMAIN_full_full_FORCING.ccs_1990-01-01_2099-12-30/
       jjj diag.wrf.ocean.entry.links.jl                     ../run_DOMAIN_full_full_FORCING.ccs_2004-02-21_2099-12-30.res/
       jjj diag.wrf.ocean.entry.links.jl                     ../run_DOMAIN_full_full_FORCING.ccs_2018-07-31_2099-12-30.res/
       jjj diag.wrf.ocean.entry.links.jl                     ../run_DOMAIN_full_full_FORCING.ccs_2025-08-12_2099-12-30.res/
       jjj diag.wrf.ocean.entry.links.jl                     ../run_DOMAIN_full_full_FORCING.ccs_2031-01-24_2099-12-30.res/
       jjj diag.wrf.ocean.entry.links.jl                     ../run_DOMAIN_full_full_FORCING.ccs_2044-06-26_2099-12-30.res/
       jjj diag.wrf.ocean.entry.links.jl                     ../run_DOMAIN_full_full_FORCING.ccs_2055-11-24_2099-12-30.res/
       jjj diag.wrf.ocean.entry.links.jl                     ../run_DOMAIN_full_full_FORCING.ccs_2060-10-23_2099-12-30.res/
       jjj diag.wrf.ocean.entry.links.jl                     ../run_DOMAIN_full_full_FORCING.ccs_2074-03-29_2099-12-30.res/
       jjj diag.wrf.ocean.entry.links.jl                     ../run_DOMAIN_full_full_FORCING.ccs_2082-12-28_2099-12-30.res/
       jjj diag.wrf.ocean.entry.links.jl                     ../run_DOMAIN_full_full_FORCING.ccs_2083-08-04_2099-12-30.res/
       jjj diag.wrf.ocean.entry.links.jl                     ../run_DOMAIN_full_full_FORCING.ccs_2091-01-16_2099-12-30.res/
       jjj diag.wrf.ocean.entry.links.jl                     ../run_DOMAIN_full_full_FORCING.ccs_2091-07-14_2099-12-30.res/
wrkg ; cd SPINUP.ccs
       jjj diag.wrf.ocean.entry.timeseries.grid.jl              run_DOMAIN_full_full_FORCING.ccs_1990-01-01_2099-12-30       ../DOMAIN_full_full/Fulldom_hires_outlets.txt
       jjj diag.wrf.ocean.entry.timeseries.grid.subset.jl       run_DOMAIN_full_full_FORCING.ccs_1990-01-01_2099-12-30_3h
       jjj diag.wrf.ocean.entry.timeseries.grid.multi.mean.jl   run_DOMAIN_full_full_FORCING.ccs_1990-01-01_2099-12-30_3h_Severn_Hudson.nc
       grads -bpc "diag.wrf.ocean.entry.timeseries.grid.multi.mean  DOMAIN_full_full_FORCING.ccs_1990-01-01_2099-12-30_3h_Severn_Hudson"
       jjj diag.wrf.ocean.entry.timeseries.neural.post.jl       run_DOMAIN_full_full_FORCING.ccs_1990-01-01_2099-12-30_3h_Severn_Hudson.nc ../CALUN.era/pest_nwm_DOMAIN_East.FORCING.era_2019-03-31_000214.remsvd.prep.nnet.elu.jld
       jjj diag.wrf.ocean.entry.timeseries.grid.multi.mean.jl   run_DOMAIN_full_full_FORCING.ccs_1990-01-01_2099-12-30_3h_Severn_Hudson.neural.nc
       grads -bpc "diag.wrf.ocean.entry.timeseries.grid.multi.mean  DOMAIN_full_full_FORCING.ccs_1990-01-01_2099-12-30_3h_Severn_Hudson.neural"

# perform a 15-year optimal-PEST-parameter WRF-forced simulation (from SPINUP restart) and
# apply a natural-river neural network post-processing to streamflow at all ocean pour points
wrkg ; cd SPINUP.wrf
       jjj exec.wrf.hydro.calun.jl                                  DOMAIN_full_full FORCING.wrf 1990-01-01 2004-12-30
       jjj exec.wrf.hydro.calun.jl                                  DOMAIN_full_full FORCING.wrf 2003-12-25 2004-12-30 run_DOMAIN_full_full_FORCING.wrf_1990-01-01_2004-12-30
wrkg ; cd SPINUP.wrf/dat
       jjj diag.wrf.ocean.entry.links.jl                     ../run_DOMAIN_full_full_FORCING.wrf_1990-01-01_2099-12-30/
       jjj diag.wrf.ocean.entry.links.jl                     ../run_DOMAIN_full_full_FORCING.wrf_2003-12-25_2099-12-30.res/
wrkg ; cd SPINUP.wrf
       jjj diag.wrf.ocean.entry.timeseries.grid.jl              run_DOMAIN_full_full_FORCING.wrf_1990-01-01_2004-12-30       ../DOMAIN_full_full/Fulldom_hires_outlets.txt
       jjj diag.wrf.ocean.entry.timeseries.grid.subset.jl       run_DOMAIN_full_full_FORCING.wrf_1990-01-01_2004-12-30_3h
       jjj diag.wrf.ocean.entry.timeseries.neural.post.jl       run_DOMAIN_full_full_FORCING.wrf_1990-01-01_2004-12-30_3h_Severn_Hudson.nc ../CALUN.era/pest_nwm_DOMAIN_East.FORCING.era_2019-03-31_000214.remsvd.prep.nnet.elu.jld

# perform a 110-year optimal-PEST-parameter WRF-forced simulation (from SPINUP restart) and
# and apply a natural-river neural network post-processing to streamflow at all ocean pour points
wrkg ; cd SPINUP.wrf
       jjj exec.wrf.hydro.calun.jl                                  DOMAIN_full_full FORCING.w85 1990-01-01 2099-12-30
       jjj exec.wrf.hydro.calun.jl                                  DOMAIN_full_full FORCING.w85 2005-09-28 2099-12-30 run_DOMAIN_full_full_FORCING.w85_1990-01-01_2099-12-30
       jjj exec.wrf.hydro.calun.jl                                  DOMAIN_full_full FORCING.w85 2020-01-14 2099-12-30 run_DOMAIN_full_full_FORCING.w85_2005-09-28_2099-12-30.res
       jjj exec.wrf.hydro.calun.jl                                  DOMAIN_full_full FORCING.w85 2025-03-24 2099-12-30 run_DOMAIN_full_full_FORCING.w85_2020-01-14_2099-12-30.res
       jjj exec.wrf.hydro.calun.jl                                  DOMAIN_full_full FORCING.w85 2040-03-01 2099-12-30 run_DOMAIN_full_full_FORCING.w85_2025-03-24_2099-12-30.res
       jjj exec.wrf.hydro.calun.jl                                  DOMAIN_full_full FORCING.w85 2055-04-26 2099-12-30 run_DOMAIN_full_full_FORCING.w85_2040-03-01_2099-12-30.res
       jjj exec.wrf.hydro.calun.jl                                  DOMAIN_full_full FORCING.w85 2069-04-09 2099-12-30 run_DOMAIN_full_full_FORCING.w85_2055-04-26_2099-12-30.res
       jjj exec.wrf.hydro.calun.jl                                  DOMAIN_full_full FORCING.w85 2083-10-14 2099-12-30 run_DOMAIN_full_full_FORCING.w85_2069-04-09_2099-12-30.res
       jjj exec.wrf.hydro.calun.jl                                  DOMAIN_full_full FORCING.w85 2089-06-27 2099-12-30 run_DOMAIN_full_full_FORCING.w85_2083-10-14_2099-12-30.res
wrkg ; cd SPINUP.wrf/dat
       jjj diag.wrf.ocean.entry.links.jl                     ../run_DOMAIN_full_full_FORCING.w85_1990-01-01_2099-12-30/
       jjj diag.wrf.ocean.entry.links.jl                     ../run_DOMAIN_full_full_FORCING.w85_2005-09-28_2099-12-30.res/
       jjj diag.wrf.ocean.entry.links.jl                     ../run_DOMAIN_full_full_FORCING.w85_2020-01-14_2099-12-30.res/
       jjj diag.wrf.ocean.entry.links.jl                     ../run_DOMAIN_full_full_FORCING.w85_2025-03-24_2099-12-30.res/
       jjj diag.wrf.ocean.entry.links.jl                     ../run_DOMAIN_full_full_FORCING.w85_2040-03-01_2099-12-30.res/
       jjj diag.wrf.ocean.entry.links.jl                     ../run_DOMAIN_full_full_FORCING.w85_2055-04-26_2099-12-30.res/
       jjj diag.wrf.ocean.entry.links.jl                     ../run_DOMAIN_full_full_FORCING.w85_2069-04-09_2099-12-30.res/
       jjj diag.wrf.ocean.entry.links.jl                     ../run_DOMAIN_full_full_FORCING.w85_2083-10-14_2099-12-30.res/
       jjj diag.wrf.ocean.entry.links.jl                     ../run_DOMAIN_full_full_FORCING.w85_2089-06-27_2099-12-30.res/
wrkg ; cd SPINUP.wrf
       jjj diag.wrf.ocean.entry.timeseries.grid.jl              run_DOMAIN_full_full_FORCING.w85_1990-01-01_2099-12-30       ../DOMAIN_full_full/Fulldom_hires_outlets.txt
       jjj diag.wrf.ocean.entry.timeseries.grid.subset.jl       run_DOMAIN_full_full_FORCING.w85_1990-01-01_2099-12-30_3h
       jjj diag.wrf.ocean.entry.timeseries.grid.multi.mean.jl   run_DOMAIN_full_full_FORCING.w85_1990-01-01_2099-12-30_3h_Severn_Hudson.nc
       grads -bpc "diag.wrf.ocean.entry.timeseries.grid.multi.mean  DOMAIN_full_full_FORCING.w85_1990-01-01_2099-12-30_3h_Severn_Hudson"
       jjj diag.wrf.ocean.entry.timeseries.neural.post.jl       run_DOMAIN_full_full_FORCING.w85_1990-01-01_2099-12-30_3h_Severn_Hudson.nc ../CALUN.era/pest_nwm_DOMAIN_East.FORCING.era_2019-03-31_000214.remsvd.prep.nnet.elu.jld
       jjj diag.wrf.ocean.entry.timeseries.grid.multi.mean.jl   run_DOMAIN_full_full_FORCING.w85_1990-01-01_2099-12-30_3h_Severn_Hudson.neural.nc
       grads -bpc "diag.wrf.ocean.entry.timeseries.grid.multi.mean  DOMAIN_full_full_FORCING.w85_1990-01-01_2099-12-30_3h_Severn_Hudson.neural"

# perform a 110-year optimal-PEST-parameter HadGEM RCP-4.5-forced simulation (from SPINUP restart)
# and apply a natural-river neural network post-processing to streamflow at all ocean pour points
wrkg ; cd SPINUP.h45
       jjj exec.wrf.hydro.calun.jl                                  DOMAIN_full_full FORCING.h45 1990-01-01 2099-12-30
       jjj exec.wrf.hydro.calun.jl                                  DOMAIN_full_full FORCING.h45 2004-09-19 2099-12-30 run_DOMAIN_full_full_FORCING.h45_1990-01-01_2099-12-30
       jjj exec.wrf.hydro.calun.jl                                  DOMAIN_full_full FORCING.h45 2018-09-30 2099-12-30 run_DOMAIN_full_full_FORCING.h45_2004-09-19_2099-12-30.res
       jjj exec.wrf.hydro.calun.jl                                  DOMAIN_full_full FORCING.h45 2027-01-27 2099-12-30 run_DOMAIN_full_full_FORCING.h45_2018-09-30_2099-12-30.res
       jjj exec.wrf.hydro.calun.jl                                  DOMAIN_full_full FORCING.h45 2033-07-28 2099-12-30 run_DOMAIN_full_full_FORCING.h45_2027-01-27_2099-12-30.res
       jjj exec.wrf.hydro.calun.jl                                  DOMAIN_full_full FORCING.h45 2048-01-04 2099-12-30 run_DOMAIN_full_full_FORCING.h45_2033-07-28_2099-12-30.res
       jjj exec.wrf.hydro.calun.jl                                  DOMAIN_full_full FORCING.h45 2058-12-21 2099-12-30 run_DOMAIN_full_full_FORCING.h45_2048-01-04_2099-12-30.res
       jjj exec.wrf.hydro.calun.jl                                  DOMAIN_full_full FORCING.h45 2063-07-22 2099-12-30 run_DOMAIN_full_full_FORCING.h45_2058-12-21_2099-12-30.res
       jjj exec.wrf.hydro.calun.jl                                  DOMAIN_full_full FORCING.h45 2076-05-30 2099-12-30 run_DOMAIN_full_full_FORCING.h45_2063-07-22_2099-12-30.res
       jjj exec.wrf.hydro.calun.jl                                  DOMAIN_full_full FORCING.h45 2084-09-15 2099-12-30 run_DOMAIN_full_full_FORCING.h45_2076-05-30_2099-12-30.res
       jjj exec.wrf.hydro.calun.jl                                  DOMAIN_full_full FORCING.h45 2085-01-13 2099-12-30 run_DOMAIN_full_full_FORCING.h45_2084-09-15_2099-12-30.res
       jjj exec.wrf.hydro.calun.jl                                  DOMAIN_full_full FORCING.h45 2092-04-29 2099-12-30 run_DOMAIN_full_full_FORCING.h45_2085-01-13_2099-12-30.res
       jjj exec.wrf.hydro.calun.jl                                  DOMAIN_full_full FORCING.h45 2092-05-23 2099-12-30 run_DOMAIN_full_full_FORCING.h45_2092-04-29_2099-12-30.res
wrkg ; cd SPINUP.h45/dat
       jjj diag.wrf.ocean.entry.links.jl                     ../run_DOMAIN_full_full_FORCING.h45_1990-01-01_2099-12-30/
       jjj diag.wrf.ocean.entry.links.jl                     ../run_DOMAIN_full_full_FORCING.h45_2004-09-19_2099-12-30.res/
       jjj diag.wrf.ocean.entry.links.jl                     ../run_DOMAIN_full_full_FORCING.h45_2018-09-30_2099-12-30.res/
       jjj diag.wrf.ocean.entry.links.jl                     ../run_DOMAIN_full_full_FORCING.h45_2027-01-27_2099-12-30.res/
       jjj diag.wrf.ocean.entry.links.jl                     ../run_DOMAIN_full_full_FORCING.h45_2033-07-28_2099-12-30.res/
       jjj diag.wrf.ocean.entry.links.jl                     ../run_DOMAIN_full_full_FORCING.h45_2048-01-04_2099-12-30.res/
       jjj diag.wrf.ocean.entry.links.jl                     ../run_DOMAIN_full_full_FORCING.h45_2058-12-21_2099-12-30.res/
       jjj diag.wrf.ocean.entry.links.jl                     ../run_DOMAIN_full_full_FORCING.h45_2063-07-22_2099-12-30.res/
       jjj diag.wrf.ocean.entry.links.jl                     ../run_DOMAIN_full_full_FORCING.h45_2076-05-30_2099-12-30.res/
       jjj diag.wrf.ocean.entry.links.jl                     ../run_DOMAIN_full_full_FORCING.h45_2084-09-15_2099-12-30.res/
       jjj diag.wrf.ocean.entry.links.jl                     ../run_DOMAIN_full_full_FORCING.h45_2085-01-13_2099-12-30.res/
       jjj diag.wrf.ocean.entry.links.jl                     ../run_DOMAIN_full_full_FORCING.h45_2092-04-29_2099-12-30.res/
       jjj diag.wrf.ocean.entry.links.jl                     ../run_DOMAIN_full_full_FORCING.h45_2092-05-23_2099-12-30.res/
wrkg ; cd SPINUP.h45
       jjj diag.wrf.ocean.entry.timeseries.grid.jl              run_DOMAIN_full_full_FORCING.h45_1990-01-01_2099-12-30       ../DOMAIN_full_full/Fulldom_hires_outlets.txt
       jjj diag.wrf.ocean.entry.timeseries.grid.subset.jl       run_DOMAIN_full_full_FORCING.h45_1990-01-01_2099-12-30_3h
       jjj diag.wrf.ocean.entry.timeseries.grid.multi.mean.jl   run_DOMAIN_full_full_FORCING.h45_1990-01-01_2099-12-30_3h_Severn_Hudson.nc
       grads -bpc "diag.wrf.ocean.entry.timeseries.grid.multi.mean  DOMAIN_full_full_FORCING.h45_1990-01-01_2099-12-30_3h_Severn_Hudson"
       jjj diag.wrf.ocean.entry.timeseries.neural.post.jl       run_DOMAIN_full_full_FORCING.h45_1990-01-01_2099-12-30_3h_Severn_Hudson.nc ../CALUN.era/pest_nwm_DOMAIN_East.FORCING.era_2019-03-31_000214.remsvd.prep.nnet.elu.jld
       jjj diag.wrf.ocean.entry.timeseries.grid.multi.mean.jl   run_DOMAIN_full_full_FORCING.h45_1990-01-01_2099-12-30_3h_Severn_Hudson.neural.nc
       grads -bpc "diag.wrf.ocean.entry.timeseries.grid.multi.mean  DOMAIN_full_full_FORCING.h45_1990-01-01_2099-12-30_3h_Severn_Hudson.neural"

# perform a 110-year optimal-PEST-parameter HadGEM RCP-8.5-forced simulation (from SPINUP restart)
# and apply a natural-river neural network post-processing to streamflow at all ocean pour points
wrkg ; cd SPINUP.h85
       jjj exec.wrf.hydro.calun.jl                                  DOMAIN_full_full FORCING.h85 1990-01-01 2099-12-30
       jjj exec.wrf.hydro.calun.jl                                  DOMAIN_full_full FORCING.h85 2004-09-19 2099-12-30 run_DOMAIN_full_full_FORCING.h85_1990-01-01_2099-12-30
       jjj exec.wrf.hydro.calun.jl                                  DOMAIN_full_full FORCING.h85 2019-03-26 2099-12-30 run_DOMAIN_full_full_FORCING.h85_2004-09-19_2099-12-30.res
       jjj exec.wrf.hydro.calun.jl                                  DOMAIN_full_full FORCING.h85 2027-04-22 2099-12-30 run_DOMAIN_full_full_FORCING.h85_2019-03-26_2099-12-30.res
       jjj exec.wrf.hydro.calun.jl                                  DOMAIN_full_full FORCING.h85 2032-12-12 2099-12-30 run_DOMAIN_full_full_FORCING.h85_2027-04-22_2099-12-30.res
       jjj exec.wrf.hydro.calun.jl                                  DOMAIN_full_full FORCING.h85 2045-10-03 2099-12-30 run_DOMAIN_full_full_FORCING.h85_2032-12-12_2099-12-30.res
       jjj exec.wrf.hydro.calun.jl                                  DOMAIN_full_full FORCING.h85 2057-03-05 2099-12-30 run_DOMAIN_full_full_FORCING.h85_2045-10-03_2099-12-30.res
       jjj exec.wrf.hydro.calun.jl                                  DOMAIN_full_full FORCING.h85 2062-10-23 2099-12-30 run_DOMAIN_full_full_FORCING.h85_2057-03-05_2099-12-30.res
       jjj exec.wrf.hydro.calun.jl                                  DOMAIN_full_full FORCING.h85 2076-08-08 2099-12-30 run_DOMAIN_full_full_FORCING.h85_2062-10-23_2099-12-30.res
       jjj exec.wrf.hydro.calun.jl                                  DOMAIN_full_full FORCING.h85 2085-05-21 2099-12-30 run_DOMAIN_full_full_FORCING.h85_2076-08-08_2099-12-30.res
       jjj exec.wrf.hydro.calun.jl                                  DOMAIN_full_full FORCING.h85 2085-11-15 2099-12-30 run_DOMAIN_full_full_FORCING.h85_2085-05-21_2099-12-30.res
       jjj exec.wrf.hydro.calun.jl                                  DOMAIN_full_full FORCING.h85 2093-08-16 2099-12-30 run_DOMAIN_full_full_FORCING.h85_2085-11-15_2099-12-30.res
       jjj exec.wrf.hydro.calun.jl                                  DOMAIN_full_full FORCING.h85 2093-09-06 2099-12-30 run_DOMAIN_full_full_FORCING.h85_2093-08-16_2099-12-30.res
wrkg ; cd SPINUP.h85/dat
       jjj diag.wrf.ocean.entry.links.jl                     ../run_DOMAIN_full_full_FORCING.h85_1990-01-01_2099-12-30/
       jjj diag.wrf.ocean.entry.links.jl                     ../run_DOMAIN_full_full_FORCING.h85_2004-09-19_2099-12-30.res/
       jjj diag.wrf.ocean.entry.links.jl                     ../run_DOMAIN_full_full_FORCING.h85_2019-03-26_2099-12-30.res/
       jjj diag.wrf.ocean.entry.links.jl                     ../run_DOMAIN_full_full_FORCING.h85_2027-04-22_2099-12-30.res/
       jjj diag.wrf.ocean.entry.links.jl                     ../run_DOMAIN_full_full_FORCING.h85_2032-12-12_2099-12-30.res/
       jjj diag.wrf.ocean.entry.links.jl                     ../run_DOMAIN_full_full_FORCING.h85_2045-10-03_2099-12-30.res/
       jjj diag.wrf.ocean.entry.links.jl                     ../run_DOMAIN_full_full_FORCING.h85_2057-03-05_2099-12-30.res/
       jjj diag.wrf.ocean.entry.links.jl                     ../run_DOMAIN_full_full_FORCING.h85_2062-10-23_2099-12-30.res/
       jjj diag.wrf.ocean.entry.links.jl                     ../run_DOMAIN_full_full_FORCING.h85_2076-08-08_2099-12-30.res/
       jjj diag.wrf.ocean.entry.links.jl                     ../run_DOMAIN_full_full_FORCING.h85_2085-05-21_2099-12-30.res/
       jjj diag.wrf.ocean.entry.links.jl                     ../run_DOMAIN_full_full_FORCING.h85_2085-11-15_2099-12-30.res/
       jjj diag.wrf.ocean.entry.links.jl                     ../run_DOMAIN_full_full_FORCING.h85_2093-08-16_2099-12-30.res/
       jjj diag.wrf.ocean.entry.links.jl                     ../run_DOMAIN_full_full_FORCING.h85_2093-09-06_2099-12-30.res/
wrkg ; cd SPINUP.h85
       jjj diag.wrf.ocean.entry.timeseries.grid.jl              run_DOMAIN_full_full_FORCING.h85_1990-01-01_2099-12-30       ../DOMAIN_full_full/Fulldom_hires_outlets.txt
       jjj diag.wrf.ocean.entry.timeseries.grid.subset.jl       run_DOMAIN_full_full_FORCING.h85_1990-01-01_2099-12-30_3h
       jjj diag.wrf.ocean.entry.timeseries.grid.multi.mean.jl   run_DOMAIN_full_full_FORCING.h85_1990-01-01_2099-12-30_3h_Severn_Hudson.nc
       grads -bpc "diag.wrf.ocean.entry.timeseries.grid.multi.mean  DOMAIN_full_full_FORCING.h85_1990-01-01_2099-12-30_3h_Severn_Hudson"
       jjj diag.wrf.ocean.entry.timeseries.neural.post.jl       run_DOMAIN_full_full_FORCING.h85_1990-01-01_2099-12-30_3h_Severn_Hudson.nc ../CALUN.era/pest_nwm_DOMAIN_East.FORCING.era_2019-03-31_000214.remsvd.prep.nnet.elu.jld
       jjj diag.wrf.ocean.entry.timeseries.grid.multi.mean.jl   run_DOMAIN_full_full_FORCING.h85_1990-01-01_2099-12-30_3h_Severn_Hudson.neural.nc
       grads -bpc "diag.wrf.ocean.entry.timeseries.grid.multi.mean  DOMAIN_full_full_FORCING.h85_1990-01-01_2099-12-30_3h_Severn_Hudson.neural"

----------------------------------------------------------------------------------//

# perform a restart file cleanup in the SPINUP dirs
wrkg ; nohup jjj diag.wrf.ocean.entry.cleanup.restarts.jl > xcom &
       csh xcom
       grep ccs xcom > xcom.ccs
       grep h45 xcom > xcom.h45
       grep h85 xcom > xcom.h85
       grep wrf xcom > xcom.wrf
       nohup csh xcom.ccs > xcom.ccs.xcom &
       nohup csh xcom.h45 > xcom.h45.xcom &
       nohup csh xcom.h85 > xcom.h85.xcom &
       nohup csh xcom.wrf > xcom.wrf.xcom &

----------------------------------------------------------------------------------//

# construct station-specific neural networks for pour points with upstream stations, where we
# compare HyDAT and WRF-Hydro streamflow using ERA/WRF forcing, with raw/neural-network output
# after identifying HyDAT stations with at least 1000 daily observations between 1990 and 2004
# (call this "validation", and as above, point the model to the the full domain with lakes)
# exclude 02OC019, 02RF001 (natural) and 02BE002, 02HK010, 02CA001, 02HA003, 02OA024 (regulated)
# then identify the station-model comparisons with good overlap in upstream catchment (i.e., stations
# for which neither the HyDAT nor WRFHydro upstream catchment area is greater than 1.2 times the other)
wrkg ; rm    DOMAIN_full_full           SPINUP.era
       ln -s DOMAIN_full_full.lake_incl DOMAIN_full_full
       ln -s       SPINUP.era.lake_incl SPINUP.era
wrkg ; mkdir                                                hydat.validation
       jjj diag.wrf.ocean.entry.timeseries.valding.jl hydat hydat.validation
       cd hydat.validation ; csh                            hydat_valding_aall_1000.com
#      ls -1 hydat* | grep -E '.nc$' | parallel julia /home/riced/bin/unpack.hydat.station.text.jl 2017-10-01-00
       jjj diag.wrf.ocean.entry.timeseries.valding.catch.jl hydat_valding_aall_1000.txt
       mkdir limbo ;         csh                            hydat_valding_aall_1000_excl.com

# locate each HyDAT station on the 15-s HydroSHEDS and 2-km WRF-Hydro river networks and identify
# and plot individual upstream catchments (with lakes)
wrkg ; cd hydat.validation
       jjj unpack.hydat.station.river.hydrosheds.jl ../HYDROSHEDS/ hydat_03JB004.sta
       jjj unpack.hydat.station.catch.hydrosheds.jl ../HYDROSHEDS/ hydat_03JB004.sta
       grads --quiet -blc "diag.hydroshed.watershed.hydat          hydat_03JB004.sta" ; di hydat_03JB004.sta.png
       ls -1 | grep -E '.sta$' | parallel --dry-run julia /home/riced/bin/unpack.hydat.station.river.hydrosheds.jl ../HYDROSHEDS/       > xrhy ; nohup parallel -j 6 < xrhy > xrhy.xcom &
       ls -1 | grep -E '.sta$' | parallel --dry-run julia /home/riced/bin/unpack.hydat.station.catch.hydrosheds.jl ../HYDROSHEDS/       > xshy ; nohup parallel -j 6 < xshy > xshy.xcom &
       ln -f -s ../DOMAIN_full_full/Fulldom_hires_river.txt Fulldom_hires_river.txt
       jjj unpack.hydat.station.river.wrfhydro.jl ../DOMAIN_full_full/ hydat_03JB004.sta
       jjj unpack.hydat.station.catch.wrfhydro.jl ../DOMAIN_full_full/ hydat_03JB004.sta
       grads --quiet -blc "diag.hydroshed.watershed.hydat              hydat_03JB004.sta" ; di hydat_03JB004.sta.png
       ls -1 | grep -E '.sta$' | parallel --dry-run julia /home/riced/bin/unpack.hydat.station.river.wrfhydro.jl ../DOMAIN_full_full/ > xrwr ; nohup parallel -j 6 < xrwr > xrwr.xcom &
       ls -1 | grep -E '.sta$' | parallel --dry-run julia /home/riced/bin/unpack.hydat.station.catch.wrfhydro.jl ../DOMAIN_full_full/ > xswr ; nohup parallel -j 6 < xswr > xswr.xcom &
       ls -1 | grep -E '.sta$' | parallel --dry-run grads --quiet -blc '"'diag.hydroshed.watershed.hydat > xgra ; vi xgra (":, s/.sta/.sta") ; nohup parallel -j 6 < xgra > xgra.xcom &
#      grads --quiet -blc "diag.wrf.ocean.entry.timeseries.valding hydat_valding_aall_1000_incl .sta.hyshed" ; di hydat_valding_aall_1000_incl.sta.hyshed.png
       grads --quiet -blc "diag.wrf.ocean.entry.timeseries.valding hydat_valding_aall_1000_incl .sta.hwshed" ; di hydat_valding_aall_1000_incl.sta.hwshed.png

# create a 1990-2022 NetCDF file that accommodates four three-hourly WRF-Hydro streamflow estimates
# (WRF/ERA forcing, raw/neural-network output) and daily HyDAT observations, then add the natural-river
# neural network output (given raw ERA/WRF-forcing streamflow), and assess performance for each watershed
wrkg ; cd hydat.validation
       ln -s ../DOMAIN_full_full/Fulldom_hires_river.txt Fulldom_hires_river.txt
       cp                                                              hydat_valding_aall_1000_incl.txt                              hydat_valding_aall_1000_iccs.txt
       cp                                                              hydat_valding_aall_1000_incl.txt                              hydat_valding_aall_1000_ih45.txt
       cp                                                              hydat_valding_aall_1000_incl.txt                              hydat_valding_aall_1000_ih85.txt
       cp                                                              hydat_valding_aall_1000_incl.txt                              hydat_valding_aall_1000_impi.txt
       jjj         diag.wrf.ocean.entry.timeseries.valding.assemble.jl 1990-01-01 2022-12-30 ../SPINUP.ccs/dat/ ../SPINUP.era/dat/   hydat_valding_aall_1000_incl
       jjj         diag.wrf.ocean.entry.timeseries.valding.assemble.jl 1990-01-01 2022-12-30 ../SPINUP.h45/dat/ ../SPINUP.era/dat/   hydat_valding_aall_1000_incl
       jjj         diag.wrf.ocean.entry.timeseries.valding.assemble.jl 1990-01-01 2022-12-30 ../SPINUP.h85/dat/ ../SPINUP.era/dat/   hydat_valding_aall_1000_incl
       jjj         diag.wrf.ocean.entry.timeseries.valding.assemble.jl 1990-01-01 2022-12-30 ../SPINUP.wrf/dat/ ../SPINUP.era/dat/   hydat_valding_aall_1000_incl
       cp                                                              hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.txt        hydat_valding_aall_1000_impi_1990-01-01_2022-12-30.txt
       ln -s                                                           hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.nc         hydat_valding_aall_1000_impi_1990-01-01_2022-12-30.nc
       jjj         diag.wrf.ocean.entry.timeseries.neural.assemble.jl  hydat_valding_aall_1000_iccs_1990-01-01_2022-12-30.nc ../CALUN.era/pest_nwm_DOMAIN_East.FORCING.era_2019-03-31_000214.remsvd.prep.nnet.elu.jld
       jjj         diag.wrf.ocean.entry.timeseries.neural.assemble.jl  hydat_valding_aall_1000_ih45_1990-01-01_2022-12-30.nc ../CALUN.era/pest_nwm_DOMAIN_East.FORCING.era_2019-03-31_000214.remsvd.prep.nnet.elu.jld
       jjj         diag.wrf.ocean.entry.timeseries.neural.assemble.jl  hydat_valding_aall_1000_ih85_1990-01-01_2022-12-30.nc ../CALUN.era/pest_nwm_DOMAIN_East.FORCING.era_2019-03-31_000214.remsvd.prep.nnet.elu.jld
       jjj         diag.wrf.ocean.entry.timeseries.neural.assemble.jl  hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.nc ../CALUN.era/pest_nwm_DOMAIN_East.FORCING.era_2019-03-31_000214.remsvd.prep.nnet.elu.jld
       cp                                                              hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.txt hydat_valding_aall_1000_impi_1990-01-01_2022-12-30.neural.txt
       ln -s                                                           hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.nc  hydat_valding_aall_1000_impi_1990-01-01_2022-12-30.neural.nc
       jjj         diag.wrf.ocean.entry.timeseries.mean.assemble.jl    hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.nc
       jjj         diag.wrf.ocean.entry.timeseries.perform.jl          hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.daily
       jjj         diag.wrf.ocean.entry.timeseries.perform.jl          hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.monthly
       jjj         diag.wrf.ocean.entry.timeseries.perform.jl          hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.annualday
       grads -bpc "diag.wrf.ocean.entry.timeseries.pervald             hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural"
       mkdir                                                           hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.plot
       mv hydat_0*png                                                  hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.plot
       jjj         diag.wrf.ocean.entry.timeseries.table.jl          3 hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.daily
       jjj         diag.wrf.ocean.entry.timeseries.table.jl          3 hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.monthly
       jjj         diag.wrf.ocean.entry.timeseries.table.jl          3 hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.annualday
       jjj         diag.wrf.ocean.entry.timeseries.table.jl          4 hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.daily
       jjj         diag.wrf.ocean.entry.timeseries.table.jl          4 hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.monthly
       jjj         diag.wrf.ocean.entry.timeseries.table.jl          4 hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.annualday
       lst *form? ; tail *form? | grep Rivers

# train station-specific "regulated" NNs that use the natural NN as input (try dense with no/elu activation functions)
# train using all-year, even-year, and odd-year data (all-year for applying to non-ERA forcing, even/odd for ERA evaluation)
wrkg ; cd hydat.validation
       jjj         diag.wrf.ocean.entry.station.neural.prepare.jl      hydat_valding_aall_1000_iccs_1990-01-01_2022-12-30.neural.nc flow train
       jjj         diag.wrf.ocean.entry.station.neural.prepare.jl      hydat_valding_aall_1000_ih45_1990-01-01_2022-12-30.neural.nc flow train
       jjj         diag.wrf.ocean.entry.station.neural.prepare.jl      hydat_valding_aall_1000_ih85_1990-01-01_2022-12-30.neural.nc flow train
       jjj         diag.wrf.ocean.entry.station.neural.prepare.jl      hydat_valding_aall_1000_impi_1990-01-01_2022-12-30.neural.nc flow train
       jjj         diag.wrf.ocean.entry.station.neural.prepare.jl      hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.nc flow train
       jjj         diag.wrf.ocean.entry.station.neural.train.jl        hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.nc flow all
       jjj         diag.wrf.ocean.entry.station.neural.train.jl        hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.nc flow eve
       jjj         diag.wrf.ocean.entry.station.neural.train.jl        hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.nc flow odd
       jjj         diag.wrf.ocean.entry.station.neural.train.jl        hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.nc flow eve elu
       jjj         diag.wrf.ocean.entry.station.neural.train.jl        hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.nc flow odd elu
       mkdir                                                           hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.neural
       mv *.jld *jldtxt                                                hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.neural

# train station-specific "regulated" NNs that use the natural NN as input (try dense with no/elu activation functions) as above, but
# assuming HYDAT and WRF-Hydro are not collocated, so first sort HYDAT and WRF-Hydro streamflow monotonically (small to large flow)
wrkg ; cd hydat.validation
       jjj         diag.wrf.ocean.entry.station.neural.train.sorted.jl hydat_valding_aall_1000_iccs_1990-01-01_2022-12-30.neural.nc flow all
       jjj         diag.wrf.ocean.entry.station.neural.train.sorted.jl hydat_valding_aall_1000_ih45_1990-01-01_2022-12-30.neural.nc flow all
       jjj         diag.wrf.ocean.entry.station.neural.train.sorted.jl hydat_valding_aall_1000_ih85_1990-01-01_2022-12-30.neural.nc flow all
       jjj         diag.wrf.ocean.entry.station.neural.train.sorted.jl hydat_valding_aall_1000_impi_1990-01-01_2022-12-30.neural.nc flow all
       jjj         diag.wrf.ocean.entry.station.neural.train.sorted.jl hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.nc flow all
       jjj         diag.wrf.ocean.entry.station.neural.train.sorted.jl hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.nc flow eve
       jjj         diag.wrf.ocean.entry.station.neural.train.sorted.jl hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.nc flow odd
       jjj         diag.wrf.ocean.entry.station.neural.train.sorted.jl hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.nc flow eve elu
       jjj         diag.wrf.ocean.entry.station.neural.train.sorted.jl hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.nc flow odd elu
       mkdir                                                           hydat_valding_aall_1000_iccs_1990-01-01_2022-12-30.neural.sorral
       mv *iccs*.jld *iccs*jldtxt                                      hydat_valding_aall_1000_iccs_1990-01-01_2022-12-30.neural.sorral
       mkdir                                                           hydat_valding_aall_1000_ih45_1990-01-01_2022-12-30.neural.sorral
       mv *ih45*.jld *ih45*jldtxt                                      hydat_valding_aall_1000_ih45_1990-01-01_2022-12-30.neural.sorral
       mkdir                                                           hydat_valding_aall_1000_ih85_1990-01-01_2022-12-30.neural.sorral
       mv *ih85*.jld *ih85*jldtxt                                      hydat_valding_aall_1000_ih85_1990-01-01_2022-12-30.neural.sorral
       mkdir                                                           hydat_valding_aall_1000_impi_1990-01-01_2022-12-30.neural.sorral
       mv *impi*.jld *impi*jldtxt                                      hydat_valding_aall_1000_impi_1990-01-01_2022-12-30.neural.sorral
       mkdir                                                           hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.sorral
       mv *incl*.jld *incl*jldtxt                                      hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.sorral

# using all-year training, apply to all-year data the station-specific "regulated" NNs that use the natural NN as input (only ~50 stations are of interest)
wrkg ; cd hydat.validation
       jjj         diag.wrf.ocean.entry.station.neural.apply.jl        hydat_valding_aall_1000_iccs_1990-01-01_2022-12-30.neural.nc flow all hydat_valding_aall_1000_iccs_1990-01-01_2022-12-30.neural.sorral
       jjj         diag.wrf.ocean.entry.station.neural.apply.jl        hydat_valding_aall_1000_ih45_1990-01-01_2022-12-30.neural.nc flow all hydat_valding_aall_1000_ih45_1990-01-01_2022-12-30.neural.sorral
       jjj         diag.wrf.ocean.entry.station.neural.apply.jl        hydat_valding_aall_1000_ih85_1990-01-01_2022-12-30.neural.nc flow all hydat_valding_aall_1000_ih85_1990-01-01_2022-12-30.neural.sorral
       jjj         diag.wrf.ocean.entry.station.neural.apply.jl        hydat_valding_aall_1000_impi_1990-01-01_2022-12-30.neural.nc flow all hydat_valding_aall_1000_impi_1990-01-01_2022-12-30.neural.sorral
       jjj         diag.wrf.ocean.entry.station.neural.apply.jl        hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.nc flow all hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.sorral
       jjj         diag.wrf.ocean.entry.timeseries.mean.assemble.jl    hydat_valding_aall_1000_iccs_1990-01-01_2022-12-30.neural.neuall.identity.nc
       jjj         diag.wrf.ocean.entry.timeseries.mean.assemble.jl    hydat_valding_aall_1000_ih45_1990-01-01_2022-12-30.neural.neuall.identity.nc
       jjj         diag.wrf.ocean.entry.timeseries.mean.assemble.jl    hydat_valding_aall_1000_ih85_1990-01-01_2022-12-30.neural.neuall.identity.nc
       jjj         diag.wrf.ocean.entry.timeseries.mean.assemble.jl    hydat_valding_aall_1000_impi_1990-01-01_2022-12-30.neural.neuall.identity.nc
       jjj         diag.wrf.ocean.entry.timeseries.mean.assemble.jl    hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.neuall.identity.nc
       jjj         diag.wrf.ocean.entry.timeseries.mean.assemble.jl    hydat_valding_aall_1000_iccs_1990-01-01_2022-12-30.neural.nc
       jjj         diag.wrf.ocean.entry.timeseries.mean.assemble.jl    hydat_valding_aall_1000_ih45_1990-01-01_2022-12-30.neural.nc
       jjj         diag.wrf.ocean.entry.timeseries.mean.assemble.jl    hydat_valding_aall_1000_ih85_1990-01-01_2022-12-30.neural.nc
       jjj         diag.wrf.ocean.entry.timeseries.mean.assemble.jl    hydat_valding_aall_1000_impi_1990-01-01_2022-12-30.neural.nc
       jjj         diag.wrf.ocean.entry.timeseries.mean.assemble.jl    hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.nc
       jjj         diag.wrf.ocean.entry.timeseries.mean.assemble.jl    hydat_valding_aall_1000_iccs_1990-01-01_2022-12-30.nc
       jjj         diag.wrf.ocean.entry.timeseries.mean.assemble.jl    hydat_valding_aall_1000_ih45_1990-01-01_2022-12-30.nc
       jjj         diag.wrf.ocean.entry.timeseries.mean.assemble.jl    hydat_valding_aall_1000_ih85_1990-01-01_2022-12-30.nc
       jjj         diag.wrf.ocean.entry.timeseries.mean.assemble.jl    hydat_valding_aall_1000_impi_1990-01-01_2022-12-30.nc
       jjj         diag.wrf.ocean.entry.timeseries.mean.assemble.jl    hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.nc

# using even-year training, apply to  odd-year data the station-specific "regulated" NNs that use the natural NN as input
# but first for reference, evaluate   odd-year data without the specific "regulated" NNs (i.e., using natural NN only)
wrkg ; cd hydat.validation
       jjj         diag.wrf.ocean.entry.timeseries.mean.assemble.jl    hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.nc      eve
       jjj         diag.wrf.ocean.entry.timeseries.perform.jl          hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neueve.daily
       jjj         diag.wrf.ocean.entry.timeseries.perform.jl          hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neueve.monthly
       jjj         diag.wrf.ocean.entry.timeseries.perform.jl          hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neueve.annualday
       jjj         diag.wrf.ocean.entry.timeseries.table.jl          3 hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neueve.daily
       jjj         diag.wrf.ocean.entry.timeseries.table.jl          3 hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neueve.monthly
       jjj         diag.wrf.ocean.entry.timeseries.table.jl          3 hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neueve.annualday
       jjj         diag.wrf.ocean.entry.timeseries.table.jl          4 hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neueve.daily
       jjj         diag.wrf.ocean.entry.timeseries.table.jl          4 hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neueve.monthly
       jjj         diag.wrf.ocean.entry.timeseries.table.jl          4 hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neueve.annualday
       jjj         diag.wrf.ocean.entry.station.neural.apply.jl        hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.nc flow eve hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.neural
       jjj         diag.wrf.ocean.entry.timeseries.mean.assemble.jl    hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.neueve.identity.nc
       jjj         diag.wrf.ocean.entry.timeseries.perform.jl          hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.neueve.identity.daily
       jjj         diag.wrf.ocean.entry.timeseries.perform.jl          hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.neueve.identity.monthly
       jjj         diag.wrf.ocean.entry.timeseries.perform.jl          hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.neueve.identity.annualday
       jjj         diag.wrf.ocean.entry.timeseries.table.jl          4 hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.neueve.identity.daily
       jjj         diag.wrf.ocean.entry.timeseries.table.jl          4 hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.neueve.identity.monthly
       jjj         diag.wrf.ocean.entry.timeseries.table.jl          4 hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.neueve.identity.annualday
       grads -bpc "diag.wrf.ocean.entry.timeseries.pervald             hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.neueve.identity"
       mkdir                                                           hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.neueve.identity.plot
       mv hydat_0*png                                                  hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.neueve.identity.plot
       jjj         diag.wrf.ocean.entry.station.neural.apply.jl        hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.nc flow eve hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.neural elu
       jjj         diag.wrf.ocean.entry.timeseries.mean.assemble.jl    hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.neueve.elu.nc
       jjj         diag.wrf.ocean.entry.timeseries.perform.jl          hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.neueve.elu.daily
       jjj         diag.wrf.ocean.entry.timeseries.perform.jl          hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.neueve.elu.monthly
       jjj         diag.wrf.ocean.entry.timeseries.perform.jl          hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.neueve.elu.annualday
       jjj         diag.wrf.ocean.entry.timeseries.table.jl          4 hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.neueve.elu.daily
       jjj         diag.wrf.ocean.entry.timeseries.table.jl          4 hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.neueve.elu.monthly
       jjj         diag.wrf.ocean.entry.timeseries.table.jl          4 hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.neueve.elu.annualday
       grads -bpc "diag.wrf.ocean.entry.timeseries.pervald             hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.neueve.elu"
       mkdir                                                           hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.neueve.elu.plot
       mv hydat_0*png                                                  hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.neueve.elu.plot
       lst *neueve*ann*form? ; tail *neueve*ann*form? | grep Rivers ; lst *neueve*mon*form? ; tail *neueve*mon*form? | grep Rivers ; lst *neueve*daily*form? ; tail *neueve*daily*form? | grep Rivers

# using even-year training, apply to  odd-year data the station-specific "regulated" NNs as above, but assuming uncollocated data
wrkg ; cd hydat.validation
       jjj         diag.wrf.ocean.entry.station.neural.apply.sorted.jl hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.nc flow eve hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.sorral
       jjj         diag.wrf.ocean.entry.timeseries.mean.assemble.jl    hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.soreve.identity.nc
       jjj         diag.wrf.ocean.entry.timeseries.perform.jl          hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.soreve.identity.daily
       jjj         diag.wrf.ocean.entry.timeseries.perform.jl          hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.soreve.identity.monthly
       jjj         diag.wrf.ocean.entry.timeseries.perform.jl          hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.soreve.identity.annualday
       jjj         diag.wrf.ocean.entry.timeseries.table.jl          4 hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.soreve.identity.daily
       jjj         diag.wrf.ocean.entry.timeseries.table.jl          4 hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.soreve.identity.monthly
       jjj         diag.wrf.ocean.entry.timeseries.table.jl          4 hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.soreve.identity.annualday
       grads -bpc "diag.wrf.ocean.entry.timeseries.pervald             hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.soreve.identity"
       mkdir                                                           hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.soreve.identity.plot
       mv hydat_0*png                                                  hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.soreve.identity.plot
       jjj         diag.wrf.ocean.entry.station.neural.apply.sorted.jl hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.nc flow eve hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.sorral elu
       jjj         diag.wrf.ocean.entry.timeseries.mean.assemble.jl    hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.soreve.elu.nc
       jjj         diag.wrf.ocean.entry.timeseries.perform.jl          hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.soreve.elu.daily
       jjj         diag.wrf.ocean.entry.timeseries.perform.jl          hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.soreve.elu.monthly
       jjj         diag.wrf.ocean.entry.timeseries.perform.jl          hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.soreve.elu.annualday
       jjj         diag.wrf.ocean.entry.timeseries.table.jl          4 hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.soreve.elu.daily
       jjj         diag.wrf.ocean.entry.timeseries.table.jl          4 hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.soreve.elu.monthly
       jjj         diag.wrf.ocean.entry.timeseries.table.jl          4 hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.soreve.elu.annualday
       grads -bpc "diag.wrf.ocean.entry.timeseries.pervald             hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.soreve.elu"
       mkdir                                                           hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.soreve.elu.plot
       mv hydat_0*png                                                  hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.soreve.elu.plot
       lst *soreve*ann*form? ; tail *soreve*ann*form? | grep Rivers ; lst *soreve*mon*form? ; tail *soreve*mon*form? | grep Rivers ; lst *soreve*daily*form? ; tail *soreve*daily*form? | grep Rivers

# using  odd-year training, apply to even-year data the station-specific "regulated" NNs that use the natural NN as input
# but first for reference, evaluate  even-year data without the specific "regulated" NNs (i.e., using natural NN only)
wrkg ; cd hydat.validation
       jjj         diag.wrf.ocean.entry.timeseries.mean.assemble.jl    hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.nc      odd
       jjj         diag.wrf.ocean.entry.timeseries.perform.jl          hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neuodd.daily
       jjj         diag.wrf.ocean.entry.timeseries.perform.jl          hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neuodd.monthly
       jjj         diag.wrf.ocean.entry.timeseries.perform.jl          hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neuodd.annualday
       jjj         diag.wrf.ocean.entry.timeseries.table.jl          3 hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neuodd.daily
       jjj         diag.wrf.ocean.entry.timeseries.table.jl          3 hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neuodd.monthly
       jjj         diag.wrf.ocean.entry.timeseries.table.jl          3 hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neuodd.annualday
       jjj         diag.wrf.ocean.entry.timeseries.table.jl          4 hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neuodd.daily
       jjj         diag.wrf.ocean.entry.timeseries.table.jl          4 hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neuodd.monthly
       jjj         diag.wrf.ocean.entry.timeseries.table.jl          4 hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neuodd.annualday
       jjj         diag.wrf.ocean.entry.station.neural.apply.jl        hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.nc flow odd hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.neural
       jjj         diag.wrf.ocean.entry.timeseries.mean.assemble.jl    hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.neuodd.identity.nc
       jjj         diag.wrf.ocean.entry.timeseries.perform.jl          hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.neuodd.identity.daily
       jjj         diag.wrf.ocean.entry.timeseries.perform.jl          hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.neuodd.identity.monthly
       jjj         diag.wrf.ocean.entry.timeseries.perform.jl          hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.neuodd.identity.annualday
       jjj         diag.wrf.ocean.entry.timeseries.table.jl          4 hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.neuodd.identity.daily
       jjj         diag.wrf.ocean.entry.timeseries.table.jl          4 hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.neuodd.identity.monthly
       jjj         diag.wrf.ocean.entry.timeseries.table.jl          4 hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.neuodd.identity.annualday
       grads -bpc "diag.wrf.ocean.entry.timeseries.pervald             hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.neuodd.identity"
       mkdir                                                           hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.neuodd.identity.plot
       mv hydat_0*png                                                  hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.neuodd.identity.plot
       jjj         diag.wrf.ocean.entry.station.neural.apply.jl        hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.nc flow odd hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.neural elu
       jjj         diag.wrf.ocean.entry.timeseries.mean.assemble.jl    hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.neuodd.elu.nc
       jjj         diag.wrf.ocean.entry.timeseries.perform.jl          hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.neuodd.elu.daily
       jjj         diag.wrf.ocean.entry.timeseries.perform.jl          hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.neuodd.elu.monthly
       jjj         diag.wrf.ocean.entry.timeseries.perform.jl          hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.neuodd.elu.annualday
       jjj         diag.wrf.ocean.entry.timeseries.table.jl          4 hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.neuodd.elu.daily
       jjj         diag.wrf.ocean.entry.timeseries.table.jl          4 hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.neuodd.elu.monthly
       jjj         diag.wrf.ocean.entry.timeseries.table.jl          4 hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.neuodd.elu.annualday
       grads -bpc "diag.wrf.ocean.entry.timeseries.pervald             hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.neuodd.elu"
       mkdir                                                           hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.neuodd.elu.plot
       mv hydat_0*png                                                  hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.neuodd.elu.plot
       lst *neuodd*ann*form? ; tail *neuodd*ann*form? | grep Rivers ; lst *neuodd*mon*form? ; tail *neuodd*mon*form? | grep Rivers ; lst *neuodd*dai*form? ; tail *neuodd*dai*form? | grep Rivers

# using  odd-year training, apply to even-year data the station-specific "regulated" NNs as above, but assuming uncollocated data
wrkg ; cd hydat.validation
       jjj         diag.wrf.ocean.entry.station.neural.apply.sorted.jl hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.nc flow odd hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.sorral
       jjj         diag.wrf.ocean.entry.timeseries.mean.assemble.jl    hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.sorodd.identity.nc
       jjj         diag.wrf.ocean.entry.timeseries.perform.jl          hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.sorodd.identity.daily
       jjj         diag.wrf.ocean.entry.timeseries.perform.jl          hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.sorodd.identity.monthly
       jjj         diag.wrf.ocean.entry.timeseries.perform.jl          hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.sorodd.identity.annualday
       jjj         diag.wrf.ocean.entry.timeseries.table.jl          4 hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.sorodd.identity.daily
       jjj         diag.wrf.ocean.entry.timeseries.table.jl          4 hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.sorodd.identity.monthly
       jjj         diag.wrf.ocean.entry.timeseries.table.jl          4 hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.sorodd.identity.annualday
       grads -bpc "diag.wrf.ocean.entry.timeseries.pervald             hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.sorodd.identity"
       mkdir                                                           hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.sorodd.identity.plot
       mv hydat_0*png                                                  hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.sorodd.identity.plot
       jjj         diag.wrf.ocean.entry.station.neural.apply.sorted.jl hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.nc flow odd hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.sorral elu
       jjj         diag.wrf.ocean.entry.timeseries.mean.assemble.jl    hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.sorodd.elu.nc
       jjj         diag.wrf.ocean.entry.timeseries.perform.jl          hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.sorodd.elu.daily
       jjj         diag.wrf.ocean.entry.timeseries.perform.jl          hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.sorodd.elu.monthly
       jjj         diag.wrf.ocean.entry.timeseries.perform.jl          hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.sorodd.elu.annualday
       jjj         diag.wrf.ocean.entry.timeseries.table.jl          4 hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.sorodd.elu.daily
       jjj         diag.wrf.ocean.entry.timeseries.table.jl          4 hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.sorodd.elu.monthly
       jjj         diag.wrf.ocean.entry.timeseries.table.jl          4 hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.sorodd.elu.annualday
       grads -bpc "diag.wrf.ocean.entry.timeseries.pervald             hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.sorodd.elu"
       mkdir                                                           hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.sorodd.elu.plot
       mv hydat_0*png                                                  hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.sorodd.elu.plot
       lst *sorodd*ann*form? ; tail *sorodd*ann*form? | grep Rivers ; lst *sorodd*mon*form? ; tail *sorodd*mon*form? | grep Rivers ; lst *sorodd*dai*form? ; tail *sorodd*dai*form? | grep Rivers

# using  all-year training, apply to  all-year data the station-specific "regulated" NNs that use the natural NN as input
wrkg ; cd hydat.validation
       jjj         diag.wrf.ocean.entry.station.neural.apply.jl        hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.nc flow all hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.neural
       jjj         diag.wrf.ocean.entry.timeseries.mean.assemble.jl    hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.neuall.identity.nc
       jjj         diag.wrf.ocean.entry.timeseries.perform.jl          hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.neuall.identity.daily
       jjj         diag.wrf.ocean.entry.timeseries.perform.jl          hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.neuall.identity.monthly
       jjj         diag.wrf.ocean.entry.timeseries.perform.jl          hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.neuall.identity.annualday
       jjj         diag.wrf.ocean.entry.timeseries.table.jl          4 hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.neuall.identity.daily
       jjj         diag.wrf.ocean.entry.timeseries.table.jl          4 hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.neuall.identity.monthly
       jjj         diag.wrf.ocean.entry.timeseries.table.jl          4 hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.neuall.identity.annualday
       grads -bpc "diag.wrf.ocean.entry.timeseries.pervald             hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.neuall.identity"
       mkdir                                                           hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.neuall.identity.plot
       mv hydat_0*png                                                  hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.neuall.identity.plot
#      jjj         diag.wrf.ocean.entry.station.neural.apply.jl        hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.nc flow all hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.neural elu
#      jjj         diag.wrf.ocean.entry.timeseries.mean.assemble.jl    hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.neuall.elu.nc
#      jjj         diag.wrf.ocean.entry.timeseries.perform.jl          hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.neuall.elu.daily
#      jjj         diag.wrf.ocean.entry.timeseries.perform.jl          hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.neuall.elu.monthly
#      jjj         diag.wrf.ocean.entry.timeseries.perform.jl          hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.neuall.elu.annualday
#      jjj         diag.wrf.ocean.entry.timeseries.table.jl          4 hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.neuall.elu.daily
#      jjj         diag.wrf.ocean.entry.timeseries.table.jl          4 hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.neuall.elu.monthly
#      jjj         diag.wrf.ocean.entry.timeseries.table.jl          4 hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.neuall.elu.annualday
#      grads -bpc "diag.wrf.ocean.entry.timeseries.pervald             hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.neuall.elu"
#      mkdir                                                           hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.neuall.elu.plot
#      mv hydat_0*png                                                  hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.neuall.elu.plot
       lst *neuall*ann*form? ; tail *neuall*ann*form? | grep Rivers ; lst *neuall*mon*form? ; tail *neuall*mon*form? | grep Rivers ; lst *neuall*dai*form? ; tail *neuall*dai*form? | grep Rivers

# using  all-year training, apply to  all-year data the station-specific "regulated" NNs as above, but assuming uncollocated data
wrkg ; cd hydat.validation
       jjj         diag.wrf.ocean.entry.station.neural.apply.sorted.jl hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.nc flow all hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.sorral
       jjj         diag.wrf.ocean.entry.timeseries.mean.assemble.jl    hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.sorall.identity.nc
       jjj         diag.wrf.ocean.entry.timeseries.perform.jl          hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.sorall.identity.daily
       jjj         diag.wrf.ocean.entry.timeseries.perform.jl          hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.sorall.identity.monthly
       jjj         diag.wrf.ocean.entry.timeseries.perform.jl          hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.sorall.identity.annualday
       jjj         diag.wrf.ocean.entry.timeseries.table.jl          4 hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.sorall.identity.daily
       jjj         diag.wrf.ocean.entry.timeseries.table.jl          4 hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.sorall.identity.monthly
       jjj         diag.wrf.ocean.entry.timeseries.table.jl          4 hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.sorall.identity.annualday
       grads -bpc "diag.wrf.ocean.entry.timeseries.pervald             hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.sorall.identity"
       mkdir                                                           hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.sorall.identity.plot
       mv hydat_0*png                                                  hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.sorall.identity.plot
       lst *sorall*ann*form? ; tail *sorall*ann*form? | grep Rivers ; lst *sorall*mon*form? ; tail *sorall*mon*form? | grep Rivers ; lst *sorall*dai*form? ; tail *sorall*dai*form? | grep Rivers

----------------------------------------------------------------------------------//

# identify stations from hydat.validation that are considered natural by HyDAT (with a couple of exceptions)
# and for each forcing, reproduce the training data files for this subset in a separate dir
wrkg ; cd hydat.validation
       jjj         diag.wrf.ocean.entry.station.valding.natural.jl     hydat_valding_aall 1000_iccs_1990-01-01_2022-12-30.neural hydat_valding_upstream_natural
       jjj         diag.wrf.ocean.entry.station.valding.natural.jl     hydat_valding_aall 1000_ih45_1990-01-01_2022-12-30.neural hydat_valding_upstream_natural
       jjj         diag.wrf.ocean.entry.station.valding.natural.jl     hydat_valding_aall 1000_ih85_1990-01-01_2022-12-30.neural hydat_valding_upstream_natural
       jjj         diag.wrf.ocean.entry.station.valding.natural.jl     hydat_valding_aall 1000_impi_1990-01-01_2022-12-30.neural hydat_valding_upstream_natural
       jjj         diag.wrf.ocean.entry.station.valding.natural.jl     hydat_valding_aall 1000_incl_1990-01-01_2022-12-30.neural hydat_valding_upstream_natural
wrkg ; cd hydat.validation/hydat_valding_upstream_natural
       ln -s ../../DOMAIN_full_full/Fulldom_hires_river.txt            Fulldom_hires_river.txt
       grads --quiet -blc "diag.wrf.ocean.entry.timeseries.valding     hydat_valding_anat_1000_incl_1990-01-01_2022-12-30.neural .sta.hwshed" ; di hydat_valding_anat_1000_incl_1990-01-01_2022-12-30.neural.sta.hwshed.png

# get baseline performance for these natural HyDAT stations (both eve and odd)
wrkg ; cd hydat.validation/hydat_valding_upstream_natural
       jjj         diag.wrf.ocean.entry.timeseries.mean.assemble.jl    hydat_valding_anat_1000_incl_1990-01-01_2022-12-30.neural.nc      eve
       jjj         diag.wrf.ocean.entry.timeseries.perform.jl          hydat_valding_anat_1000_incl_1990-01-01_2022-12-30.neueve.daily
       jjj         diag.wrf.ocean.entry.timeseries.perform.jl          hydat_valding_anat_1000_incl_1990-01-01_2022-12-30.neueve.monthly
       jjj         diag.wrf.ocean.entry.timeseries.perform.jl          hydat_valding_anat_1000_incl_1990-01-01_2022-12-30.neueve.annualday
       jjj         diag.wrf.ocean.entry.timeseries.table.jl          3 hydat_valding_anat_1000_incl_1990-01-01_2022-12-30.neueve.daily
       jjj         diag.wrf.ocean.entry.timeseries.table.jl          3 hydat_valding_anat_1000_incl_1990-01-01_2022-12-30.neueve.monthly
       jjj         diag.wrf.ocean.entry.timeseries.table.jl          3 hydat_valding_anat_1000_incl_1990-01-01_2022-12-30.neueve.annualday
       jjj         diag.wrf.ocean.entry.timeseries.table.jl          4 hydat_valding_anat_1000_incl_1990-01-01_2022-12-30.neueve.daily
       jjj         diag.wrf.ocean.entry.timeseries.table.jl          4 hydat_valding_anat_1000_incl_1990-01-01_2022-12-30.neueve.monthly
       jjj         diag.wrf.ocean.entry.timeseries.table.jl          4 hydat_valding_anat_1000_incl_1990-01-01_2022-12-30.neueve.annualday
       lst *neueve*ann*form? ; tail *neueve*ann*form? | grep Rivers ; lst *neueve*mon*form? ; tail *neueve*mon*form? | grep Rivers ; lst *neueve*daily*form? ; tail *neueve*daily*form? | grep Rivers
       jjj         diag.wrf.ocean.entry.timeseries.mean.assemble.jl    hydat_valding_anat_1000_incl_1990-01-01_2022-12-30.neural.nc      odd
       jjj         diag.wrf.ocean.entry.timeseries.perform.jl          hydat_valding_anat_1000_incl_1990-01-01_2022-12-30.neuodd.daily
       jjj         diag.wrf.ocean.entry.timeseries.perform.jl          hydat_valding_anat_1000_incl_1990-01-01_2022-12-30.neuodd.monthly
       jjj         diag.wrf.ocean.entry.timeseries.perform.jl          hydat_valding_anat_1000_incl_1990-01-01_2022-12-30.neuodd.annualday
       jjj         diag.wrf.ocean.entry.timeseries.table.jl          3 hydat_valding_anat_1000_incl_1990-01-01_2022-12-30.neuodd.daily
       jjj         diag.wrf.ocean.entry.timeseries.table.jl          3 hydat_valding_anat_1000_incl_1990-01-01_2022-12-30.neuodd.monthly
       jjj         diag.wrf.ocean.entry.timeseries.table.jl          3 hydat_valding_anat_1000_incl_1990-01-01_2022-12-30.neuodd.annualday
       jjj         diag.wrf.ocean.entry.timeseries.table.jl          4 hydat_valding_anat_1000_incl_1990-01-01_2022-12-30.neuodd.daily
       jjj         diag.wrf.ocean.entry.timeseries.table.jl          4 hydat_valding_anat_1000_incl_1990-01-01_2022-12-30.neuodd.monthly
       jjj         diag.wrf.ocean.entry.timeseries.table.jl          4 hydat_valding_anat_1000_incl_1990-01-01_2022-12-30.neuodd.annualday
       lst *neuodd*ann*form? ; tail *neuodd*ann*form? | grep Rivers ; lst *neuodd*mon*form? ; tail *neuodd*mon*form? | grep Rivers ; lst *neuodd*dai*form? ; tail *neuodd*dai*form? | grep Rivers

# train a single station-common "natural" NN that use the original natural NN as input (try dense with no/elu activation functions)
# and sort HYDAT and WRF-Hydro streamflow monotonically (small to large flow) when reading individual stations, assuming no collocation
wrkg ; cd hydat.validation/hydat_valding_upstream_natural
#      jjj         diag.wrf.ocean.entry.station.neural.train.sorted.multi.jl hydat_valding_anat_1000_iccs_1990-01-01_2022-12-30.neural.nc flow eve
#      jjj         diag.wrf.ocean.entry.station.neural.train.sorted.multi.jl hydat_valding_anat_1000_ih45_1990-01-01_2022-12-30.neural.nc flow eve
#      jjj         diag.wrf.ocean.entry.station.neural.train.sorted.multi.jl hydat_valding_anat_1000_ih85_1990-01-01_2022-12-30.neural.nc flow eve
#      jjj         diag.wrf.ocean.entry.station.neural.train.sorted.multi.jl hydat_valding_anat_1000_impi_1990-01-01_2022-12-30.neural.nc flow eve
       jjj         diag.wrf.ocean.entry.station.neural.train.sorted.multi.jl hydat_valding_anat_1000_incl_1990-01-01_2022-12-30.neural.nc flow eve
       jjj         diag.wrf.ocean.entry.station.neural.train.sorted.multi.jl hydat_valding_anat_1000_incl_1990-01-01_2022-12-30.neural.nc flow odd
       jjj         diag.wrf.ocean.entry.station.neural.train.sorted.multi.jl hydat_valding_anat_1000_incl_1990-01-01_2022-12-30.neural.nc flow eve elu
       jjj         diag.wrf.ocean.entry.station.neural.train.sorted.multi.jl hydat_valding_anat_1000_incl_1990-01-01_2022-12-30.neural.nc flow odd elu

# using eve/odd-year training, apply to odd/eve-year data the station-common "natural" NN that uses the natural NN as input, then check performance
wrkg ; cd hydat.validation/hydat_valding_upstream_natural
       jjj         diag.wrf.ocean.entry.station.neural.apply.sorted.multi.jl hydat_valding_anat_1000_incl_1990-01-01_2022-12-30.neural.nc flow eve hydat_valding_anat_1000_incl_1990-01-01_2022-12-30.neural.soreve.identity.all.jld
       jjj         diag.wrf.ocean.entry.timeseries.mean.assemble.jl          hydat_valding_anat_1000_incl_1990-01-01_2022-12-30.neural.soreve.identity.nc
       jjj         diag.wrf.ocean.entry.timeseries.perform.jl                hydat_valding_anat_1000_incl_1990-01-01_2022-12-30.neural.soreve.identity.daily
       jjj         diag.wrf.ocean.entry.timeseries.perform.jl                hydat_valding_anat_1000_incl_1990-01-01_2022-12-30.neural.soreve.identity.monthly
       jjj         diag.wrf.ocean.entry.timeseries.perform.jl                hydat_valding_anat_1000_incl_1990-01-01_2022-12-30.neural.soreve.identity.annualday
       jjj         diag.wrf.ocean.entry.timeseries.table.jl                3 hydat_valding_anat_1000_incl_1990-01-01_2022-12-30.neural.soreve.identity.daily
       jjj         diag.wrf.ocean.entry.timeseries.table.jl                3 hydat_valding_anat_1000_incl_1990-01-01_2022-12-30.neural.soreve.identity.monthly
       jjj         diag.wrf.ocean.entry.timeseries.table.jl                3 hydat_valding_anat_1000_incl_1990-01-01_2022-12-30.neural.soreve.identity.annualday
       jjj         diag.wrf.ocean.entry.timeseries.table.jl                4 hydat_valding_anat_1000_incl_1990-01-01_2022-12-30.neural.soreve.identity.daily
       jjj         diag.wrf.ocean.entry.timeseries.table.jl                4 hydat_valding_anat_1000_incl_1990-01-01_2022-12-30.neural.soreve.identity.monthly
       jjj         diag.wrf.ocean.entry.timeseries.table.jl                4 hydat_valding_anat_1000_incl_1990-01-01_2022-12-30.neural.soreve.identity.annualday
       lst *soreve*ann*form? ; tail *soreve*ann*form? | grep Rivers ; lst *soreve*mon*form? ; tail *soreve*mon*form? | grep Rivers ; lst *soreve*daily*form? ; tail *soreve*daily*form? | grep Rivers
       jjj         diag.wrf.ocean.entry.station.neural.apply.sorted.multi.jl hydat_valding_anat_1000_incl_1990-01-01_2022-12-30.neural.nc flow odd hydat_valding_anat_1000_incl_1990-01-01_2022-12-30.neural.sorodd.identity.all.jld
       jjj         diag.wrf.ocean.entry.timeseries.mean.assemble.jl          hydat_valding_anat_1000_incl_1990-01-01_2022-12-30.neural.sorodd.identity.nc
       jjj         diag.wrf.ocean.entry.timeseries.perform.jl                hydat_valding_anat_1000_incl_1990-01-01_2022-12-30.neural.sorodd.identity.daily
       jjj         diag.wrf.ocean.entry.timeseries.perform.jl                hydat_valding_anat_1000_incl_1990-01-01_2022-12-30.neural.sorodd.identity.monthly
       jjj         diag.wrf.ocean.entry.timeseries.perform.jl                hydat_valding_anat_1000_incl_1990-01-01_2022-12-30.neural.sorodd.identity.annualday
       jjj         diag.wrf.ocean.entry.timeseries.table.jl                3 hydat_valding_anat_1000_incl_1990-01-01_2022-12-30.neural.sorodd.identity.daily
       jjj         diag.wrf.ocean.entry.timeseries.table.jl                3 hydat_valding_anat_1000_incl_1990-01-01_2022-12-30.neural.sorodd.identity.monthly
       jjj         diag.wrf.ocean.entry.timeseries.table.jl                3 hydat_valding_anat_1000_incl_1990-01-01_2022-12-30.neural.sorodd.identity.annualday
       jjj         diag.wrf.ocean.entry.timeseries.table.jl                4 hydat_valding_anat_1000_incl_1990-01-01_2022-12-30.neural.sorodd.identity.daily
       jjj         diag.wrf.ocean.entry.timeseries.table.jl                4 hydat_valding_anat_1000_incl_1990-01-01_2022-12-30.neural.sorodd.identity.monthly
       jjj         diag.wrf.ocean.entry.timeseries.table.jl                4 hydat_valding_anat_1000_incl_1990-01-01_2022-12-30.neural.sorodd.identity.annualday
       lst *sorodd*ann*form? ; tail *sorodd*ann*form? | grep Rivers ; lst *sorodd*mon*form? ; tail *sorodd*mon*form? | grep Rivers ; lst *sorodd*dai*form? ; tail *sorodd*dai*form? | grep Rivers

----------------------------------------------------------------------------------//

# identify stations from hydat.validation that are close to pour points
# first create station files and for these points (notwithstanding their absence in HyDAT)
wrkg ; cd wrf_hydro_pour
       jjj unpack.hydat.station.pour.jl                                                                          ../SPINUP.wrf/run_DOMAIN_full_full_FORCING.wrf_1990-01-01_2004-12-30_3h_Severn_Hudson.txt
       ls -1 | grep -E '.sta$' | parallel --dry-run julia /home/riced/bin/unpack.hydat.station.catch.wrfhydro.jl ../DOMAIN_full_full/ > xswr ; nohup parallel -j 6 < xswr > xswr.xcom &
       jjj unpack.hydat.station.pour.hydat.jl ../hydat.validation/          hydat_valding_aall_1000_incl.txt hw  ../SPINUP.wrf/run_DOMAIN_full_full_FORCING.wrf_1990-01-01_2004-12-30_3h_Severn_Hudson.txt
               grep    "  1 \[" hydat_valding_aall_1000_incl_hwdt.txt >     hydat_valding_aall_1000_incl_hwdt.sta.hwshed.all
       ln -s                                                                                                     ../DOMAIN_full_full/Fulldom_hires_river.txt Fulldom_hires_river.txt
       grads --quiet -blc "diag.wrf.ocean.entry.timeseries.valding.assemble hydat_valding_aall_1000_incl_hwdt .sta.hwshed" ; di hydat_valding_aall_1000_incl_hwdt.sta.hwshed.png
       cp                                                                   hydat_valding_aall_1000_incl*        ../hydat.validation/

# prepare a subdir with various HyDAT station subsets
wrkg ; cd hydat.validation
       jjj         diag.wrf.ocean.entry.station.neural.apply.jl        hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.nc flow all     hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.neural
       jjj         diag.wrf.ocean.entry.station.neural.apply.jl        hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.nc flow eve     hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.neural
       jjj         diag.wrf.ocean.entry.station.neural.apply.jl        hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.nc flow odd     hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.neural
       jjj         diag.wrf.ocean.entry.station.neural.apply.sorted.jl hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.nc flow all     hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.sorral
       jjj         diag.wrf.ocean.entry.station.neural.apply.sorted.jl hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.nc flow eve     hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.sorral
       jjj         diag.wrf.ocean.entry.station.neural.apply.sorted.jl hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.nc flow odd     hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.sorral
       jjj         diag.wrf.ocean.entry.station.valding.jl             hydat_valding_aall_1000_incl 1990-01-01_2022-12-30.neural                 hydat_valding_upstream hydat_valding_aall_1000_incl_hwdt.txt
       jjj         diag.wrf.ocean.entry.station.valding.jl             hydat_valding_aall_1000_incl 1990-01-01_2022-12-30.neural.neuall.identity hydat_valding_upstream hydat_valding_aall_1000_incl_hwdt.txt
       jjj         diag.wrf.ocean.entry.station.valding.jl             hydat_valding_aall_1000_incl 1990-01-01_2022-12-30.neural.neueve.identity hydat_valding_upstream hydat_valding_aall_1000_incl_hwdt.txt
       jjj         diag.wrf.ocean.entry.station.valding.jl             hydat_valding_aall_1000_incl 1990-01-01_2022-12-30.neural.neuodd.identity hydat_valding_upstream hydat_valding_aall_1000_incl_hwdt.txt
       jjj         diag.wrf.ocean.entry.station.valding.jl             hydat_valding_aall_1000_incl 1990-01-01_2022-12-30.neural.sorall.identity hydat_valding_upstream hydat_valding_aall_1000_incl_hwdt.txt
       jjj         diag.wrf.ocean.entry.station.valding.jl             hydat_valding_aall_1000_incl 1990-01-01_2022-12-30.neural.soreve.identity hydat_valding_upstream hydat_valding_aall_1000_incl_hwdt.txt
       jjj         diag.wrf.ocean.entry.station.valding.jl             hydat_valding_aall_1000_incl 1990-01-01_2022-12-30.neural.sorodd.identity hydat_valding_upstream hydat_valding_aall_1000_incl_hwdt.txt

# get baseline raw and NN1 performance for pour-point HyDAT stations (all, eve, and odd years)
wrkg ; cd hydat.validation/hydat_valding_upstream
       jjj         diag.wrf.ocean.entry.timeseries.mean.assemble.jl    hydat_valding_aall_1000_uref_1990-01-01_2022-12-30.neural.nc
       jjj         diag.wrf.ocean.entry.timeseries.perform.jl          hydat_valding_aall_1000_uref_1990-01-01_2022-12-30.neural.daily
       jjj         diag.wrf.ocean.entry.timeseries.perform.jl          hydat_valding_aall_1000_uref_1990-01-01_2022-12-30.neural.monthly
       jjj         diag.wrf.ocean.entry.timeseries.perform.jl          hydat_valding_aall_1000_uref_1990-01-01_2022-12-30.neural.annualday
       jjj         diag.wrf.ocean.entry.timeseries.table.jl          3 hydat_valding_aall_1000_uref_1990-01-01_2022-12-30.neural.daily
       jjj         diag.wrf.ocean.entry.timeseries.table.jl          3 hydat_valding_aall_1000_uref_1990-01-01_2022-12-30.neural.monthly
       jjj         diag.wrf.ocean.entry.timeseries.table.jl          3 hydat_valding_aall_1000_uref_1990-01-01_2022-12-30.neural.annualday
       jjj         diag.wrf.ocean.entry.timeseries.table.jl          4 hydat_valding_aall_1000_uref_1990-01-01_2022-12-30.neural.daily
       jjj         diag.wrf.ocean.entry.timeseries.table.jl          4 hydat_valding_aall_1000_uref_1990-01-01_2022-12-30.neural.monthly
       jjj         diag.wrf.ocean.entry.timeseries.table.jl          4 hydat_valding_aall_1000_uref_1990-01-01_2022-12-30.neural.annualday
       lst *neural*ann*form? ; tail *neural*ann*form? | grep Rivers ; lst *neural*mon*form? ; tail *neural*mon*form? | grep Rivers ; lst *neural*dai*form? ; tail *neural*dai*form? | grep Rivers
       jjj         diag.wrf.ocean.entry.timeseries.mean.assemble.jl    hydat_valding_aall_1000_uref_1990-01-01_2022-12-30.neural.nc      eve
       jjj         diag.wrf.ocean.entry.timeseries.perform.jl          hydat_valding_aall_1000_uref_1990-01-01_2022-12-30.neueve.daily
       jjj         diag.wrf.ocean.entry.timeseries.perform.jl          hydat_valding_aall_1000_uref_1990-01-01_2022-12-30.neueve.monthly
       jjj         diag.wrf.ocean.entry.timeseries.perform.jl          hydat_valding_aall_1000_uref_1990-01-01_2022-12-30.neueve.annualday
       jjj         diag.wrf.ocean.entry.timeseries.table.jl          3 hydat_valding_aall_1000_uref_1990-01-01_2022-12-30.neueve.daily
       jjj         diag.wrf.ocean.entry.timeseries.table.jl          3 hydat_valding_aall_1000_uref_1990-01-01_2022-12-30.neueve.monthly
       jjj         diag.wrf.ocean.entry.timeseries.table.jl          3 hydat_valding_aall_1000_uref_1990-01-01_2022-12-30.neueve.annualday
       jjj         diag.wrf.ocean.entry.timeseries.table.jl          4 hydat_valding_aall_1000_uref_1990-01-01_2022-12-30.neueve.daily
       jjj         diag.wrf.ocean.entry.timeseries.table.jl          4 hydat_valding_aall_1000_uref_1990-01-01_2022-12-30.neueve.monthly
       jjj         diag.wrf.ocean.entry.timeseries.table.jl          4 hydat_valding_aall_1000_uref_1990-01-01_2022-12-30.neueve.annualday
       lst *neueve*ann*form? ; tail *neueve*ann*form? | grep Rivers ; lst *neueve*mon*form? ; tail *neueve*mon*form? | grep Rivers ; lst *neueve*dai*form? ; tail *neueve*dai*form? | grep Rivers
       jjj         diag.wrf.ocean.entry.timeseries.mean.assemble.jl    hydat_valding_aall_1000_uref_1990-01-01_2022-12-30.neural.nc      odd
       jjj         diag.wrf.ocean.entry.timeseries.perform.jl          hydat_valding_aall_1000_uref_1990-01-01_2022-12-30.neuodd.daily
       jjj         diag.wrf.ocean.entry.timeseries.perform.jl          hydat_valding_aall_1000_uref_1990-01-01_2022-12-30.neuodd.monthly
       jjj         diag.wrf.ocean.entry.timeseries.perform.jl          hydat_valding_aall_1000_uref_1990-01-01_2022-12-30.neuodd.annualday
       jjj         diag.wrf.ocean.entry.timeseries.table.jl          3 hydat_valding_aall_1000_uref_1990-01-01_2022-12-30.neuodd.daily
       jjj         diag.wrf.ocean.entry.timeseries.table.jl          3 hydat_valding_aall_1000_uref_1990-01-01_2022-12-30.neuodd.monthly
       jjj         diag.wrf.ocean.entry.timeseries.table.jl          3 hydat_valding_aall_1000_uref_1990-01-01_2022-12-30.neuodd.annualday
       jjj         diag.wrf.ocean.entry.timeseries.table.jl          4 hydat_valding_aall_1000_uref_1990-01-01_2022-12-30.neuodd.daily
       jjj         diag.wrf.ocean.entry.timeseries.table.jl          4 hydat_valding_aall_1000_uref_1990-01-01_2022-12-30.neuodd.monthly
       jjj         diag.wrf.ocean.entry.timeseries.table.jl          4 hydat_valding_aall_1000_uref_1990-01-01_2022-12-30.neuodd.annualday
       lst *neuodd*ann*form? ; tail *neuodd*ann*form? | grep Rivers ; lst *neuodd*mon*form? ; tail *neuodd*mon*form? | grep Rivers ; lst *neuodd*dai*form? ; tail *neuodd*dai*form? | grep Rivers

# get sorted and unsorted NN2 performance for pour-point HyDAT stations (both eve and odd)

# get sorted and unsorted NN2 performance for pour-point HyDAT stations (all stations)
wrkg ; cd hydat.validation/hydat_valding_upstream
       jjj         diag.wrf.ocean.entry.timeseries.mean.assemble.jl    hydat_valding_aall_1000_uref_1990-01-01_2022-12-30.neural.neuall.identity.nc
       jjj         diag.wrf.ocean.entry.timeseries.perform.jl          hydat_valding_aall_1000_uref_1990-01-01_2022-12-30.neural.neuall.identity.daily
       jjj         diag.wrf.ocean.entry.timeseries.perform.jl          hydat_valding_aall_1000_uref_1990-01-01_2022-12-30.neural.neuall.identity.monthly
       jjj         diag.wrf.ocean.entry.timeseries.perform.jl          hydat_valding_aall_1000_uref_1990-01-01_2022-12-30.neural.neuall.identity.annualday
       jjj         diag.wrf.ocean.entry.timeseries.table.jl          4 hydat_valding_aall_1000_uref_1990-01-01_2022-12-30.neural.neuall.identity.daily
       jjj         diag.wrf.ocean.entry.timeseries.table.jl          4 hydat_valding_aall_1000_uref_1990-01-01_2022-12-30.neural.neuall.identity.monthly
       jjj         diag.wrf.ocean.entry.timeseries.table.jl          4 hydat_valding_aall_1000_uref_1990-01-01_2022-12-30.neural.neuall.identity.annualday
       grads -bpc "diag.wrf.ocean.entry.timeseries.pervald             hydat_valding_aall_1000_uref_1990-01-01_2022-12-30.neural.neuall.identity"
       mkdir                                                           hydat_valding_aall_1000_uref_1990-01-01_2022-12-30.neural.neuall.identity.plot
       mv hydat_0*png                                                  hydat_valding_aall_1000_uref_1990-01-01_2022-12-30.neural.neuall.identity.plot
       lst *neuall*ann*form? ; tail *neuall*ann*form? | grep Rivers ; lst *neuall*mon*form? ; tail *neuall*mon*form? | grep Rivers ; lst *neuall*dai*form? ; tail *neuall*dai*form? | grep Rivers
       ln -s ../../DOMAIN_full_full/Fulldom_hires_river.txt Fulldom_hires_river.txt
       grads --quiet -blc "diag.wrf.ocean.entry.timeseries.valding     hydat_valding_aall_1000_uref_1990-01-01_2022-12-30.neural.neuall.identity .sta.hwshed"
wrkg ; cd hydat.validation/hydat_valding_upstream
       jjj         diag.wrf.ocean.entry.timeseries.mean.assemble.jl    hydat_valding_aall_1000_uref_1990-01-01_2022-12-30.neural.sorall.identity.nc
       jjj         diag.wrf.ocean.entry.timeseries.perform.jl          hydat_valding_aall_1000_uref_1990-01-01_2022-12-30.neural.sorall.identity.daily
       jjj         diag.wrf.ocean.entry.timeseries.perform.jl          hydat_valding_aall_1000_uref_1990-01-01_2022-12-30.neural.sorall.identity.monthly
       jjj         diag.wrf.ocean.entry.timeseries.perform.jl          hydat_valding_aall_1000_uref_1990-01-01_2022-12-30.neural.sorall.identity.annualday
       jjj         diag.wrf.ocean.entry.timeseries.table.jl          4 hydat_valding_aall_1000_uref_1990-01-01_2022-12-30.neural.sorall.identity.daily
       jjj         diag.wrf.ocean.entry.timeseries.table.jl          4 hydat_valding_aall_1000_uref_1990-01-01_2022-12-30.neural.sorall.identity.monthly
       jjj         diag.wrf.ocean.entry.timeseries.table.jl          4 hydat_valding_aall_1000_uref_1990-01-01_2022-12-30.neural.sorall.identity.annualday
       grads -bpc "diag.wrf.ocean.entry.timeseries.pervald             hydat_valding_aall_1000_uref_1990-01-01_2022-12-30.neural.sorall.identity"
       mkdir                                                           hydat_valding_aall_1000_uref_1990-01-01_2022-12-30.neural.sorall.identity.plot
       mv hydat_0*png                                                  hydat_valding_aall_1000_uref_1990-01-01_2022-12-30.neural.sorall.identity.plot
       lst *sorall*ann*form? ; tail *sorall*ann*form? | grep Rivers ; lst *sorall*mon*form? ; tail *sorall*mon*form? | grep Rivers ; lst *sorall*dai*form? ; tail *sorall*dai*form? | grep Rivers

# assess even and odd year performance of sorted training for stations close to pour points
wrkg ; cd hydat.validation/hydat_valding_upstream
       jjj         diag.wrf.ocean.entry.timeseries.mean.assemble.jl    hydat_valding_aall_1000_uref_1990-01-01_2022-12-30.neural.soreve.identity.nc
       jjj         diag.wrf.ocean.entry.timeseries.perform.jl          hydat_valding_aall_1000_uref_1990-01-01_2022-12-30.neural.soreve.identity.daily
       jjj         diag.wrf.ocean.entry.timeseries.perform.jl          hydat_valding_aall_1000_uref_1990-01-01_2022-12-30.neural.soreve.identity.monthly
       jjj         diag.wrf.ocean.entry.timeseries.perform.jl          hydat_valding_aall_1000_uref_1990-01-01_2022-12-30.neural.soreve.identity.annualday
       jjj         diag.wrf.ocean.entry.timeseries.table.jl          3 hydat_valding_aall_1000_uref_1990-01-01_2022-12-30.neural.soreve.identity.daily
       jjj         diag.wrf.ocean.entry.timeseries.table.jl          3 hydat_valding_aall_1000_uref_1990-01-01_2022-12-30.neural.soreve.identity.monthly
       jjj         diag.wrf.ocean.entry.timeseries.table.jl          3 hydat_valding_aall_1000_uref_1990-01-01_2022-12-30.neural.soreve.identity.annualday
       jjj         diag.wrf.ocean.entry.timeseries.table.jl          4 hydat_valding_aall_1000_uref_1990-01-01_2022-12-30.neural.soreve.identity.daily
       jjj         diag.wrf.ocean.entry.timeseries.table.jl          4 hydat_valding_aall_1000_uref_1990-01-01_2022-12-30.neural.soreve.identity.monthly
       jjj         diag.wrf.ocean.entry.timeseries.table.jl          4 hydat_valding_aall_1000_uref_1990-01-01_2022-12-30.neural.soreve.identity.annualday
       lst *soreve*ann*form? ; tail *soreve*ann*form? | grep Rivers ; lst *soreve*mon*form? ; tail *soreve*mon*form? | grep Rivers ; lst *soreve*daily*form? ; tail *soreve*daily*form? | grep Rivers
       jjj         diag.wrf.ocean.entry.timeseries.mean.assemble.jl    hydat_valding_aall_1000_uref_1990-01-01_2022-12-30.neural.sorodd.identity.nc
       jjj         diag.wrf.ocean.entry.timeseries.perform.jl          hydat_valding_aall_1000_uref_1990-01-01_2022-12-30.neural.sorodd.identity.daily
       jjj         diag.wrf.ocean.entry.timeseries.perform.jl          hydat_valding_aall_1000_uref_1990-01-01_2022-12-30.neural.sorodd.identity.monthly
       jjj         diag.wrf.ocean.entry.timeseries.perform.jl          hydat_valding_aall_1000_uref_1990-01-01_2022-12-30.neural.sorodd.identity.annualday
       jjj         diag.wrf.ocean.entry.timeseries.table.jl          3 hydat_valding_aall_1000_uref_1990-01-01_2022-12-30.neural.sorodd.identity.daily
       jjj         diag.wrf.ocean.entry.timeseries.table.jl          3 hydat_valding_aall_1000_uref_1990-01-01_2022-12-30.neural.sorodd.identity.monthly
       jjj         diag.wrf.ocean.entry.timeseries.table.jl          3 hydat_valding_aall_1000_uref_1990-01-01_2022-12-30.neural.sorodd.identity.annualday
       jjj         diag.wrf.ocean.entry.timeseries.table.jl          4 hydat_valding_aall_1000_uref_1990-01-01_2022-12-30.neural.sorodd.identity.daily
       jjj         diag.wrf.ocean.entry.timeseries.table.jl          4 hydat_valding_aall_1000_uref_1990-01-01_2022-12-30.neural.sorodd.identity.monthly
       jjj         diag.wrf.ocean.entry.timeseries.table.jl          4 hydat_valding_aall_1000_uref_1990-01-01_2022-12-30.neural.sorodd.identity.annualday
       lst *sorodd*ann*form? ; tail *sorodd*ann*form? | grep Rivers ; lst *sorodd*mon*form? ; tail *sorodd*mon*form? | grep Rivers ; lst *sorodd*dai*form? ; tail *sorodd*dai*form? | grep Rivers

----------------------------------------------------------------------------------//

# prepare features for all pour points and use the station-specific NNs to adjust where possible
wrkg ; cd SPINUP.ccs
       jjj         diag.wrf.ocean.entry.station.neural.prepare.jl      run_DOMAIN_full_full_FORCING.ccs_1990-01-01_2099-12-30_3h_Severn_Hudson.neural.nc riverflo
wrkg ; cd SPINUP.h45
       jjj         diag.wrf.ocean.entry.station.neural.prepare.jl      run_DOMAIN_full_full_FORCING.h45_1990-01-01_2099-12-30_3h_Severn_Hudson.neural.nc riverflo
wrkg ; cd SPINUP.h85
       jjj         diag.wrf.ocean.entry.station.neural.prepare.jl      run_DOMAIN_full_full_FORCING.h85_1990-01-01_2099-12-30_3h_Severn_Hudson.neural.nc riverflo
wrkg ; cd SPINUP.wrf
       jjj         diag.wrf.ocean.entry.station.neural.prepare.jl      run_DOMAIN_full_full_FORCING.w85_1990-01-01_2099-12-30_3h_Severn_Hudson.neural.nc riverflo
wrkg ; cd hydat.validation
       jjj         diag.wrf.ocean.entry.station.neural.apply.sort51.jl ../SPINUP.ccs/run_DOMAIN_full_full_FORCING.ccs_1990-01-01_2099-12-30_3h_Severn_Hudson.neural.nc riverflo all hydat_valding_aall_1000_iccs_1990-01-01_2022-12-30.neural.sorral hydat_valding_aall_1000_incl_hwdt.sta.hwshed.all
       jjj         diag.wrf.ocean.entry.station.neural.apply.sort51.jl ../SPINUP.h45/run_DOMAIN_full_full_FORCING.h45_1990-01-01_2099-12-30_3h_Severn_Hudson.neural.nc riverflo all hydat_valding_aall_1000_ih45_1990-01-01_2022-12-30.neural.sorral hydat_valding_aall_1000_incl_hwdt.sta.hwshed.all
       jjj         diag.wrf.ocean.entry.station.neural.apply.sort51.jl ../SPINUP.h85/run_DOMAIN_full_full_FORCING.h85_1990-01-01_2099-12-30_3h_Severn_Hudson.neural.nc riverflo all hydat_valding_aall_1000_ih85_1990-01-01_2022-12-30.neural.sorral hydat_valding_aall_1000_incl_hwdt.sta.hwshed.all
       jjj         diag.wrf.ocean.entry.station.neural.apply.sort51.jl ../SPINUP.wrf/run_DOMAIN_full_full_FORCING.w85_1990-01-01_2099-12-30_3h_Severn_Hudson.neural.nc riverflo all hydat_valding_aall_1000_impi_1990-01-01_2022-12-30.neural.sorral hydat_valding_aall_1000_incl_hwdt.sta.hwshed.all

# sum all pour points and plot the decadal trends
wrkg ; cd SPINUP.ccs
       jjj diag.wrf.ocean.entry.timeseries.grid.multi.mean.jl   run_DOMAIN_full_full_FORCING.ccs_1990-01-01_2099-12-30_3h_Severn_Hudson.neural.sorall.identity.nc
       grads -bpc "diag.wrf.ocean.entry.timeseries.grid.multi.mean  DOMAIN_full_full_FORCING.ccs_1990-01-01_2099-12-30_3h_Severn_Hudson.neural.sorall.identity"
wrkg ; cd SPINUP.h45
       jjj diag.wrf.ocean.entry.timeseries.grid.multi.mean.jl   run_DOMAIN_full_full_FORCING.h45_1990-01-01_2099-12-30_3h_Severn_Hudson.neural.sorall.identity.nc
       grads -bpc "diag.wrf.ocean.entry.timeseries.grid.multi.mean  DOMAIN_full_full_FORCING.h45_1990-01-01_2099-12-30_3h_Severn_Hudson.neural.sorall.identity"
wrkg ; cd SPINUP.h85
       jjj diag.wrf.ocean.entry.timeseries.grid.multi.mean.jl   run_DOMAIN_full_full_FORCING.h85_1990-01-01_2099-12-30_3h_Severn_Hudson.neural.sorall.identity.nc
       grads -bpc "diag.wrf.ocean.entry.timeseries.grid.multi.mean  DOMAIN_full_full_FORCING.h85_1990-01-01_2099-12-30_3h_Severn_Hudson.neural.sorall.identity"
wrkg ; cd SPINUP.wrf
       jjj diag.wrf.ocean.entry.timeseries.grid.multi.mean.jl   run_DOMAIN_full_full_FORCING.w85_1990-01-01_2099-12-30_3h_Severn_Hudson.neural.sorall.identity.nc
       grads -bpc "diag.wrf.ocean.entry.timeseries.grid.multi.mean  DOMAIN_full_full_FORCING.w85_1990-01-01_2099-12-30_3h_Severn_Hudson.neural.sorall.identity"

# estimate variance in the four CMIP models (by mean variance at all times)
wrkg ; jjj diag.wrf.ocean.entry.timeseries.grid.multi.variance.jl .nc
       jjj diag.wrf.ocean.entry.timeseries.grid.multi.variance.jl .neural.nc
       jjj diag.wrf.ocean.entry.timeseries.grid.multi.variance.jl .neural.sorall.identity.nc

----------------------------------------------------------------------------------//

# plot raw, NN1, NN2 performance for pour-point HyDAT stations (all stations)
wrkg ; cd hydat.validation/hydat_valding_upstream
       grads -bpc "diag.wrf.ocean.entry.timeseries.cmipval hydat_valding_aall_1000_ _1990-01-01_2022-12-30 neural neuall.identity"
wrkg ; cd hydat.validation
       grads -bpc "diag.wrf.ocean.entry.timeseries.cmipall run_DOMAIN_full_full_FORCING 1990-01-01_2099-12-30_3h_Severn_Hudson"
       grads -bpc "diag.wrf.ocean.entry.timeseries.cmipall run_DOMAIN_full_full_FORCING 1990-01-01_2099-12-30_3h_Severn_Hudson.neural"
       grads -bpc "diag.wrf.ocean.entry.timeseries.cmipall run_DOMAIN_full_full_FORCING 1990-01-01_2099-12-30_3h_Severn_Hudson.neural.sorall.identity"

----------------------------------------------------------------------------------//
----------------------------------------------------------------------------------//
----------------------------------------------------------------------------------//
----------------------------------------------------------------------------------//

# recreate a station data subset from the latest HyDAT database
# (from https://collaboration.cmc.ec.gc.ca/cmc/hydrometrics/www)

wrkg ; cd hydat
       jjj diag.wrf.ocean.entry.timeseries.select.jl zhydat.subset zhydat.latest
       rm  zhydat.latest/hydat_* ;               csh zhydat.subset
       cd  zhydat.latest
#      wget https://collaboration.cmc.ec.gc.ca/cmc/hydrometrics/www/Hydat_sqlite3_20231120.zip
       unzip                                                        Hydat_sqlite3_20231120.zip
       cat exec
       jjj unpack.hydat.station.jl Hydat.sqlite3 01AP005 01AQ001 01AQ003 01AQ004 01AQ006
       rm                          Hydat.sqlite3

----------------------------------------------------------------------------------//

# perform a 30-year uncalibrated ERA simulation, starting from a mean ERA forcing
# using My ; datesous("1990-01-01", "2004-12-31", "dy") = 5478 ; datesous("2010-07-25", "2021-01-01", "dy") = 3813
# link dat and res to all restart subdirs, sync cmc and cca, and sum the river outflow to the North Atlantic
wrkg ; cd CALUN.era
       jjj exec.wrf.hydro.calun.jl             DOMAIN_full_full FORCING.era      2019-05-31  153
       jjj exec.wrf.hydro.calun.jl             DOMAIN_full_full FORCING.era      2017-10-01 1461
       jjj exec.wrf.hydro.calun.jl             DOMAIN_full_full FORCING.era      1990-01-01 5478
       jjj exec.wrf.hydro.calun.jl             DOMAIN_full_full FORCING.era      1992-11-20 4424 run_DOMAIN_full_full_FORCING.era_1990-01-01_005478
       jjj exec.wrf.hydro.calun.jl             DOMAIN_full_full FORCING.era      1995-11-10 3339 run_DOMAIN_full_full_FORCING.era_1992-11-20_004424.res
       jjj exec.wrf.hydro.calun.jl             DOMAIN_full_full FORCING.era      1998-11-20 2233 run_DOMAIN_full_full_FORCING.era_1995-11-10_003339.res
       jjj exec.wrf.hydro.calun.jl             DOMAIN_full_full FORCING.era      2004-06-10  204 run_DOMAIN_full_full_FORCING.era_1998-11-20_002233.res
       jjj exec.wrf.hydro.calun.jl             DOMAIN_full_full FORCING.era      2004-12-30 5846 run_DOMAIN_full_full_FORCING.era_2004-06-10_000204.res
       jjj exec.wrf.hydro.calun.jl             DOMAIN_full_full FORCING.era      2010-07-20 3813 run_DOMAIN_full_full_FORCING.era_2004-12-30_005846.res
       jjj exec.wrf.hydro.calun.jl             DOMAIN_full_full FORCING.era      2016-02-15 1782 run_DOMAIN_full_full_FORCING.era_2010-07-20_003813.res
#      jjj exec.wrf.hydro.calun.jl             DOMAIN_0000_0000 FORCING.wrf.mcal 1903-01-01  365 run_DOMAIN_0000_0000_FORCING.wrf.mcal_1901-01-01_001095
#      jjj diag.wrf.datres.mean.links.jl 12 run_DOMAIN_0000_0000_FORCING.wrf.mcal_1901-01-01_001095
#      jjj diag.wrf.ocean.entry.timeseries.total.jl streamflow 1901010200 00.CHRTOUT_GRID1 1094 ../Fulldom_hires_outlets.txt
#      mv streamflow.190101020000.CHRTOUT_GRID1.1094.nc ../streamflow.190101020000.CHRTOUT_GRID1.1094.wrfcal.nc
#      rsync -avz SPINUP.wrf riced@graham.computecanada.ca:/scratch/riced/workg
#      rsync -avz            riced@graham.computecanada.ca:/scratch/riced/workg/SPINUP.wrf .
#rkg ; grads -blc "diag.wrf.ocean.entry.timeseries.total streamflow.190101020000.CHRTOUT_GRID1.1094" ; di streamflow.190101020000.CHRTOUT_GRID1.1094.wrfcal.png
#      convert -trim streamflow.190101020000.CHRTOUT_GRID1.1094.wrfcal.png xx.png ;             mv xx.png streamflow.190101020000.CHRTOUT_GRID1.1094.wrfcal.png

# link dat and res to all restart subdirs and extract a streamflow timeseries for all river
# outlet locations from WRF Hydro output files and save this as a separate netcdf grid file
wrkg ; cd CALUN.era
       mkdir dat ; cd dat
       jjj diag.wrf.ocean.entry.links.jl ../run_DOMAIN_full_full_FORCING.era_1990-01-01_005478/
       jjj diag.wrf.ocean.entry.links.jl ../run_DOMAIN_full_full_FORCING.era_1992-11-20_004424.res/
       jjj diag.wrf.ocean.entry.links.jl ../run_DOMAIN_full_full_FORCING.era_1995-11-10_003339.res/
       jjj diag.wrf.ocean.entry.links.jl ../run_DOMAIN_full_full_FORCING.era_1998-11-20_002233.res/
       jjj diag.wrf.ocean.entry.links.jl ../run_DOMAIN_full_full_FORCING.era_2004-06-10_000204.res/
       jjj diag.wrf.ocean.entry.links.jl ../run_DOMAIN_full_full_FORCING.era_2004-12-30_005846.res/
       jjj diag.wrf.ocean.entry.links.jl ../run_DOMAIN_full_full_FORCING.era_2010-07-20_003813.res/
       jjj diag.wrf.ocean.entry.links.jl ../run_DOMAIN_full_full_FORCING.era_2016-02-15_001782.res/
wrkg ; cd CALUN.era
       jjj diag.wrf.ocean.entry.timeseries.grid.jl 1990010106 45291 ../merit/Fulldom_hires_outlets.txt
       jjj diag.wrf.ocean.entry.timeseries.grid.monthly.mean.jl WRF-Hydro_v5.2_ERA5_MERIT-Eilander_outlet_streamflow_1990010106_2020123118_6h.nc

----------------------------------------------------------------------------------//

# perform a series of short (one-month) calibration runs starting from an ERA spinup restart
wrkg ; cd CALUN.era
       jjj exec.wrf.hydro.calibrate.pest.jl     DOMAIN$DOMZ   FORCING.era 2020-10-01 1095
       jjj exec.wrf.hydro.calibrate.pest.jl     DOMAIN$DOMZ   FORCING.wrf 2020-10-01  365 run_DOMAIN$DOMZ""_FORCING.wrf_1901-01-01_001095
       jjj diag.wrf.datres.mean.links.jl                                               12 run_DOMAIN$DOMZ""_FORCING.wrf_1901-01-01_001095
       jjj diag.wrf.ocean.entry.timeseries.total.jl streamflow 1901010200 00.CHRTOUT_GRID1 1094 ../Fulldom_hires_outlets.txt
       mv streamflow.190101020000.CHRTOUT_GRID1.1094.nc ../streamflow.190101020000.CHRTOUT_GRID1.1094.wrfcal.nc
       rsync -avz SPINUP.wrf riced@graham.computecanada.ca:/scratch/riced/workg
       rsync -avz            riced@graham.computecanada.ca:/scratch/riced/workg/SPINUP.wrf .
wrkg ; grads -blc "diag.wrf.ocean.entry.timeseries.total streamflow.190101020000.CHRTOUT_GRID1.1094" ; di streamflow.190101020000.CHRTOUT_GRID1.1094.wrfcal.png
       convert -trim streamflow.190101020000.CHRTOUT_GRID1.1094.wrfcal.png xx.png ;             mv xx.png streamflow.190101020000.CHRTOUT_GRID1.1094.wrfcal.png

----------------------------------------------------------------------------------//

       jjj unpack.hydat.station.jl Hydat.sqlite3 03JB004 AuxFeuilles
       jjj unpack.hydat.station.jl Hydat.sqlite3         Caniapiscau
       jjj unpack.hydat.station.jl Hydat.sqlite3 03MB002 ALaBaleine
       jjj unpack.hydat.station.jl Hydat.sqlite3         George
       jjj unpack.hydat.station.jl Hydat.sqlite3         Moisie
       jjj unpack.hydat.station.jl Hydat.sqlite3         Romaine
       jjj unpack.hydat.station.jl Hydat.sqlite3         Natashquan
       jjj unpack.hydat.station.jl Hydat.sqlite3         PetitMecatina
       jjj unpack.hydat.station.jl Hydat.sqlite3         StLawrence
       jjj unpack.hydat.station.jl Hydat.sqlite3         Bonaventure
       jjj unpack.hydat.station.jl Hydat.sqlite3         Restigouche
       jjj unpack.hydat.station.jl Hydat.sqlite3         Miramachi
       jjj unpack.hydat.station.jl Hydat.sqlite3         StJohn
       jjj unpack.hydat.station.jl Hydat.sqlite3         Penobscot
       jjj unpack.hydat.station.jl Hydat.sqlite3         Kennebec
       jjj unpack.hydat.station.jl Hydat.sqlite3         Merrimack

       jjj unpack.hydat.station.jl Hydat.sqlite3 02SB005 Betsiamites

       rrr plot.hydat.station.correlation.R SAINT-LAURENT wlev
       rrr plot.hydat.station.correlation.R SAINT-LAURENT flow
       rrr plot.hydat.station.correlation.R SAGUENAY wlev
       rrr plot.hydat.station.correlation.R SAGUENAY flow

# plot the domain and streamflow maps from CHRTOUT_GRID1 files (and zoom in grads to identify lat/lon indices for timeseries plots)
wrkh ; cp x.2020-04-28_start_1938-01-01_end_1949-10-08/194910081200.CHRTOUT_GRID1 .
       vi   194910081200.CHRTOUT_GRID1.ctl
      "dset 194910081200.CHRTOUT_GRID1
       dtype netcdf
       undef -888
       TITLE WRF Output Grid Coordinates: south_north, west_east
       *pdef  970 670 lcc 50.0 -73.2 490.0 340.0 50.0 50.0 -73.2 3260 3260
       xdef  970 linear -110.0       0.025
       ydef  670 linear   30.0       0.025
       zdef    1 linear    0         1
       tdef    1 linear 1jun1933 6hr
       vars 1
       streamflow=>flow 0 t,y,x streamflow
       endvars" ; grads
       "set xlab off" ; "set ylab off" ; "set mpdraw off"
       "set gxout grid" ; "d maskout(flow/100,flow)"

# plot streamflow timeseries (at specific lat/lon points) from the CHRTOUT_GRID1 files and USGS observations
# (from https://waterdata.usgs.gov/nwis/dv?referred_module=sw&site_no=04264331)
wrkh ; cd wrfhydro
       jjj         diag.wrf.ocean.entry.links.jl ../x.2020-04-07_start_1933-06-01_end_1934-07-06/
       jjj         diag.wrf.ocean.entry.links.jl ../x.2020-04-27_start_1934-01-01_end_1938-02-21/
       jjj         diag.wrf.ocean.entry.links.jl ../x.2020-04-28_start_1938-01-01_end_1949-10-08/

wrkg ; cd CALUN.era/dat
       mkdir dat ; cd dat
       jjj diag.wrf.ocean.entry.links.jl ../run_DOMAIN_full_full_FORCING.era_1990-01-01_005478/
       jjj diag.wrf.ocean.entry.links.jl ../run_DOMAIN_full_full_FORCING.era_1992-11-20_004424.res/
       jjj diag.wrf.ocean.entry.links.jl ../run_DOMAIN_full_full_FORCING.era_1995-11-10_003339.res/
       jjj diag.wrf.ocean.entry.links.jl ../run_DOMAIN_full_full_FORCING.era_1998-11-20_002233.res/
       jjj diag.wrf.ocean.entry.links.jl ../run_DOMAIN_full_full_FORCING.era_2004-06-10_000204.res/
       jjj diag.wrf.ocean.entry.links.jl ../run_DOMAIN_full_full_FORCING.era_2004-12-30_005846.res/
       jjj diag.wrf.ocean.entry.links.jl ../run_DOMAIN_full_full_FORCING.era_2010-07-20_003813.res/
       jjj diag.wrf.ocean.entry.links.jl ../run_DOMAIN_full_full_FORCING.era_2016-02-15_001782.res/

wrkg ; cd hydat
       jjj         diag.wrf.ocean.entry.timeseries.compare.jl hydat_03JB004.nc streamflow.201810010000.CHRTOUT_GRID1.1096.137.166
       grads -blc "diag.wrf.ocean.entry.timeseries.compare    hydat_03JB004.nc            2018100100 00.CHRTOUT_GRID1 1096 137 166" ; di streamflow.201810010000.CHRTOUT_GRID1.1096.137.166.png
       set  XX = "1934" ; set  YY = "1139" ; echo AuxFeuilles   ; jjj diag.wrf.ocean.entry.timeseries.jl   streamflow 2007010100 00.CHRTOUT_GRID1 2192 $YY $XX
       set  XX = "1240" ; set  YY = "1031" ; echo Churchill     ; jjj diag.wrf.ocean.entry.timeseries.jl   streamflow 2007010100 00.CHRTOUT_GRID1 2192 $YY $XX
       grads -blc "diag.wrf.ocean.entry.timeseries streamflow 2007010100 00.CHRTOUT_GRID1 2192 $YY $XX"
       grads -blc "diag.wrf.ocean.entry.timeseries    obsflow 2007010100 00.CHRTOUT_GRID1 2192 $YY $XX"

#      set  XX = "1940" ; set  YY = "1147" ; echo AuxFeuilles   ; jjj diag.wrf.ocean.entry.timeseries.jl   streamflow 1997010100 00.CHRTOUT_GRID1 2192 $YY $XX
       export XX="1940" ; export YY="1147" ; echo AuxFeuilles   ; jjj diag.wrf.ocean.entry.timeseries.jl   streamflow 2007010100 00.CHRTOUT_GRID1 2192 $YY $XX
       export XX="1998" ; export YY="1151" ; echo Caniapiscau   ; jjj diag.wrf.ocean.entry.timeseries.jl   streamflow 2007010100 00.CHRTOUT_GRID1 2192 $YY $XX
       export XX="2019" ; export YY="1139" ; echo ALaBaleine    ; jjj diag.wrf.ocean.entry.timeseries.jl   streamflow 2007010100 00.CHRTOUT_GRID1 2192 $YY $XX
       export XX="2058" ; export YY="1178" ; echo George        ; jjj diag.wrf.ocean.entry.timeseries.jl   streamflow 2007010100 00.CHRTOUT_GRID1 2192 $YY $XX
       export XX="2218" ; export YY="740"  ; echo Moisie        ; jjj diag.wrf.ocean.entry.timeseries.jl   streamflow 2007010100 00.CHRTOUT_GRID1 2192 $YY $XX
       export XX="2292" ; export YY="773"  ; echo Romaine       ; jjj diag.wrf.ocean.entry.timeseries.jl   streamflow 2007010100 00.CHRTOUT_GRID1 2192 $YY $XX
       export XX="2363" ; export YY="792"  ; echo Natashquan    ; jjj diag.wrf.ocean.entry.timeseries.jl   streamflow 2007010100 00.CHRTOUT_GRID1 2192 $YY $XX
       export XX="2428" ; export YY="854"  ; echo PetitMecatina ; jjj diag.wrf.ocean.entry.timeseries.jl   streamflow 2007010100 00.CHRTOUT_GRID1 2192 $YY $XX
       export XX="2107" ; export YY="504"  ; echo StLawrence    ; jjj diag.wrf.ocean.entry.timeseries.jl   streamflow 2007010100 00.CHRTOUT_GRID1 2192 $YY $XX
       export XX="2263" ; export YY="634"  ; echo Bonaventure   ; jjj diag.wrf.ocean.entry.timeseries.jl   streamflow 2007010100 00.CHRTOUT_GRID1 2192 $YY $XX
       export XX="2238" ; export YY="612"  ; echo Restigouche   ; jjj diag.wrf.ocean.entry.timeseries.jl   streamflow 2007010100 00.CHRTOUT_GRID1 2192 $YY $XX
       export XX="2305" ; export YY="581"  ; echo Miramachi     ; jjj diag.wrf.ocean.entry.timeseries.jl   streamflow 2007010100 00.CHRTOUT_GRID1 2192 $YY $XX
       export XX="2315" ; export YY="475"  ; echo StJohn        ; jjj diag.wrf.ocean.entry.timeseries.jl   streamflow 2007010100 00.CHRTOUT_GRID1 2192 $YY $XX
       export XX="2226" ; export YY="399"  ; echo Penobscot     ; jjj diag.wrf.ocean.entry.timeseries.jl   streamflow 2007010100 00.CHRTOUT_GRID1 2192 $YY $XX
       export XX="2201" ; export YY="356"  ; echo Kennebec      ; jjj diag.wrf.ocean.entry.timeseries.jl   streamflow 2007010100 00.CHRTOUT_GRID1 2192 $YY $XX
       export XX="2175" ; export YY="277"  ; echo Merrimack     ; jjj diag.wrf.ocean.entry.timeseries.jl   streamflow 2007010100 00.CHRTOUT_GRID1 2192 $YY $XX
       grads -blc "diag.wrf.ocean.entry.timeseries streamflow 1997010100 00.CHRTOUT_GRID1 2192 $YY $XX"

       set YY = "000" ; set XX = "000" ; echo Moses-Saunders ; jjj diag.wrf.ocean.entry.monitoring.jl   streamflow 1972060100 00.CHRTOUT_GRID1 4747 $YY $XX
       set YY = "160" ; set XX = "434" ; echo Moses-Saunders ; jjj diag.wrf.ocean.entry.timeseries.jl   streamflow 1972060100 00.CHRTOUT_GRID1 4747 $YY $XX
       set YY = "231" ; set XX = "535" ; echo St. Lawrence   ; jjj diag.wrf.ocean.entry.timeseries.jl   streamflow 1972060100 00.CHRTOUT_GRID1 4747 $YY $XX
       set YY = "274" ; set XX = "564" ; echo Saguenay       ; jjj diag.wrf.ocean.entry.timeseries.jl   streamflow 1972060100 00.CHRTOUT_GRID1 4747 $YY $XX
       set YY = "301" ; set XX = "586" ; echo Outardes       ; jjj diag.wrf.ocean.entry.timeseries.jl   streamflow 1972060100 00.CHRTOUT_GRID1 4747 $YY $XX
       set YY = "309" ; set XX = "593" ; echo Betsiamites    ; jjj diag.wrf.ocean.entry.timeseries.jl   streamflow 1972060100 00.CHRTOUT_GRID1 4747 $YY $XX
       set YY = "310" ; set XX = "596" ; echo Manitouagan    ; jjj diag.wrf.ocean.entry.timeseries.jl   streamflow 1972060100 00.CHRTOUT_GRID1 4747 $YY $XX
       set YY = "com" ; set XX = "pos" ; echo RIVSUM         ; jjj diag.wrf.ocean.entry.timecompos.jl   streamflow 1972060100 00.CHRTOUT_GRID1 4747 $YY $XX
                                                               jjj diag.wrf.ocean.entry.monthly.jl      streamflow 1972060100 00.CHRTOUT_GRID1 4747 $YY $XX
                                                               jjj diag.wrf.ocean.entry.monthly.mean.jl streamflow.1972060100.4747.$YY.$XX.monthly
       grads -blc "diag.wrf.ocean.entry.timeseries streamflow 1972060100 00.CHRTOUT_GRID1 4747 $YY $XX" ; mv streamflow.197206010000.CHRTOUT_GRID1.4747.$YY.$XX.png ..
       grads -blc "plot.rivsum.monthly             streamflow.1972060100.4747.$YY.$XX.monthly"          ; mv streamflow.1972060100.4747.$YY.$XX.monthly.png ..
       grads -blc "plot.rivsum.monthly             streamflow.1972060100.4747.$YY.$XX.monthly.mean"     ; mv streamflow.1972060100.4747.$YY.$XX.monthly.mean.png ..

----------------------------------------------------------------------------------//

# convert and plot the RIVSUM dataset from https://catalogue.ogsl.ca/en/dataset
# wget https://psl.noaa.gov/thredds/fileServer/Datasets/20thC_ReanV2/Monthlies/monolevel/pres.sfc.mon.mean.nc
wrkh ; cd rivsum
       jjj         plot.rivsum.monthly.jl               rivsum.1955_2020
       jjj         diag.wrf.ocean.entry.monthly.mean.jl rivsum.1955_2020
       grads -blc "plot.rivsum.monthly                  rivsum.1955_2020"
       grads -blc "plot.rivsum.monthly                  rivsum.1955_2020.mean"

----------------------------------------------------------------------------------//

# The variables read in by WRF-Hydro from wrfout* files are: T2, Q2, U10, V10, PSFC, GLW, SWDOWN, RAINC, RAINNC, VEGFRA, and LAI.
wrkh ; git clone https://github.com/wrf-model/WRF
       cd WRF ; git fetch && git fetch --tags ; git checkout v4.1.3
       setenv NETCDF /cvmfs/soft.computecanada.ca/easybuild/software/2017/avx2/Compiler/intel2019/netcdf/4.7.0
       ./configure

----------------------------------------------------------------------------------//

# get a copy of DDS
wrkh ; git clone https://github.com/bdb67/Dynamically-Dimensioned-Search.git

----------------------------------------------------------------------------------//

# LAKEPARM.nc Var      Description
# lake_id              Lake index (consecutively from 1 to n # of lakes)
# LkArea               Area [m2]
# LkMxE                Elevation of maximum lake height [m AMSL]
# WeirC                Weir coefficient
# WeirL                Weir length [m]
# OrificeC             Orifice coefficient
# OrificeA             Orifice area [m2]
# OrificeE             Orifice elevation [m AMSL]
# lat                  Latitude [decimal degrees]
# lon                  Longitude [decimal degrees]
# time                 time
# WeirE                weir elevation [m AMSL]
# ascendingIndex       Index to use for sorting IDs (ascending)
# ifd                  Initial fraction water depth
# crs                  CRS definition

----------------------------------------------------------------------------------//

# download (and patch together into a single grid) the HydroSHEDS tiles of a hydrologically conditioned DEM (from download
# button at https://www.dropbox.com/sh/hmpwobbz9qixxpe/AACy4RG4sRYvlOZ9sLyUj5ela/HydroSHEDS_CON/CON_3s_BIL/na_con_3s_zip_bil)
wrkg ; cd hydrosheds
       unzip na_con_3s_zip_bil.zip
       mv n2*.zip n3*.zip n??w1*.zip z.excluded
       ls -1 n* | grep -E '.zip$' | parallel -j 5 unzip
       ls -1 n* | grep -E '.bil$' | awk '{printf "gdal_translate %s %s.nc\n", $1, $1}' > exec ; cat exec | parallel -j 5
       ls -1 n* | grep -Ev '.nc$' | awk '{printf "mv %s z.included\n", $1}' | csh
       jjj diag.hydroshed.watershed.tiles.jl na_con_3s_all.nc
       gdal_translate na_con_3s_all.nc na_con_3s_all.bil
       gzip                            na_con_3s_all.bil
       mv             na_con_3s_all.nc na_con_3s_all.bil.gz ../DOMAIN_full_full/

# compare the WRFHydro routing (i.e., Canadian river outlets and corresponding watersheds, based on MERIT)
# to HydroSHEDS; first copy an outlet control file from the grads_ctl subdir of this repo to the working dir
wrkg ; cd HYDROSHEDS
       wget ftp://ftp.cdc.noaa.gov/Datasets/20thC_ReanV3/timeInvariantSI/hgt.sfc.nc
       jjj         diag.hydroshed.watershed.refer.jl HydroRIVERS_v10.shp hybas_na_lev12_v1c.shp
       jjj         diag.hydroshed.watershed.refer.jl HydroRIVERS_v10.shp hybas_ar_lev12_v1c.shp 1084
       jjj         diag.hydroshed.watershed.refer.jl HydroRIVERS_v10.shp hybas_gr_lev12_v1c.shp 2679
       mv grp???.??? shp
       grads -blc "diag.hydroshed.watershed.refer   Fulldom_hires_outlets_refer" ; di Fulldom_hires_outlets_refer.png
wrkg ; cd merit
       jjj         diag.merit.watershed.entries.jl ../DOMAIN_full_full/ ..
       add Fraser river to Fulldom_hires_outletz.txt (2100796   210   796  1355     48.79774475   -122.65019989   209   796  1355     48.78937531   -122.67388153)
       grads -blc "diag.merit.watershed.entries Fulldom_hires_outlets"       ; di Fulldom_hires_outlets.png

# also compare WRFHydro routing to NRCAN watersheds and rivers (resolution is better than 15-s Hydrosheds,
# but NRCAN uses 11 major drainage areas, along with 164 sub-drainage areas and 974 sub-sub-drainage areas
# in the Standard Drainage Area Classification [SDAC] 2003; cf. https://www.statcan.gc.ca/eng/subjects/
# standard/sdac/sdacinfo1; only the 01,02,03 major drainage areas are toward the North Atlantic)
wrkg ; cd NRCAN
#      lftp -e 'find /pub/nrcan_rncan/vector/geobase_nhn_rhn/shp_en ; bye' ftp.maps.canada.ca > ftp.maps.canada.ca.list
       echo mirror pub/nrcan_rncan/vector/geobase_nhn_rhn/shp_en/01 01 | lftp ftp.maps.canada.ca
       echo mirror pub/nrcan_rncan/vector/geobase_nhn_rhn/shp_en/02 02 | lftp ftp.maps.canada.ca
       echo mirror pub/nrcan_rncan/vector/geobase_nhn_rhn/shp_en/03 03 | lftp ftp.maps.canada.ca
       wget ftp://ftp.cdc.noaa.gov/Datasets/20thC_ReanV3/timeInvariantSI/hgt.sfc.nc
       jjj         diag.hydroshed.watershed.nrcan.jl 01 02 03
       grads -blc "diag.hydroshed.watershed.nrcan   Fulldom_hires_outlets_nrcan" ; di Fulldom_hires_outlets_nrcan.png

----------------------------------------------------------------------------------//

# download a hydrologically conditioned HydroSHEDS DEM and use ArcGIS to get a routing stack (on a BIO laptop) and visualize both for comparison
#      e.g., download button at https://www.dropbox.com/sh/hmpwobbz9qixxpe/AACy4RG4sRYvlOZ9sLyUj5ela/HydroSHEDS_CON/CON_3s_GRID/na_con_3s_zip_grid
#      e.g., wget https://edcintl.cr.usgs.gov/downloads/sciweb1/shared/hydrosheds/sa_15s_zip_grid/na_dem_15s_grid.zip
#      see https://hydrosheds.cr.usgs.gov/datadownload.php?reqdata=15demg
wrkh ; cd usgs
       wget https://edcintl.cr.usgs.gov/downloads/sciweb1/shared/hydrosheds/sa_30s_zip_grid/na_dem_30s_grid.zip
       wget https://edcintl.cr.usgs.gov/downloads/sciweb1/shared/hydrosheds/sa_30s_zip_grid/na_acc_30s_grid.zip
       wget https://edcintl.cr.usgs.gov/downloads/sciweb1/shared/hydrosheds/sa_30s_zip_grid/na_dir_30s_grid.zip
       wget https://edcintl.cr.usgs.gov/downloads/sciweb1/shared/hydrosheds/sa_shapefiles_zip/na_riv_30s.zip
       wget https://edcintl.cr.usgs.gov/downloads/sciweb1/shared/hydrosheds/sa_shapefiles_zip/na_bas_30s_beta.zip
       unzip na_bas_30s_beta.zip ; unzip na_riv_30s.zip ; echo unzip na_acc_30s_grid.zip ; echo unzip na_dem_30s_grid.zip ; echo unzip na_dir_30s_grid.zip
       wget ftp://ftp.cdc.noaa.gov/Datasets/20thC_ReanV3/timeInvariantSI/hgt.sfc.nc
       "set datawarn off" ; "d hgt-hgt"
       "set shpopts 0" ; "set line 5" ; *"draw shp na_bas_30s_beta" ; "set line 1 1 6"
       "set shpopts 0" ; "set line 3" ;  "draw shp na_riv_30s"      ; "set line 1 1 6"

----------------------------------------------------------------------------------//

       jjj diag.wrf.forcing.interp.nar.batch.jl
       parallel -j 3 julia $STEMZ/diag.wrf.forcing.interp.mean.jl 2006 3 ::: 1 500 1000 1500 2000 2500 3000 ::: 500

----------------------------------------------------------------------------------//

"set xlab off" ; "set ylab off"
"set mpdraw off" ; "set gxout grfill" ; "set yflip on"
*"d maskout(topo,topo)"
"d maskout(flodir,-flodir)"
*"set gxout grid"
*"d flodir"
*"d maskout(flodir,-flodir)"
*"set gxout grid"
*"d maskout(flodir,-(flodir-0.5))"
*"d maskout(topo,topo)"
*"set gxout grid"
*"d maskout(flodir,topo)"
*"set gxout grid"
*"d maskout(floacc,floacc)"
*"d maskout(log10(floacc),floacc)"
*"d riv" ; "d lake"
*"set ccols 1" ; "set clevs   1" ; "d maskout(riv,riv)"
*"set ccols 4" ; "set clevs 100" ; "d maskout(lake,lake)"

----------------------------------------------------------------------------------//

       vi HydroLAKES_polys_v10.R
#        library(raster)
#        x <- shapefile('HydroLAKES_polys_v10_Atlantic.shp')
#        extent(x)
#        x$area_sqkm <- area(x) / 1000000
#        z <- x[x$area_sqkm > 4, ]
#        shapefile(z,   'HydroLAKES_polys_v10_Atlantic_4km2.shp')
       vi HydroLAKES_polys_v10.sbatch
         #!/bin/tcsh
         #SBATCH --account=def-wperrie
         #SBATCH --job-name=HydroLAKES_polys_v10
         #SBATCH   --output=HydroLAKES_polys_v10.sbatco
         #SBATCH --ntasks=1
         #SBATCH --mem-per-cpu=24000M
         #SBATCH --time=01:59:00
         echo Job running at `hostname`
         echo Job running on `grep "model name" /proc/cpuinfo | head -1`
         echo Job running in `pwd`
         echo Job beginning  `date`
         echo Rscript HydroLAKES_polys_v10.R
              Rscript HydroLAKES_polys_v10.R
         echo Job ending at `date`
       sbatch HydroLAKES_polys_v10.sbatch
       module load intel/2019.3 openmpi/4.0.1 netcdf-fortran/4.4.5

----------------------------------------------------------------------------------//

# (deprecated) use ArcGIS to get a routing stack (on a BIO laptop) and a visualization; first transfer three files
# (geogrid.nc HydroLAKES_polys_v10_Atlantic_999km2.shp na_con_3s_all.bil) from DOMAIN$DOMZ to the laptop
# then transfer the resulting routing stack (WRF_Hydro_routing_grids.zip) back to DOMAIN$DOMZ and unpack
wrkg ; cd DOMAIN$DOMZ
       cp geogrid.nc HydroLAKES_polys_v10_Atlantic_999km2.shp na_con_3s_all.bil.gz ~/bio/
sftp ; to laptop the files from ~/bio/
       (may need to comment out arcpy.Delete_management(order) and (order2) on lines 2946-7 of wrf_hydro_functions.py)
       Parameter: Input GEOGRID File:                            C:\Users\DANIELSONR\Desktop\bio\geogrid.nc
       Parameter: Forecast Points (CSV):                         None
       Parameter: Mask CHANNELGRID variable to forecast basins?: false
       Parameter: Create reach-based routing (RouteLink) files?: false
       Parameter: Create lake parameter (LAKEPARM) file?:        true
       Parameter: Reservoirs Shapefile or Feature Class:         C:\Users\DANIELSONR\Desktop\bio\HydroLAKES_polys_v10_Atlantic_999km2.shp
       Parameter: Input Elevation Raster:                        C:\Users\DANIELSONR\Desktop\bio\na_con_3s_all.bil
       Parameter: Regridding (nest) Factor:                      10
       Parameter: Number of routing grid cells to define stream: 200
       Parameter: OVROUGHRTFAC Value:                            1
       Parameter: RETDEPRTFAC Value:                             1
       Parameter: Output ZIP File:                               C:\Users\DANIELSONR\Desktop\redp\b\WRF_Hydro_routing_grids.zip
wrkg ; cd DOMAIN$DOMZ
       mv  ~/WRF_Hydro_routing_grids.zip ./
       unzip WRF_Hydro_routing_grids.zip
       cp Fulldom_hires.nc Fulldom_hires.rename.nc
# (cc) module load intel/2018.3 nco/4.6.6
       ncrename -v LATITUDE,XLAT_M -v LONGITUDE,XLONG_M Fulldom_hires.rename.nc
       ncap2 -s 'defdim("Time",1);XLAT_M[$Time,$y,$x]=XLAT_M;XLONG_M[$Time,$y,$x]=XLONG_M;CHANNELGRID[$Time,$y,$x]=CHANNELGRID' Fulldom_hires.rename.nc out.nc
       ncrename -v y,south_north -v x,west_east out.nc
       ncrename -d y,south_north -d x,west_east out.nc
# (cc) module load gcc/7.3.0 r/3.6.0
       rrr plot.wrfhydro.domain.R    geogrid.nc out.nc
       rm               Fulldom_hires.rename.nc out.nc

----------------------------------------------------------------------------------//

# use an open source WRF-Hydro GIS Pre-processor (beta version) to get a routing stack (on laptop with python)
# following https://github.com/NCAR/wrf_hydro_gis_preprocessor but set up a Compute Canada virtual environment
# following https://docs.computecanada.ca/wiki/Python (module load python/3.6.10 scipy-stack/2020b) and for
# specific version of packages, see for example "avail_wheels --name "*cdf*" --all_version"; setup can be
# bash ; cd /project/6000950/riced/soft ; virtualenv --no-download python_wrfhydro ; source python_wrfhydro
# pip install --no-index --upgrade pip ; pip install netCDF4 pyproj gdal==3.0.4 whitebox==1.5.0
wrkg ; cd DOMAIN$DOMZ
       mkdir wrf_hydro_gis_preprocessor ; cd wrf_hydro_gis_preprocessor
       cp /project/6000950/riced/work/wrf_hydro_gis_preprocessor/wrfhydro_gis/* .
       mkdir out ; bash
       source /project/6000950/riced/soft/python_wrfhydro/bin/activate
       python Create_Domain_Boundary_Shapefile.py -i ../geogrid.nc -o out
#      python Build_GeoTiff_From_Geogrid_File.py -i geogrid.nc -v HGT_M -o HGT_M.tif
       python Build_Routing_Stack.py -i ../geogrid.nc -l ../HydroLAKES_polys_v10_Canada_999km2.shp -d ../na_con_3s_all.nc -o out.zip

# use an open source WRF-Hydro GIS Pre-processor (beta version) to get a routing stack (on BIO server)
# following https://github.com/NCAR/wrf_hydro_gis_preprocessor; first create conda environment and clone
       . ssmuse-sh -x hpco/exp/mib002/anaconda2/anaconda2-5.0.1-hpcobeta2
       conda config --add channels conda-forge
#      conda create -n wrfh_gis_env -c conda-forge python=3.6 gdal netCDF4 numpy pyproj whitebox=1.5.0
       conda create -n wrfh_gis_env -c conda-forge python=3.10 gdal netCDF4 numpy pyproj whitebox=2.2.0 packaging shapely
wrkg ; git clone https://github.com/NCAR/wrf_hydro_gis_preprocessor.git
wrkg ; cd                                wrf_hydro_gis_preprocessor/wrfhydro_gis
       mkdir out
(bash) source activate wrfh_gis_env
(tcsh) conda  activate wrfh_gis_env
#      python Create_Domain_Boundary_Shapefile.py -i ../../DOMAIN$DOMZ/geogrid.nc -o out
#      python Build_GeoTiff_From_Geogrid_File.py  -i ../../DOMAIN$DOMZ/geogrid.nc -v HGT_M -o out/HGT_M.tif
#      python Build_Routing_Stack.py              -i ../../DOMAIN$DOMZ/geogrid.nc -R 25 -l ../../DOMAIN$DOMZ/HydroLAKES_polys_v10_Canada_999km2.shp -d ../../DOMAIN$DOMZ/merit_hydrodem_30_85N_180_00W_30sec_elevtn.nc -o out/out.zip
       python Build_Routing_Stack.py              -i ../../DOMAIN$DOMZ/geogrid.nc -R 25 -l ../../hydrolakes/HydroLAKES_polys_v10_Canada_999km2.shp -d ../../merit/merit_hydrodem_30_85N_180_00W_30sec_elevtn.nc -o out/out.zip
       python Build_Routing_Stack.py              -i ../../DOMAIN$DOMZ/geogrid.nc -R 25 -t 50                                                      -d ../../merit/merit_hydrodem_30_85N_180_00W_30sec_elevtn.nc -o out/out.zip
(tcsh) conda  deactivate
(bash) source deactivate
       mv out/out.log out/out.zip ../../DOMAIN$DOMZ
wrkg ; cd DOMAIN$DOMZ
       unzip out.zip
       cp Fulldom_hires.nc Fulldom_hires.rename.nc
# (cc) module load intel/2018.3 nco/4.6.6
       ncrename -v LATITUDE,XLAT_M -v LONGITUDE,XLONG_M Fulldom_hires.rename.nc
       ncap2 -s 'defdim("Time",1);XLAT_M[$Time,$y,$x]=XLAT_M;XLONG_M[$Time,$y,$x]=XLONG_M;CHANNELGRID[$Time,$y,$x]=CHANNELGRID' Fulldom_hires.rename.nc out.nc
       ncrename -v y,south_north -v x,west_east out.nc
       ncrename -d y,south_north -d x,west_east out.nc
# (cc) module load gcc/7.3.0 r/3.6.0
       rrr plot.wrfhydro.domain.R geogrid.nc out.nc
       rm            Fulldom_hires.rename.nc out.nc

----------------------------------------------------------------------------------//

# or download (and patch together into a single grid) the MERIT tiles of a hydrologically conditioned DEM (from
# http://hydro.iis.u-tokyo.ac.jp/~yamadai/MERIT_Hydro)
wrkg ; cd merit
       wget --user= --password= http://hydro.iis.u-tokyo.ac.jp/~yamadai/MERIT_Hydro/distribute/v1.0/elv_n30w180.tar
       wget --user= --password= http://hydro.iis.u-tokyo.ac.jp/~yamadai/MERIT_Hydro/distribute/v1.0/elv_n30w150.tar
       wget --user= --password= http://hydro.iis.u-tokyo.ac.jp/~yamadai/MERIT_Hydro/distribute/v1.0/elv_n30w120.tar
       wget --user= --password= http://hydro.iis.u-tokyo.ac.jp/~yamadai/MERIT_Hydro/distribute/v1.0/elv_n30w090.tar
       wget --user= --password= http://hydro.iis.u-tokyo.ac.jp/~yamadai/MERIT_Hydro/distribute/v1.0/elv_n30w060.tar
       wget --user= --password= http://hydro.iis.u-tokyo.ac.jp/~yamadai/MERIT_Hydro/distribute/v1.0/elv_n30w030.tar
       wget --user= --password= http://hydro.iis.u-tokyo.ac.jp/~yamadai/MERIT_Hydro/distribute/v1.0/elv_n60w180.tar
       wget --user= --password= http://hydro.iis.u-tokyo.ac.jp/~yamadai/MERIT_Hydro/distribute/v1.0/elv_n60w150.tar
       wget --user= --password= http://hydro.iis.u-tokyo.ac.jp/~yamadai/MERIT_Hydro/distribute/v1.0/elv_n60w120.tar
       wget --user= --password= http://hydro.iis.u-tokyo.ac.jp/~yamadai/MERIT_Hydro/distribute/v1.0/elv_n60w090.tar
       wget --user= --password= http://hydro.iis.u-tokyo.ac.jp/~yamadai/MERIT_Hydro/distribute/v1.0/elv_n60w060.tar
       wget --user= --password= http://hydro.iis.u-tokyo.ac.jp/~yamadai/MERIT_Hydro/distribute/v1.0/elv_n60w030.tar
       wget --user= --password= http://hydro.iis.u-tokyo.ac.jp/~yamadai/MERIT_Hydro/distribute/v1.0/elv_n30w030.tar
       ls -1 e* | grep -E '.tar$' | awk '{printf "tar xvf %s ; mv %s/* . ; rmdir %s\n", $1, substr($1,1,11), substr($1,1,11)}' > exec ; cat exec | parallel -j 5
       rm n80* n??w00* n??w01* n??w020* n??w165* n??w17* n??w18*
       ls -1 n* | grep -E '.tif$' | awk '{printf "gdal_translate %s %s.nc\n", $1, $1}'                                         > exec ; cat exec | parallel -j 5
       jjj diag.hydroshed.watershed.tiles.merit.jl na_con_3s_merit.nc
       mv  na_con_3s_merit.nc ../DOMAIN_full_full/

# (optional) assemble views of watersheds and rivers (from https://www.nrcan.gc.ca/earth-sciences/geography/atlas-canada/selected-thematic-maps/16888)
work ; cd workg.documents
       wget http://ftp.geogratis.gc.ca/pub/nrcan_rncan/raster/atlas/eng/various_divers/north_america_cec_watersheds.jpg
       convert north_america_cec_watersheds.jpg +distort SRT 249,56,54 -format "%X" -write info: +repage x.png
#      convert north_america_cec_watersheds.jpg -distort SRT 249,56,55 x.png

# create uniform, cold-start model initial conditions (on Compute Canada, R and nco are used separately,
# so comment out all nco commands in create_wrfinput.R and issue them explicitly, following nco module load
# (cc) module load gcc/7.3.0 r/3.6.0) on GPSC the script works without modification
# (cc) module load intel/2018.3 nco/4.6.6
# (cc) ncks -O -4 -v XLAT_M,XLONG_M,HGT_M,SOILTEMP,LU_INDEX,MAPFAC_MX,MAPFAC_MY,GREENFRAC,LAI12M,SOILCTOP ../DOMAIN$DOMZ/geogrid.nc wrfinput_d01.nc
# (cc) ncrename -O -v HGT_M,HGT wrfinput_d01.nc wrfinput_d01.nc
# (cc) ncrename -O -v XLAT_M,XLAT wrfinput_d01.nc wrfinput_d01.nc
# (cc) ncrename -O -v XLONG_M,XLONG wrfinput_d01.nc wrfinput_d01.nc
# (cc) ncrename -O -v LU_INDEX,IVGTYP wrfinput_d01.nc wrfinput_d01.nc
# (cc) module load gcc/7.3.0 r/3.6.0
# (cc) create_wrfinput.R --geogrid='../DOMAIN'$DOMZ'/geogrid.nc' --filltyp=3 --laimo=1
# (cc) module load intel/2018.3 nco/4.6.6
# (cc) ncks -O -x -v SOILTEMP,GREENFRAC,LAI12M,SOILCTOP wrfinput_d01.nc wrfinput_d01.nc
# (cc) cp wrfinput_d01.nc ../DOMAIN$DOMZ/

# (optional) perform test runs with or without routing and idealized or wrf forcing
# (on Compute Canada) module load intel/2019.3 openmpi/4.0.1 netcdf-fortran/4.4.5
wrkh ; cd testcase
       wget https://github.com/NCAR/wrf_hydro_nwm_public/releases/download/v5.1.1/croton_NY_example_testcase.tar.gz
       tar xvfz croton_NY_example_testcase.tar.gz ; mv example_case/* example_case/.v* . ; rmdir example_case
       mv FORCING NWM ; cd NWM
       jjj exec.wrf.hydro.test.jl cca nomod
       mkdir                                                        wrf_hydro_routing_off_forcing_idl
       cp   wrf_hydro.sbatch hydro.namelist namelist.hrldas         wrf_hydro_routing_off_forcing_idl
       mv x.wrf_hydro.sbatco 2011* diag_hydr* [HR]*DOMAIN1          wrf_hydro_routing_off_forcing_idl
       rsync -avz riced@cedar.computecanada.ca:/scratch/riced/workh/wrf_hydro_routing_off_forcing_idl .
       jjj exec.wrf.hydro.test.jl cca nomod
       mkdir                                                        wrf_hydro_routing_off_forcing_wrf
       cp   wrf_hydro.sbatch hydro.namelist namelist.hrldas         wrf_hydro_routing_off_forcing_wrf
       mv x.wrf_hydro.sbatco 2011* diag_hydr* [HR]*DOMAIN1          wrf_hydro_routing_off_forcing_wrf
       rsync -avz riced@cedar.computecanada.ca:/scratch/riced/workh/wrf_hydro_routing_off_forcing_wrf .
       jjj exec.wrf.hydro.test.jl cca nomod
       mkdir                                                        wrf_hydro_routing_one_forcing_wrf
       cp   wrf_hydro.sbatch hydro.namelist namelist.hrldas         wrf_hydro_routing_one_forcing_wrf
       mv x.wrf_hydro.sbatco 2011* diag_hydr* [HR]*DOMAIN1          wrf_hydro_routing_one_forcing_wrf
       rsync -avz riced@cedar.computecanada.ca:/scratch/riced/workh/wrf_hydro_routing_one_forcing_wrf .

----------------------------------------------------------------------------------//

# use an open source WRF-Hydro GIS Pre-processor (https://github.com/NCAR/wrf_hydro_gis_preprocessor)
# to iterate on the output routing stack in its native Lambert conformal conic (LCC) projection to match
# large watersheds and high flow parts of the river network; first create the conda environment and
# duplicate the ArcGIS routing (ignoring lakes), then use the output LCC DEM as pre-processor input
# (". ssmuse-sh -x hpco/exp/mib002/anaconda2/anaconda2-5.0.1-hpcobeta2" is needed for conda on GPSC)
       conda config --add channels conda-forge
       conda create -n wrfh_gis_env -c conda-forge python=3.10 gdal netCDF4 numpy pyproj whitebox=2.2.0 packaging shapely
wrkg ; git clone https://github.com/NCAR/wrf_hydro_gis_preprocessor.git
wrkg ; cd                                wrf_hydro_gis_preprocessor/wrfhydro_gis
       mkdir out
       conda  activate wrfh_gis_env
#      source activate wrfh_gis_env
#      python Create_Domain_Boundary_Shapefile.py -i ../../DOMAIN$DOMZ/geogrid.nc -o out
#      python Build_GeoTiff_From_Geogrid_File.py  -i ../../DOMAIN$DOMZ/geogrid.nc -v HGT_M -o out/HGT_M.tif
#      python Build_Routing_Stack.py              -i ../../DOMAIN$DOMZ/geogrid.nc -R 25 -l ../../DOMAIN$DOMZ/HydroLAKES_polys_v10_Canada_999km2.shp -d ../../DOMAIN$DOMZ/merit_hydrodem_30_85N_180_00W_30sec_elevtn.nc -o out/out.zip
#      source deactivate
#      python Build_Routing_Stack.py              -i ../../DOMAIN$DOMZ/geogrid.nc -R 25 -l  ../../hydrolakes/HydroLAKES_polys_v10_Canada_999km2.shp -d ../../merit/merit_hydrodem_30_85N_180_00W_30sec_elevtn.nc -o out/out.zip
       python Build_Routing_Stack.py              -i ../../DOMAIN$DOMZ/geogrid.nc -R 25 -t 50                                                       -d ../../merit/merit_hydrodem_30_85N_180_00W_30sec_elevtn.nc -o out/out.zip
       conda  deactivate
       mv out/out.log out/out.zip ../../DOMAIN_LCC_duplicate_ArcGIS
wrkg ; cd                               DOMAIN_LCC_duplicate_ArcGIS
       unzip out.zip
       gdal_translate Fulldom_hires.nc     Fulldom_hires.tif -sds
       gdalsrsinfo                         Fulldom_hires_04.tif
       gdal_translate                      Fulldom_hires_04.tif Fulldom_hires_04.nc


wrkg ; cd HYDROSHEDS
       ogr2ogr -f CSV -dialect sqlite -sql "select * from HydroRIVERS_v10 where (MAIN_RIV = 70014971)" HydroRIVERS_v10_70014971.csv HydroRIVERS_v10.shp
       ogr2ogr                        -sql "select * from HydroRIVERS_v10 where (MAIN_RIV = 70014971)" HydroRIVERS_v10_70014971.shp HydroRIVERS_v10.shp

wrkg ; cd                               DOMAIN_LCC_duplicate_ArcGIS
       jjj diag.hydroshed.watershed.river.adjust.jl ../hydat/hydat_03JB002.sta.hyshed ../HYDROSHEDS/HydroRIVERS_v10_70014971.shp ../merit/merit_hydrodem_30_85N_180_00W_30sec_elevtn.nc Fulldom_hires.nc
       gdal_translate Fulldom_hires_walls.nc Fulldom_hires_walls.tif -sds
#      gdal_translate                      Fulldom_hires_04_walls.nc Fulldom_hires_04_walls.tif


wrkg ; cd HYDROSHEDS
       gdalsrsinfo                         groups.shp
       ogr2ogr -f "ESRI Shapefile" -t_srs "+proj=lcc +lat_0=59.0000038146973 +lon_0=-90 +lat_1=59 +lat_2=59 +x_0=0 +y_0=0 +R=6370000 +units=m +no_defs" -s_srs "+proj=longlat +datum=WGS84 +no_defs" grouplcc.shp groups.shp
wrkg ; cd                               DOMAIN_LCC_duplicate_ArcGIS
#      whitebox_tools -r=RaiseWalls -v -i=../HYDROSHEDS/grouplcc.shp --dem=Fulldom_hires_04.tif -o=Fulldom_hires_04_walls.tif --height=25.0
#      gdal_translate Fulldom_hires_04.tif Fulldom_hires_04.nc
wrkg ; cd                                wrf_hydro_gis_preprocessor/wrfhydro_gis
       conda  activate wrfh_gis_env
       python Build_Routing_Stack.py              -i ../../DOMAIN$DOMZ/geogrid.nc -R 25 -t 50                                                       -d ../../DOMAIN_LCC_duplicate_ArcGIS/Fulldom_hires_04_walls.tif -o out/out.zip
       conda  deactivate
       mv out/out.log out/out.zip ../../DOMAIN_LCC_duplicate_ArcGIS_DEM_walls
wrkg ; cd                               DOMAIN_LCC_duplicate_ArcGIS_DEM_walls
       unzip out.zip
       jjj unpack.hydat.station.river.wrfgrid.jl Fulldom_hires.nc
       cp ../DOMAIN_full_full/Fulldom_hires_full_full.ctl .

----------------------------------------------------------------------------------//

# prepare NARR forcing data, including a 15-year mean (repeating over 1901-1903), with variable names and data taken
# from https://psl.noaa.gov/data/gridded/data.narr.monolevel.html and ftp://ftp.cdc.noaa.gov/Datasets/NARR/monolevel
# (where the use of ncl may be more convenient on Compute Canada, but in any case, forcing can be copied and shared)
work ; cd FORCING.nar.src.native
       cd /home/sdfo404/storage/Hindcast_Canada/Atmospheric_data/NARR-A_for_Canada
       parallel --dry-run wget ftp://ftp.cdc.noaa.gov/Datasets/NARR/monolevel/ ::: air.2m. apcp. dlwrf. dswrf. pres.sfc. shum.2m. uwnd.10m. vwnd.10m. ::: 1990 1991 1992 1993 1994 1995 1996 1997 1998 1999 2000 2001 2002 2003 2004 2005 2006 2007 2008 2009 2010 2011 2012 2013 2014 2015 2016 2017 2018 2019 2020 ::: .nc > xcom
       sort xcom > xcom2 ; vi xcom2 ; :1,248s/ //g :1,248s/wgetftp/wget ftp/ ; nohup csh xcom2 > xcom3 &
wrkg ; cd FORCING.nar
       . ssmuse-sh -x hpco/exp/mib002/anaconda2/anaconda2-5.0.1-hpcobeta2 ; source   activate ncl_stable
       jjj diag.wrf.forcing.interp.nar.jl 1990-01-01-00 2005-12-31-21
       jjj diag.wrf.forcing.interp.nar.jl 2006-01-01-00 2020-12-31-21
                                                                            source deactivate
wrkg ; cd FORCING.nar.mean
       jjj diag.wrf.forcing.mean.jl ../FORCING.nar/ 2006 3
       jjj diag.wrf.forcing.mean.links.jl                3

# prepare CMC forcing data, including a 15-year mean (repeating over 1901-1903), using the operational global analyses,
# with data taken from rarc (on GPSC; see also https://www.gcpedia.gc.ca/wiki/DFO_GPSC)
# (where the use of ncl may be more convenient on Compute Canada, but in any case, forcing can be copied and shared)
wrkg ; cd FORCING.cmc
       . ssmuse-sh -d /fs/ssm/eccc/cmo/cmoi/apps/archive/rarc/latest
       cp /home/sdfo501/rarcdirectives/scratch_directives rarc.directives.gdps
       cp ~/prog/diag.hydrology/rarc.directives.gdps .
       rarc                  -i rarc.directives.gdps
# (cc) rsync -avz . riced@graham.computecanada.ca:/scratch/riced/workg/FORCING.cmc
wrkg ; cd FORCING.cmc
       jjj diag.wrf.forcing.interp.cmc.jl 1990-01-01 2004-12-31
       mv [a-z]* ../FORCING.cmc.src/
# (cc) rsync -avz   riced@graham.computecanada.ca:/scratch/riced/workg/FORCING.cmc .
wrkg ; cd FORCING.cmc.mean
       jjj diag.wrf.forcing.mean.jl ../FORCING.cmc/ 2006 6
       jjj diag.wrf.forcing.mean.links.jl                6

----------------------------------------------------------------------------------//

# prepare a basic calibration of WRF forcing data, including a 15-year mean (repeating over 1901-1903), where
# annual averages permit a linear calibration of WRF with respect to NARR (for each variable and 6-h UTC timestep)
wrkg ; cd FORCING
       jjj diag.wrf.forcing.calibrate.byday.jl       wrf_1990_2004 nar_1990_2004
       jjj diag.wrf.forcing.calibrate.byday.apply.jl wrf_1990_2004 forcing_wrf_1990_2004.cato.nar_1990_2004
       grads -bpc "diag.wrf.forcing.interp.avg.byday nar_1990_2004 wrf_1990_2004.forcing_wrf_1990_2004.cato.nar_1990_2004 nar_2005_2020 wrf_RCP45_2005_2099 wrf_RCP85_2005_2099"
       grads -bpc "diag.wrf.forcing.interp.avg.00day nar_1990_2004 wrf_1990_2004.forcing_wrf_1990_2004.cato.nar_1990_2004 nar_2005_2020 wrf_RCP45_2005_2099 wrf_RCP85_2005_2099"
wrkg ; cd FORCING.wrf.mean
       jjj diag.wrf.forcing.calibrate.byday.todir.jl ../FORCING.wrf.mcal ../FORCING/forcing_wrf_1990_2004.cato.nar_1990_2004
wrkg ; cd FORCING.wrf.mcal
       jjj diag.wrf.forcing.mean.links.jl 6

# perform a three-year spinup using calibrated 15-year mean WRF forcing, link dat and res to all restart subdirs,
# sync cmc and cca, and sum the river outflow to the North Atlantic
wrkg ; cd SPINUP.wrf.mcal
       jjj exec.wrf.hydro.spinup.jl             DOMAIN_0000_0000 FORCING.wrf.mcal 1901-01-01 1095
       jjj exec.wrf.hydro.spinup.jl             DOMAIN_0000_0000 FORCING.wrf.mcal 1903-01-01  365 run_DOMAIN_0000_0000_FORCING.wrf.mcal_1901-01-01_001095
       jjj diag.wrf.datres.mean.links.jl 12 run_DOMAIN_0000_0000_FORCING.wrf.mcal_1901-01-01_001095
       jjj diag.wrf.ocean.entry.timeseries.total.jl streamflow 1901010200 00.CHRTOUT_GRID1 1094 ../Fulldom_hires_outlets.txt
       mv streamflow.190101020000.CHRTOUT_GRID1.1094.nc ../streamflow.190101020000.CHRTOUT_GRID1.1094.wrfcal.nc
       rsync -avz SPINUP.wrf riced@graham.computecanada.ca:/scratch/riced/workg
       rsync -avz            riced@graham.computecanada.ca:/scratch/riced/workg/SPINUP.wrf .
wrkg ; grads -blc "diag.wrf.ocean.entry.timeseries.total streamflow.190101020000.CHRTOUT_GRID1.1094" ; di streamflow.190101020000.CHRTOUT_GRID1.1094.wrfcal.png
       convert -trim streamflow.190101020000.CHRTOUT_GRID1.1094.wrfcal.png xx.png ;             mv xx.png streamflow.190101020000.CHRTOUT_GRID1.1094.wrfcal.png

----------------------------------------------------------------------------------//

# perform a three-year spinup using the 15-year mean NARR forcing, link dat and res to all restart subdirs, and sync cmc and cca
wrkg ; cd SPINUP.nar
       jjj exec.wrf.hydro.spinup.jl DOMAIN_0000_0000 FORCING.nar.mean 1901-01-01 1095
       jjj exec.wrf.hydro.spinup.jl DOMAIN_0000_0000 FORCING.nar.mean 1903-01-01  365 run_DOMAIN_0000_0000_FORCING.nar.mean_1901-01-01_001095
       jjj exec.wrf.hydro.spinup.jl DOMAIN_0000_0000 FORCING.nar.mean 1902-12-28    3 /scratch/riced/workg/SPINUP.nar/res
       jjj diag.wrf.datres.mean.links.jl 12 run_DOMAIN_0000_0000_FORCING.nar.mean_1902-12-28_000003.res
       jjj diag.wrf.ocean.entry.timeseries.total.jl streamflow 1901010200 00.CHRTOUT_GRID1 1094 ../Fulldom_hires_outlets.txt
       mv streamflow.190101020000.CHRTOUT_GRID1.1094.nc ../streamflow.190101020000.CHRTOUT_GRID1.1094.narraw.nc
       rsync -avz SPINUP.nar riced@graham.computecanada.ca:/scratch/riced/workg
       rsync -avz            riced@graham.computecanada.ca:/scratch/riced/workg/SPINUP.nar .

# perform a three-year spinup using the 15-year mean CMC forcing, link dat and res to all restart subdirs, and sync cmc and cca
wrkg ; cd SPINUP.cmc
       jjj exec.wrf.hydro.spinup.jl DOMAIN_0000_0000 FORCING.cmc.mean 1901-01-01 1095
       jjj exec.wrf.hydro.spinup.jl DOMAIN_0000_0000 FORCING.cmc.mean 1903-01-01  365 run_DOMAIN_0000_0000_FORCING.cmc.mean_1901-01-01_001095
       jjj diag.wrf.datres.mean.links.jl 12
       rsync -avz SPINUP.cmc riced@graham.computecanada.ca:/scratch/riced/workg
       rsync -avz            riced@graham.computecanada.ca:/scratch/riced/workg/SPINUP.cmc .

----------------------------------------------------------------------------------//

# create another NetCDF file that accommodates 3-h WRF-Hydro and daily HyDAT streamflow estimates, apply both
# natural and regulated NNs, then sum 3-h estimates to daily, monthly, and annual-average (daily) scales, and
# tabulate performance separately for 1990-2004 and 2005-2022
wrkg ; cd hydat.validation
       ln -s ../DOMAIN_full_full/Fulldom_hires_river.txt Fulldom_hires_river.txt
       jjj         diag.wrf.ocean.entry.timeseries.mean.assemble.jl    hydat_valding_aall_1000_incl_1990-01-01_2004-12-30.neural.nc
       jjj         diag.wrf.ocean.entry.timeseries.mean.assemble.jl    hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.nc
       jjj         diag.wrf.ocean.entry.timeseries.perform.jl          hydat_valding_aall_1000_incl_1990-01-01_2004-12-30.neural.daily
       jjj         diag.wrf.ocean.entry.timeseries.perform.jl          hydat_valding_aall_1000_incl_1990-01-01_2004-12-30.neural.monthly
       jjj         diag.wrf.ocean.entry.timeseries.perform.jl          hydat_valding_aall_1000_incl_1990-01-01_2004-12-30.neural.annualday
       jjj         diag.wrf.ocean.entry.timeseries.perform.jl          hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.daily
       jjj         diag.wrf.ocean.entry.timeseries.perform.jl          hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.monthly
       jjj         diag.wrf.ocean.entry.timeseries.perform.jl          hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.annualday
#      grads -bpc "diag.wrf.ocean.entry.timeseries.perform             hydat_valding_aall_1000_incl_1990-01-01_2004-12-30.neural"
#      grads -bpc "diag.wrf.ocean.entry.timeseries.perform             hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural"
       jjj         diag.wrf.ocean.entry.timeseries.table.jl          3 hydat_valding_aall_1000_incl_1990-01-01_2004-12-30.neural.daily
       jjj         diag.wrf.ocean.entry.timeseries.table.jl          3 hydat_valding_aall_1000_incl_1990-01-01_2004-12-30.neural.monthly
       jjj         diag.wrf.ocean.entry.timeseries.table.jl          3 hydat_valding_aall_1000_incl_1990-01-01_2004-12-30.neural.annualday
       jjj         diag.wrf.ocean.entry.timeseries.table.jl          4 hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.daily
       jjj         diag.wrf.ocean.entry.timeseries.table.jl          4 hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.monthly
       jjj         diag.wrf.ocean.entry.timeseries.table.jl          4 hydat_valding_aall_1000_incl_1990-01-01_2022-12-30.neural.annualday
       tail *form? | grep Rivers
       grads -bpc "diag.wrf.ocean.entry.timeseries.pervald             hydat_valding_aall_1000_incl_1990-01-01_2004-12-30.neural"

# separate HyDAT stations and ensemble into subdirs by natural and regulated flows (as given by HyDAT) and
# for natural watersheds, derive a "natural" neural network estimate for the raw ERA/WRF forcing streamflow
# and sum 3-h estimates to daily, monthly, and annual-average (daily) scales, then tabulate performance and
# plot streamflow (with metrics) for each station
wrkg ; cd hydat.validation
       jjj         diag.wrf.ocean.entry.timeseries.valding.natural.jl  hydat_valding_aall_1000_incl   _1990-01-01_2004-12-30.nc    hydat_upstream_natural  hydat_upstream_regulat
wrkg ; cd hydat.validation/hydat_upstream_natural
       ln -s ../../DOMAIN_full_full/Fulldom_hires_river.txt Fulldom_hires_river.txt
       grads --quiet -blc "diag.wrf.ocean.entry.timeseries.valding     hydat_valding_aall_1000_incl_upnat .sta.hyshed"        ; di hydat_valding_aall_1000_incl_upnat.sta.hyshed.png
       grads --quiet -blc "diag.wrf.ocean.entry.timeseries.valding     hydat_valding_aall_1000_incl_upnat .sta.hwshed"        ; di hydat_valding_aall_1000_incl_upnat.sta.hwshed.png
       jjj         diag.wrf.ocean.entry.timeseries.neural.assemble.jl  hydat_valding_aall_1000_incl_upnat_1990-01-01_2004-12-30.nc ../../CALUN.era/pest_nwm_DOMAIN_East.FORCING.era_2019-03-31_000214.remsvd.prep.nnet.elu.jld
       jjj         diag.wrf.ocean.entry.timeseries.mean.assemble.jl    hydat_valding_aall_1000_incl_upnat_1990-01-01_2004-12-30.neural.nc
       jjj         diag.wrf.ocean.entry.timeseries.perform.jl          hydat_valding_aall_1000_incl_upnat_1990-01-01_2004-12-30.neural.daily
       jjj         diag.wrf.ocean.entry.timeseries.perform.jl          hydat_valding_aall_1000_incl_upnat_1990-01-01_2004-12-30.neural.monthly
       jjj         diag.wrf.ocean.entry.timeseries.perform.jl          hydat_valding_aall_1000_incl_upnat_1990-01-01_2004-12-30.neural.annualday
       grads -bpc "diag.wrf.ocean.entry.timeseries.perform             hydat_valding_aall_1000_incl_upnat_1990-01-01_2004-12-30.neural"
       jjj         diag.wrf.ocean.entry.timeseries.table.jl          4 hydat_valding_aall_1000_incl_upnat_1990-01-01_2004-12-30.neural.daily
       jjj         diag.wrf.ocean.entry.timeseries.table.jl          4 hydat_valding_aall_1000_incl_upnat_1990-01-01_2004-12-30.neural.monthly
       jjj         diag.wrf.ocean.entry.timeseries.table.jl          4 hydat_valding_aall_1000_incl_upnat_1990-01-01_2004-12-30.neural.annualday
       grads -bpc "diag.wrf.ocean.entry.timeseries.pervald             hydat_valding_aall_1000_incl_upnat_1990-01-01_2004-12-30.neural"

# for regulated watersheds, derive a "natural" neural network estimate for the raw ERA/WRF forcing streamflow
# and sum 3-h estimates to daily, monthly, and annual-average (daily) scales, then tabulate performance and
# plot streamflow (with metrics) for each station
wrkg ; cd hydat.validation/hydat_upstream_regulat
       ln -s ../../DOMAIN_full_full/Fulldom_hires_river.txt Fulldom_hires_river.txt
       grads --quiet -blc "diag.wrf.ocean.entry.timeseries.valding     hydat_valding_aall_1000_incl_upreg .sta.hyshed"        ; di hydat_valding_aall_1000_incl_upreg.sta.hyshed.png
       grads --quiet -blc "diag.wrf.ocean.entry.timeseries.valding     hydat_valding_aall_1000_incl_upreg .sta.hwshed"        ; di hydat_valding_aall_1000_incl_upreg.sta.hwshed.png
       jjj         diag.wrf.ocean.entry.timeseries.neural.assemble.jl  hydat_valding_aall_1000_incl_upreg_1990-01-01_2004-12-30.nc ../../CALUN.era/pest_nwm_DOMAIN_East.FORCING.era_2019-03-31_000214.remsvd.prep.nnet.elu.jld
       jjj         diag.wrf.ocean.entry.timeseries.mean.assemble.jl    hydat_valding_aall_1000_incl_upreg_1990-01-01_2004-12-30.neural.nc
       jjj         diag.wrf.ocean.entry.timeseries.perform.jl          hydat_valding_aall_1000_incl_upreg_1990-01-01_2004-12-30.neural.daily
       jjj         diag.wrf.ocean.entry.timeseries.perform.jl          hydat_valding_aall_1000_incl_upreg_1990-01-01_2004-12-30.neural.monthly
       jjj         diag.wrf.ocean.entry.timeseries.perform.jl          hydat_valding_aall_1000_incl_upreg_1990-01-01_2004-12-30.neural.annualday
       grads -bpc "diag.wrf.ocean.entry.timeseries.perform             hydat_valding_aall_1000_incl_upreg_1990-01-01_2004-12-30.neural"
       jjj         diag.wrf.ocean.entry.timeseries.table.jl          3 hydat_valding_aall_1000_incl_upreg_1990-01-01_2004-12-30.neural.daily
       jjj         diag.wrf.ocean.entry.timeseries.table.jl          3 hydat_valding_aall_1000_incl_upreg_1990-01-01_2004-12-30.neural.monthly
       jjj         diag.wrf.ocean.entry.timeseries.table.jl          3 hydat_valding_aall_1000_incl_upreg_1990-01-01_2004-12-30.neural.annualday
       grads -bpc "diag.wrf.ocean.entry.timeseries.pervald             hydat_valding_aall_1000_incl_upreg_1990-01-01_2004-12-30.neural"

----------------------------------------------------------------------------------//
