dset ^geogrid_outlets.nc
dtype netcdf
undef -888
TITLE WRF Output Grid Coordinates: south_north, west_east
*pdef 310 200 lcc 50.0 -73.0 50.0 50.0 50.0 50.0 -73.0 10000 10000
xdef  310 linear -110.0       0.25
ydef  200 linear   30.0       0.25
zdef   21 linear    0         1
tdef    1 linear 1jan2004 6hr
vars 28
XLAT_M=>xlat          0  t,y,x    latitude on mass grid
XLONG_M=>xlon         0  t,y,x    longitude on mass grid
CLAT=>clat            0  t,y,x    Computational latitude on mass grid
CLONG=>clon           0  t,y,x    Computational longitude on mass grid
LANDMASK=>land        0  t,y,x    Landmask : 1=land, 0=water
LANDUSEF=>luse        0  t,z,y,x  Noah-modified 21-category IGBP-MODIS landuse
LU_INDEX=>luseind     0  t,y,x    Dominant category
HGT_M=>hgtm           0  t,y,x    GMTED2010 30-arc-second topography height
SOILTEMP=>soiltemp    0  t,y,x    Annual mean deep soil temperature
SOILCTOP=>soilctop    0  t,z,y,x  16-category top-layer soil type
SCT_DOM=>sctdom       0  t,y,x    Dominant category in top-layer
SOILCBOT=>soilcbot    0  t,z,y,x  16-category bottom-layer soil type
SCB_DOM=>scbdom       0  t,y,x    Dominant category in bottom-layer
ALBEDO12M=>albedo12m  0  t,z,y,x  MODIS monthly surface albedo
GREENFRAC=>greenfrac  0  t,z,y,x  MODIS FPAR
LAI12M=>lai12m        0  t,z,y,x  MODIS LAI
SNOALB=>snoalb        0  t,y,x    MODIS maximum snow albedo
CON=>con              0  t,y,x    landmask
VAR=>var              0  t,y,x    landmask
OA1=>oa1              0  t,y,x    landmask
OA2=>oa2              0  t,y,x    landmask
OA3=>oa3              0  t,y,x    landmask
OA4=>oa4              0  t,y,x    landmask
OL1=>ol1              0  t,y,x    landmask
OL2=>ol2              0  t,y,x    landmask
OL3=>ol3              0  t,y,x    landmask
OL4=>ol4              0  t,y,x    landmask
VAR_SSO=>varsso       0  t,y,x    Variance of Subgrid Scale Orography
endvars
