dset ^Fulldom_hires_outlets.nc
dtype netcdf
undef -888
TITLE WRF Output Grid Coordinates: south_north, west_east
*pdef  970 670 lcc 50.0 -73.2 490.0 340.0 50.0 50.0 -73.2 3260 3260
xdef 3100 linear -110.0       0.025
ydef 2000 linear   30.0       0.025
zdef    1 linear    0         1
tdef    1 linear 1jan2004 6hr
vars 14
CHANNELGRID=>riv           0  y,x    channels
FLOWDIRECTION=>flodir      0  y,x    channels
FLOWACC=>floacc            0  y,x    channels
TOPOGRAPHY=>topo           0  y,x    channels
RETDEPRTFAC=>retdeprtfac   0  y,x    channels
OVROUGHRTFAC=>ovroughrtfac 0  y,x    channels
*byte STREAMORDER(y, x) ;
frxst_pts=>frxst           0  y,x    channels
basn_msk=>basnmsk          0  y,x    channels
LAKEGRID=>lake             0  y,x    channels
landuse=>landuse           0  y,x    channels
LKSATFAC=>lksatfac         0  y,x    channels
LATITUDE=>clat             0  y,x    latitude
LONGITUDE=>clon            0  y,x    longitude
CHANNELGRID=>riv           0  y,x    channels
endvars
