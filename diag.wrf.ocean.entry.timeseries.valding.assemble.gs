* This script is designed to plot a watershed map.
* It can be executed using a command like
*
*     grads -blc "diag.wrf.ocean.entry.timeseries.valding.assemble hydat_valding_aall_1000_incl .sta.hyshed"
*
* - RD Jun 2023, Jan 2024.

function plot(args)

fpz = "xyzzy.forgetit."subwrd(args,1) ; "!echo $HOME > "fpz ; line = read(fpz) ; home = sublin(line,2) ; ret = close(fpz) ; "!rm "fpz
fila = subwrd(args,1)".txt"
filp = subwrd(args,1)""subwrd(args,2)".png"
tail =                 subwrd(args,2)

"clear"
"set grads off"
"set grid off"
"set xlab on"
"set ylab on"
"set digsiz 0.09"
"set dignum 2"
"set mpdset hires" ; "set mpt 1 off" ; "set mpt 2 off"
"set rgb  65  30 100  50  50"
"set rgb  66  30 100  50"
"set rgb  67  30 130 250 100"
"set rgb  68  30 130 250"

"sdfopen "home"/work/workg/HYDROSHEDS/hgt.sfc.nc"
minlat = 35 ; minlon = -145
maxlat = 75 ; maxlon =  -50
minlat = 40.5 ; minlon =  -93.5
maxlat = 63 ; maxlon =  -52
*minlat = 45.1 ; minlon =  -66.6
*maxlat = 45.5 ; maxlon =  -65.8
"set lat "minlat-0.2" "maxlat+0.2
"set lon "minlon-0.2" "maxlon+0.2
"set xlint "inner_labint((maxlon - minlon) / 5)
"set ylint "inner_labint((maxlat - minlat) / 5)
"set clevs 9e9" ; "d hgt"

"q gxinfo" ; _gxinfo = result
line3 = sublin(_gxinfo,3)
line4 = sublin(_gxinfo,4)
x1 = subwrd(line3,4)
x2 = subwrd(line3,6)
y1 = subwrd(line4,4)
y2 = subwrd(line4,6)
"set clip "x1" "x2" "y1" "y2

"set rgb  44 181 101  29"
"set rgb  45 222 170 136"
"set lwid 98  3"        ; "set line 44 1 98" ; "draw shp "home"/work/workg/HYDROSHEDS/dams/GRanD_reservoirs_v1_1"
"set shpopts -1 3 0.05" ; "set line  2 1 97" ; "draw shp "home"/work/workg/HYDROSHEDS/dams/GRanD_dams_v1_1"
"set clip "x1" "x2" "y1" "y2

if (1 = 1)
filc = "Fulldom_hires_river.txt"
"set line 4 1 1"
filestat = read(filc)
while (sublin(filestat,1) = 0)
  line = sublin(filestat,2)
  linf = subwrd(line,1)" "subwrd(line,2)" "subwrd(line,3)" "subwrd(line,4)
  "draw line "inner_disp_box(linf)
  filestat = read(filc)
endwhile
filestat = close(filc)
endif

if (1 = 1)
nn = 1 ; filo = ""
filestat = read(fila)
while (sublin(filestat,1) = 0)
  line = sublin(filestat,2)
  filb =                       subwrd(line,1)""tail
  filc = "../hydat.validation/"subwrd(line,2)""tail
  numa = subwrd(line,3)
  hlat = subwrd(line,4)
  hlon = subwrd(line,5)
  numb = subwrd(line,6)
  labl = substr(filb,6,8)

  if (filb != filo)
    filestat =  read(filb) ; linf = sublin(filestat,2)
    minlata = subwrd(linf,1) ; maxlata = subwrd(linf,2)
    minlona = subwrd(linf,3) ; maxlona = subwrd(linf,4) ; lins = subwrd(linf,5)
    a = 0 ; pbord = ""
    while (a < lins)
      filestat = read(filb) ; pbord = pbord" "sublin(filestat,2)
      a = a + 1
    endwhile
    filestat = close(filb)
    inner_pbord = inner_disp_box(pbord)
  endif

  if (numb = 1)
    filestat =  read(filc) ; linf = sublin(filestat,2)
    minlata = subwrd(linf,1) ; maxlata = subwrd(linf,2)
    minlona = subwrd(linf,3) ; maxlona = subwrd(linf,4) ; lins = subwrd(linf,5)
    a = 0 ; hbord = ""
    while (a < lins)
      filestat = read(filc) ; hbord = hbord" "sublin(filestat,2)
      a = a + 1
    endwhile
    filestat = close(filc)
    inner_hbord = inner_disp_box(hbord)

*   if (numa = 0) ; "set line 65 1 1" ; "set grads off" ; "draw polyf "inner_hbord ; else
*                   "set line 67 1 1" ; "set grads off" ; "draw polyf "inner_hbord ; endif
*   if (numb = 0) ; "set line  1 1 2" ; "set grads off" ; "draw  line "inner_hbord ; endif
    "set line 67 1 1" ; "set grads off" ; "draw polyf "inner_pbord
*   "set line  1 1 4" ; "set grads off" ; "draw  line "inner_pbord
    "set line 65 1 1" ; "set grads off" ; "draw polyf "inner_hbord
    "set line  1 1 2" ; "set grads off" ; "draw  line "inner_hbord
  endif
  "set line  1 1 2" ; "set grads off" ; "draw  line "inner_pbord
  filo = filb
  filestat = read(fila)
  nn = nn + 1
endwhile
filestat = close(fila)
endif

* wrfh_09940645 hydat_02QB001  0     48.77471924    -67.54193878  1 [09940645 pour_point]   1646.30000000   1638.90000000   1638.40000000   1641.20000000     48.80937195    -67.54596710   1661.70000000

if (1 = 1)
nn = 1
filestat = read(fila)
while (sublin(filestat,1) = 0)
  line = sublin(filestat,2)
  filb =                       subwrd(line,1)""tail
  filc = "../hydat.validation/"subwrd(line,2)""tail
  numa = subwrd(line,3)
  hlat = subwrd(line,4)
  hlon = subwrd(line,5)
  numb = subwrd(line,6)
  plat = subwrd(line,13)
  plon = subwrd(line,14)
* a = 1 ; while (substr(line,a,1) != "[") ; a = a + 1 ; endwhile ; a = a + 1 + 8
* b = a ; while (substr(line,b,1) != "]") ; b = b + 1 ; endwhile ; b = b - a
  labl = substr(filb,6,8)

* if (numb < 0) ; "q w2xy "hlon" "hlat ; rec = sublin(result,1) ; xa  = subwrd(rec,3) ; ya  = subwrd(rec,6) ; "set line  9 1 8" ; "draw mark 3 "xa" "ya" 0.045" ; endif
* if (numb = 0) ; "q w2xy "hlon" "hlat ; rec = sublin(result,1) ; xa  = subwrd(rec,3) ; ya  = subwrd(rec,6) ; "set line  1 1 8" ; "draw mark 3 "xa" "ya" 0.065" ; endif
  if (numb = 1)
    "q w2xy "hlon" "hlat ; rec = sublin(result,1) ; xa  = subwrd(rec,3) ; ya  = subwrd(rec,6) ; "set line  0 1 5" ; "draw mark 2 "xa" "ya" 0.090"
    "q w2xy "plon" "plat ; rec = sublin(result,1) ; xa  = subwrd(rec,3) ; ya  = subwrd(rec,6) ; "set line  1 1 8" ; "draw mark 3 "xa" "ya" 0.065"
    nn = nn + 1
  endif
* if (numb = 2) ; "q w2xy "hlon" "hlat ; rec = sublin(result,1) ; xa  = subwrd(rec,3) ; ya  = subwrd(rec,6) ; "set line 66 1 8" ; "draw mark 3 "xa" "ya" 0.035" ; endif
* if (nn = 1 | nn = 4 | nn = 9 | nn = 10 | nn = 11 | nn = 12 | nn = 18 | nn = 22 | nn = 23 | nn = 25)
*   "set strsiz 0.09" ; "set string 1 r 6" ; "draw string "xa-0.1" "ya+0.1" "labl
* else
*   if (nn = 24)
*     "set strsiz 0.09" ; "set string 1 l 6" ; "draw string "xa+0.1" "ya-0.1" "labl
*   else
*RD   if (numb = 0) ; "set strsiz 0.15" ; "set string 1 l 3" ; "draw string "xa+0.03" "ya+0.03" "labl ; endif
*   endif
* endif
  filestat = read(fila)
endwhile
filestat = close(fila)
endif

nn = nn - 1
"set clip 0 11 0 8.5"
"set strsiz 0.19" ; "set string 1 c 6  0" ; "draw string 5.5 7.8 "nn" Pour Points and Upstream HYDAT Stations"
xref = 8.4 ; yref = 7.1 ; "set lwid 88 15" ; "set lwid 89 23"
xa = xref ; xb = xref+0.0 ; ya = yref-0.0 ; "set line  1 1 88" ; "draw line "xa" "ya" "xb" "ya
                            ya = yref-0.3 ; "set line 67 1 89" ; "draw line "xa" "ya" "xb" "ya
                            ya = yref-0.3 ; "set line 65 1 89" ; "draw line "xa" "ya" "xb" "ya
                            ya = yref-0.3 ; "set line  0 1  7" ; "draw mark 2 "xa" "ya" 0.13"
                            ya = yref-0.6 ; "set line 44 1 88" ; "draw line "xa" "ya" "xb" "ya
                            ya = yref-0.9 ; "set line  2 1 88" ; "draw line "xa" "ya" "xb" "ya
            xa = xref+0.2 ; ya = yref-0.0 ; "set strsiz 0.16" ; "set string 1 l 6" ; "draw string "xa" "ya" Pour Points"
                            ya = yref-0.3 ; "set strsiz 0.16" ; "set string 1 l 6" ; "draw string "xa" "ya" Stations"
                            ya = yref-0.6 ; "set strsiz 0.16" ; "set string 1 l 6" ; "draw string "xa" "ya" Reservoirs"
                            ya = yref-0.9 ; "set strsiz 0.16" ; "set string 1 l 6" ; "draw string "xa" "ya" Dams"

say "gxprint "filp" png white x1100 y850"
    "gxprint "filp" png white x1100 y850"
"quit"


function inner_labint(args)
  diff = subwrd(args,1)
  if                (diff > 7.50) ; cint = 10   ; endif
  if (diff <= 7.50 & diff > 3.00) ; cint =  5   ; endif
  if (diff <= 3.00 & diff > 1.50) ; cint =  2   ; endif
  if (diff <= 1.50 & diff > 0.75) ; cint =  1   ; endif
  if (diff <= 0.75 & diff > 0.30) ; cint =  0.5 ; endif
  if (diff <= 0.30 & diff > 0.15) ; cint =  0.2 ; endif
  if (diff <= 0.15)               ; cint =  0.1 ; endif
return(cint)

function inner_disp_box(args)
  a = 1
  locs = ""
  lata = subwrd(args,a)
  lona = subwrd(args,a+1)
  while lona != ""
    "q w2xy "lona" "lata
    xa = subwrd(result,3)
    ya = subwrd(result,6)
    locs = locs" "xa" "ya
    a = a + 2
    lata = subwrd(args,a)
    lona = subwrd(args,a+1)
  endwhile
return(locs)
