#=
 = Save the WRF-Hydro river network as a text file based on grids in the
 = Fulldom_hires.nc file.  River segments are unrelated - RD Apr 2023.
 =#

using My, Printf, NetCDF, LibGEOS

if     in("WRFSDOM", keys(ENV)) && ENV["WRFSDOM"] == "_AuxF"
  const FATS           =  225                           # number of  latitudes in Fulldom_hires.nc
  const FONS           =  225                           # number of longitudes in Fulldom_hires.nc
elseif in("WRFSDOM", keys(ENV)) && ENV["WRFSDOM"] == "_Cali"
  const FATS           =  775                           # number of  latitudes in Fulldom_hires.nc
  const FONS           = 1425                           # number of longitudes in Fulldom_hires.nc
elseif in("WRFSDOM", keys(ENV)) && ENV["WRFSDOM"] == "_East"
  const FATS           = 1350                           # number of  latitudes in Fulldom_hires.nc
  const FONS           = 1550                           # number of longitudes in Fulldom_hires.nc
else
  const FATS           = 2150                           # number of  latitudes in Fulldom_hires.nc
  const FONS           = 2750                           # number of longitudes in Fulldom_hires.nc
end
const EARTH            = 6.371e6                        # mean Earth radius (m)
const D2R              = pi / 180.0                     # degrees to radians conversion
const MISS             = -1                             # generic missing value

if (argc = length(ARGS)) != 1
  print("\nUsage: jjj $(basename(@__FILE__)) Fulldom_hires.nc\n\n")
  exit(1)
end
fila = ARGS[1]
filb = ARGS[1][1:end-3] * "_river.txt"

function subroute()                                                           # read (flipped) grids of flow dir
  flat = Array{Float32}(undef, FONS, FATS)                                    # and indices of rivers and lakes
  flon = Array{Float32}(undef, FONS, FATS)                                    # (evidently a few gridboxes are
  fdir =   Array{Int16}(undef, FONS, FATS)                                    # ocean/lake or river/lake, and
  friv = Set{Tuple{Int32, Int32}}()                                           # respectively, these are treated
# flak = Set{Tuple{Int32, Int32}}()                                           # only as ocean or river below)
  foce = Set{Tuple{Int32, Int32}}()
  temp = ncread(fila,      "LATITUDE", start=[1,1], count=[-1,-1])
  for a = 1:FATS, b = 1:FONS  flat[b,a] = temp[b,FATS+1-a]  end
  temp = ncread(fila,     "LONGITUDE", start=[1,1], count=[-1,-1])
  for a = 1:FATS, b = 1:FONS  flon[b,a] = temp[b,FATS+1-a]  end
  temp = ncread(fila, "FLOWDIRECTION", start=[1,1], count=[-1,-1])
  for a = 1:FATS, b = 1:FONS  fdir[b,a] = temp[b,FATS+1-a]  end
  temp = ncread(fila,   "CHANNELGRID", start=[1,1], count=[-1,-1])
  for a = 1:FATS, b = 1:FONS  temp[b,a] >= 0 && push!(friv, (Int32(b), Int32(FATS+1-a)))  end
  temp = ncread(fila,      "LAKEGRID", start=[1,1], count=[-1,-1])
  for a = 1:FATS, b = 1:FONS  temp[b,a] >= 0 && push!(friv, (Int32(b), Int32(FATS+1-a)))  end
# for a = 1:FATS, b = 1:FONS  temp[b,a] >= 0 && push!(flak, (Int32(b), Int32(FATS+1-a)))  end
  for a = 1:FATS, b = 1:FONS
#                      fdir[b,a] == 0   && (facc[b,a] = typemin(Int32))
#   in((b,a), flak) && fdir[b,a] == 0   && (setdiff!(flak, [(b,a)]) ; push!(foce, (b,a)))
    in((b,a), friv) && fdir[b,a] == 0   && (setdiff!(friv, [(b,a)]) ; push!(foce, (b,a)))
#   in((b,a), friv) &&  in((b,a), flak) &&  setdiff!(flak, [(b,a)])
  end
  @printf("\n%9d gridboxes define the       river network\n",   length(friv))
# @printf(  "%9d gridboxes define the inland lake network\n",   length(flak))
# @printf(  "%9d gridboxes define the  ocean lake network\n\n", length(foce))

  rech = Array{Tuple{Int32, Int32, Int32, Int32}}(undef, 0)                   # identify all river reaches by the
  for (b,a) in friv                                                           # position of adjacent gridboxes
    fdir[b,a] == 128 && in((b+1,a+1), friv) && (push!(rech, (b,a,b+1,a+1)))
    fdir[b,a] ==  64 && in((b  ,a+1), friv) && (push!(rech, (b,a,b  ,a+1)))
    fdir[b,a] ==  32 && in((b-1,a+1), friv) && (push!(rech, (b,a,b-1,a+1)))
    fdir[b,a] ==  16 && in((b-1,a  ), friv) && (push!(rech, (b,a,b-1,a  )))
    fdir[b,a] ==   8 && in((b-1,a-1), friv) && (push!(rech, (b,a,b-1,a-1)))
    fdir[b,a] ==   4 && in((b  ,a-1), friv) && (push!(rech, (b,a,b  ,a-1)))
    fdir[b,a] ==   2 && in((b+1,a-1), friv) && (push!(rech, (b,a,b+1,a-1)))
    fdir[b,a] ==   1 && in((b+1,a  ), friv) && (push!(rech, (b,a,b+1,a  )))
  end
  @printf("%9d gridboxes define the       river segments\n", length(rech))

  fpa = My.ouvre(filb, "w")                                                   # then save the reach segments
  for (b,a,d,c) in rech
    form = @sprintf("%9.5f %10.5f %9.5f %10.5f %6d %6d %6d %6d\n", flat[b,a], flon[b,a], flat[d,c], flon[d,c], a, b, c, d)
    write(fpa, form)
  end
  close(fpa)
end

subroute()
exit(0)

#=
  numb = 0                                                                    # define the ocean by fdir (-1) excluding
  for a = 1:FATS, b = 1:FONS                                                  # ocean entry points and inland sinks (0)
    fdir[b,a] == 0 && (fdir[b,a] = Int16(-1) ; numb += 1)                     # where river flow accumulation is a peak
  end
  @printf("%9d gridboxes define the ocean (values of -1)\n", numb)

  fpa  = My.ouvre(fila, "r")                                                  # use the HydroSHEDS river reach location
  lins = readlines(fpa, keep = true) ; close(fpa)                             # (next to the HYDAT station)
  hlat = parse(Float64, split(lins[21])[2])
  hlon = parse(Float64, split(lins[22])[2])
  fups = Set{Tuple{Int32, Int32}}()

  latmin, latind = findmin(abs.(flat .- hlat))                                # to find a corresponding grid location
  lonmin, lonind = findmin(abs.(flon .- hlon))
  @printf("distance between grid %9.5f %9.5f\n", hlat, hlon)
  @printf(" and station location %9.5f %9.5f",               flat[latind], flon[lonind])
  @printf(" is %9.5f km\n",           latlondist(hlat, hlon, flat[latind], flon[lonind]) / 1000)

  function watershed(b::Int32, a::Int32)
    numb = 0
    if 2 < a < LATS-1 && 2 < b < LONS-1
#    (a == 1 || a == LATS || b == 1 || b == LONS) && return(numb)
      fdir[b-1,a-1] == 128 && !in((b-1,a-1), fups) && (push!(fups, (b-1,a-1)) ; numb += 1)
      fdir[b  ,a-1] ==  64 && !in((b  ,a-1), fups) && (push!(fups, (b  ,a-1)) ; numb += 1)
      fdir[b+1,a-1] ==  32 && !in((b+1,a-1), fups) && (push!(fups, (b+1,a-1)) ; numb += 1)
      fdir[b-1,a  ] ==   1 && !in((b-1,a  ), fups) && (push!(fups, (b-1,a  )) ; numb += 1)
      fdir[b+1,a  ] ==  16 && !in((b+1,a  ), fups) && (push!(fups, (b+1,a  )) ; numb += 1)
      fdir[b-1,a+1] ==   2 && !in((b-1,a+1), fups) && (push!(fups, (b-1,a+1)) ; numb += 1)
      fdir[b  ,a+1] ==   4 && !in((b  ,a+1), fups) && (push!(fups, (b  ,a+1)) ; numb += 1)
      fdir[b+1,a+1] ==   8 && !in((b+1,a+1), fups) && (push!(fups, (b+1,a+1)) ; numb += 1)
    end
    numb
  end

  push!(fups, (lonind,latind))                                                # then identify the remainder of
  numb = totl = 1                                                             # the upstream watershed using fdir
  while (numb > 0)
    numb = 0
    for (b,a) in fups
      numb += watershed(b, a)
    end
    totl += numb
  end

  area = 0.0                                                                  # calculate watershed area
#=ares = (DELL * EARTH / 1e3 * D2R)^2 * cos.(flat * pi / 180)                 # and a bounding lat/lon box
  minlat = minlon =  9999.0
  maxlat = maxlon = -9999.0
  for (b,a) in fups
    minlat > flat[a] && (minlat = flat[a])
    maxlat < flat[a] && (maxlat = flat[a])
    minlon > flon[b] && (minlon = flon[b])
    maxlon < flon[b] && (maxlon = flon[b])
    area += ares[a]
  end  =#
  @printf("an area of %9.2f km2 (%9d gridboxes) define the station and its upstream watershed\n", area, totl)
#=
  fpa   = My.ouvre(fila, "r")                                                 # update the station text file
  lins  = readlines(fpa, keep = true) ; close(fpa)
  form  = "" ; for a =  1:29           form *= lins[a]  end
  hrea  = parse(Float64, split(lins[30])[2])
  form *= @sprintf("%-24s %f      15s_km2_with_no_holes %9.1f      15s_count_gridbox %9d\n", "UPLAND_SKM", hrea, area, totl)
               for a = 31:length(lins) form *= lins[a]  end
  fpa   = My.ouvre(fila, "w")
  write(fpa, form) ; close(fpa)
=#
  bord = Array{Tuple{Int32, Int32, Int8}}(undef, 0)                           # identify the watershed border
  latbeg = lonbeg = -9999                                                     # starting with a northern point
  for (b,a) in fups                                                           # and search counterclockwise
    a > latbeg && (latbeg = a ; lonbeg = b)                                   # along the outer gridbox wall
  end                                                                         # (where w = 0123 = NWSE)
  push!(bord, (lonbeg,latbeg,0))

  function shedborder((b,a,w)::Tuple{Int32,Int32,Int8})
    if     w == 0
      !in((b  ,a+1), fups) && in((b-1,a+1), fups) && !in((b-1,a+1,3), bord) && (push!(bord, (b-1,a+1,3)) ; return(true))
      !in((b-1,a+1), fups) && in((b-1,a  ), fups) && !in((b-1,a  ,0), bord) && (push!(bord, (b-1,a  ,0)) ; return(true))
      !in((b-1,a  ), fups) &&                        !in((b  ,a  ,1), bord) && (push!(bord, (b  ,a  ,1)) ; return(true))
    elseif w == 1
      !in((b-1,a  ), fups) && in((b-1,a-1), fups) && !in((b-1,a-1,0), bord) && (push!(bord, (b-1,a-1,0)) ; return(true))
      !in((b-1,a-1), fups) && in((b  ,a-1), fups) && !in((b  ,a-1,1), bord) && (push!(bord, (b  ,a-1,1)) ; return(true))
      !in((b  ,a-1), fups) &&                        !in((b  ,a  ,2), bord) && (push!(bord, (b  ,a  ,2)) ; return(true))
    elseif w == 2
      !in((b  ,a-1), fups) && in((b+1,a-1), fups) && !in((b+1,a-1,1), bord) && (push!(bord, (b+1,a-1,1)) ; return(true))
      !in((b+1,a-1), fups) && in((b+1,a  ), fups) && !in((b+1,a  ,2), bord) && (push!(bord, (b+1,a  ,2)) ; return(true))
      !in((b+1,a  ), fups) &&                        !in((b  ,a  ,3), bord) && (push!(bord, (b  ,a  ,3)) ; return(true))
    else
      !in((b+1,a  ), fups) && in((b+1,a+1), fups) && !in((b+1,a+1,2), bord) && (push!(bord, (b+1,a+1,2)) ; return(true))
      !in((b+1,a+1), fups) && in((b  ,a+1), fups) && !in((b  ,a+1,3), bord) && (push!(bord, (b  ,a+1,3)) ; return(true))
      !in((b  ,a+1), fups) &&                        !in((b  ,a  ,0), bord) && (push!(bord, (b  ,a  ,0)) ; return(true))
    end
    false
  end
  while  shedborder(bord[end]) != false  end

  numb = 1                                                                    # finally save the border to text
  (latnow,lonnow) = (latbeg,lonbeg)                                           # (with northen point duplicated)
  form = @sprintf("%9.5f %10.5f\n", flat[latnow], flon[lonnow])               # but using only gridbox centers
  for (b,a,w) in bord
    if (latnow,lonnow) != (a,b)
      form *= @sprintf("%9.5f %10.5f\n", flat[a], flon[b])
      (latnow,lonnow) = (a,b)
      numb += 1
    end
  end
  (latnow,lonnow) != (latbeg,lonbeg) && (form *= @sprintf("%9.5f %10.5f\n", flat[latbeg], flon[lonbeg]) ; numb += 1)
  forn  = @sprintf("%9.5f %9.5f %10.5f %10.5f %d\n", minlat, maxlat, minlon, maxlon, numb)
  fpa   = My.ouvre(filb, "w") ; write(fpa, forn) ; write(fpa, form) ; close(fpa)
=#
