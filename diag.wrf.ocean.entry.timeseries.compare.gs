* This script is designed to plot a simple timeseries.
* It can be executed using a command like
*
*     grads -blc "diag.wrf.ocean.entry.timeseries.compare hydat_03JB004.nc 2018100100 00.CHRTOUT_GRID1 1096 137 166"
*
* - RD November 2012

function plot(args)
fila = "streamflow."subwrd(args,2)""subwrd(args,3)"."subwrd(args,4)"."subwrd(args,5)"."subwrd(args,6)"-0.nc"
filb = "streamflow."subwrd(args,2)""subwrd(args,3)"."subwrd(args,4)"."subwrd(args,5)"."subwrd(args,6)"-1.nc"
filc = "streamflow."subwrd(args,2)""subwrd(args,3)"."subwrd(args,4)"."subwrd(args,5)"."subwrd(args,6)"-2.nc"
fild = "streamflow."subwrd(args,2)""subwrd(args,3)"."subwrd(args,4)"."subwrd(args,5)"."subwrd(args,6)"-3.nc"
file =              subwrd(args,1)
filf = "streamflow."subwrd(args,2)""subwrd(args,3)"."subwrd(args,4)"."subwrd(args,5)"."subwrd(args,6)".png"
filg = "streamflow."subwrd(args,2)""subwrd(args,3)"."subwrd(args,4)"."subwrd(args,5)"."subwrd(args,6)".txt"

line =  read(filg) ; subl = sublin(line,2) ; num.1 = subwrd(subl,1) ; nse.1 = subwrd(subl,2)
line =  read(filg) ; subl = sublin(line,2) ; num.2 = subwrd(subl,1) ; nse.2 = subwrd(subl,2)
line =  read(filg) ; subl = sublin(line,2) ; num.3 = subwrd(subl,1) ; nse.3 = subwrd(subl,2)
line =  read(filg) ; subl = sublin(line,2) ; num.4 = subwrd(subl,1) ; nse.4 = subwrd(subl,2)
ret  = close(filg)

XX = subwrd(args,6)
if (XX="1240") ; lab = "ChurchillRiver"; vmax =  3000 ; endif
if (XX="1934") ; lab = "AuxFeuilles"   ; vmax =  9000 ; endif
if (XX="1940") ; lab = "AuxFeuilles"   ; vmax =  9000 ; endif
if (XX="1998") ; lab = "Caniapiscau"   ; vmax = 40000 ; endif
if (XX="2019") ; lab = "ALaBeleine"    ; vmax =  9000 ; endif
if (XX="2058") ; lab = "George"        ; vmax = 12000 ; endif
if (XX="2218") ; lab = "Moisie"        ; vmax =  4500 ; endif
if (XX="2292") ; lab = "Romaine"       ; vmax =  3000 ; endif
if (XX="2363") ; lab = "Natashquan"    ; vmax =  3500 ; endif
if (XX="2428") ; lab = "PetitMecatina" ; vmax =  3500 ; endif
if (XX="2107") ; lab = "StLawrence"    ; vmax = 60000 ; endif
if (XX="2263") ; lab = "Bonaventure"   ; vmax =   900 ; endif
if (XX="2238") ; lab = "Restigouche"   ; vmax =  3000 ; endif
if (XX="2305") ; lab = "Miramachi"     ; vmax =  2500 ; endif
if (XX="2315") ; lab = "StJohn"        ; vmax = 12000 ; endif
if (XX="2226") ; lab = "Penobscot"     ; vmax =  3500 ; endif
if (XX="2201") ; lab = "Kennebec"      ; vmax =  4500 ; endif
if (XX="2175") ; lab = "Merrimack"     ; vmax =  3000 ; endif
*filb = subwrd(args,2)"."subwrd(args,3)""subwrd(args,4)"."subwrd(args,5)"."lab".png"
                 lab = "AuxFeuilles"   ; vmax =  4000

"sdfopen "fila
"sdfopen "filb
"sdfopen "filc
"sdfopen "fild
"sdfopen "file
*"set t 1 1475"
"q file" ; ret = sublin(result,5) ; tims = subwrd(ret,12) ; "set t 1 "tims+1
"set grads off"
"set grid off"
"set vrange 0 "vmax
"set xlopts 1 3 0.18"
"set ylopts 1 3 0.18"
*"d smth9(smth9(smth9(tmp)))"
*"d tmp/1000"
*if (subwrd(args,2) = "streamflow")

col.1 =  1 ; sty.1 = 1
col.2 =  2 ; sty.2 = 1
col.3 =  3 ; sty.3 = 1
col.4 =  4 ; sty.4 = 1
col.5 =  5 ; sty.5 = 1

"set lwid 99 15" ; "set cthick 99"
"set ccolor "col.5 ; "set cstyle "sty.5 ; "set cmark 0" ; "set gxout line" ; "set grads off" ; "d flow.5"
"set cthick 8"
"set ccolor "col.1 ; "set cstyle "sty.1 ; "set cmark 0" ; "set gxout line" ; "set grads off" ; "d tmp.1"
"set ccolor "col.2 ; "set cstyle "sty.2 ; "set cmark 0" ; "set gxout line" ; "set grads off" ; "d tmp.2"
"set ccolor "col.3 ; "set cstyle "sty.3 ; "set cmark 0" ; "set gxout line" ; "set grads off" ; "d tmp.3"
"set ccolor "col.4 ; "set cstyle "sty.4 ; "set cmark 0" ; "set gxout line" ; "set grads off" ; "d tmp.4"

*else
*"close 1"
*"sdfopen obsflow.200701010000.CHRTOUT_GRID1.2192.1031.1240.nc"
*  "set t 9131 11324"
*  "set ccolor 2"
*  "d flow"
*endif

"q gxinfo"
line3 = sublin(result,3)
line4 = sublin(result,4)
x1 = subwrd(line3,4)
x2 = subwrd(line3,6)
y1 = subwrd(line4,4)
y2 = subwrd(line4,6)
xmid = (x1 + x2) / 2.0
ymid = (y1 + y2) / 2.0
say x1" "x2" "y1" "y2" "xmid ;* 2 10.5 0.75 7.75 6.25

"set string 1 bc 5" ; "set strsiz 0.23"
*"draw string "xmid" "y2+0.2" WRF-Hydro at "lab" (mSv)"
if (subwrd(args,2) = "streamflow")
  "draw string "xmid" "y2+0.2" WRF-Hydro at "lab" (m`a3`ns)"
*else
  "set string 2"
  "draw string "xmid" "y2-0.5" Observation at 03JB004"
* "draw string "xmid" "y2+0.2" Observation at "lab" (m`a3`ns)"
endif

say "gxprint "filf" png white x1100 y850"
    "gxprint "filf" png white x1100 y850"
"quit"
