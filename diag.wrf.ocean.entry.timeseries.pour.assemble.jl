#=
 = Create a NetCDF file that accommodates an ensemble of four three-hourly WRF-Hydro streamflow
 = estimates (WRF/ERA forcing, raw/neural-network output), and daily HYDAT observations, as well
 = as performance metrics.  Raw and neural network WRF-Hydro streamflow estimates are taken from
 = source NetCDF (a WRF-Hydro substitute is given for the HYDAT data) - RD Feb 2024.
 =#

using My, Printf, NetCDF

const NPAR             = 4                              # number of WRF-Hydro streamflow estimates (WRF/ERA forcing, raw/neural-network)
const DELT             = 3                              # timestep (hours)
const HORA             = @sprintf("-0%d",      DELT)    # first hour of the first day of WRF-Hydro data
const HORB             = @sprintf("-%2d", 24 - DELT)    #  last hour of the  last day of WRF-Hydro data
const MISS             = -9999.0                        # generic missing value

if (argc = length(ARGS)) != 8
  print("\nUsage: jjj $(basename(@__FILE__)) 1990-01-01 2004-12-30 ../SPINUP.wrf/ ../SPINUP.era/ run_DOMAIN_full_full_FORCING wrf era 3h_Severn_Hudson\n\n")
  exit(1)
end
daya = ARGS[1]
dayb = ARGS[2]
fila = @sprintf("%s%s.%s_%s_%s_%s.txt",       ARGS[3], ARGS[5], ARGS[6], ARGS[1], ARGS[2], ARGS[8])
filb = @sprintf("%s%s.%s_%s_%s_%s.nc",        ARGS[3], ARGS[5], ARGS[6], ARGS[1], ARGS[2], ARGS[8])
filc = @sprintf("%s%s.%s_%s_%s_%s.neural.nc", ARGS[3], ARGS[5], ARGS[6], ARGS[1], ARGS[2], ARGS[8])
fild = @sprintf("%s%s.%s_%s_%s_%s.txt",       ARGS[4], ARGS[5], ARGS[7], ARGS[1], ARGS[2], ARGS[8])
file = @sprintf("%s%s.%s_%s_%s_%s.nc",        ARGS[4], ARGS[5], ARGS[7], ARGS[1], ARGS[2], ARGS[8])
filf = @sprintf("%s%s.%s_%s_%s_%s.neural.nc", ARGS[4], ARGS[5], ARGS[7], ARGS[1], ARGS[2], ARGS[8])
filg = @sprintf( "%s.all_%s_%s_%s.txt",                ARGS[5],          ARGS[1], ARGS[2], ARGS[8])
filh = @sprintf( "%s.all_%s_%s_%s.nc",                 ARGS[5],          ARGS[1], ARGS[2], ARGS[8])
fpa = My.ouvre(fila, "r", false) ; lina = readlines(fpa) ; close(fpa)

hors = Array{Float64}(undef,0)                                                # get WRF-Hydro times at DELT intervals
hmsk = Array{Bool   }(undef,0)                                                # and a HYDAT mask at daily intervals
hora = daya * HORA ; tima = My.datesous("1900-01-01-00", hora, "hr")
horb = dayb * HORB ; timb = My.datesous("1900-01-01-00", horb, "hr")
horc = hora        ; timc = tima
while timc <= timb
  global horc, timc       ; push!(hors, timc)
  horc[end-1:end] == "12" ? push!(hmsk, true) : push!(hmsk, false)
  horc = dateadd(horc, DELT, "hr")
  timc = My.datesous("1900-01-01-00", horc, "hr")
end
days  = hors[hmsk] ; nday = length(days)
daya *= "-12"      ; nhor = length(hors)
dayb *= "-12"
@printf("\nreading %5d WRF-Hydro values between %s and %s at %2d-h intervals\n", nhor, hora, horb, DELT)
@printf(  "reading %5d     HYDAT values between %s and %s at %2d-h intervals\n", nday, daya, dayb,   24)

#    2640905   264   905   446     55.90045166    -87.16336823   264   906   445     55.91839981    -87.16201782
# hydat_01BO001     5479     46.73600006    -65.82560730    5456.3 1 [01BO001 SOUTHWEST MIRAMICHI]

fora = ""                                                                     # read the station locations
for line in lina
  global fora
  temp = split(line) ; form = ""
  name =                temp[1]
  loni = parse(  Int64, temp[2])
  lati = parse(  Int64, temp[3])
  latf = parse(Float64, temp[5])
  lonf = parse(Float64, temp[6])
  stem = @sprintf("wrfh_%8s", name)
  stem = replace(stem, ' ' => '0')
  line = @sprintf("%s %8d %15.8f %15.8f %9.1f %d [%s WRF-Hydro]\n", stem, MISS, latf, lonf, MISS,    0, stem[6:end])
# line = @sprintf("%s %8d %15.8f %15.8f %9.1f %d [%s WRF-Hydro]\n", stem, numb, hlat, hlon, area, rhbn, stem[6:end])
  fora *= line
end
nsta = length(lina)
@printf("reading %5d station%s\n", nsta, nsta != 1 ? "s" : "")
fpa = My.ouvre(filg, "w") ; write(fpa, fora) ; close(fpa)

function nccreer(fn::AbstractString, tims::Array{Float64,1}, lats::Array{Float64,1}, lons::Array{Float64,1})
  nctim = NcDim("time", length(tims), atts = Dict{Any,Any}("units"=>"hours since 1900-01-01 00:00:0.0"), values = tims)
  nclat = NcDim( "lat", length(lats), atts = Dict{Any,Any}("units"=>"degrees_north"),                    values = lats)
  nclon = NcDim( "lon", length(lons), atts = Dict{Any,Any}("units"=> "degrees_east"),                    values = lons)
  ncvrs = Array{NetCDF.NcVar}(undef, 16)
  ncvrs[ 1] = NcVar("hydt", [       nclat, nctim], atts = Dict{Any,Any}("units"=>"m3/s", "missing_value"=>MISS), t=Float64, compress=-1)
  ncvrs[ 2] = NcVar("hydi", [       nclat, nctim], atts = Dict{Any,Any}("units"=>"none", "missing_value"=>MISS), t=Float64, compress=-1)
  ncvrs[ 3] = NcVar("flow", [nclon, nclat, nctim], atts = Dict{Any,Any}("units"=>"m3/s", "missing_value"=>MISS), t=Float64, compress=-1)
  ncvrs[ 4] = NcVar("para", [nclon,        nctim], atts = Dict{Any,Any}("units"=>"none", "missing_value"=>MISS), t=Float64, compress=-1)
  ncvrs[ 5] = NcVar("parb", [nclon,        nctim], atts = Dict{Any,Any}("units"=>"none", "missing_value"=>MISS), t=Float64, compress=-1)
  ncvrs[ 6] = NcVar("parc", [nclon,        nctim], atts = Dict{Any,Any}("units"=>"none", "missing_value"=>MISS), t=Float64, compress=-1)
  ncvrs[ 7] = NcVar("tmpa", [nclon, nclat       ], atts = Dict{Any,Any}("units"=>"none", "missing_value"=>MISS), t=Float64, compress=-1)
  ncvrs[ 8] = NcVar("tmpb", [nclon, nclat       ], atts = Dict{Any,Any}("units"=>"none", "missing_value"=>MISS), t=Float64, compress=-1)
  ncvrs[ 9] = NcVar("tmpc", [nclon, nclat       ], atts = Dict{Any,Any}("units"=>"none", "missing_value"=>MISS), t=Float64, compress=-1)
  ncvrs[10] = NcVar("tmpd", [nclon, nclat       ], atts = Dict{Any,Any}("units"=>"none", "missing_value"=>MISS), t=Float64, compress=-1)
  ncvrs[11] = NcVar("tmpe", [nclon, nclat       ], atts = Dict{Any,Any}("units"=>"none", "missing_value"=>MISS), t=Float64, compress=-1)
  ncvrs[12] = NcVar("tmpf", [nclon, nclat       ], atts = Dict{Any,Any}("units"=>"none", "missing_value"=>MISS), t=Float64, compress=-1)
  ncvrs[13] = NcVar("tmpg", [nclon, nclat       ], atts = Dict{Any,Any}("units"=>"none", "missing_value"=>MISS), t=Float64, compress=-1)
  ncvrs[14] = NcVar("tmph", [nclon, nclat       ], atts = Dict{Any,Any}("units"=>"none", "missing_value"=>MISS), t=Float64, compress=-1)
  ncvrs[15] = NcVar("tmpi", [nclon, nclat       ], atts = Dict{Any,Any}("units"=>"none", "missing_value"=>MISS), t=Float64, compress=-1)
  ncvrs[16] = NcVar("tmpj", [nclon, nclat       ], atts = Dict{Any,Any}("units"=>"none", "missing_value"=>MISS), t=Float64, compress=-1)
  ncfil = NetCDF.create(fn, ncvrs, gatts = Dict{Any,Any}("units"=>"none"), mode = NC_NETCDF4)
end
lats = collect(1.0:nsta)                                                      # create the NetCDF template
lons = collect(1.0:NPAR)
nccreer(filh, hors, lats, lons)

hydt = Array{Float64}(undef,     nsta,nhor)
hydi = Array{Float64}(undef,     nsta,nhor)
flow = Array{Float64}(undef,NPAR,nsta,nhor)
wrfr = ncread(filb, "riverflo", start=[1,1,1], count=[-1,-1,-1])
wrfn = ncread(filc, "riverflo", start=[1,1,1], count=[-1,-1,-1])
erar = ncread(file, "riverflo", start=[1,1,1], count=[-1,-1,-1])
eran = ncread(filf, "riverflo", start=[1,1,1], count=[-1,-1,-1])
for a = 1:nhor, b = 1:nsta
  hydt[  b,a] = (wrfr[1,b,a] + wrfn[1,b,a] + erar[1,b,a] + eran[1,b,a]) / 4 ; hydt[b,a] < 0 && (hydt[b,a] = MISS)
  hydi[  b,a] = 1.0
  flow[1,b,a] =  wrfr[1,b,a]
  flow[2,b,a] =  wrfn[1,b,a]
  flow[3,b,a] =  erar[1,b,a]
  flow[4,b,a] =  eran[1,b,a]
end

@printf("writing %s\n\n", filh)
ncwrite(hydt, filh, "hydt", start=[1,1],   count=[-1,-1])
ncwrite(hydi, filh, "hydi", start=[1,1],   count=[-1,-1])
ncwrite(flow, filh, "flow", start=[1,1,1], count=[-1,-1,-1])
exit(0)

#=
    lins = readlines(fpa, keep = true) ; close(fpa)
    hlat = parse(Float64, split(lins[ 6])[2])
    hlon = parse(Float64, split(lins[ 7])[2])
    area = parse(Float64, split(lins[30])[2])
    rhbn = parse(  Int64, split(lins[10])[2])
    tmpa = lins[1][26:end]
    tmpb = tmpa[end-8:end-2]
    tmpc = split(split(split(tmpa, " (")[1], " RIVER")[1], " [")[1]
    labl = " [$tmpb $tmpc]\n"
    stem = file[1:end-4]
    numb = vet(ARGS[1] * "/" * stem * ".nc")
=#
