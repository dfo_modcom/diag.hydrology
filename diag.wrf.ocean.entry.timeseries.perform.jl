#=
 = Compute performance metrics for a specified timeseries of observations (C)
 = and model predictions (U).  Omit data prior to an intermediate date and any
 = missing values - RD May,Sep,Oct 2023.
 =#

using My, Printf, NetCDF, Statistics, StatsBase #, RCall
#R"library(energy)"

const MISS             = -9999.0                        # generic missing value

if (argc = length(ARGS)) != 1 && (argc = length(ARGS)) != 2
  print("\nUsage: jjj $(basename(@__FILE__)) pest_nwm_DOMAIN_full_full_FORCING.era_2019-05-31_000153.resest [identity]\n\n")
  exit(1)
end
fila = ARGS[1] * (argc == 1 ? ".nc" : ".$(ARGS[2]).nc")

tims = ncread(fila, "time", start=[1],     count=[-1])                        # read the timeseries dates and
floh = ncread(fila, "hydt", start=[1,1],   count=[-1,-1])                     # two streamflow estimates
flow = ncread(fila, "flow", start=[1,1,1], count=[-1,-1,-1])
npes, nsta, ntim = size(flow)

nsef =  ones(npes, nsta) * MISS                                               # and allocate space for metrics
pcor =  ones(npes, nsta) * MISS
dcor =  ones(npes, nsta) * MISS
rmse =  ones(npes, nsta) * MISS
bias =  ones(npes, nsta) * MISS
numb = zeros(npes, nsta)
tavg =  ones(npes, nsta) * MISS
rerr =  ones(npes, nsta) * MISS
metr =  ones(npes, ntim) * MISS

mask = falses(npes, nsta, ntim)                                               # define mask where HYDAT observations
for a = 1:ntim, b = 1:nsta, c = 1:npes                                        # and WRF-Hydro model are both valid
  floh[b,a] > MISS && flow[c,b,a] > MISS && (mask[c,b,a] = true)
end

function nse(c::Array{Float64,1}, u::Array{Float64,1})
  1 - var(u .- c) / var(c)
end

for a = 1:npes                                                                # get metrics for individual stations
  cccc = Array{Float64}(undef,0)                                              # as well as for all stations at once
  uuuu = Array{Float64}(undef,0)
  for b = 1:nsta
    cc = floh[  b,mask[a,b,:]]
    uu = flow[a,b,mask[a,b,:]]
    if isempty(cc) || isempty(uu)
      @show length(cc), length(uu)
      print("missing data for station $b and PEST iteration $a\n")
    else
      nsef[a,b] =            nse(cc,   uu)
      pcor[a,b] =            cor(cc,   uu)
#     dcor[a,b] =  rcopy(R"dcor($cc,  $uu, index = 1.0)")
      rmse[a,b] =           rmsd(cc,   uu)
      bias[a,b] =           mean(uu .- cc)
      numb[a,b] =         length(uu)
      tavg[a,b] =           mean(uu)
      rerr[a,b] = 100 * abs(mean(uu .- cc)) / mean(cc)
      cccc = [cccc; cc]
      uuuu = [uuuu; uu]
    end
  end
  if isempty(cccc) || isempty(uuuu)
    @show length(cccc), length(uuuu)
    print("missing data for all stations and PEST iteration $a\n")
  else
    mval = mnum = 0 ; for b = 1:nsta  nsef[a,b] > MISS && (mval += nsef[a,b] ; mnum += 1)  end ; mnum > 0 && (metr[a,1] = mval / mnum)
    mval = mnum = 0 ; for b = 1:nsta  pcor[a,b] > MISS && (mval += pcor[a,b] ; mnum += 1)  end ; mnum > 0 && (metr[a,2] = mval / mnum)
    mval = mnum = 0 ; for b = 1:nsta  dcor[a,b] > MISS && (mval += dcor[a,b] ; mnum += 1)  end ; mnum > 0 && (metr[a,3] = mval / mnum)
    mval = mnum = 0 ; for b = 1:nsta  rmse[a,b] > MISS && (mval += rmse[a,b] ; mnum += 1)  end ; mnum > 0 && (metr[a,4] = mval / mnum)
    mval = mnum = 0 ; for b = 1:nsta  bias[a,b] > MISS && (mval += bias[a,b] ; mnum += 1)  end ; mnum > 0 && (metr[a,5] = mval / mnum)
    mval = mnum = 0 ; for b = 1:nsta  numb[a,b] > MISS && (mval += numb[a,b] ; mnum += 1)  end ; mnum > 0 && (metr[a,6] = mval       )
    mval = mnum = 0 ; for b = 1:nsta  tavg[a,b] > MISS && (mval += tavg[a,b] ; mnum += 1)  end ; mnum > 0 && (metr[a,7] = mval / mnum)
    mval = mnum = 0 ; for b = 1:nsta  rerr[a,b] > MISS && (mval += rerr[a,b] ; mnum += 1)  end ; mnum > 0 && (metr[a,8] = mval / mnum)
#   metr[a,1] =            nse(cccc,   uuuu)
#   metr[a,2] =            cor(cccc,   uuuu)
#   metr[a,3] =  rcopy(R"dcor($cccc,  $uuuu, index = 1.0)")
#   metr[a,4] =           rmsd(cccc,   uuuu)
#   metr[a,5] =           mean(uuuu .- cccc)
#   metr[a,6] =         length(uuuu)
#   metr[a,7] =           mean(uuuu)
#   metr[a,8] = 100 * abs(mean(uuuu .- cccc)) / mean(cccc)
  end
end

ncwrite(nsef, fila, "tmpa", start=[1,1], count=[-1,-1])                       # and save the results
ncwrite(pcor, fila, "tmpb", start=[1,1], count=[-1,-1])
ncwrite(dcor, fila, "tmpc", start=[1,1], count=[-1,-1])
ncwrite(rmse, fila, "tmpd", start=[1,1], count=[-1,-1])
ncwrite(bias, fila, "tmpe", start=[1,1], count=[-1,-1])
ncwrite(numb, fila, "tmpf", start=[1,1], count=[-1,-1])
ncwrite(tavg, fila, "tmpg", start=[1,1], count=[-1,-1])
ncwrite(rerr, fila, "tmph", start=[1,1], count=[-1,-1])
ncwrite(metr, fila, "parb", start=[1,1], count=[-1,-1])
exit(0)
