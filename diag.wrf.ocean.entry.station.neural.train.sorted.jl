#=
 = Train a neural network to better match the normalized HYDAT and WRF-Hydro
 = streamflow timeseries, treating each station like a separate catchment with
 = a separate neural network on output.  Features that are missing are simply
 = omitted.  Training is by eve/odd/all years.  An initial monotonic sorting of
 = streamflow assumes HYDAT and WRF-Hydro are not collocated - RD Feb,Mar 2024.
 =#

using My, Printf, NetCDF, Flux, Statistics, ProgressMeter, JLD2

const ERAN             = 4                              # streamflow forced by ERA-5 with neural network post-processing
const FEAT             = 3                              # number of features (neural network inputs)
const DELT             = 3                              # source data timestep  (hours)
const DELD             = 24                             # daily       timestep  (hours)
const MISS             = -9999.0                        # generic missing value

if (argc = length(ARGS)) != 3 && (argc = length(ARGS)) != 4
  print("\nUsage: jjj $(basename(@__FILE__)) hydat_valding_aall_1000_incl_1990-01-01_2004-12-30.neural.nc flow eve [identity]\n\n")
  exit(1)
end
acti = argc == 3 ? "identity" : ARGS[4]
fila = ARGS[1]
filb = ARGS[1][1:end-3] * ".featrain.nc"
stem = ARGS[1][1:end-3] * ".sor" * ARGS[3] * "." * acti
fnam = ARGS[2]

tims = ncread(  filb, "time", start=[1], count=[-1]) ; ntim = length(tims)    # define a mask for eve/odd/all years
lats = ncread(  filb,  "lat", start=[1], count=[-1]) ; nsta = length(lats)
tatt = ncgetatt(filb, "time", "units")               ; msky = falses(ntim)
for a = 1:ntim
  year = parse(Int64, dateref(tims[a], tatt)[1:4])
  ARGS[3] == "eve" && iseven(year) && (msky[a] = true)
  ARGS[3] == "odd" &&  isodd(year) && (msky[a] = true)
  ARGS[3] == "all" &&                 (msky[a] = true)
end
@printf("\n%s has %7d times, with %7d %s years, and %d stations\n", filb, ntim, length(msky[msky]), ARGS[3], nsta)

form = ""
for a = 1:nsta
  global form
  alph = ncread(filb, "alph", start=  [a,1], count=   [1,-1])                 # read normalized flow and its features
  beta = ncread(filb, "beta", start=  [a,1], count=   [1,-1])
  feah = ncread(filb, "feah", start=[1,a,1], count=[-1,1,-1])
  feat = ncread(filb, "feat", start=[1,a,1], count=[-1,1,-1])

  mskh = falses(ntim)                                                         # and define their validity masks
  mskw = falses(ntim)
  mskz = falses(ntim)
  for b = 1:ntim
    msky[b] && all(feah[:,1,b] .> MISS)                             && (mskh[b] = true)
    msky[b] &&                             all(feat[:,1,b] .> MISS) && (mskw[b] = true)
    msky[b] && all(feah[:,1,b] .> MISS) && all(feat[:,1,b] .> MISS) && (mskz[b] = true)
  end

  xxtt = Array{Float32}(undef,FEAT,0)                                         # populate the un/calibrated matrices
  yytt = Array{Float32}(undef,FEAT,0)                                         # with normalized HYDAT and WRF-Hydro
  for b = 1:ntim                                                              # short timeseries (where all valid)
    if mskz[b]
      xxaa = Float32(feat[1,1,b])
      yyaa = Float32(feah[1,1,b])
      xxbb = Float32(feat[2,1,b])
      yybb = Float32(feah[2,1,b])
      xxcc = Float32(feat[3,1,b])
      yycc = Float32(feah[3,1,b])
      xxtt = [xxtt [xxaa, xxbb, xxcc]]
      yytt = [yytt [yyaa, yybb, yycc]]
    end
  end
  nttt = size(xxtt,2)
  @printf("station %3d has %5d training collocations\n", a, nttt)

  xxso = sortperm(xxtt[1,:])                                                  # sort only the instantaneous WRF-Hydro flow
  yyso = sortperm(yytt[1,:])                                                  # (xxtt) to match a monotonic increase in HyDAT
  zztt = deepcopy(xxtt)                                                       # (but given by the nonmonotonic order in yytt)
  for b = 1:nttt
    zztt[1,yyso[b]] = xxtt[1,xxso[b]]
  end
  xxtt = zztt

  acti == "identity" && (model = Chain(Dense(FEAT => FEAT+1),                               BatchNorm(FEAT+1), Dense(FEAT+1 => FEAT)))
  acti != "identity" && (model = Chain(Dense(FEAT => FEAT+1, getfield(Main, Symbol(acti))), BatchNorm(FEAT+1), Dense(FEAT+1 => FEAT, getfield(Main, Symbol(acti)))))
  optim = Flux.setup(Flux.Adam(0.01), model)

  losses = []                                                                 # evaluate model and loss inside gradient context
  @showprogress for epoch in 1:1000                                           # and record losses outside gradient context
    loss, grads = Flux.withgradient(model) do m
      yhat = m(xxtt)
      Flux.mse(yhat, yytt)
    end
    Flux.update!(optim, model, grads[1])
    push!(losses, loss)
  end
  @printf("first   loss %15.8f\n last-1 loss %15.8f\n last   loss %15.8f\n", losses[1], losses[end-1], losses[end])

  filc = @sprintf("%s.%3d.jld", stem, a)                                      # then save the neural network
  filc = replace(filc, ' ' => '0')
  @printf("writing  neural network to %s\n\n", filc)
  model_state = Flux.state(model) ; jldsave(filc; model_state)
  form *= @sprintf("station %3d has %5d training collocations with first loss %15.8f last-1 loss %15.8f last loss %15.8f\n", a, nttt, losses[1], losses[end-1], losses[end])
end
fild = stem * ".jldtxt"
fpb  = My.ouvre(fild, "w") ; write(fpb, form) ; close(fpb)

print("\n")
exit(0)
