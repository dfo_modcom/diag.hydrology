#=
 = Apply a neural network to features of a normalized WRF-Hydro streamflow
 = timeseries, treating each station like a separate catchment with a separate
 = neural network.  Features that are missing are simply omitted.  Training by
 = eve/odd/all years is applied to odd/eve/all years, respectively.  HYDAT and
 = WRF-Hydro streamflow are not assumed to be collocated and only a selection
 = of pour points are updated - RD Apr 2024.
 =#

using My, Printf, NetCDF, Flux, Statistics, JLD2

const WRFN             = 2                              # streamflow forced by CCSM/HadGEM/MPI with neural network post-processing
const ERAN             = 4                              # streamflow forced by      ERA-5      with neural network post-processing
const FEAT             = 3                              # number of features (neural network inputs)
const DELT             = 3                              # source data timestep  (hours)
const DELD             = 24                             # daily       timestep  (hours)
const SMAL             = 0.001                          # generic small streamflow value
const MISS             = -9999.0                        # generic missing value

if (argc = length(ARGS)) != 5 && (argc = length(ARGS)) != 6
  print("\nUsage: jjj $(basename(@__FILE__)) ../SPINUP.ccs/run_DOMAIN_full_full_FORCING.ccs_1990-01-01_2099-12-30_3h_Severn_Hudson.neural.nc riverflo all hydat_valding_aall_1000_iccs_1990-01-01_2022-12-30.neural.sorall hydat_valding_aall_1000_incl_hwdt.sta.hwshed.all\n\n")
  exit(1)
end
acti = argc == 5 ? "identity" : ARGS[6]
fila = ARGS[1]
filb = ARGS[1][1:end-3] * ".txt"
filc = ARGS[1][1:end-3] * ".features.nc"
fild = ARGS[1][1:end-3] * ".sor" * ARGS[3] * ".$acti.nc"
file = ARGS[1][1:end-3] * ".sor" * ARGS[3] * ".$acti.txt"
filf = ARGS[5]
filg = ARGS[4][1:end-7] * ".txt"
stem = ARGS[4] * "/" * ARGS[4][1:end-3] * ARGS[3] * ".$acti"
fnam = ARGS[2] ;              fnid = ERAN
contains(ARGS[1], "iccs") && (fnid = WRFN)
contains(ARGS[1], "ih45") && (fnid = WRFN)
contains(ARGS[1], "ih85") && (fnid = WRFN)
contains(ARGS[1], "impi") && (fnid = WRFN)
fnam           != "flow"  && (fnid =    1)

@printf("\nwriting %d where [1,2]=streamflow forced by CCSM/HadGEM/MPI with neural network post-processing\n", fnid)
@printf(   "            and    4 =streamflow forced by      ERA-5      with neural network post-processing\n\n")

fpa  = My.ouvre(filb, "r", false) ; linb = readlines(fpa, keep = true) ; close(fpa)
fpa  = My.ouvre(filf, "r", false) ; linf = readlines(fpa, keep = true) ; close(fpa)
fpa  = My.ouvre(filg, "r", false) ; ling = readlines(fpa, keep = true) ; close(fpa)

btof = Dict{Int64,          Int64}()                                          # define dictionaries for bfg
ftog = Dict{Int64, AbstractString}()
gton = Dict{AbstractString, Int64}()
for a = 1:length(linb)  btof[a] = parse(Int64, linb[a][3:10])  end
for a = 1:length(linf)     pind = parse(Int64, linf[a][6:13]) ; hydt = split(linf[a])[2] ; ftog[pind] = hydt  end
for a = 1:length(ling)                                          hydt = split(ling[a])[1] ; gton[hydt] =    a  end

stas = Array{Int64}(undef, 0) ; for line in linb  a = parse(Int64, line[3:10])  ;  push!(stas, a)  end ; ntas = length(stas)
staz = Array{Int64}(undef, 0) ; for line in linf  a = parse(Int64, line[6:13])  ;  push!(staz, a)  end ; ntaz = length(staz)
star = Array{Int64}(undef, 0) ; for               a = 1:ntas  in(stas[a], staz) && push!(star, a)  end

@printf("found %3d WRF-Hydro pour-point stations in %s\n", ntas, filb)
@printf("found %3d WRF-Hydro pour-point stations in %s\n", ntaz, filf)
@printf("copying %s  %s\n", fila, fild) ; cp(fila, fild; force = true, follow_symlinks = true)
@printf("copying %s %s\n",  filb, file) ; cp(filb, file; force = true, follow_symlinks = true)

tims = ncread(  fild, "time", start=[1], count=[-1]) ; ntim = length(tims)    # define a mask for eve/odd/all years
lats = ncread(  fild,  "lat", start=[1], count=[-1]) ; nsta = length(lats)    #   (i.e., true for odd/eve/all years)
tatt = ncgetatt(fild, "time", "units")               ; msky = falses(ntim)
for a = 1:ntim
  year = parse(Int64, dateref(tims[a], tatt)[1:4])
  ARGS[3] == "eve" &&  isodd(year) && (msky[a] = true)
  ARGS[3] == "odd" && iseven(year) && (msky[a] = true)
  ARGS[3] == "all" &&                 (msky[a] = true)
end
@printf("\n%s has %7d times, with %7d non-%s years, and %d stations\n", fild, ntim, length(msky[msky]), ARGS[3], nsta)

for a in star
  alph = ncread(filc, "alph", start=  [a,1], count=   [1,-1])                 # read the normalized flow features
  beta = ncread(filc, "beta", start=  [a,1], count=   [1,-1])
  feat = ncread(filc, "feat", start=[1,a,1], count=[-1,1,-1])
  @printf(" %d", a)

  mskz = falses(ntim)                                                         # and define their validity mask
  for b = 1:ntim
    msky[b] && all(feat[:,1,b] .> MISS) && (mskz[b] = true)
  end

  filh = @sprintf("%s.%3d.jld", stem, gton[ftog[btof[a]]])                    # then load the neural network
  filh = replace(filh, ' ' => '0')
  pars = JLD2.load(filh, "model_state");
  acti == "identity" && (model = Chain(Dense(FEAT => FEAT+1),                               BatchNorm(FEAT+1), Dense(FEAT+1 => FEAT)))
  acti != "identity" && (model = Chain(Dense(FEAT => FEAT+1, getfield(Main, Symbol(acti))), BatchNorm(FEAT+1), Dense(FEAT+1 => FEAT, getfield(Main, Symbol(acti)))))
  Flux.loadmodel!(model, pars)

  flow = ones(1,1,ntim) * MISS                                                # and replace all WRF-Hydro estimates
  for b = 1:ntim                                                              # with rescaled neural network estimates
    if mskz[b]
      xxaa        = Float32(feat[1,1,b])
      xxbb        = Float32(feat[2,1,b])
      xxcc        = Float32(feat[3,1,b])
      flow[1,1,b] = Float64(model(reshape([xxaa,xxbb,xxcc],FEAT,1))[1]) * beta[1,b] + alph[1,b]
      flow[1,1,b] < 0 && (flow[1,1,b] = SMAL)
    end
  end
  ncwrite(flow, fild, fnam, start=[fnid,a,1], count=[1,1,-1])
end

print("\n\n")
exit(0)

#=
b (510) =    2500911   250   911   440     56.01741791    -87.60491943   250   912   439     56.03536606    -87.60378265
f ( 51) = wrfh_02500911 hydat_04CC001  1     55.37500000    -88.32499695  1 [02500911 pour_point]  90581.60000000  90578.90000000  84613.80000000  88591.43333333     56.01741791    -87.60491943  90957.00000000
g (183) = hydat_01AF010     3618     47.47055817    -68.23555756    1028.6 0 [01AF010 GREEN]
g         hydat_04CC001     1826     55.37500000    -88.32499695   90581.6 0 [04CC001 SEVERN]
=#
