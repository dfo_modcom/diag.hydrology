#=
 = This program calculates the domain-average value of a given
 = variable for the time series of interest.  Because it is often
 = executed in parallel and some NetCDF writes can be locked out
 = by others happening at the same time, output timeseries are also
 = written to text (and if this was done already, then this program
 = reads the text and writes to NetCDF, although locked writes should
 = use try-catch instead) - RD Apr 2021.
 =#

using My, Printf, NetCDF
const EARTH            = 6.371e6                        # mean Earth radius (m)
const D2R              = pi / 180.0                     # degrees to radians conversion
const GRDMIS           = 99999.0                        # gridbox unassociated with the watersheds of interest
const RECAL            = true                           # apply linear recalibration of input variables (temp, tdew, shum)

if (argc = length(ARGS)) != 4
  print("\nUsage: jjj $(basename(@__FILE__)) w85 2005 2099 var\n")
  print("       and var can be R2D S2D T2D Q2D U2D V2D PSFC RAINRATE SWDOWN LWDOWN\n\n")
  exit(1)
end

vars = [              "T2D", "Q2D", "U2D", "V2D", "PSFC", "RAINRATE", "SWDOWN", "LWDOWN"]
varz = ["R2D", "S2D", "T2D", "Q2D", "U2D", "V2D", "PSFC", "RAINRATE", "SWDOWN", "LWDOWN"]
dya  = ARGS[2] * "-01-01-00"
dyb  = ARGS[3] * "-12-31-18"
hra  = My.datesous("1900-01-01-00", dya, "hr")
hrb  = My.datesous("1900-01-01-00", dyb, "hr")
tims = collect(hra:6:hrb)
ntim = length(tims)
data = zeros(1, 1, ntim)
date = dya

filx = "forctxt_" * ARGS[1] * "_" * ARGS[2] * "_" * ARGS[3] * "_" * ARGS[4]   # if a text output file already
fily = "forcing_" * ARGS[1] * "_" * ARGS[2] * "_" * ARGS[3] * ".byday.nc"     # exists, then simply transfer
if isfile(filx)                                                               # from text to NetCDF (just in
  fpa = My.ouvre(filx, "r")                                                   # case parallel execution locked
  datb = Array{Float64}(undef, 0)                                             # out some previous NetCDF writes)
  for line in eachline(fpa)  push!(datb, parse(Float64, line))  end
  close(fpa)

  if !isfile(fily)
    nccreer(      fily, ntim, 1, 1, GRDMIS; vnames = varz)
    ncwrite(tims, fily, "time", start=[1], count=[-1])
    ncputatt(     fily, "time", Dict("units" => "hours since 1900-01-01 00:00:0.0"))
  end
  print("writing $fily $(ARGS[4])\n\n")
  ncwrite(datb, fily, ARGS[4], start=[1,1,1], count=[-1,-1,-1])
  exit(0)
end

#ila = "../DOMAIN_0000_0000/geomask.nc"
fila = "../DOMAIN_full_full/geogrid.nc"                                       # otherwise, get a landmask
gask = ncread(fila, "LANDMASK", start=[1,1,1], count=[-1,-1,-1])              # and report its areal extent
glat = ncread(fila,     "CLAT", start=[1,1,1], count=[-1,-1,-1])
glon = ncread(fila,    "CLONG", start=[1,1,1], count=[-1,-1,-1])
coslat = cos.(glat * pi / 180)
(nlon, nlat) = size(gask)
for a = 1:nlat, b = 1:nlon
  if a == 1 || a == nlat || b == 1 || b == nlon
    gask[b,a,1] = 0
# elseif -0.5 < gask[b,a,1] < 1.5
  elseif  0.5 < gask[b,a,1] < 1.5
    dellat = (glat[b,a+1,1] - glat[b,a-1,1]) / 2
    dellon = (glon[b+1,a,1] - glon[b-1,a,1]) / 2
    gask[b,a,1] = dellat * EARTH * D2R * dellon * EARTH * D2R * coslat[a]
  else
    gask[b,a,1] = 0
  end
end
@printf("\narea extent of interest is %15.8f million km2\n\n", sum(gask) / 1e12)

function rhumcal(temp::Array{T,3}, shum::Array{T,3}, psfc::Array{T,3}) where {T<:Real}
  vnew = GRDMIS * ones(nlon, nlat, 1)
  for a = 1:nlat, b = 1:nlon
    if gask[b,a,1] > 0
      vnew[b,a,1] = 0.263 * shum[b,a,1] * psfc[b,a,1] / exp(17.67 * (temp[b,a,1] - 273.15) / (temp[b,a,1] - 29.65))
      vnew[b,a,1] > 100 && (vnew[b,a,1] = 100.0)
    end
  end
  return(vnew)
end
function wspdcal(uwnd::Array{T,3}, vwnd::Array{T,3}) where {T<:Real}          # add derived variable calculations
  vnew = GRDMIS * ones(nlon, nlat, 1)                                         # (specific humidity in g/kg)
  for a = 1:nlat, b = 1:nlon
    if gask[b,a,1] > 0
      vnew[b,a,1] = (uwnd[b,a,1]^2 + vwnd[b,a,1]^2)^0.5
    end
  end
  return(vnew)
end

function domainavg(avar::Array{T,3}) where {T<:Real}                          # specify the domain average
  vsum = vnum = 0.0
  for a = 1:nlat, b = 1:nlon
    if gask[b,a,1] > 0
      vsum += gask[b,a,1] * avar[b,a,1]
      vnum += gask[b,a,1]
    end
  end
  vsum /= vnum
end

vari = findall(x -> x == ARGS[4], vars)                                       # loop through all times and get domain
if length(vari) > 0                                                           # averages for variables already available
  vnam = vars[vari[1]]
  for a = 1:ntim
    global date
    file = date[1:4] * date[6:7] * date[9:10] * date[12:13] * ".LDASIN_DOMAIN1"
    if isfile(file)
#     print("reading $file $date $vnam\n")
      field = ncread(file, vnam, start=[1,1,1], count=[-1,-1,-1])
#     ARGS[4] == "T2D" && (field = (field - alpt) / bett)
#     ARGS[4] == "Q2D" && (field = (field - alpd) / betd)
      data[a] = domainavg(field)
    else
      print("MISSING $file $date $vnam\n")
      data[a] = GRDMIS
    end
    date = My.dateadd(date, 6.0, "hr")
  end
end

if ARGS[4] == "R2D"                                                           # or loop over calculated variables
  for a = 1:ntim
    global date
    file = date[1:4] * date[6:7] * date[9:10] * date[12:13] * ".LDASIN_DOMAIN1"
    if isfile(file)
#     print("reading $file $date T2D,Q2D,PSFC\n")
#     temp = (ncread(file,  "T2D", start=[1,1,1], count=[-1,-1,-1]) - alpt) / bett
#     shum = (ncread(file,  "Q2D", start=[1,1,1], count=[-1,-1,-1]) - alpd) / betd
      temp =  ncread(file,  "T2D", start=[1,1,1], count=[-1,-1,-1])
      shum =  ncread(file,  "Q2D", start=[1,1,1], count=[-1,-1,-1])
      psfc =  ncread(file, "PSFC", start=[1,1,1], count=[-1,-1,-1])
      rhum = rhumcal(temp, shum, psfc)
      data[a] = domainavg(rhum)
    else
      print("MISSING $file $date T2D,Q2D,PSFC\n")
      data[a] = GRDMIS
    end
    date = My.dateadd(date, 6.0, "hr")
  end
end

if ARGS[4] == "S2D"
  for a = 1:ntim
    global date
    file = date[1:4] * date[6:7] * date[9:10] * date[12:13] * ".LDASIN_DOMAIN1"
    if isfile(file)
#     print("reading $file $date U2D,V2D\n")
      uwnd = ncread(file, "U2D", start=[1,1,1], count=[-1,-1,-1])
      vwnd = ncread(file, "V2D", start=[1,1,1], count=[-1,-1,-1])
      wspd = wspdcal(uwnd, vwnd)
      data[a] = domainavg(wspd)
    else
      print("MISSING $file $date U2D,V2D\n")
      data[a] = GRDMIS
    end
    date = My.dateadd(date, 6.0, "hr")
  end
end

fpa = My.ouvre(filx, "w")                                                     # write text just in case
for a = 1:ntim
  form = @sprintf("%16.9f\n", data[a])
  write(fpa, form)
end
close(fpa)

print("wrote text\n")
if !isfile(fily)                                                              # then store the domain-average
  nccreer(      fily, ntim, 1, 1, GRDMIS; vnames = varz)                      # series and update the dates
print("wrote empty file\n")
  ncwrite(tims, fily, "time", start=[1], count=[-1])
print("wrote time\n")
  ncputatt(     fily, "time", Dict("units" => "hours since 1900-01-01 00:00:0.0"))
print("wrote time units\n")
end
print("writing $fily $(ARGS[4])\n\n")
ncwrite(data,   fily,  ARGS[4], start=[1,1,1], count=[-1,-1,-1])
print("wrote   $fily $(ARGS[4])\n\n")
exit(0)

#=
alpt = 0.0 ; bett = 1.0                                                       # set linear calibration for T2D and
alpd = 0.0 ; betd = 1.0                                                       # Q2D and set the domain dimensions
if ARGS[1][1:3] == "aaa"
  elpt =  0.3586725400000000 ; eett = 0.9826762100000000 ; flpt = -0.6685837313430056 ; fett = 1.0000066856803873
  elpd =  0.5254395200000000 ; eetd = 0.9620725700000000 ; flpd = -0.0178951923749082 ; fetd = 1.0000001787115467
  if RECAL
    alpt = flpt + fett * elpt ; bett = fett * eett
    alpd = flpd + fetd * elpd ; betd = fetd * eetd
  end
end
if ARGS[1][1:3] == "bbb"
  elpt =  0.6451287850000000 ; eett = 0.9539812300000000 ; flpt = -0.3824560556531651 ; fett = 1.0000038245385998
  elpd =  0.9746049850000000 ; eetd = 0.9368738650000000 ; flpd =  0.4788443018328508 ; fetd = 0.9999952114496502
  if RECAL
    alpt = flpt + fett * elpt ; bett = fett * eett
    alpd = flpd + fetd * elpd ; betd = fetd * eetd
  end
end
RECAL && ARGS[1][1:3] == "arc" && (alpt =  0.9757281600000000 ; bett = 0.9217836400000000 ; alpd =  2.0397682600000000 ; betd = 0.8210159450000000)
RECAL && ARGS[1][1:3] == "atl" && (alpt = -1.9349257650000000 ; bett = 1.0585566350000000 ; alpd = -1.9718148600000000 ; betd = 1.1115034050000000)

west_east = 310 ;
south_north = 200 ;
Time = 1 ;
float lat(south_north, west_east) ;
float lon(south_north, west_east) ;
float T2D(Time, south_north, west_east) ;
float Q2D(Time, south_north, west_east) ;
float U2D(Time, south_north, west_east) ;
float V2D(Time, south_north, west_east) ;
float PSFC(Time, south_north, west_east) ;
float RAINRATE(Time, south_north, west_east) ;
float SWDOWN(Time, south_north, west_east) ;
float LWDOWN(Time, south_north, west_east) ;
int64 west_east(west_east) ;
int64 south_north(south_north) ;
int64 Time(Time) ;
=#
