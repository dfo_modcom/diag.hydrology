#=
 = Construct lists of stations that conform to requirements for "validation".
 = Lists distinguish (RHBN/all) stations with more than CUTN streamflow obs
 = between 1990 and 2004 and whose HydroSHEDS upstream catchments are greater
 = than CUTR km2.  Output files are stored in both source and destination dirs
 = - RD May,Jun 2023, Jan 2024.
 =#

using My, Printf, NetCDF

const CUTN             = 1000                           # minimum number of obs between 1990 and 2004
const CUTR             = 1000                           # minimum HydroSHEDS UPLAND_SKM value
const MISS             = -9999.0                        # generic missing value
const DBEG             = datesous("1900-01-01-00", "1990-01-01-00", "hr")
const DEND             = datesous("1900-01-01-00", "2004-12-31-23", "hr")

if (argc = length(ARGS)) != 2
  print("\nUsage: jjj $(basename(@__FILE__)) hydat hydat.validation\n\n")
  exit(1)
end
fila = @sprintf("%s/hydat_valding_aall.txt",    ARGS[1])
filb = @sprintf("%s/hydat_valding_rhbn.txt",    ARGS[1])
filc = @sprintf("%s/hydat_valding_aall_%s.txt", ARGS[2], CUTR)
fild = @sprintf("%s/hydat_valding_aall_%s.com", ARGS[2], CUTR)
file = @sprintf("%s/hydat_valding_rhbn_%s.txt", ARGS[2], CUTR)
filf = @sprintf("%s/hydat_valding_rhbn_%s.com", ARGS[2], CUTR)
filg = @sprintf("%s/hydat_valding_1990_%s.txt", ARGS[1], CUTN)

function vet(file::AbstractString)
  tmp = "02OC019" ; contains(file, tmp) && (print("skipping $tmp\n") ; return(0))
  tmp = "02RF001" ; contains(file, tmp) && (print("skipping $tmp\n") ; return(0))
  tmp = "02BE002" ; contains(file, tmp) && (print("skipping $tmp\n") ; return(0))
  tmp = "02HK010" ; contains(file, tmp) && (print("skipping $tmp\n") ; return(0))
  tmp = "02CA001" ; contains(file, tmp) && (print("skipping $tmp\n") ; return(0))
  tmp = "02HA003" ; contains(file, tmp) && (print("skipping $tmp\n") ; return(0))
  tmp = "02OA024" ; contains(file, tmp) && (print("skipping $tmp\n") ; return(0))

  tima = ncread(file, "time", start=[1],     count=[-1])                      # omit stations "in" or just
  floa = ncread(file, "flow", start=[1,1,1], count=[-1,-1,-1])[1,1,:]         # downstream of large lakes

  numb = 0
  for (a, tval) in enumerate(tima)
    DBEG < tval < DEND && floa[a] > 0 && (numb += 1)
  end
  return(numb)
end

forma = formb = formc = formd = forme = formf = formg = ""                    # create the station lists
files = readdir(ARGS[1])
for file in files
  global forma, formb, formc, formd, forme, formf, formg
  if isfile(ARGS[1] * "/" * file) && endswith(file, ".sta")
    fpa  = My.ouvre(ARGS[1] * "/" * file, "r")
    lins = readlines(fpa, keep = true) ; close(fpa)
    hlat = parse(Float64, split(lins[ 6])[2])
    hlon = parse(Float64, split(lins[ 7])[2])
    area = parse(Float64, split(lins[30])[2])
    rhbn = parse(  Int64, split(lins[10])[2])
    tmpa = lins[1][26:end]
    tmpb = tmpa[end-8:end-2]
    tmpc = split(split(split(tmpa, " (")[1], " RIVER")[1], " [")[1]
    labl = " [$tmpb $tmpc]\n"

    stem = file[1:end-4]
    numb = vet(ARGS[1] * "/" * stem * ".nc")
    line = @sprintf("%s %8d %15.8f %15.8f %9.1f %d\n", stem, numb, hlat, hlon, area, rhbn)
                                                               forma *= line
    numb >= CUTN && rhbn == 1 &&                              (formb *= line)
    numb >= CUTN              && area > CUTR && hlon > -90 && (formc *= line[1:end-1] * labl ; formd *= @sprintf("cp ../%s/%s* .\n", ARGS[1], stem))
    numb >= CUTN && rhbn == 1 && area > CUTR && hlon > -90 && (forme *= line[1:end-1] * labl ; formf *= @sprintf("cp ../%s/%s* .\n", ARGS[1], stem))
    numb >= CUTN                             && hlon > -90 && (formg *= line[1:end-1] * labl)
  end
end

fpa = My.ouvre(fila, "w") ; write(fpa, forma) ; close(fpa)                     # then save the lists in dirs
fpb = My.ouvre(filb, "w") ; write(fpb, formb) ; close(fpb)
fpc = My.ouvre(filc, "w") ; write(fpc, formc) ; close(fpc)
fpd = My.ouvre(fild, "w") ; write(fpd, formd) ; close(fpd)
fpe = My.ouvre(file, "w") ; write(fpe, forme) ; close(fpe)
fpf = My.ouvre(filf, "w") ; write(fpf, formf) ; close(fpf)
fpg = My.ouvre(filg, "w") ; write(fpg, formg) ; close(fpg)
print("\n")
exit(0)
