#=
 = Construct lists of stations that conform to some specification.
 = Output files are stored in the current dir - RD Jan 2024.
 =#

using My, Printf, NetCDF

const CUTN             = 1000                           # minimum number of obs between 1990 and 2004
const CUTR             = 1000                           # minimum HydroSHEDS UPLAND_SKM value
const MISS             = -9999.0                        # generic missing value
const DBEG             = datesous("1900-01-01-00", "1990-01-01-00", "hr")
const DEND             = datesous("1900-01-01-00", "2004-12-31-23", "hr")

if (argc = length(ARGS)) != 2
  print("\nUsage: jjj $(basename(@__FILE__)) zhydat.subset zhydat.latest\n\n")
  exit(1)
end
fila = ARGS[1]
dira = ARGS[2]

function vet(file::AbstractString)
  tima = ncread(file, "time", start=[1],     count=[-1])
  floa = ncread(file, "flow", start=[1,1,1], count=[-1,-1,-1])[1,1,:]

  numb = 0
  for (a, tval) in enumerate(tima)
    DBEG < tval < DEND && floa[a] > 0 && (numb += 1)
  end
  return(numb)
end

forma = ""                                                                    # create the station lists
formb = "jjj unpack.hydat.station.jl Hydat.sqlite3"
files = readdir(".")
for file in files
  global forma, formb
  if isfile(file) && endswith(file, ".sta")
    fpa  = My.ouvre(file, "r") ; lins = readlines(fpa, keep = true) ; close(fpa)
#   hlat = parse(Float64, split(lins[ 6])[2])
#   hlon = parse(Float64, split(lins[ 7])[2])
#   area = parse(Float64, split(lins[30])[2])
#   rhbn = parse(  Int64, split(lins[10])[2])
    wlat = parse(Float64, split(lins[90])[2])
    wlon = parse(Float64, split(lins[91])[2])
    tmpa = lins[1][26:end]
    tmpb = tmpa[end-8:end-2]
    tmpc = split(split(split(tmpa, " (")[1], " RIVER")[1], " [")[1]
    labl = " [$tmpb $tmpc]\n"
    stem = file[1:end-4]

#   contains(lins[1],        "SAINT JOHN RIVER") && (forma *= @sprintf("cp %s* %s\n", stem, dira) ; formb *= @sprintf(" %s", file[7:13]))
#   contains(lins[1],                "MUSQUASH") && (forma *= @sprintf("cp %s* %s\n", stem, dira) ; formb *= @sprintf(" %s", file[7:13]))
#   44.9 < hlat < 45.5 && -66.65 < hlon < -66.05 && (forma *= @sprintf("cp %s* %s\n", stem, dira) ; formb *= @sprintf(" %s", file[7:13]))
    44.9 < wlat < 45.5 && -66.65 < wlon < -66.05 && (forma *= @sprintf("cp %s* %s\n", stem, dira) ; formb *= @sprintf(" %s", file[6:13]))

#   numb = vet(ARGS[1] * "/" * stem * ".nc")
#   line = @sprintf("%s %8d %15.8f %15.8f %9.1f %d\n", stem, numb, hlat, hlon, area, rhbn)
#   forma *= @sprintf("cp ../%s/%s* .\n", ARGS[1], stem)
#                                                              forma *= line
#   numb >= CUTN && rhbn == 1 &&                              (formb *= line)
#   numb >= CUTN              && area > CUTR && hlon > -90 && (formc *= line[1:end-1] * labl ; formd *= @sprintf("cp ../%s/%s* .\n", ARGS[1], stem))
#   numb >= CUTN && rhbn == 1 && area > CUTR && hlon > -90 && (forme *= line[1:end-1] * labl ; formf *= @sprintf("cp ../%s/%s* .\n", ARGS[1], stem))
  end
end
forma *= @sprintf("echo %s > %s/exec\n", formb, dira)
fpa = My.ouvre(fila, "w") ; write(fpa, forma) ; close(fpa)                     # then save the lists in dirs
exit(0)
