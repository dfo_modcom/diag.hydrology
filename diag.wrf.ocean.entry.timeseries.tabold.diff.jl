#=
 = Tabulate performance metrics of the optimal PEST experiment
 = for individual and total rivers - RD Oct 2023.
 =#

using My, Printf, NetCDF

const MISS             = -9999.0                        # generic missing value

if (argc = length(ARGS)) != 3
  print("\nUsage: jjj $(basename(@__FILE__)) pest_nwm_DOMAIN_East.FORCING.era_2019-03-31_000214.remsvd 1 107\n\n")
  exit(1)
end
fila = ARGS[1] * ".nc" #.2019-05-01"
filb = ARGS[1] * ".txt"
filc = ARGS[1] * ".perform"

fpa  = My.ouvre(filb, "r") ; lina = readlines(fpa) ; close(fpa)               # read the station list, data dims,
tmpa = ncread(fila, "tmpa", start=[1,1], count=[-1,-1])                       # and optimal PEST performance index
npes, nsta = size(tmpa)
pesta = parse(Int64, ARGS[2])
pestb = parse(Int64, ARGS[3])
filc *= @sprintf(".%d.%d", pesta, pestb)

tarea  = 0.0
forma  = "  Station / River                &       Drainage         &  Obs &     Obs Flow    &    Model   &     NSE     & Pearson \\\\\n"
forma *= "                                 &       Area (km\$^2\$)    & Days & (m\$^3\$s\$^{-1}\$) &  Diff (\\%) &             &  Corr   \\\\\n  \\colrule\n"
gooda  = goodb = 0
for a = 1:nsta
  global tarea, forma, gooda, goodb
  vals = split(lina[a],",")                                                   # then get individual station metrics
  name = split(lina[a],"[")[2][1:end-1]                                       # (and sum individual drainage areas)
# fild = "../hydat.calibration/hydat_" * name[1:7] * ".sta"
  fild =                      "hydat_" * name[1:7] * ".sta"
  fpd  = My.ouvre(fild, "r") ; linc = readlines(fpd) ; close(fpd)
  lind = split(linc[30]) ; area = parse(Float64, lind[2]) ; areb = parse(Float64, lind[4]) ; tarea += area
  nsefa = ncread(fila, "tmpa", start=[pesta,a], count=[1,1])[1]
  nsega = ncread(fila, "tmpa", start=[    1,a], count=[1,1])[1] - nsefa
  pcora = ncread(fila, "tmpb", start=[pesta,a], count=[1,1])[1]
  dcora = ncread(fila, "tmpc", start=[pesta,a], count=[1,1])[1]
  rmsea = ncread(fila, "tmpd", start=[pesta,a], count=[1,1])[1]
  biasa = ncread(fila, "tmpe", start=[pesta,a], count=[1,1])[1]
  numba = ncread(fila, "tmpf", start=[pesta,a], count=[1,1])[1]
  tavga = ncread(fila, "tmpg", start=[pesta,a], count=[1,1])[1] - biasa
  rerra = ncread(fila, "tmph", start=[pesta,a], count=[1,1])[1]
  nsefa >= 0.50 && rerra <= 15.0 && (gooda += 1)

  nsefb = ncread(fila, "tmpa", start=[pestb,a], count=[1,1])[1]
  nsegb = ncread(fila, "tmpa", start=[    1,a], count=[1,1])[1] - nsefb
  pcorb = ncread(fila, "tmpb", start=[pestb,a], count=[1,1])[1]
  dcorb = ncread(fila, "tmpc", start=[pestb,a], count=[1,1])[1]
  rmseb = ncread(fila, "tmpd", start=[pestb,a], count=[1,1])[1]
  biasb = ncread(fila, "tmpe", start=[pestb,a], count=[1,1])[1]
  numbb = ncread(fila, "tmpf", start=[pestb,a], count=[1,1])[1]
  tavgb = ncread(fila, "tmpg", start=[pestb,a], count=[1,1])[1] - biasb
  rerrb = ncread(fila, "tmph", start=[pestb,a], count=[1,1])[1]
  nsefb >= 0.50 && rerrb <= 15.0 && (goodb += 1)

  (nsaa, nsbb) = ("    ", " ") ; (reaa, rebb) = ("    ", " ")
  nsef = nsefb - nsefa > 0 ? "1" : " "
  nseg = nsegb - nsega > 0 ?  1  : -1
  pcor = pcorb - pcora > 0 ? "1" : " "
  dcor = dcorb - dcora > 0 ?  1  : -1
  rmse = rmseb - rmsea > 0 ?  1  : -1
  bias = biasb - biasa > 0 ?  1  : -1
  numb = numbb
  tavg = tavga
  rerr = rerrb - rerra > 0 ? " " : "1"
  forma *= @sprintf("  %7s / %-20s & %21.0f  & %4.0f & %15.0f & %s %4s%s & %s %5s%s & %5s   \\\\\n", name[1:7], name[9:end], area, numb, tavg, reaa, rerr, rebb, nsaa, nsef, nsbb, pcor)
end

nsef = ncread(fila, "parb", start=[pesta,1], count=[1,1])[1] ; (nsaa, nsbb) = nsef >= 0.50 ? ("{\\bf", "}") : ("    ", " ")
nseg = ncread(fila, "parb", start=[    1,1], count=[1,1])[1] - nsef
pcor = ncread(fila, "parb", start=[pesta,2], count=[1,1])[1]
dcor = ncread(fila, "parb", start=[pesta,3], count=[1,1])[1]
rmse = ncread(fila, "parb", start=[pesta,4], count=[1,1])[1]                  # and append total performance
bias = ncread(fila, "parb", start=[pesta,5], count=[1,1])[1]
numb = ncread(fila, "parb", start=[pesta,6], count=[1,1])[1]
tavg = ncread(fila, "parb", start=[pesta,7], count=[1,1])[1] - bias
rerr = ncread(fila, "parb", start=[pesta,8], count=[1,1])[1] ; (reaa, rebb) = rerr <= 15.0 ? ("{\\bf", "}") : ("    ", " ")
forma *= @sprintf("  %24s (%3d) & %21.0f  & %4.0f & %15.0f & %s %4.0f%s & %s %5.2f%s & %5.2f   \\\\\n", "All Rivers", gooda, tarea, numb, tavg, reaa, rerr, rebb, nsaa, nsef, nsbb, pcor)
fpa = My.ouvre(filc, "w") ; write(fpa, forma) ; close(fpa)
print(forma)
exit(0)
