* This script is designed to plot cyclone location and intensity (colour code).
* It can be executed using a command like
*
*     grads -blc "diag.wrf.forcing.interp.avg.byday era_1990_2004 wrf_1990_2004 ccs_1990_2004"
*
* - RD November 2012

function plot(args)
stema = subwrd(args,1)
stemb = subwrd(args,2)
stemc = subwrd(args,3)
taila = "byday.avg.nc"

"set rgb    42     0    0    0"
"set rgb    43    30   30   30"
"set rgb    44    50   50   50"
"set rgb    45    70   70   70"
"set rgb    46    90   90   90"
"set rgb    47   110  110  110"
"set rgb    48   130  130  130"
"set rgb    49   150  150  150"
"set rgb    50   170  170  170"
"set rgb    51   190  190  190"
"set rgb    52   210  210  210"
"set rgb    53   255  255  255"

ypic = 8 ; string = "0.3 10.2 0.95 "ypic ; inner_decomp(string)
a = 1 ; while (a <= ypic) ; b = ypic - a + 1 ; low.b = _retlef.a ; hig.b = _retrig.a ; a = a + 1 ; endwhile

a = 1
while (a < 2)
  if (a = 1) ; reg = "arc" ; lef = 0.5 ; rig = 8.0 ; endif
* if (a = 2) ; reg = "atl" ; lef = 4.4 ; rig = 8.0 ; endif
  b = 1
  while (b < 9)
    if (b = 1) ; var = "3600*RAINRATE" ; endif
    if (b = 2) ; var =   "-273.15+T2D" ; endif
    if (b = 3) ; var =      "1000*Q2D" ; endif
    if (b = 4) ; var =           "R2D" ; endif
    if (b = 5) ; var =        "SWDOWN" ; endif
    if (b = 6) ; var =        "LWDOWN" ; endif
    if (b = 7) ; var =           "S2D" ; endif
    if (b = 8) ; var =     "0.01*PSFC" ; endif

    if (b = 1) ; lab = "a" ; rng =   "0.06  0.17" ; yint =  "0.03" ; til = "Precipitation (mm/hr)"          ; endif
    if (b = 2) ; lab = "b" ; rng =  "-3.0   9.0"  ; yint =  "3.0"  ; til = "Temperature (`3.`0C)"           ; endif
    if (b = 3) ; lab = "c" ; rng =   "3.0   8.0"  ; yint =  "1.0"  ; til = "Specific Humidity (g/kg)"       ; endif
    if (b = 4) ; lab = "d" ; rng =  "70.0  85.0"  ; yint =  "5.0"  ; til = "Relative Humidity (%)"          ; endif
    if (b = 5) ; lab = "e" ; rng = "100.0 150.0"  ; yint = "20.0"  ; til = "Shortwave Radiation (W/m`a2`n)" ; endif
    if (b = 6) ; lab = "f" ; rng = "260.0 320.0"  ; yint = "20.0"  ; til = "Longwave Radiation (W/m`a2`n)"  ; endif
    if (b = 7) ; lab = "g" ; rng =   "3.2   4.5"  ; yint =  "0.5"  ; til = "Wind Speed (m/s)"               ; endif
    if (b = 8) ; lab = "h" ; rng = "976.0 983.0"  ; yint =  "3.0"  ; til = "Surface Pressure (hPa)"         ; endif

    fila = "forcing_"stema"."taila ; col.1 =  1 ; sty.1 = 1
    filb = "forcing_"stemb"."taila ; col.2 =  4 ; sty.2 = 1
    filc = "forcing_"stemc"."taila ; col.3 =  3 ; sty.3 = 1 ; if (substr(stemb,1,3) = "wrf") ; col.3 = 4 ; endif
*   fild = "forcing_"stemd"."taila ; col.4 =  1 ; sty.4 = 1
*   file = "forcing_"steme"."taila ; col.5 = 48 ; sty.5 = 1
*   filf = "forcing_nar_2005_2020.byday.avg.nc" ; col.6 =  2 ; sty.6 = 1
*   filg = "forcing_nar_2005_2020.byday.avg.nc" ; col.7 =  2 ; sty.7 = 1

    "sdfopen "fila
    "sdfopen "filb
    "sdfopen "filc
*   "sdfopen "fild
*   "sdfopen "file
*   "sdfopen "filf
*   "sdfopen "filg
*   "set dfile 3" ; "set t 21500 131400" ; "define seen = 1 - "var".3 + "var".3" ; "set dfile 1"

    tima =  21917
    timb = 160708
    lap  = -0.2 ; mid = lef + (rig - lef) * tima / (tima + timb) + lap / 2
    "set parea "lef" "rig" "low.b" "hig.b
    "set mproj off"
    "set grid off"
    "set mpdraw off"
    "set xlopts 1 5 0.10"
    "set ylopts 1 5 0.10" ; "set ylpos 0 l"
    "set xlab off" ; if (b = 8) ; "set xlab on" ; endif
*   "set ylint 2"  ; if (b = 3) ; "set ylint 1.0" ; endif
    if (a = 1) ; "set ylab  on" ; endif
    if (a = 2) ; "set ylab off" ; endif
    "set t 1 "timb
    "set vrange "rng ; "set ylint "yint
*   "set xlabs 1990 | 2005"
    "set cthick 8" ;* "set line "col.1" "sty.1" 8" ; "set string 1 c 8"
*   "set ccolor "col.5 ; "set cstyle "sty.5 ; "set cmark 0" ; "set gxout line" ; "set grads off" ; "d "var".5"
    "set ccolor "col.3 ; "set cstyle "sty.3 ; "set cmark 0" ; "set gxout line" ; "set grads off" ; "d "var".3"
    "set ccolor "col.2 ; "set cstyle "sty.2 ; "set cmark 0" ; "set gxout line" ; "set grads off" ; "d "var".2"
    "set ccolor "col.1 ; "set cstyle "sty.1 ; "set cmark 0" ; "set gxout line" ; "set grads off" ; "d "var".1"

    mid = mid - lap / 2
    "set parea "mid" "rig" "low.b" "hig.b
    "set mproj off"
    "set grid off"
    "set mpdraw off"
    "set xlopts 1 5 0.10"
    "set ylopts 1 5 0.10" ; "set ylpos 0 r"
    "set xlab off" ; if (b = 8) ; "set xlab on" ; endif
*   "set ylint 2"  ; if (b = 3) ; "set ylint 1.0" ; endif
    if (a = 1) ; "set ylab off" ; endif
    if (a = 2) ; "set ylab  on" ; endif
*   "set dfile 3" ; "set t 1 "timb
    "set vrange "rng ; "set ylint "yint
*   "set xlabs | | 2025 | | 2045 | | 2065 | | 2085 |"
    "set cthick 8" ;* "set line "col.3" "sty.3" 8" ; "set string 1 c 8"
*   "set ccolor "col.7 ; "set cstyle "sty.7 ; "set cmark 0" ; "set gxout line" ; "set grads off" ; "d "var".7"
*   "set ccolor "col.5 ; "set cstyle "sty.5 ; "set cmark 0" ; "set gxout line" ; "set grads off" ; "d "var".5"
*   "set ccolor "col.4 ; "set cstyle "sty.4 ; "set cmark 0" ; "set gxout line" ; "set grads off" ; "d "var".4"
*   "set ccolor "col.3 ; "set cstyle "sty.3 ; "set cmark 0" ; "set gxout line" ; "set grads off" ; "d "var".3"
    "set strsiz 0.15" ; "set string 1 c 6" ; "draw string      4.3  "hig.b+0.15" "til
*   "set strsiz 0.19" ; "set string 1 c 6" ; "draw string "mid+0.2" "hig.b-0.2"  "lab
*   "close 7"
*   "close 6"
*   "close 5"
*   "close 4"
    "close 3"
    "close 2"
    "close 1"
    b = b + 1
  endwhile
  a = a + 1
endwhile
"set parea off"

if (substr(stemb,1,3) = "ccs") ; modlab.1 = "CCSM-4 RCP-8.5" ; modlab.2 =        "" ; endif
if (substr(stemb,1,3) = "h45") ; modlab.1 = "HadGEM RCP-4.5" ; modlab.2 = "RCP-8.5" ; endif
if (substr(stemb,1,3) = "wrf") ; modlab.1 = "MPI-LR SSP-8.5" ; modlab.2 =        "" ; endif

cen = 0.62 ; top = hig.2-0.5 ; del = -0.3
label.1 = cen" "top-1*del" ERA5"
cen = 0.62 ; top = hig.1-0.5
label.2 = cen" "top-1*del" "modlab.1
cen = 3.0
label.3 = cen" "top-1*del" "modlab.2
"set strsiz 0.15"
                      "set string "col.1" l 8" ; "draw string "label.1
                      "set string "col.2" l 8" ; "draw string "label.2
if (modlab.2 != "") ; "set string "col.3" l 8" ; "draw string "label.3 ; endif

*cen = 2.0  ; top = hig.1-0.5 ; del = -0.3
*cen = 6.5  ; top = hig.1-0.5 ; del = -0.3
*label.1 = cen+1.33" "top-1*del" RCP4.5"
*label.2 = cen+2.33" "top-1*del" RCP8.5"
*label.3 = cen+0.09" "top-0*del" WRF"
*label.4 = cen+0.09" "top-1*del" WRF"
*label.5 = cen-0.94" "top-0*del" NARR"
*label.6 = cen-0.94" "top-1*del" NARR"
*"set strsiz 0.15"
*"set string "col.3" l 8" ; "draw string "label.1
*"set string "col.4" l 8" ; "draw string "label.2
*"set string "col.3" l 8" ; "draw string "label.3
*"set string "col.4" l 8" ; "draw string "label.4
*"set string "col.6" l 8" ; "draw string "label.5
*"set string "col.7" l 8" ; "draw string "label.6

hig.1 = hig.1 + 0.5 ; cen = 5.0
*"set string "col.2" c 6" ; "draw string "cen-4.0" "hig.1" Historical"
*"set string "col.4" c 6" ; "draw string "cen+1.5" "hig.1" RCP4.5"
*"set string "col.5" c 6" ; "draw string "cen+2.5" "hig.1" RCP8.5"

*top = hig.1+0.2
*label.1 = "1.0 "top" Arctic"
*label.2 = "7.5 "top" N.Atlantic"
*"set strsiz 0.19"
*"set string 1 l 8" ; "draw string "label.1
*"set string 1 r 8" ; "draw string "label.2

plot = "plot."stema"."stemb"."stemc".byday.avg.png"
say "printim "plot" png white x1700 y2200"
    "printim "plot" png white x1700 y2200"
"quit"


function inner_decomp(args)
  lef = subwrd(args,1)
  rig = subwrd(args,2)
  wid = subwrd(args,3)
  num = subwrd(args,4)
  _retmid.1   = lef + wid / 2
  _retmid.num = rig - wid / 2
  a = 2
  while (a < num)
    _retmid.a = (_retmid.num * (a-1) + _retmid.1 * (num-a)) / (num - 1)
    a = a + 1
  endwhile

  a = 1
  while (a <= num)
    _retlef.a = _retmid.a - wid / 2
    _retrig.a = _retmid.a + wid / 2
    a = a + 1
  endwhile
return
