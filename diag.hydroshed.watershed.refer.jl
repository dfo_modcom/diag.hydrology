#=
 = Unpack individual river and basin networks that flow to the
 = ocean using the HydroSHEDs 15-sec river and basin shapefiles.
 = Computations are done mainly by ogr2ogr (GDAL) and watersheds
 = smaller than a minimum (UCUT) are ignored - RD Apr 2021, 2023.
 = (Geometry -> ST_buffer(Geometry,0.001) seems less noisy however)
 =#

using My, Printf

const UCUT             = 200.0                          # minimum upstream watershed area (km2)

if (argc = length(ARGS)) != 2
  print("\nUsage: jjj $(basename(@__FILE__)) HydroRIVERS_v10.shp hybas_na_lev12_v1c.shp\n\n")
  exit(1)
end
rivr = split(ARGS[1], ".shp")[1] ; rivs = rivr * "_30_85N_180_00W"
basn = split(ARGS[2], ".shp")[1] ; bass = basn * "_30_85N_180_00W"
print("\n")

if !isfile("$rivs.shp") || !isfile("$rivs.csv")                               # constrain the river network
  print("ogr2ogr -spat -180.0 30.0 0.0 85.0 $rivs.shp $rivr.shp\n")           # to northern ocean outflows
    run(`ogr2ogr -spat -180.0 30.0 0.0 85.0 $rivs.shp $rivr.shp`)
  print("ogr2ogr -sql \"SELECT HYRIV_ID, MAIN_RIV, UPLAND_SKM, HYBAS_L12 FROM $rivs WHERE (NEXT_DOWN = 0) ORDER BY UPLAND_SKM DESC\" $rivs.csv $rivs.shp\n")
    run(`ogr2ogr -sql  "SELECT HYRIV_ID, MAIN_RIV, UPLAND_SKM, HYBAS_L12 FROM $rivs WHERE (NEXT_DOWN = 0) ORDER BY UPLAND_SKM DESC"  $rivs.csv $rivs.shp`)
end
fpa = My.ouvre("$rivs.csv", "r") ; lins = readlines(fpa) ; close(fpa)

for a = 2:length(lins)                                                        # and for each outflow whose upstream area
  temp    = split(lins[a], ",")                                               # is larger than UCUT, group all connected
  rivnum  =     parse(  Int64,       temp[1][2:end-1])                        # basins and save the upstream shape, with
  rivcol  = sum(parse.( Int64, split(temp[1][2:end-1], ""))) % 14 + 2         # a list of shapefile names and a colour
  mainum  =     parse(  Int64,       temp[2][2:end-1])                        # between 2 and 15 (fixed by rivnum)
  upland  =     parse(Float64,       temp[3])
  basnum  =     parse(  Int64,       temp[4][2:end-1])
  rivnum != mainum && error("\nERROR : $rivnum != $mainum\n\n")

  if upland >= UCUT
    tail = @sprintf("%9.0f_%9d_%11d", upland * 10, rivnum, basnum) ; tail = replace(tail, ' ' => '0')
    isfile("bas$tail.shp") && error("\nERROR : bas$tail.shp already exists\n\n")
    print("ogr2ogr -sql \"SELECT * FROM $basn WHERE (MAIN_BAS = $basnum)\" bas$tail.shp $basn.shp\n")
      run(`ogr2ogr -sql  "SELECT * FROM $basn WHERE (MAIN_BAS = $basnum)"  bas$tail.shp $basn.shp`)
    fsiz = 0 ; isfile("bas$tail.shp") && (fsiz = filesize("bas$tail.shp"))
    if fsiz > 200
      print("ogr2ogr -f \"ESRI Shapefile\" grp$tail.shp bas$tail.shp -dialect sqlite -sql \"select ST_union(ST_buffer(Geometry,0.001)) from bas$tail\"\n")
        run(`ogr2ogr -f  "ESRI Shapefile"  grp$tail.shp bas$tail.shp -dialect sqlite -sql  "select ST_union(ST_buffer(Geometry,0.001)) from bas$tail"`)
#     print("ogr2ogr -f \"ESRI Shapefile\" grp$tail.shp bas$tail.shp -dialect sqlite -sql \"select ST_union(ST_buffer(Geometry,0.001)) from bas$tail\" -simplify 0.01\n")
#       run(`ogr2ogr -f  "ESRI Shapefile"  grp$tail.shp bas$tail.shp -dialect sqlite -sql  "select ST_union(ST_buffer(Geometry,0.001)) from bas$tail"  -simplify 0.01`)
#     if !isfile("$rivo.shp")
#       print("ogr2ogr                 $rivo.shp grp$tail.shp\n")
#         run(`ogr2ogr                 $rivo.shp grp$tail.shp`)
#     else
#       print("ogr2ogr -update -append $rivo.shp grp$tail.shp -nln $rivo\n")
#         run(`ogr2ogr -update -append $rivo.shp grp$tail.shp -nln $rivo`)
#     end
#     form = @sprintf("%s %s %3d\n", lins[a], "grp$tail.shp", rivcol)
#     write(fpb, form)
#   else
#     print("SKIPPING bas$tail.shp with size $fsiz\n")
    end
    rm("bas$tail.dbf") ; rm("bas$tail.prj")
    rm("bas$tail.shp") ; rm("bas$tail.shx")
  end
end
#close(fpb)
exit(0)

#=
# if upland > 10000 && basnum != 7120034520 && basnum != 7120028670 
  if upland > 200 # && basnum != 7120034520 && basnum != 7120028670

print("ogr2ogr -f \"ESRI Shapefile\" wsheds.shp groups.shp -dialect sqlite -sql \"select ST_union(Geometry) from groups\"\n")
  run(`ogr2ogr -f  "ESRI Shapefile"  wsheds.shp groups.shp -dialect sqlite -sql  "select ST_union(Geometry) from groups"`)
print("ogr2ogr -clipsrc wsheds.shp rivclp.shp riva.shp\n")
  run(`ogr2ogr -clipsrc wsheds.shp rivclp.shp riva.shp`)

       loop over HYBAS_L12 in rivb.csv
       ogr2ogr -sql "SELECT * FROM hybas_na_lev12_v1c WHERE (MAIN_BAS = 7120034520)" basaaa.shp hybas_na_lev12_v1c.shp
       ogr2ogr -sql "SELECT * FROM hybas_na_lev12_v1c WHERE (MAIN_BAS = 7120031710)" bas001.shp hybas_na_lev12_v1c.shp
       ogr2ogr -f "ESRI Shapefile" wat001.shp bas001.shp -dialect sqlite -sql "select ST_union(Geometry) from bas001"
       ogr2ogr -clipsrc wat001.shp riv001.shp riva.shp

# (HYRIV_ID, MAIN_RIV, UPLAND_SKM, HYBAS_L12) = split(lins[a], ",")
# @show HYRIV_ID, MAIN_RIV, parse(Float64, UPLAND_SKM), parse(Int64, HYBAS_L12[2:end-1])

# https://gdal.org/programs/ogr2ogr.html
# https://github.com/dwtkns/gdal-cheat-sheet
# https://www.gaia-gis.it/gaia-sins/spatialite-sql-4.3.0.html#p4
# https://gis.stackexchange.com/questions/237644/what-does-the-st-union-of-the-geometry-column-of-two-tables-produce

       ogr2ogr -spat -75.0 40.0 -40.0 60.0                                      hybas_na_lev12_v1c_corner.shp         hybas_na_lev12_v1c.shp
       ogr2ogr -sql "SELECT * FROM hybas_na_lev12_v1c_corner WHERE (COAST = 1)" hybas_na_lev12_v1c_coast.shp          hybas_na_lev12_v1c_corner.shp
       ogr2ogr -sql "SELECT HYBAS_ID, UP_AREA, SUB_AREA FROM hybas_na_lev12_v1c_coast ORDER BY UP_AREA DESC"    hybas_na_lev12_v1c_coast_HYBAS_ID.csv hybas_na_lev12_v1c_coast.shp
       ogr2ogr -sql "SELECT UP_AREA  FROM hybas_na_lev12_v1c_coast"             hybas_na_lev12_v1c_coast_UP_AREA.csv  hybas_na_lev12_v1c_coast.shp

       ogr2ogr -spat -95.0 40.0 -50.0 60.0                                      riva.shp         HydroRIVERS_v10_na.shp
       ogr2ogr -spat -75.0 40.0 -40.0 60.0                                      rivb.shp         HydroRIVERS_v10_na.shp
       ogr2ogr -sql "SELECT HYRIV_ID, MAIN_RIV, UPLAND_SKM, HYBAS_L12 FROM rivb WHERE (NEXT_DOWN = 0) ORDER BY UPLAND_SKM DESC" rivb.csv rivb.shp
#      loop over HYBAS_L12 in rivb.csv
       ogr2ogr -sql "SELECT * FROM hybas_na_lev12_v1c WHERE (MAIN_BAS = 7120034520)" basaaa.shp hybas_na_lev12_v1c.shp
       ogr2ogr -sql "SELECT * FROM hybas_na_lev12_v1c WHERE (MAIN_BAS = 7120031710)" bas001.shp hybas_na_lev12_v1c.shp
       ogr2ogr -f "ESRI Shapefile" wat001.shp bas001.shp -dialect sqlite -sql "select ST_union(Geometry) from bas001"
       ogr2ogr -clipsrc wat001.shp riv001.shp riva.shp

       ogr2ogr -f "ESRI Shapefile" dissolved.shp hybas_na_lev12_v1c_corner.shp -dialect sqlite -sql "select ST_union(Geometry)    from hybas_na_lev12_v1c_corner"
       ogr2ogr -f "ESRI Shapefile" dissolved.shp hybas_na_lev12_v1c_corner.shp -dialect sqlite -sql "select ST_envelope(Geometry) from hybas_na_lev12_v1c_corner"
       ogr2ogr -f "ESRI Shapefile" dissolved.shp hybas_na_lev12_v1c_corner.shp -dialect sqlite -sql "select ST_boundary(Geometry) from hybas_na_lev12_v1c_corner"


# ogr2ogr -f CSV -lco GEOMETRY=AS_XY -lco SEPARATOR=TAB warmest_hs.csv -sql "SELECT f.the_geom, f.bright_t31, ac.iso2, ac.country_name FROM chp01.global_24h as f JOIN chp01.africa_countries as ac ON ST_Contains(ac.the_geom, ST_Transform(f.the_geom, 4326)) ORDER BY f.bright_t31 DESC  LIMIT 100"

       ogrinfo -so                                                                        HydroLAKES_polys_v10_Atlantic_10km2.shp  HydroLAKES_polys_v10_Atlantic_10km2
       ogr2ogr -sql "SELECT * FROM HydroLAKES_polys_v10_Atlantic WHERE (Lake_area >  50)" HydroLAKES_polys_v10_Atlantic_50km2.shp  hybas_na_lev09_v1c_corner.shp
       ogrinfo -so                                                                        HydroLAKES_polys_v10_Atlantic_50km2.shp  HydroLAKES_polys_v10_Atlantic_50km2

# R"require(rgdal)"
# remp = rcopy(R"shape <- readOGR(dsn = \".\", layer = \"hybas_na_lev02_v1c\")")
# R"st_combine(shape)"
# lines<-st_as_sf(lines)
# lines<-lines%>%filter(X>400 & Y=="YES")

library(rgdal)
nc <- readOGR(dsn = ".", layer = "hybas_na_lev02_v1c")
library(sf)
nf <- st_as_sf(nc)
co <- nf %>% filter(COAST > 1)
methods(class = "sf")

R"library(sf)"
R"nc <- st_read(system.file("./hybas_na_lev02_v1c.shp", package="sf"))"
# R"fname <- system.file("shape/nc.shp", package="sf")"
# R"nc <- st_read(system.file("shape/nc.shp", package="sf"))"
R"fname <- system.file(\"./hybas_na_lev02_v1c.shp\", package=\"sf\")"

# if detmcd                                                                   # get a mask to exclude outliers using DetMCD in R
#   temp = [cc ss tt uu vv ww]                                                # (before precalibration, avgc = remp[:center][1],
#   remp = rcopy(R"DetMCD($temp, alpha = $limmcd)")                           # varc = remp[:cov][1,1], cvcu = remp[:cov][1,4])
#   mask = falses(length(cc)) ; for a in remp[:Hsubsets]  mask[a] = true  end
# else
#   mask =  trues(length(cc))
# end
# shape <- readOGR(dsn = ".", layer = "hybas_na_lev02_v1c")

OGR data source with driver: ESRI Shapefile
Source: "/home/riced/work/workg/NRCAN/hys", layer: "hybas_na_lev02_v1c"
with 8 features
It has 13 fields
Integer64 fields read as strings:  HYBAS_ID NEXT_DOWN NEXT_SINK MAIN_BAS SORT

> shape$HYBAS_ID

[1] 7020000010 7020014250 7020021430 7020024600 7020038340 7020046750 7020047840
[8] 7020065090
8 Levels: 7020000010 7020014250 7020021430 7020024600 ... 7020065090

> shape$MAIN_BAS

[1] 7020000010 7020014250 7020021430 7020024600 7020038340 7020046750 7020047840
[8] 7020065090
8 Levels: 7020000010 7020014250 7020021430 7020024600 ... 7020065090

> shape$SORT

[1] 1 2 3 4 5 6 7 8
Levels: 1 2 3 4 5 6 7 8
=#
