* This script is designed to plot daily/monthly and annual-mean (daily) streamflow
* It can be executed using a command like
*
*     grads -bpc "diag.wrf.ocean.entry.timeseries.cmipval hydat_valding_aall_1000_ _1990-01-01_2022-12-30 neural neuall.identity"
*
* - RD Jan 2024.

function plot(args)
stema = subwrd(args,1)
stemb = subwrd(args,2)
stemc = subwrd(args,3)
stemd = subwrd(args,4)

"sdfopen "stema"iccs"stemb"."stemc".daily.nc"     ; "q file 1" ; ret = sublin(result,5) ; tima = subwrd(ret,12) / 6
"sdfopen "stema"iccs"stemb"."stemc".monthly.nc"   ; "q file 2" ; ret = sublin(result,5) ; timb = subwrd(ret,12) / 6
"sdfopen "stema"iccs"stemb"."stemc".annualday.nc" ; "q file 3" ; ret = sublin(result,5) ; timc = subwrd(ret,12)
"sdfopen "stema"iccs"stemb"."stemc"."stemd".daily.nc"          ; lats = subwrd(ret, 6)
"sdfopen "stema"iccs"stemb"."stemc"."stemd".monthly.nc"        ; lons = subwrd(ret, 3)
"sdfopen "stema"iccs"stemb"."stemc"."stemd".annualday.nc"
"sdfopen "stema"ih45"stemb"."stemc".daily.nc"
"sdfopen "stema"ih45"stemb"."stemc".monthly.nc"
"sdfopen "stema"ih45"stemb"."stemc".annualday.nc"
"sdfopen "stema"ih45"stemb"."stemc"."stemd".daily.nc"
"sdfopen "stema"ih45"stemb"."stemc"."stemd".monthly.nc"
"sdfopen "stema"ih45"stemb"."stemc"."stemd".annualday.nc"
"sdfopen "stema"ih85"stemb"."stemc".daily.nc"
"sdfopen "stema"ih85"stemb"."stemc".monthly.nc"
"sdfopen "stema"ih85"stemb"."stemc".annualday.nc"
"sdfopen "stema"ih85"stemb"."stemc"."stemd".daily.nc"
"sdfopen "stema"ih85"stemb"."stemc"."stemd".monthly.nc"
"sdfopen "stema"ih85"stemb"."stemc"."stemd".annualday.nc"
"sdfopen "stema"impi"stemb"."stemc".daily.nc"
"sdfopen "stema"impi"stemb"."stemc".monthly.nc"
"sdfopen "stema"impi"stemb"."stemc".annualday.nc"
"sdfopen "stema"impi"stemb"."stemc"."stemd".daily.nc"
"sdfopen "stema"impi"stemb"."stemc"."stemd".monthly.nc"
"sdfopen "stema"impi"stemb"."stemc"."stemd".annualday.nc"

c = 1
lista = stema"iccs"stemb".txt"
filestat = read(lista)
while (sublin(filestat,1) = 0 & sublin(filestat,2) != "")
  if (c < 10) ; tail.c = "0"c ; else ; tail.c = c ; endif
  line = sublin(filestat,2) ; nam.c = subwrd(line,1)
  if (substr(nam.1,1,1) = "h") ; aa = 8 ; bb = 7 ; else ; aa = 9 ; bb = 8 ; endif
  a = 1 ; while (substr(line,a,1) != "[") ; a = a + 1 ; endwhile ; a = a + 1 + 8
  b = a ; while (substr(line,b,1) != "]") ; b = b + 1 ; endwhile ; b = b - a
  riv.c =        substr(line,a,b) ; stn.c = substr(line,a-aa,bb)   ; c = c + 1
  filestat = read(lista)
endwhile
filestat = close(lista) ; riv.c = "All Stations"
if (substr(nam.1,1,1) = "h") ; nam.c = "hydat_9999999" ; else ; nam.c = "wrfh_99999999" ; endif
if (c < 10) ; tail.c = "0"c ; else ; tail.c = c ; endif
*if (c =  2) ; c = 1 ; endif

lef = 1.0 ; rig = 7.5 ; cen = (lef + rig) / 2
ypic = 3 ; string = "0.5 10.5 3.0 "ypic ; inner_decomp(string)
a = 1 ; while (a <= ypic) ; b = ypic - a + 1 ; low.b = _retlef.a ; mid.b = _retmid.a ; hig.b = _retrig.a ; a = a + 1 ; endwhile
"set lwid 20 14"
"set lwid 22 14"
"set rgb 55   0 200 200 100"
"set rgb 66   0 255   0 100"
"set rgb 88 240 130  40 100"
"set rgb 99 160   0 200 100"

z =  79 ; while (z < c)
* 79 157
* z = 58 ; while (z < 59)
* z = 86 ; while (z < 87)
* if (z = 19) ; z = 20 ; endif
* if (z = 36) ; z = 37 ; endif
  "clear" ; "set datawarn off"
* if (z < c)
    "set dfile 1" ; tims = tima ; "set parea "lef" "rig" "low.1" "hig.1
    "set gxout stat" ; max = 0
    "set t 1 "tims ; "set y "z ; "set x 1" ; "d hydt" ; ret = sublin(result,8) ; val = subwrd(ret,5) ; if (val > max) ; max = val ; endif
    "set t 1 "tims ; "set y "z ; "set x 1" ; "d flow" ; ret = sublin(result,8) ; val = subwrd(ret,5) ; if (val > max) ; max = val ; endif
    "set t 1 "tims ; "set y "z ; "set x 2" ; "d flow" ; ret = sublin(result,8) ; val = subwrd(ret,5) ; if (val > max) ; max = val ; endif
    "set t 1 "tims ; "set y "z ; "set x 3" ; "d flow" ; ret = sublin(result,8) ; val = subwrd(ret,5) ; if (val > max) ; max = val ; endif
    "set t 1 "tims ; "set y "z ; "set x 4" ; "d flow" ; ret = sublin(result,8) ; val = subwrd(ret,5) ; if (val > max) ; max = val ; endif
    max = max / 1  ;  vran =20000 ; vint = 4000
    if (max <10000) ; vran =10000 ; vint = 2000 ; endif
    if (max < 5000) ; vran = 5000 ; vint = 1000 ; endif
    if (max < 4000) ; vran = 4000 ; vint =  800 ; endif
    if (max < 3000) ; vran = 3000 ; vint =  500 ; endif
    if (max < 2000) ; vran = 2000 ; vint =  400 ; endif
    if (max < 1000) ; vran = 1000 ; vint =  200 ; endif
    if (max <  500) ; vran =  500 ; vint =  100 ; endif
    if (max <  400) ; vran =  400 ; vint =   80 ; endif
    if (max <  300) ; vran =  300 ; vint =   50 ; endif
    if (max <  200) ; vran =  200 ; vint =   40 ; endif
    if (max <  100) ; vran =  100 ; vint =   20 ; endif
    if (max <   50) ; vran =   50 ; vint =   10 ; endif
    if (max <   30) ; vran =   30 ; vint =    5 ; endif
    if (max <   10) ; vran =   10 ; vint =    2 ; endif
    a = 1 ; while (a < 5) ; "set t 1" ; "set y "z ; "set x "a
      "d tmpa" ; ret = sublin(result,8) ; val = subwrd(ret,5) ; nse = math_format("%5.2f",val) ; "d tmpb" ; ret = sublin(result,8) ; val = subwrd(ret,5) ; cor = math_format("%5.2f",val)
      "d tmpd" ; ret = sublin(result,8) ; val = subwrd(ret,5) ; rms = math_format("%4.0f",val) ; "d tmpe" ; ret = sublin(result,8) ; val = subwrd(ret,5) ; bia = math_format("%4.0f",val)
      line.a = nse" "cor" "rms" "bia
    a = a + 1 ; endwhile
    "set t 1 "tims ; "set gxout line" ; "set mproj off" ; "set grid off" ; "set mpdraw off" ; "set xlab on" ; "set ylab on"
    "set vrange 0 "vran ; "set ylint "vint ; "set cthick 12" ; "set line 1 1 12" ; "set string 1 c 8" ; "set missconn off"
    "set ccolor  1" ; "set cstyle 1" ; "set cmark 0" ; "set grads off" ; "set y "z ; "set x 1" ; "d hydt"
    "set ccolor  7" ; "set cstyle 1" ; "set cmark 0" ; "set grads off" ; "set y "z ; "set x 1" ; "d flow"
    "set ccolor  8" ; "set cstyle 1" ; "set cmark 0" ; "set grads off" ; "set y "z ; "set x 2" ; "d flow"
    "set ccolor  9" ; "set cstyle 1" ; "set cmark 0" ; "set grads off" ; "set y "z ; "set x 4" ; "d flow.4"
    "set ccolor  7" ; "set cstyle 2" ; "set cmark 0" ; "set grads off" ; "set y "z ; "set x 1" ; "d flow.7"
    "set ccolor  8" ; "set cstyle 2" ; "set cmark 0" ; "set grads off" ; "set y "z ; "set x 2" ; "d flow.7"
    "set ccolor  9" ; "set cstyle 2" ; "set cmark 0" ; "set grads off" ; "set y "z ; "set x 4" ; "d flow.10"
    "set ccolor  7" ; "set cstyle 3" ; "set cmark 0" ; "set grads off" ; "set y "z ; "set x 1" ; "d flow.13"
    "set ccolor  8" ; "set cstyle 3" ; "set cmark 0" ; "set grads off" ; "set y "z ; "set x 2" ; "d flow.13"
    "set ccolor  9" ; "set cstyle 3" ; "set cmark 0" ; "set grads off" ; "set y "z ; "set x 4" ; "d flow.16"
    "set ccolor  7" ; "set cstyle 5" ; "set cmark 0" ; "set grads off" ; "set y "z ; "set x 1" ; "d flow.19"
    "set ccolor  8" ; "set cstyle 5" ; "set cmark 0" ; "set grads off" ; "set y "z ; "set x 2" ; "d flow.19"
    "set ccolor  9" ; "set cstyle 5" ; "set cmark 0" ; "set grads off" ; "set y "z ; "set x 4" ; "d flow.22"
*   "set ccolor  1" ; "set cstyle 1" ; "set cmark 0" ; "set grads off" ; "set y "z ; "set x 1" ; "d hydt+"vint
    "set cthick 22" ; "set line 1 1 22"
    "set ccolor 55" ; "set cstyle 1" ; "set cmark 0" ; "set grads off" ; "set y "z ; "set x    1" ; "d maskout(maskout(0*hydi,2.5+hydi),-1.5-hydi)"
    "set ccolor 66" ; "set cstyle 1" ; "set cmark 0" ; "set grads off" ; "set y "z ; "set x    1" ; "d maskout(maskout(0*hydi,4.5+hydi),-3.5-hydi)"
    "set cthick 12" ; "set line 1 1 12"
    "set strsiz 0.19" ; "set string 1 c 6" ; "draw string "cen-0.0" 10.7 "riv.z" flow (m`a3`n/s)"
    "set strsiz 0.12" ; "set string 1 l 6" ; "draw string 1.1 10.3 "stn.z
*   xa = 4.8 ; xb = 4.9 ; ya = hig.1-0.6 ; "set line  9 1 22" ; "draw line "xa" "ya" "xb" "ya
*                         ya = hig.1-0.4 ; "set line  8 1 22" ; "draw line "xa" "ya" "xb" "ya
*                         ya = hig.1-0.2 ; "set line  1 1 22" ; "draw line "xa" "ya" "xb" "ya
*   xa = 5.9 ; xb = 6.0 ; ya = hig.1-0.2 ; "set line 55 1 22" ; "draw line "xa" "ya" "xb" "ya
*   xa = 6.5 ; xb = 6.6 ; ya = hig.1-0.2 ; "set line 66 1 22" ; "draw line "xa" "ya" "xb" "ya
*   xa = 5.1 ;            ya = hig.1-0.6 ; "set strsiz 0.12" ; "set string 1 l 6" ; "draw string "xa" "ya" ERA-5 forcing-neural"
*                         ya = hig.1-0.4 ; "set strsiz 0.12" ; "set string 1 l 6" ; "draw string "xa" "ya" ERA-5 forcing-raw"
*                         ya = hig.1-0.2 ; "set strsiz 0.12" ; "set string 1 l 6" ; "draw string "xa" "ya" HYDAT ("
*   xa = 6.1 ;            ya = hig.1-0.2 ; "set strsiz 0.12" ; "set string 1 l 6" ; "draw string "xa" "ya" ice"
*   xa = 6.7 ;            ya = hig.1-0.2 ; "set strsiz 0.12" ; "set string 1 l 6" ; "draw string "xa" "ya" estim)"
*   xa = 4.6 ;            ya = hig.1-0.6 ; "set strsiz 0.12" ; "set string 1 r 6" ; "draw string "xa" "ya" "line.4
*   xa = 4.6 ;            ya = hig.1-0.4 ; "set strsiz 0.12" ; "set string 1 r 6" ; "draw string "xa" "ya" "line.3
*   xa = 4.6 ;            ya = hig.1-0.2 ; "set strsiz 0.12" ; "set string 1 r 6" ; "draw string "xa" "ya" NSE`3 `0 COR`3 `0 RMS`3 `0 BIAS"

    "set dfile 2" ; tims = timb ; "set parea "lef" "rig" "low.2" "hig.2
    "set gxout stat" ; max = 0
    "set t 1 "tims ; "set y "z ; "set x 1" ; "d hydt" ; ret = sublin(result,8) ; val = subwrd(ret,5) ; if (val > max) ; max = val ; endif
    "set t 1 "tims ; "set y "z ; "set x 1" ; "d flow" ; ret = sublin(result,8) ; val = subwrd(ret,5) ; if (val > max) ; max = val ; endif
    "set t 1 "tims ; "set y "z ; "set x 2" ; "d flow" ; ret = sublin(result,8) ; val = subwrd(ret,5) ; if (val > max) ; max = val ; endif
    "set t 1 "tims ; "set y "z ; "set x 3" ; "d flow" ; ret = sublin(result,8) ; val = subwrd(ret,5) ; if (val > max) ; max = val ; endif
    "set t 1 "tims ; "set y "z ; "set x 4" ; "d flow" ; ret = sublin(result,8) ; val = subwrd(ret,5) ; if (val > max) ; max = val ; endif
                      vran =20000 ; vint = 4000
    if (max <10000) ; vran =10000 ; vint = 2000 ; endif
    if (max < 5000) ; vran = 5000 ; vint = 1000 ; endif
    if (max < 4000) ; vran = 4000 ; vint =  800 ; endif
    if (max < 3000) ; vran = 3000 ; vint =  500 ; endif
    if (max < 2000) ; vran = 2000 ; vint =  400 ; endif
    if (max < 1000) ; vran = 1000 ; vint =  200 ; endif
    if (max <  500) ; vran =  500 ; vint =  100 ; endif
    if (max <  400) ; vran =  400 ; vint =   80 ; endif
    if (max <  300) ; vran =  300 ; vint =   50 ; endif
    if (max <  200) ; vran =  200 ; vint =   40 ; endif
    if (max <  100) ; vran =  100 ; vint =   20 ; endif
    if (max <   50) ; vran =   50 ; vint =   10 ; endif
    if (max <   30) ; vran =   30 ; vint =    5 ; endif
    if (max <   10) ; vran =   10 ; vint =    2 ; endif
    a = 1 ; while (a < 5) ; "set t 1" ; "set y "z ; "set x "a
      "d tmpa" ; ret = sublin(result,8) ; val = subwrd(ret,5) ; nse = math_format("%5.2f",val) ; "d tmpb" ; ret = sublin(result,8) ; val = subwrd(ret,5) ; cor = math_format("%5.2f",val)
      "d tmpd" ; ret = sublin(result,8) ; val = subwrd(ret,5) ; rms = math_format("%4.0f",val) ; "d tmpe" ; ret = sublin(result,8) ; val = subwrd(ret,5) ; bia = math_format("%4.0f",val)
      line.a = nse" "cor" "rms" "bia
    a = a + 1 ; endwhile
    "set t 0.5 "tims+0.5 ; "set gxout line" ; "set mproj off" ; "set grid off" ; "set mpdraw off" ; "set xlab on" ; "set ylab on"
    "set vrange 0 "vran ; "set ylint "vint ; "set cthick 12" ; "set line 1 1 12" ; "set string 1 c 8" ; "set missconn off"
    "set ccolor  1" ; "set cstyle 1" ; "set cmark 0" ; "set grads off" ; "set y "z ; "set x 1" ; "d hydt"
    "set ccolor  7" ; "set cstyle 1" ; "set cmark 0" ; "set grads off" ; "set y "z ; "set x 1" ; "d flow"
    "set ccolor  8" ; "set cstyle 1" ; "set cmark 0" ; "set grads off" ; "set y "z ; "set x 2" ; "d flow"
    "set ccolor  9" ; "set cstyle 1" ; "set cmark 0" ; "set grads off" ; "set y "z ; "set x 4" ; "d flow.5"
    "set ccolor  7" ; "set cstyle 2" ; "set cmark 0" ; "set grads off" ; "set y "z ; "set x 1" ; "d flow.8"
    "set ccolor  8" ; "set cstyle 2" ; "set cmark 0" ; "set grads off" ; "set y "z ; "set x 2" ; "d flow.8"
    "set ccolor  9" ; "set cstyle 2" ; "set cmark 0" ; "set grads off" ; "set y "z ; "set x 4" ; "d flow.11"
    "set ccolor  7" ; "set cstyle 3" ; "set cmark 0" ; "set grads off" ; "set y "z ; "set x 1" ; "d flow.14"
    "set ccolor  8" ; "set cstyle 3" ; "set cmark 0" ; "set grads off" ; "set y "z ; "set x 2" ; "d flow.14"
    "set ccolor  9" ; "set cstyle 3" ; "set cmark 0" ; "set grads off" ; "set y "z ; "set x 4" ; "d flow.17"
    "set ccolor  7" ; "set cstyle 5" ; "set cmark 0" ; "set grads off" ; "set y "z ; "set x 1" ; "d flow.20"
    "set ccolor  8" ; "set cstyle 5" ; "set cmark 0" ; "set grads off" ; "set y "z ; "set x 2" ; "d flow.20"
    "set ccolor  9" ; "set cstyle 5" ; "set cmark 0" ; "set grads off" ; "set y "z ; "set x 4" ; "d flow.23"
*   "set ccolor  1" ; "set cstyle 1" ; "set cmark 0" ; "set grads off" ; "set y "z ; "set x 1" ; "d hydt"
    xa = 4.8 ; xb = 4.9 ; ya = hig.2-0.2 ; "set line  9 1 22" ; "draw line "xa" "ya" "xb" "ya
                          ya = hig.2-0.4 ; "set line  8 1 22" ; "draw line "xa" "ya" "xb" "ya
                          ya = hig.2-0.6 ; "set line  7 1 22" ; "draw line "xa" "ya" "xb" "ya
                          ya = hig.1-0.2 ; "set line  1 1 22" ; "draw line "xa" "ya" "xb" "ya
    xa = 5.9 ; xb = 6.0 ; ya = hig.1-0.2 ; "set line 55 1 22" ; "draw line "xa" "ya" "xb" "ya
    xa = 6.5 ; xb = 6.6 ; ya = hig.1-0.2 ; "set line 66 1 22" ; "draw line "xa" "ya" "xb" "ya
    xa = 5.1 ;            ya = hig.2-0.2 ; "set strsiz 0.12" ; "set string 1 l 6" ; "draw string "xa" "ya" NN2"
                          ya = hig.2-0.4 ; "set strsiz 0.12" ; "set string 1 l 6" ; "draw string "xa" "ya" NN1"
                          ya = hig.2-0.6 ; "set strsiz 0.12" ; "set string 1 l 6" ; "draw string "xa" "ya" RAW"
                          ya = hig.1-0.2 ; "set strsiz 0.12" ; "set string 1 l 6" ; "draw string "xa" "ya" HYDAT ("
    xa = 6.1 ;            ya = hig.1-0.2 ; "set strsiz 0.12" ; "set string 1 l 6" ; "draw string "xa" "ya" ice"
    xa = 6.7 ;            ya = hig.1-0.2 ; "set strsiz 0.12" ; "set string 1 l 6" ; "draw string "xa" "ya" estim)"
*   xa = 4.6 ;            ya = hig.2-0.6 ; "set strsiz 0.12" ; "set string 1 r 6" ; "draw string "xa" "ya" "line.4
*   xa = 4.6 ;            ya = hig.2-0.4 ; "set strsiz 0.12" ; "set string 1 r 6" ; "draw string "xa" "ya" "line.3
*   xa = 4.6 ;            ya = hig.2-0.2 ; "set strsiz 0.12" ; "set string 1 r 6" ; "draw string "xa" "ya" NSE`3 `0 COR`3 `0 RMS`3 `0 BIAS"
*   xa = 5.3 ;*           ya = hig.2-0.6 ; "set strsiz 0.12" ; "set string 1 l 6" ; "draw string "xa" "ya" WRF forcing-neural"
*                         ya = hig.2-0.4 ; "set strsiz 0.12" ; "set string 1 l 6" ; "draw string "xa" "ya" WRF forcing-raw"
*   xa = 7.3 ; xb = 7.4 ;*ya = hig.2-0.6 ; "set line  4 1 22" ; "draw line "xa" "ya" "xb" "ya
*                         ya = hig.2-0.4 ; "set line  7 1 22" ; "draw line "xa" "ya" "xb" "ya
    xa = 1.4 ; xb = 2.4 ; ya = hig.3-0.3 ; "set line  1 1 12" ; "draw line "xa" "ya" "xb" "ya ; yb = ya - 0.2 ; "draw string "xa" "yb" CCSM-8.5"
    xa = 2.9 ; xb = xa + 1.0             ; "set line  1 2 12" ; "draw line "xa" "ya" "xb" "ya ; yb = ya - 0.2 ; "draw string "xa" "yb" HadGEM-4.5"
    xa = 4.4 ; xb = xa + 1.0             ; "set line  1 3 12" ; "draw line "xa" "ya" "xb" "ya ; yb = ya - 0.2 ; "draw string "xa" "yb" HadGEM-8.5"
    xa = 5.9 ; xb = xa + 1.0             ; "set line  1 5 12" ; "draw line "xa" "ya" "xb" "ya ; yb = ya - 0.2 ; "draw string "xa" "yb" MPI-LR-8.5"


    "set dfile 3" ; tims = timc ; "set parea "lef" "rig" "low.3" "hig.3
    "set gxout stat" ; max = 0
    "set t 1 "tims ; "set y "z ; "set x 1" ; "d hydt" ; ret = sublin(result,8) ; val = subwrd(ret,5) ; if (val > max) ; max = val ; endif
    "set t 1 "tims ; "set y "z ; "set x 1" ; "d flow" ; ret = sublin(result,8) ; val = subwrd(ret,5) ; if (val > max) ; max = val ; endif
    "set t 1 "tims ; "set y "z ; "set x 2" ; "d flow" ; ret = sublin(result,8) ; val = subwrd(ret,5) ; if (val > max) ; max = val ; endif
    "set t 1 "tims ; "set y "z ; "set x 3" ; "d flow" ; ret = sublin(result,8) ; val = subwrd(ret,5) ; if (val > max) ; max = val ; endif
    "set t 1 "tims ; "set y "z ; "set x 4" ; "d flow" ; ret = sublin(result,8) ; val = subwrd(ret,5) ; if (val > max) ; max = val ; endif
                      vran =20000 ; vint = 4000
    if (max <10000) ; vran =10000 ; vint = 2000 ; endif
    if (max < 5000) ; vran = 5000 ; vint = 1000 ; endif
    if (max < 4000) ; vran = 4000 ; vint =  800 ; endif
    if (max < 3000) ; vran = 3000 ; vint =  500 ; endif
    if (max < 2000) ; vran = 2000 ; vint =  400 ; endif
    if (max < 1000) ; vran = 1000 ; vint =  200 ; endif
    if (max <  500) ; vran =  500 ; vint =  100 ; endif
    if (max <  400) ; vran =  400 ; vint =   80 ; endif
    if (max <  300) ; vran =  300 ; vint =   50 ; endif
    if (max <  200) ; vran =  200 ; vint =   40 ; endif
    if (max <  100) ; vran =  100 ; vint =   20 ; endif
    if (max <   50) ; vran =   50 ; vint =   10 ; endif
    if (max <   30) ; vran =   30 ; vint =    5 ; endif
    if (max <   10) ; vran =   10 ; vint =    2 ; endif
    a = 1 ; while (a < 5) ; "set t 1" ; "set y "z ; "set x "a
      "d tmpa" ; ret = sublin(result,8) ; val = subwrd(ret,5) ; nse = math_format("%5.2f",val) ; "d tmpb" ; ret = sublin(result,8) ; val = subwrd(ret,5) ; cor = math_format("%5.2f",val)
      "d tmpd" ; ret = sublin(result,8) ; val = subwrd(ret,5) ; rms = math_format("%4.0f",val) ; "d tmpe" ; ret = sublin(result,8) ; val = subwrd(ret,5) ; bia = math_format("%4.0f",val)
      line.a = nse" "cor" "rms" "bia
    a = a + 1 ; endwhile
    "set t 0.5 "tims+0.5 ; "set gxout line" ; "set mproj off" ; "set grid off" ; "set mpdraw off" ; "set xlab on" ; "set ylab on"
    "set vrange 0 "vran ; "set ylint "vint ; "set cthick 12" ; "set line 1 1 12" ; "set string 1 c 8" ; "set missconn off"
    "set ccolor  1" ; "set cstyle 1" ; "set cmark 0" ; "set grads off" ; "set y "z ; "set x 1" ; "d hydt"
    "set ccolor  7" ; "set cstyle 1" ; "set cmark 0" ; "set grads off" ; "set y "z ; "set x 1" ; "d flow"
    "set ccolor  8" ; "set cstyle 1" ; "set cmark 0" ; "set grads off" ; "set y "z ; "set x 2" ; "d flow"
    "set ccolor  9" ; "set cstyle 1" ; "set cmark 0" ; "set grads off" ; "set y "z ; "set x 4" ; "d flow.6"
    "set ccolor  7" ; "set cstyle 2" ; "set cmark 0" ; "set grads off" ; "set y "z ; "set x 1" ; "d flow.9"
    "set ccolor  8" ; "set cstyle 2" ; "set cmark 0" ; "set grads off" ; "set y "z ; "set x 2" ; "d flow.9"
    "set ccolor  9" ; "set cstyle 2" ; "set cmark 0" ; "set grads off" ; "set y "z ; "set x 4" ; "d flow.12"
    "set ccolor  7" ; "set cstyle 3" ; "set cmark 0" ; "set grads off" ; "set y "z ; "set x 1" ; "d flow.15"
    "set ccolor  8" ; "set cstyle 3" ; "set cmark 0" ; "set grads off" ; "set y "z ; "set x 2" ; "d flow.15"
    "set ccolor  9" ; "set cstyle 3" ; "set cmark 0" ; "set grads off" ; "set y "z ; "set x 4" ; "d flow.18"
    "set ccolor  7" ; "set cstyle 5" ; "set cmark 0" ; "set grads off" ; "set y "z ; "set x 1" ; "d flow.21"
    "set ccolor  8" ; "set cstyle 5" ; "set cmark 0" ; "set grads off" ; "set y "z ; "set x 2" ; "d flow.21"
    "set ccolor  9" ; "set cstyle 5" ; "set cmark 0" ; "set grads off" ; "set y "z ; "set x 4" ; "d flow.24"
*   "set ccolor  1" ; "set cstyle 1" ; "set cmark 0" ; "set grads off" ; "set y "z ; "set x 1" ; "d hydt"
*   xa = 4.8 ; xb = 4.9 ; ya = hig.3-0.6 ; "set line  9 1 22" ; "draw line "xa" "ya" "xb" "ya
*                         ya = hig.3-0.4 ; "set line  8 1 22" ; "draw line "xa" "ya" "xb" "ya
*                         ya = hig.3-0.2 ; "set line  1 1 22" ; "draw line "xa" "ya" "xb" "ya
*   xa = 4.6 ;            ya = hig.3-0.6 ; "set strsiz 0.12" ; "set string 1 r 6" ; "draw string "xa" "ya" "line.4
*                         ya = hig.3-0.4 ; "set strsiz 0.12" ; "set string 1 r 6" ; "draw string "xa" "ya" "line.3
*                         ya = hig.3-0.2 ; "set strsiz 0.12" ; "set string 1 r 6" ; "draw string "xa" "ya" NSE`3 `0 COR`3 `0 RMS`3 `0 BIAS"
*   xa = 5.3 ;            ya = hig.3-0.6 ; "set strsiz 0.12" ; "set string 1 l 6" ; "draw string "xa" "ya" WRF forcing-neural"
*                         ya = hig.3-0.4 ; "set strsiz 0.12" ; "set string 1 l 6" ; "draw string "xa" "ya" WRF forcing-raw"
*   xa = 7.3 ; xb = 7.4 ; ya = hig.3-0.6 ; "set line  4 1 22" ; "draw line "xa" "ya" "xb" "ya
*                         ya = hig.3-0.4 ; "set line  7 1 22" ; "draw line "xa" "ya" "xb" "ya
* else
*   "set strsiz 0.22" ; "set string 1 c 6" ; "draw string "cen-0.0" 7.9 All Stations"
* endif

  "set line 0" ; "draw recf 0.70 "low.3-0.40" 1.20 "low.3-0.20
  xa = 7.65 ; ya = mid.1 ; "set strsiz 0.12" ; "set string 1 c 6 90" ; "draw string "xa" "ya" Daily"
              ya = mid.2 ; "set strsiz 0.12" ; "set string 1 c 6 90" ; "draw string "xa" "ya" Monthly"
              ya = mid.3 ; "set strsiz 0.12" ; "set string 1 c 6 90" ; "draw string "xa" "ya" Annual-Mean Daily"
                                               "set string 1 c 6  0"

* plot = "plot."stema"."tail.z".png"
  plot = nam.z".hyd.png"
  say "printim "plot" png white x1700 y2200"
      "printim "plot" png white x1700 y2200"
  z = z + 1
  "quit"
endwhile
*"close 1"
"quit"


function inner_decomp(args)
  lef = subwrd(args,1)
  rig = subwrd(args,2)
  wid = subwrd(args,3)
  num = subwrd(args,4)
  _retmid.1   = lef + wid / 2
  _retmid.num = rig - wid / 2
  a = 2
  while (a < num)
    _retmid.a = (_retmid.num * (a-1) + _retmid.1 * (num-a)) / (num - 1)
    a = a + 1
  endwhile

  a = 1
  while (a <= num)
    _retlef.a = _retmid.a - wid / 2
    _retrig.a = _retmid.a + wid / 2
    a = a + 1
  endwhile
return
