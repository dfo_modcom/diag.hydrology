#=
 = For each SPINUP dir, move all restart files on Jan 1st of each year from the "run_" subdirs
 = to a common "fin" subdir and remove the remaining restart files in "run_" - RD Mar 2024.
 =#

using My, Printf

const PRNT             = false                          # print commands other than mv/rm

if (argc = length(ARGS)) != 0
  print("\nUsage: jjj $(basename(@__FILE__))\n\n")
  exit(1)
end

function walkspin(dira)
  dirb = abspath(dira) ; cd(dirb)
  filb = readdir(dirb)
  PRNT && print("cd $dirb\n")
  for fibb in filb
    if isdir(fibb) && startswith(fibb, "CALUN.")
      dirc = joinpath(dirb, fibb) ; cd(dirc)
      filc = readdir(dirc)
      PRNT && print("cd $dirc\n")

#     nogo = -1
#     for (cc, ficc) in enumerate(filc)
#       if isdir(ficc) && startswith(ficc, "run_")
#         nogo = cc
#       end
#     end
#     PRNT && print("avoiding restarts in $(filc[nogo])\n")

      for (cc, ficc) in enumerate(filc)
        if isdir(ficc) && startswith(ficc, "run_") # && cc != nogo
          dird = joinpath(dirc, ficc) ; cd(dird)
          fild = readdir(dird)
          PRNT && print("cd $dird\n")
          for fidd in fild
            if isfile(fidd) && startswith(fidd, "HYDRO_RST") && fidd[16:20] == "01-01"
              print("mv $dird/$fidd $dirc/fin\n")
            elseif isfile(fidd) && startswith(fidd, "RESTART") && fidd[13:16] == "0101"
              print("mv $dird/$fidd $dirc/fin\n")
            elseif isfile(fidd) && (startswith(fidd, "HYDRO_RST") || startswith(fidd, "RESTART"))
              print("rm $dird/$fidd\n")
            elseif isfile(fidd) && (endswith(fidd, "CHRTOUT_DOMAIN1") || endswith(fidd,  "GWOUT_DOMAIN1") || endswith(fidd, "LAKEOUT_DOMAIN1") ||
                                    endswith(fidd, "LDASOUT_DOMAIN1") || endswith(fidd, "LSMOUT_DOMAIN1") || endswith(fidd,   "RTOUT_DOMAIN1")) && fidd[5:8] != "0101"
              print("rm $dird/$fidd\n")
            end
          end
          PRNT && print("cd $dirc\n")
          cd(dirc)
        end
      end
      PRNT && print("cd $dirb\n")
      cd(dirb)
    end
  end
end

walkspin(".")
#print("\n")
exit(0)
