#=
 = Convert daily HyDAT and three/six-hourly WRF-Hydro streamflow timeseries data to daily,
 = monthly, and annual-average (daily) means for each river outlet that pours into the ocean.
 = The same number of HyDAT and WRF-Hydro values are averaged (i.e., both are valid).  For
 = longer than daily, HyDAT indices are simply averaged (good=1, poor=[-1,-5]).  Optional
 = masking by eve/odd is applied to odd/eve years, respectively - RD Jan 2024.
 =#

using My, Printf, NetCDF

const WRFR             = 1                              # streamflow forced by WRF-Hydro
const WRFN             = 2                              # streamflow forced by WRF-Hydro neural network post-processing
const ERAR             = 3                              # streamflow forced by ERA-5
const ERAN             = 4                              # streamflow forced by ERA-5     neural network post-processing
const DNUM             = 4                              # number of variable types
const DELT             = 3                              # source data timestep (hours)
const DELD             = 24                             # daily       timestep (hours)
const MISS             = -9999.0                        # generic missing value

if (argc = length(ARGS)) != 1 && (argc = length(ARGS)) != 2
  print("\nUsage: jjj $(basename(@__FILE__)) hydat_valding_aall_1000_incl_1990-01-01_2004-12-30.neural.nc [eve/odd]\n\n")
  exit(1)
end
tail = argc == 1 ? ARGS[1][end-5:end-3] : ARGS[2]
fila = ARGS[1]
filb = ARGS[1][1:end-6] * tail * ".daily.nc"
filc = ARGS[1][1:end-6] * tail * ".monthly.nc"
fild = ARGS[1][1:end-6] * tail * ".annualday.nc"
file = ARGS[1][1:end-3] *        ".txt"
filf = ARGS[1][1:end-6] * tail * ".txt"

hors = ncread(  fila, "time", start=[1], count=[-1])                          # read the DELT-h times
tatt = ncgetatt(fila, "time", "units")
nhor = length(hors)

msky = trues(nhor)                                                            # set masking of eve/odd years
if argc == 2
  for a = 1:nhor
    year = parse(Int64, dateref(hors[a], tatt)[1:4])
    ARGS[2] == "eve" && iseven(year) && (msky[a] = false)
    ARGS[2] == "odd" &&  isodd(year) && (msky[a] = false)
  end
  @printf("\ncopying %s %s\n", file, filf) ; cp(file, filf; force = true, follow_symlinks = true)
end

tmpa = dateref(hors[  1], tatt)                                               # and daily/DELT-h streamflow
tmpb = dateref(hors[end], tatt)
hora = tmpa[1:4] * "-" * tmpa[5:6] * "-" * tmpa[7:8] * "-" * tmpa[9:10]
horb = tmpb[1:4] * "-" * tmpb[5:6] * "-" * tmpb[7:8] * "-" * tmpb[9:10]
hydt = ncread(fila, "hydt", start=[1,1],   count=[-1,-1])
hydi = ncread(fila, "hydi", start=[1,1],   count=[-1,-1])
dath = ncread(fila, "flow", start=[1,1,1], count=[-1,-1,-1])
nlet = size(dath, 2)

cpyt = fill(MISS, size(hydt))                                                 # duplicate daily HyDAT at
cpyi = fill(MISS, size(hydi))                                                 # 3-h intervals, then match
for a = 1:nhor                                                                # missing HyDAT and WRF-Hydro
  for b = 1:nlet
    if hydt[b,a] > MISS
      for c = -4:3
        if 1 <= a+c <= nhor
          cpyt[b,a+c] = hydt[b,a]
          cpyi[b,a+c] = hydi[b,a]
        end
      end
    end
  end
end
hydt = cpyt
hydi = cpyi
for a = 1:nhor
  for b = 1:nlet
    in(MISS, dath[:,b,a]) && (hydt[  b,a]  = MISS)
    hydt[b,a] == MISS     && (dath[:,b,a] .= MISS)
  end
end

daya = tmpa[1:4] * "-" * tmpa[5:6] * "-" * tmpa[7:8] * "-00"                  # then construct daily dates
dayb = tmpb[1:4] * "-" * tmpb[5:6] * "-" * tmpb[7:8] * "-00"
tima = My.datesous("1900-01-01-00", daya, "hr")
timb = My.datesous("1900-01-01-00", dayb, "hr")
days = collect(tima:DELD:timb) ; nday = length(days)
dytd = zeros(      nlet, nday) ; nytd = zeros(      nlet, nday)
dyid = zeros(      nlet, nday) ; nyid = zeros(      nlet, nday)
datd = zeros(DNUM, nlet, nday) ; natd = zeros(DNUM, nlet, nday)

mons = Array{Float64}(undef,0)                                                # monthly dates and
mona = tmpa[1:4] * "-" * tmpa[5:6] * "-15-00"
monb = tmpb[1:4] * "-" * tmpb[5:6] * "-15-00"
tima = My.datesous("1900-01-01-00", mona, "hr")
timb = My.datesous("1900-01-01-00", monb, "hr")
while tima <= timb
  global tima, mona
  push!(mons, tima)
  mona = dateadd(mona, 30, "dy")
  mona = mona[1:7] * "-15-00"
  tima = My.datesous("1900-01-01-00", mona, "hr")
end
nmon = length(mons)
dytm = zeros(      nlet, nmon) ; nytm = zeros(      nlet, nmon)
dyim = zeros(      nlet, nmon) ; nyim = zeros(      nlet, nmon)
datm = zeros(DNUM, nlet, nmon) ; natm = zeros(DNUM, nlet, nmon)

anna = tmpa[1:4] * "-01-01-00"                                                # annual-average-daily dates
annb = tmpa[1:4] * "-12-31-00"
tima = My.datesous("1900-01-01-00", anna, "hr")
timb = My.datesous("1900-01-01-00", annb, "hr")
anns = collect(tima:DELD:timb) ; nann = length(anns)
dyta = zeros(      nlet, nann) ; nyta = zeros(      nlet, nann)
dyia = zeros(      nlet, nann) ; nyia = zeros(      nlet, nann)
data = zeros(DNUM, nlet, nann) ; nata = zeros(DNUM, nlet, nann)

function nccreer(fn::AbstractString, tims::Array{Float64,1}, lats::Array{Float64,1}, lons::Array{Float64,1})
  nctim = NcDim("time", length(tims), atts = Dict{Any,Any}("units"=>"hours since 1900-01-01 00:00:0.0"), values = tims)
  nclat = NcDim( "lat", length(lats), atts = Dict{Any,Any}("units"=>"degrees_north"),                    values = lats)
  nclon = NcDim( "lon", length(lons), atts = Dict{Any,Any}("units"=> "degrees_east"),                    values = lons)
  ncvrs = Array{NetCDF.NcVar}(undef, 16)
  ncvrs[ 1] = NcVar("hydt", [       nclat, nctim], atts = Dict{Any,Any}("units"=>"m3/s", "missing_value"=>MISS), t=Float64, compress=-1)
  ncvrs[ 2] = NcVar("hydi", [       nclat, nctim], atts = Dict{Any,Any}("units"=>"none", "missing_value"=>MISS), t=Float64, compress=-1)
  ncvrs[ 3] = NcVar("flow", [nclon, nclat, nctim], atts = Dict{Any,Any}("units"=>"m3/s", "missing_value"=>MISS), t=Float64, compress=-1)
  ncvrs[ 4] = NcVar("para", [nclon,        nctim], atts = Dict{Any,Any}("units"=>"none", "missing_value"=>MISS), t=Float64, compress=-1)
  ncvrs[ 5] = NcVar("parb", [nclon,        nctim], atts = Dict{Any,Any}("units"=>"none", "missing_value"=>MISS), t=Float64, compress=-1)
  ncvrs[ 6] = NcVar("parc", [nclon,        nctim], atts = Dict{Any,Any}("units"=>"none", "missing_value"=>MISS), t=Float64, compress=-1)
  ncvrs[ 7] = NcVar("tmpa", [nclon, nclat       ], atts = Dict{Any,Any}("units"=>"none", "missing_value"=>MISS), t=Float64, compress=-1)
  ncvrs[ 8] = NcVar("tmpb", [nclon, nclat       ], atts = Dict{Any,Any}("units"=>"none", "missing_value"=>MISS), t=Float64, compress=-1)
  ncvrs[ 9] = NcVar("tmpc", [nclon, nclat       ], atts = Dict{Any,Any}("units"=>"none", "missing_value"=>MISS), t=Float64, compress=-1)
  ncvrs[10] = NcVar("tmpd", [nclon, nclat       ], atts = Dict{Any,Any}("units"=>"none", "missing_value"=>MISS), t=Float64, compress=-1)
  ncvrs[11] = NcVar("tmpe", [nclon, nclat       ], atts = Dict{Any,Any}("units"=>"none", "missing_value"=>MISS), t=Float64, compress=-1)
  ncvrs[12] = NcVar("tmpf", [nclon, nclat       ], atts = Dict{Any,Any}("units"=>"none", "missing_value"=>MISS), t=Float64, compress=-1)
  ncvrs[13] = NcVar("tmpg", [nclon, nclat       ], atts = Dict{Any,Any}("units"=>"none", "missing_value"=>MISS), t=Float64, compress=-1)
  ncvrs[14] = NcVar("tmph", [nclon, nclat       ], atts = Dict{Any,Any}("units"=>"none", "missing_value"=>MISS), t=Float64, compress=-1)
  ncvrs[15] = NcVar("tmpi", [nclon, nclat       ], atts = Dict{Any,Any}("units"=>"none", "missing_value"=>MISS), t=Float64, compress=-1)
  ncvrs[16] = NcVar("tmpj", [nclon, nclat       ], atts = Dict{Any,Any}("units"=>"none", "missing_value"=>MISS), t=Float64, compress=-1)
  ncfil = NetCDF.create(fn, ncvrs, gatts = Dict{Any,Any}("units"=>"none"), mode = NC_NETCDF4)
end

lats = collect(1.0:nlet)                                                      # create the NetCDF templates
lons = collect(1.0:DNUM)
nccreer(filb, days, lats, lons)
nccreer(filc, mons, lats, lons)
nccreer(fild, anns, lats, lons)

hnow = hora                                                                   # then construct each timeseries
for a = 1:nhor                                                                # (but skip eve/odd years/Feb 29)
  global hnow
  if msky[a] && hnow[5:10] != "-02-29"
    dnow = My.datesous("1900-01-01-00",             hnow[1:10] *    "-00", "hr") ; dind = findfirst(isequal(dnow), days)
    mnow = My.datesous("1900-01-01-00",             hnow[1: 7] * "-15-00", "hr") ; mind = findfirst(isequal(mnow), mons)
    anow = My.datesous("1900-01-01-00", hora[1:4] * hnow[5:10] *    "-00", "hr") ; aind = findfirst(isequal(anow), anns)
    for b = 1:nlet
      if hydt[b,a] > MISS
        dytd[b,dind] += hydt[b,a] ; nytd[b,dind] += 1 ; dyid[b,dind] += hydi[b,a] ; nyid[b,dind] += 1
        dytm[b,mind] += hydt[b,a] ; nytm[b,mind] += 1 ; dyim[b,mind] += hydi[b,a] ; nyim[b,mind] += 1
        dyta[b,aind] += hydt[b,a] ; nyta[b,aind] += 1 ; dyia[b,aind] += hydi[b,a] ; nyia[b,aind] += 1
        for c = 1:DNUM
          datd[c,b,dind] += dath[c,b,a] ; natd[c,b,dind] += 1
          datm[c,b,mind] += dath[c,b,a] ; natm[c,b,mind] += 1
          data[c,b,aind] += dath[c,b,a] ; nata[c,b,aind] += 1
        end
      end
    end
  end
  hnow = My.dateadd(hnow, DELT, "hr")
end

for z = 1:3
  z == 1 && (znam = filb[1:end-3] * ".txt" ; zzss = days ; zznn = nday ; dytz = dytd ; nytz = nytd ; dyiz = dyid ; nyiz = nyid ; datz = datd ; natz = natd)
  z == 2 && (znam = filc[1:end-3] * ".txt" ; zzss = mons ; zznn = nmon ; dytz = dytm ; nytz = nytm ; dyiz = dyim ; nyiz = nyim ; datz = datm ; natz = natm)
  z == 3 && (znam = fild[1:end-3] * ".txt" ; zzss = anns ; zznn = nann ; dytz = dyta ; nytz = nyta ; dyiz = dyia ; nyiz = nyia ; datz = data ; natz = nata)

  fpa = My.ouvre(znam, "w", false)                                            # and save averages to NetCDF and text
  for a = 1:zznn                                                              # for hydt, hydi, WRFR, WRFN, ERAR, ERAN
    line = @sprintf("%s", My.dateadd("1900-01-01-00", zzss[a], "hr"))
    for b = 1:nlet
      if   nytz[  b,a] == 0  dytz[  b,a] = MISS  else  dytz[  b,a] /= nytz[  b,a]  end
      if   nyiz[  b,a] == 0  dyiz[  b,a] = MISS  else  dyiz[  b,a] /= nyiz[  b,a]  end
      for c = 1:DNUM
        if natz[c,b,a] == 0  datz[c,b,a] = MISS  else  datz[c,b,a] /= natz[c,b,a]  end
      end
    end
    b = 1
    line *= @sprintf(" %15.8f %15.8f %15.8f %15.8f %15.8f %15.8f\n", dytz[b,a], dyiz[b,a], datz[WRFR,b,a], datz[WRFN,b,a], datz[ERAR,b,a], datz[ERAN,b,a])
    print(fpa, line)
  end
  close(fpa)

  znam = znam[1:end-4] * ".nc"
  ncwrite(dytz, znam, "hydt", start=[1,1],   count=[-1,-1])
  ncwrite(dyiz, znam, "hydi", start=[1,1],   count=[-1,-1])
  ncwrite(datz, znam, "flow", start=[1,1,1], count=[-1,-1,-1])
end

print("\n")
exit(0)

#=
tima = My.datesous("1900-01-01-00", hora, "hr")
timb = My.datesous("1900-01-01-00", horb, "hr")
hors = collect(tima:DELT:timb) ; nhor = length(hors)

mask = falses(nlet,nhor)
for a = 1:nhor  for b = 1:nlet  hydt[b,a] > MISS && (mask[b,a] = true)  end  end
@show length(hydt[mask])
mask = falses(nlet,nhor)
for a = 1:nhor  for b = 1:nlet  dath[2,b,a] > MISS && (mask[b,a] = true)  end  end
@show length(hydt[mask])
=#
