#=
 = Construct sublists of stations that conform to requirements for "validation".
 = One list includes the HyDAT and WRFHydro comparisons of interest, where
 = neither upstream catchment area is greater than CUTR times the other
 = (upstream catchments are taken from the HYDROSHEDS and WRF-Hydro routing
 = grids, respectively).  The other list is the remainder - RD Jan 2024.
 =#

using My, Printf, NetCDF

const CUTR             = 1.2                            # minimum HydroSHEDS UPLAND_SKM value
const MISS             = -9999.0                        # generic missing value

if (argc = length(ARGS)) != 1
  print("\nUsage: jjj $(basename(@__FILE__)) hydat_valding_aall_1000.txt\n\n")
  exit(1)
end
fila = ARGS[1][1:end-4] * "_incl.txt"
filb = ARGS[1][1:end-4] * "_excl.com"

fpa = My.ouvre(ARGS[1], "r") ; lins = readlines(fpa) ; close(fpa)

forma = formb = ""                                                            # read the catchment areas
for line in lins
  global forma, formb
  file = split(line)[1] * ".sta"
  if isfile(file)
    fpb  = My.ouvre(file, "r") ; fins = readlines(fpb) ; close(fpb)
    hrea = parse(Float64, split(fins[30])[4])
    wrea = parse(Float64, split(fins[90])[5])

    if hrea / wrea < CUTR && wrea / hrea < CUTR
      forma *= line * "\n"
    else
      formb *= @sprintf("mv %s* limbo\n", file[1:end-4])
    end
  end
end

fpa = My.ouvre(fila, "w") ; write(fpa, forma) ; close(fpa)                    # and save the lists to text
fpb = My.ouvre(filb, "w") ; write(fpb, formb) ; close(fpb)
exit(0)
