#=
 = Loop through 3-h WRF-Hydro pour point data and convert to text and daily NetCDF
 = where the .sta text file contains a fixed number of entries (including initially
 = missing values for the nearest positions along each of the HydroSHEDS, SWOT/SWORD
 = and HyDAT river networks), followed by any number of extra text lines for SQL
 = remarks from the HyDAT database - RD Jan 2024.
 =#

using My, Printf, NetCDF, SQLite, DataFrames

const YBEG             = 1982                           # first year of daily timeseries
const YEND             = 2022                           #  last year of daily timeseries
const FLOW             = 1                              # daily flow rate (m3/s)
const LEVL             = 3                              # daily water level (m)
const SEDC             = 5                              # daily suspended sediment concentration (mg/l)
const LOAD             = 7                              # daily suspended sediment load (tonnes) calculated by SEDC * FLOW * const
const VNUM             = 8                              # number of variables
const DELT             = 24                             # timestep (hours)
const GOOD             = 1.0                            # code to represent a good measurement

const DELMISS          = -8e9                           # generic missing value comparison
const GRDMISS          = -9e9                           # generic missing value on a grid
const MISS             = -9999.0                        # generic missing value
const MIST             = @sprintf("%d", Int64(MISS))    # generic missing value string

if (argc = length(ARGS)) != 1
  print("\nUsage: jjj $(basename(@__FILE__)) run_DOMAIN_full_full_FORCING.wrf_1990-01-01_2004-12-30_3h_Severn_Hudson.txt\n\n")
  exit(1)
end
fila = ARGS[1] ; fpa = My.ouvre(fila, "r", false) ; lins = readlines(fpa, keep = true) ; close(fpa)
@printf("\nfound %d WRF-Hydro pour-point stations in %s\n", length(lins), fila)

function stwrite(line::AbstractString)                                        # save WRF-Hydro data in text files
  temp = split(line) ; form = ""                                              # with temporary missing values for
  name =                temp[1]                                               # the nearest HyDAT and SWOT rivers
  loni = parse(  Int64, temp[2])                                              # (both location and grid indices)
  lati = parse(  Int64, temp[3])
  latf = parse(Float64, temp[5])
  lonf = parse(Float64, temp[6])
  filb = @sprintf("wrfh_%8s.sta", name)
  filb = replace(filb, ' ' => '0')
  form *= @sprintf("%-24s %s [%s]\n",           "STATION_NAME", "TBD", name) 
  form *= @sprintf("%-24s %s\n",         "PROV_TERR_STATE_LOC", "TBD")
  form *= @sprintf("%-24s %s\n",          "REGIONAL_OFFICE_ID", "TBD")
  form *= @sprintf("%-24s %s\n",                  "HYD_STATUS", "TBD")
  form *= @sprintf("%-24s %s\n",                  "SED_STATUS", "-9999 [missing]")
  form *= @sprintf("%-24s %s\n",                    "LATITUDE", "TBD")
  form *= @sprintf("%-24s %s\n",                   "LONGITUDE", "TBD")
  form *= @sprintf("%-24s %s\n",         "DRAINAGE_AREA_GROSS", "TBD")
  form *= @sprintf("%-24s %s\n",        "DRAINAGE_AREA_EFFECT", "TBD")
  form *= @sprintf("%-24s %s\n",                        "RHBN", "TBD")
  form *= @sprintf("%-24s %s\n",                   "REAL_TIME", "TBD")
  form *= @sprintf("%-24s %s\n",              "CONTRIBUTOR_ID", "TBD")
  form *= @sprintf("%-24s %s\n",                 "OPERATOR_ID", "TBD")
  form *= @sprintf("%-24s %s\n",                    "DATUM_ID", "TBD")
  form *= @sprintf("%-24s %6d %6d %7.3f\n", "$YBEG-$YEND.DLY_FLOWS",      Int64(MISS), Int64(MISS), MISS)
  form *= @sprintf("%-24s %6d %6d %7.3f\n", "$YBEG-$YEND.DLY_LEVELS",     Int64(MISS), Int64(MISS), MISS)
  form *= @sprintf("%-24s %6d %6d %7.3f\n", "$YBEG-$YEND.SED_DLY_SUSCON", Int64(MISS), Int64(MISS), MISS)
  form *= @sprintf("%-24s %6d %6d %7.3f\n", "$YBEG-$YEND.SED_DLY_LOADS",  Int64(MISS), Int64(MISS), MISS)
  form *= @sprintf("%-24s %s as of %s\n",      "HYDAT_VERSION", "1.0", "2022-10-24 22:42:23.000")
  form *= @sprintf(" ------------- HydroSHEDS ------------ \n")
  form *= @sprintf("%-24s %f %d\n",                 "LATITUDE",                 MISS,  Int64(MISS))
  form *= @sprintf("%-24s %f %d\n",                "LONGITUDE",                 MISS,  Int64(MISS))
  form *= @sprintf("%-24s %d\n",                    "HYRIV_ID",                        Int64(MISS))
  form *= @sprintf("%-24s %d\n",                   "NEXT_DOWN",                        Int64(MISS))
  form *= @sprintf("%-24s %d\n",                    "MAIN_RIV",                        Int64(MISS))
  form *= @sprintf("%-24s %d\n",                   "LENGTH_KM",                        Int64(MISS))
  form *= @sprintf("%-24s %d\n",                  "DIST_DN_KM",                        Int64(MISS))
  form *= @sprintf("%-24s %d\n",                  "DIST_UP_KM",                        Int64(MISS))
  form *= @sprintf("%-24s %d\n",                   "CATCH_SKM",                        Int64(MISS))
  form *= @sprintf("%-24s %d\n",                  "UPLAND_SKM",                        Int64(MISS))
  form *= @sprintf("%-24s %d\n",                   "ENDORHEIC",                        Int64(MISS))
  form *= @sprintf("%-24s %d\n",                  "DIS_AV_CMS",                        Int64(MISS))
  form *= @sprintf("%-24s %d\n",                    "ORD_STRA",                        Int64(MISS))
  form *= @sprintf("%-24s %d\n",                    "ORD_CLAS",                        Int64(MISS))
  form *= @sprintf("%-24s %d\n",                    "ORD_FLOW",                        Int64(MISS))
  form *= @sprintf("%-24s %d\n",                   "HYBAS_L12",                        Int64(MISS))
  form *= @sprintf("%-24s %s as of %s\n", "HydroSHEDS_VERSION", "1.0", "2019-10-01")
  form *= @sprintf(" --------------- SWOT ---------------- \n")
  form *= @sprintf("%-24s %f %d\n",                 "node_lat",                 MISS,  Int64(MISS))
  form *= @sprintf("%-24s %f %d\n",                 "node_lon",                 MISS,  Int64(MISS))
  form *= @sprintf("%-24s %d\n",                     "node_id",                        Int64(MISS))
  form *= @sprintf("%-24s %d\n",                 "node_length",                        Int64(MISS))
  form *= @sprintf("%-24s %d\n",                    "reach_id",                        Int64(MISS))
  form *= @sprintf("%-24s %d\n",                         "wse",                        Int64(MISS))
  form *= @sprintf("%-24s %d\n",                     "wse_var",                        Int64(MISS))
  form *= @sprintf("%-24s %d\n",                       "width",                        Int64(MISS))
  form *= @sprintf("%-24s %d\n",                   "width_var",                        Int64(MISS))
  form *= @sprintf("%-24s %d\n",                  "n_chan_max",                        Int64(MISS))
  form *= @sprintf("%-24s %d\n",                  "n_chan_mod",                        Int64(MISS))
  form *= @sprintf("%-24s %d\n",                  "obstr_type",                        Int64(MISS))
  form *= @sprintf("%-24s %d\n",                     "grod_id",                        Int64(MISS))
  form *= @sprintf("%-24s %d\n",                   "hfalls_id",                        Int64(MISS))
  form *= @sprintf("%-24s %d\n",                    "dist_out",                        Int64(MISS))
  form *= @sprintf("%-24s %d\n",                        "type",                        Int64(MISS))
  form *= @sprintf("%-24s %d\n",                        "facc",                        Int64(MISS))
  form *= @sprintf("%-24s %d\n",                    "lakeflag",                        Int64(MISS))
  form *= @sprintf("%-24s %d\n",                   "max_width",                        Int64(MISS))
  form *= @sprintf("%-24s %d\n",                  "river_name",                        Int64(MISS))
  form *= @sprintf("%-24s %d\n",                   "sinuosity",                        Int64(MISS))
  form *= @sprintf("%-24s %d\n",                   "meand_len",                        Int64(MISS))
  form *= @sprintf("%-24s %d\n",                  "manual_add",                        Int64(MISS))
  form *= @sprintf("%-24s %d\n",                   "reach_lat",                        Int64(MISS))
  form *= @sprintf("%-24s %d\n",                   "reach_lon",                        Int64(MISS))
  form *= @sprintf("%-24s %d\n",                    "reach_id",                        Int64(MISS))
  form *= @sprintf("%-24s %d\n",                         "wse",                        Int64(MISS))
  form *= @sprintf("%-24s %d\n",                     "wse_var",                        Int64(MISS))
  form *= @sprintf("%-24s %d\n",                       "width",                        Int64(MISS))
  form *= @sprintf("%-24s %d\n",                   "width_var",                        Int64(MISS))
  form *= @sprintf("%-24s %d\n",                     "n_nodes",                        Int64(MISS))
  form *= @sprintf("%-24s %d\n",                  "n_chan_max",                        Int64(MISS))
  form *= @sprintf("%-24s %d\n",                  "n_chan_mod",                        Int64(MISS))
  form *= @sprintf("%-24s %d\n",                  "obstr_type",                        Int64(MISS))
  form *= @sprintf("%-24s %d\n",                     "grod_id",                        Int64(MISS))
  form *= @sprintf("%-24s %d\n",                   "hfalls_id",                        Int64(MISS))
  form *= @sprintf("%-24s %d\n",                       "slope",                        Int64(MISS))
  form *= @sprintf("%-24s %d\n",                    "dist_out",                        Int64(MISS))
  form *= @sprintf("%-24s %d\n",                    "n_rch_up",                        Int64(MISS))
  form *= @sprintf("%-24s %d\n",                  "n_rch_down",                        Int64(MISS))
  form *= @sprintf("%-24s %d\n",                   "rch_id_up",                        Int64(MISS))
  form *= @sprintf("%-24s %d\n",                   "rch_id_dn",                        Int64(MISS))
  form *= @sprintf("%-24s %d\n",                    "lakeflag",                        Int64(MISS))
  form *= @sprintf("%-24s %d\n",                   "max_width",                        Int64(MISS))
  form *= @sprintf("%-24s %d\n",                        "type",                        Int64(MISS))
  form *= @sprintf("%-24s %d\n",                        "facc",                        Int64(MISS))
  form *= @sprintf("%-24s %d\n",                    "swot_obs",                        Int64(MISS))
  form *= @sprintf("%-24s %d\n",                 "swot_orbits",                        Int64(MISS))
  form *= @sprintf("%-24s %d\n",                  "river_name",                        Int64(MISS))
  form *= @sprintf("%-24s %s as of %s\n",      "SWORD_VERSION", "1.0", "2022-10-24 22:42:23.000")
  form *= @sprintf(" ------------- WRF-Hydro ------------- \n")
  form *= @sprintf("%-24s %f %d\n",                 "LATITUDE", latf, lati)
  form *= @sprintf("%-24s %f %d\n",                "LONGITUDE", lonf, loni)
  form *= @sprintf(" -------------- remarks -------------- \n")
  fpb   = My.ouvre(filb, "w") ; write(fpb, form) ; close(fpb)
end

tnam = "wrfh_zzzzzzzz.nc"                                                     # construct a netcdf template
vars = ["flow", "floi", "levl", "levi", "sedc", "sedi", "load", "loai"]       # for a fixed time interval
daya = @sprintf("%s-01-01-12", YBEG) ; tima = My.datesous("1900-01-01-00", daya, "hr")
dayb = @sprintf("%s-12-31-12", YEND) ; timb = My.datesous("1900-01-01-00", dayb, "hr")
days = collect(tima:DELT:timb) ; nday = length(days) ; data = fill(MISS, nday)
My.nccreer(   tnam, nday, 1, 1, MISS; vnames = vars)
ncputatt(     tnam, "time", Dict("units" => "hours since 1900-01-01 00:00:0.0"))
ncwrite(days, tnam, "time", start=[1], count=[-1])
for vnam in vars
  ncwrite(data, tnam, vnam, start=[1,1,1], count=[-1,-1,-1])
end

function dywrite(line::AbstractString)                                        # and prepare NetCDF for HyDAT data
  name = split(line)[1]
  filb = @sprintf("wrfh_%8s.sta.nc", name)
  filb = replace(filb, ' ' => '0')
  print("cp $tnam $filb\n") ; cp(tnam, filb; force = true)
end

for a = 1:length(lins)
  stwrite(lins[a])
  dywrite(lins[a])
end
exit(0)

#=
function obstype(code::AbstractString)                                        # assume that each measurement
  cval = GOOD                                                                 # is a good value by default
  code == "A" && (cval = -1.0)                                                # or partial day otherwise,
  code == "B" && (cval = -2.0)                                                # or ice conditions,
  code == "D" && (cval = -3.0)                                                # or dry,
  code == "E" && (cval = -4.0)                                                # or estimated,
  code == "S" && (cval = -5.0)                                                # or sample(s) taken
  cval
end
function obstype(code::Missing)  GOOD  end

function dywrite(ind::Int64)                                                  # and save each daily timeseries
  stnam = "hydat_" * stdat[ind,1] * ".nc"
  print("cp $tnam $stnam\n") ; cp(tnam, stnam; force = true)
  numb = zeros(VNUM)

  stvar = "DLY_FLOWS"  ; indv = FLOW ; indd = 12 ; indn = 2
  vara  = vars[indv  ] ; data = fill(MISS, nday)
  vari  = vars[indv+1] ; dati = fill(MISS, nday)
  daily = DBInterface.execute(stall, "SELECT * FROM $stvar WHERE STATION_NUMBER='$(stdat[ind,1])'") |> DataFrame ; raily, caily = size(daily)
  for a = 1:raily
    if YBEG <=  daily[a,2] <= YEND
      ynow = "$(daily[a,2])"
      mnow = "$(daily[a,3])" ; mnow = (length(mnow) == 1 ? "0" : "") * mnow
      dnow = "$ynow-$mnow-01-12" ; inow = indd ; mref = mnow
      while mref == mnow
        dref = My.datesous("1900-01-01-00", dnow, "hr")
        dind = findfirst(isequal(dref), days)
        if daily[a,inow] !== missing
          data[dind]  =         daily[a,inow]
          dati[dind]  = obstype(daily[a,inow+1])
          numb[indv] += 1
        end
#       line = @sprintf("%s flow is %f [%f]\n", dnow, data[dind], dati[dind]) ; print(line)
        dnow = My.dateadd(dnow, 1, "dy")
        mnow = dnow[6:7] ; inow += indn
      end
    end
  end
  ncwrite(data, stnam, vara, start=[1,1,1], count=[-1,-1,-1])
  ncwrite(dati, stnam, vari, start=[1,1,1], count=[-1,-1,-1])

  stvar = "DLY_LEVELS"  ; indv = LEVL ; indd = 13 ; indn = 2
  vara  = vars[indv  ] ; data = fill(MISS, nday)
  vari  = vars[indv+1] ; dati = fill(MISS, nday)
  daily = DBInterface.execute(stall, "SELECT * FROM $stvar WHERE STATION_NUMBER='$(stdat[ind,1])'") |> DataFrame ; raily, caily = size(daily)
  for a = 1:raily
    if YBEG <=  daily[a,2] <= YEND
      ynow = "$(daily[a,2])"
      mnow = "$(daily[a,3])" ; mnow = (length(mnow) == 1 ? "0" : "") * mnow
      dnow = "$ynow-$mnow-01-12" ; inow = indd ; mref = mnow
      while mref == mnow
        dref = My.datesous("1900-01-01-00", dnow, "hr") 
        dind = findfirst(isequal(dref), days)
        if daily[a,inow] !== missing
          data[dind]  =         daily[a,inow]
          dati[dind]  = obstype(daily[a,inow+1])
          numb[indv] += 1
        end
        dnow = My.dateadd(dnow, 1, "dy")
        mnow = dnow[6:7] ; inow += indn
      end
    end
  end
  ncwrite(data, stnam, vara, start=[1,1,1], count=[-1,-1,-1])
  ncwrite(dati, stnam, vari, start=[1,1,1], count=[-1,-1,-1])

  stvar = "SED_DLY_SUSCON"  ; indv = SEDC ; indd = 11 ; indn = 2
  vara  = vars[indv  ] ; data = fill(MISS, nday)
  vari  = vars[indv+1] ; dati = fill(MISS, nday)
  daily = DBInterface.execute(stall, "SELECT * FROM $stvar WHERE STATION_NUMBER='$(stdat[ind,1])'") |> DataFrame ; raily, caily = size(daily)
  for a = 1:raily
    if YBEG <=  daily[a,2] <= YEND
      ynow = "$(daily[a,2])"
      mnow = "$(daily[a,3])" ; mnow = (length(mnow) == 1 ? "0" : "") * mnow
      dnow = "$ynow-$mnow-01-12" ; inow = indd ; mref = mnow
      while mref == mnow
        dref = My.datesous("1900-01-01-00", dnow, "hr") 
        dind = findfirst(isequal(dref), days)
        if daily[a,inow] !== missing
          data[dind]  =         daily[a,inow]
          dati[dind]  = obstype(daily[a,inow+1])
          numb[indv] += 1
        end
        dnow = My.dateadd(dnow, 1, "dy")
        mnow = dnow[6:7] ; inow += indn
      end
    end
  end
  ncwrite(data, stnam, vara, start=[1,1,1], count=[-1,-1,-1])
  ncwrite(dati, stnam, vari, start=[1,1,1], count=[-1,-1,-1])

  stvar = "SED_DLY_LOADS"  ; indv = LOAD ; indd = 12 ; indn = 1
  vara  = vars[indv  ] ; data = fill(MISS, nday)
  vari  = vars[indv+1] ; dati = fill(MISS, nday)
  daily = DBInterface.execute(stall, "SELECT * FROM $stvar WHERE STATION_NUMBER='$(stdat[ind,1])'") |> DataFrame ; raily, caily = size(daily)
  for a = 1:raily
    if YBEG <=  daily[a,2] <= YEND
      ynow = "$(daily[a,2])"
      mnow = "$(daily[a,3])" ; mnow = (length(mnow) == 1 ? "0" : "") * mnow
      dnow = "$ynow-$mnow-01-12" ; inow = indd ; mref = mnow
      while mref == mnow
        dref = My.datesous("1900-01-01-00", dnow, "hr")
        dind = findfirst(isequal(dref), days)
        if daily[a,inow] !== missing
          data[dind]  = daily[a,inow]
          dati[dind]  = 1.0
          numb[indv] += 1
        end
        dnow = My.dateadd(dnow, 1, "dy")
        mnow = dnow[6:7] ; inow += indn
      end
    end
  end
  ncwrite(data, stnam, vara, start=[1,1,1], count=[-1,-1,-1])
  ncwrite(dati, stnam, vari, start=[1,1,1], count=[-1,-1,-1])

  stfil = "hydat_" * stdat[ind,1] * ".sta" ; fpa = My.ouvre(stfil, "r")       # finally insert time coverage
  sttmp = "hydat_" * stdat[ind,1] * ".tmp" ; fpb = My.ouvre(sttmp, "w")       # for each netcdf variable in
  lines = readlines(fpa; keep = true)      ;   c = length(lines)              # the corresponding text file
  for a = 1:14  form = lines[a] ; write(fpb, form)  end
  form = @sprintf("%-24s %6d %6d %7.3f\n", "$YBEG-$YEND.DLY_FLOWS",      numb[FLOW], nday, 100 * numb[FLOW] / nday) ; write(fpb, form)
  form = @sprintf("%-24s %6d %6d %7.3f\n", "$YBEG-$YEND.DLY_LEVELS",     numb[LEVL], nday, 100 * numb[LEVL] / nday) ; write(fpb, form)
  form = @sprintf("%-24s %6d %6d %7.3f\n", "$YBEG-$YEND.SED_DLY_SUSCON", numb[SEDC], nday, 100 * numb[SEDC] / nday) ; write(fpb, form)
  form = @sprintf("%-24s %6d %6d %7.3f\n", "$YBEG-$YEND.SED_DLY_LOADS",  numb[LOAD], nday, 100 * numb[LOAD] / nday) ; write(fpb, form)
  for a = 19:c  form = lines[a] ; write(fpb, form)  end
  close(fpa) ; close(fpb) ; mv(sttmp, stfil; force=true)
end
=#
