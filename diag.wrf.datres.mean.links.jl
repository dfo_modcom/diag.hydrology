#=
 = Link model runs to the dat (output data) and res (restart) subdirectories.
 = Arguments given are the additional subdirs from which time-continuous output
 = is expected.  Gaps in the subdir output are reported, whereas any overlap is
 = handled by ranking the run_subdirs that are given later on the command line
 = as having "better" data.  For example, when linking from dat or res to
 = run_subdirb, if any existing files in dat or res have the same name, then
 = those files are moved to a subdir of dat or res (called z.copy, whereas any
 = existing links to run_subdira are simply discarded) and links to run_subdirb
 = are created instead - RD March 2021.
 =#

const YREF             = 1901                           # first year of output
const YEAS             = 3                              # number of continuous years
const DAYS             = 365                            # number of days in each year

using My, Printf

if (argc = length(ARGS)) == 0
  print("\nUsage: jjj $(basename(@__FILE__)) timestep_hr [run_subdira [run_subdirb ... [run_subdirn]]]\n\n")
  exit(1)
end
delh = parse(Int64, ARGS[1])                                                  # first parse the arguments and
subd = Array{AbstractString}(undef, argc-1)                                   # create the dat and res z.copy dirs
for a = 1:argc-1                                                              # (for lower rank files, if needed)
  subd[a]  = ARGS[a+1]
  subd[a] *= subd[a][end] == '/' ? "" : "/"
end
copd = "dat/z.copy" ; argc > 1 && !isdir(copd) && mkdir(copd)
copr = "res/z.copy" ; argc > 1 && !isdir(copr) && mkdir(copr)

date = @sprintf("%4d010100",  YREF)
stps = div(DAYS * 24, delh) * YEAS
srcd = [              "dat/",             "dat/",             "dat/",               "dat/",             "dat/",             "dat/",        "res/",     "res/"]
stem = [                  "",                 "",                 "",                   "",                 "",                 "",  "HYDRO_RST.", "RESTART."]
tail = ["00.CHRTOUT_DOMAIN1", "00.CHRTOUT_GRID1", "00.GWOUT_DOMAIN1", "00.LDASOUT_DOMAIN1", "00.LSMOUT_DOMAIN", "00.RTOUT_DOMAIN1", ":00_DOMAIN1", "_DOMAIN1"]
tnum = length(tail)

for a = 1:stps                                                                # loop over all required dates and
  global date                                                                 # identify complete file sets already
  zutc = date[end-1:end] == "00" ? true : false                               # in dat and res (allowing that the
  fils = Array{AbstractString}(undef, tnum)                                   # restarts are only at 00 UTC)
  gota =                       falses(tnum)
  for b = 1:tnum
    b != tnum - 1 && (fils[b] = stem[b] * date                                                             * tail[b])
    b == tnum - 1 && (fils[b] = stem[b] * date[1:4] * '-' * date[5:6] * '-' * date[7:8] * '_' * date[9:10] * tail[b])
    isfile(srcd[b] * fils[b]) && (gota[b] = true)
  end
  !zutc && (gota[end-1] = gota[end] = true)
  gotaa = true ; any(x -> x == false, gota) && (gotaa = false)

  for b = argc-1:-1:1                                                         # loop backwards over any run_subdirs
    gotb = falses(tnum)                                                       # and identify complete file sets that
    for c = 1:tnum                                                            # are "better"
      isfile(subd[b] * fils[c]) && (gotb[c] = true)
    end
    !zutc && (gotb[end-1] = gotb[end] = true)
    gotbb = true ; any(x -> x == false, gotb) && (gotbb = false)

    if gotbb                                                                  # and if such a run_subdir set exists
      for c = 1:tnum                                                          # then move any files from dat and res
        fila = srcd[c] *             fils[c]                                  # to their z.copy subdirs and create
        filb = srcd[c] * "z.copy/" * fils[c]                                  # links to the desired run_subdir set
        filc = subd[b] *             fils[c]
        islink(fila) && rm(fila)
        islink(filb) && rm(filb)
        isfile(fila) && isfile(filb) && error("$filb already exists, so avoid moving $fila\n\n")
        isfile(fila) && mv(fila, filb)
        isfile(filc) && symlink("../" * filc, fila)
#       islink(fila) && print("rm($fila)\n")
#       islink(filb) && print("rm($filb)\n")
#       isfile(fila) && isfile(filb) && print("ERROR: $filb already exists, so avoid moving $fila\n")
#       isfile(fila) && print("mv($fila, $filb)\n")
#       isfile(filc) && print("symlink(../$filc, $fila)\n")
      end
      gotaa = true
      b = 2
    end
  end

  if !gotaa                                                                   # finally, report any timeseries gaps
    print("$date is missing")
    for b = 1:tnum  !gota[b] && print("  $(tail[b])")  end
    print("\n")
  end
  date = dateadd(date, delh, "hr")
end
exit(0)

#=
dat/190208301800.CHRTOUT_DOMAIN1
dat/190208301800.CHRTOUT_GRID1
dat/190208301800.GWOUT_DOMAIN1
dat/190208301800.LDASOUT_DOMAIN1
dat/190208301800.LSMOUT_DOMAIN
dat/190208301800.RTOUT_DOMAIN1
res/HYDRO_RST.1902-08-30_18:00_DOMAIN1
res/RESTART.1902083018_DOMAIN1
=#
