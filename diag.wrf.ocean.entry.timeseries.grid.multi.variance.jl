#=
 = Estimate streamflow variance over four WRF-Hydro forcing runs for daily,
 = monthly, annual, decadal, and annual-average-daily timeseries - RD Apr 2024.
 =#

using My, Printf, NetCDF, Statistics, StatsBase

const RAWW             = 1                              # raw variable
const VARR             = 2                              # and its variance
const TOTL             = 3                              # and valid data count
const DNUM             = 3                              # number of variable types
const DELT             = 3                              # source data timestep (hours)
const DELD             = 24                             # daily       timestep (hours)

const DELMISS          = -8e9                           # generic missing value comparison
const GRDMISS          = -9e9                           # generic missing value on a grid
const MISS             = -9999.0                        # generic missing value
const MISSPOS          = 10^10                          # generic missing value positive

if (argc = length(ARGS)) != 1
  print("\nUsage: jjj $(basename(@__FILE__)) .neural.sorall.identity.nc\n\n")
  exit(1)
end
print("\n")

stema = "SPINUP.ccs/run_DOMAIN_full_full_FORCING.ccs_1990-01-01_2099-12-30_3h_Severn_Hudson"
stemb = "SPINUP.h45/run_DOMAIN_full_full_FORCING.h45_1990-01-01_2099-12-30_3h_Severn_Hudson"
stemc = "SPINUP.h85/run_DOMAIN_full_full_FORCING.h85_1990-01-01_2099-12-30_3h_Severn_Hudson"
stemd = "SPINUP.wrf/run_DOMAIN_full_full_FORCING.w85_1990-01-01_2099-12-30_3h_Severn_Hudson"

for a = 1:5                                                                   # get variance for each timeseries
  a == 1 && (tail = ARGS[1][1:end-3] *   ".daily.latsum.nc")
  a == 2 && (tail = ARGS[1][1:end-3] * ".monthly.latsum.nc")
  a == 3 && (tail = ARGS[1][1:end-3] *  ".annual.latsum.nc")
  a == 4 && (tail = ARGS[1][1:end-3] * ".decadal.latsum.nc")
  a == 5 && (tail = ARGS[1][1:end-3] *  ".annday.latsum.nc")
  a == 6 && (tail = ARGS[1][1:end-3] *  ".decday.latsum.nc")

  data = ncread(stema * tail, "riverflo", start=[1,1,1], count=[-1,-1,-1])
  datb = ncread(stemb * tail, "riverflo", start=[1,1,1], count=[-1,-1,-1])
  datc = ncread(stemc * tail, "riverflo", start=[1,1,1], count=[-1,-1,-1])
  datd = ncread(stemd * tail, "riverflo", start=[1,1,1], count=[-1,-1,-1])

  ntim = size(data, 3)
  mska = falses(ntim)
  mskb = falses(ntim)
  mskc = falses(ntim)
  mskd = falses(ntim)
  mske = falses(ntim)
  for b = 1:ntim
    MISS < data[1,1,b] < MISSPOS && (mska[b] = true)
    MISS < datb[1,1,b] < MISSPOS && (mskb[b] = true)
    MISS < datc[1,1,b] < MISSPOS && (mskc[b] = true)
    MISS < datd[1,1,b] < MISSPOS && (mskd[b] = true)
    MISS < data[1,1,b] < MISSPOS && MISS < datb[1,1,b] < MISSPOS && MISS < datc[1,1,b] < MISSPOS && MISS < datd[1,1,b] < MISSPOS && (mske[b] = true)
  end
  if a == 1
    @printf("    CCSM-8.5 mean %15.3f and stdev %15.3f\n",   mean(data[1,1,mska]), var(data[1,1,mska])^0.5)
    @printf("  HadGEM-4.5 mean %15.3f and stdev %15.3f\n",   mean(datb[1,1,mskb]), var(datb[1,1,mskb])^0.5)
    @printf("  HadGEM-8.5 mean %15.3f and stdev %15.3f\n",   mean(datc[1,1,mskc]), var(datc[1,1,mskc])^0.5)
    @printf("     MPI-8.5 mean %15.3f and stdev %15.3f\n\n", mean(datd[1,1,mskd]), var(datd[1,1,mskd])^0.5)
  end

  @printf("reading CCSM/HadGEM/MPI %44s ", tail)
  devm = numb = 0
  for b = 1:ntim
    if mske[b]
      devm += var([data[1,1,b], datb[1,1,b], datc[1,1,b], datd[1,1,b]])^0.5
      numb += 1
    end
  end
  devm /= numb
  @printf("with %5d times and mean stdev over %5d values of %15.3f\n", ntim, numb, devm)
end

print("\n")
exit(0)
