#=
 = This program applies calibrations that were previously computed to all files
 = in the current dir - RD Dec 2023, Feb 2024.
 =#

using My, Printf, NetCDF

const MISS             = -9999.0                        # generic missing value
const GRDMIS           = 99999.0                        # generic missing value on file

if (argc = length(ARGS)) != 1
  print("\nUsage: jjj $(basename(@__FILE__)) ../FORCING/forcing_era_wrf_1990_2004.annual_cali.txt\n\n")
  exit(1)
end
print("\n")

mcal = [       "S2D",                                     "RAINRATE"]
vref = ["R2D", "S2D", "T2D", "Q2D", "U2D", "V2D", "PSFC", "RAINRATE", "SWDOWN", "LWDOWN"]

fpa = My.ouvre(ARGS[1], "r") ; lins = readlines(fpa) ; close(fpa)             # read calibration variables
vars = split(lins[1]) ; varn = length(vars)                                   # and values from text
nlin = div(length(lins)-1, varn)
fils = readdir(".")

linn = 2                                                                      # then for each variable, map
for a = 1:varn                                                                # calibrations to day-of-year
  global linn
  itoc = Dict{AbstractString, Float64}()
  cmax = MISS ; cmin = GRDMIS
  for b = 1:nlin
    vals = split(lins[linn])
    vals[2] != vars[a] && error("\nERROR : $(vals[2]) != $(vars[a]) on line $linn of $(ARGS[1])\n\n")
    mndy = vals[1][6:7] * vals[1][9:10]
    cali = parse(Float64, vals[3])
    cali > cmax && (cmax = cali)
    cali < cmin && (cmin = cali)
    itoc[mndy] = cali
    linn += 1
  end
  itoc["0229"] = itoc["0228"]

      vars[a] ==  "Q2D" && (cmax *= 10^3 ; cmin *= 10^3)
      vars[a] == "PSFC" && (cmax /= 100  ; cmin /= 100)
   in(vars[a], mcal) && @printf("multiplying %8s by values in the range [%.1f,%.1f]\n", vars[a], cmin, cmax)
  !in(vars[a], mcal) && @printf("  adding to %8s    values in the range [%.1f,%.1f]\n", vars[a], cmin, cmax)

  if vars[a] == "S2D"                                                         # and adjust for each day-of-year
    for file in fils
      if isfile(file) && endswith(file, ".LDASIN_DOMAIN1")
        print("multiplying $file U2D,V2D by $(itoc[file[5:8]])\n")
        uwnd = ncread(file,   "U2D", start=[1,1,1], count=[-1,-1,-1]) .* itoc[file[5:8]]
        vwnd = ncread(file,   "V2D", start=[1,1,1], count=[-1,-1,-1]) .* itoc[file[5:8]]
        ncwrite(uwnd, file,   "U2D", start=[1,1,1], count=[-1,-1,-1])
        ncwrite(vwnd, file,   "V2D", start=[1,1,1], count=[-1,-1,-1])
      end
    end
  elseif in(vars[a], mcal)
    for file in fils
      if isfile(file) && endswith(file, ".LDASIN_DOMAIN1")
        print("multiplying $file $(vars[a]) by $(itoc[file[5:8]])\n")
        data = ncread(file, vars[a], start=[1,1,1], count=[-1,-1,-1]) .* itoc[file[5:8]]
        ncwrite(data, file, vars[a], start=[1,1,1], count=[-1,-1,-1])
      end
    end
  else
    for file in fils
      if isfile(file) && endswith(file, ".LDASIN_DOMAIN1")
        print("     adding $file $(vars[a]) by $(itoc[file[5:8]])\n")
        data = ncread(file, vars[a], start=[1,1,1], count=[-1,-1,-1]) .+ itoc[file[5:8]]
        ncwrite(data, file, vars[a], start=[1,1,1], count=[-1,-1,-1])
      end
    end
  end
end

print("\n")
exit(0)
