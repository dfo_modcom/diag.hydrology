#=
 = Replace lake parameters in geogrid.nc with adjacent land values - RD Dec 2023.
 =#

using My, Printf, NetCDF

if (argc = length(ARGS)) != 1
  print("\nUsage: jjj $(basename(@__FILE__)) geogrid.nc\n")
  exit(1)                                                                     # define input and output files
end
fila = ARGS[1]
filb = ARGS[1][1:end-3] * "_laketoland.nc"

function laketoland()
  print("\ncopying $fila to $filb\n") ; cp(fila, filb; force=true)
  lats = ncread(filb,     "CLAT", start=[1,1,1], count=[-1,-1,-1])[:,:,1]
  lons = ncread(filb,    "CLONG", start=[1,1,1], count=[-1,-1,-1])[:,:,1]
  land = ncread(filb, "LANDMASK", start=[1,1,1], count=[-1,-1,-1])[:,:,1]
  lind = ncread(filb, "LU_INDEX", start=[1,1,1], count=[-1,-1,-1])[:,:,1]
  (nlon, nlat) = size(lind)

  flak = Set{Tuple{Int64, Int64, Int64, Int64}}()                             # find the land points that are
  for a = 1:nlat, b = 1:nlon                                                  # closest to each lake point
    if lind[b,a] == 21
      mdis = 9e9 ; mlat = mlon = 0
      for c = 1:nlat, d = 1:nlon
        if land[d,c] == 1
          tdis = (lats[b,a] - lats[d,c])^2 + (lons[b,a] - lons[d,c])^2
          if tdis < mdis
            mdis = tdis
            mlat = c
            mlon = d
          end
        end
      end
      push!(flak, (b,a,mlon,mlat))
    end
  end
  @printf("replacing %d lake gridboxes with nearest land point from grid of %d (nlat,nlon = %d,%d)\n", length(flak), nlat*nlon, nlat, nlon)

  for (b,a,d,c) in flak
    land[b,a] = land[d,c]
    lind[b,a] = lind[d,c]
  end
  print("writing LANDMASK\n") ; ncwrite(land, filb, "LANDMASK", start=[1,1,1], count=[-1,-1,1])
  print("writing LU_INDEX\n") ; ncwrite(lind, filb, "LU_INDEX", start=[1,1,1], count=[-1,-1,1])

  vars = ["SOILTEMP", "SNOALB", "CON", "VAR", "OA1", "OA2", "OA3", "OA4"]
  for vnam in vars
    temp = ncread(filb, vnam, start=[1,1,1], count=[-1,-1,-1])[:,:,1]
    for (b,a,d,c) in flak  temp[b,a] = temp[d,c]  end
    print("writing $vnam\n")  ; ncwrite(temp, filb, vnam, start=[1,1,1], count=[-1,-1,1])
  end

  vars = ["LANDUSEF", "SOILCTOP", "SOILCBOT", "ALBEDO12M", "GREENFRAC", "LAI12M"]
    for vnam in vars
    temp = ncread(filb, vnam, start=[1,1,1,1], count=[-1,-1,-1,-1])[:,:,:,1]
    (nlon, nlat, ntmp) = size(temp)
    for e = 1:ntmp, (b,a,d,c) in flak  temp[b,a,e] = temp[d,c,e]  end
    print("writing $vnam\n")  ; ncwrite(temp, filb, vnam, start=[1,1,1,1], count=[-1,-1,-1,1])
  end
end

laketoland()
exit(0)

#=
  lats = ncread(filb,      "CLAT", start=[  1,1,1], count=[   -1,-1,-1])[  :,:,1]
  lons = ncread(filb,     "CLONG", start=[  1,1,1], count=[   -1,-1,-1])[  :,:,1]
  land = ncread(filb,  "LANDMASK", start=[  1,1,1], count=[   -1,-1,-1])[  :,:,1]
  luse = ncread(filb,  "LANDUSEF", start=[1,1,1,1], count=[-1,-1,-1,-1])[:,:,:,1]
  lind = ncread(filb,  "LU_INDEX", start=[  1,1,1], count=[   -1,-1,-1])[  :,:,1]
  stmp = ncread(filb,  "SOILTEMP", start=[  1,1,1], count=[   -1,-1,-1])[  :,:,1]
  stop = ncread(filb,  "SOILCTOP", start=[1,1,1,1], count=[-1,-1,-1,-1])[:,:,:,1]
  sbot = ncread(filb,  "SOILCBOT", start=[1,1,1,1], count=[-1,-1,-1,-1])[:,:,:,1]
  albe = ncread(filb, "ALBEDO12M", start=[1,1,1,1], count=[-1,-1,-1,-1])[:,:,:,1]
  fpar = ncread(filb, "GREENFRAC", start=[1,1,1,1], count=[-1,-1,-1,-1])[:,:,:,1]
  lain = ncread(filb,    "LAI12M", start=[1,1,1,1], count=[-1,-1,-1,-1])[:,:,:,1]
  albs = ncread(filb,    "SNOALB", start=[  1,1,1], count=[   -1,-1,-1])[  :,:,1]
  scon = ncread(filb,       "CON", start=[  1,1,1], count=[   -1,-1,-1])[  :,:,1]
  svar = ncread(filb,       "VAR", start=[  1,1,1], count=[   -1,-1,-1])[  :,:,1]
  soa1 = ncread(filb,       "OA1", start=[  1,1,1], count=[   -1,-1,-1])[  :,:,1]
  soa2 = ncread(filb,       "OA2", start=[  1,1,1], count=[   -1,-1,-1])[  :,:,1]
  soa3 = ncread(filb,       "OA3", start=[  1,1,1], count=[   -1,-1,-1])[  :,:,1]
  soa4 = ncread(filb,       "OA4", start=[  1,1,1], count=[   -1,-1,-1])[  :,:,1]
  (nlon, nlat, nlan) = size(luse) ; print("size of  LANDUSEF is nlan,nlat,nlon = $nlan,$nlat,$nlon\n")
  (nlon, nlat, nsoi) = size(stop) ; print("size of  SOILCTOP is nsoi,nlat,nlon = $nsoi,$nlat,$nlon\n")
  (nlon, nlat, nmon) = size(albe) ; print("size of ALBEDO12M is nmon,nlat,nlon = $nmon,$nlat,$nlon\n")

  function border(b::Int64, a::Int64)
    numb = 0
    land[b-1,a-1] == 1 && (numb += 1)
    land[b  ,a-1] == 1 && (numb += 1)
    land[b+1,a-1] == 1 && (numb += 1)
    land[b-1,a  ] == 1 && (numb += 1)
    land[b+1,a  ] == 1 && (numb += 1)
    land[b-1,a+1] == 1 && (numb += 1)
    land[b  ,a+1] == 1 && (numb += 1)
    land[b+1,a+1] == 1 && (numb += 1)
    numb
  end

  nlak = 0
  flak = Set{Tuple{Int64, Int64}}()
  for a = 2:nlat-1, b = 2:nlon-1
    lind[b,a] == 21 && (nlak += 1)
    lind[b,a] == 21 && border(b,a) > 0 && push!(flak, (b,a))
  end
  @printf("%9d gridboxes are lakes (excluding outer border)\n",  nlak)
  @printf("%9d gridboxes are lakes with a land border\n", length(flak))

# mask = zeros(nlon,nlat)
# for a = 1:nlat, b = 1:nlon
#   lind[b,a]

LANDMASK=>land        0  t,y,x    Landmask : 1=land, 0=water
LANDUSEF=>luse        0  t,z,y,x  Noah-modified 21-category IGBP-MODIS landuse
LU_INDEX=>luseind     0  t,y,x    Dominant category
HGT_M=>hgtm           0  t,y,x    GMTED2010 30-arc-second topography height
SOILTEMP=>soiltemp    0  t,y,x    Annual mean deep soil temperature
SOILCTOP=>soilctop    0  t,z,y,x  16-category top-layer soil type
SCT_DOM=>sctdom       0  t,y,x    Dominant category in top-layer
SOILCBOT=>soilcbot    0  t,z,y,x  16-category bottom-layer soil type
SCB_DOM=>scbdom       0  t,y,x    Dominant category in bottom-layer
ALBEDO12M=>albedo12m  0  t,z,y,x  MODIS monthly surface albedo
GREENFRAC=>greenfrac  0  t,z,y,x  MODIS FPAR
LAI12M=>lai12m        0  t,z,y,x  MODIS LAI
SNOALB=>snoalb        0  t,y,x    MODIS maximum snow albedo
CON=>con              0  t,y,x    landmask
VAR=>var              0  t,y,x    landmask
OA1=>oa1              0  t,y,x    landmask
OA2=>oa2              0  t,y,x    landmask
OA3=>oa3              0  t,y,x    landmask
OA4=>oa4              0  t,y,x    landmask
OL1=>ol1              0  t,y,x    landmask
OL2=>ol2              0  t,y,x    landmask
OL3=>ol3              0  t,y,x    landmask
OL4=>ol4              0  t,y,x    landmask
VAR_SSO=>varsso       0  t,y,x    Variance of Subgrid Scale Orography

  flat = Array{Float32}(undef, FONS, FATS)                                    # read (flipped) grids of flow
  flon = Array{Float32}(undef, FONS, FATS)                                    # accumulation, flow direction,
  facc =   Array{Int32}(undef, FONS, FATS)                                    # and indices of rivers, lakes;
  fdir =   Array{Int16}(undef, FONS, FATS)                                    # evidently, a few gridboxes are
  friv = Set{Tuple{Int32, Int32}}()                                           # ocean/lake or river/lake, and
  flak = Set{Tuple{Int32, Int32}}()                                           # respectively, these are treated
  foce = Set{Tuple{Int32, Int32}}()                                           # (only) as ocean or river below
  temp = ncread(filb,      "LATITUDE", start=[1,1], count=[-1,-1])
  for a = 1:FATS, b = 1:FONS  flat[b,a] = temp[b,FATS+1-a]  end
  temp = ncread(filb,     "LONGITUDE", start=[1,1], count=[-1,-1])
  for a = 1:FATS, b = 1:FONS  flon[b,a] = temp[b,FATS+1-a]  end
  temp = ncread(filb,       "FLOWACC", start=[1,1], count=[-1,-1])
  for a = 1:FATS, b = 1:FONS  facc[b,a] = temp[b,FATS+1-a]  end
  temp = ncread(filb, "FLOWDIRECTION", start=[1,1], count=[-1,-1])
  for a = 1:FATS, b = 1:FONS  fdir[b,a] = temp[b,FATS+1-a]  end
  temp = ncread(filb,   "CHANNELGRID", start=[1,1], count=[-1,-1])
  for a = 1:FATS, b = 1:FONS  temp[b,a] >= 0 && push!(friv, (Int32(b), Int32(FATS+1-a)))  end
  temp = ncread(filb,      "LAKEGRID", start=[1,1], count=[-1,-1])
  for a = 1:FATS, b = 1:FONS  temp[b,a] >= 0 && push!(flak, (Int32(b), Int32(FATS+1-a)))  end
  for a = 1:FATS, b = 1:FONS
                       fdir[b,a] == 0   && (facc[b,a] = typemin(Int32))
    in((b,a), flak) && fdir[b,a] == 0   && (setdiff!(flak, [(b,a)]) ; push!(foce, (b,a)))
    in((b,a), friv) && fdir[b,a] == 0   && (setdiff!(friv, [(b,a)]) ; push!(foce, (b,a)))
    in((b,a), friv) &&  in((b,a), flak) &&  setdiff!(flak, [(b,a)])
  end
  @printf("\n%9d gridboxes define the       river network\n",   length(friv))
  @printf(  "%9d gridboxes define the inland lake network\n",   length(flak))
  @printf(  "%9d gridboxes define the  ocean lake network\n\n", length(foce))

  numb = 0                                                                    # define the ocean by fdir (-1) excluding
  for a = 1:FATS, b = 1:FONS                                                  # ocean entry points and inland sinks (0)
    fdir[b,a] == 0 && (fdir[b,a] = Int16(-1) ; numb += 1)                     # where river flow accumulation is a peak
  end
  @printf("%9d gridboxes define the ocean (values of -1)\n", numb)

  fter = Set{Tuple{Int32, Int32}}()                                           # cast ocean entry points as the only
  function terminus(b::Int32, a::Int32)                                       # locations where fdir is still zero
    (fdir[b,a] == 128) && (fdir[b+1,a+1] == -1) && return(true)
    (fdir[b,a] ==  64) && (fdir[b  ,a+1] == -1) && return(true)
    (fdir[b,a] ==  32) && (fdir[b-1,a+1] == -1) && return(true)
    (fdir[b,a] ==   1) && (fdir[b+1,a  ] == -1) && return(true)
    (fdir[b,a] ==  16) && (fdir[b-1,a  ] == -1) && return(true)
    (fdir[b,a] ==   2) && (fdir[b+1,a-1] == -1) && return(true)
    (fdir[b,a] ==   4) && (fdir[b  ,a-1] == -1) && return(true)
    (fdir[b,a] ==   8) && (fdir[b-1,a-1] == -1) && return(true)
    false
  end

  for a = 2:FATS-1, b = 2:FONS-1
    in((b,a), friv) && terminus(Int32(b), Int32(a)) && (setdiff!(friv, [(b,a)]) ; fdir[b,a] = 0 ; push!(fter, (b,a)))
  end
  @printf("%9d gridboxes define ocean entry points (fdir = 0)\n", length(fter))

  fent = Set{Tuple{Int32, Int32}}()                                           # identify all ocean entry points
  function waterway(b::Int32, a::Int32)                                       # that are connected to rivers
    in((b-1,a-1), friv) && (fdir[b-1,a-1] == 128) && return(true)
    in((b  ,a-1), friv) && (fdir[b  ,a-1] ==  64) && return(true)
    in((b+1,a-1), friv) && (fdir[b+1,a-1] ==  32) && return(true)
    in((b-1,a  ), friv) && (fdir[b-1,a  ] ==   1) && return(true)
    in((b+1,a  ), friv) && (fdir[b+1,a  ] ==  16) && return(true)
    in((b-1,a+1), friv) && (fdir[b-1,a+1] ==   2) && return(true)
    in((b  ,a+1), friv) && (fdir[b  ,a+1] ==   4) && return(true)
    in((b+1,a+1), friv) && (fdir[b+1,a+1] ==   8) && return(true)
    false
  end

  for a = 2:FATS-1, b = 2:FONS-1
    (fdir[b,a] == 0) && waterway(Int32(b), Int32(a)) && push!(fent, (b,a))
  end
  @printf("%9d gridboxes define ocean entry points connected to rivers (values of -2)\n", length(fent))

  fout =  Set{Tuple{Int32, Int32}}()                                          # and specifically those longer
  otoe = Dict{Tuple{Int32, Int32}, Tuple{Int32, Int32}}()                     # than a single gridbox, then save
  function yawretaw(b::Int32, a::Int32)                                       # the river flow outlets (fout) and
    pass = false
    in((b-1,a-1), friv) && (fdir[b-1,a-1] == 128) && waterway(Int32(b-1), Int32(a-1)) && (push!(fout, (b-1,a-1)) ; otoe[(b-1,a-1)] = (b,a) ; pass = true)
    in((b  ,a-1), friv) && (fdir[b  ,a-1] ==  64) && waterway(Int32(b  ), Int32(a-1)) && (push!(fout, (b  ,a-1)) ; otoe[(b  ,a-1)] = (b,a) ; pass = true)
    in((b+1,a-1), friv) && (fdir[b+1,a-1] ==  32) && waterway(Int32(b+1), Int32(a-1)) && (push!(fout, (b+1,a-1)) ; otoe[(b+1,a-1)] = (b,a) ; pass = true)
    in((b-1,a  ), friv) && (fdir[b-1,a  ] ==   1) && waterway(Int32(b-1), Int32(a  )) && (push!(fout, (b-1,a  )) ; otoe[(b-1,a  )] = (b,a) ; pass = true)
    in((b+1,a  ), friv) && (fdir[b+1,a  ] ==  16) && waterway(Int32(b+1), Int32(a  )) && (push!(fout, (b+1,a  )) ; otoe[(b+1,a  )] = (b,a) ; pass = true)
    in((b-1,a+1), friv) && (fdir[b-1,a+1] ==   2) && waterway(Int32(b-1), Int32(a+1)) && (push!(fout, (b-1,a+1)) ; otoe[(b-1,a+1)] = (b,a) ; pass = true)
    in((b  ,a+1), friv) && (fdir[b  ,a+1] ==   4) && waterway(Int32(b  ), Int32(a+1)) && (push!(fout, (b  ,a+1)) ; otoe[(b  ,a+1)] = (b,a) ; pass = true)
    in((b+1,a+1), friv) && (fdir[b+1,a+1] ==   8) && waterway(Int32(b+1), Int32(a+1)) && (push!(fout, (b+1,a+1)) ; otoe[(b+1,a+1)] = (b,a) ; pass = true)
    pass
  end

  for fnex in fent                                                            # map outlets to ocean entries and
    !yawretaw(fnex[1], fnex[2]) && setdiff!(fent, [(fnex[1], fnex[2])])       # de-select single-gridbox rivers
  end
  @printf("%9d gridboxes define ocean entry points connected to multiple-gridbox rivers (values of -2)\n", length(fent))

  filp = ARGS[2] * "/EEZ/EEZ_Land_v3_202030_contiguous_Alaska.csv"    ; fpp = My.ouvre(filp, "r") ; alin = readline(fpp) ; close(fpp)
  filp = ARGS[2] * "/EEZ/EEZ_Land_v3_202030_contiguous_Canada.csv"    ; fpp = My.ouvre(filp, "r") ; clin = readline(fpp) ; close(fpp)
  filp = ARGS[2] * "/EEZ/EEZ_Land_v3_202030_contiguous_Greenland.csv" ; fpp = My.ouvre(filp, "r") ; glin = readline(fpp) ; close(fpp)
  abox = readgeom("POLYGON(($alin))")
  cbox = readgeom("POLYGON(($clin))")                                         # save all (fora) or just the northern
  gbox = readgeom("POLYGON(($glin))")                                         # (forn) river outlet and ocean entry
  fora = "" ; forn = ""                                                       # points to sorted text files
  for fnex in fout
    if      fnex[2] > 9999 || fnex[2] + 10000 * fnex[1] > typemax(Int32)
      @show fnex[2] > 9999 || fnex[2] + 10000 * fnex[1] > typemax(Int32), fnex[2], fnex[1], typemax(Int32)
      error("at least one gridbox index is too large to share five digits of an Int32\n")
    end
    fnee = fnex[2] + Int32(10000) * fnex[1] ; fnxx = otoe[fnex]
    line = @sprintf("%10d %5d %5d %5d %15.8f %15.8f %5d %5d %5d %15.8f %15.8f\n", fnee,
           fnex[1], fnex[2], FATS+1-fnex[2], flat[fnex[1],fnex[2]], flon[fnex[1],fnex[2]],
           fnxx[1], fnxx[2], FATS+1-fnxx[2], flat[fnxx[1],fnxx[2]], flon[fnxx[1],fnxx[2]])
    fora *= line
    llpt = readgeom("POINT($(flon[fnex[1],fnex[2]]) $(flat[fnex[1],fnex[2]]))")
    if contains(abox, llpt) || contains(cbox, llpt) || contains(gbox, llpt)
      forn *= line
    end
  end
  fora = join(sort(split(fora[1:end-1], "\n")), "\n") * "\n"
  forn = join(sort(split(forn[1:end-1], "\n")), "\n") * "\n"
  fpa = My.ouvre(filz, "w") ; write(fpa, fora) ; close(fpa)
  fpa = My.ouvre(fizz, "w") ; write(fpa, forn) ; close(fpa)
  @printf("%9d gridboxes define solitary, multi-gridbox river outflows to the ocean (values of -2)\n", length(fout))

  for fnex in fout                                                            # initialize each river outlet gridbox
#   fnxx = otoe[fnex] #; fnxx = fnex                                          # in facc using an ocean entry (or river
#   facc[fnex[1],fnex[2]] = fnxx[2] + Int32(10000) * fnxx[1]                  # outlet) number that is assigned to the
    facc[fnex[1],fnex[2]] = fnex[2] + Int32(10000) * fnex[1]                  # entire watershed, and save river outlets
  end
  fsav = deepcopy(fout)

  fwat = Base.union(friv, flak)
# for a = 1:FATS, b = 1:FONS
#   in((b,a), fwat) && (fdir[b,a] == 0) && setdiff!(fwat, [(b,a)])
# end
  function watermrk(b::Int32, a::Int32)
    numb = 0
    in((b-1,a-1), fwat) && (fdir[b-1,a-1] == 128) && (setdiff!(fwat, [(b-1,a-1)]) ; push!(fout, (b-1,a-1)) ; facc[b-1,a-1] = facc[b,a] ; numb += 1)
    in((b  ,a-1), fwat) && (fdir[b  ,a-1] ==  64) && (setdiff!(fwat, [(b  ,a-1)]) ; push!(fout, (b  ,a-1)) ; facc[b  ,a-1] = facc[b,a] ; numb += 1)
    in((b+1,a-1), fwat) && (fdir[b+1,a-1] ==  32) && (setdiff!(fwat, [(b+1,a-1)]) ; push!(fout, (b+1,a-1)) ; facc[b+1,a-1] = facc[b,a] ; numb += 1)
    in((b-1,a  ), fwat) && (fdir[b-1,a  ] ==   1) && (setdiff!(fwat, [(b-1,a  )]) ; push!(fout, (b-1,a  )) ; facc[b-1,a  ] = facc[b,a] ; numb += 1)
    in((b+1,a  ), fwat) && (fdir[b+1,a  ] ==  16) && (setdiff!(fwat, [(b+1,a  )]) ; push!(fout, (b+1,a  )) ; facc[b+1,a  ] = facc[b,a] ; numb += 1)
    in((b-1,a+1), fwat) && (fdir[b-1,a+1] ==   2) && (setdiff!(fwat, [(b-1,a+1)]) ; push!(fout, (b-1,a+1)) ; facc[b-1,a+1] = facc[b,a] ; numb += 1)
    in((b  ,a+1), fwat) && (fdir[b  ,a+1] ==   4) && (setdiff!(fwat, [(b  ,a+1)]) ; push!(fout, (b  ,a+1)) ; facc[b  ,a+1] = facc[b,a] ; numb += 1)
    in((b+1,a+1), fwat) && (fdir[b+1,a+1] ==   8) && (setdiff!(fwat, [(b+1,a+1)]) ; push!(fout, (b+1,a+1)) ; facc[b+1,a+1] = facc[b,a] ; numb += 1)
    numb
  end

  totl = length(fout)                                                         # using the combined river and lake
  numb = 1                                                                    # indices (fwat), identify upstream
  while (numb > 0)                                                            # water networks connected to each
    numb = 0                                                                  # ocean entry point (or river outlet)
    for fnex in fout
      numb += watermrk(fnex[1], fnex[2])
    end
    totl += numb
#   @printf("%9d gridboxes are connected to the outflow (values of -2)\n", numb)
  end
  @printf("%9d gridboxes define the upstream rivers, lakes, and connected outflow(s) (values of -2)\n", totl)

  function watershed(b::Int64, a::Int64)
    numb = 0
    fdir[b-1,a-1] == 128 && (fdir[b-1,a-1] = -2 ; facc[b-1,a-1] = facc[b,a] ; numb += 1)
    fdir[b  ,a-1] ==  64 && (fdir[b  ,a-1] = -2 ; facc[b  ,a-1] = facc[b,a] ; numb += 1)
    fdir[b+1,a-1] ==  32 && (fdir[b+1,a-1] = -2 ; facc[b+1,a-1] = facc[b,a] ; numb += 1)
    fdir[b-1,a  ] ==   1 && (fdir[b-1,a  ] = -2 ; facc[b-1,a  ] = facc[b,a] ; numb += 1)
    fdir[b+1,a  ] ==  16 && (fdir[b+1,a  ] = -2 ; facc[b+1,a  ] = facc[b,a] ; numb += 1)
    fdir[b-1,a+1] ==   2 && (fdir[b-1,a+1] = -2 ; facc[b-1,a+1] = facc[b,a] ; numb += 1)
    fdir[b  ,a+1] ==   4 && (fdir[b  ,a+1] = -2 ; facc[b  ,a+1] = facc[b,a] ; numb += 1)
    fdir[b+1,a+1] ==   8 && (fdir[b+1,a+1] = -2 ; facc[b+1,a+1] = facc[b,a] ; numb += 1)
    numb
  end

  for fnex in fout  fdir[fnex[1],fnex[2]] = -2  end                           # then identify the remainder of
  numb = 1                                                                    # each watershed using fdir < 0
  while (numb > 0)
    numb = 0
    for a = 2:FATS-1, b = 2:FONS-1
      fdir[b,a] < -1 && (numb += watershed(b, a))
    end
    totl += numb
  end
  @printf("%9d gridboxes define the upstream watershed without a border (values of -2)\n", totl)

  tmpa = Array{Int32}(undef, FONS, FATS)
  for fnex in fent  facc[fnex[1],fnex[2]]  = typemin(Int32)  end              # and finlly, set ocean entry and
  for fnex in foce  facc[fnex[1],fnex[2]]  = typemin(Int32)  end              # inland sinks to ocean, and outlet
  for fnex in fsav  facc[fnex[1],fnex[2]] *= -1              end              # points to negative and write facc
  for a = 1:FATS, b = 1:FONS
    facc[b,a] > 0 && fdir[b,a] == 0 && (facc[b,a] = typemin(Int32))
    tmpa[b,FATS+1-a] = facc[b,a]
  end
  ncwrite(tmpa, fily, "FLOWACC", start=[1,1], count=[-1,-1])
=#
