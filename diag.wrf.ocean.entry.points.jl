#=
 = Loop through all files in a directory (and any subdirectories, but
 = note that a link is created only to the first file found, where two
 = files from different subdirs may have the same name).  Parse daily
 = data into reduced 6-h files (ncks) and then interpolate (ncl) - RD
 = February 2020.
 =#

floacc -2.14748e+09 missing and neighbour values > 1e2 being river inputs to ocean
last point before St. Lawrence - Xdim = 536  Ydim = 439 (from northwest to southeast)
but last nonzero flow point is at x=535 y=231 on CHRTOUT_GRID1

if (argc = length(ARGS)) != 3
  print("\nUsage: jjj $(basename(@__FILE__)) src_data wrf_grid ncl_dir\n")
  print(  "as in: jjj $(basename(@__FILE__)) /nas/data/zhangm/NA2/post-pro35-3/surface ../DOMAIN_full_full/geogrid.nc ~/prog/diag.hydrology\n\n")
  exit(1)
end
pgstem = basename(@__FILE__)[1:end-3]
srcdir = ARGS[1]
wrfgrd = ARGS[2]
wgtgrd =                 pgstem * ".weight.nc" ; isfile(wgtgrd) && rm(wgtgrd)
intgrd =                 pgstem * ".interp.nc" ; isfile(intgrd) && rm(intgrd)
wgtncl = ARGS[3] * "/" * pgstem * ".weight.ncl"
intncl = ARGS[3] * "/" * pgstem * ".interp.ncl"

function interpo(file::AbstractString)                                        # define daily WRF interpolation to 6-h output files
  file[17] != 'y' && (stem = file[17:20] * file[22:23] * file[25:26])
  file[17] == 'y' && (stem = file[18:21] * file[23:24] * file[26:27])
  for a = 0:3
    a == 0 && (filf = stem * "00.LDASIN_DOMAIN1")
    a == 1 && (filf = stem * "06.LDASIN_DOMAIN1")
    a == 2 && (filf = stem * "12.LDASIN_DOMAIN1")
    a == 3 && (filf = stem * "18.LDASIN_DOMAIN1")
    print("ncks -d time,$a,$a -v time,p_sfc,T_2m,q_2m,u_10m_tr,v_10m_tr,rainncv,raincv,SW_d,LW_d $file $filf\n")
      run(`ncks -d time,$a,$a -v time,p_sfc,T_2m,q_2m,u_10m_tr,v_10m_tr,rainncv,raincv,SW_d,LW_d $file $filf`)
    print("ncap2 -s  rain=(rainncv+raincv)  $filf $filf.nc ; rm $filf\n")
      run(`ncap2 -s "rain=(rainncv+raincv)" $filf $filf.nc`) ; rm(filf)
# 'srcFileName="hrrr.*.grib2"' 'dstGridName="geogrid.nc"' HRRR2WRFHydro_regrid.ncl
#   print("ncl srcFileName=\"$filf\" dstGridName=\"$wrfgrd\" wgtFileName=\"$wgtgrd\" $wgtncl\n")
#     run(`ncl srcFileName=\"$filf\" dstGridName=\"$wrfgrd\" wgtFileName=\"$wgtgrd\" $wgtncl`)
  end
end

function walkdir(dir::AbstractString, getweight::Bool)                        # for each daily WRF file in srcdir
  absdir = abspath(dir)
  files  = readdir(absdir)
  for file in files
    full = joinpath(absdir, file)
    if isfile(full) && endswith(full, ".nc")
      if islink(file)
        print("\n$file link already exists, so not interpolating from $full\n\n")
      else
        if getweight                                                          # first get fixed interpolation weights
          getweight = false
          solnk = "libcrypto.so.10" ; solib = "/lib/x86_64-linux-gnu/libcrypto.so.1.0.0" ; islink(solnk) && rm(solnk) ; symlink(solib, solnk)
          solnk = "libssl.so.10"    ; solib = "/lib/x86_64-linux-gnu/libssl.so.1.0.0"    ; islink(solnk) && rm(solnk) ; symlink(solib, solnk)
          print("\nncl interp_opt=\"bilinear\" srcGridName=\"$full\" dstGridName=\"$wrfgrd\" wgtFileName=\"$wgtgrd\" $wgtncl\n\n")
              run(`ncl interp_opt=\"bilinear\" srcGridName=\"$full\" dstGridName=\"$wrfgrd\" wgtFileName=\"$wgtgrd\" $wgtncl`)
          rm("SCRIP_HRRR_bilinear.nc") ; rm("SCRIP_WRFHydro_bilinear.nc") ; rm("PET0.RegridWeightGen.Log") ; print("\n")
#         rm("libcrypto.so.10") ; rm("SCRIP_HRRR_bilinear.nc") ; rm("PET0.RegridWeightGen.Log")
#         rm("libssl.so.10")    ; rm("SCRIP_WRFHydro_bilinear.nc") ; print("\n")
        end
        symlink(full, file)
        interpo(file)                                                         # then interpolate each daily file
        rm(file)
      end
    elseif isdir(full)
      walkdir(full, false)
    end
  end
end

walkdir(srcdir, true)
exit(0)
