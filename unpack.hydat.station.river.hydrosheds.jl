#=
 = Replace initially missing HydroSHEDS data in a text station file (.sta) with
 = those of a nearby location that matches well in upstream catchment area, as
 = given by the North American, Arctic, or Greenland HydroSHEDS reaches.  When
 = the HyDAT station is close to where two streams meet, matching by upstream
 = catchment (to within some fraction) allows the HyDAT station to be located
 = either slightly downstream (if catchment area is large) or slightly upstream
 = on one of the two smaller streams (if catchment area is small).  Save a list
 = of any HyDAT stations that have no HydroSHEDS reaches nearby - RD Feb 2023.
 =#

using My, Printf, NetCDF, Shapefile

const RDIS             = 0.02                           # search distance for reach endpoints near a station (degrees)
const FRAC             = 0.15                           # fractional match between HyDAT and HydroSHEDS upstream catchment
const D2R              = 3.141592654 / 180.0            # degrees to radians conversion
const DELMISS          = -8e9                           # generic missing value comparison
const GRDMISS          = -9e9                           # generic missing value on a grid
const MISS             = -9999.0                        # generic missing value
const MIST             = @sprintf("%d", Int64(MISS))    # generic missing value string

if (argc = length(ARGS)) != 2
  print("\nUsage: jjj $(basename(@__FILE__)) ../HYDROSHEDS/ hydat_03JB002.sta\n\n")
  exit(1)
end
fila  = ARGS[2]
nafil = ARGS[1] * "HydroRIVERS_v10_na.shp" ; natab = Shapefile.Table(nafil) ; nalen = length(natab) ; nageo = Shapefile.shapes(natab)
arfil = ARGS[1] * "HydroRIVERS_v10_ar.shp" ; artab = Shapefile.Table(arfil) ; arlen = length(artab) ; argeo = Shapefile.shapes(artab)
grfil = ARGS[1] * "HydroRIVERS_v10_gr.shp" ; grtab = Shapefile.Table(grfil) ; grlen = length(grtab) ; grgeo = Shapefile.shapes(grtab)
fmis  = "hydat_zzzzzzz.mis"

function findreach()                                                          # locate a HydroSHEDS river reach
  fpa   = My.ouvre(fila, "r")                                                 # endpoint that is near a station and
  lins  = readlines(fpa, keep = true) ; close(fpa)                            # is similar in upstream area (but if
  hlat  = parse(Float64, split(lins[6])[2])                                   # there is no upstream area value,
  hlon  = parse(Float64, split(lins[7])[2])                                   # then ignore this limit)
  if split(lins[8])[2] != "missing"
    hare = parse(Float64, split(lins[8])[2]) ; hdel = hare * FRAC
  else
    hare = 0.0 ; hdel = 9e9
  end

  clos = Array{Tuple{Float64, Int8, Int64, Int16, Float64, Float64}}(undef, 0)
  for a = 1:3
    a == 1 && (ddtab = natab ; ddlen = nalen ; ddgeo = nageo)                 # first compile the HydroSHEDS endpoints
    a == 2 && (ddtab = artab ; ddlen = arlen ; ddgeo = argeo)                 # inside the station search distance
    a == 3 && (ddtab = grtab ; ddlen = grlen ; ddgeo = grgeo)
    for b = 1:ddlen
      ddpts = ddgeo[b].points
      for c = 1:length(ddpts)
        (ddlat, ddlon) = (ddpts[c].y, ddpts[c].x)
        tmpdis = abs(ddlat - hlat) + abs(ddlon - hlon) * cos(hlat * D2R)
        tmpdis < RDIS && push!(clos, (tmpdis, a, b, c, ddtab.MAIN_RIV[b], ddtab.UPLAND_SKM[b]))
      end
    end
  end

  if length(clos) == 0
    print("no match for $fila\n")
    form = "mv $(fila[1:end-4]) hydat_noriver\n"
    fpa = My.ouvre(fmis, "a") ; write(fpa, form) ; close(fpa)
  else                                                                        # and if FRAC is not too constraining,
    print("   match for $fila is $(length(clos))\n")                          # select from among HydroSHEDS endpoints
    mindisa = 9e9 ; mininda = 0                                               # within FRAC of upstream catchment area
    mindisb = 9e9 ; minindb = 0                                               # (so mininda != 0 but minindb can be 0)
    for a = 1:length(clos)
      clos[a][1] < mindisa                                           && (mindisa = clos[a][1] ; mininda = a)
      clos[a][1] < mindisb && hare - hdel < clos[a][6] < hare + hdel && (mindisb = clos[a][1] ; minindb = a)
    end
    minind = minindb == 0 ? mininda : minindb
    mina = clos[minind][2] ; minb = clos[minind][3] ; minc = clos[minind][4]
    mina == 1 && (ddtab = natab ; ddlen = nalen ; ddgeo = nageo)
    mina == 2 && (ddtab = artab ; ddlen = arlen ; ddgeo = argeo)
    mina == 3 && (ddtab = grtab ; ddlen = grlen ; ddgeo = grgeo)              # then save the match and station data

    form  = "" ; for a = 1:20  form *= lins[a]  end
    form *= @sprintf("%-24s %17.14f %d\n",            "LATITUDE", ddgeo[minb].points[minc].y, Int64(MISS))
    form *= @sprintf("%-24s %17.14f %d\n",           "LONGITUDE", ddgeo[minb].points[minc].x, Int64(MISS))
    form *= @sprintf("%-24s %d\n",                    "HYRIV_ID", ddtab.HYRIV_ID[minb])
    form *= @sprintf("%-24s %d\n",                   "NEXT_DOWN", ddtab.NEXT_DOWN[minb])
    form *= @sprintf("%-24s %d\n",                    "MAIN_RIV", ddtab.MAIN_RIV[minb])
    form *= @sprintf("%-24s %f\n",                   "LENGTH_KM", ddtab.LENGTH_KM[minb])
    form *= @sprintf("%-24s %f\n",                  "DIST_DN_KM", ddtab.DIST_DN_KM[minb])
    form *= @sprintf("%-24s %f\n",                  "DIST_UP_KM", ddtab.DIST_UP_KM[minb])
    form *= @sprintf("%-24s %f\n",                   "CATCH_SKM", ddtab.CATCH_SKM[minb])
    form *= @sprintf("%-24s %f\n",                  "UPLAND_SKM", ddtab.UPLAND_SKM[minb])
    form *= @sprintf("%-24s %d\n",                   "ENDORHEIC", ddtab.ENDORHEIC[minb])
    form *= @sprintf("%-24s %f\n",                  "DIS_AV_CMS", ddtab.DIS_AV_CMS[minb])
    form *= @sprintf("%-24s %d\n",                    "ORD_STRA", ddtab.ORD_STRA[minb])
    form *= @sprintf("%-24s %d\n",                    "ORD_CLAS", ddtab.ORD_CLAS[minb])
    form *= @sprintf("%-24s %d\n",                    "ORD_FLOW", ddtab.ORD_FLOW[minb])
    form *= @sprintf("%-24s %d\n",                   "HYBAS_L12", ddtab.HYBAS_L12[minb])
    form *= @sprintf("%-24s %s as of %s\n", "HydroSHEDS_VERSION", "1.0", "2019-10-01")
    for a = 38:length(lins)  form *= lins[a]  end
    fpa = My.ouvre(fila, "w") ; write(fpa, form) ; close(fpa)
  end
end

findreach()
exit(0)
