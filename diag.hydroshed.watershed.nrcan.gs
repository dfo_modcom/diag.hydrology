* This script is designed to plot a watershed map.
* It can be executed using a command like
*
*     grads -blc "diag.hydroshed.watershed.nrcan Fulldom_hires_outlets_nrcan"
*
* - RD April 2021.

function plot(args)
fila = subwrd(args,1)".ctl"
filb = subwrd(args,1)".txt"
filc = subwrd(args,1)".png"

"clear"
"set grads off"
"set grid off"
"set xlab off"
"set ylab off"
"set digsiz 0.09"
"set dignum 2"
"set mpdset hires"
"set mpdraw off"
"set mproj off"
*"set gxout grfill"

"sdfopen hgt.sfc.nc"
"set lat  41.2089  58.1372"
"set lon -94.6194 -51.3806"
"set clevs 9e9" ; "d hgt"
"run basemap L 16 16 H"

a = 1
filestat = read(filb)
while (sublin(filestat,1) = 0)
  line = sublin(filestat,2)
  ncol = subwrd(line,1)
  nriv = subwrd(line,2)
  nshp = subwrd(line,3)
* ndom = substr(nshp,1,6)
* if (ndom = "NHN_02") ; ncol = 12 ; endif

  "set line 1 1 3"
  "set shpopts "ncol
  "set grads off" ; "set mpdraw off" ; "draw shp shp/"nshp
  filestat = read(filb)
  say a" "line
  a = a + 1
* if (a = 50) ; filestat = "stop" ; endif
endwhile
filestat = close(filb)

"set rgb 16 80 80 80"
"run basemap O 0 0 H"

"set line 0 1 10"
"draw recf 0.65 4.12 5.65 7.63"
"set vpage 0.4 5.9 3.75 8.00"
"set grads  off" ; "set grid off"
"set mpdraw off" ; "set mproj off"
"set xlab   off" ; "set xlopts 1 3 0.18"
"set ylab   off" ; "set ylopts 1 3 0.18"
"set lat  46.7530  49.0967"
"set lon -71.6077 -66.7101"
"set clevs 9e9" ; "d hgt"
"set grads  off" ; "set grid off"
"set mpdraw off" ; "set mproj off"
"run basemap L 16 16 H"

a = 1
filestat = read(filb)
while (sublin(filestat,1) = 0)
  line = sublin(filestat,2)
  ncol = subwrd(line,1)
  nriv = subwrd(line,2)
  nshp = subwrd(line,3)
* ndom = substr(nshp,1,6)
* if (ndom = "NHN_02") ; ncol = 12 ; endif

  "set line 1 1 3"
  "set shpopts "ncol
  "set grads off" ; "set mpdraw off" ; "draw shp shp/"nshp
  "set line 0 1 1"
  "set shpopts 0"
  "set grads off" ; "set mpdraw off" ; "draw shp shp/"nriv
  filestat = read(filb)
  say a" "line
  a = a + 1
* if (a = 50) ; filestat = "stop" ; endif
endwhile
filestat = close(filb)

"set rgb 16 80 80 80"
"set grads  off" ; "set grid off"
"set mpdraw off" ; "set mproj off"
"run basemap O 0 0 H"

say "gxprint "filc" png white x1100 y850"
    "gxprint "filc" png white x1100 y850"
"quit"

* 12       NHN_01AA000_2_0_HN_NLFLOW_1       NHN_01AA000___WORKUNIT_LIMIT_2
