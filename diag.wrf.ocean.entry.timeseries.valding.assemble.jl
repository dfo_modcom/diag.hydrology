#=
 = Create a NetCDF file that accommodates an ensemble of four three-hourly WRF-Hydro streamflow
 = estimates (WRF/ERA forcing, raw/neural-network output), and daily HYDAT observations, as well
 = as performance metrics.  Raw WRF-Hydro streamflow, HYDAT streamflow, and HYDAT obs indicators
 = are taken from source NetCDF and HYDAT data and station names are taken from the current dir
 = (neural network estimates are added later).  This script seems to have difficulty writing data
 = in one complete pass, so all stations are written at each timestep and a date file is updated
 = so the script can restart where it left off - RD Jan, Feb 2024.
 =#

using My, Printf, NetCDF

const NPAR             = 4                              # number of WRF-Hydro streamflow estimates (WRF/ERA forcing, raw/neural-network)
const DELT             = 3                              # timestep (hours)
const HORA             = @sprintf("-0%d",      DELT)    # first hour of the first day of WRF-Hydro data
const HORB             = @sprintf("-%2d", 24 - DELT)    #  last hour of the  last day of WRF-Hydro data
const MISS             = -9999.0                        # generic missing value

if (argc = length(ARGS)) != 5
  print("\nUsage: jjj $(basename(@__FILE__)) 1990-01-01 2004-12-30 ../SPINUP.wrf/dat/ ../SPINUP.era/dat/ hydat_valding_aall_1000_incl\n\n")
  exit(1)
end
daya = ARGS[1] ; contains(ARGS[3], "ccs") && (tail = "ccs")
dayb = ARGS[2] ; contains(ARGS[3], "h45") && (tail = "h45")
dira = ARGS[3] ; contains(ARGS[3], "h85") && (tail = "h85")
dirb = ARGS[4] ; contains(ARGS[3], "wrf") && (tail = "ncl")
fila = ARGS[5][1:end-3] * tail * ".txt" ; fpa = My.ouvre(fila, "r", false) ; lina = readlines(fpa) ; close(fpa)
filb = ARGS[5][1:end-3] * tail * "_" * daya * "_" * dayb * ".nc"
filc = ARGS[5][1:end-3] * tail * "_" * daya * "_" * dayb * ".txt"
fild = ARGS[5][1:end-3] * tail * "_" * daya * "_" * dayb * ".lastdate"

@printf("\ncopying %s %s\n", fila, filc) ; cp(fila, filc; force = true, follow_symlinks = true)

hors = Array{Float64}(undef,0)                                                # get WRF-Hydro times at DELT intervals
hmsk = Array{Bool   }(undef,0)                                                # and a HYDAT mask at daily intervals
hora = daya * HORA ; tima = My.datesous("1900-01-01-00", hora, "hr")
horb = dayb * HORB ; timb = My.datesous("1900-01-01-00", horb, "hr")
horc = hora        ; timc = tima
while timc <= timb
  global horc, timc       ; push!(hors, timc)
  horc[end-1:end] == "12" ? push!(hmsk, true) : push!(hmsk, false)
  horc = dateadd(horc, DELT, "hr")
  timc = My.datesous("1900-01-01-00", horc, "hr")
end
days  = hors[hmsk] ; nday = length(days)
daya *= "-12"      ; nhor = length(hors)
dayb *= "-12"
@printf("reading %5d WRF-Hydro values between %s and %s at %2d-h intervals\n", nhor, hora, horb, DELT)
@printf("reading %5d     HYDAT values between %s and %s at %2d-h intervals\n", nday, daya, dayb,   24)

xind = Array{Int64}(undef,0)                                                  # read the station locations
yind = Array{Int64}(undef,0)
for line in lina
  name = split(line)[1] * ".sta"
  fpb  = My.ouvre(name, "r", false) ; lins = readlines(fpb, keep = true) ; close(fpb)
  lati = parse(Int64, split(lins[90])[3])
  loni = parse(Int64, split(lins[91])[3])
  push!(xind, loni)
  push!(yind, lati)
end
nsta = length(lina)
@printf("reading %5d station%s\n", nsta, nsta != 1 ? "s" : "")

function nccreer(fn::AbstractString, tims::Array{Float64,1}, lats::Array{Float64,1}, lons::Array{Float64,1})
  nctim = NcDim("time", length(tims), atts = Dict{Any,Any}("units"=>"hours since 1900-01-01 00:00:0.0"), values = tims)
  nclat = NcDim( "lat", length(lats), atts = Dict{Any,Any}("units"=>"degrees_north"),                    values = lats)
  nclon = NcDim( "lon", length(lons), atts = Dict{Any,Any}("units"=> "degrees_east"),                    values = lons)
  ncvrs = Array{NetCDF.NcVar}(undef, 16)
  ncvrs[ 1] = NcVar("hydt", [       nclat, nctim], atts = Dict{Any,Any}("units"=>"m3/s", "missing_value"=>MISS), t=Float64, compress=-1)
  ncvrs[ 2] = NcVar("hydi", [       nclat, nctim], atts = Dict{Any,Any}("units"=>"none", "missing_value"=>MISS), t=Float64, compress=-1)
  ncvrs[ 3] = NcVar("flow", [nclon, nclat, nctim], atts = Dict{Any,Any}("units"=>"m3/s", "missing_value"=>MISS), t=Float64, compress=-1)
  ncvrs[ 4] = NcVar("para", [nclon,        nctim], atts = Dict{Any,Any}("units"=>"none", "missing_value"=>MISS), t=Float64, compress=-1)
  ncvrs[ 5] = NcVar("parb", [nclon,        nctim], atts = Dict{Any,Any}("units"=>"none", "missing_value"=>MISS), t=Float64, compress=-1)
  ncvrs[ 6] = NcVar("parc", [nclon,        nctim], atts = Dict{Any,Any}("units"=>"none", "missing_value"=>MISS), t=Float64, compress=-1)
  ncvrs[ 7] = NcVar("tmpa", [nclon, nclat       ], atts = Dict{Any,Any}("units"=>"none", "missing_value"=>MISS), t=Float64, compress=-1)
  ncvrs[ 8] = NcVar("tmpb", [nclon, nclat       ], atts = Dict{Any,Any}("units"=>"none", "missing_value"=>MISS), t=Float64, compress=-1)
  ncvrs[ 9] = NcVar("tmpc", [nclon, nclat       ], atts = Dict{Any,Any}("units"=>"none", "missing_value"=>MISS), t=Float64, compress=-1)
  ncvrs[10] = NcVar("tmpd", [nclon, nclat       ], atts = Dict{Any,Any}("units"=>"none", "missing_value"=>MISS), t=Float64, compress=-1)
  ncvrs[11] = NcVar("tmpe", [nclon, nclat       ], atts = Dict{Any,Any}("units"=>"none", "missing_value"=>MISS), t=Float64, compress=-1)
  ncvrs[12] = NcVar("tmpf", [nclon, nclat       ], atts = Dict{Any,Any}("units"=>"none", "missing_value"=>MISS), t=Float64, compress=-1)
  ncvrs[13] = NcVar("tmpg", [nclon, nclat       ], atts = Dict{Any,Any}("units"=>"none", "missing_value"=>MISS), t=Float64, compress=-1)
  ncvrs[14] = NcVar("tmph", [nclon, nclat       ], atts = Dict{Any,Any}("units"=>"none", "missing_value"=>MISS), t=Float64, compress=-1)
  ncvrs[15] = NcVar("tmpi", [nclon, nclat       ], atts = Dict{Any,Any}("units"=>"none", "missing_value"=>MISS), t=Float64, compress=-1)
  ncvrs[16] = NcVar("tmpj", [nclon, nclat       ], atts = Dict{Any,Any}("units"=>"none", "missing_value"=>MISS), t=Float64, compress=-1)
  ncfil = NetCDF.create(fn, ncvrs, gatts = Dict{Any,Any}("units"=>"none"), mode = NC_NETCDF4)
end

lats = collect(1.0:nsta)                                                      # either create the NetCDF template
lons = collect(1.0:NPAR)                                                      # or read what was written to this
if !isfile(filb)                                                              # file so far (in case complete data
  nccreer(filb, hors, lats, lons)                                             # matrices can be written this time)
  flow = ones(NPAR, nsta, nhor) * MISS
  para = ones(NPAR,       nhor) * MISS
  ncwrite(flow, filb, "flow", start=[1,1,1], count=[-1,-1,-1])
  ncwrite(para, filb, "para", start=[1,1],   count=[-1,-1])
  dnow = hora[1:4] * hora[6:7] * hora[9:10] * hora[12:13]
  nbeg = 1
else
  flow = ncread(filb, "flow", start=[1,1,1], count=[-1,-1,-1])
  para = ncread(filb, "para", start=[1,1],   count=[-1,-1])
  line = split(readline(fild))
  dnow =              line[1]
  nbeg = parse(Int64, line[2])
end

file = "../hydat/hydat_zzzzzzz.nc"                                            # get the HYDAT source times
thyd = ncread(file, "time", start=[1], count=[-1])
tima = My.datesous("1900-01-01-00", daya, "hr")
tind = findfirst(isequal(tima), thyd)

hydt = ones(nsta, nhor) * MISS                                                # read and store the HYDAT data
hydi = ones(nsta, nhor) * MISS                                                # (including obs indicators)
for a = 1:length(lina)
  filh = split(lina[a])[1] * ".nc"
  hydt[a,hmsk] = vec(ncread(filh, "flow", start=[1,1,tind], count=[-1,-1,nday]))
  hydi[a,hmsk] = vec(ncread(filh, "floi", start=[1,1,tind], count=[-1,-1,nday]))
end
ncwrite(hydt, filb, "hydt", start=[1,1], count=[-1,-1])
ncwrite(hydi, filb, "hydi", start=[1,1], count=[-1,-1])

function ncseries()                                                           # read and store the WRF-Hydro data
  global dnow                                                                 # and identify raw ERA-5 forcing as
  for a = nbeg:nhor                                                           # reanalysis synoptic forcing is more
    nama = dira * dnow * "00.CHRTOUT_GRID1"                                   # optimal (i.e., in HYDAT comparisons,
    namb = dirb * dnow * "00.CHRTOUT_GRID1"                                   # reanalysis synoptic forcing is more
    if isfile(nama) && isfile(namb)                                           # consistent than that of WRF RCP)
      print("reading streamflow for $nama and $namb\n")
      grida = ncread(nama, "streamflow", start=[1,1,1], count=[-1,-1,-1])
      gridb = ncread(namb, "streamflow", start=[1,1,1], count=[-1,-1,-1])
      for b = 1:nsta
        flow[1,b,a] = grida[xind[b],yind[b],1]
        flow[3,b,a] = gridb[xind[b],yind[b],1]
      end
      ncwrite(flow[:,:,a], filb, "flow", start=[1,1,a], count=[-1,-1,1])
    else
      print("MISSING streamflow for $nama and $namb ***\n")
    end
    form = dnow * " $a"
    fpc  = My.ouvre(fild, "w", false) ; write(fpc, form) ; close(fpc)
    dnow = My.dateadd(dnow, DELT, "hr")
  end
end
ncseries()

@printf("writing streamflow to %s\n\n", filb)
pind = 3 ; para[:,2] .= -pind ; para[pind,2] = pind
#ncwrite(flow, filb, "flow", start=[1,1,1], count=[-1,-1,-1])
ncwrite(para, filb, "para", start=[1,1],   count=[-1,-1])
print("\n")
exit(0)
