#=
 = Sum a selected set of timeseries - RD May 2020.
 =#

using My, Printf, NetCDF

const DELT             = 1, "dy"                        # input file timestep
const MISS             = -9999.0                        # generic missing value
const D2R              = 3.141592654 / 180.0            # degrees to radians conversion

if (argc = length(ARGS)) != 6
  print("\nUsage: jjj $(basename(@__FILE__)) streamflow 1972060100 00.CHRTOUT_GRID1 4747 com pos\n")
  exit(1)
end

fila = ARGS[1] * "." * ARGS[2] * ARGS[3] * "." * ARGS[4] * ".231.535.nc" ; vala = ncread(fila, "tmp", start=[1,1,1], count=[-1,-1,-1])
filb = ARGS[1] * "." * ARGS[2] * ARGS[3] * "." * ARGS[4] * ".274.564.nc" ; valb = ncread(filb, "tmp", start=[1,1,1], count=[-1,-1,-1])
filc = ARGS[1] * "." * ARGS[2] * ARGS[3] * "." * ARGS[4] * ".301.586.nc" ; valc = ncread(filc, "tmp", start=[1,1,1], count=[-1,-1,-1])
fild = ARGS[1] * "." * ARGS[2] * ARGS[3] * "." * ARGS[4] * ".309.593.nc" ; vald = ncread(fild, "tmp", start=[1,1,1], count=[-1,-1,-1])
file = ARGS[1] * "." * ARGS[2] * ARGS[3] * "." * ARGS[4] * ".310.596.nc" ; vale = ncread(file, "tmp", start=[1,1,1], count=[-1,-1,-1])

filf = ARGS[1] * "." * ARGS[2] * ARGS[3] * "." * ARGS[4] * ".160.434.nc" ; valf = ncread(filf, "tmp", start=[1,1,1], count=[-1,-1,-1])
filg = ARGS[1] * "." * ARGS[2] * ARGS[3] * "." * ARGS[4] * ".000.000.nc" ; valg = ncread(filg, "tmp", start=[1,1,1], count=[-1,-1,-1])

ndat = length(vala)
for a = 1:ndat
  if vala[a] != MISS && valb[a] != MISS && valc[a] != MISS && vald[a] != MISS && vale[a] != MISS
#   vala[a] += valb[a] + valc[a] + vald[a] + vale[a]
#   valz = true
    if valf[a] != MISS && valg[a] != MISS
      vala[a] += valg[a] - valf[a]
    else
      vala[a] = MISS
    end
  else
    vala[a] = MISS
  end
end

filb = ARGS[1] * "." * ARGS[2] * ARGS[3] * "." * ARGS[4] * "." * ARGS[5] * "." * ARGS[6] * ".nc"
cp(fila,      filb; force=true)
ncwrite(vala, filb, "tmp", start=[1,1,1], count=[-1,-1,-1])
exit(0)
