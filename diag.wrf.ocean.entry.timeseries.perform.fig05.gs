* This script is designed to plot cyclone location and intensity (colour code).
* It can be executed using a command like
*
*     grads -bpc "diag.wrf.ocean.entry.timeseries.perform 2019-05-31 Rpest_nwm_DOMAIN_East.FORCING.era_2019-03-31_000214.remsvd prep.nnet.elu"
*
* - RD November 2012

function plot(args)
datea = subwrd(args,1)
stema = subwrd(args,2)
tempa = subwrd(args,3)
if (tempa != "") ; stemb = stema"."tempa ; endif

"sdfopen "stema".nc" ; "q file"
ret  = sublin(result,5)
tims = subwrd(ret,12)
lats = subwrd(ret, 6)
lons = subwrd(ret, 3)

"run gui_getdate_clim 1 "datea"-00"
ret  = sublin(result,1)
tind = subwrd(ret, 1)

c = 1
filb = stema".txt"
filestat = read(filb)
while (sublin(filestat,1) = 0 & sublin(filestat,2) != "")
  if (c < 10) ; tail.c = "0"c ; else ; tail.c = c ; endif
  line = sublin(filestat,2)
  a = 1 ; while (substr(line,a,1) != "[") ; a = a + 1 ; endwhile ; a = a + 1 + 8
  b = a ; while (substr(line,b,1) != "]") ; b = b + 1 ; endwhile ; b = b - a
  riv.c =        substr(line,a,b) ; stn.c = substr(line,a-8,7)   ; c = c + 1
  filestat = read(filb)
endwhile
filestat = close(filb) ; riv.c = "All Stations"
if (c < 10) ; tail.c = "0"c ; else ; tail.c = c ; endif
*if (c =  2) ; c = 1 ; endif

lef = 1.0 ; rig = 7.5 ; mid = (lef + rig) / 2
ypic = 4 ; string = "1.0 7.2 1.2 "ypic ; inner_decomp(string)
a = 1 ; while (a <= ypic) ; b = ypic - a + 1 ; low.b = _retlef.a ; hig.b = _retrig.a ; a = a + 1 ; endwhile
"set lwid 20 14"
"set lwid 22 14"
"set rgb 55   0 200 200 100"
"set rgb 66   0 255   0 100"
"set rgb 88 240 130  40 100"
"set rgb 99 160   0 200 100"

"sdfopen "stemb".nc"

z = 1 ; while (z <= c)
  "clear" ; "set datawarn off"
  "set t 2" ; "set y 1" ; "set x 1" ; "d abs(para)" ; pind = subwrd(result,4)
  if (z < c)
    "set gxout stat"
    "set t 1 "tims ; "set y "z ; "set x    1" ; "d flow+0*hydt" ; ret = sublin(result,8) ; max = subwrd(ret,5)
                      vran = 5000 ; vint = 1000
    if (max < 4000) ; vran = 4000 ; vint =  800 ; endif
    if (max < 3000) ; vran = 3000 ; vint =  500 ; endif
    if (max < 2000) ; vran = 2000 ; vint =  400 ; endif
    if (max < 1000) ; vran = 1000 ; vint =  200 ; endif
    if (max <  500) ; vran =  500 ; vint =  100 ; endif
    if (max <   10) ; vran =  2.0 ; vint =  0.5 ; endif
    "set gxout line"
    "set parea "lef" "rig" 7.9 10.5"
    "set mproj off" ; "set grid off" ; "set mpdraw off"
    "set xlab on" ; "set ylab  on"
    "set vrange 0 "vran ; "set ylint "vint
*   "set vrange -2 4" ; "set ylint "vint
    "set t 2" ; "set y 1" ; "set x 1" ; "d abs(para)" ; pind = subwrd(result,4)
    "set cthick 12" ; "set line 1 1 12" ; "set string 1 c 8" ; "set missconn on"
    "set ccolor  1" ; "set cstyle 1" ; "set cmark 0" ; "set grads off" ; "set t 1 "tims ; "set y "z ; "set x    1" ; "d hydt"
    "set ccolor  8" ; "set cstyle 1" ; "set cmark 0" ; "set grads off" ; "set t 1 "tims ; "set y "z ; "set x    1" ; "d flow+0*hydt"
    "set ccolor  9" ; "set cstyle 1" ; "set cmark 0" ; "set grads off" ; "set t 1 "tims ; "set y "z ; "set x "pind ; "d flow+0*hydt"
    "set ccolor 15" ; "set cstyle 1" ; "set cmark 0" ; "set grads off" ; "set t 1 "tims ; "set y "z ; "set x "pind ; "d flow.2+0*hydt"
    "set cthick 22" ; "set line 1 1 22"
    "set ccolor 55" ; "set cstyle 1" ; "set cmark 0" ; "set grads off" ; "set t 1 "tims ; "set y "z ; "set x    1" ; "d maskout(maskout(0*hydi,2.5+hydi),-1.5-hydi)"
    "set ccolor 66" ; "set cstyle 1" ; "set cmark 0" ; "set grads off" ; "set t 1 "tims ; "set y "z ; "set x    1" ; "d maskout(maskout(0*hydi,4.5+hydi),-3.5-hydi)"
    "set cthick 12" ; "set line 1 1 12"
*   "set strsiz 0.19" ; "set string 1 c 6" ; "draw string "mid-0.0" 10.7 "riv.z" flow (m`a3`n/s)"
*   "set strsiz 0.12" ; "set string 1 l 6" ; "draw string 1.1 10.3 "stn.z
    "q gr2xy "tind"    0" ; xa = subwrd(result,3) ; ya = subwrd(result,6)
    "q gr2xy "tind" "vran ; xb = subwrd(result,3) ; yb = subwrd(result,6)
    "set line 1 2 4" ; "draw line "xa" "ya" "xb" "yb
  else
    "set strsiz 0.22" ; "set string 1 c 6" ; "draw string "mid-0.0" 7.9 All Stations"
  endif

  a = 1 ; b = 1
  stat001 = ""
  stat107 = ""
  statnnn = ""
  while (a < 5)
    if (a = 1) ; lab = "a" ; rng =    "0.0    1.0" ; yint =   "0.2" ; lab = "b) Nash-Sutcliffe Eff." ; parc = "tmpa" ; form = "%5.2f" ; endif
    if (a = 2) ; lab = "b" ; rng =    "0.0    1.0" ; yint =   "0.2" ; lab = "c) Pearson Corr."       ; parc = "tmpb" ; form = "%5.2f" ; endif
    if (a = 3) ; lab = "c" ; rng =    "0.0  600.0" ; yint = "200.0" ; lab = "d) RMSD (m`a3`n/s)"     ; parc = "tmpd" ; form = "%4.0f" ; endif
    if (a = 4) ; lab = "d" ; rng = "-250.0  250.0" ; yint = "100.0" ; lab = "e) Bias (m`a3`n/s)"     ; parc = "tmpe" ; form = "%4.0f" ; endif

*   "set parea "lef" "rig" "low.a" "hig.a
*   "set mproj off" ; "set grid off" ; "set mpdraw off"
*   "set xlopts 1 5 0.10"
*   "set ylopts 1 5 0.10" ; "set ylpos 0 l"
*   "set xlab off" ; if (a = ypic) ; "set xlab on" ; endif
*   "set ylab  on"
*   "set vrange "rng ; "set ylint "yint
*   "set cthick 8" ; "set line 1 1 8" ; "set string 1 c 8"
*   "set ccolor 1" ; "set cstyle 1" ; "set cmark 0" ; "set gxout line" ; "set grads off"
*   if (z < c) ; "set t 1" ; "set y "z ; "set x 1 "lons ; "d "parc                                                            ; endif
    if (z < c) ; "set t 1" ; "set y "z ; "set x 1"      ; "d "parc     ; pvak = subwrd(result,4) ; pval = math_format(form, pvak) ; endif
    if (z < c) ; "set t 1" ; "set y "z ; "set x   "pind ; "d "parc     ; pvak = subwrd(result,4) ; pvar = math_format(form, pvak) ; endif
    if (z < c) ; "set t 1" ; "set y "z ; "set x   "pind ; "d "parc".2" ; pvak = subwrd(result,4) ; pvas = math_format(form, pvak) ; endif
*   "set ccolor 1" ; "set cstyle 1" ; "set cmark 0" ; "set gxout line" ; "set grads off"
*   if (z = c) ; "set t "b ; "set y 1" ; "set x 1 "lons ; "d parb"                                                            ; endif
    if (z = c) ; "set t "b ; "set y 1" ; "set x 1"      ; "d parb"     ; pvak = subwrd(result,4) ; pval = math_format(form, pvak) ; endif
    if (z = c) ; "set t "b ; "set y 1" ; "set x   "pind ; "d parb"     ; pvak = subwrd(result,4) ; pvar = math_format(form, pvak) ; endif
    if (z = c) ; "set t "b ; "set y 1" ; "set x   "pind ; "d parb.2"   ; pvak = subwrd(result,4) ; pvas = math_format(form, pvak) ; endif
*   "set strsiz 0.19" ; "set string 1 c  6" ; "draw string "mid+0.2" "hig.a+0.2" "lab
*   "set strsiz 0.19" ; "set string 8 l 12" ; "draw string "lef-0.0" "hig.a+0.2" "pval
*   "set strsiz 0.19" ; "set string 9 r 12" ; "draw string "rig-0.0" "hig.a+0.2" "pvar
    stat001 = stat001" "pval
    stat107 = stat107" "pvar
    statnnn = statnnn" "pvas

*   a = 1 ; while (a < 5) ; "set t 1" ; "set y "z ; "set x "a
*     "d tmpa" ; ret = sublin(result,8) ; val = subwrd(ret,5) ; nse = math_format("%5.2f",val) ; "d tmpb" ; ret = sublin(result,8) ; val = subwrd(ret,5) ; cor = math_format("%5.2f",val)
*     "d tmpd" ; ret = sublin(result,8) ; val = subwrd(ret,5) ; rms = math_format("%4.0f",val) ; "d tmpe" ; ret = sublin(result,8) ; val = subwrd(ret,5) ; bia = math_format("%4.0f",val)
*     line.a = nse" "cor" "rms" "bia
*   a = a + 1 ; endwhile

    a = a + 1 ; b = b + 1
*   if (a = 3)
*     "set t "b
*     "set ccolor 1" ; "set cstyle 2" ; "set cmark 0" ; "set gxout line" ; "set grads off"
*     if (z < c) ; "set t 1" ; "set y "z ; "set x 1 "lons ; "d tmpc" ; endif
*     if (z = c) ; "set t "b ; "set y 1" ; "set x 1 "lons ; "d parb" ; endif
*     b = b + 1
*   endif
*   if (a = 5)
*     "q w2xy     1  0" ; xa = subwrd(result,3) ; ya = subwrd(result,6)
*     "q w2xy "lons" 0" ; xb = subwrd(result,3) ; yb = subwrd(result,6)
*     "set line  1 1 4" ; "draw line "xa" "ya" "xb" "yb
*   endif
*   lo = subwrd(rng,1) ; "q w2xy   1.3  "lo ; xa = subwrd(result,3) ; ya = subwrd(result,6)
*   hi = subwrd(rng,2) ; "q w2xy   1.3  "hi ; xb = subwrd(result,3) ; yb = subwrd(result,6)
*   "set line 88 1 20" ; "draw line "xa" "ya" "xb" "yb
*   lo = subwrd(rng,1) ; "q w2xy "pind" "lo ; xa = subwrd(result,3) ; ya = subwrd(result,6)
*   hi = subwrd(rng,2) ; "q w2xy "pind" "hi ; xb = subwrd(result,3) ; yb = subwrd(result,6)
*   "set line 99 1 20" ; "draw line "xa" "ya" "xb" "yb
  endwhile
* "set parea off"

  if (z < c)
*    "set strsiz 0.19" ; "set string 1 c 6" ; "draw string "mid-0.0" 10.7 "riv.z" flow (m`a3`n/s)"
*    "set strsiz 0.12" ; "set string 1 l 6" ; "draw string 1.1 10.3 "stn.z
    xa = 1.2 ;                 ya =  9.7 ; "set strsiz 0.12" ; "set string 1 l 6" ; "draw string "xa" "ya" "stn.z
                               ya = 10.0 ; "set strsiz 0.12" ; "set string 1 l 6" ; "draw string "xa" "ya" "riv.z
                               ya = 10.3 ; "set strsiz 0.12" ; "set string 1 l 6" ; "draw string "xa" "ya" (m`a3`n/s)"
    xa = 6.1 ; xb = xa + 0.1 ; ya =  9.7 ; "set line 15 1 22" ; "draw line "xa" "ya" "xb" "ya
                               ya =  9.9 ; "set line  9 1 22" ; "draw line "xa" "ya" "xb" "ya
                               ya = 10.1 ; "set line  8 1 22" ; "draw line "xa" "ya" "xb" "ya
                               ya = 10.3 ; "set line  1 1 22" ; "draw line "xa" "ya" "xb" "ya
*   xa = 5.9 ; xb = 6.0 ;      ya = 10.3 ; "set line 55 1 22" ; "draw line "xa" "ya" "xb" "ya
*   xa = 6.5 ; xb = 6.6 ;      ya = 10.3 ; "set line 66 1 22" ; "draw line "xa" "ya" "xb" "ya
    xa = 6.5 ;                 ya =  9.7 ; "set strsiz 0.12" ; "set string 1 l 6" ; "draw string "xa" "ya" NN232"
                               ya =  9.9 ; "set strsiz 0.12" ; "set string 1 l 6" ; "draw string "xa" "ya" PEST"
                               ya = 10.1 ; "set strsiz 0.12" ; "set string 1 l 6" ; "draw string "xa" "ya" Default"
                               ya = 10.3 ; "set strsiz 0.12" ; "set string 1 l 6" ; "draw string "xa" "ya" HYDAT" ;* ("
*   xa = 6.1 ;                 ya = 10.3 ; "set strsiz 0.12" ; "set string 1 l 6" ; "draw string "xa" "ya" ice"
*   xa = 6.7 ;                 ya = 10.3 ; "set strsiz 0.12" ; "set string 1 l 6" ; "draw string "xa" "ya" estim)"
    xa = 5.8 ;                 ya =  9.7 ; "set strsiz 0.12" ; "set string 1 r 6" ; "draw string "xa" "ya" "statnnn
                               ya =  9.9 ; "set strsiz 0.12" ; "set string 1 r 6" ; "draw string "xa" "ya" "stat107
                               ya = 10.1 ; "set strsiz 0.12" ; "set string 1 r 6" ; "draw string "xa" "ya" "stat001
                               ya = 10.3 ; "set strsiz 0.12" ; "set string 1 r 6" ; "draw string "xa" "ya" NSE`3 `0 COR`3 `0 RMS`3 `0 BIAS"
  endif
* "set strsiz 0.19" ; "set string 1 c 6" ; "draw string  4.3  0.6 PEST iteration"
  plot = "plot."stema"."tail.z".png"
  say "printim "plot" png white x1700 y2200"
      "printim "plot" png white x1700 y2200"
  z = z + 1
* "quit"
endwhile
"close 1"
"quit"


function inner_decomp(args)
  lef = subwrd(args,1)
  rig = subwrd(args,2)
  wid = subwrd(args,3)
  num = subwrd(args,4)
  _retmid.1   = lef + wid / 2
  _retmid.num = rig - wid / 2
  a = 2
  while (a < num)
    _retmid.a = (_retmid.num * (a-1) + _retmid.1 * (num-a)) / (num - 1)
    a = a + 1
  endwhile

  a = 1
  while (a <= num)
    _retlef.a = _retmid.a - wid / 2
    _retrig.a = _retmid.a + wid / 2
    a = a + 1
  endwhile
return
