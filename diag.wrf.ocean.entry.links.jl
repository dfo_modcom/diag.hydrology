#=
 = Create local links to all files in a directory and any subdirectories
 = (note that a link is created only to the last file found, where two
 =  files from different subdirs have the same name) - RD April 2020.
 =#

using My, Printf
const ADYR             = 0                              # number of years to add before linking

if (argc = length(ARGS)) != 1
  print("\nUsage: jjj $(basename(@__FILE__)) ../x.2020-04-07_start_1933-06-01\n\n")
  exit(1)
end

function walkdir(dir)
  absdir = abspath(dir)
  files  = readdir(absdir)
  for file in files
    full = joinpath(absdir, file)
    if isfile(full) && endswith(full, ".CHRTOUT_GRID1")
      ADYR != 0 && (file = @sprintf("%d", parse(Int, file[1:4]) + 36) * file[5:end])
      if islink(file)
        print("$file link already exists, but replacing with a link to $full\n")
        rm(file)
      end
      symlink(full, file)
    elseif isdir(full)
      walkdir(full)
    end
  end
end

walkdir(ARGS[1])
