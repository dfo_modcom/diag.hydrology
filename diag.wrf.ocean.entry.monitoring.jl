#=
 = Write a timeseries of some variable at a given location based on observations (say, at the
 = Moses-Saunders Dam) and save this as a separate netcdf file for plotting - RD January 2021.
 =#

using My, Printf, NetCDF

const DELT             = 24.0                           # timestep (hours)
const MISS             = -9999.0                        # generic missing value
const D2R              = 3.141592654 / 180.0            # degrees to radians conversion
const RANGE            = -7.0:0.01:7.0                  # allowable range in Gaussian values
const IOLS             = false                          # calibrate by IOLS (otherwise INFR)

if (argc = length(ARGS)) != 6
  print("\nUsage: jjj $(basename(@__FILE__)) variable first_date filename_ending num_dates y_gridbox x_gridbox\n")
  print(  "e.g.,  jjj $(basename(@__FILE__)) streamflow 1933060100 00.CHRTOUT_GRID1 1601 231 535\n\n")
  exit(1)
end
vnam =            ARGS[1]
fbeg =            ARGS[2]
fend =            ARGS[3]
ndat = parse(Int, ARGS[4])
yloc = parse(Int, ARGS[5])
xloc = parse(Int, ARGS[6])
tima = Array{Float64}(undef,0)
vala = Array{Float64}(undef,0)
timb = Array{Float64}(undef,0)
valb = Array{Float64}(undef,0)

fila = "../moses-saunders/2019-06-26_Moses_Saunders_Dam_daily_discharge_1935-2020.txt"
print("\n") ; fpa = My.ouvre(fila, "r") ; lins = readlines(fpa) ; close(fpa)

for line in lins                                                              # convert discharge to m3/s
  ents = split(line)
  if length(ents) == 5 && ents[1] == "USGS" && (ents[3][1:2] == "19" || ents[3][1:2] == "20")
    tmpt = parse(Float64, ents[3][1:4] * ents[3][6:7] * ents[3][9:10]) * 100
    tmpv = parse(Float64, ents[4]) * 0.0283168
    push!(tima, tmpt) ; push!(vala, tmpv)
  end
end
print("found $(length(vala)) discharge values\n")

dnow = fbeg                                                                   # store a nonmissing discharge
for a = 1:ndat                                                                # for every desired date that
  global dnow                                                                 # can be matched in fila
  tnow = parse(Float64, dnow)
  tind = findfirst(tima .== tnow)
  valu = tind == nothing ? MISS : vala[tind]
  time = My.datesous("1900010100", dnow, "hr")
  push!(valb, valu) ; push!(timb, time)
  dnow = My.dateadd(dnow, DELT, "hr")
end

fnam = ARGS[1] * "." * ARGS[2] * ARGS[3] * "." * ARGS[4] * "." * ARGS[5] * "." * ARGS[6] * ".nc"
nccreer(      fnam, length(valb), 1, 1, MISS)
ncwrite(valb, fnam,  "tmp", start=[1,1,1], count=[-1,-1,-1])
ncwrite(timb, fnam, "time", start=[1],     count=[-1])
ncputatt(     fnam, "time", Dict("units" => "hours since 1900-01-01 00:00:0.0"))
exit(0)
