#=
 = Calibrate WRFHydro using PEST by ANL parallel SLURM method with ANL calibration params.
 = Begin by setting up namelists for the domain, forcing, and period of interest.  Restart
 = files are sought from a specified ARGS directory.  Multiple launches are performed for
 = the target computer system (as determined by user name) in a subdirectory - RD Aug 2023.
 =#

using My, Printf

const RSAV             = false                          # save daily restarts if true
const PAGN             = 6                              # number of PEST agents (one less than total number of job nodes)

if     in("WRFSDOM", keys(ENV)) && ENV["WRFSDOM"] == "_AuxF"                  # define restart domain endings
  const ENDA           = "1"
  const ENDB           = "2"
elseif in("WRFSDOM", keys(ENV)) && ENV["WRFSDOM"] == "_Cali"
  const ENDA           = "1"
  const ENDB           = "2"
elseif in("WRFSDOM", keys(ENV)) && ENV["WRFSDOM"] == "_East"
  const ENDA           = "1"
  const ENDB           = "2"
else
  const ENDA           = "1"
  const ENDB           = "1"
end

if (argc = length(ARGS)) != 4 && (argc = length(ARGS)) != 5
  print("\nUsage: jjj $(basename(@__FILE__)) domain           forcing          start_date days [restart_dir]\n")
  print(  " e.g.: jjj $(basename(@__FILE__)) DOMAIN_full_full FORCING.wrf.mean 1901-01-01 1095 [. or ../RESTART]\n\n")
  exit(1)
end
doma =               ARGS[1]                                                  # start in the new launch subdir
forc =               ARGS[2]
dsta =               ARGS[3]  ; (yr, mo, dy) = split(dsta, "-")
dnum =  parse(Int64, ARGS[4]) ; dirl  = @sprintf("pest_anl_%s_%s_%s_%6d", doma, forc, dsta, dnum)
argc == 5 && (dirl *= ".res") ; dirl  = replace(dirl, ' ' => '0')
argc == 5 && (rsta = ARGS[5]  ; rsta *= rsta[end] == '/' ? "" : "/")
argc == 4 && (tmpa = "SPINUP." * forc[9:11] * "/res/" ; isdir("../" * tmpa) && (rsta = tmpa))
isdir(dirl)    && error("$dirl already exists (please rename)\n\n")
mkdir(dirl)    ; print("\ncd $dirl\n\n") ; cd(dirl)
isdir("input") && error("$dirl/input already exists\n\n")
mkdir("input") ; print("\ncd input\n\n") ; cd("input")

ENV["USER"] == "riced"  && (dirw =      "/project/6000950/riced/work/wrf_hydro_nwm_public/trunk/NDHMS/Run/")
ENV["USER"] == "rid000" && (dirw = "/gpfs/fs7/dfo/bioios/rid000/work/wrf_hydro_nwm_public/trunk/NDHMS/Run/")
fils = ["CHANPARM.TBL", "GENPARM.TBL", "OTHER.TBL", "MPTABLE.TBL", "SOILPARM.TBL"]
for file in fils
  full = dirw * file
  print("cp $full .\n")                                                       # then copy the model and tables
  cp(full, file; force = true, follow_symlinks = true)                        # and domain and link the forcing
end

dirw = "../../../" * forc ; !isdir(dirw) && error("$dirw is missing\n\n") ; symlink(dirw, "FORCING")
dirw = "../../../" * doma ; !isdir(dirw) && error("$dirw is missing\n\n") ; symlink(dirw,  "DOMAIN")
fils = ["Fulldom_hires.nc", "GEOGRID_LDASOUT_Spatial_Metadata.nc", "GWBASINS.nc", "GWBUCKPARM.nc", "LAKEPARM.nc", "geogrid.nc", "hydro2dtbl.nc", "soil_properties.nc", "wrfinput_d01.nc"]
for file in fils
  full = dirw * "/" * file
  if isfile(full)
    print("copy $full .\n")
    cp(full, file; force = true, follow_symlinks = true)
  else
    print("miss $full\n")
  end
end
lake = isfile("LAKEPARM.nc") ? true : false

if @isdefined(rsta) && argc == 4
  rstb = @sprintf("HYDRO_RST.1903-%s-%s_00:00_DOMAIN%s",   mo, dy, ENDA)      # (get restart files, if needed)
  rstc = @sprintf(        "RESTART.1903%s%s00_DOMAIN%s",   mo, dy, ENDB)
  tmpf = "../../../" * rsta * rstb ; !isfile(tmpf) && error("$tmpf is missing\n\n") ; cp(tmpf, rstb)
  tmpf = "../../../" * rsta * rstc ; !isfile(tmpf) && error("$tmpf is missing\n\n") ; cp(tmpf, rstc)
end
if @isdefined(rsta) && argc == 5
  rstb = @sprintf("HYDRO_RST.%s-%s-%s_00:00_DOMAIN%s", yr, mo, dy, ENDA)      # (get restart files, if needed)
  rstc = @sprintf(        "RESTART.%s%s%s00_DOMAIN%s", yr, mo, dy, ENDB)
  tmpf = "../../" * rsta * rstb ; !isfile(tmpf) && error("$tmpf is missing\n\n") ; cp(tmpf, rstb)
  tmpf = "../../" * rsta * rstc ; !isfile(tmpf) && error("$tmpf is missing\n\n") ; cp(tmpf, rstc)
end

#length(findall("era", forc)) > 0 && (fstp = 10800)                            # and define the forcing timestep (s)
#length(findall("nar", forc)) > 0 && (fstp = 21600)
#length(findall("wrf", forc)) > 0 && (fstp = 21600)
fstp = 21600

function namelists()
  form  = "&HYDRO_nlist\n"                                                    # write the hydro.namelist file
  form *= "sys_cpl                = 1\n"                                      # COUPLING: 1=HRLDAS (offline Noah-LSM), 2=WRF, 3=NASA/LIS, 4=CLM
  form *= "GEO_STATIC_FLNM        = \"./geogrid.nc\"\n"                       # INPUT DATA: land surface model gridded input data file (e.g.: "geo_em.d01.nc")
  form *= "GEO_FINEGRID_FLNM      = \"./Fulldom_hires.nc\"\n"                 # high-resolution routing terrain input data file (e.g.: "Fulldom_hires.nc")
  form *= "HYDROTBL_F             = \"./hydro2dtbl.nc\"\n"                    # spatial hydro parameters file (e.g.: "hydro2dtbl.nc") created if nonexist
  form *= "LAND_SPATIAL_META_FLNM = \"./GEOGRID_LDASOUT_Spatial_Metadata.nc\"\n"  # spatial metadata file for land surface grid. (e.g.: "GEOGRID_LDASOUT_Spatial_Metadata.nc")
   @isdefined(rsta) && (form *= "!ESTART_FILE is from   = \"$rsta\"\n")       # add a comment about where the restart files were copied from
   @isdefined(rsta) && (form *= "RESTART_FILE           = \"$rstb\"\n")       # restart file if starting from restart...comment out with '!' if not...
  form *= "IGRID      =    1\n"                                               # SETUP OPTIONS: domain or nest number identifier (integer)
   RSAV && (form *= "rst_dt     = 1440\n")                                    # restart file write frequency (minutes) -99999 will output restarts on the first day of the month only.
  !RSAV && (form *= "rst_dt     = 57600000\n")                                #                                     (57600000 will output restarts as infrequently as possible)
  form *= "rst_typ    =    1\n"                                               # reset LSM soil states using high-res routing restart file (1=overwrite, 0=no overwrite) ONLY if routing
  form *= "rst_bi_in  =    0\n"                                               # 0: use netcdf input restart file format 1: use parallel io for reading multiple restart files, 1 per core
  form *= "rst_bi_out =    0\n"                                               # 0: use netcdf output restart file format 1: use parallel io for outputting multiple restart files, 1 per core
  form *= "RSTRT_SWC  =    0\n"                                               # set restart accumulation variables to 0 (0=no reset, 1=yes reset to 0.0)
  form *= "GW_RESTART =    1\n"                                               # baseflow/bucket model initialization...(0=cold start from table, 1=restart file)
  form *= "out_dt                      = 180\n"                               # OUTPUT CONTROL: file write frequency...(minutes)
  form *= "SPLIT_OUTPUT_COUNT          =   1\n"                               # number of times in each output file (1 for CHANNEL ROUTING ONLY/CALIBRATION SIMS/COUPLED TO WRF)
  form *= "order_to_write              =   1\n"                               # minimum stream order to output to netcdf point file...(integer) (lower value produces more output)
  form *= "io_form_outputs             =   4\n"                               # I/O routines: 0=deprecated, 1=scl/off/compress, 2=scl/off/NOcompress, 3=compress only, 4=no scl/off/compress
  form *= "io_config_outputs           =   0\n"                               # Realtime run config 0=all (default), 1=analys, 2/3/4=short/med/long-rng, 5=retrospec, 6=diag (1-4 combined)
  form *= "t0OutputFlag                =   0\n"                               # Option to write output files at time 0 (restart cold start time): 0=no, 1=yes (default)
  form *= "output_channelBucket_influx =   0\n"                               # channel+bucket influx (if UDMP_OPT=1 out_dt=NOAH_TIMESTEP): 2=qSfcLatRunoff&qBucket+qBtmVertRunoff_toBucket
  form *= "CHRTOUT_DOMAIN              =   0\n"                               # Netcdf point timeseries output at all channel points (1d):                0 = no output, 1 = output
  form *= "CHANOBS_DOMAIN              =   0\n"                               # Netcdf point timeseries at forecast/gage points (defined in Routelink):   0 = no output, 1 = output
  form *= "CHRTOUT_GRID                =   0\n"                               # Netcdf grid of channel streamflow values (2d), but not for reach routing: 0 = no output, 1 = output
  form *= "LSMOUT_DOMAIN               =   0\n"                               # Netcdf grid of variables passed between LSM and routing components (2d):  0 = no output, 1 = output
  form *= "RTOUT_DOMAIN                =   0\n"                               # Netcdf grid of terrain routing variables on routing grid (2d):            0 = no output, 1 = output
  form *= "output_gw                   =   0\n"                               # Netcdf GW output:                                                         0 = no output, 1 = output
   lake && (form *= "outlake            =   0\n")                             # Netcdf grid of lake values (1d):                                          0 = no output, 1 = output
  !lake && (form *= "outlake            =   0\n")                             # Netcdf grid of lake values (1d):                                          0 = no output, 1 = output
  form *= "frxst_pts_out               =   1\n"                               # ASCII text file of forecast points or gage points (defined in Routelink): 0 = no output, 1 = output
  form *= "NSOIL            =    4\n"                                         # PHYSICS OPTIONS: number of soil layers (integer)
  form *= "ZSOIL8(1)        =   -0.10\n"                                      # depth of the bottom of  first layer... (meters)
  form *= "ZSOIL8(2)        =   -0.40\n"                                      # depth of the bottom of second layer... (meters)
  form *= "ZSOIL8(3)        =   -1.00\n"                                      # depth of the bottom of  third layer... (meters)
  form *= "ZSOIL8(4)        =   -2.00\n"                                      # depth of the bottom of fourth layer... (meters)
  form *= "DXRT             = 2000.0\n"                                       # grid spacing of the terrain routing grid...(meters)
  form *= "AGGFACTRT        =   25\n"                                         # multiple between the land model grid and the terrain routing grid...(integer)
  form *= "DTRT_CH          =   30\n"                                         # channel routing model timestep...(seconds)
  form *= "DTRT_TER         =   30\n"                                         # terrain routing model timestep...(seconds)
  form *= "SUBRTSWCRT       =    1\n"                                         # activate subsurface routing...(0=no, 1=yes)
  form *= "OVRTSWCRT        =    1\n"                                         # activate surface overland flow routing...(0=no, 1=yes)
  form *= "rt_option        =    1\n"                                         # overland flow routing option: 1=Seepest Descent (D8) 2=CASC2D (not active)
  form *= "CHANRTSWCRT      =    1\n"                                         # activate channel routing...(0=no, 1=yes)
  form *= "channel_option   =    3\n"                                         # channel routing option: 1=Muskingam-reach, 2=Musk.-Cunge-reach, 3=Diff.Wave-gridded
# form *= "route_link_f     = \"./Route_Link.nc\"\n"                          # reach file for reach-based routing options (e.g.: "Route_Link.nc")
  form *= "compound_channel = .FALSE.\n"                                      # activate compound channel (if channel_option=2, with reach-based routing and UDMP=1) Default=.FALSE.
  lake && (form *= "route_lake_f     = \"./LAKEPARM.nc\"\n")                  # lake parameter file (e.g.: "LAKEPARM.nc") REQUIRED if lakes are on
  form *= "GWBASESWCRT      =    1\n"                                         # activate baseflow bucket model...(0=none, 1=exp. bucket, 2=pass-through)
  form *= "gwbasmskfil      = \"./GWBASINS.nc\"\n"                            # Groundwater/baseflow mask on land surface grid, required for active baseflow model (1/2) and UDMP_OPT=0
  form *= "GWBUCKPARM_file  = \"./GWBUCKPARM.nc\"\n"                          # Groundwater bucket parameter file (e.g.: "GWBUCKPARM.nc")
  form *= "UDMP_OPT         =    0\n"                                         # User defined mapping, such as NHDPlus: 0=no (default), 1=yes
# form *= "udmap_file       = \"./spatialweights.nc\"\n"                      # user-defined mapping file (e.g.: "spatialweights.nc")
  form *= "/\n"
  form *= "&NUDGING_nlist\n"                                                  # NUDGING PARAMS
  form *= "timeSlicePath          = \"./nudgingTimeSliceObs/\"\n"             # Path to the "timeslice" observation files
  form *= "nudgingParamFile       = \"./nudgingParams.nc\"\n"                 # Nudging parameter file
# form *= "nudgingLastObsFile     = \'/nudging/cold/start/file\'\n"           # Nudging restart file (default='' uses nudgingLastObs.YYYY-mm-dd_HH:MM:SS.nc AT RUN INITALIZATION)
  form *= "readTimesliceParallel  =  .TRUE.\n"                                # Parallel input of nudging timeslice observation files
  form *= "temporalPersistence    =  .TRUE.\n"                                # temporalPersistence defaults to true, only runs if necessary params are present
  form *= "nLastObs               =     480\n"                                # number of last (obs, modeled) pairs to save in nudgingLastObs for removal of bias (max length)
  form *= "persistBias            =  .TRUE.\n"                                # persist bias after the last observation (If using temporalPersistence the last obs persists by default)
  form *= "biasWindowBeforeT0     = .FALSE.\n"                                # An(F) vs Forecast(T) bias persistence (F=window ends at model time(moving),T=window ends at init=t0(fcst))
  form *= "maxAgePairsBiasPersist =       3\n"                                # If persistBias: Only use this many last (obs, modeled) pairs. (If Commented out, Default=-1*nLastObs)
  form *= "minNumPairsBiasPersist =       1\n"                                # If persistBias: The minimum number of last (obs, modeled) pairs, with age less than maxAgePairsBiasPersist
  form *= "invDistTimeWeightBias  =  .TRUE.\n"                                # If persistBias: give more weight to observations closer in time? (default=FALSE)
  form *= "noConstInterfBias      =  .TRUE.\n"                                # If persistBias: "No constructive interference in bias correction?", Reduce the bias adjustment...
  form *= "/\n"
  fpa = My.ouvre("hydro.namelist", "w") ; write(fpa, form) ; close(fpa)

  form  = "&NOAHLSM_OFFLINE\n"                                                # write the namelist.hrldas file
  form *= "HRLDAS_SETUP_FILE = \"./wrfinput_d01.nc\"\n"
  form *= "INDIR             = \"./FORCING\"\n"
  form *= "SPATIAL_FILENAME  = \"./soil_properties.nc\"\n"
  form *= "OUTDIR            = \"./\"\n"
  form *= "START_YEAR  = $yr\n"
  form *= "START_MONTH = $mo\n"
  form *= "START_DAY   = $dy\n"
  form *= "START_HOUR  = 00\n"
  form *= "START_MIN   = 00\n"
   @isdefined(rsta) && (form *= "!ESTART_FILENAME is from   = \"$rsta\"\n")
   @isdefined(rsta) && (form *= "RESTART_FILENAME_REQUESTED = \"$rstc\"\n")
  form *= "KDAY        =    $(ARGS[4])\n"                                     # simulation length in  days
# form *= "KHOUR       =    8\n"                                              # simulation length in hours
  form *= "DYNAMIC_VEG_OPTION                = 4\n"                           # Physics options (see the documentation for details)
  form *= "CANOPY_STOMATAL_RESISTANCE_OPTION = 1\n"
  form *= "BTR_OPTION                        = 1\n"
  form *= "RUNOFF_OPTION                     = 3\n"
  form *= "SURFACE_DRAG_OPTION               = 1\n"
  form *= "FROZEN_SOIL_OPTION                = 1\n"
  form *= "SUPERCOOLED_WATER_OPTION          = 1\n"
  form *= "RADIATIVE_TRANSFER_OPTION         = 3\n"
  form *= "SNOW_ALBEDO_OPTION                = 1\n"
  form *= "PCP_PARTITION_OPTION              = 1\n"
  form *= "TBOT_OPTION                       = 2\n"
  form *= "TEMP_TIME_SCHEME_OPTION           = 3\n"
  form *= "GLACIER_OPTION                    = 2\n"
  form *= "SURFACE_RESISTANCE_OPTION         = 4\n"
  form *= "FORCING_TIMESTEP        = $fstp\n"                                 # in seconds (10800 = 3h)
  form *= "NOAH_TIMESTEP           =  3600\n"                                 # in seconds ( 3600 = 1h)
  form *= "OUTPUT_TIMESTEP         = -9999\n"                                 # in seconds (21600 = 6h)
   RSAV && (form *= "RESTART_FREQUENCY_HOURS = 24\n")                         # Land surface model restart file write frequency
  !RSAV && (form *= "RESTART_FREQUENCY_HOURS = 960000\n")                     #                    (960000 hrs = 57600000 min)
  form *= "SPLIT_OUTPUT_COUNT      =    1\n"                                  # Split output after split_output_count output times
  form *= "NSOIL                   =    4\n"                                  # Soil layer specification
  form *= "soil_thick_input(1)     =    0.10\n"
  form *= "soil_thick_input(2)     =    0.30\n"
  form *= "soil_thick_input(3)     =    0.60\n"
  form *= "soil_thick_input(4)     =    1.00\n"
  form *= "ZLVL                    =   10.0\n"                                # Forcing data measurement height for winds, temp, humidity
  form *= "rst_bi_in               =    0\n"                                  # 0: use netcdf input restart file 1: use parallel io for reading multiple restart files (1 per core)
  form *= "rst_bi_out              =    0\n"                                  # 0: use netcdf output restart file 1: use parallel io for outputting multiple restart files (1 per core)
  form *= "/\n"
  form *= "&WRF_HYDRO_OFFLINE\n"                                              # Specification of forcing data:  1=HRLDAS-hr format, 2=HRLDAS-min format, 3=WRF, 4=Idealized,
  form *= "FORC_TYP                =    1\n"                                  # 5=Ideal w/ Spec.Precip., 6=HRLDAS-hrl y fomat w/ Spec. Precip., 7=WRF w/ Spec. Precip., 9=Channel-only,
  form *= "/\n"                                                               # 10=Channel+Bucket only forcing, see hydro.namelist output_channelBucket_influxes
  fpa = My.ouvre("namelist.hrldas", "w") ; write(fpa, form) ; close(fpa)
end

print("\n")
namelists()
cd("..")

ENV["USER"] == "riced"  && (dirw =      "/project/6000950/riced/work/wrf_hydro_nwm_public/trunk/NDHMS/Run/")
ENV["USER"] == "rid000" && (dirw = "/gpfs/fs7/dfo/bioios/rid000/work/wrf_hydro_nwm_public/trunk/NDHMS/Run/")
fils = ["wrf_hydro.exe"]
for file in fils
  full = dirw * file
  print("cp $full .\n")                                                       # then copy the model and scripts
  cp(full, file; force = true, follow_symlinks = true)
end

fsrc = fdst = "frxst_pts_out.ins" ; psrc = pdst = "wrftest.pst"
in("WRFSDOM", keys(ENV)) && ENV["WRFSDOM"] == "_AuxF" && (fsrc *= "_AuxF" ; psrc *= "_AuxF")
in("WRFSDOM", keys(ENV)) && ENV["WRFSDOM"] == "_Cali" && (fsrc *= "_Cali" ; psrc *= "_Cali")
in("WRFSDOM", keys(ENV)) && ENV["WRFSDOM"] == "_East" && (fsrc *= "_East" ; psrc *= "_East")
ENV["USER"] == "riced"  && (dirw =      "/project/6000950/riced/work/wrf_hydro_nwm_public_pest/anl/")
ENV["USER"] == "rid000" && (dirw = "/gpfs/fs7/dfo/bioios/rid000/work/wrf_hydro_nwm_public_pest/anl/")
fils = ["OTHER.TPL", fsrc, psrc, "wrftest.rmf", "runwrf1.sh", "runwrf2.sh"]
for file in fils
  full = dirw * file
  print("cp $full .\n")
  cp(full, file; force = true, follow_symlinks = true)
end
fsrc != fdst && mv(fsrc, fdst; force = true)
psrc != pdst && mv(psrc, pdst; force = true)

function gpscrun(launch::AbstractString)
  dira  = abspath(".")
  form  = "#!/usr/bin/bash\n"
  form *= "#SBATCH --export=USER,LOGNAME,HOME,MAIL,PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:.:/home/rid000/soft/pest/PEST.bin\n"
  form *= "#SBATCH --job-name=x.$dirl\n"
  form *= "#SBATCH --output=$dira" * "x.$dirl.jgeo\n"
  form *= "#SBATCH --account=dfo_bioios\n"
  form *= "#SBATCH --partition=standard\n"
  form *= "#SBATCH --time=24:00:00\n"
  form *= "#SBATCH --nodes=$(PAGN+1)\n"
  form *= "#SBATCH --ntasks-per-node=64\n"
# form *= "#SBATCH --cpus-per-task=8\n"
  form *= "#SBATCH --mem-per-cpu=3000M\n"
  form *= "#SBATCH --comment=\"image=registry.maze.science.gc.ca/ssc-hpcs/generic-job:ubuntu20.04\"\n"
# form *= "#SBATCH --comment=\"image=registry.maze.science.gc.ca/ssc-hpcs/generic-job:ubuntu20.04,bin_sh=bash\"\n"
  form *= "export SLURM_EXPORT_ENV=ALL\n"
  form *= "export ORDENV_SITE_PROFILE=20230503\n"
  form *= ". /fs/ssm/main/env/ordenv-boot-20230327.sh\n"
  form *= ". ssmuse-sh -x main/opt/openmpi/openmpi-4.1.2rc4--hpcx-2.10-mofed-5.x--gcc\n"
  form *= "cd $dira\n"
# form *= "rumpirun -np 64 ./wrf_hydro.exe\n"
  form *= "runwrf1.sh $PAGN $PAGN\n"
  fpa = My.ouvre(launch, "w") ; write(fpa, form) ; close(fpa)
  print("sbatch $launch\n")
#   run(`sbatch $launch`)
end

print("\n")
gpscrun("x." * dirl * ".sbatch")
print("\n")
exit(0)
