* This script is designed to plot a simple timeseries.
* It can be executed using a command like
*
*     grads -blc "plot.rivsum.monthly rivsum.1955_2020"
*
* - RD November 2012

function plot(args)
fila = subwrd(args,1)".nc"
filb = subwrd(args,1)".png"

"sdfopen "fila
"q file" ; ret = sublin(result,5) ; tims = subwrd(ret,12) ; "set t 1 "tims
if (tims = 24) ; "set t 8 20" ; endif
*if (tims > 365) ; "set t 210 366" ; endif
"set grads off"
"set grid off"
"set cmark 0"
"set vrange 0 40"
"set xlopts 1 3 0.18"
"set ylopts 1 3 0.18"
*"d smth9(smth9(smth9(tmp)))"
*"d tmp+9000"
"d tmp/1000"

"q gxinfo"
line3 = sublin(result,3)
line4 = sublin(result,4)
x1 = subwrd(line3,4)
x2 = subwrd(line3,6)
y1 = subwrd(line4,4)
y2 = subwrd(line4,6)
xmid = (x1 + x2) / 2.0
ymid = (y1 + y2) / 2.0
say x1" "x2" "y1" "y2" "xmid ;* 2 10.5 0.75 7.75 6.25

"set string 1 bc 5" ; "set strsiz 0.23"
*"draw string "xmid" "y2+0.2" WRF-Hydro at Quebec (mSv)"
"draw string "xmid" "y2+0.2" Bourgault-Koutitonsky at Quebec (mSv)"

say "gxprint "filb" png white x1100 y850"
    "gxprint "filb" png white x1100 y850"
"quit"
