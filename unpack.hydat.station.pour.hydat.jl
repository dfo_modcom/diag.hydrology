#=
 = Loop through the WRF-Hydro pour point data and identify the nearest HyDAT station
 = upstream whose catchment best overlaps with that of the pour point.  Also output a
 = list of all HyDAT stations that fall inside these WRF-Hydro catchments - RD Jan 2024.
 =#

using My, Printf, NetCDF, LibGEOS, GeoInterface

const FRAC             = 0.4                            # minimum fraction of WRF-Hydro area calculated from station / area calculated from pour point
const MISS             = -9999.0                        # generic missing value
const MIST             = @sprintf("%d", Int64(MISS))    # generic missing value string

if (argc = length(ARGS)) != 4
  print("\nUsage: jjj $(basename(@__FILE__)) ../hydat.validation/ hydat_valding_aall_1000_incl.txt hw ../SPINUP.wrf/run_DOMAIN_full_full_FORCING.wrf_1990-01-01_2004-12-30_3h_Severn_Hudson.txt\n\n")
  exit(1)
end
dira = ARGS[1]
fila = ARGS[1] * ARGS[2] ; fpa = My.ouvre(fila, "r", false) ; lina = readlines(fpa, keep = true) ; close(fpa)
filb =           ARGS[4] ; fpa = My.ouvre(filb, "r", false) ; linb = readlines(fpa, keep = true) ; close(fpa)
filc = ARGS[2][1:end-4] * "_" * ARGS[3] * "dt.txt"
@printf("\nfound %4d lines in %s\n", length(lina), fila)
@printf(  "found %4d lines in %s\n", length(linb), filb)

hydt = Array{Tuple{Float64, Float64, Float64, Float64, Float64, Int64, AbstractString}}(undef, 0)
for a = 1:length(lina)
  filh = dira * split(lina[a])[1] * ".sta"                                    # read the HyDAT station data
  if isfile(filh)                                                             # area = HydroSHEDS area UPLAND_SKM from station
    fpb  = My.ouvre(filh, "r", false) ; fins = readlines(fpb) ; close(fpb)    # hrea = HydroSHEDS area calculated from station
    hlat = parse(Float64, split(fins[ 6])[2])                                 # wrea = WRF-Hydro  area calculated from station
    hlon = parse(Float64, split(fins[ 7])[2])
    area = parse(Float64, split(fins[30])[2])
    if length(split(fins[30])) > 2
      hrea = parse(Float64, split(fins[30])[4])
    else
      hrea = MISS
    end
    wrea = parse(Float64, split(fins[90])[5])
    natr =                split(fins[93])[5]  == "Natural" ? 1 : 0
    push!(hydt, (hlat, hlon, area, hrea, wrea, natr, lina[a]))
  end
end
nydt = length(hydt)
@printf("found %4d HyDAT station positions\n", nydt)

function readshed(file::AbstractString)                                       # (parse an upstream catchment area;
  txt = "POLYGON(("                                                           #  buffer seeks to make valid geom)
  fpb = My.ouvre(file, "r", false) ; lins = readlines(fpb) ; close(fpb)
  for a = 2:length(lins)  pos = split(lins[a]) ; txt *= pos[2] * " " * pos[1] * ", "  end
  txt = txt[1:end-2] * "))"
  tmp = readgeom(txt)
  GeoInterface.buffer(tmp, 0)
end

vars = ["flow", "floi", "levl", "levi", "sedc", "sedi", "load", "loai"]       # and for each WRF-Hydro catchment
numa = numb = 0                                                               # check for HyDAT stations inside
fora = forb = ""                                                              # prea = WRF-Hydro area calculated from pour point
for a = 1:length(linb)
  global numa, numb, fora, forb
  temp = split(linb[a])
  name =                temp[1]
  plat = parse(Float64, temp[5])
  plon = parse(Float64, temp[6])
  filw = @sprintf("wrfh_%8s.sta",        name)          ; filw = replace(filw, ' ' => '0')
  fpc  = My.ouvre(filw, "r", false) ; linz = readlines(fpc) ; close(fpc)
  prea = parse(Float64, split(linz[90])[5])
  filw = @sprintf("wrfh_%8s.sta.%sshed", name, ARGS[3]) ; filw = replace(filw, ' ' => '0')
  shdw = readshed(filw)
  hyin = Array{Tuple{Float64, Float64, Float64, Float64, Float64, Int64, Float64, Float64, Float64, AbstractString}}(undef, 0)
  for b = 1:nydt
    (hlat, hlon, area, hrea, wrea, natr, line) = hydt[b]
    llpt = readgeom("POINT($hlon $hlat)")
    contains(shdw, llpt) && push!(hyin, (hlat, hlon, area, hrea, wrea, natr, plat, plon, prea, line))
  end
  nyin = length(hyin)
# numb += nyin

  maxa = -9e9                                                                 # identify the best HyDAT station to
  best = natb = 1                                                             # characterize each WRF-Hydro catchment
  for b = 1:nyin                                                              # but save all station locations for plotting
    (hlat, hlon, area, hrea, wrea, natr, plat, plon, prea, line) = hyin[b]    # (natb/natr = 0/1 for regulated/natural and
    natr == 0 && (natb = 0)                                                   #  b = -1/0 for pour point with no/yes HyDAT
    srea = (area + hrea + wrea) / 3                                           #       1/2 for best/other HyDAT station)
    if srea > maxa
      best = b
#     natb = natr
      maxa = srea
    end
  end
#=
  if nyin > 0                                                                 # copy best HyDAT to the WRF-Hydro station
    (hlat, hlon, area, hrea, wrea, natr, line) = hyin[best] ; hnam = split(line)[1]
    fild = ARGS[1] * hnam * ".nc"
    file = filw[1:13] * 
    print("cp $filh $fils\n") ; cp(filh, fils; force = true)
    filh = ARGS[1] * hnam * ".nc"
    filw = 
    for vnam in vars
      data = ncread(fild, vnam, start=[1,1,1], count=[-1,-1,-1])
      ncwrite(data, file, vnam, start=[1,1,1], count=[-1,-1,-1])
    end
    @printf("%s\n", hnam)
  end
=#
  if nyin == 0
    fora   *= @sprintf("%s %s %2d %15.8f %15.8f %2d [%s pour_point]\n", filw[1:13], "hydat_0000000",    1, plat, plon, -1, filw[6:13])
  else
    fora   *= @sprintf("%s %s %2d %15.8f %15.8f %2d [%s pour_point]\n", filw[1:13], "hydat_0000000", natb, plat, plon,  0, filw[6:13])
    for b = 1:nyin
      (hlat, hlon, area, hrea, wrea, natr, plat, plon, prea, line) = hyin[b] ; hnam = split(line)[1]
      bb = 2 ; b == best && wrea / prea > FRAC && (bb = 1 ; numb += 1)
      fora *= @sprintf("%s %s %2d %15.8f %15.8f %2d [%s pour_point] %15.8f %15.8f %15.8f %15.8f %15.8f %15.8f %15.8f\n", filw[1:13], hnam, natr, hlat, hlon, bb, filw[6:13], area, hrea, wrea, (area + hrea + wrea) / 3, plat, plon, prea)
    end
    @printf("found %8s with %4d HyDAT stations\n", name, nyin)
    numa += 1
  end
end
@printf("found %4d WRF-Hydro catchments with %d proxy-pour-point HyDAT stations inside\n", numa, numb)

fpa = My.ouvre(filc, "w") ; write(fpa, fora) ; close(fpa)
exit(0)

#=
# hydat_01AF010     3618     47.47055817    -68.23555756    1028.6 0 [01AF010 GREEN]
#    2500911   250   911   440     56.01741791    -87.60491943   250   912   439     56.03536606    -87.60378265

for refw = 1:length(linb)                                                     # copy the HYDROSHEDS catchment
  temp = split(linb[refw])                                                    # by comparing HyDAT catchments
  name =                temp[1]                                               # to the WRF-Hydro catchment
  latw = parse(Float64, temp[5])
  lonw = parse(Float64, temp[6])
  filw = @sprintf("wrfh_%8s.sta.hwshed", name)
  filw = replace(filw, ' ' => '0')
  shdw = readshed(filw)

  refh = shedmatch(refw)                                                      # upstream of the HyDAT station
  name =   split(linb[refw])[1] ; filw = @sprintf("wrfh_%8s.sta.hwshed", name) ; filw = replace(filw, ' ' => '0')
  fpc  =   My.ouvre(filw[1:end-7], "r", false) ; linw = readlines(fpc, keep = true) ; close(fpc)
  if refh > 0
    name = split(lina[refh])[1] ; filh =    dira * name * ".sta.hyshed"
    fpc =  My.ouvre(filh[1:end-7], "r", false) ; linh = readlines(fpc, keep = true) ; close(fpc)
    @printf("  matched %s to %s ; ", filw, filh)
    form  = "" ; for a =  1:88           form *= linh[a]  end
                 for a = 89:91           form *= linw[a]  end
                 for a = 92:length(linh) form *= linh[a]  end
    fpc = My.ouvre(filw[1:end-7], "w", false) ; write(fpc, form) ; close(fpc)
    fils =         filw[1:end-7] * ".hyshed"
    print("cp $filh $fils\n") ; cp(filh, fils; force = true)
  else
    @printf("unmatched %s ; ", filw)
    form  = "" ; for a =  1:92           form *= linw[a]  end
    form *= @sprintf("%-24s %s\n", "STN_REGULATION", "-9999 to -9999 Natural [0]")
    fpc = My.ouvre(filw[1:end-7], "w", false) ; write(fpc, form) ; close(fpc)
    fils =         filw[1:end-7] * ".hyshed"
    print("cp $filw $fils\n") ; cp(filw, fils; force = true)
  end
end


function shedmatch(refw::Int64)                                               # identify a nearby HyDAT station
  temp = split(linb[refw])                                                    # by comparing HyDAT catchments
  name =                temp[1]                                               # to the WRF-Hydro catchment
  latw = parse(Float64, temp[5])
  lonw = parse(Float64, temp[6])
  filw = @sprintf("wrfh_%8s.sta.hwshed", name)
  filw = replace(filw, ' ' => '0')
  shdw = readshed(filw)
  arew = area(shdw)

  clos = Array{Tuple{Int64, Float64, Float64, Float64}}(undef, 0)             # include areas of nonoverlap
  for a = 1:length(lina)
    temp = split(lina[a])
    filh = dira *         temp[1] * ".sta.hyshed"
    lath = parse(Float64, temp[3])
    lonh = parse(Float64, temp[4])
    if latw - DELL < lath < latw + DELL && lonw - DELL < lonh < lonw + DELL
      shdh = readshed(filh)
      shdd = GeoInterface.symdifference(shdw,shdh)
      areh = area(shdh)
      ared = area(shdd)
      push!(clos, (a, arew, areh, ared))
    end
  end
  length(clos) == 0 && return(0)

  refh = 0 ; refd = 9e9                                                       # as nonoverlap should be smaller than
  for a = 1:length(clos)                                                      # catchment area (and smallest overall)
    arei, arew, areh, ared = clos[a]
    ared < arew && ared < areh && ared < refd && (refh = arei ; refd = ared)
  end
  return(refh)
end

for refw = 1:length(linb)                                                     # copy the HYDROSHEDS catchment
  refh = shedmatch(refw)                                                      # upstream of the HyDAT station
  name =   split(linb[refw])[1] ; filw = @sprintf("wrfh_%8s.sta.hwshed", name) ; filw = replace(filw, ' ' => '0')
  fpc  =   My.ouvre(filw[1:end-7], "r", false) ; linw = readlines(fpc, keep = true) ; close(fpc)
  if refh > 0
    name = split(lina[refh])[1] ; filh =    dira * name * ".sta.hyshed"
    fpc =  My.ouvre(filh[1:end-7], "r", false) ; linh = readlines(fpc, keep = true) ; close(fpc)
    @printf("  matched %s to %s ; ", filw, filh)
    form  = "" ; for a =  1:88           form *= linh[a]  end
                 for a = 89:91           form *= linw[a]  end
                 for a = 92:length(linh) form *= linh[a]  end
    fpc = My.ouvre(filw[1:end-7], "w", false) ; write(fpc, form) ; close(fpc)
    fils =         filw[1:end-7] * ".hyshed"
    print("cp $filh $fils\n") ; cp(filh, fils; force = true)
  else
    @printf("unmatched %s ; ", filw)
    form  = "" ; for a =  1:92           form *= linw[a]  end
    form *= @sprintf("%-24s %s\n", "STN_REGULATION", "-9999 to -9999 Natural [0]")
    fpc = My.ouvre(filw[1:end-7], "w", false) ; write(fpc, form) ; close(fpc)
    fils =         filw[1:end-7] * ".hyshed"
    print("cp $filw $fils\n") ; cp(filw, fils; force = true)
  end
end
exit(0)
=#


#gbox = readgeom("POLYGON((-67.24 49.17, -67.05 48.90, -65.63 49.19, -64.25 48.84, -63.84 49.03, -65.00 49.56, -66.10 49.56, -67.24 49.17))")
#nbox = readgeom("POLYGON((-64.95 50.10, -64.75 49.82, -64.12 49.98, -64.12 50.10, -64.95 50.10))")
#sbox = readgeom("POLYGON((-63.82 48.69, -64.52 47.32, -63.47 47.32, -62.44 48.29, -63.82 48.69))")

#bbox = GeoInterface.symdifference(gbox,nbox)
#@show area(gbox), area(nbox), area(bbox)
#=
llpt = readgeom("POINT($(lons[c,b,1]) $(lats[c,b,1]))")
contains(ddbb, llpt)

const DELL             = 5.0                            # HyDAT station search lat/lon range
const DELMISS          = -8e9                           # generic missing value comparison
const GRDMISS          = -9e9                           # generic missing value on a grid

function stwrite(line::AbstractString)                                        # save WRF-Hydro data in text files
  temp = split(line) ; form = ""                                              # with temporary missing values for
  name =                temp[1]                                               # the nearest HyDAT and SWOT rivers
  loni = parse(  Int64, temp[2])                                              # (both location and grid indices)
  lati = parse(  Int64, temp[3])
  latf = parse(Float64, temp[5])
  lonf = parse(Float64, temp[6])
  filb = @sprintf("wrfh_%8s.sta", name)
  filb = replace(filb, ' ' => '0')
  form *= @sprintf("%-24s %s [%s]\n",           "STATION_NAME", "TBD", name) 
  form *= @sprintf(" -------------- remarks -------------- \n")
  fpb   = My.ouvre(filb, "w") ; write(fpb, form) ; close(fpb)
end

tnam = "wrfh_zzzzzzzz.nc"                                                     # construct a netcdf template
vars = ["flow", "floi", "levl", "levi", "sedc", "sedi", "load", "loai"]       # for a fixed time interval
daya = @sprintf("%s-01-01-12", YBEG) ; tima = My.datesous("1900-01-01-00", daya, "hr")
dayb = @sprintf("%s-12-31-12", YEND) ; timb = My.datesous("1900-01-01-00", dayb, "hr")
days = collect(tima:DELT:timb) ; nday = length(days) ; data = fill(MISS, nday)
My.nccreer(   tnam, nday, 1, 1, MISS; vnames = vars)
ncputatt(     tnam, "time", Dict("units" => "hours since 1900-01-01 00:00:0.0"))
ncwrite(days, tnam, "time", start=[1], count=[-1])
for vnam in vars
  ncwrite(data, tnam, vnam, start=[1,1,1], count=[-1,-1,-1])
end

function dywrite(line::AbstractString)                                        # and prepare NetCDF for HyDAT data
  name = split(line)[1]
  filb = @sprintf("wrfh_%8s.sta.nc", name)
  filb = replace(filb, ' ' => '0')
  print("cp $tnam $filb\n") ; cp(tnam, filb; force = true)
end

for a = 1:length(lins)
  stwrite(lins[a])
  dywrite(lins[a])
end
exit(0)
=#
#=
gnum = nnum = snum = 0
gbox = readgeom("POLYGON((-67.24 49.17, -67.05 48.90, -65.63 49.19, -64.25 48.84, -63.84 49.03, -65.00 49.56, -66.10 49.56, -67.24 49.17))")
nbox = readgeom("POLYGON((-64.95 50.10, -64.75 49.82, -64.12 49.98, -64.12 50.10, -64.95 50.10))")
sbox = readgeom("POLYGON((-63.82 48.69, -64.52 47.32, -63.47 47.32, -62.44 48.29, -63.82 48.69))")
gnam = name * "_Current_SAR_coverage_" * bnam * "_ingas.txt" ; fpg = My.ouvre(gnam, "w", false)
nnam = name * "_Current_SAR_coverage_" * bnam * "_innor.txt" ; fpn = My.ouvre(nnam, "w", false)
snam = name * "_Current_SAR_coverage_" * bnam * "_insou.txt" ; fps = My.ouvre(snam, "w", false)
gspd = ncread("../../wnd.era5/era5gosl_2008_2021.nc", "gspd", start=[1,1,1], count=[-1,-1,-1])[1,1,:]
nspd = ncread("../../wnd.era5/era5gosl_2008_2021.nc", "nspd", start=[1,1,1], count=[-1,-1,-1])[1,1,:]
sspd = ncread("../../wnd.era5/era5gosl_2008_2021.nc", "sspd", start=[1,1,1], count=[-1,-1,-1])[1,1,:]

hnow = hora                                                                   # then construct each timeseries
for a = 1:nhor
  global hnow, gnum, nnum, snum
  dnow = My.datesous("1900-01-01-00",             hnow[1:10] *       "-00", "hr") ; dind = findfirst(isequal(dnow), days)
  mnow = My.datesous("1900-01-01-00",             hnow[1: 7] *    "-15-00", "hr") ; mind = findfirst(isequal(mnow), mons)
  snow = My.datesous("1900-01-01-00",             hnow[1: 4] * "-06-30-00", "hr") ; sind = findfirst(isequal(snow), seas)
  anow = My.datesous("1900-01-01-00", hora[1:4] * hnow[5:10] *       "-00", "hr") ; aind = findfirst(isequal(anow), anns)
  stem = hnow[1:10] * "_" * hnow[12:13]
  for file in fils
    if isfile(file) && startswith(file, stem) && endswith(file, ".00800.sar.anom.nc")
      seain = SBEG <= 100 * parse(Int, file[6:7]) + parse(Int, file[9:10]) <= SEND ? true : false
    if seain
      fph = My.ouvre("../" * file[1:25] * ".hdr", "r")
      linh = readlines(fph; keep = true) ; close(fph)
      ullat = split(linh[11])[2] ; ullon = split(linh[12])[2]
      urlat = split(linh[13])[2] ; urlon = split(linh[14])[2]
      lllat = split(linh[15])[2] ; lllon = split(linh[16])[2]
      lrlat = split(linh[17])[2] ; lrlon = split(linh[18])[2]
      sarbox = readgeom("POLYGON(($ullon $ullat, $urlon $urlat, $lrlon $lrlat, $lllon $lllat, $ullon $ullat))")
      gasin = contains(sarbox, gbox) || overlaps(sarbox, gbox) ; gasin && (gnum += 1 ; line = @sprintf("%s\n", file[1:25]) ; print(fpg, line))
      norin = contains(sarbox, nbox) || overlaps(sarbox, nbox) ; norin && (nnum += 1 ; line = @sprintf("%s\n", file[1:25]) ; print(fpn, line))
      souin = contains(sarbox, sbox) || overlaps(sarbox, sbox) ; souin && (snum += 1 ; line = @sprintf("%s\n", file[1:25]) ; print(fps, line))
      seain = SBEG <= 100 * parse(Int, file[6:7]) + parse(Int, file[9:10]) <= SEND ? true : false
      filf = "../" * file[1:25] * ".sar.nc"
      lats = ncread(filf, "lats", start=[1,1,1], count=[-1,-1,-1])



function obstype(code::AbstractString)                                        # assume that each measurement
  cval = GOOD                                                                 # is a good value by default
  code == "A" && (cval = -1.0)                                                # or partial day otherwise,
  code == "B" && (cval = -2.0)                                                # or ice conditions,
  code == "D" && (cval = -3.0)                                                # or dry,
  code == "E" && (cval = -4.0)                                                # or estimated,
  code == "S" && (cval = -5.0)                                                # or sample(s) taken
  cval
end
function obstype(code::Missing)  GOOD  end

function dywrite(ind::Int64)                                                  # and save each daily timeseries
  stnam = "hydat_" * stdat[ind,1] * ".nc"
  print("cp $tnam $stnam\n") ; cp(tnam, stnam; force = true)
  numb = zeros(VNUM)

  stvar = "DLY_FLOWS"  ; indv = FLOW ; indd = 12 ; indn = 2
  vara  = vars[indv  ] ; data = fill(MISS, nday)
  vari  = vars[indv+1] ; dati = fill(MISS, nday)
  daily = DBInterface.execute(stall, "SELECT * FROM $stvar WHERE STATION_NUMBER='$(stdat[ind,1])'") |> DataFrame ; raily, caily = size(daily)
  for a = 1:raily
    if YBEG <=  daily[a,2] <= YEND
      ynow = "$(daily[a,2])"
      mnow = "$(daily[a,3])" ; mnow = (length(mnow) == 1 ? "0" : "") * mnow
      dnow = "$ynow-$mnow-01-12" ; inow = indd ; mref = mnow
      while mref == mnow
        dref = My.datesous("1900-01-01-00", dnow, "hr")
        dind = findfirst(isequal(dref), days)
        if daily[a,inow] !== missing
          data[dind]  =         daily[a,inow]
          dati[dind]  = obstype(daily[a,inow+1])
          numb[indv] += 1
        end
#       line = @sprintf("%s flow is %f [%f]\n", dnow, data[dind], dati[dind]) ; print(line)
        dnow = My.dateadd(dnow, 1, "dy")
        mnow = dnow[6:7] ; inow += indn
      end
    end
  end
  ncwrite(data, stnam, vara, start=[1,1,1], count=[-1,-1,-1])
  ncwrite(dati, stnam, vari, start=[1,1,1], count=[-1,-1,-1])

  stvar = "DLY_LEVELS"  ; indv = LEVL ; indd = 13 ; indn = 2
  vara  = vars[indv  ] ; data = fill(MISS, nday)
  vari  = vars[indv+1] ; dati = fill(MISS, nday)
  daily = DBInterface.execute(stall, "SELECT * FROM $stvar WHERE STATION_NUMBER='$(stdat[ind,1])'") |> DataFrame ; raily, caily = size(daily)
  for a = 1:raily
    if YBEG <=  daily[a,2] <= YEND
      ynow = "$(daily[a,2])"
      mnow = "$(daily[a,3])" ; mnow = (length(mnow) == 1 ? "0" : "") * mnow
      dnow = "$ynow-$mnow-01-12" ; inow = indd ; mref = mnow
      while mref == mnow
        dref = My.datesous("1900-01-01-00", dnow, "hr") 
        dind = findfirst(isequal(dref), days)
        if daily[a,inow] !== missing
          data[dind]  =         daily[a,inow]
          dati[dind]  = obstype(daily[a,inow+1])
          numb[indv] += 1
        end
        dnow = My.dateadd(dnow, 1, "dy")
        mnow = dnow[6:7] ; inow += indn
      end
    end
  end
  ncwrite(data, stnam, vara, start=[1,1,1], count=[-1,-1,-1])
  ncwrite(dati, stnam, vari, start=[1,1,1], count=[-1,-1,-1])

  stvar = "SED_DLY_SUSCON"  ; indv = SEDC ; indd = 11 ; indn = 2
  vara  = vars[indv  ] ; data = fill(MISS, nday)
  vari  = vars[indv+1] ; dati = fill(MISS, nday)
  daily = DBInterface.execute(stall, "SELECT * FROM $stvar WHERE STATION_NUMBER='$(stdat[ind,1])'") |> DataFrame ; raily, caily = size(daily)
  for a = 1:raily
    if YBEG <=  daily[a,2] <= YEND
      ynow = "$(daily[a,2])"
      mnow = "$(daily[a,3])" ; mnow = (length(mnow) == 1 ? "0" : "") * mnow
      dnow = "$ynow-$mnow-01-12" ; inow = indd ; mref = mnow
      while mref == mnow
        dref = My.datesous("1900-01-01-00", dnow, "hr") 
        dind = findfirst(isequal(dref), days)
        if daily[a,inow] !== missing
          data[dind]  =         daily[a,inow]
          dati[dind]  = obstype(daily[a,inow+1])
          numb[indv] += 1
        end
        dnow = My.dateadd(dnow, 1, "dy")
        mnow = dnow[6:7] ; inow += indn
      end
    end
  end
  ncwrite(data, stnam, vara, start=[1,1,1], count=[-1,-1,-1])
  ncwrite(dati, stnam, vari, start=[1,1,1], count=[-1,-1,-1])

  stvar = "SED_DLY_LOADS"  ; indv = LOAD ; indd = 12 ; indn = 1
  vara  = vars[indv  ] ; data = fill(MISS, nday)
  vari  = vars[indv+1] ; dati = fill(MISS, nday)
  daily = DBInterface.execute(stall, "SELECT * FROM $stvar WHERE STATION_NUMBER='$(stdat[ind,1])'") |> DataFrame ; raily, caily = size(daily)
  for a = 1:raily
    if YBEG <=  daily[a,2] <= YEND
      ynow = "$(daily[a,2])"
      mnow = "$(daily[a,3])" ; mnow = (length(mnow) == 1 ? "0" : "") * mnow
      dnow = "$ynow-$mnow-01-12" ; inow = indd ; mref = mnow
      while mref == mnow
        dref = My.datesous("1900-01-01-00", dnow, "hr")
        dind = findfirst(isequal(dref), days)
        if daily[a,inow] !== missing
          data[dind]  = daily[a,inow]
          dati[dind]  = 1.0
          numb[indv] += 1
        end
        dnow = My.dateadd(dnow, 1, "dy")
        mnow = dnow[6:7] ; inow += indn
      end
    end
  end
  ncwrite(data, stnam, vara, start=[1,1,1], count=[-1,-1,-1])
  ncwrite(dati, stnam, vari, start=[1,1,1], count=[-1,-1,-1])

  stfil = "hydat_" * stdat[ind,1] * ".sta" ; fpa = My.ouvre(stfil, "r")       # finally insert time coverage
  sttmp = "hydat_" * stdat[ind,1] * ".tmp" ; fpb = My.ouvre(sttmp, "w")       # for each netcdf variable in
  lines = readlines(fpa; keep = true)      ;   c = length(lines)              # the corresponding text file
  for a = 1:14  form = lines[a] ; write(fpb, form)  end
  form = @sprintf("%-24s %6d %6d %7.3f\n", "$YBEG-$YEND.DLY_FLOWS",      numb[FLOW], nday, 100 * numb[FLOW] / nday) ; write(fpb, form)
  form = @sprintf("%-24s %6d %6d %7.3f\n", "$YBEG-$YEND.DLY_LEVELS",     numb[LEVL], nday, 100 * numb[LEVL] / nday) ; write(fpb, form)
  form = @sprintf("%-24s %6d %6d %7.3f\n", "$YBEG-$YEND.SED_DLY_SUSCON", numb[SEDC], nday, 100 * numb[SEDC] / nday) ; write(fpb, form)
  form = @sprintf("%-24s %6d %6d %7.3f\n", "$YBEG-$YEND.SED_DLY_LOADS",  numb[LOAD], nday, 100 * numb[LOAD] / nday) ; write(fpb, form)
  for a = 19:c  form = lines[a] ; write(fpb, form)  end
  close(fpa) ; close(fpb) ; mv(sttmp, stfil; force=true)
end
=#
