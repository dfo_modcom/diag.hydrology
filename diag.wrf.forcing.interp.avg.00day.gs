* This script is designed to plot cyclone location and intensity (colour code).
* It can be executed using a command like
*
*     grads -blc "diag.wrf.forcing.interp.avg.00day nar_1990_2004 wrf_1990_2004 nar_2005_2020 wrf_RCP45_2005_2099 wrf_RCP85_2005_2099"
*
* - RD November 2012

function plot(args)
stema = subwrd(args,1)
stemb = subwrd(args,2)
stemc = subwrd(args,3)
stemd = subwrd(args,4)
steme = subwrd(args,5)
stemf = "wrf_1990_2004.forcing_wrf_1990_2004.cato.nar_1990_2004"
taila = "day.avg.nc"

"set rgb    42     0    0    0"
"set rgb    43    30   30   30"
"set rgb    44    50   50   50"
"set rgb    45    70   70   70"
"set rgb    46    90   90   90"
"set rgb    47   110  110  110"
"set rgb    48   130  130  130"
"set rgb    49   150  150  150"
"set rgb    50   170  170  170"
"set rgb    51   190  190  190"
"set rgb    52   210  210  210"
"set rgb    53   255  255  255"

xpic = 4 ; string = "0.5  8.0 1.70 "xpic ; inner_decomp(string)
a = 1 ; while (a <= xpic) ; lef.a = _retlef.a ; cen.a = _retmid.a ; rig.a = _retrig.a ; a = a + 1 ; endwhile

ypic = 8 ; string = "0.3 10.2 0.95 "ypic ; inner_decomp(string)
a = 1 ; while (a <= ypic) ; b = ypic - a + 1 ; low.b = _retlef.a ; hig.b = _retrig.a ; a = a + 1 ; endwhile

a = 1
while (a <= xpic)
* if (a = 1) ; reg = "arc" ; lef = 0.5 ; rig = 8.0 ; endif
* if (a = 2) ; reg = "atl" ; lef = 4.4 ; rig = 8.0 ; endif
  b = 1
  while (b < 9)
    if (b = 1) ; var = "3600*RAINRATE" ; endif
    if (b = 2) ; var =   "-273.15+T2D" ; endif
    if (b = 3) ; var =      "1000*Q2D" ; endif
    if (b = 4) ; var =           "R2D" ; endif
    if (b = 5) ; var =        "SWDOWN" ; endif
    if (b = 6) ; var =        "LWDOWN" ; endif
    if (b = 7) ; var =           "S2D" ; endif
    if (b = 8) ; var =     "0.01*PSFC" ; endif

    if (b = 1) ; lab = "a" ; rng =   "0.05  0.20" ; yint =   "0.05" ; til = "Precipitation (mm/hr)"          ; endif
    if (b = 2) ; lab = "b" ; rng =  "-4.0  12.0"  ; yint =   "4.0"  ; til = "Temperature (`3.`0C)"           ; endif
    if (b = 3) ; lab = "c" ; rng =   "4.0   7.0"  ; yint =   "1.0"  ; til = "Specific Humidity (g/kg)"       ; endif
    if (b = 4) ; lab = "d" ; rng =  "55.0  95.0"  ; yint =  "10.0"  ; til = "Relative Humidity (%)"          ; endif
    if (b = 5) ; lab = "e" ; rng = "-20.0 500.0"  ; yint = "150.0"  ; til = "Shortwave Radiation (W/m`a2`n)" ; endif
    if (b = 6) ; lab = "f" ; rng = "265.0 315.0"  ; yint =  "15.0"  ; til = "Longwave Radiation (W/m`a2`n)"  ; endif
    if (b = 7) ; lab = "g" ; rng =   "3.5   6.5"  ; yint =   "1.0"  ; til = "Wind Speed (m/s)"               ; endif
    if (b = 8) ; lab = "h" ; rng = "972.0 983.0"  ; yint =   "4.0"  ; til = "Surface Pressure (hPa)"         ; endif

    if (a = 1) ; tail = "00"taila ; endif
    if (a = 2) ; tail = "06"taila ; endif
    if (a = 3) ; tail = "12"taila ; endif
    if (a = 4) ; tail = "18"taila ; endif

    fila = "forcing_"stema"."tail ; col.1 =  2 ; sty.1 = 1
    filb = "forcing_"stemb"."tail ; col.2 =  1 ; sty.2 = 1
    filc = "forcing_"stemc"."tail ; col.3 =  2 ; sty.3 = 1
    fild = "forcing_"stemd"."tail ; col.4 =  1 ; sty.4 = 1
    file = "forcing_"steme"."tail ; col.5 = 48 ; sty.5 = 1
filf = "forcing_"stemf"."tail ; col.6 =  4 ; sty.6 = 1

    "sdfopen "fila
    "sdfopen "filb
    "sdfopen "filc
    "sdfopen "fild
    "sdfopen "file
"sdfopen "filf
*   "set dfile 3" ; "set t 21500 131400" ; "define seen = 1 - "var".3 + "var".3" ; "set dfile 1"

    tima =   5479
    timb = 131400
    lap  = -0.2 ;* mid = lef + (rig - lef) * tima / (tima + timb) + lap / 2
    "set parea "lef.a" "rig.a" "low.b" "hig.b
    "set mproj off"
    "set grid off"
    "set mpdraw off"
    "set xlopts 1 5 0.10"
    "set ylopts 1 5 0.10" ; "set ylpos 0 l"
    "set xlab off" ; if (b = 8) ; "set xlab on" ; endif
*   "set ylint 2"  ; if (b = 3) ; "set ylint 1.0" ; endif
    if (a = 1) ; "set ylab  on" ; endif
    if (a > 1) ; "set ylab off" ; endif
    "set t 1 "tima
    "set vrange "rng ; "set ylint "yint
    "set xlabs 1990 | 2005"
    "set xlabs 1990 | 1995 | 2000 | "
    "set cthick 8" ;* "set line "col.1" "sty.1" 8" ; "set string 1 c 8"
*   "set ccolor "col.5 ; "set cstyle "sty.5 ; "set cmark 0" ; "set gxout line" ; "set grads off" ; "d "var".5"
"set ccolor "col.6 ; "set cstyle "sty.6 ; "set cmark 0" ; "set gxout line" ; "set grads off" ; "d "var".6"
    "set ccolor "col.2 ; "set cstyle "sty.2 ; "set cmark 0" ; "set gxout line" ; "set grads off" ; "d "var".2"
    "set ccolor "col.1 ; "set cstyle "sty.1 ; "set cmark 0" ; "set gxout line" ; "set grads off" ; "d "var".1"

*   mid = mid - lap / 2
*   "set parea "mid" "rig" "low.b" "hig.b
*   "set mproj off"
*   "set grid off"
*   "set mpdraw off"
*   "set xlopts 1 5 0.10"
*   "set ylopts 1 5 0.10" ; "set ylpos 0 r"
*   "set xlab off" ; if (b = 8) ; "set xlab on" ; endif
**  "set ylint 2"  ; if (b = 3) ; "set ylint 1.0" ; endif
*   if (a = 1) ; "set ylab off" ; endif
*   if (a = 2) ; "set ylab  on" ; endif
*   "set dfile 3" ; "set t 1 "timb
*   "set vrange "rng ; "set ylint "yint
*   "set xlabs | | 2025 | | 2045 | | 2065 | | 2085 |"
*   "set cthick 8" ;* "set line "col.3" "sty.3" 8" ; "set string 1 c 8"
*   "set ccolor "col.7 ; "set cstyle "sty.7 ; "set cmark 0" ; "set gxout line" ; "set grads off" ; "d "var".7"
*   "set ccolor "col.6 ; "set cstyle "sty.6 ; "set cmark 0" ; "set gxout line" ; "set grads off" ; "d "var".6"
*   "set ccolor "col.4 ; "set cstyle "sty.4 ; "set cmark 0" ; "set gxout line" ; "set grads off" ; "d "var".4"
*   "set ccolor "col.3 ; "set cstyle "sty.3 ; "set cmark 0" ; "set gxout line" ; "set grads off" ; "d "var".3"
    "set strsiz 0.15" ; "set string 1 c 6" ; "draw string      4.3  "hig.b+0.15" "til
*   "set strsiz 0.19" ; "set string 1 c 6" ; "draw string "mid+0.2" "hig.b-0.2"  "lab
"close 6"
    "close 5"
    "close 4"
    "close 3"
    "close 2"
    "close 1"
    b = b + 1
  endwhile
  a = a + 1
endwhile
"set parea off"

cen = 0.62 ; top = hig.1-0.5 ; del = -0.3
label.1 = cen" "top-1*del" NARR"
cen = 1.65
label.2 = cen" "top-1*del" WRF"
cen = 2.65
label.3 = cen" "top-1*del" WRF-calib"
"set strsiz 0.15"
"set string "col.1" l 8" ; "draw string "label.1
"set string "col.2" l 8" ; "draw string "label.2
"set string "col.6" l 8" ; "draw string "label.3

hig.1 = hig.1 + 0.5
"set string 1 c 6" ; "draw string "cen.1" "hig.1" 00 UTC"
"set string 1 c 6" ; "draw string "cen.2" "hig.1" 06 UTC"
"set string 1 c 6" ; "draw string "cen.3" "hig.1" 12 UTC"
"set string 1 c 6" ; "draw string "cen.4" "hig.1" 18 UTC"

*cen = 2.0  ; top = hig.1-0.5 ; del = -0.3
*cen = 6.5  ; top = hig.1-0.5 ; del = -0.3
*label.1 = cen+1.33" "top-1*del" RCP4.5"
*label.2 = cen+2.33" "top-1*del" RCP8.5"
*label.3 = cen+0.09" "top-0*del" WRF"
*label.4 = cen+0.09" "top-1*del" WRF"
*label.5 = cen-0.94" "top-0*del" NARR"
*label.6 = cen-0.94" "top-1*del" NARR"
*"set strsiz 0.15"
*"set string "col.3" l 8" ; "draw string "label.1
*"set string "col.4" l 8" ; "draw string "label.2
*"set string "col.3" l 8" ; "draw string "label.3
*"set string "col.4" l 8" ; "draw string "label.4
*"set string "col.6" l 8" ; "draw string "label.5
*"set string "col.7" l 8" ; "draw string "label.6

*top = hig.1+0.2
*label.1 = "1.0 "top" Arctic"
*label.2 = "7.5 "top" N.Atlantic"
*"set strsiz 0.19"
*"set string 1 l 8" ; "draw string "label.1
*"set string 1 r 8" ; "draw string "label.2

plot = "plot."stema"."stemb"."stemc"."stemd"."steme".00day.avg.png"
say "printim "plot" png white x1700 y2200"
    "printim "plot" png white x1700 y2200"
"quit"


function inner_decomp(args)
  lef = subwrd(args,1)
  rig = subwrd(args,2)
  wid = subwrd(args,3)
  num = subwrd(args,4)
  _retmid.1   = lef + wid / 2
  _retmid.num = rig - wid / 2
  a = 2
  while (a < num)
    _retmid.a = (_retmid.num * (a-1) + _retmid.1 * (num-a)) / (num - 1)
    a = a + 1
  endwhile

  a = 1
  while (a <= num)
    _retlef.a = _retmid.a - wid / 2
    _retrig.a = _retmid.a + wid / 2
    a = a + 1
  endwhile
return
