#=
 = Unzip watershed and river shapefiles from all NRCAN zip files
 = in subdirectories (like 01 02 03) and create a text file with
 = the names and colours to plot them - RD April 2020.
 =#

using My, Printf

if (argc = length(ARGS)) == 0
  print("\nUsage: jjj $(basename(@__FILE__)) 01 02 03\n\n")
  exit(1)
end

function walkdir(dir::AbstractString)
  files  = readdir(dir)
  for file in files
    full = dir * "/" * file
    if isfile(full) && startswith(file, "nhn") && endswith(file, ".zip")
      print("unzip -qo $full -d x\n")
        run(`unzip -qo $full -d x`)
      cd("x") ; filez = readdir(".")
      for filz in filez
        occursin(  "NLFLOW", filz) && mv(filz, "../shp/" * filz, force=true)
        occursin("WORKUNIT", filz) && mv(filz, "../shp/" * filz, force=true)
      end
      cd("..") ; run(`rm -r x`)
    end
  end
end

!isdir("shp") && mkdir("shp")
for argdir in ARGS
# print("\nunzipping $argdir\n\n")
# walkdir(argdir)
end
cd("shp") ; files = readdir(".")

basn = Dict{AbstractString,          Int64}()                                 # define a subbasin dictionary
rtos = Dict{AbstractString, AbstractString}()
for file in files
  if endswith(file, "WORKUNIT_LIMIT_2.shp")
    basn[file[5:7]] = 0
    rtos[file     ] = "fill"
  end
end

colr = 2                                                                      # and define its colour (1 to 15)
for (bas, col) in basn
  global colr
  basn[bas] = colr
  colr += 1 ; colr == 16 && (colr = 2)
end
@show basn

for file in files                                                             # and link the river and domain shps
  if endswith(file, "NLFLOW_1.shp")
    rtos[file     ] = "fill"
  end
end

form = ""                                                                     # then save the dictionary to txt
for file in files
  if endswith(file, "WORKUNIT_LIMIT_2.shp")
    global form
    other = ""
    for filz in files
      if startswith(filz, "NHN") && filz[1:11] == file[1:11] && endswith(filz, "NLFLOW_1.shp")
        other = filz[1:end-4]
      end
    end
    form *= @sprintf("%5d %33s       %s\n", basn[file[5:7]], other, file[1:end-4])
  end
end

cd("..")
filo = "Fulldom_hires_outlets_nrcan.txt"
fpa = My.ouvre(filo, "w") ; write(fpa, form) ; close(fpa)
exit(0)

#=
NHN_01HA000_2_0_HD_ISLAND_2"
NHN_01HA000_2_0_HD_SLWATER_1"
NHN_01HA000_2_0_HD_WATERBODY_2"
NHN_01HA000_2_0_HN_BANK_1"
NHN_01HA000_2_0_HN_DELIMITER_1"
NHN_01HA000_2_0_HN_HYDROJUNCT_0"
NHN_01HA000_2_0_HN_NLFLOW_1"
NHN_01HA000___WORKUNIT_LIMIT_2
=#
