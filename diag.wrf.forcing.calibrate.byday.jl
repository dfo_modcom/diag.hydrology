#=
 = This program calibrates one timeseries against another, where both might be
 = domain- and running-annual-averages at 6-h intervals.  Separate calibrations
 = are performed for each UTC hour (0,6,12,18), as well as for the continuous
 = 6-h timeseries.  Output is a text file with all linear calibration parameters
 = (additive and multiplicative) - RD May 2021.
 =#

using My, Printf, NetCDF, Statistics #, RCall ; R"library(DetMCD)"
const DETMCD           = false                          # Minimum Covariance Determinant use (i.e., with trimming of outliers)
const LIMMCD           = 0.90                           # Minimum Covariance Determinant trimming (nonoutlier percent is higher)
const MISS             = -9999.0                        # generic missing value
const GRDMIS           = 99999.0                        # generic missing value on file

if (argc = length(ARGS)) != 2
  print("\nUsage: jjj $(basename(@__FILE__)) wrf_1990_2004 nar_1990_2004\n\n")
  exit(1)
end

vars = ["R2D", "S2D", "T2D", "Q2D", "U2D", "V2D", "PSFC", "RAINRATE", "SWDOWN", "LWDOWN"]
utcs = ["00", "06", "12", "18", "by"]
nvar = length(vars)
nutc = length(utcs)
cals = Array{Float64}(undef, 2, nutc, nvar)
for a = 1:nvar, b = 1:nutc  cals[1,b,a] = 0.0 ; cals[2,b,a] = 1.0  end

for a = 1:nvar, b = 1:nutc                                                    # loop through each variable/hour and
  filc = "forcing_" * ARGS[2] * "." * utcs[b] * "day.avg.nc"                  # read the calibrated and uncalibrated
  filu = "forcing_" * ARGS[1] * "." * utcs[b] * "day.avg.nc"                  # timeseries
  cccc = ncread(filc, vars[a], start=[1,1,1], count=[-1,-1,-1])
  uuuu = ncread(filu, vars[a], start=[1,1,1], count=[-1,-1,-1])
  size(cccc) != size(uuuu) && error("\nERROR : $filc and $filu have different dimensions\n\n")

  (nlon, nlat, ntim) = size(cccc)                                             # then define a joint missing data mask
  mask = trues(ntim)
  for c = 1:ntim ; (cccc[1,1,c] == GRDMIS || uuuu[1,1,c] == GRDMIS) && (mask[c] = false) ; end

  avcc = mean(cccc[1,1,mask]) ; varc = var(cccc[1,1,mask])
  avuu = mean(uuuu[1,1,mask]) ; varu = var(uuuu[1,1,mask])
  cvcu =  cov(cccc[1,1,mask],              uuuu[1,1,mask])
# if varc > 0 && varu > 0
#   cals[2,b,a] = cvcu / varc
    cals[1,b,a] = avuu - cals[2,b,a] * avcc
# end
end

fila = "forcing_" * ARGS[1] * ".cato." * ARGS[2]                              # and save all calibration parameters
form = ""
for a = 1:nvar
  global form
  form *= @sprintf("%12s", vars[a])
  for b = 1:nutc
    form *= @sprintf(" %22.11f %22.11f", cals[1,b,a], cals[2,b,a])
  end
  form *= "\n"
end
fpa = My.ouvre(fila, "w")
write(fpa, form)
close(fpa)
exit(0)
