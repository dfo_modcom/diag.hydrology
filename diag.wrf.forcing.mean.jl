#=
 = Construct a 365-day, 3- or 6-hourly annual average over, say, a 15-year period.
 = For a three-year spinup (e.g., an "average year" that repeats during 1901-1903),
 = it suffices to ignore 366-day leap years and average by Julian day (e.g., for
 = a 2006-2020 average, ignore Dec 31 for the years 2008/12/16/20) - RD February,
 = March 2021.
 =#

using My, Printf, NetCDF

const YEAS             = 15                             # number of years to average
const DAYS             = 365                            # number of days in the year
if     in("WRFSDOM", keys(ENV)) && ENV["WRFSDOM"] == "_AuxF"
  const LATS           =      9                         # number of  latitudes in WRFHydro grid
  const LONS           =      9                         # number of longitudes in WRFHydro grid
  const OSIZ           =  18288                         # number of bytes of a typical output file
elseif in("WRFSDOM", keys(ENV)) && ENV["WRFSDOM"] == "_Cali"
  const LATS           =     31                         # number of  latitudes in WRFHydro grid
  const LONS           =     57                         # number of longitudes in WRFHydro grid
  const OSIZ           =  86288                         # number of bytes of a typical output file
elseif in("WRFSDOM", keys(ENV)) && ENV["WRFSDOM"] == "_East"
  const LATS           =     54                         # number of  latitudes in WRFHydro grid
  const LONS           =     62                         # number of longitudes in WRFHydro grid
  const OSIZ           = 149752                         # number of bytes of a typical output file
else
  const LATS           =     86                         # number of  latitudes in WRFHydro grid
  const LONS           =    108                         # number of longitudes in WRFHydro grid
  const OSIZ           = 394872                         # number of bytes of a typical output file
end

if (argc = length(ARGS)) != 3 && (argc = length(ARGS)) != 5
  print("\nUsage: jjj $(basename(@__FILE__))         srcdir year_one delta_hr [step_start step_interval]\n\n")
  print(  " e.g.: jjj $(basename(@__FILE__)) ../FORCING.nar/    2006        3        1500           500\n\n")
  exit(1)
end
yone =             parse(Int64, ARGS[2])
delh =             parse(Int64, ARGS[3])
stps =             div(DAYS * 24, delh)
stpa = argc == 5 ? parse(Int64, ARGS[4]) : 1
stpb = argc == 5 ? parse(Int64, ARGS[5]) : stps
vars = ["T2D", "Q2D", "U2D", "V2D", "PSFC", "RAINRATE", "LWDOWN", "SWDOWN"]

ystr = Array{AbstractString}(undef, YEAS)                                     # define all dates to average over
dats = Array{AbstractString}(undef, YEAS, stps)
fats = Array{AbstractString}(undef,       stps)
for             b = 1:YEAS  ystr[b]   = @sprintf("%4s", b + yone - 1)
                            dats[b,1] = ystr[b] * "010100"                end
for a = 2:stps, b = 1:YEAS  dats[b,a] = dateadd(dats[b,a-1], delh, "hr")  end
                            fats[1]   = "1901010100"
for a = 2:stps              fats[a]   = dateadd(fats[  a-1], delh, "hr")  end

stpc = stpa + stpb
stpc > stps && (stpc = stps)
for a = stpa:stpc                                                             # and for each data, average vars
  fila = ARGS[1] * dats[1,a]        * ".LDASIN_DOMAIN1"
  filb =  "1901" * dats[1,a][5:end] * ".LDASIN_DOMAIN1"
  fsiz = 0 ; isfile(filb) && (fsiz = filesize(filb))
  if fsiz != OSIZ
    print("copying $fila to $filb\nwriting")
    cp(fila, filb; force = true)
    for b = 1:length(vars)
      avgg = zeros(LONS, LATS, 1)
      for c = 1:YEAS
        fila = ARGS[1] * dats[c,a] * ".LDASIN_DOMAIN1"
#       print(" reading $fila $(vars[b])\n")
        avgg += ncread(fila, vars[b], start=[1,1,1], count=[-1,-1,-1])
      end
      print(" $(vars[b])")
      avgg ./= YEAS
      ncwrite(avgg, filb, vars[b], start=[1,1,1], count=[-1,-1,-1])
    end
    print("\n")
  end
end
exit(0)
