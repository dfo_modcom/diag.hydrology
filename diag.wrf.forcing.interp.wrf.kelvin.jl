#=
 = HadGEM temperature may be in Celcius, so a check is done here
 = and a correction to Kelvin reported if necessary - RD Feb 2024.
 =#

using My, Printf, NetCDF

if (argc = length(ARGS)) != 2
  print("\nUsage: jjj $(basename(@__FILE__)) 1990-01-01 2004-12-31\n\n")
  exit(1)
end

dnow = ARGS[1]                                                                # update the temperature
flag = true                                                                   # grid if not in Kelvin
while flag
  global dnow, flag
  for (a, hour) in enumerate(["00", "06", "12", "18"])
    file = dnow[1:4] * dnow[6:7] * dnow[9:10] * hour * ".LDASIN_DOMAIN1"
    data = ncread(  file, "T2D", start=[1,1,1], count=[-1,-1,-1])
    (nlon, nlat, ntim) = size(data) ; numb = 0
    for b = 1:ntim, c = 1:nlat, d = 1:nlon
      data[d,c,b] < -99 && (data[d,c,b] += 273.15 ; numb += 1)
      data[d,c,b] > 400 && (data[d,c,b] -= 273.15 ; numb += 1)
    end
    if numb > 0
      ncwrite(data, file, "T2D", start=[1,1,1], count=[-1,-1,-1])
      print("writing $file with $numb adjustments to T2D\n")
    else
      print("keeping $file unadjusted\n")
    end
  end
  dnow == ARGS[2] && (flag = false)
  dnow = dateadd(dnow, 1, "dy")
end
exit(0)
