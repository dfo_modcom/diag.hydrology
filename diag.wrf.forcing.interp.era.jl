#=
 = Interpolate ERA5 surface data to the WRFHydro grid using ncl.  Although precip
 = is a 3-h accumulation and LW/SW radiation are a forecast/average, all variables
 = are taken to be valid at the start of the 3-h period.  Yearly ERA5 variables are
 = interpolated to the WRFHydro grid, and all variables are then written to forcing
 = files (one file every 3h with variables in the expected unit) - RD June 2022.
 =#

using My, Printf, NetCDF

const AGAIN            = false                          # recreate previously interpolated ERA5 grids
const DELT             = 3                              # number of      hours between WRFHydro grids
if     in("WRFSDOM", keys(ENV)) && ENV["WRFSDOM"] == "_AuxF"
  const LATS           =    9                           # number of  latitudes      in WRFHydro grid
  const LONS           =    9                           # number of longitudes      in WRFHydro grid
elseif in("WRFSDOM", keys(ENV)) && ENV["WRFSDOM"] == "_Cali"
  const LATS           =   31                           # number of  latitudes      in WRFHydro grid
  const LONS           =   57                           # number of longitudes      in WRFHydro grid
elseif in("WRFSDOM", keys(ENV)) && ENV["WRFSDOM"] == "_East"
  const LATS           =   54                           # number of  latitudes      in WRFHydro grid
  const LONS           =   62                           # number of longitudes      in WRFHydro grid
else
  const LATS           =   86                           # number of  latitudes      in WRFHydro grid
  const LONS           =  108                           # number of longitudes      in WRFHydro grid
end

if (argc = length(ARGS)) != 2
  print("\nUsage: jjj $(basename(@__FILE__)) 2006-01-01-00 2020-12-31-21\n\n")
  exit(1)
end
yearon = ARGS[1][1:4]
srcsrc = "../FORCING.era.src/ERA5_t2_y$yearon" * "m01.nc"                     # define interpolation file names
dstsrc = "../DOMAIN_full_full/geogrid.nc"
nclsrc = "z_$yearon.nclcn.ncl" ; isfile(nclsrc) && rm(nclsrc)
srcwgt = "z_$yearon.srcwgt.nc" ; isfile(srcwgt) && rm(srcwgt)
dstwgt = "z_$yearon.dstwgt.nc" ; isfile(dstwgt) && rm(dstwgt)
nclwgt = "z_$yearon.nclwgt.nc" #; isfile(nclwgt) && rm(nclwgt)

function create_nclwgt()                                                      # generate weights for interpolating
  form  = "load \"\$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl\"\n"     # ERA5 variables to the WRF grid
  form *= "load \"\$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl\"\n"      # (can't seem to turn off PETLog)
  form *= "load \"\$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl\"\n"
  form *= "load \"\$NCARG_ROOT/lib/ncarg/nclscripts/contrib/ut_string.ncl\"\n"
  form *= "load \"\$NCARG_ROOT/lib/ncarg/nclscripts/esmf/ESMF_regridding.ncl\"\n"
  form *= "begin\n"
  form *= "srcfil = addfile(\"$srcsrc\", \"r\")\n"
  form *= "srclat = srcfil->latitude(:)\n"
  form *= "srclon = srcfil->longitude(:)\n"
  form *= "Opt              = True\n"
  form *= "Opt@SrcRegional  = True\n"
  form *= "Opt@NoPETLog     = True\nOpt@ESMF_LOGKIND_Multi_On_Error = True\n"
  form *= "rectilinear_to_SCRIP(\"$srcwgt\", srclat, srclon, Opt)\ndelete(Opt)\n"
  form *= "dstfil = addfile(\"$dstsrc\", \"r\")\n"
  form *= "dstlat = dstfil->CLAT(0,:,:)\n"
  form *= "dstlon = dstfil->CLONG(0,:,:)\n"
  form *= "Opt              = True\n"
  form *= "Opt@DstRegional  = True\n"
# form *= "Opt@GridMask     = where(.not.ismissing(dstlat),1,0)\n"
  form *= "Opt@NoPETLog     = True\nOpt@ESMF_LOGKIND_Multi_On_Error = True\n"
  form *= "curvilinear_to_SCRIP(\"$dstwgt\", dstlat, dstlon, Opt)\ndelete(Opt)\n"
  form *= "Opt              = True\n"
  form *= "Opt@InterpMethod = \"bilinear\"\n"
  form *= "Opt@SrcRegional  = True\n"
  form *= "Opt@DstRegional  = True\n"
  form *= "Opt@NoPETLog     = True\nOpt@ESMF_LOGKIND_Multi_On_Error = True\n"
  form *= "ESMF_regrid_gen_weights(\"$srcwgt\", \"$dstwgt\", \"$nclwgt\", Opt)\ndelete(Opt)\n"
  form *= "end\n"
  fpa = My.ouvre(nclsrc, "w", false) ; write(fpa, form) ; close(fpa)
  print(" ncl $nclsrc\n")
     run(`ncl $nclsrc`)
            rm(nclsrc) ; rm(srcwgt) ; rm(dstwgt) ; rm("PET0.RegridWeightGen.Log") ; print("\n")
end

print("\n")
if !isfile(nclwgt)
  print("creating interpolation weights $nclwgt\n")
  create_nclwgt()
end

function interpolate(srcfil::AbstractString, dstfil::AbstractString, vara::AbstractString)
  form  = "load \"\$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl\"\n"
  form *= "load \"\$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl\"\n"      # then interpolate each ERA5 file
  form *= "load \"\$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl\"\n"  # (variable/year/month combination)
  form *= "load \"\$NCARG_ROOT/lib/ncarg/nclscripts/contrib/ut_string.ncl\"\n"
  form *= "load \"\$NCARG_ROOT/lib/ncarg/nclscripts/esmf/ESMF_regridding.ncl\"\n"
  form *= "begin\n"
  form *= "Opt              = True\n"
  form *= "Opt@NoPETLog     = True\nOpt@ESMF_LOGKIND_Multi_On_Error = True\n"
  form *= "fila = addfile(\"$srcfil\", \"r\")\n"
  form *= "filb = addfile(\"$dstfil\", \"c\")\n"
  form *= "vara = fila->$vara\n"
  form *= "varb = ESMF_regrid_with_weights(vara, \"$nclwgt\", Opt)\n"
  form *= "filb->$vara = varb\n"
  form *= "end\n"
  fpa = My.ouvre(nclsrc, "w", false) ; write(fpa, form) ; close(fpa)
  print(" ncl $nclsrc\n")
     run(`ncl $nclsrc`)
            rm(nclsrc) ; print("\n")
end

fils = ["t2",  "precip_snow", "clsw",  "clsw",  "press", "q2",  "u10", "v10"]
vars = ["t2m", "precip",      "therm", "solar", "sp",    "q2m", "u10", "v10"]
yeas = collect(parse(Int64, ARGS[1][1:4]) : parse(Int64, ARGS[2][1:4]))
mons = ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"]

for (fnum, file) in enumerate(fils)
  for year in yeas, mont in mons
    srcfil = @sprintf("../FORCING.era.src/ERA5_%s_y%dm%s.nc", file, year, mont)
    dstfil = @sprintf(                         "%d-%s_%s.nc",       year, mont, vars[fnum])
    if AGAIN || !isfile(dstfil)
      print("interpolating $srcfil to $dstfil\n")
      isfile(dstfil) && rm(dstfil)
      interpolate(srcfil, dstfil, vars[fnum])
    end
  end
end

function template(fn::AbstractString, nlat::Int64, nlon::Int64)               # define a NetCDF template using
  nctim = NcDim(       "Time",    1, values = [1])                            # HRLDAS-hr format (FORC_TYP = 1)
  nclat = NcDim("south_north", nlat, values = collect(1:nlat))
  nclon = NcDim(  "west_east", nlon, values = collect(1:nlon))
  nctat = NcVar(     "lat", [nclon, nclat       ], t=Float32, compress=-1)
  ncton = NcVar(     "lon", [nclon, nclat       ], t=Float32, compress=-1)
  ncair = NcVar(     "T2D", [nclon, nclat, nctim], t=Float32, compress=-1)
  ncshu = NcVar(     "Q2D", [nclon, nclat, nctim], t=Float32, compress=-1)
  ncuwd = NcVar(     "U2D", [nclon, nclat, nctim], t=Float32, compress=-1)
  ncvwd = NcVar(     "V2D", [nclon, nclat, nctim], t=Float32, compress=-1)
  ncprs = NcVar(    "PSFC", [nclon, nclat, nctim], t=Float32, compress=-1)
  ncrai = NcVar("RAINRATE", [nclon, nclat, nctim], t=Float32, compress=-1)
  ncswd = NcVar(  "SWDOWN", [nclon, nclat, nctim], t=Float32, compress=-1)
  nclwd = NcVar(  "LWDOWN", [nclon, nclat, nctim], t=Float32, compress=-1)
  ncfil = NetCDF.create(fn, [nctat, ncton, ncair, ncshu, ncuwd, ncvwd, ncprs, ncrai, ncswd, nclwd], mode = NC_NETCDF4)
  print("created $fn with $nlat lats and $nlon lons\n")
end

template(dstwgt, LATS, LONS)                                                  # and initialize invariants (lat/lon)
lats = ncread(dstsrc,  "CLAT", start=[1,1,1], count=[-1,-1,-1])[:,:,1]
lons = ncread(dstsrc, "CLONG", start=[1,1,1], count=[-1,-1,-1])[:,:,1]
ncwrite(lats, dstwgt,   "lat", start=[1,1],   count=[-1,-1])
ncwrite(lons, dstwgt,   "lon", start=[1,1],   count=[-1,-1])

srcvar = ["t2m",   "precip",  "therm",  "solar",   "sp", "q2m", "u10", "v10"]
dstvar = ["T2D", "RAINRATE", "LWDOWN", "SWDOWN", "PSFC", "Q2D", "U2D", "V2D"]

dnow = ARGS[1]                                                                # then copy the template and update the
flag = true                                                                   # variable forcing at each time step
while flag
  global dnow, flag
  year   = dnow[1:4]
  mont   =             dnow[6:7]
  srcref = dnow[1:4] * dnow[6:7] * "0100"
  srcnow = dnow[1:4] * dnow[6:7] * dnow[9:10] * dnow[12:13]
  srcind = round(Int64, datesous(srcref, srcnow, "hr") + 1)
  dstfil = srcnow * ".LDASIN_DOMAIN1"
  cp(dstwgt, dstfil; force = true)
  print("writing ")
  for (a, file) in enumerate(srcvar)
    print("$(srcvar[a]) ")
    srcfil = @sprintf("%s-%s_%s.nc", year, mont, srcvar[a])
    scl  = ncgetatt(srcfil, srcvar[a], "scale_factor") ; scl === nothing && (scl = 1.0)
    add  = ncgetatt(srcfil, srcvar[a],   "add_offset") ; add === nothing && (add = 0.0)
    vara =   ncread(srcfil, srcvar[a], start=[1,1,srcind], count=[-1,-1,1]) .* scl .+ add #; varb = vara[:,LATS:-1:1,:] .* scl .+ add
@show dstfil, size(vara)
    ncwrite(vara,   dstfil, dstvar[a], start=[1,1,     1], count=[-1,-1,1])
  end
  print("to $dstfil\n")
  dnow == ARGS[2] && (flag = false)
  dnow = dateadd(dnow, DELT, "hr")
end
exit(0)

#=
#          src_file = addfile(srcFileName,"r")
#          temp     = src_file->TEMP(0,0,:,:)
#          Opt@GridMask       = where(.not.ismissing(temp),1,0)    ; use "Mask2D" in NCL V6.2.1 and earlier
#          Opt@ForceOverwrite = True
#          Opt@PrintTimings   = True
#          rectilinear_to_SCRIP(srcGridName,temp&LAT,temp&LON,Opt)

Accumulated total precipitation 3-hourly Accumulation   apcp.yyyy.nc
Downward Longwave Rad. Flux     3-hourly forecast       dlwrf.yyyy.nc
Downward Shortwave Rad. Flux    3-hourly average        dswrf.yyyy.nc
Pressure                        3-hourly value          pres.sfc.yyyy.nc
Air Temperature at 2 m          3-hourly value  2m      air.2m.yyyy.nc
Specific Humidity at 2m         3-hourly value  2m      shum.2m.yyyy.nc
U-wind at 10 m                  3-hourly value  10m     uwnd.10m.yyyy.nc
V-wind at 10 m                  3-hourly value  10m     vwnd.10m.yyyy.nc
Values labeled 3 hourly values are output at that exact time (no averaging).
Values labeled accumulations are valid 3 hours later.
Values labeled 3 hourly averages are averaged over 0 to 3 hr later.

form *= "srcfil = addfile(\"$srcsrc\", \"r\")\n"
form *= "srcvar = srcfil->u10_0001(0,:,:)\n"
form *= "Opt              = True\n"
form *= "Opt@SrcRegional  = True\n"
form *= "Opt@NoPETLog     = True\nOpt@ESMF_LOGKIND_Multi_On_Error = True\n"
form *= "rectilinear_to_SCRIP(\"$erawgt\", srcvar&latitude, srcvar&longitude, Opt)\ndelete(Opt)\n"

fils = ["air.2m", "apcp", "dlwrf", "dswrf", "pres.sfc", "shum.2m", "uwnd.10m", "vwnd.10m"]
vars = ["air",    "apcp", "dlwrf", "dswrf", "pres",     "shum",    "uwnd",     "vwnd"    ]

  nctim = NcDim("Time",           1, values = [1])
  nclat = NcDim("south_north", nlat, values = collect(1:nlat))
  nclon = NcDim("west_east",   nlon, values = collect(1:nlon))
  nctat = NcVar("lat", [nclon, nclat], atts = Dict{Any,Any}(
                "units"                       => "degrees latitude",
                "description"                 => "Latitude on mass grid",
                "stagger"                     => "M"), t=Float32, compress=-1)
  ncton = NcVar("lon", [nclon, nclat], atts = Dict{Any,Any}(
                "units"                       => "degrees longitude",
                "description"                 => "Longitude on mass grid",
                "stagger"                     => "M"), t=Float32, compress=-1)
  ncair = NcVar("T2D", [nclon, nclat, nctim], atts = Dict{Any,Any}(
                "long_name"                   => "Temperature at 2 m",
                "standard_name"               => "air_temperature",
                "units"                       => "degK"), t=Float32, compress=-1)
  ncshu = NcVar("Q2D", [nclon, nclat, nctim], atts = Dict{Any,Any}(
                "long_name"                   => "Specific Humidity at 2 m",
                "standard_name"               => "specific_humidity",
                "units"                       => "kg kg-1"), t=Float32, compress=-1)
  ncuwd = NcVar("U2D", [nclon, nclat, nctim], atts = Dict{Any,Any}(
                "long_name"                   => "u-Component of wind at 10 m (Earth)",
                "standard_name"               => "eastward_wind",
                "units"                       => "m s-1"), t=Float32, compress=-1)
  ncvwd = NcVar("V2D", [nclon, nclat, nctim], atts = Dict{Any,Any}(
                "long_name"                   => "v-Component of wind at 10 m (Earth)",
                "standard_name"               => "northward_wind",
                "units"                       => "m s-1"), t=Float32, compress=-1)
  ncprs = NcVar("PSFC", [nclon, nclat, nctim], atts = Dict{Any,Any}(
                "long_name"                   => "Pressure at the Surface",
                "standard_name"               => "surface_air_pressure",
                "units"                       => "Pa"), t=Float32, compress=-1)
  ncrai = NcVar("RAINRATE", [nclon, nclat, nctim], atts = Dict{Any,Any}(
                "long_name"                   => "Rain rate",
                "standard_name"               => "large_scale_precipitation_amount",
                "units"                       => "mm s^-1"), t=Float32, compress=-1)
  ncswd = NcVar("SWDOWN", [nclon, nclat, nctim], atts = Dict{Any,Any}(
                "long_name"                   => "Shortwave Flux - Downward - at surface",
                "standard_name"               => "surface_downwelling_shortwave_flux_in_air",
                "units"                       => "W m-2"), t=Float32, compress=-1)
  nclwd = NcVar("LWDOWN", [nclon, nclat, nctim], atts = Dict{Any,Any}(
                "long_name"                   => "Longwave Flux - Downward - at surface",
                "standard_name"               => "surface_downwelling_longwave_flux_in_air",
                "units"                       => "W m-2"), t=Float32, compress=-1)
  ncfil = NetCDF.create(fn, [ncair, ncshu, ncuwd, ncvwd, ncprs, ncrai, ncswd, nclwd], mode = NC_NETCDF4)
=#
