#=
 = Submit all jobs that pertain to a given WRF domain (arc or atl) for
 = any two datasets (icowrf, icoera, wrfera, or erawrf) - RD June 2018.
 =#

using My, Printf

const SLEP             = 1.5                            # number of seconds to sleep between job submissions
const OSIZ             = 1                              # minimum number of bytes of a typical job output file
const PROG             = "diag.wrf.forcing.interp.nar.jl"  # job program

if (argc = length(ARGS)) != 0
  print("\nUsage: jjj $(basename(@__FILE__))\n\n")
  exit(1)
end

# listing = readdir(".")
for year = 2019:2020
  ystr = @sprintf("%4s", year)
  fila = "x." * PROG[1:end-2] * "_" * ystr * ".sbatch"

# file = filter(x -> contains(x, "$fila.o"), listing)
# fsiz = 0 ; size(file)[1] != 0 && (fsiz = filesize(file[end]))
# fsiz = 0 ; isfile(filo) && (fsiz = filesize(filo))
# if fsiz < OSIZ
    fpa = My.ouvre(fila, "w", false)
    form  = "#!/bin/tcsh\n"
    form *= "#SBATCH --account=def-wperrie\n"
    form *= "#SBATCH --time=00:29:00\n"
    form *= "#SBATCH --job-name=$(fila[1:end-7])\n"
    form *= "#SBATCH   --output=$(fila[1:end-1])o\n"
    form *= "#SBATCH --mem-per-cpu=2000M\n"
    form *= "echo Job running at \`hostname\`\n"
    form *= "echo Job running on \`grep \"model name\" /proc/cpuinfo | head -1\`\n"
    form *= "echo Job running in \`pwd\`\n"
    form *= "echo Job beginning  \`date\`\n"
    forl  = "julia /home/riced/bin/$PROG $ystr-01-01-00 $ystr-12-31-21\n"
    form *= forl * "echo Job ending at \`date\`\n"
    write(fpa, form) ; close(fpa)
    print(forl)
    print(   "sbatch $fila\n")
    try  run(`sbatch $fila`)  catch;
    print( "\nsbatch $fila FAILED\n\n")  end
    sleep(SLEP)
# end
end
exit(0)
