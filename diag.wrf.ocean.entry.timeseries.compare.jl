#=
 = Compute NSE performance metric for a specified timeseries of observations (C)
 = and model predictions (U), allowing for missing values - RD May 2023.
 =#

using My, Printf, NetCDF, Statistics

const DELT             = 24.0                           # timestep (hours)
const MISS             = -9999.0                        # generic missing value

if (argc = length(ARGS)) != 2
  print("\nUsage: jjj $(basename(@__FILE__))        c_flow.nc u_flow.nc\n")
  print(  "e.g.,  jjj $(basename(@__FILE__)) hydat_03JB004.nc streamflow.201810010000.CHRTOUT_GRID1.1096.137.166\n\n")
  exit(1)
end
fila = ARGS[1]
filb = ARGS[2]
filc = ARGS[2] * ".txt"

#fila = "hydat_03JB004.nc"
#filb = "streamflow.201810010000.CHRTOUT_GRID1.1096.137.166-0.nc"

function nse(fila::AbstractString, filb::AbstractString)
  tima = ncread(fila, "time", start=[1],     count=[-1])
  floa = ncread(fila, "flow", start=[1,1,1], count=[-1,-1,-1])[1,1,:]
  timb = ncread(filb, "time", start=[1],     count=[-1])
  flob = ncread(filb,  "tmp", start=[1,1,1], count=[-1,-1,-1])[1,1,:]

  vala = Array{Float64}(undef,0)
  valb = Array{Float64}(undef,0)
  for (b, tval) in enumerate(timb)
    a = findfirst(isequal(tval+12), tima)
    if floa[a] > 0 && flob[b] > 0
      push!(vala, floa[a])
      push!(valb, flob[b])
    end
  end
  numb = length(vala)
  nse  = 1 - var(valb .- vala) / var(vala)

  print("found $numb matching pairs in $fila and $filb\n")
  @sprintf("%5d %10.8f\n", numb, nse)
end

form  = ""                                                                    # calculate the metrics for each pair
form *= nse(fila, filb * "-0.nc")
form *= nse(fila, filb * "-1.nc")
form *= nse(fila, filb * "-2.nc")
form *= nse(fila, filb * "-3.nc")

fpa = My.ouvre(filc, "w")                                                     # then save the results to text
write(fpa, form)
close(fpa)
exit(0)

#=
vnam =            ARGS[1]
fbeg =            ARGS[2]
fend =            ARGS[3]
ndat = parse(Int, ARGS[4])
flet =            ARGS[5]
vals = Array{Float64}(undef,0)
tims = Array{Float64}(undef,0)
xloc = Array{  Int64}(undef,0)
yloc = Array{  Int64}(undef,0)

fpa = My.ouvre(flet, "r") ; lins = readlines(fpa) ; close(fpa)                # read river outlet locations (immediately
nlin = length(lins)                                                           # upriver of the corresponding ocean entries)
for a = 1:nlin
  tmpa = split(lins[a])
  push!(xloc, parse(Int64, tmpa[2]))
  push!(yloc, parse(Int64, tmpa[3]))
end

dnow = fbeg                                                                   # then construct the timeseries and store it
for a = 1:ndat
  global dnow
  fnam = "dat/" * dnow * fend
  if isfile(fnam)
    grid = ncread(fnam, vnam, start=[1,1,1], count=[-1,-1,-1])
    valu = 0
    for b = 1:nlin
      valu += grid[xloc[b],yloc[b],1]
    end
  else
    print("missing grid ($MISS value) for $fnam\n")
    valu = MISS
  end
  time = My.datesous("1900010100", dnow, "hr")
  push!(vals, valu) ; push!(tims, time)
  dnow = My.dateadd(dnow, DELT, "hr")
end

fnam = ARGS[1] * "." * ARGS[2] * ARGS[3] * "." * ARGS[4] * ".nc"
nccreer(      fnam, length(vals), 1, 1, MISS)
ncwrite(vals, fnam,  "tmp", start=[1,1,1], count=[-1,-1,-1])
ncwrite(tims, fnam, "time", start=[1],     count=[-1])
ncputatt(     fnam, "time", Dict("units" => "hours since 1900-01-01 00:00:0.0"))
exit(0)
=#
