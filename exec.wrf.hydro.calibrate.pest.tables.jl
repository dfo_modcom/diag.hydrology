#=
 = Transfer calibration values written by PEST to the corresponding WRF-Hydro
 = parameter files, with limits imposed where appropriate - RD Sep 2023. 
 =#

using My, Printf, NetCDF

const MISS             = -9999.0                        # generic missing value

if (argc = length(ARGS)) != 1
  print("\nUsage: jjj $(basename(@__FILE__)) OTHER.TBL\n\n")
  exit(1)
end
fpa = My.ouvre(ARGS[1], "r") ; lins = readlines(fpa) ; close(fpa)

fils = ["Fulldom_hires.nc", "soil_properties.nc"]
for file in fils
  full = "DOMAIN/" * file ; print("copy $full .\n")
  cp(full, file; force = true, follow_symlinks = true)
end

for line in lins
  vals = split(line) ; pnam = vals[1] ; pval = parse(Float64, vals[2])
  if (pnam == "bexp" || pnam == "dksat")                                                                                              && pval != 1
    fsrc = 2 ; @printf("updating %22s in %22s to %f\n", pnam, fils[fsrc], pval)
    grid = ncread(      fils[fsrc], pnam, start=[1,1,1,1], count=[-1,-1,-1,-1]) ; for a = 1:length(grid)                      grid[a] *= pval  end
          ncwrite(grid, fils[fsrc], pnam, start=[1,1,1,1], count=[-1,-1,-1,-1])
  end
  if  pnam == "smcmax"                                                                                                                && pval != 1
    fsrc = 2 ; @printf("updating %22s in %22s to %f\n", pnam, fils[fsrc], pval)
    grid = ncread(      fils[fsrc], pnam, start=[1,1,1,1], count=[-1,-1,-1,-1]) ; for a = 1:length(grid)  0 < grid[a] < 1 && (grid[a] *= pval)  end
          ncwrite(grid, fils[fsrc], pnam, start=[1,1,1,1], count=[-1,-1,-1,-1])
  end
  if  pnam ==   "mp"                                                                                                                  && pval != 1
    fsrc = 2 ; @printf("updating %22s in %22s to %f\n", pnam, fils[fsrc], pval)
    grid = ncread(      fils[fsrc], pnam, start=[1,1,1],   count=[-1,-1,-1])    ; for a = 1:length(grid)                      grid[a] *= pval  end
          ncwrite(grid, fils[fsrc], pnam, start=[1,1,1],   count=[-1,-1,-1])
  end
  if  pnam == "slope"
    fsrc = 2 ; @printf("updating %22s in %22s to %f\n", pnam, fils[fsrc], pval)
    grid = ncread(      fils[fsrc], pnam, start=[1,1,1],   count=[-1,-1,-1])    ; for a = 1:length(grid)                      grid[a]  = pval  end
          ncwrite(grid, fils[fsrc], pnam, start=[1,1,1],   count=[-1,-1,-1])
  end
  if  pnam == "mfsno"
    fsrc = 2 ; @printf("updating %22s in %22s to %f\n", pnam, fils[fsrc], pval)
    grid = ncread(      fils[fsrc], pnam, start=[1,1,1],   count=[-1,-1,-1])    ; for a = 1:length(grid)                      grid[a]  = pval  end
          ncwrite(grid, fils[fsrc], pnam, start=[1,1,1],   count=[-1,-1,-1])
  end
  if  pnam == "refkdt"
    fsrc = 2 ; @printf("updating %22s in %22s to %f\n", pnam, fils[fsrc], pval)
    grid = ncread(      fils[fsrc], pnam, start=[1,1,1],   count=[-1,-1,-1])    ; for a = 1:length(grid)                      grid[a]  = pval  end
          ncwrite(grid, fils[fsrc], pnam, start=[1,1,1],   count=[-1,-1,-1])
  end
  if (pnam == "RETDEPRTFAC" || pnam == "OVROUGHRTFAC")                                                                                && pval != 1
    fsrc = 1 ; @printf("updating %22s in %22s to %f\n", pnam, fils[fsrc], pval)
    grid = ncread(      fils[fsrc], pnam, start=[1,1],     count=[-1,-1])       ; for a = 1:length(grid)                      grid[a] *= pval  end
          ncwrite(grid, fils[fsrc], pnam, start=[1,1],     count=[-1,-1])
  end
end
exit(0)

#=
bexp               1.0
dksat              1.0
smcmax             1.0
mp                 1.0
slope              0.1
mfsno              2.5
refkdt             3.0
retdeprtfac        1.0
ovrghrtfac         1.0

soil_properties.nc
------------------
float bexp(Time, soil_layers_stag, south_north, west_east) ;
float cwpvt(Time, south_north, west_east) ;
float dksat(Time, soil_layers_stag, south_north, west_east) ;
float dwsat(Time, soil_layers_stag, south_north, west_east) ;
float hvt(Time, south_north, west_east) ;
float mfsno(Time, south_north, west_east) ;
float mp(Time, south_north, west_east) ;
float psisat(Time, soil_layers_stag, south_north, west_east) ;
float quartz(Time, soil_layers_stag, south_north, west_east) ;
float refdk(Time, south_north, west_east) ;
float refkdt(Time, south_north, west_east) ;
float rsurfexp(Time, south_north, west_east) ;
float slope(Time, south_north, west_east) ;
float smcdry(Time, soil_layers_stag, south_north, west_east) ;
float smcmax(Time, soil_layers_stag, south_north, west_east) ;
float smcref(Time, soil_layers_stag, south_north, west_east) ;
float smcwlt(Time, soil_layers_stag, south_north, west_east) ;
float vcmx25(Time, south_north, west_east) ;

Fulldom_hires.nc
----------------
int CHANNELGRID(y, x) ;
short FLOWDIRECTION(y, x) ;
int FLOWACC(y, x) ;
float TOPOGRAPHY(y, x) ;
float RETDEPRTFAC(y, x) ;
float OVROUGHRTFAC(y, x) ;
byte STREAMORDER(y, x) ;
int frxst_pts(y, x) ;
int basn_msk(y, x) ;
int LAKEGRID(y, x) ;
float landuse(y, x) ;
float LKSATFAC(y, x) ;
float LATITUDE(y, x) ;
float LONGITUDE(y, x) ;

GWBUCKPARM.nc
-------------
int Basin(feature_id) ;
float Coeff(feature_id) ;
float Expon(feature_id) ;
float Zmax(feature_id) ;
float Zinit(feature_id) ;
float Area_sqkm(feature_id) ;
int ComID(feature_id) ;
=#
