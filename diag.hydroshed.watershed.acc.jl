#=
 = Isolate the flow-accumulation that covers all watersheds of interest.  This assumes
 = prior conversion of 15-s HydroSHEDs from, say, .tif to .tif.nc (for example,
 = by gdal_translate tile.tif tile.nc), possibly followed by conversation
 = of the resulting .nc file back to .tif (and thus, an attempt is made to
 = duplicate the NetCDF metadata in "crs" that GDAL expects) - RD Apr 2022, Mar 2023.
 =#

using My, Printf, NetCDF

const LATS             = 12959                          # number of latitudes
const LONS             = 43200                          # number of longitudes
const DELL             =    0.0041666666666666666       # lat/lon grid spacing
const LATA             =   30.0020833333333333333       # first latitude on grid
const LONA             = -179.9979166666666666666       # first longitude on grid
const MISS             = Int32(-1)                      # generic missing value

if (argc = length(ARGS)) != 2
  print("\nUsage: jjj $(basename(@__FILE__)) hyd_glo_acc_15s.tif.nc hydrosheds_30_85N_180_00W_15sec_floacc.nc\n\n")
  exit(1)
end
fila = ARGS[1]
filb = ARGS[2]
print("\n")

function nccreate(fn::AbstractString, lats::Array{Float64,1}, lons::Array{Float64,1}, missing::Int32)
  nclat = NcDim("lat", length(lats), atts = Dict{Any,Any}(
                "standard_name"               => "latitude",
                "long_name"                   => "latitude",
                "units"                       => "degrees_north"), values = lats)
  nclon = NcDim("lon", length(lons), atts = Dict{Any,Any}(
                "standard_name"               => "longitude",
                "long_name"                   => "longitude",
                "units"                       => "degrees_east"), values = lons)
  nclen = NcDim("len", 0)
  ncchr = NcVar("crs", [nclen], atts = Dict{Any,Any}(                         # define a NetCDF grid that GDAL
                "grid_mapping_name"           => "latitude_longitude",        # would recognize when converting
                "long_name"                   => "CRS definition",            # from .nc to .tif (attributes are
                "longitude_of_prime_meridian" => 0.,                          # taken from the .tif.nc files that
                "semi_major_axis"             => 6378137.,                    # GDAL creates from the .tif tiles)
                "inverse_flattening"          => 298.257223563,
                "spatial_ref"                 => "GEOGCS[\"WGS 84\",DATUM[\"WGS_1984\",SPHEROID[\"WGS 84\",6378137,298.257223563,AUTHORITY[\"EPSG\",\"7030\"]],AUTHORITY[\"EPSG\",\"6326\"]],PRIMEM[\"Greenwich\",0],UNIT[\"degree\",0.0174532925199433,AUTHORITY[\"EPSG\",\"9122\"]],AXIS[\"Latitude\",NORTH],AXIS[\"Longitude\",EAST],AUTHORITY[\"EPSG\",\"4326\"]]",
                "crs_wkt"                     => "GEOGCS[\"WGS 84\",DATUM[\"WGS_1984\",SPHEROID[\"WGS 84\",6378137,298.257223563,AUTHORITY[\"EPSG\",\"7030\"]],AUTHORITY[\"EPSG\",\"6326\"]],PRIMEM[\"Greenwich\",0],UNIT[\"degree\",0.0174532925199433,AUTHORITY[\"EPSG\",\"9122\"]],AXIS[\"Latitude\",NORTH],AXIS[\"Longitude\",EAST],AUTHORITY[\"EPSG\",\"4326\"]]",
                "GeoTransform"                => "-180 0.00416666666666667 0 84.00000000000011 0 -0.00416666666666667 "), t=NC_CHAR, compress=-1)
  ncbnd = NcVar("Band1", [nclon, nclat], atts = Dict{Any,Any}(
                "long_name"                   => "GDAL Band Number 1",
                "_FillValue"                  => missing,
                "grid_mapping"                => "crs"), t=Int32, compress=-1)
  ncfil = NetCDF.create(fn, [ncchr, ncbnd], gatts = Dict{Any,Any}(
                "Conventions"                 => "CF-1.5",
                "GDAL"                        => "GDAL 3.0.4, released 2020/01/28"), mode = NC_NETCDF4)
  print("created $fn with $(length(lats)) lats and $(length(lons)) lons\n")
  return
end

lats = Array{Float64}(undef, LATS)                                            # define output dimensions and file
lons = Array{Float64}(undef, LONS)
for a = 1:LATS  lats[a] = LATA + DELL * (a - 1)  end
for a = 1:LONS  lons[a] = LONA + DELL * (a - 1)  end
nccreate(filb, lats, lons, MISS)

glat = ncread(fila, "lat", start=[1], count=[-1])                             # read the global input dimensions
glon = ncread(fila, "lon", start=[1], count=[-1])                             # and grid subset
dlat = round(Int, (LATA - glat[1]) / DELL)
dlon = round(Int, (LONA - glon[1]) / DELL)

print("reading $fila and extracting from [$(glat[1+dlat]),$(glat[LATS+dlat])] and [$(glon[1+dlon]),$(glon[LONS+dlon])]\n")
grdb = ncread(fila, "Band1", start=[1+dlon,1+dlat], count=[LONS,LATS])

misnum = 0                                                                    # then check for missing values
maxval = typemax(Int32)                                                       # and save in the desired type
grdc   =   Array{Int32}(undef, LONS, LATS)
for a = 1:LATS, b = 1:LONS
  global misnum
  if 0 < grdb[b,a] < maxval
    grdc[b,a] = Int32(grdb[b,a])
  else
#   grdb[b,a] != 4294967296 && print("warning: $(grdb[b,a]) is outside range (0, $maxval) so setting to $MISS\n")
    grdc[b,a] = MISS
    misnum += 1
  end
end
@printf("writing %s with %d missing values (%.1f%% of %d)\n\n", filb, misnum, 100*misnum/LATS/LONS, LATS*LONS)
ncwrite(grdc, filb, "Band1", start=[1,1], count=[-1,-1])
exit(0)
