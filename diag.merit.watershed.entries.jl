#=
 = Identify all terminal river flow outlets to the three oceans and their upstream
 = watersheds, create a watershed boundary grid, and output a text list of the outlet
 = locations.  Outlets are defined as the gridboxes immediately upriver of the ocean
 = entry points (i.e., where terminal streamflow is given in the CHRTOUT_GRID1 output).
 = These outlets can also double as seed locations for watershed island identification
 = (as given in diag.merit.watershed.islands.jl).  Note that latitude is flipped only
 = in the Fulldom_hires.nc grids (not in CHRTOUT_GRID1), so outlet/seed location, as
 = well as grids read from Fulldom_hires, are reversed below.  All watersheds are
 = numbered according to the y_rev,x-indices of their outlet gridboxes (immediately
 = upriver of the corresponding ocean entry points) - RD Mar 2021, Apr 2022.
 =#

using My, Printf, NetCDF, LibGEOS

if     in("WRFSDOM", keys(ENV)) && ENV["WRFSDOM"] == "_AuxF"
  const GATS             =    9                         # number of  latitudes in geogrid.nc
  const GONS             =    9                         # number of longitudes in geogrid.nc
  const FATS             =  225                         # number of  latitudes in Fulldom_hires.nc
  const FONS             =  225                         # number of longitudes in Fulldom_hires.nc
elseif in("WRFSDOM", keys(ENV)) && ENV["WRFSDOM"] == "_Cali"
  const GATS             =   31                         # number of  latitudes in geogrid.nc
  const GONS             =   57                         # number of longitudes in geogrid.nc
  const FATS             =  775                         # number of  latitudes in Fulldom_hires.nc
  const FONS             = 1425                         # number of longitudes in Fulldom_hires.nc
elseif in("WRFSDOM", keys(ENV)) && ENV["WRFSDOM"] == "_East"
  const GATS             =   54                         # number of  latitudes in geogrid.nc
  const GONS             =   62                         # number of longitudes in geogrid.nc
  const FATS             = 1350                         # number of  latitudes in Fulldom_hires.nc
  const FONS             = 1550                         # number of longitudes in Fulldom_hires.nc
else
  const GATS             =   86                         # number of  latitudes in geogrid.nc
  const GONS             =  108                         # number of longitudes in geogrid.nc
  const FATS             = 2150                         # number of  latitudes in Fulldom_hires.nc
  const FONS             = 2750                         # number of longitudes in Fulldom_hires.nc
end

if (argc = length(ARGS)) != 2
  print("\nUsage: jjj $(basename(@__FILE__)) ../DOMAIN_full_full/ ..\n")
  exit(1)                                                                     # define input and output files
end
#fnam =       "geomask.nc" ;                         filw = fnam[1:end-3] * "_outlets.nc"
#fnam =       "geogrid.nc" ; fila = ARGS[1] * fnam ; filx = fnam[1:end-3] * "_outlets.nc"
 fnam = "Fulldom_hires.nc" ; filb = ARGS[1] * fnam ; fily = fnam[1:end-3] * "_outlets.nc"
                                                     filz = fnam[1:end-3] * "_outlets.txt"
                                                     fizz = fnam[1:end-3] * "_outletz.txt"

function subroute()
##print("\ncopying $fila to $filw\n") ; cp(fila, filw; force=true)
##print(  "copying $fila to $filx\n") ; cp(fila, filx; force=true)
  print(  "copying $filb to $fily\n") ; cp(filb, fily; force=true)

  flat = Array{Float32}(undef, FONS, FATS)                                    # read (flipped) grids of flow
  flon = Array{Float32}(undef, FONS, FATS)                                    # accumulation, flow direction,
  facc =   Array{Int32}(undef, FONS, FATS)                                    # and indices of rivers, lakes;
  fdir =   Array{Int16}(undef, FONS, FATS)                                    # evidently, a few gridboxes are
  friv = Set{Tuple{Int32, Int32}}()                                           # ocean/lake or river/lake, and
  flak = Set{Tuple{Int32, Int32}}()                                           # respectively, these are treated
  foce = Set{Tuple{Int32, Int32}}()                                           # (only) as ocean or river below
  temp = ncread(filb,      "LATITUDE", start=[1,1], count=[-1,-1])
  for a = 1:FATS, b = 1:FONS  flat[b,a] = temp[b,FATS+1-a]  end
  temp = ncread(filb,     "LONGITUDE", start=[1,1], count=[-1,-1])
  for a = 1:FATS, b = 1:FONS  flon[b,a] = temp[b,FATS+1-a]  end
  temp = ncread(filb,       "FLOWACC", start=[1,1], count=[-1,-1])
  for a = 1:FATS, b = 1:FONS  facc[b,a] = temp[b,FATS+1-a]  end
  temp = ncread(filb, "FLOWDIRECTION", start=[1,1], count=[-1,-1])
  for a = 1:FATS, b = 1:FONS  fdir[b,a] = temp[b,FATS+1-a]  end
  temp = ncread(filb,   "CHANNELGRID", start=[1,1], count=[-1,-1])
  for a = 1:FATS, b = 1:FONS  temp[b,a] >= 0 && push!(friv, (Int32(b), Int32(FATS+1-a)))  end
  temp = ncread(filb,      "LAKEGRID", start=[1,1], count=[-1,-1])
  for a = 1:FATS, b = 1:FONS  temp[b,a] >= 0 && push!(flak, (Int32(b), Int32(FATS+1-a)))  end
  for a = 1:FATS, b = 1:FONS
                       fdir[b,a] == 0   && (facc[b,a] = typemin(Int32))
    in((b,a), flak) && fdir[b,a] == 0   && (setdiff!(flak, [(b,a)]) ; push!(foce, (b,a)))
    in((b,a), friv) && fdir[b,a] == 0   && (setdiff!(friv, [(b,a)]) ; push!(foce, (b,a)))
    in((b,a), friv) &&  in((b,a), flak) &&  setdiff!(flak, [(b,a)])
  end
  @printf("\n%9d gridboxes define the       river network\n",   length(friv))
  @printf(  "%9d gridboxes define the inland lake network\n",   length(flak))
  @printf(  "%9d gridboxes define the  ocean lake network\n\n", length(foce))

  numb = 0                                                                    # define the ocean by fdir (-1) excluding
  for a = 1:FATS, b = 1:FONS                                                  # ocean entry points and inland sinks (0)
    fdir[b,a] == 0 && (fdir[b,a] = Int16(-1) ; numb += 1)                     # where river flow accumulation is a peak
  end
  @printf("%9d gridboxes define the ocean (values of -1)\n", numb)

  fter = Set{Tuple{Int32, Int32}}()                                           # cast ocean entry points as the only
  function terminus(b::Int32, a::Int32)                                       # locations where fdir is still zero
    (fdir[b,a] == 128) && (fdir[b+1,a+1] == -1) && return(true)
    (fdir[b,a] ==  64) && (fdir[b  ,a+1] == -1) && return(true)
    (fdir[b,a] ==  32) && (fdir[b-1,a+1] == -1) && return(true)
    (fdir[b,a] ==   1) && (fdir[b+1,a  ] == -1) && return(true)
    (fdir[b,a] ==  16) && (fdir[b-1,a  ] == -1) && return(true)
    (fdir[b,a] ==   2) && (fdir[b+1,a-1] == -1) && return(true)
    (fdir[b,a] ==   4) && (fdir[b  ,a-1] == -1) && return(true)
    (fdir[b,a] ==   8) && (fdir[b-1,a-1] == -1) && return(true)
    false
  end

  for a = 2:FATS-1, b = 2:FONS-1
    in((b,a), friv) && terminus(Int32(b), Int32(a)) && (setdiff!(friv, [(b,a)]) ; fdir[b,a] = 0 ; push!(fter, (b,a)))
  end
  @printf("%9d gridboxes define ocean entry points (fdir = 0)\n", length(fter))

  fent = Set{Tuple{Int32, Int32}}()                                           # identify all ocean entry points
  function waterway(b::Int32, a::Int32)                                       # that are connected to rivers
    in((b-1,a-1), friv) && (fdir[b-1,a-1] == 128) && return(true)
    in((b  ,a-1), friv) && (fdir[b  ,a-1] ==  64) && return(true)
    in((b+1,a-1), friv) && (fdir[b+1,a-1] ==  32) && return(true)
    in((b-1,a  ), friv) && (fdir[b-1,a  ] ==   1) && return(true)
    in((b+1,a  ), friv) && (fdir[b+1,a  ] ==  16) && return(true)
    in((b-1,a+1), friv) && (fdir[b-1,a+1] ==   2) && return(true)
    in((b  ,a+1), friv) && (fdir[b  ,a+1] ==   4) && return(true)
    in((b+1,a+1), friv) && (fdir[b+1,a+1] ==   8) && return(true)
    false
  end

  for a = 2:FATS-1, b = 2:FONS-1
    (fdir[b,a] == 0) && waterway(Int32(b), Int32(a)) && push!(fent, (b,a))
  end
  @printf("%9d gridboxes define ocean entry points connected to rivers (values of -2)\n", length(fent))

  fout =  Set{Tuple{Int32, Int32}}()                                          # and specifically those longer
  otoe = Dict{Tuple{Int32, Int32}, Tuple{Int32, Int32}}()                     # than a single gridbox, then save
  function yawretaw(b::Int32, a::Int32)                                       # the river flow outlets (fout) and
    pass = false
    in((b-1,a-1), friv) && (fdir[b-1,a-1] == 128) && waterway(Int32(b-1), Int32(a-1)) && (push!(fout, (b-1,a-1)) ; otoe[(b-1,a-1)] = (b,a) ; pass = true)
    in((b  ,a-1), friv) && (fdir[b  ,a-1] ==  64) && waterway(Int32(b  ), Int32(a-1)) && (push!(fout, (b  ,a-1)) ; otoe[(b  ,a-1)] = (b,a) ; pass = true)
    in((b+1,a-1), friv) && (fdir[b+1,a-1] ==  32) && waterway(Int32(b+1), Int32(a-1)) && (push!(fout, (b+1,a-1)) ; otoe[(b+1,a-1)] = (b,a) ; pass = true)
    in((b-1,a  ), friv) && (fdir[b-1,a  ] ==   1) && waterway(Int32(b-1), Int32(a  )) && (push!(fout, (b-1,a  )) ; otoe[(b-1,a  )] = (b,a) ; pass = true)
    in((b+1,a  ), friv) && (fdir[b+1,a  ] ==  16) && waterway(Int32(b+1), Int32(a  )) && (push!(fout, (b+1,a  )) ; otoe[(b+1,a  )] = (b,a) ; pass = true)
    in((b-1,a+1), friv) && (fdir[b-1,a+1] ==   2) && waterway(Int32(b-1), Int32(a+1)) && (push!(fout, (b-1,a+1)) ; otoe[(b-1,a+1)] = (b,a) ; pass = true)
    in((b  ,a+1), friv) && (fdir[b  ,a+1] ==   4) && waterway(Int32(b  ), Int32(a+1)) && (push!(fout, (b  ,a+1)) ; otoe[(b  ,a+1)] = (b,a) ; pass = true)
    in((b+1,a+1), friv) && (fdir[b+1,a+1] ==   8) && waterway(Int32(b+1), Int32(a+1)) && (push!(fout, (b+1,a+1)) ; otoe[(b+1,a+1)] = (b,a) ; pass = true)
    pass
  end

  for fnex in fent                                                            # map outlets to ocean entries and
    !yawretaw(fnex[1], fnex[2]) && setdiff!(fent, [(fnex[1], fnex[2])])       # de-select single-gridbox rivers
  end
  @printf("%9d gridboxes define ocean entry points connected to multiple-gridbox rivers (values of -2)\n", length(fent))

  filp = ARGS[2] * "/EEZ/EEZ_Land_v3_202030_contiguous_Alaska.csv"    ; fpp = My.ouvre(filp, "r") ; alin = readline(fpp) ; close(fpp)
  filp = ARGS[2] * "/EEZ/EEZ_Land_v3_202030_contiguous_Canada.csv"    ; fpp = My.ouvre(filp, "r") ; clin = readline(fpp) ; close(fpp)
  filp = ARGS[2] * "/EEZ/EEZ_Land_v3_202030_contiguous_Greenland.csv" ; fpp = My.ouvre(filp, "r") ; glin = readline(fpp) ; close(fpp)
  abox = readgeom("POLYGON(($alin))")
  cbox = readgeom("POLYGON(($clin))")                                         # save all (fora) or just the northern
  gbox = readgeom("POLYGON(($glin))")                                         # (forn) river outlet and ocean entry
  fora = "" ; forn = ""                                                       # points to sorted text files
  for fnex in fout
    if      fnex[2] > 9999 || fnex[2] + 10000 * fnex[1] > typemax(Int32)
      @show fnex[2] > 9999 || fnex[2] + 10000 * fnex[1] > typemax(Int32), fnex[2], fnex[1], typemax(Int32)
      error("at least one gridbox index is too large to share five digits of an Int32\n")
    end
    fnee = fnex[2] + Int32(10000) * fnex[1] ; fnxx = otoe[fnex]
    line = @sprintf("%10d %5d %5d %5d %15.8f %15.8f %5d %5d %5d %15.8f %15.8f\n", fnee,
           fnex[1], fnex[2], FATS+1-fnex[2], flat[fnex[1],fnex[2]], flon[fnex[1],fnex[2]],
           fnxx[1], fnxx[2], FATS+1-fnxx[2], flat[fnxx[1],fnxx[2]], flon[fnxx[1],fnxx[2]])
    fora *= line
    llpt = readgeom("POINT($(flon[fnex[1],fnex[2]]) $(flat[fnex[1],fnex[2]]))")
    if contains(abox, llpt) || contains(cbox, llpt) || contains(gbox, llpt)
      forn *= line
    end
  end
  fora = join(sort(split(fora[1:end-1], "\n")), "\n") * "\n"
  forn = join(sort(split(forn[1:end-1], "\n")), "\n") * "\n"
  fpa = My.ouvre(filz, "w") ; write(fpa, fora) ; close(fpa)
  fpa = My.ouvre(fizz, "w") ; write(fpa, forn) ; close(fpa)
  @printf("%9d gridboxes define solitary, multi-gridbox river outflows to the ocean (values of -2)\n", length(fout))

  for fnex in fout                                                            # initialize each river outlet gridbox
#   fnxx = otoe[fnex] #; fnxx = fnex                                          # in facc using an ocean entry (or river
#   facc[fnex[1],fnex[2]] = fnxx[2] + Int32(10000) * fnxx[1]                  # outlet) number that is assigned to the
    facc[fnex[1],fnex[2]] = fnex[2] + Int32(10000) * fnex[1]                  # entire watershed, and save river outlets
  end
  fsav = deepcopy(fout)

  fwat = Base.union(friv, flak)
# for a = 1:FATS, b = 1:FONS
#   in((b,a), fwat) && (fdir[b,a] == 0) && setdiff!(fwat, [(b,a)])
# end
  function watermrk(b::Int32, a::Int32)
    numb = 0
    in((b-1,a-1), fwat) && (fdir[b-1,a-1] == 128) && (setdiff!(fwat, [(b-1,a-1)]) ; push!(fout, (b-1,a-1)) ; facc[b-1,a-1] = facc[b,a] ; numb += 1)
    in((b  ,a-1), fwat) && (fdir[b  ,a-1] ==  64) && (setdiff!(fwat, [(b  ,a-1)]) ; push!(fout, (b  ,a-1)) ; facc[b  ,a-1] = facc[b,a] ; numb += 1)
    in((b+1,a-1), fwat) && (fdir[b+1,a-1] ==  32) && (setdiff!(fwat, [(b+1,a-1)]) ; push!(fout, (b+1,a-1)) ; facc[b+1,a-1] = facc[b,a] ; numb += 1)
    in((b-1,a  ), fwat) && (fdir[b-1,a  ] ==   1) && (setdiff!(fwat, [(b-1,a  )]) ; push!(fout, (b-1,a  )) ; facc[b-1,a  ] = facc[b,a] ; numb += 1)
    in((b+1,a  ), fwat) && (fdir[b+1,a  ] ==  16) && (setdiff!(fwat, [(b+1,a  )]) ; push!(fout, (b+1,a  )) ; facc[b+1,a  ] = facc[b,a] ; numb += 1)
    in((b-1,a+1), fwat) && (fdir[b-1,a+1] ==   2) && (setdiff!(fwat, [(b-1,a+1)]) ; push!(fout, (b-1,a+1)) ; facc[b-1,a+1] = facc[b,a] ; numb += 1)
    in((b  ,a+1), fwat) && (fdir[b  ,a+1] ==   4) && (setdiff!(fwat, [(b  ,a+1)]) ; push!(fout, (b  ,a+1)) ; facc[b  ,a+1] = facc[b,a] ; numb += 1)
    in((b+1,a+1), fwat) && (fdir[b+1,a+1] ==   8) && (setdiff!(fwat, [(b+1,a+1)]) ; push!(fout, (b+1,a+1)) ; facc[b+1,a+1] = facc[b,a] ; numb += 1)
    numb
  end

  totl = length(fout)                                                         # using the combined river and lake
  numb = 1                                                                    # indices (fwat), identify upstream
  while (numb > 0)                                                            # water networks connected to each
    numb = 0                                                                  # ocean entry point (or river outlet)
    for fnex in fout
      numb += watermrk(fnex[1], fnex[2])
    end
    totl += numb
#   @printf("%9d gridboxes are connected to the outflow (values of -2)\n", numb)
  end
  @printf("%9d gridboxes define the upstream rivers, lakes, and connected outflow(s) (values of -2)\n", totl)

  function watershed(b::Int64, a::Int64)
    numb = 0
    fdir[b-1,a-1] == 128 && (fdir[b-1,a-1] = -2 ; facc[b-1,a-1] = facc[b,a] ; numb += 1)
    fdir[b  ,a-1] ==  64 && (fdir[b  ,a-1] = -2 ; facc[b  ,a-1] = facc[b,a] ; numb += 1)
    fdir[b+1,a-1] ==  32 && (fdir[b+1,a-1] = -2 ; facc[b+1,a-1] = facc[b,a] ; numb += 1)
    fdir[b-1,a  ] ==   1 && (fdir[b-1,a  ] = -2 ; facc[b-1,a  ] = facc[b,a] ; numb += 1)
    fdir[b+1,a  ] ==  16 && (fdir[b+1,a  ] = -2 ; facc[b+1,a  ] = facc[b,a] ; numb += 1)
    fdir[b-1,a+1] ==   2 && (fdir[b-1,a+1] = -2 ; facc[b-1,a+1] = facc[b,a] ; numb += 1)
    fdir[b  ,a+1] ==   4 && (fdir[b  ,a+1] = -2 ; facc[b  ,a+1] = facc[b,a] ; numb += 1)
    fdir[b+1,a+1] ==   8 && (fdir[b+1,a+1] = -2 ; facc[b+1,a+1] = facc[b,a] ; numb += 1)
    numb
  end

  for fnex in fout  fdir[fnex[1],fnex[2]] = -2  end                           # then identify the remainder of
  numb = 1                                                                    # each watershed using fdir < 0
  while (numb > 0)
    numb = 0
    for a = 2:FATS-1, b = 2:FONS-1
      fdir[b,a] < -1 && (numb += watershed(b, a))
    end
    totl += numb
  end
  @printf("%9d gridboxes define the upstream watershed without a border (values of -2)\n", totl)

  tmpa = Array{Int32}(undef, FONS, FATS)
  for fnex in fent  facc[fnex[1],fnex[2]]  = typemin(Int32)  end              # and finlly, set ocean entry and
  for fnex in foce  facc[fnex[1],fnex[2]]  = typemin(Int32)  end              # inland sinks to ocean, and outlet
  for fnex in fsav  facc[fnex[1],fnex[2]] *= -1              end              # points to negative and write facc
  for a = 1:FATS, b = 1:FONS
    facc[b,a] > 0 && fdir[b,a] == 0 && (facc[b,a] = typemin(Int32))
    tmpa[b,FATS+1-a] = facc[b,a]
  end
  ncwrite(tmpa, fily, "FLOWACC", start=[1,1], count=[-1,-1])
end

subroute()
exit(0)

#=
  ftmp = Set{Tuple{Int32, Int32}}()
  function yawretaw(b::Int32, a::Int32)
    numb = 0
    in((b-1,a-1), fent) && (fdir[b-1,a-1] == 128) && (facc[b-1,a-1] < facc[b,a]) && (push!(ftmp, (b-1,a-1)) ; numb += 1)
    in((b  ,a-1), fent) && (fdir[b  ,a-1] ==  64) && (facc[b  ,a-1] < facc[b,a]) && (push!(ftmp, (b  ,a-1)) ; numb += 1)
    in((b+1,a-1), fent) && (fdir[b+1,a-1] ==  32) && (facc[b+1,a-1] < facc[b,a]) && (push!(ftmp, (b+1,a-1)) ; numb += 1)
    in((b-1,a  ), fent) && (fdir[b-1,a  ] ==   1) && (facc[b-1,a  ] < facc[b,a]) && (push!(ftmp, (b-1,a  )) ; numb += 1)
    in((b+1,a  ), fent) && (fdir[b+1,a  ] ==  16) && (facc[b+1,a  ] < facc[b,a]) && (push!(ftmp, (b+1,a  )) ; numb += 1)
    in((b-1,a+1), fent) && (fdir[b-1,a+1] ==   2) && (facc[b-1,a+1] < facc[b,a]) && (push!(ftmp, (b-1,a+1)) ; numb += 1)
    in((b  ,a+1), fent) && (fdir[b  ,a+1] ==   4) && (facc[b  ,a+1] < facc[b,a]) && (push!(ftmp, (b  ,a+1)) ; numb += 1)
    in((b+1,a+1), fent) && (fdir[b+1,a+1] ==   8) && (facc[b+1,a+1] < facc[b,a]) && (push!(ftmp, (b+1,a+1)) ; numb += 1)
    numb
  end

  for fnex in fent                                                            # then de-select (among connected
    yawretaw(fnex[1], fnex[2])                                                # duplicates) the non-terminating
  end                                                                         # (smaller accumulation) points
  for fnex in ftmp
    setdiff!(fent, [(fnex[1], fnex[2])])
       push!(friv,  (fnex[1], fnex[2]) )
  end
  totl -= length(ftmp)
  @printf("%9d gridboxes define  solitary river outflows to the ocean (values of -2)\n", totl)
=#

#=
# temp = Array{Int16}(undef, FONS, FATS)
#   @printf("%9d gridboxes are connected to the upstream watersheds (values of -2)\n", numb)
#   for a = 1:FATS, b = 1:FONS  temp[b,FATS+1-a] = fdir[b,a]  end
#   ncwrite(temp, fily, "FLOWDIRECTION", start=[1,1], count=[-1,-1])

  function square(b::Int64, a::Int64)                                         # create a blocky (low-res) mask
    numb = 0                                                                  # but only where the full-res grid
    for c = 0:9, d = 0:9                                                      # has no ocean (-1)
      fdir[b+d,a+c] != -2 && (fdir[b+d,a+c] = -2 ; numb += 1)
    end
    numb
  end

  gask = ncread(fila, "LANDMASK", start=[1,1,1], count=[-1,-1,-1])            # and save this at both full and
  numb = 0                                                                    # low resolution (1=land 0=water
  for a = 1:10:FATS, b = 1:10:FONS                                            # becomes -3 and -4 so that other
    wshed = false ; ocean = false                                             # low-res grids only change where
    for c = 0:9, d = 0:9                                                      # negative values occur, but these
      fdir[b+d,a+c] == -1 && (ocean = true)                                   # are later stored as 0=water on
      fdir[b+d,a+c] == -2 && (wshed = true)                                   # the low-res grid); if a low-res
    end                                                                       # gridbox contains -1 (ocean at
    if !ocean                                                                 # full res), then leave both the
      wshed && (numb += square(b, a))                                         # low-res and full-res grids alone
      if !wshed
        c = div(a - 1, 10) + 1
        d = div(b - 1, 10) + 1
        gask[d,c,1] == 1 && (gask[d,c,1] = -3)
        gask[d,c,1] == 0 && (gask[d,c,1] = -4)
      end
    end
  end
  totl += numb
  @printf("%9d gridboxes define the upstream watershed with low-res blockiness on land (values of -2)\n\n", totl)

  temp = Array{Int16}(undef, FONS, FATS)
  for a = 1:FATS, b = 1:FONS  temp[b,FATS+1-a] = fdir[b,a]  end
  ncwrite(temp, fily, "FLOWDIRECTION", start=[1,1],   count=[-1,-1])
  ncwrite(gask, filw,      "LANDMASK", start=[1,1,1], count=[-1,-1,-1])
=#

#=
  tmpa = zeros(Int32,        FONS, FATS)
  tmpb = Array{Int32}(undef, FONS, FATS)
# for fnex in fent  tmpa[fnex[1],fnex[2]] = -2  end
  for fnex in fout  1670 < fnex[1] < 1730 && @show fnex[1],FATS+1-fnex[2],-(100000 * fnex[1] + FATS+1-fnex[2])  end
  for fnex in fout  tmpa[fnex[1],fnex[2]] = -(100000 * fnex[1] + FATS+1-fnex[2])  end  #FATS+1-fnex[2])  end # + 100000 * fnex[1])  end
  for a = 1:FATS, b = 1:FONS  tmpb[b,FATS+1-a] = tmpa[b,a]  end
  ncwrite(tmpb, fily, "FLOWACC", start=[1,1], count=[-1,-1])

tmpd = Array{Int32}(undef, FONS, FATS)
tmpc = ncread(fily, "FLOWACC", start=[1,1], count=[-1,-1])
for a = 1:FATS, b = 1:FONS  tmpd[b,FATS+1-a] = tmpc[b,a]  end
for fnex in fout  1670 < fnex[1] < 1730 && @show fnex[1],FATS+1-fnex[2],-(100000 * fnex[1] + FATS+1-fnex[2]), tmpd[fnex[1],fnex[2]]  end

#     print("$b $(FATS+1-a)\n")
#     for aa = -1:1, bb = -1:1
#       @show facc[b+bb,a+aa], in((b+bb,a+aa), friv), in((b+bb,a+aa), fent), in((b+bb,a+aa), flak)
#     end

# function border(b::Int64, a::Int64)                                         # then add an outer border
#   numb = 0
#   fdir[b-1,a-1] >= 0 && (mask[b-1,a-1] = -2 ; numb += 1)
#   fdir[b  ,a-1] >= 0 && (mask[b  ,a-1] = -2 ; numb += 1)
#   fdir[b+1,a-1] >= 0 && (mask[b+1,a-1] = -2 ; numb += 1)
#   fdir[b-1,a  ] >= 0 && (mask[b-1,a  ] = -2 ; numb += 1)
#   fdir[b+1,a  ] >= 0 && (mask[b+1,a  ] = -2 ; numb += 1)
#   fdir[b-1,a+1] >= 0 && (mask[b-1,a+1] = -2 ; numb += 1)
#   fdir[b  ,a+1] >= 0 && (mask[b  ,a+1] = -2 ; numb += 1)
#   fdir[b+1,a+1] >= 0 && (mask[b+1,a+1] = -2 ; numb += 1)
#   numb
# end

# mask = deepcopy(fdir)                                                       # by looping a few times
# for c = 1:5
#   numb = 0
#   for a = 2:FATS-1, b = 2:FONS-1
#     fdir[b,a] == -2 && (numb += border(b, a))
#   end
#   totl += numb
#   for a = 1:FATS, b = 1:FONS  fdir[b,a] = mask[b,a]  end
# end
# @printf("%9d gridboxes define the upstream watershed with a border (values of -2)\n", totl)

MASK && masklow(fila, filb, filc, filw, filx, fily, filz, sedx, sedy)         # first use the filw low-res mask to
gask = ncread(filw,   "LANDMASK", start=[1,1,1], count=[-1,-1,-1])            # modify the high-res DEM (dask), but
fdir = ncread(filb, "TOPOGRAPHY", start=[1,1],   count=[-1,-1])               # note that grids are assumed to be on
dask = ncread(filc,      "Band1", start=[1,1],   count=[-1,-1])               # Mercator and lat/lon projections, so
glat = ncread(fila,       "CLAT", start=[1,1,1], count=[ 1,-1, 1])[1,:,1]     # only a slice of each lat/lon grid is
glon = ncread(fila,      "CLONG", start=[1,1,1], count=[-1, 1, 1])[:,1,1]     # read (the whole grid would be needed
flat = ncread(filb,   "LATITUDE", start=[1,1],   count=[ 1,-1])[1,:]          # for a Lambert conformal projection)
flon = ncread(filb,  "LONGITUDE", start=[1,1],   count=[-1, 1])[:,1]
dlat = ncread(filc,        "lat", start=[1],     count=[-1])
dlon = ncread(filc,        "lon", start=[1],     count=[-1])

ilat = Array{Int64}(undef, length(dlat))                                      # find the nearest location in gask
ilon = Array{Int64}(undef, length(dlon))                                      # to each location in the DEM, then
for a = 1:length(dlat)  ilat[a] = findmin(abs.(glat .- dlat[a]))[2]  end      # switch external watersheds to ocean
for a = 1:length(dlon)  ilon[a] = findmin(abs.(glon .- dlon[a]))[2]  end      # and save the modified DEM to filz
numb = 0
for a = 1:DATS, b = 1:DONS
  global numb
  gask[ilon[b],ilat[a]] < 0 && (dask[b,a] = DISS ; numb += 1)
end
ncwrite(dask, filz, "Band1", start=[1,1], count=[-1,-1])
@printf("\n%10d gridboxes out of %12d (%4.1f%%) are now ocean in %s\n",
        numb, length(dlat) * length(dlon), 100 * numb / length(dlat) / length(dlon), filz)

ilat = Array{Int64}(undef, length(flat))                                      # also find the nearest location in the
ilon = Array{Int64}(undef, length(flon))                                      # modified DEM to each location in fdir
for a = 1:length(flat)  ilat[a] = findmin(abs.(dlat .- flat[a]))[2]  end      # and save the subsampled DEM (only for
for a = 1:length(flon)  ilon[a] = findmin(abs.(dlon .- flon[a]))[2]  end      # visualization of fily)
for a = 1:FATS, b = 1:FONS
  fdir[b,a] = dask[ilon[b],ilat[a]]
end
ncwrite(fdir, fily, "TOPOGRAPHY", start=[1,1],   count=[-1,-1])

vars = ["LU_INDEX", "HGT_M", "SOILTEMP", "SCT_DOM", "SCB_DOM", "SNOALB", "CON", "VAR", "OA1", "OA2", "OA3", "OA4", "OL1", "OL2", "OL3", "OL4", "VAR_SSO"]
vals = [        17,       0,          0,        14,        14,        0,     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,         0]
for (ind, var) in enumerate(vars)
  grid = ncread(fila, var, start=[1,1,1], count=[-1,-1,-1])
  for a = 1:GATS, b = 1:GONS
    gask[b,a,1] < 0 && (grid[b,a,1] = vals[ind])                              # then use the filw low-res mask
  end                                                                         # to modify low-res vars in fila
  ncwrite(grid, filw, var, start=[1,1,1], count=[-1,-1,-1])                   # (both 2D and 3D modifications,
  ncwrite(grid, filx, var, start=[1,1,1], count=[-1,-1,-1])                   # which are saved in filw and filx)
end

vars = ["LANDUSEF", "SOILCTOP", "SOILCBOT", "ALBEDO12M", "GREENFRAC", "LAI12M"]
vals = [         0,          0,          0,           8,           0,        0]
levs = [        21,         16,         16,          12,          12,       12]
valt = [         1,          1,          1,         -99,         -99,      -99]
levt = [        17,         14,         14,         -99,         -99,      -99]
for (ind, var) in enumerate(vars)
  grid = ncread(fila, var, start=[1,1,1,1], count=[-1,-1,-1,-1])
  for a = 1:levs[ind], b = 1:GATS, c = 1:GONS
    if a != levt[ind]
      gask[c,b,1] < 0 && (grid[c,b,a,1] = vals[ind])
    else
      gask[c,b,1] < 0 && (grid[c,b,a,1] = valt[ind])
    end
  end
  ncwrite(grid, filw, var, start=[1,1,1,1], count=[-1,-1,-1,-1])
  ncwrite(grid, filx, var, start=[1,1,1,1], count=[-1,-1,-1,-1])
end

numb = 0                                                                      # finally, update gask in filx
for a = 1:GATS, b = 1:GONS                                                    # to set negative values to zero
  global numb                                                                 # (-3,-4 values remain in filw)
  gask[b,a,1] < 0 && (gask[b,a,1] = 0 ; numb += 1)
end
ncwrite(gask, filx, "LANDMASK", start=[1,1,1], count=[-1,-1,-1])
@printf("%10d gridboxes out of %12d (%4.1f%%) are now ocean in %s\n\n",
        numb, length(glat) * length(glon), 100 * numb / length(glat) / length(glon), filx)
=#
