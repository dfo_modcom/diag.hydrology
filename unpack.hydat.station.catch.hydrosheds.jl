#=
 = Identify the drainage boundary and river network that lies upstream of a HYDAT station.
 = Include a bounding box for plotting and append the watershed area to the station file.
 = The input grid is flow direction from HydroSHEDS - RD Mar,Apr,Jun 2023.
 =#

using My, Printf, NetCDF, Shapefile

const LATS             = 12959                          # number of latitudes
const LONS             = 43200                          # number of longitudes
const DELL             =     0.0041666666666666666      # lat/lon grid spacing
const LATA             =    30.0020833333333333333      # first latitude on grid
const LONA             =  -179.9979166666666666666      # first longitude on grid

const EARTH            = 6.371e6                        # mean Earth radius (m)
const D2R              = pi / 180.0                     # degrees to radians conversion
const MISS             = -1                             # generic missing value

if (argc = length(ARGS)) != 2
  print("\nUsage: jjj $(basename(@__FILE__)) ../HYDROSHEDS/ hydat_03JB002.sta\n\n")
  exit(1)
end
fila = ARGS[2]
filb = ARGS[2] * ".hyshed"
filc = ARGS[1] * "hydrosheds_30_85N_180_00W_15sec_flodir.nc"

function subroute()                                                           # use the HydroSHEDS river reach location
  fpa  = My.ouvre(fila, "r")                                                  # (next to the HYDAT station)
  lins = readlines(fpa, keep = true) ; close(fpa)
  hlat = parse(Float64, split(lins[21])[2])
  hlon = parse(Float64, split(lins[22])[2])
  hnum =                split(lins[25])[2]
  fups = Set{Tuple{Int32, Int32}}()

  flat = ncread(filc,   "lat", start=[1],   count=[-1])                       # to find a corresponding grid location
  flon = ncread(filc,   "lon", start=[1],   count=[-1])
  fdir = ncread(filc, "Band1", start=[1,1], count=[-1,-1])
  latmin, latind = findmin(abs.(flat .- hlat))
  lonmin, lonind = findmin(abs.(flon .- hlon))
  @printf("distance between grid %9.5f %9.5f\n", hlat, hlon)
  @printf(" and station location %9.5f %9.5f",               flat[latind], flon[lonind])
  @printf(" is %9.5f km\n",           latlondist(hlat, hlon, flat[latind], flon[lonind]) / 1000)

  function watershed(b::Int32, a::Int32)
    numb = 0
    if 2 < a < LATS-1 && 2 < b < LONS-1
#    (a == 1 || a == LATS || b == 1 || b == LONS) && return(numb)
      fdir[b-1,a-1] == 128 && !in((b-1,a-1), fups) && (push!(fups, (b-1,a-1)) ; numb += 1)
      fdir[b  ,a-1] ==  64 && !in((b  ,a-1), fups) && (push!(fups, (b  ,a-1)) ; numb += 1)
      fdir[b+1,a-1] ==  32 && !in((b+1,a-1), fups) && (push!(fups, (b+1,a-1)) ; numb += 1)
      fdir[b-1,a  ] ==   1 && !in((b-1,a  ), fups) && (push!(fups, (b-1,a  )) ; numb += 1)
      fdir[b+1,a  ] ==  16 && !in((b+1,a  ), fups) && (push!(fups, (b+1,a  )) ; numb += 1)
      fdir[b-1,a+1] ==   2 && !in((b-1,a+1), fups) && (push!(fups, (b-1,a+1)) ; numb += 1)
      fdir[b  ,a+1] ==   4 && !in((b  ,a+1), fups) && (push!(fups, (b  ,a+1)) ; numb += 1)
      fdir[b+1,a+1] ==   8 && !in((b+1,a+1), fups) && (push!(fups, (b+1,a+1)) ; numb += 1)
    end
    numb
  end

  push!(fups, (lonind,latind))                                                # then identify the remainder of
  numb = totl = 1                                                             # the upstream watershed using fdir
  while (numb > 0)
    numb = 0
    for (b,a) in fups
      numb += watershed(b, a)
    end
    totl += numb
  end

  area = 0.0                                                                  # calculate watershed area
  ares = (DELL * EARTH / 1e3 * D2R)^2 * cos.(flat * pi / 180)                 # and a bounding lat/lon box
  minlat = minlon =  9999.0
  maxlat = maxlon = -9999.0
  for (b,a) in fups
    minlat > flat[a] && (minlat = flat[a])
    maxlat < flat[a] && (maxlat = flat[a])
    minlon > flon[b] && (minlon = flon[b])
    maxlon < flon[b] && (maxlon = flon[b])
    area += ares[a]
  end
  @printf("an area of %9.2f km2 (%9d gridboxes) define the station and its upstream watershed\n", area, totl)

  fpa   = My.ouvre(fila, "r")                                                 # update the station text file
  lins  = readlines(fpa, keep = true) ; close(fpa)
  form  = "" ; for a =  1:29           form *= lins[a]  end
  hrea  = parse(Float64, split(lins[30])[2])
  form *= @sprintf("%-24s %f      15s_km2_with_no_holes %9.1f      15s_count_gridbox %9d\n", "UPLAND_SKM", hrea, area, totl)
               for a = 31:length(lins) form *= lins[a]  end
  fpa   = My.ouvre(fila, "w")
  write(fpa, form) ; close(fpa)

  bord = Array{Tuple{Int32, Int32, Int8}}(undef, 0)                           # identify the watershed border
  latbeg = lonbeg = -9999                                                     # starting with a northern point
  for (b,a) in fups                                                           # and search counterclockwise
    a > latbeg && (latbeg = a ; lonbeg = b)                                   # along the outer gridbox wall
  end                                                                         # (where w = 0123 = NWSE)
  push!(bord, (lonbeg,latbeg,0))

  function shedborder((b,a,w)::Tuple{Int32,Int32,Int8})
    if     w == 0
      !in((b  ,a+1), fups) && in((b-1,a+1), fups) && !in((b-1,a+1,3), bord) && (push!(bord, (b-1,a+1,3)) ; return(true))
      !in((b-1,a+1), fups) && in((b-1,a  ), fups) && !in((b-1,a  ,0), bord) && (push!(bord, (b-1,a  ,0)) ; return(true))
      !in((b-1,a  ), fups) &&                        !in((b  ,a  ,1), bord) && (push!(bord, (b  ,a  ,1)) ; return(true))
    elseif w == 1
      !in((b-1,a  ), fups) && in((b-1,a-1), fups) && !in((b-1,a-1,0), bord) && (push!(bord, (b-1,a-1,0)) ; return(true))
      !in((b-1,a-1), fups) && in((b  ,a-1), fups) && !in((b  ,a-1,1), bord) && (push!(bord, (b  ,a-1,1)) ; return(true))
      !in((b  ,a-1), fups) &&                        !in((b  ,a  ,2), bord) && (push!(bord, (b  ,a  ,2)) ; return(true))
    elseif w == 2
      !in((b  ,a-1), fups) && in((b+1,a-1), fups) && !in((b+1,a-1,1), bord) && (push!(bord, (b+1,a-1,1)) ; return(true))
      !in((b+1,a-1), fups) && in((b+1,a  ), fups) && !in((b+1,a  ,2), bord) && (push!(bord, (b+1,a  ,2)) ; return(true))
      !in((b+1,a  ), fups) &&                        !in((b  ,a  ,3), bord) && (push!(bord, (b  ,a  ,3)) ; return(true))
    else
      !in((b+1,a  ), fups) && in((b+1,a+1), fups) && !in((b+1,a+1,2), bord) && (push!(bord, (b+1,a+1,2)) ; return(true))
      !in((b+1,a+1), fups) && in((b  ,a+1), fups) && !in((b  ,a+1,3), bord) && (push!(bord, (b  ,a+1,3)) ; return(true))
      !in((b  ,a+1), fups) &&                        !in((b  ,a  ,0), bord) && (push!(bord, (b  ,a  ,0)) ; return(true))
    end
    false
  end
  while  shedborder(bord[end]) != false  end

  numb = 1                                                                    # then save the border to text
  (latnow,lonnow) = (latbeg,lonbeg)                                           # (with northen point duplicated)
  form = @sprintf("%9.5f %10.5f\n", flat[latnow], flon[lonnow])               # but using only gridbox centers
  for (b,a,w) in bord
    if (latnow,lonnow) != (a,b)
      form *= @sprintf("%9.5f %10.5f\n", flat[a], flon[b])
      (latnow,lonnow) = (a,b)
      numb += 1
    end
  end
  (latnow,lonnow) != (latbeg,lonbeg) && (form *= @sprintf("%9.5f %10.5f\n", flat[latbeg], flon[lonbeg]) ; numb += 1)
  forn  = @sprintf("%9.5f %9.5f %10.5f %10.5f %d\n", minlat, maxlat, minlon, maxlon, numb)
  fpa   = My.ouvre(filb, "w") ; write(fpa, forn) ; write(fpa, form) ; close(fpa)

  stem = ARGS[2][1:end-4] * "_hyshed"                                         # for shapefiles, read the border back in
  sdst = stem * ".csv"
  fpa = My.ouvre(filb, "r") ; lins = readlines(fpa, keep = true) ; close(fpa)
  form = "way_id,pt_id,x,y\n"
  for a = 2:length(lins)
    latnow, lonnow = split(lins[a])
    form *= "1,$(a-1),$lonnow,$latnow\n"
  end
  fpa = My.ouvre(sdst, "w") ; write(fpa, form) ; close(fpa)

  ssrc = stem * ".csv"                                                        # convert the border to a shapefile
  sdst = stem * ".shp"
  print("ogr2ogr -s_srs EPSG:4326 -t_srs EPSG:4326 -f \"ESRI Shapefile\" $sdst $ssrc -dialect SQLite -sql \"SELECT way_id, MakeLine (MakePoint(CAST(x AS float),CAST(y AS float))) FROM $stem GROUP BY way_id\" -nlt POLYGON\n")
    run(`ogr2ogr -s_srs EPSG:4326 -t_srs EPSG:4326 -f  "ESRI Shapefile"  $sdst $ssrc -dialect SQLite -sql  "SELECT way_id, MakeLine (MakePoint(CAST(x AS float),CAST(y AS float))) FROM $stem GROUP BY way_id"  -nlt POLYGON`)
  rm(ssrc)

  stem = "HydroRIVERS_v10_na"                                                 # and use the shapefile to isolate
  ssrc = ARGS[1] * stem * ".shp"                                              # all rivers interior to the border
  sdst = ARGS[2][1:end-4] * "_hyrivr.shp"
  clip = ARGS[2][1:end-4] * "_hyshed.shp"
  print("ogr2ogr -sql \"select * from $stem where (MAIN_RIV = $hnum)\" $sdst $ssrc -clipsrc $clip -segmentize 0.0005\n")
    run(`ogr2ogr -sql  "select * from $stem where (MAIN_RIV = $hnum)"  $sdst $ssrc -clipsrc $clip -segmentize 0.0005`)

  stem = ARGS[2][1:end-4]                                                     # then save shapefiles in a subdir
  ssrc = stem * "_hy*"                                                        # ("segmentize" adds extra points)
  sdst = stem * ".shp"
  !isdir(sdst) && mkdir(sdst)
  print("mv $ssrc $sdst\n")
  for aa in ["_hyrivr", "_hyshed"], bb in ["dbf", "prj", "shp", "shx"]
    run(`mv $stem$aa.$bb $sdst`)
  end
end

subroute()
exit(0)

#=
# print("ogr2ogr -sql \"select * from $stem where (MAIN_RIV = $hnum)\" $sdst $ssrc -clipsrc $minlon $minlat $maxlon $maxlat\n")
#   run(`ogr2ogr -sql  "select * from $stem where (MAIN_RIV = $hnum)"  $sdst $ssrc -clipsrc $minlon $minlat $maxlon $maxlat`)

const LATS             =  501                           # number of latitudes
const LONS             = 1201                           # number of longitudes
fild = ARGS[1] * "hydrosheds_30_85N_180_00W_15sec_floput.nc"
  print("copying $filc to $fild\n") ; cp(filc, fild; force=true)

  otoe = Dict{Tuple{Int32, Int32}, Tuple{Int32, Int32}}()
  flat = Array{Float32}(undef, LONS, FATS)                                    # read (flipped) grids of flow
  flon = Array{Float32}(undef, LONS, FATS)                                    # accumulation, flow direction,
  facc =   Array{Int32}(undef, LONS, FATS)                                    # and indices of rivers, lakes;
  fdir =   Array{Int16}(undef, LONS, FATS)                                    # evidently, a few gridboxes are
  friv = Set{Tuple{Int32, Int32}}()                                           # ocean/lake or river/lake, and
  flak = Set{Tuple{Int32, Int32}}()                                           # respectively, these are treated

    !in((b-1,a+1), fups) && in((b-1,a  ), fups) && !in((b-1,a  ), bord) && (push!(bord, (b-1,a  )) ; return(true))
    !in((b-1,a  ), fups) && in((b-1,a-1), fups) && !in((b-1,a-1), bord) && (push!(bord, (b-1,a-1)) ; return(true))
    !in((b-1,a-1), fups) && in((b  ,a-1), fups) && !in((b  ,a-1), bord) && (push!(bord, (b  ,a-1)) ; return(true))
    !in((b  ,a-1), fups) && in((b+1,a-1), fups) && !in((b+1,a-1), bord) && (push!(bord, (b+1,a-1)) ; return(true))
    !in((b+1,a-1), fups) && in((b+1,a  ), fups) && !in((b+1,a  ), bord) && (push!(bord, (b+1,a  )) ; return(true))
    !in((b+1,a  ), fups) && in((b+1,a+1), fups) && !in((b+1,a+1), bord) && (push!(bord, (b+1,a+1)) ; return(true))
    !in((b+1,a+1), fups) && in((b  ,a+1), fups) && !in((b  ,a+1), bord) && (push!(bord, (b  ,a+1)) ; return(true))
    !in((b  ,a+1), fups) && in((b-1,a+1), fups) && !in((b-1,a+1), bord) && (push!(bord, (b-1,a+1)) ; return(true))
    fing = 0
    !in((b-1,a  ), fups) && (fing += 1)                                      # (completing the circuit may need
    !in((b  ,a-1), fups) && (fing += 1)                                      # to avoid any "dangling fingers")
    !in((b+1,a  ), fups) && (fing += 1)
    !in((b  ,a+1), fups) && (fing += 1)
    fing > 2 && (aa = pop!(bord) ; bb = pop!(bord) ; push!(bord, aa) ; push!(bord, bb) ; return(true))

  fdir = zeros(Int16, LONS, LATS)
  for (b,a) in fups
    fdir[b,a] = in((b,a), bord) ? Int16(2) : Int16(1)
  end
  fdir[lonind,latind] = Int16(3)
  ncwrite(fdir, fild, "Band1", start=[1,1], count=[-1,-1])
=#
