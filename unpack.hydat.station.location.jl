#=
 = tabulate the HYDAT station locations for WRF-Hydro preprocessing - RD Jun 2023.
 =#

using My, Printf

const D2R              = 3.141592654 / 180.0            # degrees to radians conversion
const DELMISS          = -8e9                           # generic missing value comparison
const GRDMISS          = -9e9                           # generic missing value on a grid
const MISS             = -9999.0                        # generic missing value
const MIST             = @sprintf("%d", Int64(MISS))    # generic missing value string

if (argc = length(ARGS)) != 1
  print("\nUsage: jjj $(basename(@__FILE__)) .\n\n")
  exit(1)
end
sdir = ARGS[1] * "/" ; files = readdir(sdir)
form = "FID,LON,LAT,STATION,Name\n"

ind = 1
for file in files
  global ind, form
  if isfile(sdir * file) && endswith(file, ".sta")
    fpa   = My.ouvre(sdir * file, "r")
    lins  = readlines(fpa, keep = true) ; close(fpa)
    name  = file[7:13]
    numb  = split(lins[23])[2]
    hlat  = split(lins[90])[2]
    hlon  = split(lins[91])[2]
    form *= @sprintf("%d,%s,%s,%s,%s\n", ind, hlon, hlat, name, numb)
    ind  += 1
  end
end

fil = "hydat_station_location.csv"
fpa = My.ouvre(fil, "w")
write(fpa, form)
close(fpa)
exit(0)
