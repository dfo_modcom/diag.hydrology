#=
 = Convert a monthly timeseries to a mean for each of the 12 months - RD May 2020. 
 =#

using My, Printf, NetCDF

const DELT             = 5                              # starting month of input timeseries
const MISS             = -9999.0                        # generic missing value
const D2R              = 3.141592654 / 180.0            # degrees to radians conversion

if (argc = length(ARGS)) != 1
  print("\nUsage: jjj $(basename(@__FILE__)) rivsum.1955_2020\n\n")
  exit(1)
end
fila = ARGS[1] * ".nc"
filb = ARGS[1] * ".mean.nc"
vals = ncread(fila, "tmp", start=[1,1,1], count=[-1,-1,-1])

num = length(vals)
dat = zeros(24)
riv = zeros(24)
for a = 1:12
  cnt = 0.0
  for b = a:12:num
    vals[b] != MISS && (riv[a] += vals[b] ; cnt += 1.0)
  end
  riv[a] = cnt == 0 ? MISS : riv[a] / cnt
  riv[a+12] = riv[a]

                                      yra = "1900" ; yrb = "1901"
  b = a + DELT ; b > 12 && (b -= 12 ; yra = "1901" ; yrb = "1902")
  ref = yra * (b < 10 ? "0" : "") * @sprintf("%d01", b)
  dat[a]    = My.datesous("19000101", ref, "hr")
  ref = yrb * (b < 10 ? "0" : "") * @sprintf("%d01", b)
  dat[a+12] = My.datesous("19000101", ref, "hr")
end

nccreer(     filb, 24, 1, 1, MISS)
ncwrite(riv, filb,  "tmp", start=[1,1,1], count=[-1,-1,-1])
ncwrite(dat, filb, "time", start=[1],     count=[-1])
ncputatt(    filb, "time", Dict("units" => "hours since 1900-01-01 00:00:0.0"))
exit(0)
