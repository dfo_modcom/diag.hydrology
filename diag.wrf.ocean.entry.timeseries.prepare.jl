#=
 = Normalize the HYDAT and WRF-Hydro streamflow timeseries at each station
 = to roughly [0,1] and save the normalizations to text.  Use PEST-optimal
 = WRF-Hydro timeseries as a normalization reference - RD Oct,Nov 2023.
 =#

using My, Printf, NetCDF, Statistics, StatsBase

const MISS             = -9999.0                        # generic missing value

if (argc = length(ARGS)) != 2
  print("\nUsage: jjj $(basename(@__FILE__)) 2019-05-01 pest_nwm_DOMAIN_AuxF.FORCING.era_2019-03-31_000214.remest\n\n")
  exit(1)
end
cutt = ARGS[1] * "-00"
fila = ARGS[2] * ".nc"
filb = ARGS[2] * ".txt"
filc = ARGS[2] * ".val"
fild = ARGS[2] * ".prep.nc"
file = ARGS[2] * ".prep.txt"
filf = ARGS[2] * ".prep.val"
@printf("\ncopying %s %s\n", fila, fild) ; cp(fila, fild; force = true)
@printf(  "copying %s %s\n", filb, file) ; cp(filb, file; force = true)
fpa  = My.ouvre(file, "r") ; line = readlines(fpa) ; close(fpa)

tims = ncread(fild, "time", start=[1],     count=[-1])                        # read the timeseries dates and
floh = ncread(fild, "hydt", start=[1,1],   count=[-1,-1])                     # two streamflow estimates
flow = ncread(fild, "flow", start=[1,1,1], count=[-1,-1,-1])
npes, nsta, ntim = size(flow)
pind = parse(Int64, split(line[nsta+2])[4])
@printf("with %d times %d stations %d PEST iterations of which %d is optimal\n\n", ntim, nsta, npes, pind)

cutd = datesous("1900-01-01-00", cutt, "hr")
cuti = findfirst(isequal(cutd), tims)
@printf("omitting 1:%d of 1:%d timeseries\n", cuti, ntim)

mskh = falses(      nsta, ntim)                                               # define masks where HYDAT observations
mskw = falses(npes, nsta, ntim)                                               # and WRF-Hydro model are (either/both)
mask = falses(npes, nsta, ntim)                                               # valid (but normalize whole timeseries)
for a = 1:ntim, b = 1:nsta
  floh[b,a]   > MISS &&                                    (mskh[  b,a] = true)
  for c = 1:npes
                        flow[c,b,a] > MISS &&              (mskw[c,b,a] = true)
    floh[b,a] > MISS && flow[c,b,a] > MISS && a >= cuti && (mask[c,b,a] = true)
  end
  a == ntim && @printf("station %3d has %5d/%5d/%5d valid data (HYDAT/PEST-optimal WRF-Hydro/both)\n",
    b, length(mskh[b,mskh[b,:]]), length(mskw[pind,b,mskw[pind,b,:]]), length(mask[pind,b,mask[pind,b,:]]))
end

function flonorm(x::Array{Float64}, alp::Float64, bet::Float64)               # and normalize streamflow by its mean
  if alp == MISS || bet == MISS                                               # and std (or else by input mean, std)
    alp = mean(x) ; bet = std(x, mean = alp, corrected = false)
  end
  y = (x .- alp) ./ (bet + eps())
  return y, alp, bet
end

form = ""                                                                     # use the mean and std of PEST-optimal
for a = 1:nsta                                                                # WRF-Hydro to normalize all values
  global form
  flow[pind,a,mskw[pind,a,:]], alp, bet = flonorm(flow[pind,a,mskw[pind,a,:]], MISS, MISS)
  floh[     a,mskh[     a,:]], aaa, bbb = flonorm(floh[     a,mskh[     a,:]],  alp,  bet)
  for b = setdiff(1:npes, pind)
    flow[ b,a,mskw[   b,a,:]], aaa, bbb = flonorm(flow[   b,a,mskw[   b,a,:]],  alp,  bet)
  end
  form *= @sprintf("%15.8f %15.8f\n", alp, bet)
end

fpa = My.ouvre(filf, "w") ; write(fpa, form) ; close(fpa)                     # and save the results
ncwrite(floh, fild, "hydt", start=[1,1],   count=[-1,-1])
ncwrite(flow, fild, "flow", start=[1,1,1], count=[-1,-1,-1])
exit(0)
