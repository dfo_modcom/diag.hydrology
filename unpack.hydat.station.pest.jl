#=
 = Create observation and instruction PEST files to compare daily streamflow estimates
 = that fall between a first and third date, based on HYDAT (for the "observation data"
 = section of wrftest.pst) and WRF-Hydro (for the instruction file "frxst_pts_out.ins")
 = sources.  Included are only the dates and stations that offer valid comparisons.  A
 = weight of zero is given to comparisons prior to an intermediate date, while a HYDAT
 = flag determines the weight given to comparisons after this date - RD Aug,Sep 2023.
 =#

using My, Printf, NetCDF

const MISS             = -9999.0                        # generic missing value

if (argc = length(ARGS)) != 5
  print("\nUsage: jjj $(basename(@__FILE__)) 2019-03-31 2019-05-01 2019-10-30 hydat_station_location.csv frxst_pts_out.txt\n\n")
  exit(1)
end
data = ARGS[1] * "-12" ; dela = datesous("1900-01-01-00", data, "hr")
datb = ARGS[2] * "-12" ; delb = datesous("1900-01-01-00", datb, "hr")
datc = ARGS[3] * "-12" ; delc = datesous("1900-01-01-00", datc, "hr")
filh = ARGS[4] ; fpa = My.ouvre(filh, "r") ; linh = readlines(fpa)[2:end] ; close(fpa)
filw = ARGS[5] ; fpa = My.ouvre(filw, "r") ; linw = readlines(fpa)        ; close(fpa)

hind = Array{         Int64}(undef, length(linh))                             # first parse hydat_station_location.csv
hlon = Array{       Float64}(undef, length(linh))
hlat = Array{       Float64}(undef, length(linh))
hnam = Array{AbstractString}(undef, length(linh))
hnum = Array{         Int64}(undef, length(linh))
hloc = Array{AbstractString}(undef, length(linh))
iton = Dict{Int64, AbstractString}()
for a = 1:length(linh)
  vals = split(linh[a], ",")
  file = "hydat_" * vals[4] * ".sta"                                              ; valsloc = "FILLER"
  isfile(file) && (fpf = My.ouvre(file, "r") ; lins = readlines(fpf) ; close(fpf) ; valsloc = lins[1][26:end])
  hind[a] = parse(  Int64, vals[1])
  hlon[a] = parse(Float64, vals[2])
  hlon[a] = parse(Float64, vals[3])
  hnam[a] =                vals[4]
  hnum[a] = parse(  Int64, vals[5])
  hloc[a] =                valsloc
  iton[hind[a]] = hnam[a]
end

filh = "hydat_" * iton[1] * ".nc"                                             # then get an array of available times from
tims = ncread(filh, "time", start=[1], count=[-1])                            # one of the HYDAT data files (all the same)

wnum = zeros(3)
vnum = zeros(length(linh))                                                    # count valid comparisons by station while
skip = numb = 0                                                               # looping through the 12 UTC data from
foro = forc = "" ; fori = "pif #\n"                                           # frxst_pts_out.txt and the corresponding
for a = 1:length(linw)                                                        # valid HYDAT data to create observation
  global skip, numb, foro, fori, forc                                         # and instruction files
  vals = split(linw[a], ",") ; skip += 1
  dats = split(vals[2], " ") ; datw = dats[1] * "-" * dats[2][1:2]
  delw = datesous("1900-01-01-00", datw, "hr")
  if dela <= delw <= delc && dats[2][1:2] == "12"
    staw = parse(  Int64, vals[3])
    flow = parse(Float64, vals[6])
    file = "hydat_" * iton[staw] * ".nc"
    tind = findfirst(isequal(delw), tims)
    floh = ncread(file, "flow", start=[1,1,tind], count=[-1,-1,1])[1]
    floi = ncread(file, "floi", start=[1,1,tind], count=[-1,-1,1])[1]
                    wght = 1.0 ; wind = 3                                     # exclude obs before datb and halve weight
    delw < delb && (wght = 0.0 ; wind = 1)                                    # on ice/dry/estimate/sample (but put full
    floi < -1.5 && (wght = 0.5 ; wind = 2)                                    # weight on good and partial day obs)
    if floh > MISS
      numb += 1
      foro *= @sprintf("o%-6d %-7.0f %3.1f     obsgroup\n", numb, floh, wght)
      fori *= @sprintf("l%d    #,#   #,#   #,#   #,#   #,#   !o%d!   #,#  #,#\n", skip, numb)
      forc *= @sprintf("%6d %7.0f %11.5f %6d %s %s\n", numb, floh, flow, skip, file, linw[a])
      wnum[wind] += 1
      vnum[staw] += 1
      skip  = 0
    end
  end
end
numb != sum(vnum) && error("$numb != $(sum(vnum))\n\n")

foru = "$data $datb $datc\n" 
for a = 1:length(linh)
  global foru
           @printf("found %5d possible comparisons for station %s:%s\n", vnum[a], hnam[a], hloc[a])
  foru *= @sprintf("found %5d possible comparisons for station %s:%s\n", vnum[a], hnam[a], hloc[a])
end
         @printf("total %5d possible comparisons\n\n", sum(vnum))
foru *= @sprintf("total %5d possible comparisons\n\n", sum(vnum))
for a = 1:3
  global foru
  a == 1 && (wght = 0.0) ; a == 2 && (wght = 0.5) ; a == 3 && (wght = 1.0)
           @printf("found %5d comparisons for weight %3.1f\n", wnum[a], wght)
  foru *= @sprintf("found %5d comparisons for weight %3.1f\n", wnum[a], wght)
end

filo = ARGS[4][1:end-4] * ".obs"                                              # also save the valid flow comparisons
fili = ARGS[4][1:end-4] * ".ins"                                              # (for these WRF-Hydro flows) as a check
filc = ARGS[4][1:end-4] * ".chk"
filu = ARGS[4][1:end-4] * ".out"
fpa  = My.ouvre(filo, "w") ; write(fpa, foro) ; close(fpa)
fpb  = My.ouvre(fili, "w") ; write(fpb, fori) ; close(fpb)
fpc  = My.ouvre(filc, "w") ; write(fpc, forc) ; close(fpc)
fpd  = My.ouvre(filu, "w") ; write(fpd, foru) ; close(fpd)
exit(0)
