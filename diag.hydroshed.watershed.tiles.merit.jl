#=
 = Isolate the DEM that covers all watersheds of interest.  This assumes
 = prior conversion of 30-s MERIT from, say, .tif to .tif.nc (for example,
 = by gdal_translate tile.tif tile.nc), possibly followed by conversation
 = of the resulting .nc file back to .tif (and thus, an attempt is made to
 = duplicate the NetCDF metadata in "crs" that GDAL expects) - RD Apr 2022.
 =#

using My, Printf, NetCDF

const LATS             =  6600                          # number of latitudes
const LONS             = 21600                          # number of longitudes
const DELL             =    0.0083333333333333333       # lat/lon grid spacing
const LATA             =   30.0041666666666666667       # first latitude on grid
const LONA             = -179.9958333333333333333       # first longitude on grid
const MISS             = Int16(-9999)                   # generic missing value

if (argc = length(ARGS)) != 2
  print("\nUsage: jjj $(basename(@__FILE__)) 30sec_elevtn.tif.nc merit_hydrodem_30_85N_180_00W_30sec_elevtn.nc\n\n")
  exit(1)
end
fila = ARGS[1]
filb = ARGS[2]

function nccreate(fn::AbstractString, lats::Array{Float64,1}, lons::Array{Float64,1}, missing::Int16)
  nclat = NcDim("lat", length(lats), atts = Dict{Any,Any}(
                "standard_name"               => "latitude",
                "long_name"                   => "latitude",
                "units"                       => "degrees_north"), values = lats)
  nclon = NcDim("lon", length(lons), atts = Dict{Any,Any}(
                "standard_name"               => "longitude",
                "long_name"                   => "longitude",
                "units"                       => "degrees_east"), values = lons)
  nclen = NcDim("len", 0)
  ncchr = NcVar("crs", [nclen], atts = Dict{Any,Any}(                         # define a NetCDF grid that GDAL
                "grid_mapping_name"           => "latitude_longitude",        # would recognize when converting
                "long_name"                   => "CRS definition",            # from .nc to .tif (attributes are
                "longitude_of_prime_meridian" => 0.,                          # taken from the .tif.nc files that
                "semi_major_axis"             => 6378137.,                    # GDAL creates from the .tif tiles)
                "inverse_flattening"          => 298.257223563,
                "spatial_ref"                 => "GEOGCS[\"WGS 84\",DATUM[\"WGS_1984\",SPHEROID[\"WGS 84\",6378137,298.257223563,AUTHORITY[\"EPSG\",\"7030\"]],AUTHORITY[\"EPSG\",\"6326\"]],PRIMEM[\"Greenwich\",0,AUTHORITY[\"EPSG\",\"8901\"]],UNIT[\"degree\",0.0174532925199433,AUTHORITY[\"EPSG\",\"9122\"]],AXIS[\"Latitude\",NORTH],AXIS[\"Longitude\",EAST],AUTHORITY[\"EPSG\",\"4326\"]]",
                "GeoTransform"                => "-160 0.008333333333333333 0 80 0 -0.008333333333333333 "), t=NC_CHAR, compress=-1)
  ncbnd = NcVar("Band1", [nclon, nclat], atts = Dict{Any,Any}(
                "long_name"                   => "GDAL Band Number 1",
                "_FillValue"                  => missing,
                "grid_mapping"                => "crs"), t=Int16, compress=-1)
  ncfil = NetCDF.create(fn, [ncchr, ncbnd], gatts = Dict{Any,Any}(
                "Conventions"                 => "CF-1.5",
                "GDAL"                        => "GDAL 3.0.4, released 2020/01/28"), mode = NC_NETCDF4)
  print("created $fn with $(length(lats)) lats and $(length(lons)) lons\n")
  return
end

lats = Array{Float64}(undef, LATS)                                            # define the output dimensions
lons = Array{Float64}(undef, LONS)
for a = 1:LATS  lats[a] = LATA + DELL * (a - 1)  end
for a = 1:LONS  lons[a] = LONA + DELL * (a - 1)  end
nccreate(filb, lats, lons, MISS)

print("reading $fila ")                                                       # read the elevation subset and save
glat = ncread(fila, "lat", start=[1], count=[-1])
glon = ncread(fila, "lon", start=[1], count=[-1])
dlat = round(Int, (LATA - glat[1]) / DELL)
dlon = round(Int, (LONA - glon[1]) / DELL)
print("and extracting from [$(glat[1+dlat]),$(glat[LATS+dlat])] and [$(glon[1+dlon]),$(glon[LONS+dlon])]\n")

grdb = ncread(fila, "Band1", start=[1+dlon,1+dlat], count=[LONS,LATS])
ncwrite(grdb, filb, "Band1", start=[1,1], count=[-1,-1])
exit(0)
