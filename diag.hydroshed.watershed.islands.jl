#=
 = Identify all watersheds that are upstream, either of the Atlantic Ocean or of
 = a seed location on the Fulldom_hires.nc grid, and create a mask for watersheds
 = that are not upstream (perhaps including a border around upstream watersheds).
 = Then identify the corresponding low resolution geogrid.nc mask (i.e., where
 = a low resolution pixel is masked if all full resolution pixels within it (say,
 = across a 10x10 subgrid) are masked.  Use this low resolution mask to redefine
 = the grids in geogrid, Fulldom_hires, and the HydroSHEDs DEM (na_con_3s_all),
 = as taken from a source directory.  Also output a watershed/border mask file.
 = Note that latitude is flipped in the Fulldom_hires.nc grids, so seed location,
 = as well as grids read from Fulldom_hires, are reversed below (i.e., seeds can
 = be chosen by zooming in on a Fulldom_hires location using grads).  Here and
 = below, low/full/high resolution refers to geogrid, Fulldom_hires, and DEM
 = resolution, respectively - RD Jan 2021.
 =#

using My, Printf, NetCDF

const MASK             = true                           # create mask files (or read from previously created)
const GATS             = 200                            # number of  latitudes  in geogrid.nc
const GONS             = 310                            # number of longitudes  in geogrid.nc
const FATS             = 2000                           # number of  latitudes  in Fulldom_hires.nc
const FONS             = 3100                           # number of longitudes  in Fulldom_hires.nc
const DATS             = 24000                          # number of  latitudes  in HydroSHEDs DEM
const DONS             = 54000                          # number of longitudes  in HydroSHEDs DEM
const DELL             =   0.0008333333333333333        # lat/lon grid spacing  of HydroSHEDs DEM
const DATA             =  40.0004166666666666667        # first      latitude   of HydroSHEDs DEM
const DONA             = -94.9995833333333333333        # first     longitude   of HydroSHEDs DEM
const DISS             = Int16(-9999)                   # missing value (ocean) of HydroSHEDs DEM

if (argc = length(ARGS)) != 3
  print("\nUsage: jjj $(basename(@__FILE__)) ../DOMAIN_full_full/ 2900 150\n")
  print(  "       where 2900 and 150 are the seed x and y grid coordinates that all upstream watersheds connect to\n\n")
  exit(1)
end
sedx =            parse(Int, ARGS[2])                                         # flip the latitude index of seed
sedy = FATS + 1 - parse(Int, ARGS[3])             ; filw = "geomask.nc"       # (refers to Fulldom_hires grid) and
fnam =       "geogrid.nc" ; fila = ARGS[1] * fnam ; filx = fnam               # define input and output file names
fnam = "Fulldom_hires.nc" ; filb = ARGS[1] * fnam ; fily = fnam
fnam = "na_con_3s_all.nc" ; filc = ARGS[1] * fnam ; filz = fnam

function masklow(fila::AbstractString, filb::AbstractString, filc::AbstractString, filw::AbstractString,
                 filx::AbstractString, fily::AbstractString, filz::AbstractString, sedx::Int64, sedy::Int64)
  print("\ncopying $fila to $filw\n")#; cp(fila, filw; force=true)
  print(  "copying $fila to $filx\n")#; cp(fila, filx; force=true)
  print(  "copying $filb to $fily\n")#; cp(filb, fily; force=true)
  print(  "copying $filc to $filz\n")#; cp(filc, filz; force=true)

  fask = Array{Int16}(undef, FONS, FATS)                                      # mask rivers/lakes/watersheds
  fnet = Set{Tuple{Int32, Int32}}()                                           # on a grid of flow direction
  fnew = Set{Tuple{Int32, Int32}}()
  temp = ncread(filb, "FLOWDIRECTION", start=[1,1], count=[-1,-1])
  for a = 1:FATS, b = 1:FONS  fask[b,a] = temp[b,FATS+1-a]  end
  temp = ncread(filb,   "CHANNELGRID", start=[1,1], count=[-1,-1])
  for a = 1:FATS, b = 1:FONS  temp[b,a] >= 0 && push!(fnet, (Int32(b), Int32(FATS+1-a)))  end
  temp = ncread(filb,      "LAKEGRID", start=[1,1], count=[-1,-1])
  for a = 1:FATS, b = 1:FONS  temp[b,a] >= 0 && push!(fnet, (Int32(b), Int32(FATS+1-a)))  end
  temp = Array{Int16}(undef, FONS, FATS)
  @printf("\n%9d gridboxes define the total river and lake network\n", length(fnet))

  function waterway(b::Int32, a::Int32)
    numb = 0
    in((b-1,a-1), fnet) && (setdiff!(fnet, [(b-1,a-1)]) ; push!(fnew, (b-1,a-1)) ; numb += 1)
    in((b  ,a-1), fnet) && (setdiff!(fnet, [(b  ,a-1)]) ; push!(fnew, (b  ,a-1)) ; numb += 1)
    in((b+1,a-1), fnet) && (setdiff!(fnet, [(b+1,a-1)]) ; push!(fnew, (b+1,a-1)) ; numb += 1)
    in((b-1,a  ), fnet) && (setdiff!(fnet, [(b-1,a  )]) ; push!(fnew, (b-1,a  )) ; numb += 1)
    in((b+1,a  ), fnet) && (setdiff!(fnet, [(b+1,a  )]) ; push!(fnew, (b+1,a  )) ; numb += 1)
    in((b-1,a+1), fnet) && (setdiff!(fnet, [(b-1,a+1)]) ; push!(fnew, (b-1,a+1)) ; numb += 1)
    in((b  ,a+1), fnet) && (setdiff!(fnet, [(b  ,a+1)]) ; push!(fnew, (b  ,a+1)) ; numb += 1)
    in((b+1,a+1), fnet) && (setdiff!(fnet, [(b+1,a+1)]) ; push!(fnew, (b+1,a+1)) ; numb += 1)
    numb
  end

  if sedx == 0                                                                # initialize the Fulldom_hires mask
    numb = 0                                                                  # by identifying the Atlantic Ocean
    for a = 1:FATS, b = round(Int64, (a + 2400) / 2.2):FONS                   # (using an ad hoc definition)
      fask[b,a] == 0 && (fask[b,a] = -1 ; numb += 1)
    end
    @printf("%9d gridboxes define the Atlantic Ocean (values of -1)\n", numb)

    numb = 0                                                                  # then identify all river outflow
    for a = 2:FATS-1, b = 2:FONS-1                                            # points connected to the Atlantic
      fask[b,a] < 0 && (numb += waterway(Int32(b), Int32(a)))
    end
    totl = numb
    @printf("%9d gridboxes define river outflows to the Atlantic (values of -2)\n", totl)
  else                                                                        # otherwise initialize a single
    fask[sedx,sedy] = -2                                                      # value at the downstream outflow
    totl = 1                                                                  # of the watershed(s) of interest
    @printf("%9d gridbox defines the single outflow of interest (value of -2)\n", 1)
  end

  numb = 1                                                                    # then identify the upstream river
  while (numb > 0)                                                            # and lake network connected to
    numb = 0                                                                  # these outflow points
    for fnex in fnew
      numb += waterway(fnex[1], fnex[2])
    end
    totl += numb
#   @printf("%9d gridboxes are connected to the outflow (values of -2)\n", numb)
  end
  for fnex in fnew  fask[fnex[1],fnex[2]] = -2  end
  @printf("%9d gridboxes define the upstream rivers, lakes, and connected outflow(s) (values of -2)\n", totl)

  function watershed(b::Int64, a::Int64)                                      # identify the network watersheds
    numb = 0
    (fask[b-1,a-1] == 0 || fask[b-1,a-1] == 128) && (fask[b-1,a-1] = -2 ; numb += 1)
    (fask[b  ,a-1] == 0 || fask[b  ,a-1] ==  64) && (fask[b  ,a-1] = -2 ; numb += 1)
    (fask[b+1,a-1] == 0 || fask[b+1,a-1] ==  32) && (fask[b+1,a-1] = -2 ; numb += 1)
    (fask[b-1,a  ] == 0 || fask[b-1,a  ] ==   1) && (fask[b-1,a  ] = -2 ; numb += 1)
    (fask[b+1,a  ] == 0 || fask[b+1,a  ] ==  16) && (fask[b+1,a  ] = -2 ; numb += 1)
    (fask[b-1,a+1] == 0 || fask[b-1,a+1] ==   2) && (fask[b-1,a+1] = -2 ; numb += 1)
    (fask[b  ,a+1] == 0 || fask[b  ,a+1] ==   4) && (fask[b  ,a+1] = -2 ; numb += 1)
    (fask[b+1,a+1] == 0 || fask[b+1,a+1] ==   8) && (fask[b+1,a+1] = -2 ; numb += 1)
    numb
  end

  numb = 1                                                                    # including plateaus at elevation
  while (numb > 0)                                                            # (fask=0) not explicitly directed
    numb = 0                                                                  # down into the central gridbox
    for a = 2:FATS-1, b = 2:FONS-1
      fask[b,a] < 0 && (numb += watershed(b, a))
    end
    totl += numb
#   @printf("%9d gridboxes are connected to the upstream watersheds (values of -2)\n", numb)
#   for a = 1:FATS, b = 1:FONS  temp[b,FATS+1-a] = fask[b,a]  end
#   ncwrite(temp, fily, "FLOWDIRECTION", start=[1,1], count=[-1,-1])
  end
  @printf("%9d gridboxes define the upstream watershed without a border (values of -2)\n", totl)

  function border(b::Int64, a::Int64)                                         # then add an outer border
    numb = 0
    fask[b-1,a-1] >= 0 && (mask[b-1,a-1] = -2 ; numb += 1)
    fask[b  ,a-1] >= 0 && (mask[b  ,a-1] = -2 ; numb += 1)
    fask[b+1,a-1] >= 0 && (mask[b+1,a-1] = -2 ; numb += 1)
    fask[b-1,a  ] >= 0 && (mask[b-1,a  ] = -2 ; numb += 1)
    fask[b+1,a  ] >= 0 && (mask[b+1,a  ] = -2 ; numb += 1)
    fask[b-1,a+1] >= 0 && (mask[b-1,a+1] = -2 ; numb += 1)
    fask[b  ,a+1] >= 0 && (mask[b  ,a+1] = -2 ; numb += 1)
    fask[b+1,a+1] >= 0 && (mask[b+1,a+1] = -2 ; numb += 1)
    numb
  end

  mask = deepcopy(fask)                                                       # by looping a few times
  for c = 1:5
    numb = 0
    for a = 2:FATS-1, b = 2:FONS-1
      fask[b,a] == -2 && (numb += border(b, a))
    end
    totl += numb
    for a = 1:FATS, b = 1:FONS  fask[b,a] = mask[b,a]  end
  end
  @printf("%9d gridboxes define the upstream watershed with a border (values of -2)\n", totl)

  function square(b::Int64, a::Int64)                                         # create a blocky (low-res) mask
    numb = 0                                                                  # but only where the full-res grid
    for c = 0:9, d = 0:9                                                      # has no ocean (-1)
      fask[b+d,a+c] != -2 && (fask[b+d,a+c] = -2 ; numb += 1)
    end
    numb
  end

  gask = ncread(fila, "LANDMASK", start=[1,1,1], count=[-1,-1,-1])            # and save this at both full and
  numb = 0                                                                    # low resolution (1=land 0=water
  for a = 1:10:FATS, b = 1:10:FONS                                            # becomes -3 and -4 so that other
    wshed = false ; ocean = false                                             # low-res grids only change where
    for c = 0:9, d = 0:9                                                      # negative values occur, but these
      fask[b+d,a+c] == -1 && (ocean = true)                                   # are later stored as 0=water on
      fask[b+d,a+c] == -2 && (wshed = true)                                   # the low-res grid); if a low-res
    end                                                                       # gridbox contains -1 (ocean at
    if !ocean                                                                 # full res), then leave both the
      wshed && (numb += square(b, a))                                         # low-res and full-res grids alone
      if !wshed                                                               # except to make low-res ocean=4
        c = div(a - 1, 10) + 1
        d = div(b - 1, 10) + 1
        gask[d,c,1] == 1 && (gask[d,c,1] = -3)
        gask[d,c,1] == 0 && (gask[d,c,1] = -4)
      end
    else
      c = div(a - 1, 10) + 1
      d = div(b - 1, 10) + 1
      gask[d,c,1] == 0 && (gask[d,c,1] = 4)
    end
  end
  totl += numb
  @printf("%9d gridboxes define the upstream watershed with low-res blockiness on land (values of -2)\n", totl)

  function lowreslake(b::Int64, a::Int64)                                     # also at low resolution, having
    numb = 0                                                                  # distinguished between ocean (4)
    gask[b-1,a-1,1] == 4 && (gask[b-1,a-1,1] = 0 ; numb += 1)                 # and lake (0), switch ocean to
    gask[b  ,a-1,1] == 4 && (gask[b  ,a-1,1] = 0 ; numb += 1)                 # lake where adjacent points are
    gask[b+1,a-1,1] == 4 && (gask[b+1,a-1,1] = 0 ; numb += 1)                 # already lake (around Melville)
    gask[b-1,a  ,1] == 4 && (gask[b-1,a  ,1] = 0 ; numb += 1)
    gask[b+1,a  ,1] == 4 && (gask[b+1,a  ,1] = 0 ; numb += 1)
    gask[b-1,a+1,1] == 4 && (gask[b-1,a+1,1] = 0 ; numb += 1)
    gask[b  ,a+1,1] == 4 && (gask[b  ,a+1,1] = 0 ; numb += 1)
    gask[b+1,a+1,1] == 4 && (gask[b+1,a+1,1] = 0 ; numb += 1)
    numb
  end

  totl = 0 ; numb = 1
  while (numb > 0)
    numb = 0
    for a = 2:GATS-1, b = 2:GONS-1
      gask[b,a,1] == 0 && (numb += lowreslake(b, a))
    end
    totl += numb
  end
  @printf("%9d gridboxes switched from ocean (values of 4) to inland lake (values of 0)\n", totl)

  function loneoceancenter(b::Int64, a::Int64)
    numb = 0
    gask[b-1,a-1,1] == 1 && gask[b  ,a-1,1] == 1 && gask[b+1,a-1,1] == 1 && gask[b-1,a  ,1] == 1 &&
    gask[b+1,a  ,1] == 1 && gask[b-1,a+1,1] == 1 && gask[b  ,a+1,1] == 1 && gask[b+1,a+1,1] == 1 && (gask[b,a,1] = 0 ; numb += 1)
    numb
  end

  numb = 0
  for a = 2:GATS-1, b = 2:GONS-1
    gask[b,a,1] == 4 && (numb += loneoceancenter(b, a))
  end
  @printf("%9d gridboxes switched from ocean (values of 4) to inland lake (values of 0)\n\n", numb)

  for a = 1:FATS, b = 1:FONS  temp[b,FATS+1-a] = fask[b,a]  end
  ncwrite(temp, fily, "FLOWDIRECTION", start=[1,1],   count=[-1,-1])
  ncwrite(gask, filw,      "LANDMASK", start=[1,1,1], count=[-1,-1,-1])
end

MASK && masklow(fila, filb, filc, filw, filx, fily, filz, sedx, sedy)         # first use the filw low-res mask to
exit(0)
gask = ncread(filw,   "LANDMASK", start=[1,1,1], count=[-1,-1,-1])            # modify the high-res DEM (dask), but
fask = ncread(filb, "TOPOGRAPHY", start=[1,1],   count=[-1,-1])               # note that grids are assumed to be on
dask = ncread(filc,      "Band1", start=[1,1],   count=[-1,-1])               # Mercator and lat/lon projections, so
glat = ncread(fila,       "CLAT", start=[1,1,1], count=[ 1,-1, 1])[1,:,1]     # only a slice of each lat/lon grid is
glon = ncread(fila,      "CLONG", start=[1,1,1], count=[-1, 1, 1])[:,1,1]     # read (the whole grid would be needed
flat = ncread(filb,   "LATITUDE", start=[1,1],   count=[ 1,-1])[1,:]          # for a Lambert conformal projection)
flon = ncread(filb,  "LONGITUDE", start=[1,1],   count=[-1, 1])[:,1]
dlat = ncread(filc,        "lat", start=[1],     count=[-1])
dlon = ncread(filc,        "lon", start=[1],     count=[-1])

ilat = Array{Int64}(undef, length(dlat))                                      # find the nearest location in gask
ilon = Array{Int64}(undef, length(dlon))                                      # to each location in the DEM, then
for a = 1:length(dlat)  ilat[a] = findmin(abs.(glat .- dlat[a]))[2]  end      # switch external watersheds to ocean
for a = 1:length(dlon)  ilon[a] = findmin(abs.(glon .- dlon[a]))[2]  end      # and save the modified DEM to filz
numb = 0
for a = 1:DATS, b = 1:DONS
  global numb
  gask[ilon[b],ilat[a],1] < 0 && (dask[b,a] = DISS ; numb += 1)
end
ncwrite(dask, filz, "Band1", start=[1,1], count=[-1,-1])
@printf("\n%10d gridboxes out of %12d (%4.1f%%) are now ocean in %s\n",
        numb, length(dlat) * length(dlon), 100 * numb / length(dlat) / length(dlon), filz)

ilat = Array{Int64}(undef, length(flat))                                      # also find the nearest location in the
ilon = Array{Int64}(undef, length(flon))                                      # modified DEM to each location in fask
for a = 1:length(flat)  ilat[a] = findmin(abs.(dlat .- flat[a]))[2]  end      # and save the subsampled DEM (only for
for a = 1:length(flon)  ilon[a] = findmin(abs.(dlon .- flon[a]))[2]  end      # visualization of fily)
for a = 1:FATS, b = 1:FONS
  fask[b,a] = dask[ilon[b],ilat[a]]
end
ncwrite(fask, fily, "TOPOGRAPHY", start=[1,1],   count=[-1,-1])

vars = ["LU_INDEX", "HGT_M", "SOILTEMP", "SCT_DOM", "SCB_DOM", "SNOALB", "CON", "VAR", "OA1", "OA2", "OA3", "OA4", "OL1", "OL2", "OL3", "OL4", "VAR_SSO"]
vals = [        17,       0,          0,        14,        14,        0,     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,         0]
for (ind, var) in enumerate(vars)
  grid = ncread(fila, var, start=[1,1,1], count=[-1,-1,-1])
  for a = 1:GATS, b = 1:GONS
    gask[b,a,1] < 0 && (grid[b,a,1] = vals[ind])                              # then use the filw low-res mask
  end                                                                         # to modify low-res vars in fila
  ncwrite(grid, filw, var, start=[1,1,1], count=[-1,-1,-1])                   # (both 2D and 3D modifications,
  ncwrite(grid, filx, var, start=[1,1,1], count=[-1,-1,-1])                   # which are saved in filw and filx)
end

vars = ["LANDUSEF", "SOILCTOP", "SOILCBOT", "ALBEDO12M", "GREENFRAC", "LAI12M"]
vals = [         0,          0,          0,           8,           0,        0]
levs = [        21,         16,         16,          12,          12,       12]
valt = [         1,          1,          1,         -99,         -99,      -99]
levt = [        17,         14,         14,         -99,         -99,      -99]
for (ind, var) in enumerate(vars)
  grid = ncread(fila, var, start=[1,1,1,1], count=[-1,-1,-1,-1])
  for a = 1:levs[ind], b = 1:GATS, c = 1:GONS
    if a != levt[ind]
      gask[c,b,1] < 0 && (grid[c,b,a,1] = vals[ind])
    else
      gask[c,b,1] < 0 && (grid[c,b,a,1] = valt[ind])
    end
  end
  ncwrite(grid, filw, var, start=[1,1,1,1], count=[-1,-1,-1,-1])
  ncwrite(grid, filx, var, start=[1,1,1,1], count=[-1,-1,-1,-1])
end

numb = 0                                                                      # finally, update gask in filx
for a = 1:GATS, b = 1:GONS                                                    # to set negative values to zero
  global numb                                                                 # (-3,-4 values remain in filw)
  gask[b,a,1] < 0 && (gask[b,a,1] = 0 ; numb += 1)
end
ncwrite(gask, filx, "LANDMASK", start=[1,1,1], count=[-1,-1,-1])
@printf("%10d gridboxes out of %12d (%4.1f%%) are now ocean in %s\n\n",
        numb, length(glat) * length(glon), 100 * numb / length(glat) / length(glon), filx)
exit(0)

#=
        Time = UNLIMITED ; // (1 currently)
        DateStrLen = 19 ;
        west_east = 310 ;                                          geogrid.nc 2D and 3D variables
        south_north = 200 ;                                        that seem to need changing and
        south_north_stag = 201 ;                                   ocean values that they require
        west_east_stag = 311 ;
        land_cat = 21 ;
        soil_cat = 16 ;
        month = 12 ;
        float LANDUSEF  (Time, land_cat, south_north, west_east) ;  0 except 1 at level 17
        float LU_INDEX  (Time,           south_north, west_east) ; 17
        float HGT_M     (Time,           south_north, west_east) ;  0
        float SOILTEMP  (Time,           south_north, west_east) ;  0
        float SOILCTOP  (Time, soil_cat, south_north, west_east) ;  0 except 1 at level 14
        float SCT_DOM   (Time,           south_north, west_east) ; 14
        float SOILCBOT  (Time, soil_cat, south_north, west_east) ;  0 except 1 at level 14
        float SCB_DOM   (Time,           south_north, west_east) ; 14
        float ALBEDO12M (Time,    month, south_north, west_east) ;  8
        float GREENFRAC (Time,    month, south_north, west_east) ;  0
        float LAI12M    (Time,    month, south_north, west_east) ;  0
        float SNOALB    (Time,           south_north, west_east) ;  0
        float CON       (Time,           south_north, west_east) ;  0
        float VAR       (Time,           south_north, west_east) ;  0
        float OA1       (Time,           south_north, west_east) ;  0
        float OA2       (Time,           south_north, west_east) ;  0
        float OA3       (Time,           south_north, west_east) ;  0
        float OA4       (Time,           south_north, west_east) ;  0
        float OL1       (Time,           south_north, west_east) ;  0
        float OL2       (Time,           south_north, west_east) ;  0
        float OL3       (Time,           south_north, west_east) ;  0
        float OL4       (Time,           south_north, west_east) ;  0
        float VAR_SSO   (Time,           south_north, west_east) ;  0
=#
