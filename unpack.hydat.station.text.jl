#=
 = Create a text file of date-streamflow (one per line) from NetCDF - RD Aug 2023.
 =#

using My, Printf, NetCDF

const MISS             = -9999.0                        # generic missing value

if (argc = length(ARGS)) != 2
  print("\nUsage: jjj $(basename(@__FILE__)) 2017-10-01-00 hydat_03JB004.nc\n\n")
  exit(1)
end
fila = ARGS[2]
filb = ARGS[2][1:end-3] * ".txt"
filc = ARGS[2][1:end-3] * ".txtsub"
delt = datesous("1900-01-01-00", ARGS[1], "hr")
dela = datesous("1900-01-01-00", "2019-05-31-12", "hr")
delb = datesous("1900-01-01-00", "2019-10-30-12", "hr")
tima = ncread(fila, "time", start=[1],     count=[-1])
floa = ncread(fila, "flow", start=[1,1,1], count=[-1,-1,-1])[1,1,:]

b = 1
formb = formc = ""                                                            # save the station data to text
for a = 1:length(tima)
  global b, formb, formc
  if tima[a] > delt
    tdat   = dateadd("1900-01-01-00", tima[a], "hr")
                                formb *= @sprintf("%s %15.8f\n", tdat, floa[a])
    dela <= tima[a] <= delb && (formc *= @sprintf("o%-6d %-7.0f 1.0     obsgroup\n", b, floa[a]) ; b = b + 1)
  end
end

fpb = My.ouvre(filb, "w") ; write(fpb, formb) ; close(fpb)
fpc = My.ouvre(filc, "w") ; write(fpc, formc) ; close(fpc)
exit(0)
