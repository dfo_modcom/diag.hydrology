#=
 = Create links to an existing year of time-mean files in order to replicate that
 = timeseries during other years.  If the existing year is not a leap year, then
 = neither are the linked years - RD Feb, Mar 2021, Aug 2022.
 =#

const YREF             = 1901                           # year of existing time-mean
const YDUP             = 1902                           # first year of link files
const YEAS             = 2                              # number of link file years
const DAYS             = 365                            # number of days in the year

using My, Printf

if (argc = length(ARGS)) != 1
  print("\nUsage: jjj $(basename(@__FILE__)) 3\n\n")
  exit(1)
end
delh = parse(Int64, ARGS[1])

data = @sprintf("%4d010100", YREF)                                            # repeat links for years beyond YREF
datb = @sprintf("%4d010100", YDUP)
stps = div(DAYS * 24, delh)
for a = 1:stps
  global data, datb
  fila = data * ".LDASIN_DOMAIN1"
  for b = 1:YEAS
    filb = @sprintf("%4d", YDUP + b - 1) * datb[5:end] * ".LDASIN_DOMAIN1"
    if islink(filb)
      print("$filb link will be replaced by link to $fila\n")
      rm(filb)
    end
    !isfile(fila) && print("WARNING: $fila does not exist\n")
    symlink(fila, filb)
  end
  data = dateadd(data, delh, "hr")
  datb = dateadd(datb, delh, "hr")
end

data = @sprintf("%4d010100", YREF)                                            # as well as for one extra day past
datb = @sprintf("%4d010100", YDUP)                                            # the last year, for use by the model
stps = div(24, delh)
for a = 1:stps
  global data, datb
  fila = data * ".LDASIN_DOMAIN1"
  for b = 1:1
    filb = @sprintf("%4d", YDUP + YEAS) * datb[5:end] * ".LDASIN_DOMAIN1"
    if islink(filb)
      print("$filb link will be replaced by link to $fila\n")
      rm(filb)
    end
    !isfile(fila) && print("WARNING: $fila does not exist\n")
    symlink(fila, filb)
  end
  data = dateadd(data, delh, "hr")
  datb = dateadd(datb, delh, "hr")
end
exit(0)
