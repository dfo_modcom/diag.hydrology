#=
 = Represent HydroLAKES shapefiles as contiguous shapes on the WRF-Hydro routing grid.
 = Either routing_geogrid.nc or Fulldom_hires.nc can provide the lat/lon source grids, but
 = routing_geogrid.nc might be better, if only where routing lat/lon are needed surrounding
 = the outer low-res lat/lon points from geogrid.nc (i.e., Fulldom_hires.nc seems to copy
 = rather than extrapolate).  Both geogrid.nc and routing_geogrid.nc can be generated using
 = WPS.  Lake Melville (estuary) is excluded - RD Dec 2023.
 =#

using My, Printf, NetCDF, Shapefile, ArchGDAL, GeoDataFrames, GeoFormatTypes, DataFrames

const FDIS             = 2.0                            # WRF-Hydro routing grid     resolution (km)
const DEL              = 0.001                          # WRF-Hydro routing grid sub-resolution (degrees)
const MISS             = -9999.0                        # generic missing value

if (argc = length(ARGS)) != 3
  print("\nUsage: jjj $(basename(@__FILE__)) HydroLAKES_polys_v10_East_999km2.shp ../DOMAIN_full_full/routing_geogrid.nc HydroLAKES_polys_v10_East.shp\n")
  print(  "   or: jjj $(basename(@__FILE__)) HydroLAKES_polys_v10_East_999km2.shp ../DOMAIN_full_full/Fulldom_hires.nc   HydroLAKES_polys_v10_East.shp\n\n")
  exit(1)
end
fila = ARGS[1] ; shpa = Shapefile.Table(fila) ; lena = length(shpa) ; geoa = Shapefile.shapes(shpa)
filb = ARGS[2]
filc = ARGS[3]

function simplify()
  if     typeof(ncgetatt(filb,  "LATITUDE", "units")) != Nothing              # read Fulldom_hires.nc (and flip latitude)
    lats =      ncread(  filb,  "LATITUDE",  start=[1,1], count=[-1,-1])[:,end:-1:1]
    lons =      ncread(  filb, "LONGITUDE",  start=[1,1], count=[-1,-1])[:,end:-1:1]
  elseif typeof(ncgetatt(filb,      "CLAT", "units")) != Nothing              # or routing_geogrid.nc (and drop time index)
    lats =      ncread(  filb,      "CLAT",  start=[1,1,1], count=[-1,-1,-1])[:,:,1]
    lons =      ncread(  filb,     "CLONG",  start=[1,1,1], count=[-1,-1,-1])[:,:,1] 
  else
    print("\nERROR : expecting to read (with a units attribute) either a LATITUDE (Fulldom_hires.nc) or CLAT (routing_geogrid.nc) variable\n\n")
    exit(-1)
  end
  (nlon, nlat) = size(lons)

  function telescope(rlat::Float64, rlon::Float64)                            # telescope through the WRF-Hydro grid
    mindis = minlat = minlon = 9e9                                            # to get a location closest to a lake
    minlin = div(nlat, 2)                                                     # shapefile point
    minpix = div(nlon, 2)
    for a = 25:-1:0
      for b = minlin - 2^a : 2^a : minlin + 2^a
        if 1 <= b <= nlat
          for c = minpix - 2^a : 2^a : minpix + 2^a
            if 1 <= c <= nlon
              gdlatitude  = lats[c,b]
              gdlongitude = lons[c,b]
              tmpdis = (rlat - gdlatitude)^2 + (rlon - gdlongitude)^2
              if tmpdis < mindis
                mindis = tmpdis
                minlat = gdlatitude
                minlon = gdlongitude
                minlin = b
                minpix = c
#               @printf("a b c are (%8d %8d %8d) and lat lon mindis are %lf %lf %e ***\n", a, b, c, gdlatitude, gdlongitude, mindis)
#             else
#               @printf("a b c are (%8d %8d %8d) and lat lon tmpdis are %lf %lf %e\n", a, b, c, gdlatitude, gdlongitude, tmpdis)
              end
            end
          end
        end
      end
    end
    if 111 * mindis^0.5 > 10 * FDIS
      print("\nrlat rlon are $rlat $rlon\n")
      print("minlat minlon are $minlat $minlon at line pixel $minlin $minpix\n")
      print("allowable distance between them is about $(10 * FDIS) km\n")
      print("actual    distance between them is about $(111 * mindis^0.5) km\n")
      exit(0)
    end
    (minpix, minlin)
  end

  laka = Set{Tuple{Int64, Int64, Int64}}()                                    # identify all WRF-Hydro gridboxes that are
  for a = 1:lena                                                              # collocated with HydroSHEDS lake points
    locs = geoa[a].points                                                     # (and a few that ogr2ogr didn't segmentize)
    @printf("shapefile %2d has %6d points\n", a, length(locs))
    for b = 1:length(locs)
      push!(laka, (a, telescope(locs[b].y, locs[b].x)...))
    end
  end
  push!(laka, (1,343,391))
  push!(laka, (2,383,346))
  push!(laka, (2,383,345)) 
  push!(laka, (2,383,344)) ; setdiff!(laka, [(2,384,343)])
  push!(laka, (4,383,345))
  push!(laka, (4,383,344))
  push!(laka, (4,383,343))
  push!(laka, (4,384,343))
  push!(laka, (4,516,251))
  @printf("\n%9d gridboxes define the lake shapefiles on the WRF-Hydro grid\n", length(laka))

  function surround(c::Int64, b::Int64, a::Int64)                             # exclude dangling points with
    nums = 0                                                                  # less than two neighbours
    in((c,b+1,a  ), laka) && (nums += 1)
    in((c,b-1,a  ), laka) && (nums += 1)
    in((c,b  ,a+1), laka) && (nums += 1)
    in((c,b  ,a-1), laka) && (nums += 1)
    in((c,b+1,a+1), laka) && (nums += 1)
    in((c,b+1,a-1), laka) && (nums += 1)
    in((c,b-1,a+1), laka) && (nums += 1)
    in((c,b-1,a-1), laka) && (nums += 1)
    nums
  end

  numb = 1
  while (numb > 0)
    numb = 0
    for (c,b,a) in laka
      surround(c,b,a) < 2 && (setdiff!(laka, [(c,b,a)]) ; numb += 1)
    end
  end
  @printf("%9d gridboxes define the lake shapefiles after removing dangling points\n\n", length(laka))

  function shedborder(bord::Array{Tuple{Int64, Int64, Int8}}, fups::Set{Tuple{Int64, Int64}})
    (b,a,w) = bord[end]
    if     w == 0 
      !in((b  ,a+1), fups) && in((b-1,a+1), fups) && !in((b-1,a+1,3), bord) && (push!(bord, (b-1,a+1,3)) ; return(true))
      !in((b-1,a+1), fups) && in((b-1,a  ), fups) && !in((b-1,a  ,0), bord) && (push!(bord, (b-1,a  ,0)) ; return(true))
      !in((b-1,a  ), fups) &&                        !in((b  ,a  ,1), bord) && (push!(bord, (b  ,a  ,1)) ; return(true))
    elseif w == 1 
      !in((b-1,a  ), fups) && in((b-1,a-1), fups) && !in((b-1,a-1,0), bord) && (push!(bord, (b-1,a-1,0)) ; return(true))
      !in((b-1,a-1), fups) && in((b  ,a-1), fups) && !in((b  ,a-1,1), bord) && (push!(bord, (b  ,a-1,1)) ; return(true))
      !in((b  ,a-1), fups) &&                        !in((b  ,a  ,2), bord) && (push!(bord, (b  ,a  ,2)) ; return(true))
    elseif w == 2 
      !in((b  ,a-1), fups) && in((b+1,a-1), fups) && !in((b+1,a-1,1), bord) && (push!(bord, (b+1,a-1,1)) ; return(true))
      !in((b+1,a-1), fups) && in((b+1,a  ), fups) && !in((b+1,a  ,2), bord) && (push!(bord, (b+1,a  ,2)) ; return(true))
      !in((b+1,a  ), fups) &&                        !in((b  ,a  ,3), bord) && (push!(bord, (b  ,a  ,3)) ; return(true))
    else
      !in((b+1,a  ), fups) && in((b+1,a+1), fups) && !in((b+1,a+1,2), bord) && (push!(bord, (b+1,a+1,2)) ; return(true))
      !in((b+1,a+1), fups) && in((b  ,a+1), fups) && !in((b  ,a+1,3), bord) && (push!(bord, (b  ,a+1,3)) ; return(true))
      !in((b  ,a+1), fups) &&                        !in((b  ,a  ,0), bord) && (push!(bord, (b  ,a  ,0)) ; return(true))
    end
    false
  end

  poly = [Vector{Tuple{Float64, Float64}}(undef, 0) for _ = 1:lena]           # loop through each lake and
  for d = 1:lena                                                              # identify an outer border
    bord = Array{Tuple{Int64, Int64, Int8}}(undef, 0)
    fups = Set{Tuple{Int64, Int64}}()
    for (c,b,a) in laka  c == d && push!(fups, (b,a))  end

    latbeg = lonbeg = -9999                                                   # start with a northern point
    for (b,a) in fups                                                         # and search counterclockwise
      a > latbeg && (latbeg = a ; lonbeg = b)                                 # along the outer gridbox wall
    end                                                                       # (where w = 0123 = NWSE)
    push!(bord, (lonbeg,latbeg,0))
    while  shedborder(bord, fups)  end

    lon = 0.0 ; lat = DEL                                                     # save the border as a slightly
    (lontmp,lattmp) = (lon+lons[lonbeg,latbeg], lat+lats[lonbeg,latbeg])      # expanded polygon with northen
    push!(poly[d],    (lontmp,lattmp))                                        # point duplicated to close
    (lonnow,latnow) = (lontmp,lattmp)
    for (b,a,w) in bord
      w == 0 && (lon =  0.0 ; lat =  DEL)
      w == 1 && (lon = -DEL ; lat =  0.0)
      w == 2 && (lon =  0.0 ; lat = -DEL)
      w == 3 && (lon =  DEL ; lat =  0.0)
      (lontmp,lattmp) = (lon+lons[b,a], lat+lats[b,a])
      if (lonnow,latnow) != (lontmp,lattmp)
        push!(poly[d],      (lontmp,lattmp))
         (lonnow,latnow)  = (lontmp,lattmp)
      end
    end
    poly[d] = unique(poly[d])
    lon = 0.0 ; lat = DEL
    (lontmp,lattmp) = (lon+lons[lonbeg,latbeg], lat+lats[lonbeg,latbeg])
    (lonnow,latnow) != (lontmp,lattmp) && push!(poly[d], (lontmp,lattmp))
    @printf("shapefile %2d now has %6d points\n", d, length(poly[d]))
  end

  @printf("\nwriting %s\n\n", filc)
  lakb = GeoDataFrames.read(fila)                                             # replace complex with simple polygons
  for a = 1:lena                                                              # define a new ID and save shapefile
    lakb[a,1] = ArchGDAL.createpolygon(poly[a])                               # (after omitting Lake Melville)
#   @show ArchGDAL.geomarea(poly[a])
  end
  deleteat!(lakb, findall(==("Melville"), lakb.Lake_name))
  rename!(lakb, :Hylak_id => :ID)
  lakb[:,:ID] = 1:length(lakb[:,1])
  GeoDataFrames.write(filc, lakb, crs=GeoFormatTypes.EPSG(4326))
end

simplify()
exit(0)

#=
  cp(filb, "x.nc"; force = true) ; filb = "x.nc"                              # store shapefiles on grid
  mask = ones(Int64, nlon, nlat) * Int(MISS)                                  # (and view using GrADs)
  for (c,b,a) in laka
    mask[b,a] = c
  end
  ncwrite(mask, "x.nc", "FLOWACC", start=[1,1], count=[-1,-1])

cvar = "floacc"
"set gxout contour" ; "d "cvar
"set gxout grfill"  ; "d "cvar
"set gxout grid"    ; "d "cvar
"draw shp /home/riced/work/workg/hydrolakes/HydroLAKES_polys_v10_East"
=#
