#=
 = Create local links to all files in a directory and any subdirectories
 = (note that a link is created only to the last file found, where two
 =  files from different subdirs have the same name) - RD April 2020.
 =#

using My, Printf

if (argc = length(ARGS)) != 1
  print("\nUsage: jjj $(basename(@__FILE__)) ../shp.na/\n\n")
  exit(1)
end

# grp000004399_070597919_07120041380.shp

function walkdir(dir)
  absdir = abspath(dir)
  files  = readdir(absdir)
  for file in files
    full = joinpath(absdir, file)
    if isfile(full) && startswith(file, "grp")
      if !isfile(file) && !islink(file)
        symlink(full, file)
      else
        print("WARNING : $file already exists\n")
      end
#     year = parse(Int64, file[18:21])
#     if 1990 <= year <= 2004
#       if islink(file)
#         print("$file link already exists, but replacing with a link to $full\n")
#         rm(file)
#       end
#       symlink(full, file)
#     end
    elseif isdir(full)
      walkdir(full)
    end
  end
end

walkdir(ARGS[1])
