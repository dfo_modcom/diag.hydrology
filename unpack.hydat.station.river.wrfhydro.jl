#=
 = Update the text station file (.sta) with the location of WRF-Hydro river
 = reach endpoint that is closest to the HyDAT station location - RD Apr 2023.
 =#

using My, Printf

const EARTH            = 6.371e6                        # mean Earth radius (m)
const D2R              = pi / 180.0                     # degrees to radians conversion
const MISS             = -9999.0                        # generic missing value

if (argc = length(ARGS)) != 2
  print("\nUsage: jjj $(basename(@__FILE__)) ../DOMAIN_full_full/ hydat_03JB002.sta\n\n")
  exit(1)
end
fila = ARGS[2]
filb = ARGS[2] * ".hwshed"
filc = ARGS[1] * "Fulldom_hires_river.txt"

function subroute()                                                           # locate a WRF-Hydro river reach
  fpa   = My.ouvre(fila, "r")                                                 # endpoint that is near a station
  lins  = readlines(fpa, keep = true) ; close(fpa)                            # (or else select a location that
  hlat  = parse(Float64, split(lins[6])[2])                                   # yields the desired drainage)
  hlon  = parse(Float64, split(lins[7])[2])
  ARGS[2] == "hydat_01AD002.sta" && (hlat = 47.2868 ; hlon = -68.5730)
  ARGS[2] == "hydat_01BC001.sta" && (hlat = 47.7001 ; hlon = -67.4533)
  ARGS[2] == "hydat_01BO001.sta" && (hlat = 46.7477 ; hlon = -65.8073)
# ARGS[2] == "hydat_02XC001.sta" && (hlat = 51.7865 ; hlon = -57.6177)
  ARGS[2] == "hydat_03MD001.sta" && (hlat = 56.7767 ; hlon = -64.8887)
# ARGS[2] == "hydat_04FC001.sta" && (hlat = 53.0997 ; hlon = -85.0917)
  ARGS[2] == "hydat_04KA001.sta" && (hlat = 51.1256 ; hlon = -80.9521)
  ARGS[2] == "hydat_04LJ001.sta" && (hlat = 49.5636 ; hlon = -83.2922)
  ARGS[2] == "hydat_04MF001.sta" && (hlat = 51.0402 ; hlon = -80.7988)
# ARGS[2] == "hydat_04NA001.sta" && (hlat = 48.2759 ; hlon = -78.2231)

           minlai = minloi = 0
  mindis = minlat = minlon = 9e9
  fpa  = My.ouvre(filc, "r")
  locs = readlines(fpa, keep = true) ; close(fpa)
  for sloc in locs
    (slat, slon, tmpa, tmpb, slai, sloi) = split(sloc)
    tlat = parse(Float64, slat)
    tlon = parse(Float64, slon)
    tdis = abs(tlat - hlat) + abs(tlon - hlon) * cos(hlat * D2R)
    if tdis < mindis
      mindis = tdis ; minlat = tlat ; minlon = tlon
      minlai = parse(Int64, slai)
      minloi = parse(Int64, sloi)
    end
  end

  form  = "" ; for a =  1:89           form *= lins[a]  end
  form *= @sprintf("%-24s %f %d\n",  "LATITUDE", minlat, minlai)
  form *= @sprintf("%-24s %f %d\n", "LONGITUDE", minlon, minloi)
               for a = 92:length(lins) form *= lins[a]  end
  fpa   = My.ouvre(fila, "w")
  write(fpa, form) ; close(fpa)
end

subroute()
exit(0)
